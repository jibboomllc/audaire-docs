EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:speaker-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L D D1
U 1 1 58A6674E
P 4750 2750
F 0 "D1" H 4750 2850 50  0000 C CNN
F 1 "1N400" H 4750 2650 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P7.62mm_Horizontal" H 4750 2750 50  0001 C CNN
F 3 "" H 4750 2750 50  0000 C CNN
	1    4750 2750
	0    1    1    0   
$EndComp
$Comp
L SPEAKER SP1
U 1 1 58A6678C
P 5700 2750
F 0 "SP1" H 5600 3000 50  0000 C CNN
F 1 "SPEAKER" H 5600 2500 50  0000 C CNN
F 2 "Buzzers_Beepers:Buzzer_12x9.5RM7.6" H 5700 2750 50  0001 C CNN
F 3 "" H 5700 2750 50  0000 C CNN
	1    5700 2750
	1    0    0    -1  
$EndComp
$Comp
L PN2222A Q1
U 1 1 58A6680E
P 5000 3400
F 0 "Q1" H 5200 3475 50  0000 L CNN
F 1 "PN2222A" H 5200 3400 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 5200 3325 50  0001 L CIN
F 3 "" H 5000 3400 50  0000 L CNN
	1    5000 3400
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 58A680BF
P 4550 3400
F 0 "R1" V 4630 3400 50  0000 C CNN
F 1 "R" V 4550 3400 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4480 3400 50  0001 C CNN
F 3 "" H 4550 3400 50  0000 C CNN
	1    4550 3400
	0    1    1    0   
$EndComp
$Comp
L SPEAKER SP2
U 1 1 58A687AF
P 3950 3500
F 0 "SP2" H 3850 3750 50  0000 C CNN
F 1 "SPKR HOLES" H 3850 3250 50  0000 C CNN
F 2 "Buzzers_Beepers:BUZZER" H 3950 3500 50  0001 C CNN
F 3 "" H 3950 3500 50  0000 C CNN
	1    3950 3500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 2650 5400 2650
Wire Wire Line
	5100 2450 5100 2650
Wire Wire Line
	5400 2850 5100 2850
Wire Wire Line
	5100 2850 5100 3200
Wire Wire Line
	5100 3600 5100 3800
Wire Wire Line
	4750 2600 4750 2550
Wire Wire Line
	4750 2550 5100 2550
Connection ~ 5100 2550
Wire Wire Line
	4750 2900 4750 2950
Wire Wire Line
	4750 2950 5100 2950
Connection ~ 5100 2950
Wire Wire Line
	4800 3400 4700 3400
Wire Wire Line
	4400 3400 4250 3400
Wire Wire Line
	4250 3600 4550 3600
Wire Wire Line
	4550 3600 4550 3650
Wire Wire Line
	4550 3650 5100 3650
Connection ~ 5100 3650
$Comp
L TEST_1P W1
U 1 1 58A726D0
P 5100 2450
F 0 "W1" H 5100 2720 50  0000 C CNN
F 1 "5V" H 5100 2650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 5300 2450 50  0000 C CNN
F 3 "" H 5300 2450 50  0000 C CNN
	1    5100 2450
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P W2
U 1 1 58A72704
P 5100 3800
F 0 "W2" H 5100 4070 50  0000 C CNN
F 1 "GND" H 5100 4000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 5300 3800 50  0000 C CNN
F 3 "" H 5300 3800 50  0000 C CNN
	1    5100 3800
	-1   0    0    1   
$EndComp
$EndSCHEMATC
