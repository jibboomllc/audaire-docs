#!/usr/bin/php
<?php 

$options = getopt("t:c:");
//var_dump($options);

if (array_key_exists("t", $options)) {
	$templateFilename = $options['t'];
	$outputFilename = "label.zpl";
	$ftpServer = "192.168.0.100";
	$remotePath = "/dest/d1prn";

	// Read template zpl
	$handle = fopen($templateFilename, "r");
	if ($handle) {
		$tempString = fread($handle, filesize($templateFilename));
		fclose($handle);

		// Create real zpl
		$handle = fopen("label.zpl", "w+");
		if ($handle) {
			$id = str_replace(".", "0", uniqid('a', true));
			fprintf($handle, "%s", str_replace("{{id}}", $id, $tempString));
			fclose($handle);
			print("\nOutput file " . $outputFilename . " with id = " . $id . ", written.\n");

			// Print label
			$connId = ftp_connect($ftpServer);
			if ($connId) {
				if (ftp_login($connId, "root", "")) {
					$loginResult = ftp_login($connId, "root", "");
					if (ftp_chdir($connId, $remotePath)) {
						if (ftp_put($connId, $outputFilename, $outputFilename, FTP_ASCII)) {
							print("Successfully uploaded $outputFilename\n");
						} else {
							print("ERROR: There was a problem while uploading $outputFilename\n");
						}
					} else {
						print("ERROR: There was a problem while changing directory on $ftpServer\n");
					}
				} else {
					print("ERROR: There was a problem while logging into $ftpServer\n");
				}

				ftp_close($connId);
			} else {
				print("ERROR: There was a problem while connecting to $ftpServer -- make sure than you are on the same subnet as the printer\n");
			}

		} else {
			print("\nERROR: Output file " . $outputFilename . " could not be opened.\n");
		}

	} else {
		print("\nERROR: Template file " . $templateFilename . " does not exist or could not be opened.\n");
	}

} else {
	print("ERROR: You must specify a template file with the -t parameter\n");
}

exit(0);

?>