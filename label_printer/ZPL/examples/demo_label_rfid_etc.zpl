^XA

^FX ftp root@192.168.0.100  
^FX cd /dest/d1prn 
^FX put demo_label_rfid_etc.zpl 

^FX FN0 is a placeholder field variable for the tag data

^FX Left section
^CFA,20
^FO20,135^FDAudaire Health, Inc.^FS
^FO20,160^FDhttps://audaire.com^FS
^FO20,190^FN0^FS

^FX Right section with QR Code
^FO320,120^BQ,2,3^FN0^FS

^FX Sets tag type to Gen 2
^RS8

^FX R,H = read hexadecimal. The read results are put into field variable 0 (FN0). 
^FX At this point, the printer substitutes previous instances of FN0 in the label
^FX format with the data from this field. 
^FN0^FDQA,^RFR,H^FS

^XZ

