^XA

^FX ftp root@192.168.0.100  
^FX cd /dest/d1prn 
^FX put demo_label_rfid_etc.zpl 


^FX Left section
^CFA,20
^FO20,135^FDAudaire Health, Inc.^FS
^FO20,160^FDhttps://audaire.com^FS
^FO20,190^FD0123456789abcdef01234567^FS

^FX Right section with QR Code
^FO320,120^BQ,2,7^FDQA,0123456789abcdef01234567^FS

^FX Sets tag type to Gen 2
^RS8

^FX W,H = write hexadecimal
^RFW,H^FD0123456789abcdef01234567^FS

^XZ
