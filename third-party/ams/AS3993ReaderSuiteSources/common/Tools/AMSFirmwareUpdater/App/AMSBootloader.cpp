/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSBootloader
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author F. Lobmaier
 *
 *  \brief  AMSBootloader class header file
 *
 *  AMSBootloader is an abstract class used for abstraction
 *  of different firmware upgrade mechanisms between the PC
 *  and the demo boards.
 */

#include "stdafx.h"
#include "AMSBootloader.h"

const char * AMSBootloader::errorStrings[ ] =
    { "Success"
    , "Error: cannot connect to bootloader"
    , "Error: cannot erase the flash"
    , "Error: cannot program the flash"
    , "Error: cannot start the application"
    , "Error: cannot open the image file"
    , "Error: out of memory"
    , "Error: wrong parameters"
    , "Error: image file is incompatible with bootloader"
    , "Error: unknown"
    };
const char * AMSBootloader::errorToString( Error errCode )
{
    int max = (sizeof( errorStrings )/sizeof(const char * ) ) - 1;
    if ( errCode > max )
    {
        errCode = static_cast< enum Error >( max );
    }
    return errorStrings[ errCode ];
}


AMSBootloader::AMSBootloader() : itsUpgradeStartAddress( 0 )
{
}