/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSBootloader
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author F. Lobmaier
 *
 *  \brief  AMSBootloader class header file
 *
 *  AMSBootloader is an abstract class used for abstraction
 *  of different firmware upgrade mechanisms between the PC
 *  and the demo boards.
 */

#ifndef AMSBOOTLOADER_H
#define AMSBOOTLOADER_H

#include <QtGui>
#include "AMSCommunication.hxx"



class AMSBootloader : public QObject
{
    static const char * errorStrings[ ];

public:
	enum Error
	{
		NoError = 0,
		ConnectionError = 1,
		EraseFlashError = 2,
		ProgramError = 3,
		AppStartError = 4,
		FileError = 5,
		MemoryError = 6,
		ParameterError = 7,
        VersionError = 8 /* the image is for bootloader 1.z and the board has bootloader 2.x.y */
	};

    /* function to convert error code to a human readable string */
    static const char * errorToString( Error errCode ); 
         
	AMSBootloader();

	virtual Error connect() = 0;
	virtual Error eraseFlash(unsigned int startAddress, unsigned int length) = 0;
	virtual Error writeFlash(const char * buffer, unsigned int address, unsigned int size) = 0;
	virtual Error resetDevice() = 0;
	virtual int getChunkSize() = 0;
	virtual int getBytesPerAddress() = 0;
	virtual Error disconnect() = 0;
    virtual bool needBootloaderUpgrade( QString & filename, int & address, bool & isBootloaderIncompatible ) { isBootloaderIncompatible = false; return false; };
    QString & bootloaderFileName( ) { return itsBootloaderName; };
    int bootloaderStartAddress( ) { return itsUpgradeStartAddress; };
protected:
	QString itsBootloaderName;
    int itsUpgradeStartAddress;
    AMSCommunication *com;
};

#endif // AMSBOOTLOADER_H