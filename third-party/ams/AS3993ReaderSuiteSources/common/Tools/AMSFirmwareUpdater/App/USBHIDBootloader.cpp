/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "stdafx.h"
#include "USBHIDBootloader.h"
#include "USBHIDCommunication.hxx"
#include "AMSBootloader.h"
#include "AMSCommunication.hxx"

#define HID_AMS_VID		0x1325
#define HID_GENERIC_PID 0x0000
#define CHUNK_SIZE		60

class USBHIDBootloader::FlashDelay : public QThread
{
public:
    static void msleep(unsigned long msecs) {
        QThread::msleep(msecs);
    }
};

USBHIDBootloader::USBHIDBootloader()
{
	usbhidConfigStructure usbConfig;
	
	AMSCommunication *com = new USBHIDCommunication(0x46);
	usbConfig.pid = HID_GENERIC_PID;
	usbConfig.vid = HID_AMS_VID;
	usbConfig.inReportID = 0x81;
	usbConfig.outReportID = 0x91;
	com->setConnectionProperties(&usbConfig);
	this->com = com;
}

AMSBootloader::Error USBHIDBootloader::connect()
{
	if (com->connect() != AMSCommunication::NoError)
	{
		return ConnectionError;
	}
	return NoError;	
}

AMSBootloader::Error USBHIDBootloader::disconnect()
{
	com->disconnect();
	return NoError;	
}

AMSBootloader::Error USBHIDBootloader::eraseFlash(unsigned int startAddress, unsigned int length)
{
	int k;
	unsigned char *intptr;
	QString command;
	QString answer;

	OutputReportBuffer[0] = 0x55; //FLASH_PROGRAM

	/* first, erase flash */
	OutputReportBuffer[1] = 3; /* flash erase */
	intptr = (unsigned char*)&length;
    OutputReportBuffer[2] = *(intptr + 3);
    OutputReportBuffer[3] = *(intptr + 2);
    OutputReportBuffer[4] = *(intptr + 1);
    OutputReportBuffer[5] = *(intptr + 0);
	
	command.clear();
	for (k=0; k<=5;k++)
	{
		command.append(QString::QString("%1").arg(QString::number(OutputReportBuffer[k], 16), 2, '0'));
	}
	command.prepend("SetOutputReport_Control(");
	command.append(")");

	if (this->com->sendCommand(command, &answer) == AMSCommunication::NoError)
	{
		command.clear();
		command = "GetInputReport_Interrupt()";
        FlashDelay::msleep(200);
		if (this->com->sendCommand(command, &answer) == AMSCommunication::NoError)
		{
			return NoError;
		}
	}
	return EraseFlashError;
}

AMSBootloader::Error USBHIDBootloader::writeFlash(const char * buffer, unsigned int address, unsigned int size)
{
	QString answer;
	QString command;
	unsigned int k;
	
	OutputReportBuffer[0] = 0x55; //FLASH_PROGRAM
	OutputReportBuffer[1] = 1; /* write flash */
		
    memcpy(&OutputReportBuffer[3], buffer, size);
    OutputReportBuffer[2] = size;
	command.clear();
	for (k=0; k<size+3;k++)
	{
		command.append(QString::QString("%1").arg(QString::number(OutputReportBuffer[k], 16), 2, '0'));
	}
	command.prepend("SetOutputReport_Control(");
	command.append(")");
	if (this->com->sendCommand(command, &answer) == AMSCommunication::NoError)
	{
		command.clear();
		command = "GetInputReport_Interrupt()";
		if (this->com->sendCommand(command, &answer) == AMSCommunication::NoError)
		{
			if (!answer.mid(4, 2).startsWith("ff"))
			{
				return ProgramError;
			}
		}
		else
		{
			return ProgramError;
		}
	}
	else
	{
		return ProgramError;
	}
	return NoError;

}

AMSBootloader::Error USBHIDBootloader::resetDevice()
{
	QString answer;
	QString command;
	int k;
	
	OutputReportBuffer[0] = 0x55; //FLASH_PROGRAM
	OutputReportBuffer[1] = 2; // start image

	command.clear();
	for (k=0; k<=1;k++)
	{
		command.append(QString::QString("%1").arg(QString::number(OutputReportBuffer[k], 16), 2, '0'));
	}
	command.prepend("SetOutputReport_Control(");
	command.append(")");

    if (this->com->sendCommand(command, &answer) == AMSCommunication::NoError)
	{
	    command.clear();
		command = "GetInputReport_Interrupt()";
		com->sendCommand(command, &answer);
	}
	return NoError;
}

int USBHIDBootloader::getChunkSize()
{
	return CHUNK_SIZE;
}

int USBHIDBootloader::getBytesPerAddress()
{
	return 1;
}