/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "stdafx.h"
#include "PicUSBBootloader.h"
#include "USBHIDCommunication.hxx"
#include "AMSBootloader.h"
#include "AMSCommunication.hxx"

#define BOOTLOADER_UPGRADE_START_ADDRESS 0x2000 /* start with the application id and user reset */

#define HID_AMS_VID		0x1325
#define HID_GENERIC_PID 0x0000
#define CHUNK_SIZE		56

/*! Protocol Commands */
#define WT_FLASH    	0x01	/*!< write flash command */
#define RESET_DEV		0x02	/*!< reset device command */
#define ER_FLASH    	0x03	/*!< erase flash command */
#define WT_CONFIG       0x04    /*!< allow/deny write config page command */

#define USER_PROG_RESET_ADDR    0x8FFE    /*!< User app reset vector location */
#define APP_ID_ADDR     		0x8FFC    /*!< application ID location */
#define APP_ID					0xBAAE	  /*!< application ID */

PicUSBBootloader::PicUSBBootloader( ) : AMSBootloader( )  
{
	init(HID_GENERIC_PID);
}

PicUSBBootloader::PicUSBBootloader(unsigned int pid) : AMSBootloader( )
{
	init(pid);
}

AMSBootloader::Error PicUSBBootloader::connect()
{
	if (com->connect() != AMSCommunication::NoError)
	{
		return ConnectionError;
	}
	return NoError;	
}

AMSBootloader::Error PicUSBBootloader::disconnect()
{
	com->disconnect();
	return NoError;	
}

bool PicUSBBootloader::needBootloaderUpgrade( QString & filename, int & address, bool & isBootloaderIncompatible )
{
    bool isBootloader2Installed = false;
    bool firmwareNeedsBootloader2 = false;
    bool isStartAddressForBootloader2 = ( address == 0 ); /* if 0== bootloader2 required */
    isBootloaderIncompatible = false; 

    if (  itsBootloaderName.isEmpty() /* only pic usb bootloader can upgrade all others have this string empty */
       || filename.compare( itsBootloaderName ) == 0 /* if we try to upgrade for bootloader itself do not use this function again */
       )
    { 
        return false;
    }

    /* find out if it is bootloader 1.z or 2.x.y */
    if ( NoError == eraseFlash( address, 1 ) )
    {
        QString answerStr = answer.mid( 2,2 );
        bool success;
        unsigned int ans = answerStr.toUInt( &success, 16 );
        if ( static_cast< unsigned char>( ans ) == static_cast< unsigned char >( ~ER_FLASH ) )
        { /* this is bootloader 2.x.y */
            isBootloader2Installed = true;
        }
    }

    /* find out if image is for bootloader 1.z or 2.x.y */
    QFile file(filename);
	if ( file.open( QIODevice::ReadOnly ) )
	{
	    if ( file.size() > ( APP_ID_ADDR * 2 ) ) /* application size is in 16-bit words, file size is in bytes */
        {
            char b1;
            char b2;
            file.seek( APP_ID_ADDR * 2 );
            if ( 1 == file.read( &b1 , 1 ) && 1 == file.read( &b2, 1 ) )
            {
                unsigned short appId = ( b2 << 8 ) | ( 0xFF & b1 );
                if ( appId == static_cast< unsigned short>( APP_ID ) )
                {
                    firmwareNeedsBootloader2 = true;
                }
            }
        }
        file.close();
    }

    bool upgrade;
    if ( isBootloader2Installed )
    { 
        upgrade = false; /* we already have bootloader2 installed, we cannot upgrade bootloader */
        if ( firmwareNeedsBootloader2 )
        { /* no upgrade is needed */
            if ( ! isStartAddressForBootloader2 )
            {
                address = 0; /* bootloader needs start address 2 ! */
            } /* else everything is for bootloader 2 ! great */
        }
        else /* bootloader is 2.x.y but firmware is for 1.z -> error cannot go on */
        {
            isBootloaderIncompatible = true;
        }
    }
    else /* bootloader 1.z is installed */
    {
        if ( firmwareNeedsBootloader2 )
        { /* upgrade is needed */
            upgrade = true;
            if ( ! isStartAddressForBootloader2 )
            {
                address = 0; /* bootloader needs start address 2 ! */
            }
        }
        else /* everything is for bootloader 1.z */
        {
            upgrade = false;
        }
    }

    return upgrade;
}

AMSBootloader::Error PicUSBBootloader::eraseFlash(unsigned int startAddress, unsigned int length)
{
	int k;
	unsigned char *intptr;
	QString command;

	OutputReportBuffer[0] = 0x55; //FLASH_PROGRAM

	/* first, erase flash */
	OutputReportBuffer[1] = 3; /* flash erase */
    intptr = (unsigned char*)&startAddress;    
    OutputReportBuffer[2] = *(intptr + 0);
    OutputReportBuffer[3] = *(intptr + 1);
    OutputReportBuffer[4] = *(intptr + 2);
    OutputReportBuffer[5] = *(intptr + 3);
	intptr = (unsigned char*)&length;
    OutputReportBuffer[6] = *(intptr + 0);
    OutputReportBuffer[7] = *(intptr + 1);
    OutputReportBuffer[8] = *(intptr + 2);
    OutputReportBuffer[9] = *(intptr + 3);
	
	command.clear();
	for (k=0; k<=9;k++)
	{
		command.append(QString::QString("%1").arg(QString::number(OutputReportBuffer[k], 16), 2, '0'));
	}
	command.prepend("SetOutputReport_Interrupt(");
	command.append(")");
    AMSCommunication::Error result = com->sendCommand(command, &answer);
	if (result == AMSCommunication::NoError)
	{
		command.clear();
		command = "GetInputReport_Interrupt()";
		if (this->com->sendCommand(command, &answer) == AMSCommunication::NoError)
		{
			return NoError;
		}
	}
	return EraseFlashError;
}

AMSBootloader::Error PicUSBBootloader::writeFlash(const char * buffer, unsigned int address, unsigned int size)
{
	QString command;
	unsigned int k;
    unsigned char *intptr;
	
	OutputReportBuffer[0] = 0x55; //FLASH_PROGRAM
	OutputReportBuffer[1] = 1; /* write flash */
		
    memcpy(&OutputReportBuffer[7], buffer, size);
    intptr = (unsigned char*)&address;    
    OutputReportBuffer[2] = *(intptr + 0);
    OutputReportBuffer[3] = *(intptr + 1);
    OutputReportBuffer[4] = *(intptr + 2);
    OutputReportBuffer[5] = *(intptr + 3);
    OutputReportBuffer[6] = size;
	command.clear();
	for (k=0; k<size+7;k++)
	{
		command.append(QString::QString("%1").arg(QString::number(OutputReportBuffer[k], 16), 2, '0'));
	}
	command.prepend("SetOutputReport_Interrupt(");
	command.append(")");
	if (this->com->sendCommand(command, &answer) == AMSCommunication::NoError)
	{
		command.clear();
		command = "GetInputReport_Interrupt()";
		if (this->com->sendCommand(command, &answer) == AMSCommunication::NoError)
		{
			if (!answer.mid(4, 2).startsWith("ff"))
			{
				//return ProgramError;
			}
		}
		else
		{
			return ProgramError;
		}
	}
	else
	{
		return ProgramError;
	}
	return NoError;

}

AMSBootloader::Error PicUSBBootloader::resetDevice()
{
	QString command;
	int k;
	
	OutputReportBuffer[0] = 0x55; //FLASH_PROGRAM
	OutputReportBuffer[1] = 2; // start image

	command.clear();
	for (k=0; k<=1;k++)
	{
		command.append(QString::QString("%1").arg(QString::number(OutputReportBuffer[k], 16), 2, '0'));
	}
	command.prepend("SetOutputReport_Interrupt(");
	command.append(")");

    if (this->com->sendCommand(command, &answer) == AMSCommunication::NoError)
	{
	    command.clear();
		command = "GetInputReport_Interrupt()";
		com->sendCommand(command, &answer);
	}
	return NoError;
}

int PicUSBBootloader::getChunkSize()
{
	return CHUNK_SIZE;
}

int PicUSBBootloader::getBytesPerAddress()
{
	return 2;
}

void PicUSBBootloader::init(unsigned int pid)
{
	itsUpgradeStartAddress = BOOTLOADER_UPGRADE_START_ADDRESS;
    itsBootloaderName = "PIC24F_USB_HID_Bootloader_Upgrade.bin";
	usbhidConfigStructure usbConfig;
	AMSCommunication *com = new USBHIDCommunication(0x46);
    com->getConnectionProperties(&usbConfig);
    usbConfig.pid = pid;
	usbConfig.vid = HID_AMS_VID;
    usbConfig.reportType = INTERRUPT_REPORT;	
	usbConfig.inReportID = 0x81;
	usbConfig.outReportID = 0x91;
	com->setConnectionProperties(&usbConfig);
	this->com = com;
}