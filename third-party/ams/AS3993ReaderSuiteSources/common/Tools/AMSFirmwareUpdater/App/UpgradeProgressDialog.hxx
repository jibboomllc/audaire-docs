/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Firmware updater lib
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file UpgradeProgressDialog.h
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief  A progress dialog which can be used to show the progress of firmware
 *          upgrade.
 *
 *  Closing the dialog before upgrade finished is prevented.
 *  example usage:
 *  UpgradeProgressDialog* progress = new UpgradeProgressDialog(upgrader, this);
 *  QThread *thread = new QThread;
 *  upgrader->moveToThread(thread);
 *  thread->start();
 *  QMetaObject::invokeMethod(upgrader, "upgradeSlot", Qt::QueuedConnection);
 *  progress->exec();
 */


#ifndef UPGRADEPROGRESSDIALOG_H
#define UPGRADEPROGRESSDIALOG_H

#include <QProgressDialog>

class AMSFirmwareUpgrader;
class QString;
class QWidget;
 
class UpgradeProgressDialog : public QProgressDialog
{
    Q_OBJECT

public:
    UpgradeProgressDialog(AMSFirmwareUpgrader * upgrader, QWidget * parent = 0, const QString &labelText = "Updating Firmware...");

private:
	void reject();

};

#endif