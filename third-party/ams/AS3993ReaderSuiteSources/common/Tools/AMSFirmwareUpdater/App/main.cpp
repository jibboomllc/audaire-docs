/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
// AMSBootloaderr.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <stdio.h>
#include "AMSCommunication.hxx"
#include "AMSFirmwareUpgrader.h"
#include "USBHIDBootloader.h"
#include "USBHIDCommunication.hxx"
#include "UARTQtCommunication.hxx"
#include "USBWinApiCommunication.hxx"
#include "PicUARTBootloader.h"
#include "PicUSBBootloader.h"
#include "AmsComStream.h"
#include "HidComDriver.h"
#include "CtrlComObjects.h"

const char* DEV_TYPE_SILABS = "silabs";
const char* DEV_TYPE_PICUART = "picuart";
const char* DEV_TYPE_PICUSB = "picusb";

const char* VERSION = "2.0.3";

void usage(char *progname)
{
    printf("Version %s\n", VERSION);
    printf("\nUsage:\n%s binfile <%s|%s|%s> [startaddress] [comportnumber]\n", progname, DEV_TYPE_SILABS, DEV_TYPE_PICUART, DEV_TYPE_PICUSB);
	printf("\nExamples:\n%s new_fw.bin %s 0x1000 1\n", progname, DEV_TYPE_PICUART);
    printf("%s new_fw.bin %s 0x1E00 --streampid=c20e\n", progname, DEV_TYPE_PICUSB);
    printf("%s new_fw.bin %s 0x1E00 --pid=c429\n", progname, DEV_TYPE_PICUSB);
	printf("%s improved_fw.bin %s\n", progname, DEV_TYPE_SILABS);
    printf("%s improved_fw.bin %s --pid=C430\n", progname, DEV_TYPE_SILABS);
}

int main(int argc, char *argv[])
{
	int address = 0, port = 0;
	AMSBootloader *bootloader;
	AMSFirmwareUpgrader *upgrader;
	unsigned int pid = 0;
    bool isPicHid = false;
    bool isPicStream = false;

	QCoreApplication  app(argc, argv);

	if (argc < 3)
	{
		usage(argv[0]);
		return (int)AMSBootloader::ParameterError;
	}

	if (strncmp(DEV_TYPE_SILABS, argv[2], strlen(DEV_TYPE_SILABS)) == 0)
	{
        int curr_arg = 3;
        if (curr_arg < argc)
        {
            if (strncmp("--pid=",argv[curr_arg],6) == 0)
            {
                sscanf_s(argv[curr_arg]+6, "%x", &pid);
                if ( pid == 0)
                { /* 0 is also not a valid pid, 0 is reserved for bootloader */
                    printf("not a valid --pid=....\n");
                    usage(argv[0]);
                    return (int)AMSBootloader::ParameterError;
                }
            }
        }
        if (pid)
        {
            AMSCommunication *com = new USBHIDCommunication(0x00);
            usbhidConfigStructure connectionProperties;
            //FIXME: Why are this functions of USBHIDCommunication protected?
            ((AMSCommunication*)com)->getConnectionProperties(&connectionProperties);
            connectionProperties.pid = pid;
            connectionProperties.vid = 0x1325;
            connectionProperties.inReportID = 0x01;
            connectionProperties.outReportID = 0x02;
            connectionProperties.reportType = CONTROL_REPORT;
            ((AMSCommunication*)com)->setConnectionProperties(&connectionProperties);
            printf("Connecting to silabs application to enter bootloader\n");
            com->connect();
            if (com->isConnected())
            {
                printf("connected\n");
                QString command, answer;
                command = "SetOutputReport_Control(020B)";   // outReportID, cEnterBootloader
                com->sendCommand(command, &answer);
                com->disconnect();
                printf("command sent\n");
            }
            else
            {
                printf("could not connect to silabs application to switch into bootloader mode\n");
            }
        }
		bootloader = new USBHIDBootloader();
	}
    else if (strncmp(DEV_TYPE_PICUSB, argv[2], strlen(DEV_TYPE_PICUSB)) == 0)
	{
		int curr_arg = 3;
		address = 0x1e00;
		while (curr_arg < argc)
		{
            if (strncmp("--pid=", argv[curr_arg],6) == 0)
            {
                sscanf_s(argv[curr_arg]+6, "%x", &pid);
                if ( pid == 0)
                { /* 0 is also not a valid pid, 0 is reserved for bootloader */
                    printf("not a valid --pid=....\n");
                    usage(argv[0]);
                    return (int)AMSBootloader::ParameterError;
                }
                isPicHid = true;
            }
			else if (strncmp("--streampid=",argv[curr_arg],12) == 0)
			{
				sscanf_s(argv[curr_arg]+12, "%x", &pid);
				if ( pid == 0)
				{ /* 0 is also not a valid pid, 0 is reserved for bootloader */
					printf("not a valid --streampid=....\n");
					usage(argv[0]);
					return (int)AMSBootloader::ParameterError;
				}
                isPicStream = true;
			}
			else if (0 == sscanf_s(argv[curr_arg], "0x%x", &address))
			{
				printf("bad address\n");
				usage(argv[0]);
				return (int)AMSBootloader::ParameterError;
			}
			curr_arg++;
		}
        if (pid)
		{
            if (isPicStream)
            {
			    HidComDriver driver(pid,AMS_VID);
			    AmsComStream stream(driver);
			    EnterBootloaderObject enterBoot;
			    printf("Connecting to stream application to enter bootloader\n");
			    fflush(stdout);
			    if (stream.open())
			    {
					if (stream.isProtocolVersionCompatible(true))
					{
						stream<<enterBoot;
						stream.close();
					}
					else
					{
						printf("Tried to enter bootloader mode with streamV1\n",pid);
						fflush(stdout);
					}
			    }
			    else
			    {
				    printf("could not open stream application with pid %x, continuing...\n",pid);
				    fflush(stdout);
			    }
            }
            else if (isPicHid)
            {
                USBHIDCommunication hidComm(pid);
                usbhidConfigStructure connectionProperties;
                //FIXME: Why are this functions of USBHIDCommunication protected?
                ((AMSCommunication*)&hidComm)->getConnectionProperties(&connectionProperties);
                connectionProperties.pid = pid;
                connectionProperties.vid = AMS_VID;
                connectionProperties.inReportID = 0x00;
                connectionProperties.outReportID = 0x00;
                connectionProperties.reportType = INTERRUPT_REPORT;
                ((AMSCommunication*)&hidComm)->setConnectionProperties(&connectionProperties);
                hidComm.connect();
                if (hidComm.isConnected())
                {
                    QString command, answer;

                    // put decvice into firmware upgrade mode
                    command = "SetOutputReport_Interrupt(00EB00)";
                    hidComm.sendCommand(command, &answer);
                    hidComm.disconnect();
                }
            }
		}

		bootloader = new PicUSBBootloader();
	}
	else if(strncmp(DEV_TYPE_PICUART, argv[2], strlen(DEV_TYPE_PICUART)) == 0)
	{
		if (argc < 5)
		{
			usage(argv[0]);
			return (int)AMSBootloader::ParameterError;
		}
		if (0 == sscanf_s(argv[4], "%d", &port))
		{
			usage(argv[0]);
			return (int)AMSBootloader::ParameterError;
		}
		bootloader = new PicUARTBootloader(port);
		if (0 == sscanf_s(argv[3], "0x%x", &address))
		{
			usage(argv[0]);
			return (int)AMSBootloader::ParameterError;
		}
	}
	else
	{
		printf("ERROR: device type not provided or not recognized\n");
		usage(argv[0]);
		return (int)AMSBootloader::ParameterError;
	}

	upgrader = new AMSFirmwareUpgrader(bootloader);
	return upgrader->doUpgrade(QString(argv[1]), address);

	QTimer::singleShot(1000, &app, SLOT(quit())); //stop after 1 seconds (will execute the event queue!)
	return app.exec(); //and we run the application}
}