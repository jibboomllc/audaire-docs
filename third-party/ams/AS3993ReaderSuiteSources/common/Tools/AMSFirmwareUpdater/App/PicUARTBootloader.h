/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSBootloader
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  Declaration for Microchip's UART bootloader for PIC 24FJ series
 *
 *  Class declaration for the UART bootloader for the PIC 24FJ series from
 *  Microchip. This class inherits from AMSBootloader which provides an
 *  generic bootloader interface for uC firmware upgrades.
 */

#ifndef PICUARTBOOTLOADER_H
#define PICUARTBOOTLOADER_H

#include "AMSCommunication.hxx"
#include "AMSBootloader.h"

#define COMMAND_READVER		0
#define COMMAND_READPM		1
#define COMMAND_WRITEPM		2
#define COMMAND_ERASEPM		3
#define COMMAND_READEE		4
#define COMMAND_WRITEEE		5
#define COMMAND_READCFG		6
#define COMMAND_WRITECFG	7
#define COMMAND_VERIFYOK	8
#define COMMAND_RESET   	31
#define MAX_PACKET			261
#define ERASE_PAGESIZE 2048
#define WRITE_PAGESIZE 256

#define STX    85	//0x55 for Autobaud
#define ETX		4
#define DLE		5

class PicUARTBootloader : public AMSBootloader
{
public:
	PicUARTBootloader(unsigned char portNumber = 1);
	AMSBootloader::Error connect();
	AMSBootloader::Error disconnect();
	AMSBootloader::Error eraseFlash(unsigned int startAddress, unsigned int length);
	AMSBootloader::Error writeFlash(const char * buffer, unsigned int address, unsigned int size);
	AMSBootloader::Error resetDevice(void);
	int getChunkSize();
	int getBytesPerAddress();
private:
	void encodeMessage(QByteArray* inBuffer, QByteArray* outBuffer, int* outsize);
	void decodeMessage(QByteArray* inbuffer, QByteArray* outbuffer, int* outsize);
};

#endif // PICUARTBOOTLOADER_H