/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 *      PROJECT:   AMSBootloader
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  FirmwareUpgrader class header file
 *
 *  This class is the actual workhorse. It opens the file to be flashed
 *  and sends the particular chunks to the respective bootloader implementation.
 *
 */
#ifndef AMSFIRMWAREUPGRADER_H
#define AMSFIRMWAREUPGRADER_H

#include "AMSBootloader.h"

class AMSFirmwareUpgrader : public QObject
{
    Q_OBJECT
public:
	AMSFirmwareUpgrader(AMSBootloader* bootloader);

    void prepareUpgradeSlot(QString filename, int address); //if upgrade is started via upgradeSlot() this is the way to define the upgrade parameters
    AMSBootloader* bootloader();
    AMSBootloader::Error errorCode();

public slots:
    void upgradeSlot();                                     //this slot can be used for a QThread::
	int doUpgrade(QString filename, int address, bool emitSuccess = true );
	
signals:
    void updateProgress(int progress);                      //signals firmware download progress in %
    void updateFinished(int errorCode);                     //signals end of FW update and delivers return code of update.

private:
    AMSBootloader::Error tryToConnect( );

private:
	AMSBootloader *itsBootloader;

    QString itsFilename;
    int itsAddress;
    AMSBootloader::Error itsError;
};


#endif /* AMSFIRMWAREUPGRADER_H */
