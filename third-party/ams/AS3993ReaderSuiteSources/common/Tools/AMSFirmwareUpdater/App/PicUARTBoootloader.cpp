/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 *      PROJECT:   AMSBootloader
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  Implementation for Microchip's UART bootloader for PIC 24FJ series
 *
 */

#include "stdafx.h"
#include "PicUARTBootloader.h"
#include "AMSBootloader.h"
#include "AMSCommunication.hxx"
#include "PicUARTCommunication.h"

PicUARTBootloader::PicUARTBootloader(unsigned char uartPortNumber)
{
	this->com = new PicUARTCommunication(uartPortNumber);

}

AMSBootloader::Error PicUARTBootloader::connect()
{
	if (com->connect() != AMSCommunication::NoError)
	{
		return ConnectionError;
	}
	return NoError;	
}

AMSBootloader::Error PicUARTBootloader::disconnect()
{
	com->disconnect();
	return NoError;	
}

AMSBootloader::Error PicUARTBootloader::eraseFlash(unsigned int startAddress, unsigned int length)
{
	int outsize;
	QString answer;
	QByteArray* buffer = new QByteArray(5, 0);
	QByteArray* transferbuffer = new QByteArray(15, 0);

	buffer->data()[0] = COMMAND_ERASEPM;
	buffer->data()[1] = (length / ERASE_PAGESIZE) + ((length % ERASE_PAGESIZE) ? 1 : 0);
	buffer->data()[2] = 0xff & startAddress;
	buffer->data()[3] = 0xff & (startAddress >> 8);
	buffer->data()[4] = 0xff & (startAddress >> 16);

	this->encodeMessage(buffer, transferbuffer, &outsize);

#if QT_VERSION < 0x050000
    if (com->sendCommand(QString::fromAscii(transferbuffer->data(), outsize), &answer) != AMSCommunication::NoError)
    {
        return EraseFlashError;
    }
    decodeMessage(&(answer.toAscii()), transferbuffer, &outsize);
#else
    if (com->sendCommand(QString::fromLatin1(transferbuffer->data(), outsize), &answer) != AMSCommunication::NoError)
    {
        return EraseFlashError;
    }
    decodeMessage(&(answer.toLatin1()), transferbuffer, &outsize);
#endif
    	

	if (outsize < 1)
	{
		return EraseFlashError;
	}
	if (transferbuffer->at(0) != COMMAND_ERASEPM)
	{
		return EraseFlashError;
	}
	return NoError;
}

AMSBootloader::Error PicUARTBootloader::writeFlash(const char * buf, unsigned int address, unsigned int size)
{
	int outsize;
	QString answer;
	QByteArray* buffer = new QByteArray(buf, size);
	QByteArray* transferbuffer = new QByteArray((size * 2) + 5, 0);
	
	buffer->prepend(0xff & (address >> 16));
	buffer->prepend(0xff & (address >> 8));
	buffer->prepend(0xff & address);
	buffer->prepend(size / WRITE_PAGESIZE + ((size % WRITE_PAGESIZE) ? 1 : 0));
	buffer->prepend(COMMAND_WRITEPM);
	
	this->encodeMessage(buffer, transferbuffer, &outsize);

#if QT_VERSION < 0x050000
    if (com->sendCommand(QString::fromAscii(transferbuffer->data(), outsize), &answer) != AMSCommunication::NoError)
    {
        return ProgramError;
    }
    decodeMessage(&(answer.toAscii()), transferbuffer, &outsize);
#else
    if (com->sendCommand(QString::fromLatin1(transferbuffer->data(), outsize), &answer) != AMSCommunication::NoError)
    {
        return ProgramError;
    }
    decodeMessage(&(answer.toLatin1()), transferbuffer, &outsize);
#endif
		
    
    if (outsize < 1)
	{
		return ProgramError;
	}
	if (transferbuffer->at(0) != COMMAND_WRITEPM)
	{
		return ProgramError;
	}
	return NoError;
}

int PicUARTBootloader::getChunkSize()
{
	return WRITE_PAGESIZE;
}

AMSBootloader::Error PicUARTBootloader::resetDevice(void)
{
	int outsize;
	QString answer;
	QByteArray* buffer = new QByteArray(1, 0);
	QByteArray* resetbuffer = new QByteArray(2, 0);
	QByteArray* transferbuffer = new QByteArray(5, 0);
	
	buffer->data()[0] = COMMAND_VERIFYOK;

	this->encodeMessage(buffer, transferbuffer, &outsize);

#if QT_VERSION < 0x050000
    if (com->sendCommand(QString::fromAscii(transferbuffer->data(), outsize), &answer) != AMSCommunication::NoError)
    {
        return AppStartError;
    }
    decodeMessage(&(answer.toAscii()), transferbuffer, &outsize);
#else
    if (com->sendCommand(QString::fromLatin1(transferbuffer->data(), outsize), &answer) != AMSCommunication::NoError)
    {
        return AppStartError;
    }
    decodeMessage(&(answer.toLatin1()), transferbuffer, &outsize);
#endif
    	
	if (outsize < 1)
	{
		return AppStartError;
	}
	if (transferbuffer->at(0) != COMMAND_VERIFYOK)
	{
		return AppStartError;
	}

	resetbuffer->data()[0] = COMMAND_RESET;
	resetbuffer->data()[1] = 0x0;

	this->encodeMessage(resetbuffer, transferbuffer, &outsize);

#if QT_VERSION < 0x050000
    if (com->sendCommand(QString::fromAscii(transferbuffer->data(), outsize), &answer) != AMSCommunication::NoError)
    {
        return AppStartError;
    }
#else
    if (com->sendCommand(QString::fromLatin1(transferbuffer->data(), outsize), &answer) != AMSCommunication::NoError)
    {
        return AppStartError;
    }
#endif	

	return NoError;
}

void PicUARTBootloader::encodeMessage(QByteArray* inBuffer, QByteArray* outBuffer, int* outsize)
{
	char *inptr = inBuffer->data();
	char *outptr = outBuffer->data();
	int i = 0, size;
	int checksum = 0;
	unsigned char tmp;

	/* start condition */
	*outptr++ = STX;
	*outptr++ = STX;
	size = 2;

	/* data */
	for (i = 0; i < inBuffer->size(); i++)
	{
		switch (inptr[i])
		{
			case ETX:
			case STX:
			case DLE:
				*outptr++ = DLE;
				size++;
			break;
		}
		*outptr++ = inptr[i];
		size++;
		checksum += inptr[i];
	}

	/* checksum */
	tmp = ((~checksum) + 1) & 0xff;
	switch (tmp)
	{
		case ETX:
		case STX:
		case DLE:
			*outptr++ = DLE;
			size++;
		break;
	}
	*outptr++ = tmp;
	size++;

	/* stop condition */
	*outptr++ = ETX;
	size++;

	*outsize = size;
}

void PicUARTBootloader::decodeMessage(QByteArray* inbuffer, QByteArray* outbuffer, int* outsize)
{
	char *inptr = inbuffer->data();
	char *outptr = outbuffer->data();
	int size = 0;
	bool startcond = false;
	
	for (int i = 0; i < inbuffer->length(); i++)
	{
		/* first, find start condition */
		if ((inptr[i] == STX) && (inptr[i+1] == STX))
		{
			startcond = true;
		}
		if (startcond)
		{
			/* copy data */
			switch (inptr[i])
			{
				case STX:
					break;
				case ETX:
					*outsize = size;
					return;
					break;
				case DLE:
					i++;
				/* fall through */
				default:
					*outptr++ = inptr[i];
					size++;
			}
		}
	}
	*outsize = size;
}

int PicUARTBootloader::getBytesPerAddress()
{
	return 2;
}