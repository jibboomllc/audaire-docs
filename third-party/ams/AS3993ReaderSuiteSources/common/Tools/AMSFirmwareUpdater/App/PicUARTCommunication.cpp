/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSBootloader
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  Reimplementation of UART QT communication class
 */
#include "stdafx.h"
#include "PicUARTCommunication.h"
#include "PicUARTBootloader.h"

PicUARTCommunication::PicUARTCommunication(unsigned char comPort) :
	UARTQtCommunication(comPort)
{
	sProperties comProperties;
	comProperties.port = comPort;
	/* FIXME: encapsulate this */
	comProperties.speed = BAUD115200;
	comProperties.flow = FLOW_OFF;
	comProperties.parity = PAR_NONE;
	comProperties.databits = DATA_8;
	comProperties.stopbits = STOP_1;
	this->setConnectionProperties(&comProperties);
}

AMSCommunication::Error PicUARTCommunication::hwSendCommand(QString command, QString * answer)
{
	UartError uartError = ERR_UART_NO_ERR;
	AMSCommunication::Error err = NoError;
	int i;
    char tmp = 0;

	if (connected)
	{
#if QT_VERSION < 0x050000
        port->write(command.toAscii());
#else
        port->write(command.toLatin1());
#endif
		
		/* in case of reset (COMMAND_RESET) don't wait for an answer */
		if (command.at(2) == COMMAND_RESET)
		{
			return NoError;
		}
		while(port->waitForReadyRead(1000))
		{
			i = port->bytesAvailable();
			if (i == 0)
			{
				break;
			}
			
			port->read(&tmp, 1);
			answer->append(tmp);
			if (tmp == ETX)
			{
				break;
			}
		}
		if (tmp != ETX)
		{
			uartError = ERR_UART_RECEIVE;
		}
	
	}
	else
	{
		uartError = ERR_UART_NOT_CONNECTED;
	}

	if (uartError == ERR_UART_NOT_CONNECTED)
		err = ConnectionError;
	else if (uartError == ERR_UART_NO_ERR)
		err = NoError;
	else
		err = ReadError;

	return err;
}