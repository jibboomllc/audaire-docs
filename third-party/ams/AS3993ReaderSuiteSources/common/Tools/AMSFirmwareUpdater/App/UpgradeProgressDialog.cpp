/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Firmware updater lib
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file UpgradeProgressDialog.cpp
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief  A progress dialog which can be used to show the progress of firmware
 *          upgrade.
 */

#include "UpgradeProgressDialog.hxx"
#include "AMSFirmwareUpgrader.h"

#include <QString>
#include <QWidget>

UpgradeProgressDialog::UpgradeProgressDialog(AMSFirmwareUpgrader * upgrader, QWidget * parent, const QString & labelText) :
     QProgressDialog(labelText, QString(), 0, 100, parent)
{
    Qt::WindowFlags flags;

    setWindowModality(Qt::ApplicationModal);
    //disable close button
    flags = windowFlags() ;
    flags |= Qt::CustomizeWindowHint ;
    flags &= ~Qt::WindowCloseButtonHint ;
    setWindowFlags( flags ) ;
    setValue(0);
    setVisible(true);
    setAutoClose(false);

    connect(upgrader, SIGNAL(updateProgress(int)), this, SLOT(setValue(int)), Qt::QueuedConnection);
    connect(upgrader, SIGNAL(updateFinished(int)), this, SLOT(done(int)), Qt::QueuedConnection);
}

void UpgradeProgressDialog::reject()
{
    //Do not handle reject: prevent closing of dialog with ESC key.
}
