/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 *      PROJECT:   AMSBootloader
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  FirmwareUpgrader class
 *
 *  This class is the actual workhorse. It opens the file to be flashed
 *  and sends the particular chunks to the respective bootloader implementation.
 *
 */
#include "stdafx.h"
#include "AMSFirmwareUpgrader.h"
#include "AMSBootloader.h"
#include <qthread.h>
#include <windows.h>

#define NUM_CONNECTION_RETRIES 15


class SleepHelper : public QThread
{
public:
	static void msleep(unsigned long msecs) {
		QThread::msleep(msecs);
	}
};

AMSFirmwareUpgrader::AMSFirmwareUpgrader(AMSBootloader* bootloader)
{
	this->itsBootloader = bootloader;
    this->itsError = AMSBootloader::NoError;
}

AMSBootloader::Error AMSFirmwareUpgrader::tryToConnect( )
{
	AMSBootloader::Error retCode;
	int numretries = 0;

	/* connect to bootloader of device */
	retCode = itsBootloader->connect();
	while (retCode != AMSBootloader::NoError)
	{
		SleepHelper::msleep(1000);
		if (numretries >= NUM_CONNECTION_RETRIES)
		{
			break;
		}
		numretries++;
		retCode = itsBootloader->connect();
	}
    return retCode;
}

int AMSFirmwareUpgrader::doUpgrade(QString filename, int address, bool emitSuccess )
{
	AMSBootloader::Error retCode;
	int size = 0;
	int accSize = 0;
	char * buffer;

	/* connect to bootloader of device */
    retCode = tryToConnect();

    if (retCode == AMSBootloader::NoError)
	{
        bool isBootloaderIncompatible = false;
		if ( itsBootloader->needBootloaderUpgrade( filename, address, isBootloaderIncompatible ) )
        { /* upgrade to new bootloader */
			QFileInfo file(itsBootloader->bootloaderFileName());
			printf("Info: device needs bootloader update\n");
			if (! file.exists())
			{
				char mfn[255];
				GetModuleFileName(NULL, mfn, 255);
				file.setFile(mfn);
				file.setFile(file.absoluteDir().path() + "/" + itsBootloader->bootloaderFileName());
				printf("Info: %s was not found in local directory, trying %s\n"
					, itsBootloader->bootloaderFileName().toLocal8Bit().constData()
					, file.absoluteFilePath().toLocal8Bit().constData());
			}
            int result = doUpgrade( file.filePath(), itsBootloader->bootloaderStartAddress(), false /* do not emit success */ );
            if ( result != 0 )
            {
                return result; /* bootloader update failed -> bail out */
            }
            retCode = tryToConnect(); /* need to re-connect to bootloader v2.x.y */
        }
        else if ( isBootloaderIncompatible ) 
        { /* return error and bail out */
    		itsBootloader->disconnect();
            itsError = AMSBootloader::VersionError;
            emit updateFinished((int)AMSBootloader::VersionError);
		    return (int)AMSBootloader::VersionError;
        }

		QFile * file = new QFile(filename);
		buffer = (char*)malloc(itsBootloader->getChunkSize());
		if (!buffer)
		{
			printf("Error: Can't alloc buffer!\n");
			itsBootloader->disconnect();
            itsError = AMSBootloader::MemoryError;
            emit updateFinished((int)AMSBootloader::MemoryError);
			return (int)AMSBootloader::MemoryError;
		}
		if (file->open(QIODevice::ReadOnly))
		{
			int counter = 0;
			printf("Erasing flash ... ");
			fflush(stdout);
			if (itsBootloader->eraseFlash(address, file->size()) != AMSBootloader::NoError)
			{
				printf("FAILED!\n");
				free(buffer);
				file->close();
				itsBootloader->disconnect();
                itsError = AMSBootloader::EraseFlashError;
                emit updateFinished((int)AMSBootloader::EraseFlashError);
				return (int)AMSBootloader::EraseFlashError;
			}
			printf("OK!\n");
			fflush(stdout);

            int prog = 0;
            /* wait some time after erase command */
            //SleepHelper::msleep(100);
			/* programming flash */
			printf("Programming flash\n");
			fflush(stdout);
			while (!file->atEnd())
			{
				counter++;
				size = file->read(buffer, itsBootloader->getChunkSize());
                prog += size;
				printf(".");
				fflush(stdout);
				if (counter % 80 == 0)
				{
					printf("\n");
					fflush(stdout);
				}
				retCode = itsBootloader->writeFlash(buffer, address, size);
                emit updateProgress((int)((double)prog * 100.0 / file->size()));

                if (retCode != AMSBootloader::NoError)
				{
					printf("FAILED!\n");
					free(buffer);
					file->close();
					itsBootloader->disconnect();
                    itsError = AMSBootloader::ProgramError;
                    emit updateFinished((int)AMSBootloader::ProgramError);
					return (int)AMSBootloader::ProgramError;
				}
				address += size / itsBootloader->getBytesPerAddress();
				accSize += size;
                /* wait some time before next packet is sent */
                //SleepHelper::msleep(10);

			}
			printf("\nDone! Successfully wrote %ld bytes\n", accSize);
			printf("Starting flashed application ... ");
			fflush(stdout);
			if (itsBootloader->resetDevice() != AMSBootloader::NoError)
			{
				printf("FAILED!\n");
				free(buffer);
				file->close();
				itsBootloader->disconnect();
                itsError = AMSBootloader::AppStartError;
                emit updateFinished((int)AMSBootloader::AppStartError);
				return (int)AMSBootloader::AppStartError;
			}
			free(buffer);
			file->close();
		    itsBootloader->disconnect();
			printf("OK!\n");
		}
		else
		{
			printf("Error: Can't open file %s\n",QFileInfo(*file).absoluteFilePath().toLocal8Bit().constData());
			free(buffer);
			itsBootloader->disconnect();
            itsError = AMSBootloader::FileError;
            emit updateFinished((int)AMSBootloader::FileError);
			return (int)AMSBootloader::FileError;
		}
	}
	else
	{
		printf("Error: Connection to Bootloader could not be established.\n");
        itsError = AMSBootloader::ConnectionError;
        emit updateFinished((int)AMSBootloader::ConnectionError);
		return (int)AMSBootloader::ConnectionError;
	}
    itsError = AMSBootloader::NoError;
    if ( emitSuccess )
    { 
        emit updateFinished((int)AMSBootloader::NoError);
    }
	return itsError;
}

void AMSFirmwareUpgrader::prepareUpgradeSlot( QString filename, int address )
{
    itsFilename = filename;
    itsAddress = address;
}

void AMSFirmwareUpgrader::upgradeSlot()
{
    doUpgrade(itsFilename, itsAddress);
}

AMSBootloader::Error AMSFirmwareUpgrader::errorCode()
{
    return itsError;
}

AMSBootloader* AMSFirmwareUpgrader::bootloader()
{
    return itsBootloader;
}
