/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef DBL_SPINBOX_VALUE_MAPPER_H
#define DBL_SPINBOX_VALUE_MAPPER_H


#include <QObject>

#if QT_VERSION < 0x050000
#include <QtGui/QSpinBox>
#else
#include <QSpinBox>
#endif

#include <QList>
#include "ValueMapper.hxx"
#include "WidgetLinker.hxx"



class DoubleSpinBoxValueMapper : public QObject, public ValueMapper
{
	Q_OBJECT

public:
	DoubleSpinBoxValueMapper(QDoubleSpinBox *widget, WidgetLinker *widgetLinker);
	~DoubleSpinBoxValueMapper(void);
	void updateRanges(QList<valueRangeStruct> valueRanges);

	int currentValue() { return itsCurrentIndex; }
	void setCurrentValue(int value);
	void updateValue(int value);

protected:
	double roundClosest(double value);
	int roundClosestInt(double value);

private:
	bool compare(double in1, double in2, short method, short precision=0);
	bool isValueWithinRange( int value );
	int getNextValidIndex(int oldIndex, bool searchDown, int &rangeIndex);	

	QDoubleSpinBox *itsWidget;
	WidgetLinker *itsWidgetLinker;
	QList<valueRangeStruct> itsValueRanges;
	double itsCurrentValue;
	int itsCurrentRangeIndex;
	int	itsCurrentIndex;	

private slots:
	void onSpinBoxValueChanged(double value);

};

#endif //DBL_SPINBOX_VALUE_MAPPER_H

