/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  AMSDoubleSpinBox class
 *
 *  AMSDoubleSpinBox is an extension of the QDoubleSpinBox class. 
 *  It has an extension which allows a direct connection between 
 *  a register in a chip and the selected item within the box.
 */

#ifndef AMSDOUBLESPINBOX_H
#define AMSDOUBLESPINBOX_H

#include <QtGui>

#if QT_VERSION < 0x050000
#else
#include <QDoubleSpinBox>
#endif

#include "AMSCommunication.hxx"
#include "AMSControl.h"

struct valueRangeStruct
{
	double			fromValue;
	double			toValue;
    unsigned char	fromRegisterValue;
	unsigned char	toRegisterValue;
	double			offset;
	double			stepSize;
	unsigned short	idxOffset;
};

class AMSDoubleSpinBox : public QDoubleSpinBox, public AMSControl
{
	Q_OBJECT

public:
	AMSDoubleSpinBox(QWidget * parent);

    void unlink();
	void link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool verify=true, bool doEmit = true, unsigned char trcLevel=1);
	void link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool verify=true, bool doEmit = true, unsigned char trcLevel=1);
	void addRange(valueRangeStruct newRange);
	void clearRange();
	void forceUpdate(unsigned char reg, unsigned char bitpos, unsigned char mask, unsigned char value);
	int forceGetRegValue();
	int currentIndex() { return itsCurrentIndex; }
	
public slots:
	void update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value);
	void onChange(double value);

protected:
    double roundClosest(double unprocessedValue);
	int roundClosestInt(double unprocessedValue);
	QList<valueRangeStruct> valueRanges;
	void mousePressEvent ( QMouseEvent * event );

private:
	bool compare(double in1, double in2, short method, short precision=0);
    
private:
	uint itsCurrentRangeIdx;
	int	 itsCurrentIndex;
};

#endif /* AMSDOUBLESPINBOX_H */