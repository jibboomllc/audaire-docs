/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Puri
 *
 *  \brief  AMSRadioButton class
 *
 *  AMSRadioButton is an extension of the QRadioButton class. It has an extension
 *  which allows a direct connection between a register/bit in a chip
 *  and the radiobutton.
 */

#ifndef AMSRADIOBUTTON_H
#define AMSRADIOBUTTON_H

#include <QtGui>

#if QT_VERSION < 0x050000
#else
#include <QRadioButton>
#endif

#include "AMSCommunication.hxx"
#include "AMSControl.h"

class AMSRadioButton : public QRadioButton, public AMSControl
{
	Q_OBJECT

public:
	AMSRadioButton(QWidget * parent);

    void unlink();
	void link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, bool invert, bool verify=true, bool doEmit = true, unsigned char trcLevel=1);
	void link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, bool invert, bool verify=true, bool doEmit = true, unsigned char trcLevel=1);

private slots:
	void update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value);
	void onClick(bool checked);
    
private:
	bool linkInvert;
};

#endif /* AMSRADIOBUTTON_H */