/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Dedek
 *  \author F. Lobmaier
 *
 *  \brief  AMSLed class
 *
 *  AMSLed implements LED functionality controlled by a register 
 */

#ifndef AMSLED_H
#define AMSLED_H

#include <QtGui>

#if QT_VERSION < 0x050000
#else
#include <QFrame>
#endif

#include "AMSCommunication.hxx"
#include "AMSControl.h"

enum AMSLedType
{
    RedGreen,
    GrayGreen,
	OffGreen,
    OffRed
};

class AMSLed: public QFrame, public AMSControl
{
    Q_OBJECT
public:
    AMSLed(QWidget *parent = 0, Qt::WindowFlags f = 0);
	
    void unlink();
    void link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool invert=false);
	void link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool invert=false);
	void setInterval(int in) {blinkTimer.setInterval(in);}
    void setLedType(AMSLedType ledType);
	void startStopBlink();
	void ledOff();
	void ledOn();
	void ledOnOff(bool);
	void singleShotStart(bool on, int timeout);
	bool ledIsOn() { return lit; }
	void setLedOnImage(QString str) { ledOnStyleSheet = str; }
	void setLedOffImage(QString str) { ledOffStyleSheet = str; }
	
protected:
	void mouseReleaseEvent(QMouseEvent *event);

signals:
	void clicked(bool click=false);

private slots:
	virtual void update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value);
	void ledTimeout();
public slots:
    void switchLED();
    
private:
	bool lit, useRounded, isActive, inverted;

    int             interval;
	unsigned char   ledRegister; 
    unsigned char   ledMask;
    AMSLedType      itsLedType;
	QTimer          blinkTimer;
	QString         ledOnStyleSheet;
	QString         ledOffStyleSheet;
};

#endif // AMSLED_H