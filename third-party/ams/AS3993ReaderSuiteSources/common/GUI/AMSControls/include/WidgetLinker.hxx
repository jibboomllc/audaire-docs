/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef WIDGET_LINKER_H
#define WIDGET_LINKER_H


#include <QObject>
#include <QtGui>

#if QT_VERSION < 0x050000
#include <QtGui/QSlider>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QComboBox>
#else
#include <QSlider>
#include <QDoubleSpinBox>
#include <QComboBox>
#endif

#include "AMSCommunication.hxx"
#include "AMSControl.h"
#include "WidgetLinker.hxx"
#include "ValueMapper.hxx"
#include "AMSDoubleSpinBox.hxx"


class WidgetLinker : public QObject, public AMSControl
{
	Q_OBJECT

public:
	WidgetLinker(void);
	~WidgetLinker(void);
	
	//Widget links
	void linkTo(QSlider *widget);
	void linkTo(QDoubleSpinBox *widget);
    void linkTo(QComboBox *widget);
	void unlinkFrom(QSlider *widget);	
	void unlinkFrom(QDoubleSpinBox *widget);
    void unlinkFrom(QComboBox *widget);
    ValueMapper* findValueMapperOf(QWidget *widget);

	//Communication links
    void unlink();
	void link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool verify=true, bool doEmit = true, unsigned char trcLevel=1);
	void link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel);

	QList<valueRangeStruct> valueRanges() { return itsValueRanges; }
	void addRange(valueRangeStruct newRange);
	void clearRanges();
	void updateValue(int controlValue, bool writeBack, ValueMapper *source = NULL);	

	
private slots:	
	void update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char regValue); //implementation of virtual function in AMSControl
    
private:
	QList<valueRangeStruct> itsValueRanges;
	QMap<QWidget*, ValueMapper*> itsValueMappers;

    void unlinkFromWidget(QWidget *widget);	
};

#endif //WIDGET_LINKER_H