/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef SLIDER_VALUE_MAPPER_H
#define SLIDER_VALUE_MAPPER_H

#include <QObject>
#include <QTimer>
#include <QList>

#if QT_VERSION < 0x050000
#include <QtGui/QSlider>
#else
#include <QSlider>
#endif

#include "ValueMapper.hxx"
#include "WidgetLinker.hxx"


class SliderValueMapper : public QObject, public ValueMapper
{
	Q_OBJECT

public:
	SliderValueMapper(QSlider *widget, WidgetLinker *widgetLinker);
	~SliderValueMapper(void);

    void setReleaseInterval(int interval);
    void setTrackingInterval(int interval);
    int releaseInterval();
    int trackingInterval();
    void updateValue(int regVal);
	void setAllowContinousValueChange(bool value) { itsAllowContinousValueChange = value; }
	bool allowContinousValueChange() { return itsAllowContinousValueChange; }
	void updateRanges(QList<valueRangeStruct> valueRanges);
	
private slots:
	void on_slider_valueChanged(int value);
	void on_slider_sliderPressed();
	void on_slider_sliderReleased();
	void on_releaseTimer_elapsed();
    void on_trackingTimer_elapsed();

private:
    static const int ERR_Conv = -1001; // error value when value conversion cannot be done
	void raiseValueChanging();
	void raiseValueChanged();
    int regValToSliderVal(int regVal);
    int sliderValToRegVal(int sliderVal);

	QSlider *itsWidget;
	WidgetLinker *itsWidgetLinker;
	QList<valueRangeStruct> itsValueRanges;
	QTimer *itsReleaseTimer; //timer to wait for a certain time after the last change, then raise a value changed. (to hide all changes while sliding)
    QTimer *itsTrackingTimer; //timer that elapses while tracking to get values while tracking. (to notify changes while sliding)
	
    bool itsAllowContinousValueChange;
    
	bool itsIsMouseDown;    
    bool itsHasValueChanged;
};

#endif //SLIDER_VALUE_MAPPER_H

