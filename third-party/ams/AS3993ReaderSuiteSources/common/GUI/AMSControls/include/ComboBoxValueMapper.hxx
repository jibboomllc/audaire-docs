/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef COMBOBOX_VALUE_MAPPER_H
#define COMBOBOX_VALUE_MAPPER_H

#include <QObject>
#include <QTimer>
#include <QList>

#if QT_VERSION < 0x050000
#include <QtGui/QComboBox>
#else
#include <QComboBox>
#endif

#include "ValueMapper.hxx"
#include "WidgetLinker.hxx"


class ComboBoxValueMapper : public QObject, public ValueMapper
{
	Q_OBJECT

public:
	ComboBoxValueMapper(QComboBox *widget, WidgetLinker *widgetLinker);
	~ComboBoxValueMapper(void);

	void updateValue(int regVal);
	void updateRanges(QList<valueRangeStruct> valueRanges);
	
private slots:
	void on_comboBox_currentIndexChanged(int value);
	
private:
    static const int ERR_Conv = -1001; // error value when value conversion cannot be done
	void raiseValueChanging();
	void raiseValueChanged();
    bool isValueWithinRange(int value);
    int regValToComboBoxVal(int regVal);
    int comboBoxValToRegVal(int comboBoxVal);
	
	QComboBox *itsWidget;
	WidgetLinker *itsWidgetLinker;
	QList<valueRangeStruct> itsValueRanges;
    bool itsSuspendValueChange;
};

#endif //COMBOBOX_VALUE_MAPPER_H

