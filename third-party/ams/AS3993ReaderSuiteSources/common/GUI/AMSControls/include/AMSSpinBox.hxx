/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Puri-Jobi
 *
 *  \brief  AMSSpinBox class
 *
 *  AMSSpinBox is an extension of the QSpinBox class. 
 *  It has an extension which allows a direct connection between 
 *  a register in a chip and the selected item within the box.
 */

#ifndef AMSSPINBOX_H
#define AMSSPINBOX_H

#include <QtGui>

#if QT_VERSION < 0x050000
#else
#include <QSpinBox>
#endif

#include "AMSCommunication.hxx"
#include "AMSControl.h"

class AMSSpinBox : public QSpinBox, public AMSControl
{
	Q_OBJECT

public:
	AMSSpinBox(QWidget * parent);

	enum Representation {Decimal, Hexdecimal, Binary};

    void unlink();
	void link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool verify=true, bool doEmit = true, unsigned char trcLevel=1);
	void link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool verify=true, bool doEmit = true, unsigned char trcLevel=1);
	void setNumberOfDigits(int digits);
	void setRepresentation(Representation mode);

signals:
    void arrowPressed();

protected:
	QValidator::State validate(QString &input, int &pos) const;
    int valueFromText(const QString &text) const;
    QString textFromValue(int val) const;

private slots:
	void update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value);
	void onChange(int value);
    void mousePressEvent ( QMouseEvent * event );

private:
	qint8			numberOfDigits;
	quint8			base;
	Representation	mode; 
};

#endif /* AMSSPINBOX_H */