/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  AMSControl class
 *
 *  AMSControl contains common logic of all AMSControls.
 */

#ifndef AMSCONTROL_H
#define AMSCONTROL_H

#include "AMSCommunication.hxx"

class AMSControl
{

public:
	AMSControl();
	void setControlRegisterAddress(unsigned char reg, unsigned char mask);
    bool isLinked();
    AMSCommunication::Error updateControl();

protected:
	virtual void update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value) = 0;	
    AMSCommunication* com; /*!< holds the specific ams communication class, e.g. USBcom */
	unsigned char linkRegister; /*!< the address of the register linked with the control */
	unsigned char linkSubregister; /*!< the address of the subregister linked with the control */
	unsigned char linkMask; /*!< mask within the address (without bitshift offset) */
	unsigned char linkPos; /*!< bitshift position, i.e. actual mask is linkMask << linkPos */
	unsigned char linkControlRegister; /*!< the control register to use */
	unsigned char linkControlMask; /*!< mask within the control register */
	bool activated; /*!< boolean to temporarely disable the update slot */
	bool filterEvent; /*!< boolean to temporarely disable the control's action slot, e.g. click */
    bool isLinkedToCom; /*!<boolean to check if control is linked to communication */
	bool useSubregister; /*!< indicates whether the control is connected to a subregister or not */
	bool doEmit;
	bool doVerify;
	unsigned char trcLevel;
};

#endif /* AMSCONTROL_H */