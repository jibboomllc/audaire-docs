/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef VALUE_MAPPER_FACTORY_H
#define VALUE_MAPPER_FACTORY_H

#if QT_VERSION < 0x050000
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QSlider>
#include <QtGui/QComboBox>
#else
#include <QDoubleSpinBox>
#include <QSlider>
#include <QComboBox>
#endif

#include "ValueMapper.hxx"
#include "WidgetLinker.hxx"


class ValueMapperFactory
{
public:
	ValueMapperFactory(void);
	~ValueMapperFactory(void);

	static ValueMapper* create(QDoubleSpinBox *widget, WidgetLinker *widgetLinker );
	static ValueMapper* create(QSlider *widget, WidgetLinker *widgetLinker );
    static ValueMapper* create(QComboBox *widget, WidgetLinker *widgetLinker );
};

#endif //VALUE_MAPPER_FACTORY_H