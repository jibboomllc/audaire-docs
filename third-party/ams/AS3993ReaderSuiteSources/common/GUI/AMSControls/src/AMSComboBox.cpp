/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Puri
 *
 *  \brief  AMSComboBox class
 *
 *  AMSComboBox is an extension of the QComboBox class. It has an extension
 *  which allows a direct connection between a register in a chip
 *  and the index of the ComboBox.
 */

#include "AMSComboBox.hxx"

AMSComboBox::AMSComboBox(QWidget * parent) : QComboBox(parent)
{
}

void AMSComboBox::unlink()
{
    if(!isLinkedToCom)
        return;

    disconnect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(onChange(int)));
    disconnect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));

    this->linkRegister = 0x00;
    this->linkPos = 0;
    this->useSubregister = false;
    this->linkMask = 0x00;
    this->com = NULL;
    this->doVerify = false;
    this->doEmit = false;
    this->trcLevel = 0;    
    AMSControl::activated = false;
    isLinkedToCom = false;
}

void AMSComboBox::link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel)
{
	this->linkRegister = reg;
	this->linkPos = bitpos;
	this->useSubregister = false;
	this->linkMask = mask;
	this->com = com;
	this->doVerify = verify;
	this->doEmit = doEmit;
	this->trcLevel = trcLevel;
	connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(onChange(int)));
	connect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));
	AMSControl::activated = true;
    isLinkedToCom = true;
}

void AMSComboBox::link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel)
{
	link(com, reg, bitpos, mask);
	this->linkSubregister = subreg;
	this->useSubregister = true;
}

void AMSComboBox::update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value)
{
	if (AMSControl::activated)
	{
		if(!useSubregister && reg == linkRegister)
		{
			filterEvent = true;
			this->setCurrentIndex((value & (linkMask << linkPos)) >> linkPos);
			filterEvent = false;
		}
		else if(useSubregister && isSubreg && reg == linkRegister && subreg == linkSubregister)
		{
			filterEvent = true;
			this->setCurrentIndex((value & (linkMask << linkPos)) >> linkPos);
			filterEvent = false;
		}
	}
}

void AMSComboBox::onChange(int index)
{
	if (NULL != com && !filterEvent)
	{
		AMSControl::activated = false;
		
        if(linkControlRegister != 0xff)
			this->com->setControlRegisterAddress(linkControlRegister, linkControlMask);
		
        if (useSubregister)
		{
			this->com->modifySubRegister(linkRegister, linkMask << linkPos, index << linkPos, linkSubregister);
		}
		else
		{
			this->com->modifyRegister(linkRegister, linkMask << linkPos, index << linkPos,this->doVerify, this->doEmit, this->trcLevel);
		}

		AMSControl::activated = true;
	}
}
