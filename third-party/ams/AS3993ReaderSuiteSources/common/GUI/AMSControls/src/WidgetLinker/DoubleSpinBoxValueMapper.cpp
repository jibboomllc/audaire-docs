/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */


#include "DoubleSpinBoxValueMapper.hxx"

DoubleSpinBoxValueMapper::DoubleSpinBoxValueMapper(QDoubleSpinBox *widget, WidgetLinker *widgetLinker)
{
	itsWidget = widget;
	itsWidgetLinker = widgetLinker;
	itsCurrentValue = widget->value();
	itsCurrentRangeIndex = 0;
	itsCurrentIndex = 0;
	
	connect(widget, SIGNAL(valueChanged(double)), this, SLOT(onSpinBoxValueChanged(double)));	
	updateRanges(widgetLinker->valueRanges());
}


DoubleSpinBoxValueMapper::~DoubleSpinBoxValueMapper(void)
{
	
}

double DoubleSpinBoxValueMapper::roundClosest(double value)
{
	double higherValue, lowerValue;
	double deltaHigherValue, deltaLowerValue;
	double processedValue;
	double mult = pow(10.0, itsWidget->decimals());


	higherValue = ceil(value * mult);
	lowerValue = floor(value * mult);

	deltaHigherValue = higherValue - value * mult;
	deltaLowerValue = value * mult - lowerValue;

	if(deltaLowerValue < deltaHigherValue)
	{
		processedValue = lowerValue / mult;
	}
	else   // by this we round up on equal deltas
	{
		processedValue = higherValue / mult;
	}

	return processedValue;
}

int DoubleSpinBoxValueMapper::roundClosestInt(double unprocessedValue)
{	
	double higherValue, lowerValue;
	double deltaHigherValue, deltaLowerValue;
	int processedValue;

	higherValue = ceil(unprocessedValue);
	lowerValue = floor(unprocessedValue);

	deltaHigherValue = higherValue - unprocessedValue;
	deltaLowerValue = unprocessedValue - lowerValue;

	if(deltaLowerValue < deltaHigherValue)
	{
		processedValue = lowerValue;
	}
	else   // by this we round up on equal deltas
	{
		processedValue = higherValue;
	}

	return processedValue;	
}

bool DoubleSpinBoxValueMapper::isValueWithinRange( int value )
{
	foreach(valueRangeStruct range, itsValueRanges)
	{
		if(value >= range.fromRegisterValue && value <= range.toRegisterValue)
			return true;
	}

	return false;
}

int DoubleSpinBoxValueMapper::getNextValidIndex( int oldIndex, bool searchDown, int &rangeIndex )
{
	if(itsValueRanges.count() <= 0)
	{
		rangeIndex = -1;
		return searchDown ? oldIndex - 1 : oldIndex + 1;
	}

	if(searchDown)
	{
		for(int i = itsValueRanges.count() - 1; i >= 0; i--)
		{
			valueRangeStruct range = itsValueRanges[i];
			rangeIndex = i;

			if(oldIndex > range.toRegisterValue)
				return range.toRegisterValue;

			if(oldIndex <= range.toRegisterValue && oldIndex > range.fromRegisterValue)
				return oldIndex - 1;
		}

		rangeIndex = 0;
		return (itsValueRanges[0]).fromRegisterValue; //return first value
	}
	else
	{
		for(int i = 0; i < itsValueRanges.count(); i++)
		{
			valueRangeStruct range = itsValueRanges[i];
			rangeIndex = i;

			if(oldIndex < range.fromRegisterValue)
				return range.fromRegisterValue;

			if(oldIndex >= range.fromRegisterValue && oldIndex < range.toRegisterValue)
				return oldIndex + 1;
		}

		rangeIndex = itsValueRanges.count() - 1;
		return (itsValueRanges[itsValueRanges.count() - 1]).toRegisterValue; // return last value
	}	
}


void DoubleSpinBoxValueMapper::updateRanges(QList<valueRangeStruct> valueRanges)
{
	itsValueRanges = valueRanges;
	//todo: re-initialize spin box
}


void DoubleSpinBoxValueMapper::updateValue(int value)
{
	unsigned char valueReduction = 0;
	unsigned char realValue = value; //((value & (linkMask << linkPos)) >> linkPos);
	double offset = 0;
		
	if (1) //activated)
	{		
		double newValue=0.0;
		itsCurrentRangeIndex = 0;
		if(itsValueRanges.size()>0)
		{
			for(int i = itsValueRanges.size()-1;i>=0;i--)
			{
				if(realValue >= itsValueRanges[i].idxOffset)
				{
					itsCurrentRangeIndex = i;
					itsWidget->setSingleStep(itsValueRanges[itsCurrentRangeIndex].stepSize);
					break;
				}
			}
			newValue = roundClosest(itsValueRanges[itsCurrentRangeIndex].fromValue + (realValue - itsValueRanges[itsCurrentRangeIndex].idxOffset)*itsWidget->singleStep());
		}
		else
		{
			newValue = itsWidget->minimum() + realValue * itsWidget->singleStep();
		}

		//filterEvent = true;		
		itsWidget->blockSignals(true);
		itsCurrentValue = newValue;
		itsCurrentIndex = value;
		itsWidget->setValue(newValue);
		itsWidget->blockSignals(false);
		//filterEvent = false;	
	}
}

void DoubleSpinBoxValueMapper::onSpinBoxValueChanged(double value)
{
	bool filterEvent = false;
	bool isManuallyEntered = false;
	bool roundUp;

	if(itsValueRanges.size() > 0)
	{
		roundUp = value > itsCurrentValue;

		if(abs(value - itsCurrentValue) > itsValueRanges[itsCurrentRangeIndex].stepSize)
			isManuallyEntered = true;

		for(int i = itsValueRanges.size() - 1; i >= 0; i--)
		{
			if(value >= itsValueRanges[i].fromValue)
			{
				itsCurrentRangeIndex = i;
				itsWidget->setSingleStep(itsValueRanges[itsCurrentRangeIndex].stepSize);					
				break;
			}
		}

		if(isManuallyEntered)
		{
			//calculation if value is entered manually
			itsCurrentIndex = itsValueRanges[itsCurrentRangeIndex].idxOffset + roundClosestInt((value - itsValueRanges[itsCurrentRangeIndex].fromValue) / itsWidget->singleStep());

			if(!isValueWithinRange(itsCurrentIndex))
			{					
				int nextUpper = getNextValidIndex(itsCurrentIndex, false, itsCurrentRangeIndex);
				int nextLower = getNextValidIndex(itsCurrentIndex, true, itsCurrentRangeIndex);

				if((nextUpper - itsCurrentIndex) < (itsCurrentIndex - nextLower))
					itsCurrentIndex = nextUpper;
				else
					itsCurrentIndex = nextLower;
			}
		}
		else
		{
			if(roundUp)
			{
				itsCurrentIndex += 1; //itsValueRanges[itsCurrentRangeIndex].idxOffset + ceil((value - itsValueRanges[itsCurrentRangeIndex].fromValue) / itsWidget->singleStep());
				if(!isValueWithinRange(itsCurrentIndex))
					itsCurrentIndex = getNextValidIndex(itsCurrentIndex, false, itsCurrentRangeIndex);
			}
			else
			{
				itsCurrentIndex -= 1; //itsValueRanges[itsCurrentRangeIndex].idxOffset + floor((value - itsValueRanges[itsCurrentRangeIndex].fromValue) / itsWidget->singleStep());
				if(!isValueWithinRange(itsCurrentIndex))
					itsCurrentIndex = getNextValidIndex(itsCurrentIndex, true, itsCurrentRangeIndex);
			}
		}

	}
	else
	{
		itsCurrentIndex = roundClosestInt((value - itsWidget->minimum()) / itsWidget->singleStep());
	}

	if(itsValueRanges.size() > 0)
	{
		value = roundClosest(itsValueRanges[itsCurrentRangeIndex].fromValue + (itsCurrentIndex - itsValueRanges[itsCurrentRangeIndex].idxOffset) * itsWidget->singleStep());
		filterEvent = true;
		itsWidget->blockSignals(true);
		itsCurrentValue = value;
		itsWidget->setValue(value);
		itsWidget->blockSignals(false);
		filterEvent = false;
	}

	itsWidgetLinker->updateValue(itsCurrentIndex, true, this);	
}


