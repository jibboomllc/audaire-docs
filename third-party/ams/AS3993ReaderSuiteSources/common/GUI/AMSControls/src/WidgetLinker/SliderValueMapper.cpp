/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "SliderValueMapper.hxx"


SliderValueMapper::SliderValueMapper( QSlider *widget, WidgetLinker *widgetLinker ) :   itsHasValueChanged(false),
                                                                                        itsIsMouseDown(false),
                                                                                        itsAllowContinousValueChange(true)
{
	itsWidget = widget;
	itsWidgetLinker = widgetLinker;
    	
    itsReleaseTimer = new QTimer(this);
	itsReleaseTimer->setInterval(250);
	connect(itsReleaseTimer, SIGNAL(timeout()), this, SLOT(on_releaseTimer_elapsed()));

    itsTrackingTimer = new QTimer(this);
    itsTrackingTimer->setInterval(200);
    connect(itsTrackingTimer, SIGNAL(timeout()), this, SLOT(on_trackingTimer_elapsed()));

	connect(widget, SIGNAL(valueChanged(int)), this, SLOT(on_slider_valueChanged(int)));
	connect(widget, SIGNAL(sliderPressed()), this, SLOT(on_slider_sliderPressed()));
	connect(widget, SIGNAL(sliderReleased()), this, SLOT(on_slider_sliderReleased()));
}


SliderValueMapper::~SliderValueMapper(void)
{
	delete itsReleaseTimer;
}


void SliderValueMapper::setReleaseInterval(int interval)
{
    itsReleaseTimer->setInterval(interval);
}

void SliderValueMapper::setTrackingInterval(int interval)
{
    itsTrackingTimer->setInterval(interval);
}

int SliderValueMapper::releaseInterval()
{
    return itsReleaseTimer->interval();
}

int SliderValueMapper::trackingInterval()
{
    return itsTrackingTimer->interval();
}


void SliderValueMapper::updateValue( int regVal )
{
    unsigned char sliderVal = 0;

	if(itsIsMouseDown) //do no update if mouse is pressed
		return;

    sliderVal = regValToSliderVal(regVal);
    
    if (sliderVal != ERR_Conv)
    {
	    itsWidget->blockSignals(true);
        itsWidget->setValue(sliderVal);
	    itsWidget->blockSignals(false);
    }
}

void SliderValueMapper::on_slider_valueChanged(int value)
{
	if(itsReleaseTimer->isActive())
		itsReleaseTimer->stop();

	if(!itsIsMouseDown) //do only if mousewheel is used and continous value change is enabled
		itsReleaseTimer->start();
	
    if(itsAllowContinousValueChange && (itsTrackingTimer->isActive() == false))
        itsTrackingTimer->start();

    itsHasValueChanged = true;

	raiseValueChanging();
}

void SliderValueMapper::on_slider_sliderPressed()
{
	itsIsMouseDown = true;
	itsReleaseTimer->stop();

    if(itsAllowContinousValueChange)
        itsTrackingTimer->start();
}

void SliderValueMapper::on_slider_sliderReleased()
{
	itsIsMouseDown = false;
    
    if(itsTrackingTimer->isActive())
        itsTrackingTimer->stop();

	raiseValueChanging();
	raiseValueChanged();
}

void SliderValueMapper::on_releaseTimer_elapsed()
{
	itsReleaseTimer->stop();
	    
	raiseValueChanging();

    if(itsHasValueChanged)
	raiseValueChanged();
}

void SliderValueMapper::on_trackingTimer_elapsed()
{
    if(/*itsIsMouseDown && */itsHasValueChanged)
        raiseValueChanged();
}

void SliderValueMapper::raiseValueChanging()
{
    int regVal;
	int value = itsWidget->value();
		
    regVal = sliderValToRegVal(value);
	if (regVal != ERR_Conv)
        itsWidgetLinker->updateValue(regVal, false, this);
}

void SliderValueMapper::raiseValueChanged()
{
    int regVal;
    itsHasValueChanged = false;
	int value = itsWidget->value();
    	
    regVal = sliderValToRegVal(value);
    if (regVal != ERR_Conv)
		itsWidgetLinker->updateValue(regVal, true, this);
}


void SliderValueMapper::updateRanges(QList<valueRangeStruct> valueRanges)
{
	itsValueRanges = valueRanges;

	unsigned char minRegValue = 0xFF;
	unsigned char maxRegValue = 0x00;
    unsigned char sliderMaxPos = 0;

	foreach(valueRangeStruct range, itsValueRanges)
	{
        if (range.fromRegisterValue <= range.toRegisterValue)
        {
            minRegValue = std::min(minRegValue, range.fromRegisterValue);
            maxRegValue = std::max(maxRegValue, range.toRegisterValue);
        }
        else
        {
            minRegValue = std::min(minRegValue, range.toRegisterValue);
            maxRegValue = std::max(maxRegValue, range.fromRegisterValue);
        }
	}

    sliderMaxPos = maxRegValue - minRegValue;
    
	//set minimum and maximum value, minimum is fixed to 0
	itsWidget->blockSignals(true);
    itsWidget->setMinimum(0);
	itsWidget->setMaximum(sliderMaxPos);
    itsWidget->blockSignals(false);
}

int SliderValueMapper::regValToSliderVal( int regVal )
{
    unsigned int pos = 0;
    //find range with regVal
    foreach(valueRangeStruct range, itsValueRanges)
    {
        unsigned int minRegValue = qMin(range.fromRegisterValue, range.toRegisterValue);
        unsigned int maxRegValue = qMax(range.fromRegisterValue, range.toRegisterValue);

        pos = minRegValue;

        if (range.fromRegisterValue <= range.toRegisterValue)
        {
            if ((regVal >= range.fromRegisterValue) && (regVal <= range.toRegisterValue))
            {
                return (pos + (regVal - range.fromRegisterValue));
            }
            pos += (range.toRegisterValue - range.fromRegisterValue + 1);
        }
        else
        {
            if ((regVal >= range.toRegisterValue) && (regVal <= range.fromRegisterValue))
            {
                return (pos + (range.fromRegisterValue - regVal));
            }
            pos += (range.fromRegisterValue - range.toRegisterValue + 1);
        }        
    }
    return ERR_Conv;
}

int SliderValueMapper::sliderValToRegVal( int sliderVal )
{
    int rangesteps = 0;
    int pos = 0;
    //find range with regVal
    foreach(valueRangeStruct range, itsValueRanges)
    {
        int minRegValue = qMin(range.fromRegisterValue, range.toRegisterValue);
        int maxRegValue = qMax(range.fromRegisterValue, range.toRegisterValue);
                
        rangesteps = maxRegValue - minRegValue + 1;
        pos = minRegValue;

        if (sliderVal < (pos + rangesteps) && sliderVal >= minRegValue)
        {
            if (range.fromRegisterValue <= range.toRegisterValue)
            {
                return (range.fromRegisterValue + (sliderVal - pos));
            }
            else
            {
                return (range.fromRegisterValue - (sliderVal - pos));
            }
        }        
    }

    return ERR_Conv;
}
