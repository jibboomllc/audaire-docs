/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "ComboBoxValueMapper.hxx"

ComboBoxValueMapper::ComboBoxValueMapper( QComboBox *widget, WidgetLinker *widgetLinker )
{
    itsSuspendValueChange = false;
	itsWidget = widget;
	itsWidgetLinker = widgetLinker;
    
	connect(widget, SIGNAL(currentIndexChanged(int)), this, SLOT(on_comboBox_currentIndexChanged(int)));
}


ComboBoxValueMapper::~ComboBoxValueMapper(void)
{
}

void ComboBoxValueMapper::updateValue( int regVal )
{
    unsigned int comboBoxVal = 0;
    comboBoxVal = regValToComboBoxVal(regVal);
    if (comboBoxVal != ERR_Conv)
    {
        itsWidget->blockSignals(true);
        itsWidget->setCurrentIndex(comboBoxVal);
        itsWidget->blockSignals(false);
    }
}

void ComboBoxValueMapper::on_comboBox_currentIndexChanged(int value)
{
	raiseValueChanging();	
    raiseValueChanged();
}

void ComboBoxValueMapper::raiseValueChanging()
{
    int regVal;
    if(itsSuspendValueChange)
        return;

	int value = itsWidget->currentIndex();
	
	//if(isValueWithinRange(value))
    regVal = comboBoxValToRegVal(value);
    if (regVal != ERR_Conv)
	    itsWidgetLinker->updateValue(regVal, false, this);
}

void ComboBoxValueMapper::raiseValueChanged()
{
    int regVal;
    if(itsSuspendValueChange)
        return;

	int value = itsWidget->currentIndex();

	//if(isValueWithinRange(value))
    regVal = comboBoxValToRegVal(value);
    if (regVal != ERR_Conv)
	    itsWidgetLinker->updateValue(regVal, true, this);
}


void ComboBoxValueMapper::updateRanges(QList<valueRangeStruct> valueRanges)
{
    itsSuspendValueChange = true;

	itsValueRanges = valueRanges;
    
    QList<QString> itemNames;

	foreach(valueRangeStruct range, itsValueRanges)
	{
        double decValue = range.fromValue;

        if(range.fromRegisterValue == range.toRegisterValue)
        {
            //just add one entry
            itemNames.append(QString("%1").arg(decValue));
            continue;
        }

        for(int i = 0; i <= ((range.toValue - range.fromValue)/range.stepSize); i++)
        {
            itemNames.append(QString("%1").arg(decValue));
            decValue += range.stepSize;
        }
	}

    itsWidget->blockSignals(true);
    itsWidget->clear();
	itsWidget->addItems(itemNames);
    itsWidget->blockSignals(false);
    
    itsSuspendValueChange = false;
}

bool ComboBoxValueMapper::isValueWithinRange( int value )
{
	foreach(valueRangeStruct range, itsValueRanges)
	{
		if(value >= range.fromRegisterValue && value <= range.toRegisterValue)
			return true;
	}

	return false;
}

int ComboBoxValueMapper::regValToComboBoxVal( int regVal )
{
    unsigned int pos = 0;
    //find range with regVal
    foreach(valueRangeStruct range, itsValueRanges)
    {
        if (range.fromRegisterValue <= range.toRegisterValue)
        {
            if ((regVal >= range.fromRegisterValue) && (regVal <= range.toRegisterValue))
            {
                return (pos + (regVal - range.fromRegisterValue));
            }
            pos += (range.toRegisterValue - range.fromRegisterValue + 1);
        }
        else
        {
            if ((regVal >= range.toRegisterValue) && (regVal <= range.fromRegisterValue))
            {
                return (pos + (range.fromRegisterValue - regVal));
            }
            pos += (range.fromRegisterValue - range.toRegisterValue + 1);
        }        
    }
    return ERR_Conv;
}

int ComboBoxValueMapper::comboBoxValToRegVal( int comboBoxVal )
{
    int rangesteps = 0;
    int pos = 0;
    //find range with regVal
    foreach(valueRangeStruct range, itsValueRanges)
    {
        rangesteps = (range.toValue - range.fromValue) / range.stepSize + 1;

        if (comboBoxVal < (pos + rangesteps))
        {
            if (range.fromRegisterValue <= range.toRegisterValue)
                return (range.fromRegisterValue + (comboBoxVal - pos));
            else
                return (range.fromRegisterValue - (comboBoxVal - pos));
        }
        else
        {
            pos += rangesteps;
        }
    }
    return ERR_Conv;
}
