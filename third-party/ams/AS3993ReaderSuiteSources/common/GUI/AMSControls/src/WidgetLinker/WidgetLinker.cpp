/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "WidgetLinker.hxx"
#include "DoubleSpinBoxValueMapper.hxx"
#include "ValueMapperFactory.hxx"


#pragma region //Construction
WidgetLinker::WidgetLinker(void) : AMSControl()
{
}


WidgetLinker::~WidgetLinker(void)
{
}
#pragma endregion

#pragma region //Widget_Links
void WidgetLinker::linkTo(QSlider *widget) 
{
    if(itsValueMappers.contains(widget))
        unlinkFrom(widget);

	ValueMapper *valueMapper = ValueMapperFactory::create(widget, this);
	valueMapper->updateRanges(itsValueRanges);
	itsValueMappers.insert(widget, valueMapper);
}

void WidgetLinker::linkTo(QDoubleSpinBox *widget)
{
    if(itsValueMappers.contains(widget))
        unlinkFrom(widget);

	ValueMapper *valueMapper = ValueMapperFactory::create(widget, this);
	valueMapper->updateRanges(itsValueRanges);
	itsValueMappers.insert(widget, valueMapper);
}

void WidgetLinker::linkTo(QComboBox *widget)
{
    if(itsValueMappers.contains(widget))
        unlinkFrom(widget);

    ValueMapper *valueMapper = ValueMapperFactory::create(widget, this);
    valueMapper->updateRanges(itsValueRanges);
    itsValueMappers.insert(widget, valueMapper);
}

void WidgetLinker::unlinkFromWidget(QWidget *widget)
{
    if(itsValueMappers.contains(widget))
    {
        ValueMapper *valueMapper = itsValueMappers.value(widget);

        itsValueMappers.remove(widget);
        delete valueMapper;
    }    
}

void WidgetLinker::unlinkFrom(QSlider *widget)
{ 
    unlinkFromWidget(widget);
}

void WidgetLinker::unlinkFrom(QDoubleSpinBox *widget)
{	
    unlinkFromWidget(widget);
}

void WidgetLinker::unlinkFrom(QComboBox *widget)
{	
    unlinkFromWidget(widget);
}
#pragma endregion

#pragma region //Communication_Links

void WidgetLinker::unlink()
{
    if(!isLinkedToCom)
        return;

    disconnect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));

    this->linkRegister = 0x00;
    this->linkPos = 0;
    this->useSubregister = false;
    this->linkMask = 0x00;	        
    this->com = NULL;
    activated = false;
    isLinkedToCom = false;
}

void WidgetLinker::link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel)
{
	this->linkRegister = reg;
	this->linkPos = bitpos;
	this->useSubregister = false;
	this->linkMask = mask;	
	this->com = com;
	connect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));
	activated = true;
    isLinkedToCom = true;
}


void WidgetLinker::link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel)
{
	link(com, reg, bitpos, mask);
	this->linkSubregister = subreg;
	this->useSubregister = true;
}

#pragma endregion


ValueMapper* WidgetLinker::findValueMapperOf( QWidget *widget )
{
    if(itsValueMappers.contains(widget))
        return itsValueMappers[widget];

    return NULL;
}

#pragma region //Internal_Value_Handling

void WidgetLinker::addRange(valueRangeStruct newRange)
{
	bool added = false;
	for(int i=0; i < itsValueRanges.size(); i++)
	{
		if(newRange.fromValue<itsValueRanges[i].fromValue)
		{
			itsValueRanges.insert(i,newRange);
			added = true;
			break;
		}
	}
	
    if(!added)
	{
		itsValueRanges.append(newRange);
	}

	foreach(ValueMapper *mapper, itsValueMappers)
	{
		mapper->updateRanges(itsValueRanges);
	}
}


void WidgetLinker::clearRanges()
{
	itsValueRanges.clear();

	foreach(ValueMapper *mapper, itsValueMappers)
	{
		mapper->updateRanges(itsValueRanges);
	}
}


void WidgetLinker::updateValue(int controlValue, bool writeBack, ValueMapper *source)
{
	foreach(ValueMapper *valueMapper, itsValueMappers)
	{
		if(valueMapper != source)
		{
			valueMapper->updateValue(controlValue);
		}
	}

	if(writeBack)
	{
		if(this->com != NULL)
		{
			activated = false;
			if(linkControlRegister != 0xff)
				this->com->setControlRegisterAddress(linkControlRegister, linkControlMask);

			if (useSubregister)
			{
				this->com->modifySubRegister(linkRegister, linkMask << linkPos, (0xff & controlValue) << linkPos, linkSubregister);
			}
			else
			{
				this->com->modifyRegister(linkRegister, linkMask << linkPos, (0xff & controlValue) << linkPos);
			}
			activated = true;
		}
	}
}

#pragma endregion


#pragma region //External_Value_Handling
//implementation of virtual function in AMSControl, so don't rename
void WidgetLinker::update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char regValue)
{
	if(activated)
	{
        unsigned char controlValue = ((regValue & (linkMask << linkPos)) >> linkPos);

		if((reg == linkRegister && !isSubreg) || (isSubreg && reg == linkRegister && subreg == linkSubregister))
		{
			filterEvent = true;
			updateValue(controlValue, false, NULL); //update comes from outside, so there is no source widget
			filterEvent = false;
		}
	}	
}

#pragma endregion

