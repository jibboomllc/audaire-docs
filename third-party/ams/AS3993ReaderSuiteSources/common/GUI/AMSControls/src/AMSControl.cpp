/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  AMSControl class
 *
 *  AMSControl contains common logic of all AMSControls.
 */
#include "AMSControl.h"

AMSControl::AMSControl()
{
	activated = false;
	filterEvent = false;
    isLinkedToCom = false;
	linkControlRegister = 0xff; //invalid as default
	linkControlMask = 0xff;
	doEmit = true;
	doVerify = true;
	trcLevel = 1;
}

AMSCommunication::Error AMSControl::updateControl()
{
    if(!isLinkedToCom)
    {
        qDebug() << "AMSComboBox is not linked to com";
        return AMSCommunication::ReadError;
    }

    if(this->com == NULL)
    {
        qDebug() << "no communication available";
        return AMSCommunication::ReadError;
    }


    unsigned char registerValue;
    AMSCommunication::Error comErr = AMSCommunication::NoError;


    Q_ASSERT(useSubregister == false); //Subregister handling not yet implemented.

    comErr = com->readRegister(linkRegister, &registerValue, false);
    if(comErr != AMSCommunication::NoError)
        return comErr;

    update(linkRegister, 0x00, useSubregister, registerValue);

    return comErr;
}

void AMSControl::setControlRegisterAddress(unsigned char reg, unsigned char mask)
{
	linkControlRegister = reg;
	linkControlMask = mask;
}

bool AMSControl::isLinked()
{
    return isLinkedToCom;
}

