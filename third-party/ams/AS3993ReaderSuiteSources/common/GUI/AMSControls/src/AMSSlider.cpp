/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  AMSSlider class
 *
 *  AMSSlider is an extension of the QSlider class. It has an extension
 *  which allows a direct connection between a register in a chip
 *  and the slider position.
 */

#include "AMSSlider.hxx"

AMSSlider::AMSSlider(QWidget * parent) : QSlider(parent)
{
}

void AMSSlider::unlink()
{
    if(!isLinkedToCom)
        return;

    disconnect(this, SIGNAL(valueChanged(int)), this, SLOT(onChange(int)));
    disconnect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));

    this->doVerify = false;
    this->doEmit = false;
    this->trcLevel = 0;
    this->linkRegister = 0x00;
    this->linkPos = 0;
    this->useSubregister = false;    
    this->com = NULL;    
    activated = false;
    isLinkedToCom = false;
}

void AMSSlider::link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char min, unsigned char max, bool verify, bool doEmit, unsigned char trcLevel)
{
	this->doVerify = verify;
	this->doEmit = doEmit;
	this->trcLevel = trcLevel;
	this->linkRegister = reg;
	this->linkPos = bitpos;
	this->useSubregister = false;
	this->min = min;
	this->max = max;
    this->mask = calcMask(max);
	this->setMinimum(min);
	this->setMaximum(max);
	this->setSingleStep(1);
	this->com = com;
	connect(this, SIGNAL(valueChanged(int)), this, SLOT(onChange(int)));
	connect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));
	activated = true;
    isLinkedToCom = true;
}

void AMSSlider::link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char min, unsigned char max, bool verify, bool emit, unsigned char trcLevel)
{
	link(com, reg, bitpos, min, max);
	this->linkSubregister = subreg;
	this->useSubregister = true;
}

void AMSSlider::update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value)
{
	if (activated)
	{
		if(!useSubregister && reg == linkRegister)
		{
			filterEvent = true;
			this->setSliderPosition((value & (mask << linkPos)) >> linkPos);
			filterEvent = false;
		}
		else if(useSubregister && isSubreg && reg == linkRegister && subreg == linkSubregister)
		{
			filterEvent = true;
			this->setSliderPosition((value & (mask << linkPos)) >> linkPos);
			filterEvent = false;
		}
	}
}

void AMSSlider::onChange(int pos)
{
	if (NULL != com && !filterEvent)
	{
		activated = false;
		if(linkControlRegister != 0xff)
			this->com->setControlRegisterAddress(linkControlRegister, linkControlMask);
		if (useSubregister)
		{
			this->com->modifySubRegister(linkRegister, mask << linkPos, pos << linkPos, linkSubregister, doVerify, doEmit, trcLevel);
		}
		else
		{
			this->com->modifyRegister(linkRegister, mask << linkPos, pos << linkPos, doVerify, doEmit, trcLevel);
		}
		activated = true;
	}
}

unsigned char AMSSlider::calcMask( unsigned char max )
{
    unsigned char val, idx;
    idx = 0;
    val = max;
    while (val > 0)
    {
        val = val >> 1;
        idx++;
    }
    return pow(2.0, idx) - 1;
}
