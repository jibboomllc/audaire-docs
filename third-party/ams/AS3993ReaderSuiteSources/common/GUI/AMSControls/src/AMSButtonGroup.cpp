/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Dedek
 *
 *  \brief  AMSButtonGroup class
 *
 *  AMSButtonGroup is an extension of the QButtonGroup class. It has an extension
 *  which allows a direct connection between a register/bit in a chip
 *  and the groupbox.
 */

#include "AMSButtonGroup.hxx"
#if QT_VERSION < 0x050000
#else
#include <QAbstractButton>
#endif

AMSButtonGroup::AMSButtonGroup(QWidget * parent) : QButtonGroup(parent)
{
}

void AMSButtonGroup::unlink()
{
    if(!isLinkedToCom)
        return;

    disconnect(this, SIGNAL(buttonClicked(int)), this, SLOT(onClick(int)));
    disconnect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));

    this->linkRegister = 0x00;
    this->linkPos = 0;
    this->useSubregister = false;
    this->linkMask = 0x00;        
    this->com = NULL;
    activated = false;
    useSubregister = false;
    isLinkedToCom = false;
}

void AMSButtonGroup::link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel)
{
	this->linkRegister = reg;
	this->linkPos = bitpos;
	this->useSubregister = false;
	this->linkMask = mask;
	this->com = com;
	connect(this, SIGNAL(buttonClicked(int)), this, SLOT(onClick(int)));
	connect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));
	activated = true;
	useSubregister = false;
    isLinkedToCom = true;
}

void AMSButtonGroup::link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool verif, bool doEmit, unsigned char trcLevel)
{
	link(com, reg, bitpos, mask);
	this->linkSubregister = subreg;
	useSubregister = true;
}

void AMSButtonGroup::update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value)
{
	unsigned char actvalue = (linkMask & value);
	QAbstractButton* tmp = NULL;

	if (AMSControl::activated)
	{
		if(!useSubregister && reg == linkRegister)
		{
			filterEvent = true;
			tmp = this->button(this->getIDFromValue(((value >> this->linkPos)) & this->linkMask));
			if(tmp)
				tmp->setChecked(true);
			filterEvent = false;
		}
		else if(useSubregister && isSubreg && reg == linkRegister && subreg == linkSubregister)
		{
			filterEvent = true;
			this->button(this->getIDFromValue((value >> this->linkPos)) & this->linkMask)->setChecked(true);
			filterEvent = false;
		}
	}
}

void AMSButtonGroup::onClick(int id)
{
	if (NULL != com && !filterEvent)
	{
		activated = true;
		if(linkControlRegister != 0xff)
			this->com->setControlRegisterAddress(linkControlRegister, linkControlMask);
	
		if (useSubregister)
		{
			this->com->modifySubRegister(linkRegister, linkMask << linkPos, getValueFromID(id) << linkPos, linkSubregister);
		}
		else
		{
			this->com->modifyRegister(linkRegister, linkMask << linkPos, getValueFromID(id) << linkPos);
		}
		activated = true;
	}
}

int AMSButtonGroup::getValueFromID(int id)
{
	// this function has to be overwritten to support corresponding functionality
	return id;
}

int AMSButtonGroup::getIDFromValue(int id)
{
	// this function has to be overwritten to support corresponding functionality
	return id;
}