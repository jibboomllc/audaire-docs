/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Puri
 *
 *  \brief  AMSRadioButton class
 *
 *  AMSRadioButton is an extension of the QRadioButton class. It has an extension
 *  which allows a direct connection between a register/bit in a chip
 *  and the radiobutton.
 */

#include "AMSRadioButton.hxx"

AMSRadioButton::AMSRadioButton(QWidget * parent) : QRadioButton(parent)
{
}

void AMSRadioButton::unlink()
{
    if(!isLinkedToCom)
        return;

    disconnect(this, SIGNAL(clicked(bool)), this, SLOT(onClick(bool)));
    disconnect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));    

    this->linkRegister = 0x00;
    this->linkMask = 0;
    this->linkInvert = false;
    this->com = NULL;    
    activated = false;
    useSubregister = false;
    isLinkedToCom = false;
}

void AMSRadioButton::link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, bool invert, bool verify, bool doEmit, unsigned char trcLevel)
{
	this->linkRegister = reg;
	this->linkMask = 1 << bitpos;
	this->linkInvert = invert;
	this->com = com;
	connect(this, SIGNAL(clicked(bool)), this, SLOT(onClick(bool)));
	connect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));
	activated = true;
	useSubregister = false;
    isLinkedToCom = true;
}

void AMSRadioButton::link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, bool invert, bool verify, bool doEmit, unsigned char trcLevel)
{
	link(com, reg, bitpos, invert);
	this->linkSubregister = subreg;
	useSubregister = true;
}

void AMSRadioButton::update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value)
{
	bool actvalue = ((linkMask & value) > 0 ? true : false) ^ linkInvert;
	
	if (activated)
	{
		if(!useSubregister && reg == linkRegister)
		{
			this->setChecked(actvalue);
			filterEvent = true;
			emit clicked(actvalue);
			filterEvent = false;
		}
		else if(useSubregister && isSubreg && reg == linkRegister && subreg == linkSubregister)
		{
			this->setChecked(actvalue);
			filterEvent = true;
			emit clicked(actvalue);
			filterEvent = false;
		}
	}
}

void AMSRadioButton::onClick(bool checked)
{
	if (NULL != com && !filterEvent)
	{
		activated = false;
		if(linkControlRegister != 0xff)
			this->com->setControlRegisterAddress(linkControlRegister, linkControlMask);
		if (useSubregister)
		{
			this->com->modifySubRegister(linkRegister, linkMask, (checked^linkInvert) ? linkMask : 0, linkSubregister);
		}
		else
		{
			this->com->modifyRegister(linkRegister, linkMask, (checked^linkInvert) ? linkMask : 0);
		}
		activated = true;
	}
}
