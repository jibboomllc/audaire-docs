/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  AMSDoubleSpinBox class
 *
 *  AMSDoubleSpinBox is an extension of the QDoubleSpinBox class. 
 *  It has an extension which allows a direct connection between 
 *  a register in a chip and the selected item within the box.
 */

#include "AMSDoubleSpinBox.hxx"

#if QT_VERSION < 0x050000
#else
#include <QStyleOptionSpinBox>
#endif

AMSDoubleSpinBox::AMSDoubleSpinBox(QWidget * parent) : QDoubleSpinBox(parent)
{
	this->com          = NULL;
	itsCurrentRangeIdx = 0;
	itsCurrentIndex    = 0;
	connect(this, SIGNAL(valueChanged(double)), this, SLOT(onChange(double)));
}

double AMSDoubleSpinBox::roundClosest(double unprocessedValue)
{
	double higherValue, lowerValue;
	double deltaHigherValue, deltaLowerValue;
	double processedValue;
	double mult = pow(10.0,decimals());


	higherValue = ceil(unprocessedValue * mult);
	lowerValue = floor(unprocessedValue * mult);

	deltaHigherValue = higherValue - unprocessedValue * mult;
	deltaLowerValue = unprocessedValue * mult - lowerValue;

	if(deltaLowerValue < deltaHigherValue)
	{
		processedValue = lowerValue / mult;
	}
	else   // by this we round up on equal deltas
	{
		processedValue = higherValue / mult;
	}

	return processedValue;
}

int AMSDoubleSpinBox::roundClosestInt(double unprocessedValue)
{
	double higherValue, lowerValue;
	double deltaHigherValue, deltaLowerValue;
	int processedValue;

	higherValue = ceil(unprocessedValue);
	lowerValue = floor(unprocessedValue);

	deltaHigherValue = higherValue - unprocessedValue;
	deltaLowerValue = unprocessedValue - lowerValue;

	if(deltaLowerValue < deltaHigherValue)
	{
		processedValue = lowerValue;
	}
	else   // by this we round up on equal deltas
	{
		processedValue = higherValue;
	}

	return processedValue;
}

void AMSDoubleSpinBox::unlink()
{
    if(!isLinkedToCom)
        return;

    disconnect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));

    this->linkRegister = 0x00;
    this->linkPos = 0;
    this->useSubregister = false;
    this->linkMask = 0x00;
    this->com = NULL;    
    activated = false;
    isLinkedToCom = false;
}

void AMSDoubleSpinBox::link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel)
{
	this->linkRegister = reg;
	this->linkPos = bitpos;
	this->useSubregister = false;
	this->linkMask = mask;
	this->com = com;
	connect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));
	activated = true;
    isLinkedToCom = true;
	this->clearRange();
}

void AMSDoubleSpinBox::link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel)
{
	link(com, reg, bitpos, mask);
	this->linkSubregister = subreg;
	this->useSubregister = true;
}

void AMSDoubleSpinBox::addRange(valueRangeStruct newRange)
{
	bool added = false;
	for(int i=0;i<valueRanges.size();i++)
	{
		if(newRange.fromValue<valueRanges[i].fromValue)
		{
			this->valueRanges.insert(i,newRange);
			added = true;
			break;
		}
	}
	if(!added)
	{
		this->valueRanges.append(newRange);
	}
}

void AMSDoubleSpinBox::clearRange()
{
	this->valueRanges.clear();
}

void AMSDoubleSpinBox::update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value)
{
	unsigned char valueReduction = 0;
	unsigned char realValue = ((value & (linkMask << linkPos)) >> linkPos);
	double offset = 0;

	if (activated)
	{
		if((reg == linkRegister && !isSubreg) || (isSubreg && reg == linkRegister && subreg==linkSubregister))
		{
			double newValue=0.0;
			itsCurrentRangeIdx = 0;
			if(valueRanges.size()>0)
			{
				for(int i = valueRanges.size()-1;i>=0;i--)
				{
					if(realValue >= valueRanges[i].idxOffset)
					{
						itsCurrentRangeIdx = i;
						this->setSingleStep(valueRanges[itsCurrentRangeIdx].stepSize);
						break;
					}
				}
				newValue = roundClosest(valueRanges[itsCurrentRangeIdx].fromValue + (realValue-valueRanges[itsCurrentRangeIdx].idxOffset)*singleStep());
			}
			else
				newValue = minimum() + realValue*singleStep();
			filterEvent = true;
			this->setValue(newValue);
			filterEvent = false;
		}
	}
}

void AMSDoubleSpinBox::onChange(double value)
{
	if (!filterEvent)
	{
		if(valueRanges.size() > 0)
		{
			for(int i = valueRanges.size()-1;i>=0;i--)
			{
				if(value >= valueRanges[i].fromValue)
				{
					itsCurrentRangeIdx = i;
					this->setSingleStep(valueRanges[itsCurrentRangeIdx].stepSize);
					break;
				}
			}
			itsCurrentIndex = valueRanges[itsCurrentRangeIdx].idxOffset + 
				roundClosestInt((value-valueRanges[itsCurrentRangeIdx].fromValue) / singleStep());
		}
		else
			itsCurrentIndex = roundClosestInt((value - minimum()) / singleStep());

		if(valueRanges.size() > 0)
		{
			value = roundClosest(valueRanges[itsCurrentRangeIdx].fromValue + (itsCurrentIndex-valueRanges[itsCurrentRangeIdx].idxOffset)*singleStep());
			filterEvent = true;
			this->setValue(value);
			filterEvent = false;
		}
		
		if(this->com != NULL)
		{
			activated = false;
			if(linkControlRegister != 0xff)
				this->com->setControlRegisterAddress(linkControlRegister, linkControlMask);
			if (useSubregister)
			{
				this->com->modifySubRegister(linkRegister, linkMask << linkPos, itsCurrentIndex << linkPos, linkSubregister);
			}
			else
			{
				this->com->modifyRegister(linkRegister, linkMask << linkPos, itsCurrentIndex << linkPos);
			}
			activated = true;
		}
	}
}

void AMSDoubleSpinBox::forceUpdate(unsigned char reg, unsigned char bitpos, unsigned char mask, unsigned char value)
{
	unsigned char valueReduction = 0;
	double offset = 0;
	unsigned char realValue = ((value & (mask << bitpos)) >> bitpos);

	double newValue=0.0;
	itsCurrentRangeIdx = 0;
	if(valueRanges.size()>0)
	{
		for(int i = valueRanges.size()-1;i>=0;i--)
		{
			if(realValue >= valueRanges[i].idxOffset)
			{
				itsCurrentRangeIdx = i;
				this->setSingleStep(valueRanges[itsCurrentRangeIdx].stepSize);
				break;
			}
		}
		newValue = roundClosest(valueRanges[itsCurrentRangeIdx].fromValue + (realValue-valueRanges[itsCurrentRangeIdx].idxOffset)*singleStep());
		itsCurrentIndex = valueRanges[itsCurrentRangeIdx].idxOffset + 
			roundClosestInt((newValue-valueRanges[itsCurrentRangeIdx].fromValue) / singleStep());
}
	else
	{
		newValue = roundClosest(minimum() + realValue*singleStep());
		itsCurrentIndex = roundClosestInt((newValue - minimum()) / singleStep());
	}
	filterEvent = true;
	this->setValue(newValue);
	filterEvent = false;
}

int AMSDoubleSpinBox::forceGetRegValue()
{
	return itsCurrentIndex;
}

void AMSDoubleSpinBox::mousePressEvent ( QMouseEvent * event )
{
	if(event->button() != Qt::LeftButton)
	{
		event->ignore();
		return;
	}
	if(valueRanges.size() == 0) // fall back to parent implementation
	{
		QDoubleSpinBox::mousePressEvent (event);
		return;
	}
	QStyleOptionSpinBox opt;
	this->initStyleOption(&opt);
	QRect rect = this->style()->subControlRect(QStyle::CC_SpinBox,&opt,QStyle::SC_SpinBoxUp);
	double newValue = 0.0;
	if(rect.contains(event->pos()))
	{
		newValue = roundClosest(this->value() + this->singleStep());
		
		if(newValue > this->maximum())
		{
			event->accept();
			return;
		}
		
		if(((int)itsCurrentRangeIdx < (valueRanges.size() - 1)) 
			&& compare(newValue,valueRanges[itsCurrentRangeIdx].toValue, 1) 
			&& (this->stepEnabled() & QDoubleSpinBox::StepUpEnabled))
		{
			this->setSingleStep(valueRanges[++itsCurrentRangeIdx].stepSize);
			this->setValue(compare(valueRanges[itsCurrentRangeIdx].fromValue,this->value(),0) ? 
																		this->value() + this->singleStep() :
																		valueRanges[itsCurrentRangeIdx].fromValue);
			event->accept();
			return;
		}
	}
	else
	{
		newValue = this->value() - this->singleStep();
		if(itsCurrentRangeIdx>0
			&& (compare(newValue,valueRanges[itsCurrentRangeIdx].fromValue, -1)) 
			&& (this->stepEnabled() & QDoubleSpinBox::StepDownEnabled))
		{
			this->setSingleStep(valueRanges[--itsCurrentRangeIdx].stepSize);
			this->setValue(compare(valueRanges[itsCurrentRangeIdx].toValue,this->value(),0) ? 
																						this->value() - this->singleStep() : 
																						valueRanges[itsCurrentRangeIdx].toValue);
			event->accept();
			return;
		}
	}
	QDoubleSpinBox::mousePressEvent (event);
}

bool AMSDoubleSpinBox::compare(double in1, double in2, short method, short precision)
{
	short pre = precision > 0 ? precision : this->decimals();
	int intIn1 = (int)(in1 * pow(10.0,pre));
	int intIn2 = (int)(in2 * pow(10.0,pre));
	
	if(method == 0)
		return (intIn1 == intIn2);
	else if(method == 1)	
		return (intIn1 > intIn2);
	else if(method == 2)	
		return (intIn1 >= intIn2);
	else  if(method == -1)	
		return (intIn1 < intIn2);
	else  if(method == -2)	
		return (intIn1 <= intIn2);
	return false;
}
