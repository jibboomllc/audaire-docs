/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Puri
 *
 *  \brief  AMSGroupBox class
 *
 *  AMSGroupBox is an extension of the QGroupBox class. It has an extension
 *  which allows a direct connection between a register/bit in a chip
 *  and the backgroud color style sheet of the QGroupBox.
 */

#include "AMSGroupBox.hxx"

AMSGroupBox::AMSGroupBox(QWidget * parent) : QGroupBox(parent)
{
}

void AMSGroupBox::unlink()
{
    if(!isLinkedToCom)
        return;

    disconnect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));

    this->linkRegister = 0x00;
    this->linkMask = 0;
    this->linkInvert = false;
    this->com = NULL;    
    activated = false;
    useSubregister = false;
    isLinkedToCom = false;
}

void AMSGroupBox::link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, bool invert, bool verify, bool doEmit, unsigned char trcLevel)
{
	this->linkRegister = reg;
	this->linkMask = 1 << bitpos;
	this->linkInvert = invert;
	this->com = com;
	connect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));
	activated = true;
	useSubregister = false;
    isLinkedToCom = true;
}

void AMSGroupBox::link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, bool invert, bool verify, bool doEmit, unsigned char trcLevel)
{
	link(com, reg, bitpos, invert);
	this->linkSubregister = subreg;
	useSubregister = true;
}

void AMSGroupBox::setStyleSheetTrue(const QString& styleSheet)
{
	this->styleSheetTrue = styleSheet;
}

void AMSGroupBox::setStyleSheetFalse(const QString& styleSheet)
{
	this->styleSheetFalse = styleSheet;
}

void AMSGroupBox::update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value)
{
	bool actvalue = ((linkMask & value) > 0 ? true : false) ^ linkInvert;
	
	if (activated)
	{
		if(!useSubregister && reg == linkRegister)
		{
			if(actvalue)
				this->setStyleSheet(this->styleSheetTrue);
			else
				this->setStyleSheet(this->styleSheetFalse);
		}
		else if(useSubregister && isSubreg && reg == linkRegister && subreg == linkSubregister)
		{
			if(actvalue)
				this->setStyleSheet(this->styleSheetTrue);
			else
				this->setStyleSheet(this->styleSheetFalse);
		}
	}
}
