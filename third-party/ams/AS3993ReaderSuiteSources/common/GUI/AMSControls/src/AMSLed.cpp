/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Dedek
 *  \author F. Lobmaier
 *
 *  \brief  AMSLed class
 *
 *  AMSLed implements LED functionality controlled by a register 
 */

#include "AMSLed.hxx"

AMSLed::AMSLed(QWidget *parent, Qt::WindowFlags f): QFrame(parent, f)
{
    itsLedType = RedGreen;
	lit = false;
	connect(&blinkTimer, SIGNAL(timeout()), this, SLOT(switchLED()));
	this->isActive = false;	
    setLedType(itsLedType);
}

void AMSLed::unlink()
{
    if(!isLinkedToCom)
        return;

    disconnect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));

    this->linkRegister = 0x00;
    this->linkPos = 0;
    this->useSubregister = false;
    this->linkMask = 0x00;
    this->com = NULL;
    this->inverted = false;    
    activated = false;
    useSubregister = false;
    isLinkedToCom = false;
}

void AMSLed::link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool invert)
{
	this->linkRegister = reg;
	this->linkPos = bitpos;
	this->useSubregister = false;
	this->linkMask = mask;
	this->com = com;
	this->inverted = invert;
	connect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));
	activated = true;
	useSubregister = false;
    isLinkedToCom = true;
}

void AMSLed::link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool invert)
{
	link(com, reg, bitpos, mask, invert);
	this->linkSubregister = subreg;
	useSubregister = true;
}

void AMSLed::update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value)
{
	bool isOn = false;
	unsigned char actvalue = (linkMask & value);
	
	if (AMSControl::activated)
	{
		if(!useSubregister && reg == linkRegister)
		{
			isOn = (value >> this->linkPos) & this->linkMask;
			if(this->inverted)
				isOn = !isOn;
			if(isOn)
				ledOn();
			else 
				ledOff();

		}
		else if(useSubregister && isSubreg && reg == linkRegister && subreg == linkSubregister)
		{
			isOn = (value >> this->linkPos) & this->linkMask;
		}
	}
}

void AMSLed::switchLED()
{
	lit = ! lit;
    QString ledStylesheet(QString("image: url(%1);").arg(lit ? ledOnStyleSheet : ledOffStyleSheet));
	this->setStyleSheet(ledStylesheet);
}

void AMSLed::setLedType( AMSLedType ledType )
{
    itsLedType = ledType;

    switch(ledType)
    {
        case RedGreen:
            ledOffStyleSheet = ":/icons/green/rec";
            ledOnStyleSheet = ":/icons/red/rec";            
            break;
		case GrayGreen:
			ledOffStyleSheet = ":/icons/lightgray/record";
			ledOnStyleSheet = ":/icons/green/rec";
			break;
        case OffGreen:
            ledOffStyleSheet = ":/icons/lightgray/ledoff";
            ledOnStyleSheet = ":/icons/green/rec";
            break;
        case OffRed:            
            ledOffStyleSheet = ":/icons/lightgray/ledoff";
            ledOnStyleSheet = ":/icons/red/rec";
            break;
        default:
            ledOffStyleSheet = ":/icons/green/rec";
            ledOnStyleSheet = ":/icons/red/rec";
            break;
    }

    QString ledStylesheet(QString("image: url(%1);").arg(lit ? ledOnStyleSheet : ledOffStyleSheet));
    this->setStyleSheet(ledStylesheet);
}

void AMSLed::ledOn()
{
	if(lit == false)
		switchLED();
}

void AMSLed::ledOff()
{
	if(lit == true)
		switchLED();
}

void AMSLed::startStopBlink()
{
	blinkTimer.isActive() ?  blinkTimer.stop() : blinkTimer.start();
}

void AMSLed::singleShotStart(bool on, int timeout)
{
	if(this->isActive)
		return;
	if(on)
		ledOn();
	else
		ledOff();
	blinkTimer.singleShot(timeout, this, SLOT(ledTimeout()));
}

void AMSLed::ledTimeout()
{
	this->switchLED();
	this->isActive = false;
}

void AMSLed::ledOnOff(bool on)
{
	if(on)
		ledOn();
	else
		ledOff();
}

void AMSLed::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		emit clicked(true);
	}
}



