/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSControls
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Puri-Jobi
 *
 *  \brief  AMSSpinBox class
 *
 *  AMSSpinBox is an extension of the QSpinBox class. 
 *  It has an extension which allows a direct connection between 
 *  a register in a chip and the selected item within the box.
 */

#include "AMSSpinBox.hxx"

#if QT_VERSION < 0x050000
#else
#include <QStyleOptionSpinBox>
#endif

AMSSpinBox::AMSSpinBox(QWidget * parent) : QSpinBox(parent)
{
	this->numberOfDigits = 8;
	this->base = 16;
	this->mode = Hexdecimal;
}

void AMSSpinBox::unlink()
{
    if(!isLinkedToCom)
        return;

    disconnect(this, SIGNAL(valueChanged(int)), this, SLOT(onChange(int)));
    disconnect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));

    this->linkRegister = 0x00;
    this->linkPos = 0;
    this->useSubregister = false;
    this->linkMask = 0x00;
    this->com = NULL;    
    activated = false;
    isLinkedToCom = false;
}

void AMSSpinBox::link(AMSCommunication* com, unsigned char reg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel)
{
	this->linkRegister = reg;
	this->linkPos = bitpos;
	this->useSubregister = false;
	this->linkMask = mask;
	this->com = com;
	connect(this, SIGNAL(valueChanged(int)), this, SLOT(onChange(int)));
	connect(com, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(update(unsigned char, unsigned char, bool, unsigned char)));
	activated = true;
    isLinkedToCom = true;
}

void AMSSpinBox::link(AMSCommunication* com, unsigned char reg, unsigned char subreg, unsigned char bitpos, unsigned char mask, bool verify, bool doEmit, unsigned char trcLevel)
{
	link(com, reg, bitpos, mask);
	this->linkSubregister = subreg;
	this->useSubregister = true;
}

void AMSSpinBox::setNumberOfDigits(int digits)
{
	this->numberOfDigits = digits;
}

QValidator::State AMSSpinBox::validate(QString &input, int &pos) const
{
	return QValidator::Acceptable;
}

int AMSSpinBox::valueFromText(const QString &text) const
{
	bool ok;
	QString tmp = text;

	if(!suffix().isEmpty())
		tmp.chop(suffix().length());

	if (tmp.contains("0x"))
	{
		return tmp.mid(2).toInt(&ok, 16);
	}
	else if (tmp.endsWith("b"))
	{
		return tmp.left(tmp.length()-1).toInt(&ok, 2);
	}
	else
	{
		return tmp.toInt();
	}
}

QString AMSSpinBox::textFromValue(int value) const
{
	QString valueStr;
    if (numberOfDigits == -1)
        valueStr = QString::QString("%1").arg(QString::number(value, this->base)).toUpper();
    else
	    valueStr = QString::QString("%1").arg(QString::number(value, this->base), numberOfDigits, '0').toUpper();
	if(mode==Hexdecimal)
		valueStr.prepend("0x");
	else if(mode==Binary)
		valueStr.append("b");
	return valueStr;
}

void AMSSpinBox::update(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value)
{
	if (activated)
	{
		if(reg == linkRegister &&
			(!useSubregister || (useSubregister && isSubreg && (subreg == linkSubregister))))
		{
			filterEvent = true;
			this->setValue(((value & (linkMask << linkPos)) >> linkPos) * singleStep() + minimum());
			filterEvent = false;
		}
	}
}

void AMSSpinBox::onChange(int value)
{
	if (NULL != com && !filterEvent)
	{
		activated = false;
		if(linkControlRegister != 0xff)
			this->com->setControlRegisterAddress(linkControlRegister, linkControlMask);
		if(minimum() < 0)
			value = value + (-1)*minimum();

		if (useSubregister)
		{
			this->com->modifySubRegister(linkRegister, linkMask << linkPos, value << linkPos, linkSubregister);
		}
		else
		{
			this->com->modifyRegister(linkRegister, linkMask << linkPos, value << linkPos);
		}
		activated = true;
	}
}

void AMSSpinBox::setRepresentation(Representation mode)
{
	this->mode = mode;
	if(mode == Decimal)
		base = 10;
	else if(mode == Hexdecimal)
		base = 16;
	else if(mode == Binary)
		base = 2;
}

void AMSSpinBox::mousePressEvent ( QMouseEvent * event )
{
    if(event->button() != Qt::LeftButton)
    {
        event->ignore();
        return;
    }
    QStyleOptionSpinBox opt;
    this->initStyleOption(&opt);
    QRect rect = this->style()->subControlRect(QStyle::CC_SpinBox,&opt,QStyle::SC_SpinBoxUp);
    int newValue = 0;
    if(rect.contains(event->pos()))
    {
        newValue = this->value() + this->singleStep();

        if(newValue <= this->maximum())
        {
            this->setValue(newValue);
            emit arrowPressed();
            event->accept();
            return;
        }
        else
        {
            event->accept();
            return;
        }
    }
    else
    {
        newValue = this->value() - this->singleStep();

        if(newValue >= this->minimum())
        {
            this->setValue(newValue);
            emit arrowPressed();
            event->accept();
            return;
        }
        else
        {
            event->accept();
            return;
        }
    }
    QSpinBox::mousePressEvent (event);
}
