/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams Streaming Communication
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file I2cComObjects.cpp
 *
 *  \author M. Arpa
 *
 *  \brief Implemenation of the classes that use i2c as protocol. E.g. to
 * read/write registers, block-read/write. 
 */

#include "I2cComObjects.h"
#include "AmsCom.h" 
#include <QXmlStreamWriter>
#include <QString>
#include <QByteArray>

#define AMS_COM_XML_REGISTER_TAG		"register"
#define AMS_COM_XML_BLOCK_TAG			"block"
#define AMS_COM_XML_CONFIG_TAG			"config"
#define AMS_COM_XML_SLAVE_ADDRESS		"slaveaddress"
#define AMS_COM_XML_ADDRESS_SIZE		"addresssize"
#define AMS_COM_XML_ADDRESS				"address"
#define AMS_COM_XML_DATA_SIZE			"datasize"
#define AMS_COM_XML_DATA				"data"
#define AMS_COM_XML_MAX_BLOCK_SIZE		"maxblocksize"
#define AMS_COM_XML_BLOCK_SIZE			"blocksize"
#define AMS_COM_XML_MODULE				"module"
#define AMS_COM_XML_CLOCK_MODE			"clockmode"
#define AMS_COM_XML_CUSTOM_CLOCK_SPEED  "customclockspeed"


AmsComObject * I2cComXmlReader::clone( QXmlStreamReader * xml, unsigned char protocol )
{
    AmsComObject * p = 0;
    if ( xml && ! xml->atEnd() && ! xml->hasError() )
    {
        /* sanity check - we are at a telegram */
        if ( xml->isStartElement() && xml->name() == AMS_COM_XML_TAG )
        {
            if ( xml->readNextStartElement() )
            {
                if ( xml->name() == AMS_COM_XML_REGISTER_TAG )
                {
                    p = new I2cRegisterObject( 0, 0, 0 );
                }
                else if ( xml->name() == AMS_COM_XML_BLOCK_TAG )
                {
                    p = new I2cBlockObject( 0, 0 );
                }
                else if ( xml->name() == AMS_COM_XML_CONFIG_TAG )
                {
                    p = new I2cConfigObject( 0, 0 );
                }
            }
        }
    }
    return p;
}



I2cRegisterObject::I2cRegisterObject 
	( unsigned char i2cSlaveAddress
	, unsigned int registerAddress
	, unsigned int data
    , unsigned int tracePattern
	, int addressSize
	, int dataSize
	) : AmsComObject
	      ( AMS_COM_I2C
 	      , 1 + addressSize + dataSize
	      , 1 + addressSize + 1
	      , dataSize
          , tracePattern
	      )
	, itsI2CSlaveAddress( i2cSlaveAddress )
    , itsAddressSize( addressSize )
	, itsDataSize( dataSize )
	, itsAddress( registerAddress )
	, itsData( data ) 
{ 
}

I2cRegisterObject::I2cRegisterObject 
	( const I2cRegisterObject & other 
	) : AmsComObject( other )
	, itsI2CSlaveAddress( other.itsI2CSlaveAddress )
	, itsAddressSize( other.itsAddressSize )
	, itsDataSize( other.itsDataSize )
	, itsAddress( other.itsAddress )
	, itsData( other.itsData ) 
{ 
}

bool I2cRegisterObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_WRITE );
	if ( bufferSize >= itsSerialSize )
	{
        if ( serialising( buffer, 1, itsI2CSlaveAddress << 1 ) )  /* i2c write */
        {   
        	buffer++;
            if ( serialising( buffer, itsAddressSize, itsAddress ) )
            {
                buffer += itsAddressSize;
                result = serialising( buffer, itsDataSize, itsData );
                if ( result && xml )
                {
                    xml->writeStartElement( AMS_COM_XML_REGISTER_TAG );
                    xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_DATA_SIZE, QString::number( itsDataSize, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_DATA, QString::number( itsData, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeEndElement( );
                }
            }
        }
    }
    writeClosingTag( xml );
    return result;
}
	
bool I2cRegisterObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                int val = xml->readElementText().toInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_SLAVE_ADDRESS )
                {
                    result = true;
                    itsI2CSlaveAddress = val;
                }
                else if ( xml->name() == AMS_COM_XML_ADDRESS_SIZE )
                {
                    result = true;
                    itsAddressSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_ADDRESS )
                {
                    result = true;
                    itsAddress = val;
                }
                else if ( xml->name() == AMS_COM_XML_DATA_SIZE )
                {
                    result = true;
                    itsDataSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_DATA )
                {
                    result = true;
                    itsData = val;
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
    }
    return result;
}

bool I2cRegisterObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ );
	if ( bufferSize >= itsRxSerialSize )
	{
        if ( serialising( buffer, 1, itsI2CSlaveAddress << 1 ) )  /* i2c write */
        {
    	    buffer++;
            if ( serialising( buffer, itsAddressSize, itsAddress ) )
            {
                buffer += itsAddressSize;
                result = serialising( buffer, 1, ( itsI2CSlaveAddress << 1 ) | 0x1 );  /* i2c read */
                if ( result && xml )
                {
                    xml->writeStartElement( AMS_COM_XML_REGISTER_TAG );
                    xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_DATA_SIZE, QString::number( itsDataSize, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeEndElement( );
                }
            }
        }
    }
    writeClosingTag( xml );
    return result;
}
	
bool I2cRegisterObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ_RESULT );
	if ( bufferSize >= itsDeserialSize )
	{	/* buffer[ 0 ] = begin of data */
        result = deserialising( buffer, itsDataSize, itsData );
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_REGISTER_TAG );
            xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_DATA_SIZE, QString::number( itsDataSize, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_DATA, QString::number( itsData, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeEndElement( );
        }
	}
    writeClosingTag( xml );
    return result;
}

	
I2cBlockObject::I2cBlockObject 
	( unsigned char i2cSlaveAddress
	, unsigned int startRegisterAddress
	, int blockSize
	, unsigned char * data
	, int addressSize
	, int maxBlockSize
	)
	: AmsComObject( AMS_COM_I2C, 1 + addressSize + blockSize, 1 + addressSize + 1, blockSize )
	, itsI2CSlaveAddress( i2cSlaveAddress )
	, itsAddressSize( addressSize )
	, itsBlockSize( blockSize )
	, itsAddress( startRegisterAddress )
	, itsMaxBlockSize( maxBlockSize )
	, itsData( new unsigned char [ itsMaxBlockSize ] ) 
{ 
	if ( data )
	{
		set( data );
	}
}

I2cBlockObject::I2cBlockObject 
	( const I2cBlockObject & other 
	) : AmsComObject( other )
	, itsI2CSlaveAddress( other.itsI2CSlaveAddress )
	, itsAddressSize( other.itsAddressSize )
	, itsBlockSize( other.itsBlockSize )
	, itsAddress( other.itsAddress )
	, itsMaxBlockSize( other.itsMaxBlockSize )
	, itsData( new unsigned char [ itsMaxBlockSize ] ) 
{ 
	set( other.itsData );
}

I2cBlockObject::~I2cBlockObject ( )
{
	delete [ ] itsData;
}

bool I2cBlockObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                QString value = xml->readElementText();
                int val = value.toInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_SLAVE_ADDRESS )
                {
                    itsI2CSlaveAddress = val;
                    result = true;
                }
                else if ( xml->name() == AMS_COM_XML_ADDRESS_SIZE )
                {
                    result = true;
                    itsAddressSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_ADDRESS )
                {
                    result = true;
                    itsAddress = val;
                }
                else if ( xml->name() == AMS_COM_XML_MAX_BLOCK_SIZE )
                {
                    result = true;
                    itsMaxBlockSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_BLOCK_SIZE )
                {
                    result = true;
                    itsBlockSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_DATA )
                {
                    result = true;
                    int i;
                    for ( i = 0; i < itsBlockSize; ++i )
                    {
                        itsData[ i ] = static_cast< unsigned char >( value.mid( i*2, 2 ).toUShort( 0, AMS_COM_XML_NUMBER_BASE ) );
                    }
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
    }
    return result;
}

bool I2cBlockObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_WRITE );
	if ( bufferSize >= serialiseSize() )
	{
        if ( serialising( buffer, 1, itsI2CSlaveAddress << 1 ) )  /* i2c write */
        {   
        	buffer++;
            result = serialising( buffer, itsAddressSize, itsAddress );
        	buffer += itsAddressSize;
           	memcpy( buffer, itsData, itsBlockSize );
            if ( xml )
            {
                xml->writeStartElement( AMS_COM_XML_BLOCK_TAG );
                xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_MAX_BLOCK_SIZE, QString::number( itsMaxBlockSize, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_BLOCK_SIZE, QString::number( itsBlockSize, AMS_COM_XML_NUMBER_BASE ) );
                char * p = reinterpret_cast< char *>( itsData );
                xml->writeTextElement( AMS_COM_XML_DATA, QString( QByteArray( p, itsBlockSize).toHex() ) );
                xml->writeEndElement( );
            }
        }
    }
    writeClosingTag( xml );
    return result;
}
	

bool I2cBlockObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ );
	if ( bufferSize >= itsRxSerialSize )
	{
        if ( serialising( buffer, 1, itsI2CSlaveAddress << 1 ) )  /* i2c write */
        {
    	    buffer++;
            if ( serialising( buffer, itsAddressSize, itsAddress ) )
            {
                buffer += itsAddressSize;
                result = serialising( buffer, 1, ( itsI2CSlaveAddress << 1 ) | 0x1 );  /* i2c read */
                if ( xml )
                {
                    xml->writeStartElement( AMS_COM_XML_BLOCK_TAG );
                    xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_MAX_BLOCK_SIZE, QString::number( itsMaxBlockSize, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeEndElement( );
                }
            }
        }
    }
    writeClosingTag( xml );
    return result;
}
	
bool I2cBlockObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ_RESULT );
	if ( bufferSize <= itsMaxBlockSize ) // FIXME check with wri
	{ /* buffer[ 0 ] = begin of data */
        result = true;
        set( buffer, bufferSize ); // FIXME check with wri
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_BLOCK_TAG );
            xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_MAX_BLOCK_SIZE, QString::number( itsMaxBlockSize, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_BLOCK_SIZE, QString::number( itsBlockSize, AMS_COM_XML_NUMBER_BASE ) );
            char * p = reinterpret_cast< char *>( itsData );
            xml->writeTextElement( AMS_COM_XML_DATA, QString( QByteArray( p, itsBlockSize ).toHex() ) );
            xml->writeEndElement( );
        }
	}
    writeClosingTag( xml );
    return result;
}

void I2cBlockObject::set( const unsigned char * data ) 
{ 
	memcpy( itsData, data, itsBlockSize ); 
}

void I2cBlockObject::set( const unsigned char * data, int size ) 
{ 
	if ( size != itsBlockSize )
	{ /* size has changed -> adjust sizes */
		itsBlockSize = ( size > itsMaxBlockSize ? itsMaxBlockSize : size ); 
		itsSerialSize = 1 + itsAddressSize + itsBlockSize; 
		itsRxSerialSize = 1 + itsAddressSize + 1;
		itsDeserialSize = itsBlockSize;
	}
	set( data );
}

void I2cBlockObject::set( int startRegisterAddress, const unsigned char * data, int size ) 
{ 
	itsAddress = startRegisterAddress; 
	set( data, size ); 
}

const char * I2cConfigObject::clockModeToFrequency( unsigned char clockMode )
{
    switch ( clockMode )
    {
    case I2cConfigObject::theClockMode100KHz:
        return "100KHz";
    case I2cConfigObject::theClockMode400KHz:
        return "400KHz";
    case I2cConfigObject::theClockMode1MHz:
        return "1MHz";
    case I2cConfigObject::theClockMode3_4MHz:
        return "3.4MHz";
	case I2cConfigObject::theClockModeCustomSpeed:
		return "custom speed";
    }
    return "unknown";
}


I2cConfigObject::I2cConfigObject 
	( unsigned char module
	, unsigned char clockMode 
	, unsigned int customClockSpeed
	)
	: AmsComObject( AMS_COM_I2C_CONFIG, AMS_STREAM_I2C_CONFIG_OBJ_LENGTH, 0, 0 )
	, itsModule( ( module == 1 || module == 2 ? module : 1 ) )
	, itsClockMode( clockMode )
	, itsCustomClockSpeed( customClockSpeed )
{
}


I2cConfigObject::I2cConfigObject 
	( const I2cConfigObject & other 
	)
	: AmsComObject( other )
	, itsModule( other.itsModule )
	, itsClockMode( other.itsClockMode )
	, itsCustomClockSpeed( other.itsCustomClockSpeed )
{
}

bool I2cConfigObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    bool res = true;
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                int val = xml->readElementText().toInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_MODULE )
                {
                    itsModule = val;
                    result = true;
                }
                else if ( xml->name() == AMS_COM_XML_ADDRESS_SIZE )
                {
                    if ( val != 1 )
                    {
                        res = false; 
                    }
                    result = true;
                }
                else if ( xml->name() == AMS_COM_XML_CLOCK_MODE )
                {
                    itsClockMode = val;
                    result = true;
                }
				else if ( xml->name() == AMS_COM_XML_CUSTOM_CLOCK_SPEED )
				{
					itsCustomClockSpeed = val;
					result = true;
				}
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
    }
    return ( result ? res : result );
}

bool I2cConfigObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml  )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_WRITE );
	if ( bufferSize >= serialiseSize() )
	{
        buffer[ 0 ] = itsModule;
	    buffer[ 1 ] = 0x01;  /* currently the pc sw only supports 7-bit i2c-addressing */
	    buffer[ 2 ] = itsClockMode;
		AMS_STREAM_I2C_CONFIG_SET_CUSTOM_CLK_SPEED(buffer, itsCustomClockSpeed);
		
        result = true;
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_CONFIG_TAG );
            xml->writeTextElement( AMS_COM_XML_MODULE, QString::number( itsModule, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( 1, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_CLOCK_MODE, QString::number( itsClockMode, AMS_COM_XML_NUMBER_BASE ) );
			xml->writeTextElement( AMS_COM_XML_CUSTOM_CLOCK_SPEED, QString::number( itsCustomClockSpeed, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeEndElement( );
        }
    }
    writeClosingTag( xml );
    return result;
}

bool I2cConfigObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml  ) 
{ 
	return false; 
}

bool I2cConfigObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml  ) 
{ 
	return false; 
}

I2cBlockWriteObject::I2cBlockWriteObject 
    ( unsigned char i2cSlaveAddress
    , unsigned int startRegisterAddress
    , int blockSize
    , unsigned char * data
    , int addressSize
    , int maxBlockSize
    )
    : AmsComObject( AMS_COM_I2C, 0, 1 + addressSize + blockSize, 0 /* just ack status */ )
    , itsI2CSlaveAddress( i2cSlaveAddress )
    , itsAddressSize( addressSize )
    , itsBlockSize( blockSize )
    , itsAddress( startRegisterAddress )
    , itsMaxBlockSize( maxBlockSize )
    , itsData( new unsigned char [ itsMaxBlockSize ] ) 
{ 
    if ( data )
    {
        set( data );
    }
}

I2cBlockWriteObject::I2cBlockWriteObject 
    ( const I2cBlockWriteObject & other 
    ) : AmsComObject( other )
    , itsI2CSlaveAddress( other.itsI2CSlaveAddress )
    , itsAddressSize( other.itsAddressSize )
    , itsBlockSize( other.itsBlockSize )
    , itsAddress( other.itsAddress )
    , itsMaxBlockSize( other.itsMaxBlockSize )
    , itsData( new unsigned char [ itsMaxBlockSize ] ) 
{ 
    set( other.itsData );
}

I2cBlockWriteObject::~I2cBlockWriteObject ( )
{
    delete [ ] itsData;
}

bool I2cBlockWriteObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                QString value = xml->readElementText();
                int val = value.toInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_SLAVE_ADDRESS )
                {
                    itsI2CSlaveAddress = val;
                    result = true;
                }
                else if ( xml->name() == AMS_COM_XML_ADDRESS_SIZE )
                {
                    result = true;
                    itsAddressSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_ADDRESS )
                {
                    result = true;
                    itsAddress = val;
                }
                else if ( xml->name() == AMS_COM_XML_MAX_BLOCK_SIZE )
                {
                    result = true;
                    itsMaxBlockSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_BLOCK_SIZE )
                {
                    result = true;
                    itsBlockSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_DATA )
                {
                    result = true;
                    int i;
                    for ( i = 0; i < itsBlockSize; ++i )
                    {
                        itsData[ i ] = static_cast< unsigned char >( value.mid( i*2, 2 ).toUShort( 0, AMS_COM_XML_NUMBER_BASE ) );
                    }
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
    }
    return result;
}

bool I2cBlockWriteObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    return false; /* never transmit without receive */
}


bool I2cBlockWriteObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ );
    if ( bufferSize >= serialiseSize() )
    {
        if ( serialising( buffer, 1, itsI2CSlaveAddress << 1 ) )  /* i2c write */
        {   
            buffer++;
            result = serialising( buffer, itsAddressSize, itsAddress );
            buffer += itsAddressSize;
            memcpy( buffer, itsData, itsBlockSize );
            if ( xml )
            {
                xml->writeStartElement( AMS_COM_XML_BLOCK_TAG );
                xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_MAX_BLOCK_SIZE, QString::number( itsMaxBlockSize, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_BLOCK_SIZE, QString::number( itsBlockSize, AMS_COM_XML_NUMBER_BASE ) );
                char * p = reinterpret_cast< char *>( itsData );
                xml->writeTextElement( AMS_COM_XML_DATA, QString( QByteArray( p, itsBlockSize).toHex() ) );
                xml->writeEndElement( );
            }
        }
    }
    writeClosingTag( xml );
    return result;
}

bool I2cBlockWriteObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    writeOpeningTag( xml, AMS_COM_XML_READ_RESULT );
    if ( xml )
    {
            xml->writeStartElement( AMS_COM_XML_BLOCK_TAG );
            xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeEndElement( );
    }
    writeClosingTag( xml );
    return ( itsStatus == AMS_STREAM_NO_ERROR);
}

void I2cBlockWriteObject::set( const unsigned char * data ) 
{ 
    memcpy( itsData, data, itsBlockSize ); 
}

void I2cBlockWriteObject::set( const unsigned char * data, int size ) 
{ 
    if ( size != itsBlockSize )
    { /* size has changed -> adjust sizes */
        itsBlockSize = ( size > itsMaxBlockSize ? itsMaxBlockSize : size ); 
        itsSerialSize = 0; // not possible 
        itsRxSerialSize = 1 + itsAddressSize + itsBlockSize;
        itsDeserialSize = 0;
    }
    set( data );
}

void I2cBlockWriteObject::set( int startRegisterAddress, const unsigned char * data, int size ) 
{ 
    itsAddress = startRegisterAddress; 
    set( data, size ); 
}


I2cBlockReadObject::I2cBlockReadObject 
    ( unsigned char i2cSlaveAddress
    , int txBlockSize
    , int rxBlockSize
    , unsigned char * txData
    , int maxBlockSize
    )
    : AmsComObject( AMS_COM_I2C, 0, 1 + txBlockSize + 1 , rxBlockSize )
    , itsI2CSlaveAddress( i2cSlaveAddress )
    , itsTxBlockSize( txBlockSize )
    , itsRxBlockSize( rxBlockSize )
    , itsMaxBlockSize( maxBlockSize )
    , itsTxData( new unsigned char [ itsMaxBlockSize ] ) 
    , itsRxData( new unsigned char [ itsMaxBlockSize ] ) 
{ 
    if ( txData )
    {
        memcpy( itsTxData, txData, itsTxBlockSize ); 
    }
}

I2cBlockReadObject::~I2cBlockReadObject ( )
{
    delete [ ] itsTxData;
    delete [ ] itsRxData;
}

bool I2cBlockReadObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    return result;
}

bool I2cBlockReadObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    return result;
}


bool I2cBlockReadObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ );
    if ( bufferSize >= rxSerialiseSize() )
    {
        if ( serialising( buffer, 1, itsI2CSlaveAddress << 1 ) )  /* i2c write */
        {   
            buffer++;
            memcpy( buffer, itsTxData, itsTxBlockSize );
            buffer += itsTxBlockSize;
            if ( xml )
            {
                xml->writeStartElement( AMS_COM_XML_BLOCK_TAG );
                xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeEndElement( );
            }
            result = serialising( buffer, 1, ( itsI2CSlaveAddress << 1 ) | 0x1 );  /* i2c read */
        }
    }
    writeClosingTag( xml );
    return result;
}

bool I2cBlockReadObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    itsRxBlockSize = bufferSize; /* all that we get is desired */
    if ( itsRxBlockSize > itsMaxBlockSize )
    {
        itsRxBlockSize = itsMaxBlockSize;
    }
    memcpy( itsRxData, buffer, itsRxBlockSize);

    writeOpeningTag( xml, AMS_COM_XML_READ_RESULT );
    if ( xml )
    {
        xml->writeStartElement( AMS_COM_XML_BLOCK_TAG );
        xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsI2CSlaveAddress, AMS_COM_XML_NUMBER_BASE ) );
        xml->writeEndElement( );
    }
    writeClosingTag( xml );
    return ( itsStatus == AMS_STREAM_NO_ERROR);
}

