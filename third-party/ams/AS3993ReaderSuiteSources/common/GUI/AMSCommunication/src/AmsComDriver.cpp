/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComDriver.cpp
 *
 *  \author M. Arpa
 *
 *  \brief  This is the base class of all steam communication drivers. I.e. 
 * any driver that shall be used for stream communication must be derived
 * from this class.
 */

#include "AmsComDriver.h"

const char * AmsComDriver::errorStrings[] =
{ "Success"
, "Error: channel not open"
, "Error: transmit failed"
, "Error: receive failed"
, "Error: received and expected data mismatch"
, "Error: unknown error code"
};

const int AmsComDriver::minErrorCode = AmsComDriver::RxDataMismatch; //negative
const int AmsComDriver::maxErrorCode = AmsComDriver::NoError; //null

const char * AmsComDriver::errorToString ( int code )
{
    if ( code > maxErrorCode || code < minErrorCode )
    {
        code = maxErrorCode;
    }

    return errorStrings[ code - minErrorCode ];
}