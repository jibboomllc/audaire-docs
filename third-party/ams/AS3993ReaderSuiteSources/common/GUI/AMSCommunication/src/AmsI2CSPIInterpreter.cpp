/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       * 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams I2C and SPI interpreter
 *      $Revision: $
 *      LANGUAGE: QT C++
 */
/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  Class to read in an i2c or spi text pattern and convert it into i2c/spi commands.
 *
 */

#include "AmsI2CSPIInterpreter.h"
#include <QStringList>
#include <QStringListIterator>
#include <QMessageBox>
#include <QDebug>

AmsI2CSPIInterpreter::AmsI2CSPIInterpreter ( )
{
}

AmsI2CSPIInterpreter::~AmsI2CSPIInterpreter ( )
{
}

bool AmsI2CSPIInterpreter::interpretI2CCommand ( const QString & command, unsigned char & deviceAddress, unsigned char & txSize, unsigned char * tx, unsigned char & rxSize )
{
    bool endOfCommand = false;
    bool repeatedStart = false;
    unsigned char toTx = 0;
    QStringList cmd = command.split( ' ', QString::SkipEmptyParts );
    QStringListIterator iterator( cmd );

 
    if ( cmd.size() > txSize ) /* some check that we can store the whole command in the buffer */
    {
        QMessageBox::critical( 0, "Illegal command", "Command is too long");
        txSize = 0;
        return false;
    }

    /* command structure is as follows:
       S devAddr W regAddr Value0 Value1 ... P - use this to write n bytes
       S devAddr W regAddr ... Sr devAddr R A A A ... N P - use this to read n bytes, the number is defined by the number of A for ack you add + 1 for a Nak 
       */
    if ( ! iterator.hasNext() ) 
    {
        txSize = 0;
        return false; /* no real command found */
    }

    qDebug() << command;

    QString start( I2C_START );
    /* 1. start */
    if ( start.compare( iterator.next().toLatin1(), Qt::CaseInsensitive ) )
    {
        QMessageBox::critical( 0, "Illegal command", "Command does not have correct start pattern" );
        txSize = 0;
        return false;
     }

    do 
    {
        /* 2. device address */
        if ( ! iterator.hasNext() ) /* no complete command found */
        {
            QMessageBox::critical( 0, "Illegal command", "Command too short - no device address found");
            txSize = 0;
            return false;
        }
        bool success;
        const QString & temp = iterator.next();
        deviceAddress = static_cast< unsigned char >( temp.toUInt( &success, 16 ) );
        if ( ! success || deviceAddress > 0x7F )
        {
            deviceAddress = 0;
            QMessageBox::critical( 0, "Illegal command", "Malformed device address (a device address must be a 7-bit value) in Hex");
            txSize = 0;
            return false;
        }
       
        /* 3. direction R/W */
        if ( ! iterator.hasNext() ) /* no complete command found */
        {
            QMessageBox::critical( 0, "Illegal command", "Command too short - no direction indication found (either I2C_READ for read or I2C_WRITE for write)");
            txSize = 0;
            return false;
        }
        QString direction( iterator.next().toLatin1() );
        if ( direction.compare( QString( I2C_WRITE ), Qt::CaseInsensitive) == 0 )
        { /* this is a write */
            /* 4. for write Bytes, Stop or Repeated Start should follow */
            while ( iterator.hasNext() && ! endOfCommand && ! repeatedStart )
            {
                const QString & next = iterator.next();
                unsigned char value = static_cast<unsigned char >( next.toUInt( &success, 16) );
                if ( !success ) /* must be a token like I2C_STOP or I2C_REPEATED_START */
                {
                    QString token( next.toLatin1() );
                    if ( token.compare( QString( I2C_STOP ), Qt::CaseInsensitive ) == 0 )
                    {
                        endOfCommand = true;
                    }
                    else if ( token.compare( QString( I2C_REPEATED_START ), Qt::CaseInsensitive ) == 0 
                        || token.compare( QString( I2C_START ), Qt::CaseInsensitive ) == 0 /* also accept I2C_START as a repeated-Start */
                        )
                    {
                        repeatedStart = true;
                    }
                    else
                    {
                        QMessageBox::critical( 0, "Illegal command", "Malformed command too short - after direction indication an illegal character found");
                        txSize = 0;
                        return false;
                    }
                }
                else /* a single byte found */
                {
                    if ( toTx < txSize )
                    {
                        tx[ toTx ] = value;
                        ++toTx; 
                    }
                    else
                    {
                        QMessageBox::critical( 0, "Illegal command", "Command is too long");
                        txSize = 0;
                        return false;
                    }
                }
            } /* end while ( ... ) */
        }
        else if ( direction.compare( QString( I2C_READ ), Qt::CaseInsensitive ) == 0 )
        { /* this is a read */
            /* 4. for read only Acks or Naks and Stop should follow */
            while ( iterator.hasNext() && ! endOfCommand )
            {
                const QString & token = iterator.next().toLatin1();
                if (  token.compare( QString( I2C_ACK ), Qt::CaseInsensitive ) == 0 
                   || token.compare( QString( I2C_NAK ), Qt::CaseInsensitive ) == 0 
                   )
                {
                    rxSize++; /* read in another byte - one ACK or NAK indicates another byte to be read */
                }
                else if ( token.compare( QString( I2C_STOP ), Qt::CaseInsensitive ) == 0 )
                {
                    endOfCommand = true;
                }
                else
                {
                    QMessageBox::critical( 0, "Illegal command", "Malformed command a read command may only contain Acks/Naks or Stop patterns after the device address");
                    txSize = 0;
                    return false;
                }
            }
        }
        else 
        {
            QMessageBox::critical( 0, "Illegal command", "Malformed transfer direction or no direction indication found (either I2C_READ for read or I2C_WRITE for write)");
            txSize = 0;
            return false;
        }
    } while ( ! endOfCommand && iterator.hasNext() );
    if ( ! endOfCommand )
    {
        QMessageBox::critical( 0, "Illegal command", "Malformed end of command must be a stop pattern");
        txSize = 0;
        return false;
    }
    txSize = toTx;
    return true;
}

void AmsI2CSPIInterpreter::fillInI2CResult ( const QString & command, QString & result, unsigned char rxSize, unsigned char * rx )
{
    QStringList cmd = command.split( ' ', QString::SkipEmptyParts );
    QStringListIterator iterator( cmd );
    unsigned char filledIn = 0;

    result = command;
    result.append( I2C_SEPERATOR );
    while ( iterator.hasNext() ) 
    {
        QString token( iterator.next().toLatin1() );
        if (  token.compare( QString( I2C_ACK ), Qt::CaseInsensitive ) == 0 
           || token.compare( QString( I2C_NAK ), Qt::CaseInsensitive ) == 0 
           )
        {   /* replace Ack and Nak by the received values */
            if ( filledIn < rxSize )
            {
                QString value;
                value.setNum( rx[ filledIn ], 16);
                if ( value.size() < 2 )
                {
                    value.prepend( "0" );
                }
                result.append( value );
                filledIn++; /* added another value */
            }
            else
            {
                result.append( "??" ); /* no byte received */
            }
        }
        else
        {
            result.append( token );
        }
        result.append( " " );
    }
    while ( filledIn < rxSize )
    { /* more bytes received than requested */
        QString value;
        value.setNum( rx[ filledIn ], 16);
        if ( value.size() < 2 )
        {
            value.prepend( "0" );
        }
        result.append( "?" ).append( value ).append( "?" );
        filledIn++; /* added another value */
    }
    qDebug() << result;
}

bool AmsI2CSPIInterpreter::interpretSPICommand( const QString & command, unsigned char & txSize, unsigned char * tx )
{
    unsigned char toTx = 0;
    unsigned char data;
    QStringList cmd = command.split( ' ', QString::SkipEmptyParts );
    QStringListIterator iterator( cmd );

    if ( cmd.size() > txSize ) /* some check that we can store the whole command in the buffer */
    {
        QMessageBox::critical( 0, "Illegal command", "Command is too long");
        txSize = 0;
        return false;
    }

    /* command structure is as follows:
       tx0 tx1 tx2 ... txN - use this to write and read n bytes
       */
    qDebug() << command;

    while ( iterator.hasNext() )
    {
        bool success;
        data = static_cast< unsigned char >( iterator.next().toUInt( &success, 16 ) );
        if ( ! success  )
        {
            QString msg( "Malformed data byte at position " );
            msg.append( "%1" ).arg( toTx );
            QMessageBox::critical( 0, "Illegal command",  msg );
            txSize = 0;
            return false;
        }
        else /* a single byte found */
        {
            if ( toTx < txSize )
            {
                tx[ toTx ] = data;
                ++toTx; 
            }
            else
            {
                QMessageBox::critical( 0, "Illegal command", "Command is too long");
                txSize = 0;
                return false;
            }
        }
    } 

    txSize = toTx;
    return true;
}

void AmsI2CSPIInterpreter::fillInSPIResult ( const QString & command, QString & result, unsigned char rxSize, unsigned char * rx )
{
    QStringList cmd = command.split( ' ', QString::SkipEmptyParts );
    QStringListIterator iterator( cmd );
    unsigned char filledIn = 0;

    result = command;
    result.append( SPI_SEPERATOR );
    while ( filledIn < rxSize )
    {
        /* insert receive byte */
        QString value;
        value.setNum( rx[ filledIn ], 16);
        if ( value.size() < 2 )
        {
            value.prepend( "0" );
        }
        if ( filledIn > cmd.size() )
        {   /* more bytes received than sent */
            result.append( "?" ).append( value ).append( "?" );
        }
        else
        {
            result.append( value );
        }
        result.append( " " );
        filledIn++; /* added another value */
    }
    while ( filledIn < cmd.size() )
    {
        /*  received too less bytes */
        result.append( "?? " );
        filledIn++; /* added another value */
    }

#if 0 /* mar, 2013/10/28: after discussion with uhe we changed the output format to the following:
         tx0 tx1 tx2 ... txN -> rx0 rx1 rx2 ... rxN
         */
    /* command structure is as follows:
       tx0 tx1 tx2 ... txN - use this to write and read n bytes
       result structure is as follows:
       tx0/rx0 tx1/rx1 tx2/rx2 ... txN/rxN
       */
    result.clear();
    while ( iterator.hasNext() ) 
    {
        result.append( iterator.next() );
        result.append( SPI_SEPERATOR );
        /* insert receive byte */
        if ( filledIn < rxSize )
        {
            QString value;
            value.setNum( rx[ filledIn ], 16);
            if ( value.size() < 2 )
            {
                value.prepend( "0" );
            }
            result.append( value );
            result.append( " " );
            filledIn++; /* added another value */
        }
        else /*  received too less bytes */
        {
            result.append( "??" );
        }
    }
#endif

    qDebug() << result;
}
