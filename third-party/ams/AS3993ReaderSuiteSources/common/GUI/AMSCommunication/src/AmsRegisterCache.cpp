
/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author R. Veigl
 *
 *  \brief  AmsRegisterCache class implementation
 *
 *  AmsRegisterCache is a class used for caching values
 *  which can be written to the device's memory as once.
 */


#include "stable.h"
#include "AmsRegisterCache.hxx"


uchar getMaskOffset(unsigned char mask);

#pragma region Construction

AmsRegisterCache::AmsRegisterCache(AMSCommunication *com) : itsIsComBlocked(false)
{	
    itsCom = com;

	itsRegisterMap = QMap<unsigned char, unsigned char>();

    /*for(unsigned char i = 0; i < 0xFF; i++)
    itsRegisterMap.insert(i, 0x00);

    itsRegisterMap.insert(0xFF, 0x00);*/

    QObject::connect(itsCom, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char)), this, SLOT(on_com_dataChanged(unsigned char, unsigned char, bool, unsigned char)));
    QObject::connect(itsCom, SIGNAL(dataChanged(unsigned char, unsigned char, bool, unsigned char, unsigned char, unsigned char, int, QString)), this, SLOT(on_com_dataChanged(unsigned char, unsigned char, bool, unsigned char, unsigned char, unsigned char, int, QString)));    
}

AmsRegisterCache::~AmsRegisterCache(void)
{
}
#pragma endregion Construction


void AmsRegisterCache::setConnectionProperties ( void* properties ) 
{
    AmsRegisterCacheProperties * props = reinterpret_cast<AmsRegisterCacheProperties *>( properties );
    
    itsRegisterMap.clear();

    foreach(unsigned char registerAddress, props->itsAvailableRegisterAddresses)
        itsRegisterMap.insert(registerAddress, 0x00);
}

AMSCommunication::Error AmsRegisterCache::hwReadRegister( unsigned char reg, unsigned char *val )
{
    if(getRegisterValue(reg, 0xFF, *val) == 0)
        return AMSCommunication::NoError;

    return AMSCommunication::ReadError;
}

AMSCommunication::Error AmsRegisterCache::hwWriteRegister( unsigned char reg, unsigned char val )
{
    if(setRegisterValue(reg, 0xFF, val) == 0)
        return AMSCommunication::NoError;

    return AMSCommunication::WriteError;
}

AMSCommunication::Error AmsRegisterCache::hwSendCommand( QString command, QString * answer )
{
    return AMSCommunication::WriteError; // not implemented
}

AMSCommunication::Error AmsRegisterCache::hwConnect()
{
    return AMSCommunication::NoError;
}

void AmsRegisterCache::hwDisconnect()
{

}

void AmsRegisterCache::on_com_dataChanged(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value)
{

}

void AmsRegisterCache::on_com_dataChanged(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char mask, unsigned char value, unsigned char access, int err, QString any)
{

}





#pragma region RegisterAccess
int AmsRegisterCache::setRegisterValue(unsigned char address, unsigned char mask, unsigned char value)
{

#ifdef _DEBUG
	if(value > (mask >> getMaskOffset(mask)))
		qDebug() << "value is greater than mask.";
#endif

	if(itsRegisterMap.contains(address))
	{
		unsigned char regEntry = itsRegisterMap.value(address);
		itsRegisterMap[address] = ((regEntry & ~mask) | ((value << getMaskOffset(mask)) & mask));
		
		unsigned char tmpMask = mask;
		if(itsChanges.contains(address))
			tmpMask |= itsChanges.value(address);
		itsChanges[address] = tmpMask; //set the changed bits
		return 0;
	}

	qDebug() << QString("address '0x%1' not valid.").arg(address, 2, 16);
	return -1;	
}


int AmsRegisterCache::getRegisterValue(unsigned char address, unsigned char mask, unsigned char &value)
{
	if(itsRegisterMap.contains(address))
	{
		value = (itsRegisterMap.value(address) & mask) >> getMaskOffset(mask);

		return 0;
	}
	
	return -1;
}

int AmsRegisterCache::getRegisterValue(unsigned char address, unsigned char mask, bool &value)
{
	unsigned char tmpValue;
	int ret = getRegisterValue(address, mask, tmpValue);
	if(ret != 0)
		return ret;
	
	value = (bool)tmpValue;
	return 0;	
}

void AmsRegisterCache::blockCommunication(bool block)
{
	itsIsComBlocked = block;
}

bool AmsRegisterCache::copyFrom(AmsRegisterCache* source, const unsigned char startAddress, const unsigned char mask, const int length)
{
	if(NULL == source)
		return false;
	
	unsigned char addr = 0;
	unsigned char value = 0;

	for(int i = 0; i < length; i++)
	{
		addr = startAddress + i;
		if(source->getRegisterValue(addr, mask, value) == 0)
		{
			if(setRegisterValue(addr, mask, value) != 0)
				return false;
		}
		else 
		{
			return false;
		}
	}

	return true;
}


bool AmsRegisterCache::copyFrom(AmsRegisterCache* source)
{
	if(NULL == source)
		return false;

	foreach(unsigned char addr, source->getAddresses())
	{
		unsigned char value;
		if(source->getRegisterValue(addr, 0xFF, value) == 0)
			setRegisterValue(addr, 0xFF, value);
		else 
			return false;
	}

	return true;
}


void AmsRegisterCache::loadFromDevice(bool disableMask)
{ 
	foreach(unsigned char key, itsRegisterMap.keys())
	{
		unsigned char value;
		itsCom->readRegister(key, &value, false, 99);

		unsigned char origVal = value;

		if(!disableMask && itsReadMask.contains(key))
			value = (value & itsReadMask.value(key));

		setRegisterValue(key, 0xFF, value);
		//qDebug() << QString("load from device: reg 0x%1: 0x%2 <-> 0x%3").arg(key, 0, 16).arg(value, 0, 16).arg(origVal, 0, 16);
	}
    
    foreach(unsigned char key, itsRegisterMap.keys())
    {
        unsigned char value;
        getRegisterValue(key, 0xFF, value);
        emit dataChanged(key, 0x00, false, value);
    }
}

bool AmsRegisterCache::writeToDevice(bool disableMask)
{
	const QChar prefillChar = '0';
	if(itsIsComBlocked)
	{
		qDebug() << "communication blocked\n";
		return false;
	}
    
	bool result = true;

	foreach(unsigned char key, itsChanges.keys())
	{
		if(itsRegisterMap.contains(key))
		{
			if(FALSE == disableMask && FALSE == itsWriteMask.contains(key))
				continue; // don't write unmasked parts

			int cnt = 0;
			AMSCommunication::Error err = AMSCommunication::ConnectionError;
			while(err != AMSCommunication::NoError && cnt < 5)
			{
				unsigned char maskedValue = itsRegisterMap.value(key);
				
				if(!disableMask)
				{
					unsigned char writeMask = itsWriteMask.value(key);
					unsigned char readValue = 0x00;
					maskedValue &= writeMask;
					
					err = itsCom->readRegister(key, &readValue, false);
					
					unsigned char writeValue = maskedValue | (readValue & (~writeMask));
					if(err == 0)
					{
						err = itsCom->writeRegister(key, writeValue, false, true);
						//qDebug() << QString("write to device: reg 0x%1: 0x%2").arg(key, 0, 16).arg(writeValue, 0, 16);
						//err = com->modifyRegister(key, itsChanges.value(key), maskedValue, false);
					}
				}
				else
				{
					unsigned char readValue = 0x00;
					err = itsCom->readRegister(key, &readValue, false);

					unsigned char writeValue = maskedValue;
					if(err == 0)
					{
						//SleepThread::msleep(10);
						err = itsCom->writeRegister(key, writeValue, false, true);
						//qDebug() << QString("write to device: reg 0x%1: 0x%2").arg(key, 0, 16).arg(writeValue, 0, 16);
						//err = com->modifyRegister(key, itsChanges.value(key), maskedValue, false);
					}

					//err = com->modifyRegister(key, itsChanges.value(key), maskedValue, false);
					//err = com->writeRegister(key, maskedValue, false);
				}				

				if(err != 0)
					qDebug() << "Communication error: " << err << endl;

				//if(cnt > 0)
				//SleepThread::msleep(100);

				cnt++;				
			}
			
			if(err != 0)
				result = false;
						
			//qDebug() << QString("0x%1 : 0x%2 (%3) mask:%4").arg(key, 2, 16, prefillChar).arg(itsRegisterMap.value(key), 2, 16, prefillChar).arg(itsRegisterMap.value(key), 8, 2, prefillChar).arg(itsChanges.value(key), 8, 2, prefillChar);
		}
	};

	itsChanges.clear();

	return result;
}

#pragma endregion RegisterAccess



QList<unsigned char> AmsRegisterCache::getAddresses()
{
	return itsRegisterMap.keys();
}

QList<unsigned char> AmsRegisterCache::getChangedRegisters()
{
	return itsChanges.keys();
}



#pragma region HelperMethods

uchar getMaskOffset(unsigned char mask)
{
	int offset = 0;

	while((mask & 0x1) == 0 && offset < 8)
	{
		offset++;
		mask >>= 1;
	}

	return offset;
}

#pragma endregion HelperMethods















AmsRegisterCacheProperties::AmsRegisterCacheProperties()
{

}

AmsRegisterCacheProperties::AmsRegisterCacheProperties( unsigned char startRegister, unsigned char count )
{    
    for(int i = 0; i <= count; i++)
    {
        itsAvailableRegisterAddresses.append((unsigned char)(startRegister + i));
    }
}

AmsRegisterCacheProperties::~AmsRegisterCacheProperties()
{

}
