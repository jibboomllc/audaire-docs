/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams Streaming Communication
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file CtrlComObjects.cpp
 *
 *  \author M. Arpa
 *
 *  \brief Implementation of the classes that implement control commands.
 */

#include "AmsCom.h" 
#include "AmsComObject.h"
#include "ApplComObjects.h"
#include "CtrlComObjects.h"
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

/* define tags not to be used or interpreted outside this file */
#define AMS_COM_XML_RESET_TAG                   "reset"
#define AMS_COM_XML_FIRMWARE_INFORMATION_TAG    "firmwareinformation"
#define AMS_COM_XML_ENABLE_BOOTLOADER_TAG       "enablebootloader"
#define AMS_COM_XML_FIRMWARE_NUMBER_TAG         "firmwarenumber"
#define AMS_COM_XML_RESET_OBJECTS               "objects"
#define AMS_COM_XML_FIRMWARE_INFORMATION        "data"
#define AMS_COM_XML_RELEASE_MAJOR               "major"
#define AMS_COM_XML_RELEASE_MINOR               "minor"
#define AMS_COM_XML_RELEASE_MARKER              "marker"

#define AMS_COM_XML_CONFIG_OBJECT_TAG           "config"
#define AMS_COM_XML_CONFIG_ADDRESS              "address"
#define AMS_COM_XML_CONFIG_DATA                 "data"
#define AMS_COM_XML_CONFIG_MASK                 "mask"


AmsComObject * CtrlComXmlReader::clone( QXmlStreamReader * xml, unsigned char protocol )
{
    AmsComObject * p = 0;
    if ( xml && ! xml->atEnd() && ! xml->hasError() )
    {
        /* sanity check - we are at a telegram */
        if ( xml->isStartElement() && xml->name() == AMS_COM_XML_TAG )
        {
            if ( xml->readNextStartElement( ) ) /* should be the command itself */
            {
                if ( xml->name() == AMS_COM_XML_APPLICATION_TAG ) 
                {
                    p = ApplComXmlReader::clone( xml, protocol );
                }
                else if ( xml->name() == AMS_COM_XML_RESET_TAG )
                {
                    p = new ResetObject( 0 );
                }
                else if ( xml->name() == AMS_COM_XML_FIRMWARE_INFORMATION_TAG )
                {
                    p = new GetFirmwareInfoObject( );
                }
                else if ( xml->name() == AMS_COM_XML_FIRMWARE_NUMBER_TAG )
                {
                    p = new FirmwareNumberObject( );
                }
                else if ( xml->name() == AMS_COM_XML_ENABLE_BOOTLOADER_TAG )
                {
                    p = new EnterBootloaderObject( );
                }
            }
        }
    }
    return p;
}


bool ResetObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_WRITE );
	if ( bufferSize >= itsSerialSize )
	{
		result = true;
	    buffer[ 0 ] = itsObjectsToReset; /* which devices to reset */
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_RESET_TAG );
            xml->writeTextElement( AMS_COM_XML_RESET_OBJECTS, QString::number( itsObjectsToReset, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeEndElement( );
        }
    }
    writeClosingTag( xml );
	return result;
}

 
bool ResetObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                QString value = xml->readElementText();
                int val = value.toInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_RESET_OBJECTS )
                { /* well formed reset */
                    itsObjectsToReset = static_cast< unsigned char >( val );
                    result = true;
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
    }
    return result;
}

GetFirmwareInfoObject::GetFirmwareInfoObject ( unsigned int tracePattern )
	: AmsComObject( AMS_COM_CTRL_CMD_FW_INFORMATION, 0, 0, AMS_STREAM_SHORT_STRING, tracePattern )
{
	memset( itsData, '\0', AMS_STREAM_SHORT_STRING );
}

GetFirmwareInfoObject::GetFirmwareInfoObject ( const GetFirmwareInfoObject & other )
	: AmsComObject( other )
{
	memcpy( itsData, other.itsData, AMS_STREAM_SHORT_STRING );
}


bool GetFirmwareInfoObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ );
	if ( bufferSize >= itsRxSerialSize )
	{
		result = true;
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_FIRMWARE_INFORMATION_TAG );
            xml->writeEndElement( );
        }
    }
    writeClosingTag( xml );
	return result;
}


bool GetFirmwareInfoObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = true;
    writeOpeningTag( xml, AMS_COM_XML_READ_RESULT );
    memcpy( itsData, buffer, ((bufferSize > AMS_STREAM_SHORT_STRING) ? AMS_STREAM_SHORT_STRING : bufferSize) );
    /* terminate the string is all cases */
    itsData[ AMS_STREAM_SHORT_STRING - 1 ] = '\0'; 
    if ( xml )
    {
        xml->writeStartElement( AMS_COM_XML_FIRMWARE_INFORMATION_TAG );
        char * p = reinterpret_cast< char *>( itsData );
        xml->writeTextElement( AMS_COM_XML_FIRMWARE_INFORMATION, p );
        xml->writeEndElement( );
    }
    writeClosingTag( xml );
	return result;
}

bool GetFirmwareInfoObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                if ( xml->name() == AMS_COM_XML_FIRMWARE_INFORMATION )
                { /* well formed info */
                    QString value = xml->readElementText();
                    memcpy( itsData, value.toLocal8Bit(), value.size() );
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
        result = true;
    }
    return result;
}


FirmwareNumberObject::FirmwareNumberObject ( unsigned int tracePattern )
	: AmsComObject( AMS_COM_CTRL_CMD_FW_NUMBER, 0, 0, 3, tracePattern )
	, itsVersionNumber( 0 )
{
}

FirmwareNumberObject::FirmwareNumberObject ( const FirmwareNumberObject & other )
	: AmsComObject( other )
	, itsVersionNumber( other.itsVersionNumber )
{
}


bool FirmwareNumberObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ );
	if ( bufferSize >= itsRxSerialSize )
	{
		result = true;
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_FIRMWARE_NUMBER_TAG );
            xml->writeEndElement( );
        }
    }
    writeClosingTag( xml );
	return result;
}

bool FirmwareNumberObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ_RESULT );
    itsVersionNumber = 0;
    /* check if this is a response to cmd AMS_COM_CTRL_CMD_FW_NUMBER */
    if ( bufferSize >= itsDeserialSize )
    {
		result = true;
		itsVersionNumber = buffer[0]; /* major number */
		itsVersionNumber = (itsVersionNumber << 8) | buffer[1]; /* minor number */
		itsVersionNumber = (itsVersionNumber << 8) | buffer[2]; /* release marker */
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_FIRMWARE_NUMBER_TAG );
            xml->writeTextElement( AMS_COM_XML_RELEASE_MAJOR, QString::number( buffer[0], AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_RELEASE_MINOR, QString::number( buffer[1], AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_RELEASE_MARKER, QString::number( buffer[2], AMS_COM_XML_NUMBER_BASE ) );
            xml->writeEndElement( );
        }
    }
    writeClosingTag( xml );
	return result;
}

bool FirmwareNumberObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                QString value = xml->readElementText();
                int val = value.toInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_RELEASE_MAJOR )
                { /* well formed info */
            		itsVersionNumber = ( 0x00FFFF & itsVersionNumber ) | ( ( val << 16 ) & 0xFF0000 ); /* major number */
                }
                else if ( xml->name() == AMS_COM_XML_RELEASE_MINOR )
                {
            		itsVersionNumber = ( 0xFF00FF & itsVersionNumber ) | ( ( val <<  8 ) & 0xFF00 ); /* minor number */
                }
                else if ( xml->name() == AMS_COM_XML_RELEASE_MARKER )
                {
            		itsVersionNumber = ( 0xFFFF00 & itsVersionNumber ) | (   val         & 0xFF ); /* release marker */
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
        result = true;
    }
    return result;
}

bool EnterBootloaderObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_WRITE );
	if ( bufferSize >= itsSerialSize )
	{
		result = true;
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_ENABLE_BOOTLOADER_TAG );
            xml->writeEndElement( );
        }
    }
    writeClosingTag( xml );
	return result;
}

bool EnterBootloaderObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        xml->skipCurrentElement();
        result = true;
    }
    return result;
}


AmsConfigObject::AmsConfigObject ( unsigned short address, unsigned short data, unsigned short mask, unsigned int tracePattern )
    : AmsComObject( AMS_COM_CONFIG, 6, 2, 2, tracePattern )
    , itsAddress( address )
    , itsMask( mask )
    , itsData( itsMask & data )
{
}

bool AmsConfigObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_WRITE );
    if ( bufferSize >= itsSerialSize )
    {
        if ( serialising( buffer, 2, itsAddress ) )
        {
            buffer += 2;
            if ( serialising( buffer, 2, itsMask ) )
            {
                buffer += 2;
                if ( serialising( buffer, 2, itsData ) )
                {
                    buffer += 2;
                    result = true;
                    if ( xml )
                    {
                        xml->writeStartElement( AMS_COM_XML_CONFIG_OBJECT_TAG );
                        xml->writeTextElement( AMS_COM_XML_CONFIG_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
                        xml->writeTextElement( AMS_COM_XML_CONFIG_MASK,    QString::number( itsMask, AMS_COM_XML_NUMBER_BASE ) );
                        xml->writeTextElement( AMS_COM_XML_CONFIG_DATA,    QString::number( itsData, AMS_COM_XML_NUMBER_BASE ) );
                        xml->writeEndElement( );
                    }
                }
            }
        }
    }
    writeClosingTag( xml );
    return result;
}

bool AmsConfigObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ );
    if ( bufferSize >= itsRxSerialSize )
    {
        if ( serialising( buffer, 2, itsAddress ) )
        {
            buffer += 2;
            result = true;
            if ( xml )
            {
                xml->writeStartElement( AMS_COM_XML_CONFIG_OBJECT_TAG );
                xml->writeTextElement( AMS_COM_XML_CONFIG_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_CONFIG_MASK,    QString::number( itsMask, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeEndElement( );
            }
        }
    }
    writeClosingTag( xml );
    return result;
}

bool AmsConfigObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ_RESULT );
    if ( bufferSize >= itsDeserialSize )
    {
        unsigned int value;
        if ( deserialising( buffer, 2, value ) )
        {
            result = true;
            itsData = value & itsMask;
            if ( xml )
            {
                xml->writeStartElement( AMS_COM_XML_CONFIG_OBJECT_TAG );
                xml->writeTextElement( AMS_COM_XML_CONFIG_ADDRESS, QString::number( itsAddress, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_CONFIG_MASK,    QString::number( itsMask, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_CONFIG_DATA,    QString::number( itsData, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeEndElement( );
            }
        }
    }
    writeClosingTag( xml );
    return result;
}

bool AmsConfigObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        itsData = 0;
        itsMask = 0xFFFF;
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                QString value = xml->readElementText();
                int val = value.toInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_CONFIG_ADDRESS )
                { /* well formed info */
                    itsAddress = val;
                }
                else if ( xml->name() == AMS_COM_XML_CONFIG_DATA )
                {
                    itsData = val;
                }
                else if ( xml->name() == AMS_COM_XML_CONFIG_MASK )
                {
                    itsMask = val;
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
        itsData = itsData & itsMask; 
        result = true;
    }
    return result;
}
