/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams Streaming Communication
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file 
 *
 *  \author F. Lobmaier,
 *          M. Arpa
 *
 *  \brief Implementation of the classes that use spi as protocol. E.g. to
 * read/write registers, block-read/write. 
 */

#include "SpiComObjects.h"
#include "AmsCom.h" 
#include <QXmlStreamWriter>
#include <QString>
#include <QByteArray>

#define AMS_COM_XML_REGISTER_TAG    "register"
#define AMS_COM_XML_BLOCK_TAG       "block"
#define AMS_COM_XML_CONFIG_TAG      "config"
#define AMS_COM_XML_FLEX_TAG        "flex"

#define AMS_COM_XML_SLAVE_ADDRESS_SIZE  "slaveaddesssize"
#define AMS_COM_XML_SLAVE_ADDRESS       "slaveaddress"
#define AMS_COM_XML_ADDRESS_SIZE        "addresssize"
#define AMS_COM_XML_ADDRESS             "address"
#define AMS_COM_XML_DATA_SIZE           "datasize"
#define AMS_COM_XML_DATA                "data"
#define AMS_COM_XML_TX_DATA             "txdata"
#define AMS_COM_XML_RX_DATA             "rxdata"

#define AMS_COM_XML_MAX_BLOCK_SIZE      "maxblocksize"
#define AMS_COM_XML_BLOCK_SIZE          "blocksize"

#define AMS_COM_XML_MODULE              "module"
#define AMS_COM_XML_DEVICE              "device"
#define AMS_COM_XML_POLARITY            "polarity"
#define AMS_COM_XML_PHASE               "phase"
#define AMS_COM_XML_CLOCK_FREQUENCY     "clock"

AmsComObject * SpiComXmlReader::clone( QXmlStreamReader * xml, unsigned char protocol )
{
    AmsComObject * p = 0;
    if ( xml && ! xml->atEnd() && ! xml->hasError() )
    {
        /* sanity check - we are at a telegram */
        if ( xml->isStartElement() && xml->name() == AMS_COM_XML_TAG )
        {
            if ( xml->readNextStartElement() )
            {
                if ( xml->name() == AMS_COM_XML_REGISTER_TAG )
                {
                    p = new SpiRegisterObject( 0, 0, 0, 0, 0, 0 );
                }
                else if ( xml->name() == AMS_COM_XML_BLOCK_TAG )
                {
                    /* FIXME not there yet p = new SpiBlockObject( 0, 0 ); */
                }
                else if ( xml->name() == AMS_COM_XML_CONFIG_TAG )
                {
                    p = new SpiConfigObject( );
                }
                else if ( xml->name() == AMS_COM_XML_FLEX_TAG )
                {
                    p = new SpiFlexObject( );
                }
            }
        }
    }
    return p;
}

SpiRegisterObject::SpiRegisterObject 
	( unsigned int slaveReadAddress
    , unsigned int slaveWriteAddress
    , int slaveAddressSize
    , unsigned int readRegisterAddress
    , unsigned int writeRegisterAddress
	, unsigned int data
	, int addressSize
	, int dataSize 
	) : AmsComObject
        ( AMS_COM_SPI
        , addressSize + dataSize
        , addressSize
	    , dataSize
	    )
    , itsSlaveAddressSize( slaveAddressSize )
	, itsAddressSize( addressSize )
	, itsDataSize( dataSize )
    , itsSlaveReadAddress( slaveReadAddress )
    , itsSlaveWriteAddress( slaveWriteAddress )
	, itsReadAddress( readRegisterAddress )
    , itsWriteAddress( writeRegisterAddress )
	, itsData( data ) 
{ 
}


SpiRegisterObject::SpiRegisterObject 
	( const SpiRegisterObject & other 
	) : AmsComObject( other )
    , itsSlaveAddressSize( other.itsSlaveAddressSize )
	, itsAddressSize( other.itsAddressSize )
	, itsDataSize( other.itsDataSize )
    , itsSlaveReadAddress( other.itsSlaveReadAddress )
    , itsSlaveWriteAddress( other.itsSlaveWriteAddress )
	, itsReadAddress( other.itsReadAddress )
	, itsWriteAddress( other.itsWriteAddress )
	, itsData( other.itsData ) 
{ 
}

	
bool SpiRegisterObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                int val = xml->readElementText().toInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_SLAVE_ADDRESS_SIZE )
                {
                    result = true;
                    itsSlaveAddressSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_SLAVE_ADDRESS )
                {
                    result = true;
                    itsSlaveReadAddress = val;
                    itsSlaveWriteAddress = val;
                }
                else if ( xml->name() == AMS_COM_XML_ADDRESS_SIZE )
                {
                    result = true;
                    itsAddressSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_ADDRESS )
                {
                    result = true;
                    itsReadAddress = val;
                    itsWriteAddress = val;
                }
                else if ( xml->name() == AMS_COM_XML_DATA_SIZE )
                {
                    result = true;
                    itsDataSize = val;
                }
                else if ( xml->name() == AMS_COM_XML_DATA )
                {
                    result = true;
                    itsData = val;
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
    }
    return result;
}


bool SpiRegisterObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;

    writeOpeningTag( xml, AMS_COM_XML_WRITE );
	if ( bufferSize >= itsSerialSize )
	{
        if ( serialising( buffer, itsSlaveAddressSize, itsSlaveWriteAddress ) )
        {   
            buffer += itsSlaveAddressSize;
            if ( serialising( buffer, itsAddressSize, itsWriteAddress ) )
            {
                buffer += itsAddressSize;
                result = serialising( buffer, itsDataSize, itsData );
                if ( result && xml )
                {
                    xml->writeStartElement( AMS_COM_XML_REGISTER_TAG );
                    if ( itsSlaveAddressSize > 0 )
                    {
                        xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS_SIZE, QString::number( itsSlaveAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                        xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsSlaveWriteAddress, AMS_COM_XML_NUMBER_BASE ) );
                    }
                    xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsWriteAddress, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_DATA_SIZE, QString::number( itsDataSize, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_DATA, QString::number( itsData, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeEndElement( );
                }
            }
        }
    }
    writeClosingTag( xml );
    return result;
}
	

bool SpiRegisterObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ );
	if ( bufferSize >= itsRxSerialSize )
	{
        if ( serialising( buffer, itsSlaveAddressSize, itsSlaveReadAddress ) )
        {
            buffer += itsSlaveAddressSize;
            result = serialising( buffer, itsAddressSize, itsReadAddress );
            if ( result && xml )
            {
                xml->writeStartElement( AMS_COM_XML_REGISTER_TAG );
                if ( itsSlaveAddressSize > 0 )
                {
                    xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS_SIZE, QString::number( itsSlaveAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                    xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsSlaveReadAddress, AMS_COM_XML_NUMBER_BASE ) );
                }
                xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsReadAddress, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_DATA_SIZE, QString::number( itsDataSize, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeEndElement( );
            }
        }
    }
    writeClosingTag( xml );
    return result;
}
	
bool SpiRegisterObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ_RESULT );
	if ( bufferSize >= itsDeserialSize )
	{	/* buffer[ 0 ] = begin of data */
        result = deserialising( buffer, itsDataSize, itsData );
        if ( result && xml )
        {
            xml->writeStartElement( AMS_COM_XML_REGISTER_TAG );
            if ( itsSlaveAddressSize > 0 )
            {
                xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS_SIZE, QString::number( itsSlaveAddressSize, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_SLAVE_ADDRESS, QString::number( itsSlaveReadAddress, AMS_COM_XML_NUMBER_BASE ) );
            }
            xml->writeTextElement( AMS_COM_XML_ADDRESS_SIZE, QString::number( itsAddressSize, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_ADDRESS, QString::number( itsReadAddress, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_DATA_SIZE, QString::number( itsDataSize, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeTextElement( AMS_COM_XML_DATA, QString::number( itsData, AMS_COM_XML_NUMBER_BASE ) );
            xml->writeEndElement( );
        }
	}
    writeClosingTag( xml );
    return result;
}


SpiConfigObject::SpiConfigObject 
	( unsigned char module
    , unsigned char deviceId
	, unsigned int clockFrequency
    , unsigned char clockPhase
    , unsigned char clockPolarity
	)
	: AmsComObject( AMS_COM_SPI_CONFIG, 8, 0, 0 )
	, itsModule( module )
    , itsDeviceId( deviceId )
	, itsClockFrequency( clockFrequency )
    , itsClockPhase( clockPhase )
    , itsClockPolarity( clockPolarity )
{
}


SpiConfigObject::SpiConfigObject 
	( const SpiConfigObject & other 
	)
	: AmsComObject( other )
	, itsModule( other.itsModule )
    , itsDeviceId( other.itsDeviceId )
	, itsClockFrequency( other.itsClockFrequency )
    , itsClockPhase( other.itsClockPhase )
    , itsClockPolarity( other.itsClockPolarity )
{
}

bool SpiConfigObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                int val = xml->readElementText().toInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_MODULE )
                {
                    result = true;
                    itsModule = val;
                }
                else if ( xml->name() == AMS_COM_XML_DEVICE )
                {
                    result = true;
                    itsDeviceId = val;
                }
                else if ( xml->name() == AMS_COM_XML_POLARITY )
                {
                    result = true;
                    itsClockPolarity = val;
                }
                else if ( xml->name() == AMS_COM_XML_PHASE )
                {
                    result = true;
                    itsClockPhase = val;
                }
                else if ( xml->name() == AMS_COM_XML_CLOCK_FREQUENCY )
                {
                    result = true;
                    itsClockFrequency = val;
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
    }
    return result;
}

bool SpiConfigObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;

    writeOpeningTag( xml, AMS_COM_XML_WRITE );
	if ( bufferSize >= serialiseSize() )
	{
        if ( serialising( buffer, 4, itsClockFrequency ) )
        {
            buffer += 4;
            buffer[ 0 ] = itsModule;
            buffer[ 1 ] = itsDeviceId;
            buffer[ 2 ] = itsClockPhase;
            buffer[ 3 ] = itsClockPolarity;
            result = true;
            if ( xml )
            {
                xml->writeStartElement( AMS_COM_XML_CONFIG_TAG );
                xml->writeTextElement( AMS_COM_XML_CLOCK_FREQUENCY, QString::number( itsClockFrequency, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_MODULE, QString::number( itsModule, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_DEVICE, QString::number( itsDeviceId, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_POLARITY, QString::number( itsClockPhase, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeTextElement( AMS_COM_XML_PHASE, QString::number( itsClockPolarity, AMS_COM_XML_NUMBER_BASE ) );
                xml->writeEndElement( );
            }
        }
    }
    writeClosingTag( xml );
    return result;
}

bool SpiConfigObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) 
{ 
	return false; 
}

bool SpiConfigObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) 
{ 
	return false; 
}

SpiFlexObject::SpiFlexObject ( unsigned int maxDataSize, const unsigned char * data , unsigned int dataSize )
    : AmsComObject( AMS_COM_SPI, dataSize, dataSize, dataSize )
    , itsMaxDataSize( maxDataSize )
    , itsTxData( new unsigned char[ maxDataSize ] )
    , itsRxData( new unsigned char[ maxDataSize ] )
    , itsDataSize( dataSize )
{
    memset( itsTxData, 0, maxDataSize ); /* prefill buffer with 0 */
    memset( itsRxData, 0, maxDataSize ); /* prefill buffer with 0 */
    if ( data && dataSize )
    {
        memcpy( itsTxData, data, dataSize );
    }
}

bool SpiFlexObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;

    writeOpeningTag( xml, AMS_COM_XML_WRITE );
    if ( bufferSize >= itsSerialSize )
    {
        memcpy( buffer, itsTxData, itsDataSize );
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_FLEX_TAG );
            xml->writeTextElement( AMS_COM_XML_DATA_SIZE, QString::number( itsDataSize, AMS_COM_XML_NUMBER_BASE ) );
            char * p = reinterpret_cast< char *>( itsTxData );
            xml->writeTextElement( AMS_COM_XML_TX_DATA, QString( QByteArray( p, itsDataSize ).toHex() ) );
            xml->writeEndElement( );
        }
        result = true;
    }
    writeClosingTag( xml );
    return result;
}

bool SpiFlexObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ );
    if ( bufferSize >= itsRxSerialSize )
    {
        memcpy( buffer, itsTxData, itsDataSize );
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_FLEX_TAG );
            xml->writeTextElement( AMS_COM_XML_DATA_SIZE, QString::number( itsDataSize, AMS_COM_XML_NUMBER_BASE ) );
            char * p = reinterpret_cast< char *>( itsTxData );
            xml->writeTextElement( AMS_COM_XML_TX_DATA, QString( QByteArray( p, itsDataSize ).toHex() ) );
            xml->writeEndElement( );
        }
        result = true;
    }
    writeClosingTag( xml );
    return result;
}

bool SpiFlexObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    bool result = false;
    writeOpeningTag( xml, AMS_COM_XML_READ_RESULT );
    { 
        result = true;
        if ( bufferSize > int( itsMaxDataSize ) )
        {
            bufferSize = itsMaxDataSize; /* ignore the rest of the data, as we cannot copy it into the internal buffer */
        }
        itsDataSize = bufferSize;
        memcpy( itsRxData, buffer, bufferSize ); 
        if ( xml )
        {
            xml->writeStartElement( AMS_COM_XML_FLEX_TAG );
            xml->writeTextElement( AMS_COM_XML_DATA_SIZE, QString::number( itsDataSize, AMS_COM_XML_NUMBER_BASE ) );
            char * p = reinterpret_cast< char *>( itsRxData );
            xml->writeTextElement( AMS_COM_XML_RX_DATA, QString( QByteArray( p, itsDataSize ).toHex() ) );
            xml->writeEndElement( );
        }
    }
    writeClosingTag( xml );
    return result;
}


bool SpiFlexObject::fill( QXmlStreamReader * xml )
{
    bool result = false;
    memset( itsTxData, 0, itsMaxDataSize ); /* clear memory */
    memset( itsRxData, 0, itsMaxDataSize ); /* clear memory */
    if ( xml )
    {
        xml->readNext();
        while ( xml->isStartElement() || xml->isWhitespace() )
        {
            if ( xml->isStartElement() )
            {
                QString value = xml->readElementText();
                unsigned int val = value.toUInt( & result, AMS_COM_XML_NUMBER_BASE );
                if ( xml->name() == AMS_COM_XML_DATA_SIZE )
                {
                    if ( val < itsMaxDataSize )
                    {
                        result = true;
                        itsDataSize = val;
                    }
                }
                else if ( xml->name() == AMS_COM_XML_TX_DATA )
                {
                    result = true;
                    unsigned int i;
                    for ( i = 0; i < itsDataSize; ++i )
                    {
                        itsTxData[ i ] = static_cast< unsigned char >( value.mid( i*2, 2 ).toUShort( 0, AMS_COM_XML_NUMBER_BASE ) );
                    }
                }
                else if ( xml->name() == AMS_COM_XML_RX_DATA )
                { /* data read in from device -> we dump this */
                    result = true; 
                }
            }
            xml->readNext();
        }
        xml->skipCurrentElement();
    }
    return result;
}

bool SpiFlexObject::set( const unsigned char * data, unsigned int dataSize )
{
    if ( dataSize < itsMaxDataSize )
    {
        itsDataSize = dataSize;
        memcpy( itsTxData, data, dataSize );
        memset( itsTxData + itsDataSize, 0, itsMaxDataSize - itsDataSize ); /* clear rest of memory */
        memset( itsRxData, 0, itsMaxDataSize ); /* clear receive part */
        return true;
    }
    return false;
}
