/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsFirmwareCheck.cpp
 *
 *  \author M. Arpa
 *
 *  \brief  This class provides a means to check if your GUI and firmware are
 * compatible according to ams firmware compatible rules.
 */


/* ------------------------------ includes -------------------------------------------- */

#include "AmsFirmwareCheck.h"
#include <QStringList>

/* ------------------------------ defines --------------------------------------------- */



/* ------------------------------ class implementations ------------------------------- */

unsigned int AmsFirmwareCheck::convert( const QString & fromVersion, bool & success )
{
	QStringList fromVersionNumbers = fromVersion.split( "." );
	unsigned int toVersion = 0;
	int i;

	success = true;
	for ( i = 0; i < fromVersionNumbers.size() && i < 3 && success; ++i ) 
	{
		toVersion = toVersion << 8;
		toVersion += fromVersionNumbers.at(i).toInt( &success, 10 );
	}
	while ( (i-- < 3) && (i > 0) )
	{
		toVersion <<= 8;
	}
	return toVersion;
}

void AmsFirmwareCheck::convert( unsigned int fromVersion, QString & toVersion )
{
	toVersion.setNum( fromVersion & 0xFF );
	toVersion.prepend( '.' );
	QString temp;
	temp.setNum ( ( (fromVersion & 0xFF00) >> 8 ) );
	toVersion.prepend( temp );
	toVersion.prepend( '.' );
	temp.setNum ( ( (fromVersion & 0xFF0000) >> 16 ) );
	toVersion.prepend( temp );
}

bool AmsFirmwareCheck::convert( const QString & fromVersion, unsigned int & toVersion )
{
	bool success;
	toVersion = convert( fromVersion, success );
	return success;
}


/* this is the function that does the real checking. All other functions with the same name,
just are here for convenience and call this function finally to do the work. */
bool AmsFirmwareCheck::isCompatible
	( unsigned int guiStoredFwVersion
	, unsigned int fwVersion
	, QString & result
	)
{
	unsigned char guiReleaseMarker = static_cast< unsigned char >(   guiStoredFwVersion         & 0xFF );
	unsigned char guiMinor         = static_cast< unsigned char >( ( guiStoredFwVersion >>  8 ) & 0xFF );
	unsigned char guiMajor         = static_cast< unsigned char >( ( guiStoredFwVersion >> 16 ) & 0xFF );

	unsigned char fwReleaseMarker = static_cast< unsigned char >(   fwVersion         & 0xFF );
	unsigned char fwMinor         = static_cast< unsigned char >( ( fwVersion >>  8 ) & 0xFF );
	unsigned char fwMajor         = static_cast< unsigned char >( ( fwVersion >> 16 ) & 0xFF );

	/* 1. major numbers must be equal */
	bool success = ( fwMajor == guiMajor );

	/* 2. minor number of firmware must be at least as big as the minor number stored
	      in the gui */
	success = success && ( fwMinor >= guiMinor );

	/* 3. if minor number of firmware is equal to the minor number stored in the gui
	      the release marker in the firmware must be at least as big as the release
		  marker in the gui */
	if ( fwMinor == guiMinor )
	{
		success = success && ( fwReleaseMarker >= guiReleaseMarker );
	}

	result.clear();
	if ( success )
	{
		result.append( "Firmware and application are compatible." );
	}
	else
	{ /* find out if we need to upgrade firmware or gui */
		if (  (   fwMajor <  guiMajor ) 
		   || ( ( fwMajor == guiMajor ) && ( fwMinor <  guiMinor ) )
		   || ( ( fwMajor == guiMajor ) && ( fwMinor == guiMinor ) && ( fwReleaseMarker < guiReleaseMarker ) )
		   )
		{ /* we need to upgrade the firmware */
			result.append( "Current firmware and application are not compatible. ");
			result.append( "Please upgrade your firmware to version: ");
			QString res = QString( "%1.%2.%3 or higher.").arg(guiMajor).arg(guiMinor ).arg( guiReleaseMarker );
			result.append( res );
		}
		else
		{ /* we need to upgrade the gui */
			result.append( "The application is not compatible with the current firmware version. ");
			result.append( "Please upgrade your application software." );
		}
	}
	return success;
}

bool AmsFirmwareCheck::isCompatible
	( const QString & guiStoredFwVersion
	, const QString & fwVersion
	, QString & result 
	) 
{
	bool successA;
	bool successB;
	bool successC = isCompatible( convert( guiStoredFwVersion, successA ), convert( fwVersion, successB ), result );
	return successA && successB && successC;
}

bool AmsFirmwareCheck::isCompatible
	( const QString & guiStoredFwVersion
	, unsigned int fwVersion
	, QString & result 
	) 
{
	bool successA;
	bool successC = isCompatible( convert( guiStoredFwVersion, successA ), fwVersion, result );
	return successA && successC;
}

bool AmsFirmwareCheck::isCompatible
	( const char * guiStoredFwVersion
	, unsigned int fwVersion
	, QString & result 
	) 
{
	return isCompatible( QString( guiStoredFwVersion ), fwVersion, result );
}

bool AmsFirmwareCheck::isCompatible
	( const char * guiStoredFwVersion
	, const char * fwVersion
	, QString & result 
	) 
{
	return isCompatible( QString( guiStoredFwVersion ), QString( fwVersion ), result );
}
