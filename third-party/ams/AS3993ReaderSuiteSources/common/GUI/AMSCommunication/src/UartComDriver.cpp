/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file UartComDriver.cpp
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief  Communication classes for UART streaming communication 
 */

#include "AmsCom.h"
#include "UartComDriver.h"

#define UART_PACKET_SIZE (AMS_STREAM_BUFFER_SIZE + UART_HEADER_SIZE)
#define UART_RX_TIMEOUT 1000
#define UART_RX_TIMEOUT_COUNTER 5

UartComDriver::UartComDriver( unsigned int port )
    : AmsComDriver( ) 
    , itsPort( port )
    , itsUart( 0 )
    , itsTxHook( new unsigned char [ UART_PACKET_SIZE ] )
    , itsToTx( 0 )
    , itsTxBuffer( itsTxHook + UART_HEADER_SIZE )
    , itsRxHook( new unsigned char [ UART_PACKET_SIZE ] )
    , itsRxed( 0 )
    , itsRxBuffer( itsRxHook + UART_HEADER_SIZE )
    , itsTid( 0 )
{
    // do a buffer memset using pattern 0xCB (for clean buffer) 
    memset( itsTxHook, 0xCB, UART_PACKET_SIZE );

    itsPortSettings.BaudRate         = BAUD115200;
    itsPortSettings.DataBits         = DATA_8;
    itsPortSettings.FlowControl      = FLOW_OFF;
    itsPortSettings.Parity           = PAR_NONE;
    itsPortSettings.StopBits         = STOP_1;
    itsPortSettings.Timeout_Millisec = 500;
    itsPortSettings.Timeout_Sec      = 0;
}

UartComDriver::~UartComDriver ( )
{
    delete [] itsTxHook;
    delete [] itsRxHook;
}

void UartComDriver::clearError ( )
{
    itsRxed = 0;
    itsToTx = 0;
}

void UartComDriver::clearFirmwareError ( )
{
}

bool UartComDriver::open ( )
{
    if ( itsUart )
    { /* we are already connected */
        itsIsChannelOpen = true;
        return itsIsChannelOpen;
    }
    /* clear any internal states in the base class before trying to connect */
    internalReset( );

    itsUart = new QextSerialPort(QString("\\\\.\\COM%1").arg(QString::number(itsPort)), itsPortSettings);

    /* open port with flag Unbuffered
    * this speeds up the serial communication when using QextSerial */
    itsUart->open( QIODevice::ReadWrite | QIODevice::Unbuffered );
    if ( itsUart->isOpen( ) )
    {
        itsIsChannelOpen = true;
        lastError( );
        lastFirmwareError( );
        return itsIsChannelOpen;
    }

    delete itsUart;
    itsUart = 0;
    itsIsChannelOpen = false;
    itsLastError = ChannelOpenError;
    return itsIsChannelOpen;
}

bool UartComDriver::close ( )
{
    if ( itsUart )
    {
        if ( itsUart->isOpen( ) )
        {
            itsUart->close(); 
        }
        itsIsChannelOpen = false;
        delete itsUart; 
        itsUart = 0;
        lastError();
        lastFirmwareError( );
    }
    return true;
}

int UartComDriver::flush ( )
{
    if ( itsIsChannelOpen )
    {   /* add the header now */
        if ( itsLastError == NoError && itsToTx > 0 )
        {
            /* do transmit */
            tx( );
        }
    }
    else
    {
        itsLastError = ChannelOpenError; /* channel not open */
    }
    return itsLastError;
}

/* function transmits one full uart packet: consisting of a UART-Header + UART-Payload */
void UartComDriver::tx ( )
{
    if ( itsIsChannelOpen )
    {   /* add the header now */
        itsTid++;
        UART_TID( itsTxHook )    = itsTid;      /* transaction id */
        UART_STATUS( itsTxHook ) = 0; /* reserved from host to device */
        UART_SET_PAYLOAD_SIZE( itsTxHook, itsToTx );    /* tell the peer how many payload data there is really in the transaction */
        if ( itsUart->write( reinterpret_cast<char*>( itsTxHook ), itsToTx + UART_HEADER_SIZE ) != itsToTx + UART_HEADER_SIZE )
        {
            itsLastError = TxError; /* tx failed */
        }
        itsToTx = 0;
        memset( itsTxHook, 0xCB, UART_PACKET_SIZE );
    }
    else
    {
        itsLastError = ChannelOpenError; /* channel not open */
    }
}

int UartComDriver::txBuffer ( unsigned char * buffer, int size )
{
    if ( itsIsChannelOpen )
    {
        if ( itsToTx + size > AMS_STREAM_BUFFER_SIZE )
        {  /* data is too big to add to last packet */
            tx( ); /* do an immediate flush of previous data */
        }
        /* copy data into payload location in buffer */
        memcpy( itsTxBuffer + itsToTx, buffer, size );
        itsToTx += size;
        /* data will be transmitted when flush is called */
    }
    else
    {
        itsLastError = ChannelOpenError; /* channel not open */
    }
    return itsLastError;
}

/* function receives one full uart packet: consisting of a UART-Header + UART-Payload */
void UartComDriver::rx()
{
    qint64 bytesReturned = 0;
    int payload = 0;
    int count = 0;
    if ( itsIsChannelOpen )
    {
        itsRxHook [ 0 ] = 0; /* clear rx-tid */

        /* we are at the start of a new transaction wait for full header */
        do
        {
            if ( itsUart->bytesAvailable() < UART_HEADER_SIZE - bytesReturned )
            {
                itsUart->waitForReadyRead( UART_RX_TIMEOUT );
            }

            bytesReturned += itsUart->read( reinterpret_cast< char* >( itsRxHook + bytesReturned ), UART_HEADER_SIZE - bytesReturned );
            if ( bytesReturned == 0 )
            {
                ++count;
                if ( count > UART_RX_TIMEOUT_COUNTER ) 
                {
                    itsLastError = RxError;
                    return;
                }
            }
        } while ( bytesReturned < UART_HEADER_SIZE );
        /* if we get here we have received a full uart header */
        unsigned char fStatus = UART_STATUS( itsRxHook );
        if ( fStatus != 0 )
        {
            itsLastFirmwareError = fStatus;
        }
        payload = UART_GET_PAYLOAD_SIZE( itsRxHook );

        /* receive payload of whole packet at once */
        count = 0;
        do 
        {
            if ( itsUart->bytesAvailable() < payload - itsRxed )
            {
                itsUart->waitForReadyRead( UART_RX_TIMEOUT );
            }
            bytesReturned = itsUart->read( reinterpret_cast< char* >( itsRxBuffer + itsRxed ), payload - itsRxed );
            itsRxed += bytesReturned;
            if ( bytesReturned == 0 )
            { /* if we do not receive data for several times, assume that receive channel is dead */
                ++count;
                if ( count > UART_RX_TIMEOUT_COUNTER )
                {
                    itsLastError = RxError;
                    return;
                }
            }
        } while ( itsRxed < payload );
        /* if we get here the full packet is received */
    }
    else
    {
        itsLastError = ChannelOpenError; /* channel not open */
    }
}

/* the function copys <size> bytes into <buffer> - if necessary it
   receives one uart packet */
int UartComDriver::rxBuffer ( unsigned char * buffer, int size )
{
    if ( itsLastError == NoError && itsRxed == 0 && size > 0 )
    {
        rx( );    /* receive one uart packet */
    }

    if ( itsLastError == NoError && size > 0 )
    {
        if ( itsRxed < size )
        { /* as we have received a full payload -> no packet can be split into 2 payloads
             this must be an error */
            itsLastError = RxDataMismatch;
        }
        else
        {
            memcpy( buffer, itsRxBuffer, size );
            itsRxed -= size;
            if ( itsRxed > 0 )
            { /* move the remaining part of the buffer to the front -> easier code */
                memmove( itsRxBuffer, itsRxBuffer + size, itsRxed );
            }
        }
    }
    return itsLastError;
}

PortSettings UartComDriver::portSettings ( ) const
{
    return itsPortSettings;
}

void UartComDriver::setPortSettings ( const PortSettings &settings )
{
    itsPortSettings = settings;
    if ( itsUart == 0 )
    {
        return;
    }

    itsUart->setBaudRate(itsPortSettings.BaudRate);
    itsUart->setFlowControl(itsPortSettings.FlowControl);
    itsUart->setParity(itsPortSettings.Parity);
    itsUart->setDataBits(itsPortSettings.DataBits);
    itsUart->setStopBits(itsPortSettings.StopBits);
    itsUart->setTimeout(itsPortSettings.Timeout_Sec, itsPortSettings.Timeout_Millisec);
}

/* Tid, payload, protocol-id, rx-length, tx-length, firmware-number */
const unsigned char oldUartFirmwareNumberRequest[ ] = 
{ 0x07, 0x04, 0x03, 0x03, 0x01, 0x04 };
/* old firmware returns a tid like this: 0x7z where z is any nibble */
/* new firmware returns a tid like this: 0xde */

/* Tid, payload, protocol-id, rx-length, tx-length, enable-bootloader */
const unsigned char oldUartEnableBootloaderRequest[ ] =
{ 0x08, 0x04, 0x03, 0x00, 0x01, 0xEB };
/* old firmware returns a tid like this: 0x8z where z is any nibble */
/* new firmware returns a tid like this: 0xde */

#define OLD_UART_HEADER_SIZE 2

bool UartComDriver::isProtocolVersionCompatible ( bool enableBootloaderIfNotCompatible )
{
    bool toClose = ! isOpened( );

    /* 1. make sure we have communication channel */
    if ( ! isOpened( ) )
    {
        if ( ! open( ) )
        {
            itsLastError = ChannelOpenError;
            return false; /* failed to open a channel */
        }
    }

    /* 2. fill in a request in the old uart/protocol format */
    int size = sizeof( oldUartFirmwareNumberRequest );
    memcpy( itsTxHook, oldUartFirmwareNumberRequest, size );

    /* 3. send the request in the old uart format */
    if ( itsUart->write( reinterpret_cast<char*>( itsTxHook ), size ) != size )
    {
		itsLastError = TxError; 
        if ( toClose )
        {
            close( );
        }
        return false;
    }

    /* 3. read back the result of the request */

    /* 3.1 read back uart header */
    DWORD bytesReturned = 0;
    int count = 0;
    do
    {
        if ( itsUart->bytesAvailable() < OLD_UART_HEADER_SIZE - bytesReturned ) 
        {
            itsUart->waitForReadyRead( UART_RX_TIMEOUT );
        }
        bytesReturned += itsUart->read( reinterpret_cast< char* >( itsRxHook + bytesReturned ), OLD_UART_HEADER_SIZE );
        ++count;
    } while ( bytesReturned < OLD_UART_HEADER_SIZE && count < UART_RX_TIMEOUT_COUNTER );
    if ( bytesReturned < OLD_UART_HEADER_SIZE )
    { /* timeout during receive */
        itsLastError = RxError;
        if ( toClose )
        {
            close( );
        }
        return false;
    }

    /* 3.2 interpret header in old format */
    unsigned char tid     = itsRxHook[ 0 ];
    unsigned int  payload = itsRxHook[ 1 ]; /* old format had payload in this byte */

    /* 3.3 read back uart payload so that we have a clean line afterwards */
    bytesReturned = 0;
    count = 0;
    do
    {
        if ( itsUart->bytesAvailable() < payload - bytesReturned ) 
        {
            itsUart->waitForReadyRead( UART_RX_TIMEOUT );
        }
        bytesReturned += itsUart->read( reinterpret_cast< char* >( itsRxHook + bytesReturned ), payload - bytesReturned );
        ++count;
    } while ( bytesReturned < payload && count < UART_RX_TIMEOUT_COUNTER );
    if ( bytesReturned < payload )
    { /* timeout during receive */
        itsLastError = RxError;
        if ( toClose )
        {
            close( );
        }
        return false;
    }

	/* 4. interpret the received result: if the received TID has a special pattern, than it is the new firmware */
    if ( tid == AMS_STREAM_COMPATIBILITY_TID )
    {
        if ( toClose )
        {
            close( );
        }
        return true;
    }

    if ( enableBootloaderIfNotCompatible )
    { /* 5. if enable bootloader was requested, fill in the request in the old format */

        size = sizeof( oldUartEnableBootloaderRequest );
        memcpy( itsTxHook, oldUartEnableBootloaderRequest, size );

        /* 6. send the request in the old hid format */
        if ( itsUart->write( reinterpret_cast<char*>( itsTxHook ), size ) != size )
        {
		    itsLastError = TxError; 
        }
    }

    if ( toClose )
    {
        close( );
    }
    return false;
}


