/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  AMSCommunication class
 *
 *  AMSCommunication is an abstract class used for abstraction
 *  of different communication interfaces between the PC
 *  and the demo boards.
 */

#include "AMSCommunication.hxx"
#include "AMSTrace.hxx"

#define DEBUG_MUTEX_USAGE 0
#define USE_NEW_TRACE 1

const char * AMSCommunication::errorStrings[ ] =
{ "Success"
, "Error: no connection to device"
, "Error: read failed"
, "Error: write failed"
, "Error: verify failed"
, "Error: timeout occured"
, "Error: parity failure"
, "Error: incompatible firmware version" 
, "Error: unknown error code"
};


const char * AMSCommunication::errorToString ( Error code )
{
    int max = (sizeof( errorStrings )/sizeof(const char * ) ) - 1;
    int idx = code * -1; /* negative to positive value */
    if ( idx > max )
    {
        idx = max ;
    }
    return errorStrings[ idx ];
}

AMSCommunication::AMSCommunication()
{
	this->connected = false;
	mutex = new QMutex(QMutex::NonRecursive);
	itsLastError = NoError;
}

void AMSCommunication::deviceNodeChanged()
{
	// have a look if a board with the correct PID is connected
	if(isConnected())
	{
		// try to connect to this board, if we are already connected, nothing will happen
		connect();
	}
	else
	{
		// there is no board with correct PID connected
		// we try to disconnect
		disconnect();
	}
}

void AMSCommunication::disconnect()
{
	emit beforeDisconnect();
	this->hwDisconnect();
	emit connectionChanged(false);
    connected = false;
}

AMSCommunication::Error AMSCommunication::connect()
{
	AMSCommunication::Error err = this->hwConnect();
	if (NoError == err)
	{
		connected = true;
	}
	else
	{	
		connected = false;
	}
	emit connectionChanged(connected);
	return err;

}

bool AMSCommunication::isConnected()
{
	return this->connected;
}

AMSCommunication::Error AMSCommunication::readRegister(unsigned char registerAddress, unsigned char* registerValue, bool doemit, unsigned char trcLevel)
{
	itsLastError = NoError;

	if(!connected)
	{
		/* try to connect */
		itsLastError = this->connect();
	}
	mutex->lock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() readRegister(" << registerAddress << ")";
#endif
	if (NoError == itsLastError)
	{
		itsLastError = this->hwReadRegister(registerAddress, registerValue);
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock() readRegister";
#endif
	mutex->unlock();
	if ((doemit) && (NoError == itsLastError))
	{
		emit this->dataChanged(registerAddress, 0 /* don't care */, false, *registerValue);
	}
#if (USE_NEW_TRACE==1)
	QStringList lst;
	lst << "Read" << QString::number(registerAddress) << QString("255") << QString::number(*registerValue) <<  lastError();
	
	AMSTrace::trc(trcLevel,(void*)new QStringList(lst));
#else
	emit this->dataChanged(registerAddress, 0 /* don't care */, false, 0, *registerValue, Read,itsLastError,"");
#endif
	return itsLastError;
}

AMSCommunication::Error AMSCommunication::writeRegister(unsigned char registerAddress, unsigned char registerValue, bool verify,bool doemit, unsigned char trcLevel)
{
	unsigned char verifyValue;

    itsLastError = NoError;

	if(!connected)
	{
		/* try to connect */
		itsLastError = this->connect();
	}
	mutex->lock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() writeRegister(" << registerAddress << ", " << registerValue << ")";
#endif
    if (NoError == itsLastError)
	{
		itsLastError = this->hwWriteRegister(registerAddress, registerValue);
		if (NoError == itsLastError)
		{
			if(verify == true) /* verify */
			{
				itsLastError = this->hwReadRegister(registerAddress, &verifyValue);
				if (NoError == itsLastError)
				{
					if (verifyValue != registerValue)
					{
						itsLastError = VerifyError;
					}
				}
			}
		}
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock() writeRegister";
#endif
	mutex->unlock();
	if ((doemit) && (NoError == itsLastError))
	{
		emit this->dataChanged(registerAddress, 0 /* don't care */, false, registerValue);
	}
#if (USE_NEW_TRACE==1)
	QStringList lst;
	lst << "Write" << QString::number(registerAddress) << QString("255") << QString::number(registerValue) <<  lastError();
	AMSTrace::trc(trcLevel,(void*)new QStringList(lst));
#else
	emit this->dataChanged(registerAddress, 0 /* don't care */, false, 0, registerValue, Write,itsLastError,"");
#endif
	return itsLastError;
}

AMSCommunication::Error AMSCommunication::modifyRegister(unsigned char reg, unsigned char mask, unsigned char val, bool verify,bool doemit, unsigned char trcLevel)
{
	unsigned char readval=0, verifyValue = 0;
	itsLastError = NoError;

	if(!connected)
	{
		/* try to connect */
		itsLastError = this->connect();
	}
	mutex->lock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() modifyRegister(" << reg << ", " << mask << ", " << val << ")";
#endif
    if (NoError == itsLastError)
	{
		itsLastError = this->hwReadRegister(reg, &readval);
		if (NoError == itsLastError)
		{
			readval &= ~mask;
			readval |= (val & mask);
			itsLastError = this->hwWriteRegister(reg, readval);
			if (NoError == itsLastError)
			{
				if(verify == true)
				{
					itsLastError = this->hwReadRegister(reg, &verifyValue);
					if (NoError != itsLastError || verifyValue != readval)
					{
						itsLastError = VerifyError;
					}
				}
			}
		}
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock() modifyRegister";
#endif
	mutex->unlock();
	if ((doemit) && (NoError == itsLastError))
	{
		emit this->dataChanged(reg, 0 /* don't care */, false, readval);
	}
#if (USE_NEW_TRACE==1)
	QStringList lst;
	lst << "Modify" << QString::number(reg) << QString::number(mask) << QString::number(readval) <<  lastError();
	AMSTrace::trc(trcLevel,(void*)new QStringList(lst));
#else
	emit this->dataChanged(reg, 0 /* don't care */, false, mask, val, Modify,itsLastError,"");
#endif
	return itsLastError;
}

void AMSCommunication::setControlRegisterAddress(unsigned short ctrlRegister, unsigned char ctrlMask)
{
	this->ctrlRegisterAddr = ctrlRegister;
	this->ctrlRegisterMask = ctrlMask;
	emit this->dataChanged(ctrlRegister, 0, 0, ctrlMask,0, ControlReg , NoError,"");
}

AMSCommunication::Error AMSCommunication::writeSubRegister(unsigned char reg, unsigned char val, unsigned char subAddress, bool verify, bool doemit, unsigned char trcLevel)
{
	Error res = NoError;
	unsigned char readval, verifyvalue;

	if(!connected)
	{
		/* try to connect */
		res = this->connect();
	}
    mutex->lock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() writeSubRegister(" << reg << ", " << val << ", " << subAddress << ")";
#endif
	if (NoError == res)
	{
		res = this->hwReadRegister(ctrlRegisterAddr, &readval);
		if (NoError == res)
		{
			readval &= ~this->ctrlRegisterMask;
			readval |= (subAddress & this->ctrlRegisterMask);
			res = this->hwWriteRegister(ctrlRegisterAddr, readval);
			if (res == NoError)
			{
				res = this->hwWriteRegister(reg, val);
				if (NoError == res)
				{
					if(verify == true) /* verify */
					{
						/* write again to the master register */
						res = this->hwWriteRegister(ctrlRegisterAddr, readval);
						if (NoError == res)
						{
							res = this->hwReadRegister(reg, &verifyvalue);
							if (NoError != res || verifyvalue != val)
							{
								res = VerifyError;
							}
						}
					}
				}
			}
		}
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock() writeSubRegister";
#endif
	mutex->unlock();
	if ((doemit) && (NoError == res))
	{
		emit this->dataChanged(reg, subAddress, true, val);
	}
	emit this->dataChanged(reg, subAddress, true, 0,val, Write,res,"");
#if (USE_NEW_TRACE==1)
    QStringList lst;
    lst << "Write" << QString::number(devAddr) << QString::number(reg) + "-" + QString::number(subAddress) << QString("255") << QString::number(val) <<  lastError();
    AMSTrace::trc(trcLevel,(void*)new QStringList(lst));
#endif
	return res;
}

AMSCommunication::Error AMSCommunication::readSubRegister(unsigned char reg, unsigned char *val, unsigned char subAddress, bool doemit, unsigned char trcLevel)
{
	Error res = NoError;
	unsigned char readval;

	if(!connected)
	{
		/* try to connect */
		res = this->connect();
	}
    mutex->lock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() readSubRegister(" << reg << ", " << subAddress << ")";
#endif
	if (NoError == res)
	{
		res = this->hwReadRegister(ctrlRegisterAddr, &readval);
		if (NoError == res)
		{
			readval &= ~this->ctrlRegisterMask;
			readval |= (subAddress & this->ctrlRegisterMask);
			res = this->hwWriteRegister(ctrlRegisterAddr, readval);
			if (res == 0)
			{
				res = this->hwReadRegister(reg, val);
			}
		}
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock() readSubRegister";
#endif
	mutex->unlock();
	if ((doemit) && (NoError == res))
	{
		emit this->dataChanged(reg, subAddress, true, *val);
	}
	emit this->dataChanged(reg, subAddress, true, 0, *val, Read,res,"");
#if (USE_NEW_TRACE==1)
    QStringList lst;
    lst << "Read" << QString::number(devAddr) << QString::number(reg) + "-" + QString::number(subAddress) << QString("255") << QString::number(*val) <<  lastError();
    AMSTrace::trc(trcLevel,(void*)new QStringList(lst));
#endif
	return res;
}
	
AMSCommunication::Error AMSCommunication::modifySubRegister(unsigned char reg, unsigned char mask, unsigned char val, unsigned char subAddress, bool verify, bool doemit, unsigned char trcLevel)
{
	unsigned char readval=0, readvalCtrl=0, verifyValue=0;
	Error res = NoError;

	if(!connected)
	{
		/* try to connect */
		res = this->connect();
	}
    mutex->lock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() modifySubRegister(" << reg << ", " << mask << ", " << val << ", " << subAddress << ")";
#endif
	if (NoError == res)
	{
		res = this->hwReadRegister(ctrlRegisterAddr, &readvalCtrl);
		if (NoError == res)
		{
			readvalCtrl &= ~this->ctrlRegisterMask;
			readvalCtrl |= (subAddress & this->ctrlRegisterMask);
			res = this->hwWriteRegister(ctrlRegisterAddr, readvalCtrl);
			if (NoError == res)
			{
				res = this->hwReadRegister(reg, &readval);
				if (NoError == res)
				{
					res = this->hwWriteRegister(ctrlRegisterAddr, readvalCtrl);
					if (NoError == res)
					{
						readval &= ~mask;
						readval |= (val & mask);
						res = this->hwWriteRegister(reg, readval);
						if (NoError == res)
						{
							/* write again to the master register */
							res = this->hwWriteRegister(ctrlRegisterAddr, readvalCtrl);
							if (NoError == res)
							{
								if(verify == true)
								{
									res = this->hwReadRegister(reg, &verifyValue);
									if (NoError != res || verifyValue != readval)
									{
										res = VerifyError;
									}
								}
							}
						}
					}
				}
			}
		}
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock() modifySubRegister";
#endif
	mutex->unlock();
	if ((doemit) && (NoError == res))
	{
		emit this->dataChanged(reg, subAddress, true, readval);
	}
	emit this->dataChanged(reg, subAddress, true, mask, val, Modify,res,"");
#if (USE_NEW_TRACE==1)
    QStringList lst;
    lst << "Modify" << QString::number(devAddr) << QString::number(reg) + "-" + QString::number(subAddress) << QString(mask) << QString::number(val) <<  lastError();
    AMSTrace::trc(trcLevel,(void*)new QStringList(lst));
#endif
	return res;
}

AMSCommunication::Error AMSCommunication::sendCommand(QString command, QString * answer)
{
	AMSCommunication::Error err = NoError;
	bool doemit = true;

	if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}
    mutex->lock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() sendCommand(" << command << ")";
#endif
	if (NoError == err)
	{
		err = this->hwSendCommand(command, answer);
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock() sendCommand";
#endif
	mutex->unlock();
	if ((doemit) && (NoError == err))
	{
	}
	emit this->dataChanged(0, 0, false, 0, 0, Command,err,command);
	return err;
}

void AMSCommunication::doEmitDataChanged(unsigned char registerAddress, unsigned char registerValue)
{
	emit this->dataChanged(registerAddress, 0 /* don't care */, false, registerValue);
}

void AMSCommunication::doEmitDataChanged(unsigned char registerAddress, unsigned char subAddress, unsigned char registerValue)
{
	emit this->dataChanged(registerAddress, subAddress, true, registerValue);
}

QString AMSCommunication::lastError()
{
	if(itsLastError == NoError)
		return QString("OK");
	if(itsLastError == ConnectionError)
		return QString("Connection Error");
	if(itsLastError == ReadError)
		return QString("Read Error");
	if(itsLastError == WriteError)
		return QString("Write Error");
	if(itsLastError == VerifyError)
		return QString("Verify Error");
	return QString("Unknown Error");
}
