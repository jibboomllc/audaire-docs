/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams Stream Communication
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsCom.cpp
 *
 *  \author M. Arpa
 *
 *  \brief Implementation of the xml interpreter.
 */

#include "AmsCom.h"
#include "AmsComObject.h"
#include "AmsComStream.h"
#include "ApplComObjects.h"
#include "CtrlComObjects.h"
#include "I2cComObjects.h"
#include "SpiComObjects.h"

#include <QString>
#include <QXmlStreamReader>
#include <QQueue>

AmsComObject * AmsComXmlReader::clone( QXmlStreamReader * xml, unsigned char protocol )
{
    AmsComObject * p = 0;
    switch ( protocol )
    {
        case AMS_COM_FLUSH:
            p = new AmsFlushObject( );
            break;

        case AMS_COM_I2C:
        case AMS_COM_I2C_CONFIG:
            p = I2cComXmlReader::clone( xml, protocol );
            break;
            
        case AMS_COM_SPI:
        case AMS_COM_SPI_CONFIG:
            p = SpiComXmlReader::clone( xml, protocol );
            break;

        case AMS_COM_CTRL_CMD_RESET:
        case AMS_COM_CTRL_CMD_FW_INFORMATION:
        case AMS_COM_CTRL_CMD_FW_NUMBER:
        case AMS_COM_CTRL_CMD_ENTER_BOOTLOADER:
            p = CtrlComXmlReader::clone( xml, protocol );
            break;

        case AMS_COM_WRITE_REG:
        case AMS_COM_READ_REG:
            p = ApplComXmlReader::clone( xml, protocol );
            break;

        default:
            if ( protocol >= AMS_COM_CONFIG )
            { /* not implemented - reverved protocol */
            }
            else /* application specific protocol */
            {
                p = ApplComXmlReader::clone( xml, protocol );
            }
            break;
    }
    return p;
}
 

AmsComObject * AmsComXmlReader::clone( QXmlStreamReader * xml, const char * & direction )
{
    AmsComObject * p = 0;
    unsigned char protocol;
    if ( xml )
    {
        while ( ! xml->atEnd() && ! xml->hasError() )
        {
            /* we need at a telegram */
            if ( xml->isStartElement() && xml->name() == AMS_COM_XML_TAG )
            {  /* get all attributes and its value of a tag in attrs variable.			*/			
                QXmlStreamAttributes attrs = xml->attributes();
                /*get value of each attribute from QXmlStreamAttributes */
                QStringRef pid = attrs.value( AMS_COM_XML_PROTOCOL_ID );
                QStringRef dir = attrs.value( AMS_COM_XML_DIRECTION );
                QStringRef trace = attrs.value( AMS_COM_XML_TRACE_PATTERN );
                if ( ! pid.isEmpty() && ! dir.isEmpty() )
                { 
                    bool result;
                    int i = pid.toString().toInt( & result, AMS_COM_XML_NUMBER_BASE );
                    protocol = static_cast< unsigned char >( i );
                    direction = getDirection( dir.toString() );
                    if ( result )
                    {
                        p = clone( xml, protocol );
                        if ( p )
                        {
                            unsigned int tracePattern = trace.toString().toUInt( 0, AMS_COM_XML_NUMBER_BASE );
                            p->setTracePattern( tracePattern );
                        }
                        return p;
                    }
                }
            }
            xml->readNext();
        }
    }
    return p;
}

const char * AmsComXmlReader::getDirection( const QString & direction )
{
    if ( direction.compare( AMS_COM_XML_WRITE ) == 0 )
    {
        return AMS_COM_XML_WRITE;
    }
    else if ( direction.compare( AMS_COM_XML_READ ) == 0 )
    {
        return AMS_COM_XML_READ;
    }
    else if ( direction.compare( AMS_COM_XML_READ_RESULT ) == 0 ) 
    {
        return AMS_COM_XML_READ_RESULT;
    }
    else
    {
        return AMS_COM_XML_NO_DIRECTION;
    }
}


AmsComXmlPlayer::AmsComXmlPlayer ( QXmlStreamReader * xml, AmsComStream * stream )
    : itsXml( xml )
    , itsStream( stream )
    , itsObjToRead( )
{
}

void AmsComXmlPlayer::reset ( )
{
    bool open = ( itsStream ? itsStream->isOpened( ) : false );
    if ( open )
    { /* must close stream before we remove objects from the queue */
        itsStream->close( );
    }
    while ( ! itsObjToRead.isEmpty( ) )
    {
        AmsComObject * temp = itsObjToRead.dequeue( );
        delete temp;
    }
    if ( open )
    {
        itsStream->open( );
    }
}

AmsComXmlPlayer::~AmsComXmlPlayer ( )
{
    reset( );
}

void AmsComXmlPlayer::set ( QXmlStreamReader * xml, AmsComStream * stream )
{
    itsXml = xml;
    itsStream = stream;
}



bool AmsComXmlPlayer::playNext( )
{
    bool result = false;
    if ( itsXml && itsStream )
    {
        const char * direction;

        /* 1. get object */
        AmsComObject * obj = AmsComXmlReader::clone( itsXml, direction );
        if ( obj )
        {
            /* 2. fill object */
            if ( obj->fill( itsXml ) )
            {
                QString d( direction );
                /* 3. transmit or receive object */
                if ( d == AMS_COM_XML_WRITE ) 
                {
                    *itsStream << *obj;
                }
                else if ( d == AMS_COM_XML_READ )
                {
                    *itsStream >> *obj;
                }
                else /* if ( d == AMS_COM_XML_READ_RESULT ) */
                {
                }
                result = true;
            }
            /* 4. delete all object in queue that are valid now */
            while ( ! itsObjToRead.isEmpty() && itsObjToRead.head()->valid() )
            {
                AmsComObject * temp = itsObjToRead.dequeue( );
                /* temp object was read from the stream -> can delete it now */
                delete temp;
            }

            /* 5. see if we can delete the object */
            if ( obj->valid( ) ) 
            {
                delete obj;
            }
            else /* keep object for later delete */
            {
                itsObjToRead.enqueue( obj );
            }
        }
    }
    return result;
}
