
#include "UsbBox.hxx"
#include "UsbBoxUsingCyApi.hxx"
#include "UsbBoxUsingUsbConDll.hxx"

UsbBox UsbBox::itsDummyInstance;
UsbBox * UsbBox::itsInstance = 0;

UsbBox & UsbBox::instance ( )
	{
		if ( ! itsInstance )
		{ /* while we do not have an instance we try to see if a usb box is connected to the pc */
			if ( UsbBoxUsingCyApi::isDriverAvailable( ) )
			{ /* usb box is connected and we use the cyusb driver */
				itsInstance = new UsbBoxUsingCyApi( );
			}
			else if ( UsbBoxUsingUsbConDll::isDriverAvailable( ) )
			{ /* usb box is connected and we use the ezusb driver */
				itsInstance = new UsbBoxUsingUsbConDll( );
			}
			else /* no driver available - i.e. no usb box is connected to the pc */
			{
				return itsDummyInstance;
			}
		}
		return *itsInstance;
	};

UsbError UsbBox::switchInterface ( Interface iface )
{
	char packet[ USB_PACKET_SIZE ]; 
	bool toSend = false;
	USB_TASK( packet ) = TSK_SWITCH_INTERFACE;
	USB_STATE( packet ) = 0;
	itsInterface = iface;
	if ( iface == OPTO )
	{
		USB_SWITCH_GET_PROTOCOL( packet ) = OPTO_I2C;
		toSend = true;
	}
	else if ( iface == I2C )
	{
		USB_SWITCH_GET_PROTOCOL( packet ) = PLAIN_I2C;
		toSend = true;
	}
	else if ( iface == MULTI_LEVEL )
	{
		USB_SWITCH_GET_PROTOCOL( packet ) = MULTI_LEVEL_I2C;
		USB_SWITCH_GET_SPIKE_SUP_TIME( packet ) = itsMultiLevelI2cHalfSpikeSupTime;
		USB_SWITCH_GET_EN_TIME( packet ) = itsMultiLevelI2cHalfEnTime;
		toSend = true;
	}

	if ( toSend )
	{
		return sendPacketFn(packet,USB_PACKET_SIZE,packet,USB_PACKET_SIZE);
	}
	return ERR_NO_ERR;
}

