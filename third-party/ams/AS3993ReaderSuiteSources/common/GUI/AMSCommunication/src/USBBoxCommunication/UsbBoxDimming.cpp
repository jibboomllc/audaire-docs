/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  
 *
 */

#include "UsbBoxDimming.h"
#include "usb.h"
#include "UsbBoxPwm.h" 
#include "USBBoxCommunication.hxx"

#define DIMM_MAX 4096
#define DIMM_MIN 0
#define DIMM_SHIFT 5
#define DIMM_COUNTER	(23 * DIMM_PACKETS) /* 1 dimm-packet correspond to ~ 22 ms */

#define DIMM_UP			0
#define DIMM_MIDDLE		1
#define DIMM_UP_2ND  	2
#define DIMM_TOP		3
#define DIMM_DOWN_2ND	4
#define DIMM_MIDDLE_2ND	5
#define DIMM_DOWN		6
#define DIMM_BOTTOM		7

const char UsbBoxDimming::theSkip[] = 
{	25	/* 125 ms */
,	14	/* 250 ms */
,	7	/* 500 ms */
,	3	/* 1000 ms */
,	1	/* 2000 ms */
,	0	/* 4000 ms */
};


const int UsbBoxDimming::theMs[ ] = { 125, 250, 500, 1000, 2000, 4000 };
const int UsbBoxDimming::theValues[ ] = 
{ 0		/* +14	 0 % */
, 14	/* +15	 5 % */
, 29	/* +16	10 % */
, 45	/* +18	15 % */
, 64	/* +25	20 % */
, 88	/* +29	25 % */
, 117	/* +37	30 % */
, 154	/*		35 % */
, 201	/*		40 % */
, 261	/*		45 % */
, 339	/*		50 % */
, 438	/*		55 % */
, 566	/*		60 % */
, 729	/*		65 % */
, 937	/*		70 % */
, 1205	/*		75 % */
, 2546	/*		80 % */
, 1983	/*		85 % */
, 2539	/* +556	90 % */
, 3252	/* 		95 % */
, 4096	/*		100 % */
};

int UsbBoxDimming::convertToValue( int percent )
{
	percent /= 5;
	return theValues[ percent ];
}


/* we use frequency of 220 Hz == USB_BOX_DIMMING_FREQUENCY, therfore a duty cycle is 1/220 ~ 4.545454 ms
   this time interval is divided into 4096 slots with one slot the size of 4.545454 / 4096 ~ 1.1097 us
   The dimming is calculated to take a value between 0 and 4096 so we need to convert this value to
   ticks by using the formula: ( value * 1.1097 ) which is converted to the following integer
   formula: ( value * 111 ) / 100 
   Do not forget to multiply it by 2 as one micro-second is 2 ticks */
short UsbBoxDimming::roundToTicks( short value )
{
	DWORD val = value;
	val = ( val * 111 * 2 ) / 100;
	return static_cast< short >( val );
}

short UsbBoxDimming::upNextValue( short value, short limit, signed char skip )
{
	while ( ( value += 1 + ( value >> DIMM_SHIFT ) ) < limit && --skip >= 0 ) ;
	if ( value > limit )
	{
		value = limit;
	}
	return value;
}

short UsbBoxDimming::downNextValue( short value, short limit, signed char skip )
{
	while ( ( value -= ( 1 + ( value >> DIMM_SHIFT ) ) ) > limit && --skip >= 0 ) ;
	if ( value < limit )
	{
		value = limit;
	}
	return value;
}

	
bool UsbBoxDimming::startOrStop( bool isFirstSignal, int start, int stop, int skipIndex )
{
	itsState = DIMM_BOTTOM;
	itsCounter = 10; /* do a quick start */

	if (  stop  < start 
	   || start > stop
	   )
	{
		return false; /* illegal values */
	}

	if ( skipIndex >= sizeof( theSkip ) || skipIndex < 0 )
	{
		return false; /* illegal time */
	}

	dimming * dimm = &itsDimm[ ( isFirstSignal ? 0 : 1 ) ];
	dimm->start = start;
	dimm->stop = stop;
	dimm->skip = theSkip[ skipIndex ]; /* skip represents the dimming interval -> 125ms - 4 s */
	dimm->currValue = dimm->start; /* we always start to dimm up */
	return true;
}


int UsbBoxDimming::generatePacket( char * buf )
{
	char * bufStart = buf;
	int i;
	int j;
	for ( i = 0; i < DIMM_PACKETS; )
	{
		UsbBoxPwm sorter( itsDutyCycle );
		/* every signal starts with high - with the exception that it is totaly low == off */
		switch ( itsState )
		{
			case DIMM_UP:
				/* signal 0 is dimmed up, signal 1 stays at start */
				itsDimm[ 0 ].currValue = upNextValue( itsDimm[0].currValue, itsDimm[0].stop, itsDimm[0].skip );
				if ( itsDimm[0].currValue >= itsDimm[0].stop )
				{
					itsState = DIMM_MIDDLE;
					itsCounter = DIMM_COUNTER;
					break; /* do not add points -> might be signal off */
				}
				sorter.addPoint(0, itsDimm[0].signal, true ); /* signal goes up at 0 */
				sorter.addPoint( roundToTicks( itsDimm[0].currValue ), itsDimm[0].signal, false ); /* signal goes down at calculated point */
				if ( itsDimm[1].start > DIMM_MIN )
				{ /* we do not stay low all the time, so signal must go up and down */
					sorter.addPoint(0, itsDimm[1].signal, true ); 
					if ( itsDimm[1].start < DIMM_MAX )
					{
						sorter.addPoint( roundToTicks( itsDimm[1].start ), itsDimm[1].signal, false );
					}
				}
			break;
			case DIMM_MIDDLE:
				if ( itsCounter == 0 )
				{ 
					if ( itsDimm[1].stop > itsDimm[1].start )
					{ /* we do some dimming */
						itsState = DIMM_UP_2ND;
					}
					else
					{
						itsState = DIMM_DOWN;
					}
					break;
				} 
				itsCounter--;
				if ( itsDimm[0].stop > DIMM_MIN )
				{
					sorter.addPoint(0, itsDimm[0].signal, true );
					if ( itsDimm[0].stop < DIMM_MAX )
					{ /* if we are not at the maximum, the signal must go low at some point in time */
						sorter.addPoint( roundToTicks( itsDimm[0].stop ), itsDimm[0].signal, false );
					}
				}
				if ( itsDimm[1].start > DIMM_MIN )
				{ /* we do not stay low all the time, so signal must go up and down */
					sorter.addPoint(0, itsDimm[1].signal, true ); 
					if ( itsDimm[1].start < DIMM_MAX )
					{
						sorter.addPoint( roundToTicks( itsDimm[1].start ), itsDimm[1].signal, false );
					}
				}
			break;
			case DIMM_UP_2ND:
				/* signal 1 is dimmed up, signal 0 stays at stop */
				itsDimm[ 1 ].currValue = upNextValue( itsDimm[1].currValue, itsDimm[1].stop, itsDimm[1].skip );
				if ( itsDimm[1].currValue >= itsDimm[1].stop )
				{
					itsState = DIMM_TOP;
					itsCounter = DIMM_COUNTER;
					break; /* do not add points -> might be signal off */
				}
				sorter.addPoint(0, itsDimm[1].signal, true ); /* signal goes up at 0 */
				sorter.addPoint( roundToTicks( itsDimm[1].currValue ), itsDimm[1].signal, false ); /* signal goes down at calculated point */
				if ( itsDimm[0].stop > DIMM_MIN )
				{ /* we do not stay low all the time, so signal must go up */
					sorter.addPoint(0, itsDimm[0].signal, true ); 
					if ( itsDimm[0].stop < DIMM_MAX )
					{
						sorter.addPoint( roundToTicks( itsDimm[0].stop ), itsDimm[0].signal, false );
					}
				}
			break;
			case DIMM_TOP:
				/* 2nd signal stays at its stop values */
				if ( itsCounter == 0 )
				{ 
					itsState = DIMM_DOWN_2ND;
					break;
				} 
				itsCounter--;
				sorter.addPoint(0, itsDimm[1].signal, true );
				if ( itsDimm[1].stop < DIMM_MAX )
				{ /* if we are not at the maximum, the signal must go low at some point in time */
					sorter.addPoint( roundToTicks( itsDimm[1].stop ), itsDimm[1].signal, false );
				}
				if ( itsDimm[0].stop > DIMM_MIN )
				{ /* we do not stay low all the time, so signal must go up */
					sorter.addPoint(0, itsDimm[0].signal, true ); 
					if ( itsDimm[0].stop < DIMM_MAX )
					{
						sorter.addPoint( roundToTicks( itsDimm[0].stop ), itsDimm[0].signal, false );
					}
				}
			break;
			case DIMM_DOWN_2ND:
				/* signal 1 is dimmed down, signal 0 stays at stop */
				itsDimm[ 1 ].currValue = downNextValue( itsDimm[1].currValue, itsDimm[1].start, itsDimm[1].skip );
				if ( itsDimm[1].currValue <= itsDimm[1].start )
				{
					itsState = DIMM_MIDDLE_2ND;
					itsCounter = DIMM_COUNTER;
					break; /* do not add points */
				}
				sorter.addPoint(0, itsDimm[1].signal, true ); /* signal goes up at 0 */
				sorter.addPoint( roundToTicks( itsDimm[1].currValue ), itsDimm[1].signal, false ); /* signal goes down at calculated point */
				if ( itsDimm[0].stop > DIMM_MIN )
				{ /* we do not stay low all the time, so signal must go up */
					sorter.addPoint(0, itsDimm[0].signal, true ); 
					if ( itsDimm[0].stop < DIMM_MAX )
					{
						sorter.addPoint( roundToTicks( itsDimm[0].stop ), itsDimm[0].signal, false );
					}
				}
			break;
			case DIMM_MIDDLE_2ND:
				if ( itsCounter == 0 )
				{ 
					if ( itsDimm[0].stop > itsDimm[0].start )
					{ /* we do some dimming */
						itsState = DIMM_DOWN;
					}
					else
					{
						itsState = DIMM_UP_2ND;
					}
					break;
				} 
				itsCounter--;
				if ( itsDimm[0].stop > DIMM_MIN )
				{
					sorter.addPoint(0, itsDimm[0].signal, true ); 
					if ( itsDimm[0].stop < DIMM_MAX )
					{ /* if we are not at the maximum, the signal must go low at some point in time */
						sorter.addPoint( roundToTicks( itsDimm[0].stop ), itsDimm[0].signal, false );
					}
				}
				if ( itsDimm[1].start > DIMM_MIN )
				{ /* we do not stay low all the time, so signal must go up and down */
					sorter.addPoint(0, itsDimm[1].signal, true ); 
					if ( itsDimm[1].start < DIMM_MAX )
					{
						sorter.addPoint( roundToTicks( itsDimm[1].start ), itsDimm[1].signal, false );
					}
				}
				else if ( itsDimm[0].stop <= DIMM_MIN )
				{ /* add at least the low signal so that we get 1 point */
					sorter.addPoint(0, itsDimm[0].signal, false ); 
				}
			break;
			case DIMM_DOWN:
				/* signal 0 is dimmed down, signal 1 stays at start */
				itsDimm[ 0 ].currValue = downNextValue( itsDimm[0].currValue, itsDimm[0].start, itsDimm[0].skip );
				if ( itsDimm[0].currValue <= itsDimm[0].start )
				{
					itsState = DIMM_BOTTOM;
					itsCounter = DIMM_COUNTER;
					break; /* do not add points -> might be signal off */
				}
				sorter.addPoint(0, itsDimm[0].signal, true ); /* signal goes up at 0 */
				sorter.addPoint( roundToTicks( itsDimm[0].currValue ), itsDimm[0].signal, false ); /* signal goes down at calculated point */
				if ( itsDimm[1].start > DIMM_MIN )
				{ /* we do not stay low all the time, so signal must go up and down */
					sorter.addPoint(0, itsDimm[1].signal, true ); 
					if ( itsDimm[1].start < DIMM_MAX )
					{
						sorter.addPoint( roundToTicks( itsDimm[1].start ), itsDimm[1].signal, false );
					}
				}
			break;
			case DIMM_BOTTOM:
			default:
				if ( itsCounter == 0 )
				{ 
					if ( itsDimm[0].stop > itsDimm[0].start )
					{ /* we do some dimming */
						itsState = DIMM_UP;
					}
					else if ( itsDimm[1].stop > itsDimm[1].start )
					{
						itsState = DIMM_UP_2ND;
					}
					else
					{
						itsState = DIMM_BOTTOM;
						itsCounter = DIMM_COUNTER;
					}
					break;
				} 
				itsCounter--;
				if ( itsDimm[0].start > DIMM_MIN )
				{
					sorter.addPoint(0, itsDimm[0].signal, true ); 
					if ( itsDimm[0].start < DIMM_MAX )
					{ /* if we are not at the maximum, the signal must go low at some point in time */
						sorter.addPoint( roundToTicks( itsDimm[0].start ), itsDimm[0].signal, false );
					}
				}
				else if ( itsDimm[1].start <= DIMM_MIN )
				{ /* add at least the low signal so that we get 1 point */
					sorter.addPoint(0, itsDimm[0].signal, false ); 
				}
				if ( itsDimm[1].start > DIMM_MIN )
				{ /* we do not stay low all the time, so signal must go up and down */
					sorter.addPoint(0, itsDimm[1].signal, true ); 
					if ( itsDimm[1].start < DIMM_MAX )
					{
						sorter.addPoint( roundToTicks( itsDimm[1].start ), itsDimm[1].signal, false );
					}
				}
			break;
		} /* end switch */

		/* now get all sorted points and write them to the buffer */
		int reloadValues = sorter.getNumberOfPoints( );
		if ( reloadValues > 0 )
		{
			if ( reloadValues == 2 )
			{ /* if just 1 point is added check if both signals are active
			     and add another point if both are active */
				if (  itsDimm[0].currValue > DIMM_MIN								/* signal 0 is active and is not dark */
				   && itsDimm[1].currValue <= DIMM_MIN                              /* signal 1 is dark */
				   )
				{
					short temp = itsDimm[0].currValue;
					/* must add some very small value that is greater than 0 and
					   not equal to the already added point */
					if ( temp > 1 )
					{
						temp /= 2;
					}
					else
					{
						temp = 2;
					}
					sorter.addPoint( roundToTicks( temp ), itsDimm[1].signal, false ); /* add another timer value to make the duty cycle the same */
				}
				if (  itsDimm[1].currValue > DIMM_MIN								/* signal 1 is active and is not dark */
				   && itsDimm[0].currValue <= DIMM_MIN                              /* signal 0 is dark */
				   )
				{ 
					short temp = itsDimm[1].currValue;
					/* must add some very small value that is greater than 0 and
					   not equal to the already added point */
					if ( temp > 1 )
					{
						temp /= 2;
					}
					else
					{
						temp = 2;
					}
					sorter.addPoint( roundToTicks( temp ), itsDimm[0].signal, false ); /* add another timer value to make the duty cycle the same */
				}
				reloadValues = sorter.getNumberOfPoints( );
			}
			for ( j = 0; j < reloadValues; ++j )
			{
				DWORD ticks;
				unsigned char signal;
				if ( sorter.getDuration( j, ticks, signal ) )
				{
					/* the 8051 timer 2 counts up to 0xFFFF */
					ticks = 0xFFFF - ticks;
						
					unsigned char ticksHigh = static_cast< unsigned char >( ticks >> 8 ); 
					unsigned char ticksLow  = static_cast< unsigned char >( ticks & 0xFF ); 
					USB_DIMM_NUM_RELOAD( buf ) = reloadValues;
					USB_DIMM_RELOAD_HIGH( buf ) = ticksHigh; 
					USB_DIMM_RELOAD_LOW( buf ) = ticksLow;
					USB_DIMM_SIGNAL( buf ) = signal;
					USB_DIMM_INC( buf );
				}
			}
			i++; /* we generated another packet */
		}
		
	}		
	return ( buf - bufStart);
}


UsbBoxDimming::UsbBoxDimming( unsigned char signal0, unsigned char signal1 )
	: itsDutyCycle( USBBoxCommunication::frequencyTo8051Ticks( USB_BOX_DIMMING_FREQUENCY ) )
	, itsState( DIMM_BOTTOM )
	, itsCounter( DIMM_COUNTER )
{
	itsDimm[ 0 ].signal = signal0;
	itsDimm[ 0 ].start = 0;
	itsDimm[ 0 ].stop = 0;
	itsDimm[ 0 ].skip = 0;
	itsDimm[ 0 ].currValue = 0;	
	itsDimm[ 1 ].signal = signal1;
	itsDimm[ 1 ].start = 0;
	itsDimm[ 1 ].stop = 0;
	itsDimm[ 1 ].skip = 0;
	itsDimm[ 1 ].currValue = 0;	
}

UsbBoxDimming::~UsbBoxDimming( )
{
}