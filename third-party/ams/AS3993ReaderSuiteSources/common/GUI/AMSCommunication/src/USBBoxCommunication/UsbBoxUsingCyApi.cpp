/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "UsbBoxUsingCyApi.hxx"
#include <cyapi.h>
#include <windows.h>
#include <string.h>

bool UsbBoxUsingCyApi::isDriverAvailable ( )
{
	bool result = false;
	CCyUSBDevice * device = new CCyUSBDevice( 0, CYUSBDRV_GUID, true );
	if ( device->DeviceCount() > 0 )
	{
		result = true;
	}
	else /* did not find a cypress- guid usb device -> try for ams- guid usb device */
	{ 
		device = new CCyUSBDevice( 0, AMSUSBDRV_GUID, true );
		if ( device->DeviceCount() > 0 )
		{
			result = true;
		}
	}
	delete device;
	return result;
}


UsbBoxUsingCyApi::UsbBoxUsingCyApi ( )
: UsbBox( )
, itsDevice( 0 )
, itsEp0( 0 )
, itsEp2In( 0 )
, itsEp2Out( 0 )
, i2c_device_address_( 0 )
{
	itsDevice = new CCyUSBDevice( 0, CYUSBDRV_GUID, true );
	if ( itsDevice->DeviceCount() == 0 ) /* did not find a cypress- guid usb device -> try for ams- guid usb device */
	{ 
		itsDevice = new CCyUSBDevice( 0, AMSUSBDRV_GUID, true );
	}
	usbConnectFn( );
}

UsbBoxUsingCyApi::~UsbBoxUsingCyApi ( )
{
	if ( itsDevice )
	{
		itsDevice->Close( );
		delete itsDevice;
	}
}

bool UsbBoxUsingCyApi::getTaskFromInterface ( Interface desiredInterface, BYTE & task )
{
	switch ( desiredInterface )
	{
		case I2C:
			task = TSK_I2C;
			break;
		case SPI:
			task = TSK_SPI;
			break;
		case I2C_HardClock:
			task = TSK_I2C_HARD;
			break;
		case Standard_SPI:
			task = TSK_STANDARD_SPI;
			break;
		case PPTRIM:
			task = TSK_PPTRIM;
			break;
		case SSPI:
			task = TSK_SSPI;
			break;
		case SWI:
			task = TSK_SWI;
			break;
		default:
			task = TSK_UNKNOWN;
			return false;
	}
	return true;
}

UsbError UsbBoxUsingCyApi::sendReceive ( BYTE task, BYTE * out, BYTE * in )
{
	UsbError error;

	USB_TASK(out) = task;
	USB_STATE(out) = USB_STATE_NEW;

	error = usbTx(out);
	if (error == ERR_NO_ERR)
	{
		error = usbRx(in);
		if (error == ERR_NO_ERR)
		{	
			if (USB_TASK(in) != task)
			{
				error = ERR_CMD_FAILED;
			}
		}
	}
	return error;
}

UsbError UsbBoxUsingCyApi::usbTx ( BYTE * data, LONG num_bytes )
{
	LONG outBytes = num_bytes;
	if ( !itsEp2Out )
		return ERR_NOT_CONNECTED;

	if ( itsEp2Out->XferData( data, num_bytes ) 
	   && num_bytes == outBytes 
	   )
	{
		return ERR_NO_ERR;
	}
	return ERR_TRANSMIT;
}

UsbError UsbBoxUsingCyApi::usbRx ( BYTE * data )
{
	if ( !data )
		return ERR_ILLEGAL_ARGUMENTS;
	if ( !itsEp2In )
		return ERR_NOT_CONNECTED;
	
	LONG num_bytes = USB_PACKET_SIZE;
	if (  itsEp2In->XferData( data, num_bytes ) 
	   && num_bytes == USB_PACKET_SIZE 
	   )
	{
		return ERR_NO_ERR;
	}
	return ERR_RECEIVE;
}


UsbError UsbBoxUsingCyApi::usbConnectFn ( ) 
{
	if ( itsDevice->DeviceCount() > 0 )
	{
		if ( itsEp0 == 0 || itsEp2In == 0 )
		{
			itsDevice->Close( );
			itsDevice->Open( 0 ); /* open 0th device */
			itsEp0 = itsDevice->ControlEndPt;
			/* we have to select alternate settings 1 to have ep2 available */
			itsDevice->SetAltIntfc( 1 );
			/* we use bulk-endpoint 2 */
			CCyUSBEndPoint * ep2In;
			CCyUSBEndPoint * ep2Out;
			ep2In = itsDevice->EndPointOf( 0x82 );
			ep2Out = itsDevice->EndPointOf( 0x02 );
			if (  ep2In && ep2Out /* both exist */
			   && ep2In->Attributes == 2 /* is bulk */
		       && ep2Out->Attributes == 2 /* is bulk */
		       )
			{
				itsEp2In = dynamic_cast< CCyBulkEndPoint *>( ep2In );
				itsEp2Out = dynamic_cast< CCyBulkEndPoint *>( ep2Out );
				itsEp2In->Reset();
				itsEp2Out->Reset();
				return ERR_NO_ERR;
			}
		}
		else
		{
			return ERR_NO_ERR;
		}
	}
	else
	{
		itsDevice->Close();
		itsEp0 = 0;
		itsEp2In = 0;
		itsEp2Out = 0;
	}
	return ERR_CANNOT_CONNECT;
}

UsbError UsbBoxUsingCyApi::usbDisconnectFn ( ) 
{
	itsDevice->Close();
	itsEp0 = 0;
	itsEp2In = 0;
	itsEp2Out = 0;
	return ERR_NO_ERR;
}

bool UsbBoxUsingCyApi::usbIsConnectedFn ( ) 
{
	BYTE packet[USB_PACKET_SIZE];
	return ( sendReceive( TSK_PING, packet, packet ) == ERR_NO_ERR );
}

UsbError UsbBoxUsingCyApi::readFirmwareIdFn ( unsigned char * str ) 
{
	BYTE packet[USB_PACKET_SIZE];
	UsbError error = sendReceive( TSK_FW_ID, packet, packet ); 
	if ( error == ERR_NO_ERR )
	{
		const int strSize = strlen( reinterpret_cast< const char *>( USB_FW_ID_DATA(packet) ) );
		const int datSize = USB_PACKET_SIZE - ( USB_FW_ID_DATA(packet) - packet );
		strcpy_s( reinterpret_cast< char *>( str )
			    , MIN( strSize, datSize ) + 1
				, reinterpret_cast< const char * >( USB_FW_ID_DATA(packet) )
				);
	}
	return error;
}


UsbError UsbBoxUsingCyApi::usbDebugMsgFn ( unsigned char* str ) 
{
	BYTE packet[USB_PACKET_SIZE];
	UsbError error = sendReceive( TSK_DBG, packet, packet );
	if ( error == ERR_NO_ERR )
	{
		const int strSize = strlen( reinterpret_cast< const char *>( USB_DBG_DATA(packet) ) );
		const int datSize = USB_PACKET_SIZE - ( USB_DBG_DATA(packet) - packet );
		strcpy_s( reinterpret_cast< char *>( str )
			    , MIN( strSize, datSize ) + 1
				, reinterpret_cast< const char * >( USB_DBG_DATA(packet) ) 
				);
	}
	return error;
}

UsbError UsbBoxUsingCyApi::setI2CDevAddrFn ( unsigned char address ) 
{
	i2c_device_address_ = address;
	return ERR_NO_ERR;
}

UsbError UsbBoxUsingCyApi::setInterfaceFn ( Interface iface ) 
{
	itsInterface = iface;
	return ERR_NO_ERR;
}

UsbError UsbBoxUsingCyApi::writeByteFn ( unsigned char register_address, unsigned char value ) 
{
	BYTE packet[USB_PACKET_SIZE];
	BYTE task;

	if ( itsInterface == SWI )
	{
		return SWI_Write_AddressFn( itsSingleWirePin, register_address, value, itsSingleWireDelay );
	}
	else if ( getTaskFromInterface( itsInterface, task ) )
	{
		USB_SH_DEV_ADDR(packet) = i2c_device_address_;
		USB_SH_R_NW(packet) = USB_SERIAL_WRITE;
		USB_SH_START_ADDR(packet) = register_address;
		USB_SH_NUM_BYTES(packet) = 1;
		USB_SERIAL_DATA(packet)[0] = value;

		return sendReceive( task, packet, packet );

	}
	return ERR_GENERIC; /* unknown interface */
}


UsbError UsbBoxUsingCyApi::readByteFn(unsigned char register_address, unsigned char* value) 
{
	BYTE packet[USB_PACKET_SIZE];
	BYTE task;
	UsbError error = ERR_GENERIC; /* unknown interface */

	if ( getTaskFromInterface( itsInterface, task ) )
	{
		USB_SH_DEV_ADDR(packet) = i2c_device_address_;
		USB_SH_R_NW(packet) = USB_SERIAL_READ;
		USB_SH_START_ADDR(packet) = register_address;
		USB_SH_NUM_BYTES(packet) = 1;

		error = sendReceive( task, packet, packet );
		*value = USB_SERIAL_DATA(packet)[0];
	}
	return error;
}

UsbError UsbBoxUsingCyApi::blkWriteFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes) 
{
	BYTE packet[USB_PACKET_SIZE];
	BYTE task;
	UsbError error = ERR_GENERIC; /* unknown interface */

	if ( num_bytes > USB_MAX_SERIAL_SIZE )
		return ERR_ILLEGAL_ARGUMENTS; 
		 
	if ( getTaskFromInterface( itsInterface, task ) )
	{
		USB_SH_DEV_ADDR(packet) = i2c_device_address_;
		USB_SH_R_NW(packet) = USB_SERIAL_WRITE;
		USB_SH_START_ADDR(packet) = start_addr;
		USB_SH_NUM_BYTES(packet) = num_bytes;
		memmove(USB_SERIAL_DATA(packet), values, num_bytes);
		error = sendReceive( task, packet, packet );
	}
	return error;
}

UsbError UsbBoxUsingCyApi::blkReadFn ( unsigned char start_addr, unsigned char * values, unsigned char num_bytes ) 
{
	BYTE packet[USB_PACKET_SIZE];
	BYTE task;
	UsbError error = ERR_GENERIC; /* unknown interface */

	if ( num_bytes > USB_MAX_SERIAL_SIZE )
		return ERR_ILLEGAL_ARGUMENTS; 

	if ( getTaskFromInterface( itsInterface, task ) )
	{
		USB_SH_DEV_ADDR(packet) = i2c_device_address_;
		USB_SH_R_NW(packet) = USB_SERIAL_READ;
		USB_SH_START_ADDR(packet) = start_addr;
		USB_SH_NUM_BYTES(packet) = num_bytes;
		error = sendReceive( task, packet, packet );
		memmove( values, USB_SERIAL_DATA(packet), num_bytes );
	}
	return error;
}

UsbError UsbBoxUsingCyApi::readADCFn(unsigned char channel,unsigned* value) 
{
	BYTE packet[USB_PACKET_SIZE];
	USB_ADC_CHANNEL(packet) = channel;
	UsbError error = sendReceive( TSK_ADC, packet, packet );
	*value = (static_cast<unsigned>(USB_ADC_DATA(packet)[0])<<8) | USB_ADC_DATA(packet)[1];
	*value = *value >> 6;
	return error;
}

UsbError UsbBoxUsingCyApi::readPortFn(unsigned char *data, int io_port, unsigned char mask) 
{
	BYTE packet[USB_PACKET_SIZE];
	USB_PIO_TSK(packet) = USB_PIO_READ;
	USB_PIO_PORT(packet) = io_port;
	USB_PIO_MASK(packet) = mask;
	UsbError error = sendReceive( TSK_PIO, packet, packet );
	*data = USB_PIO_DATA(packet);
	return error;
}

UsbError UsbBoxUsingCyApi::writePortFn(unsigned char data, int io_port, unsigned char mask) 
{
	BYTE packet[USB_PACKET_SIZE];
	USB_PIO_TSK(packet) = USB_PIO_WRITE;
	USB_PIO_PORT(packet) = io_port;
	USB_PIO_MASK(packet) = mask;
	USB_PIO_DATA(packet) = data;
	return sendReceive( TSK_PIO, packet, packet );
}

UsbError UsbBoxUsingCyApi::setPortAsInputFn(int io_port, unsigned char mask) 
{
	BYTE packet[USB_PACKET_SIZE];
	USB_PIO_TSK(packet) = USB_PIO_SET_IN;
	USB_PIO_PORT(packet) = io_port;
	USB_PIO_MASK(packet) = mask;
	return sendReceive( TSK_PIO, packet, packet );
}

UsbError UsbBoxUsingCyApi::setPortAsOutputFn(int io_port, unsigned char mask)
{
	BYTE packet[USB_PACKET_SIZE];
	USB_PIO_TSK(packet) = USB_PIO_SET_OUT;
	USB_PIO_PORT(packet) = io_port;
	USB_PIO_MASK(packet) = mask;
	return sendReceive( TSK_PIO, packet, packet );
}

UsbError UsbBoxUsingCyApi::getPortDirectionFn(Port io_port,unsigned char* direction) 
{
	BYTE packet[USB_PACKET_SIZE];
	USB_PIO_TSK(packet) = USB_PIO_GET_DIR;
	USB_PIO_PORT(packet) = io_port;
	UsbError error = sendReceive( TSK_PIO, packet, packet );
	*direction = USB_PIO_DATA(packet);
	return error;
}


UsbError UsbBoxUsingCyApi::sendPacketFn(char *in,int in_length, const char *out, int out_length) 
{
	UsbError error = ERR_NO_ERR;

	if (out_length > USB_PACKET_SIZE)
		return ERR_ILLEGAL_ARGUMENTS;
	if (in_length != USB_PACKET_SIZE)
		return ERR_ILLEGAL_ARGUMENTS;

	if (out_length)
	{
		error = usbTx( const_cast< BYTE * >( reinterpret_cast< const BYTE * >( out ) ),out_length);
		if(error != ERR_NO_ERR)
			return error;
	}

	if (in_length)
	{
		error = usbRx(reinterpret_cast< BYTE * >( in ));
		if(error != ERR_NO_ERR)
			return error;
	}
	return ERR_NO_ERR;
}

UsbError UsbBoxUsingCyApi::SWI_Write_EN1Fn(BYTE num_bytes,BYTE* values,int delay)
{ /* no more supported by the usb-box v 2.50 and higher */
	return ERR_GENERIC;
}

UsbError UsbBoxUsingCyApi::SWI_Write_EN2Fn(BYTE num_bytes,BYTE* values,int delay)
{
	BYTE packet[USB_PACKET_SIZE];

	if ( num_bytes > USB_MAX_SERIAL_SIZE )
		return ERR_ILLEGAL_ARGUMENTS; 

	USB_TASK(packet) = TSK_SWI_EN2;
	USB_STATE(packet) = USB_STATE_NEW;
	USB_SH_DEV_ADDR(packet) = delay>>8;
	USB_SH_R_NW(packet) = USB_SERIAL_WRITE;
	USB_SH_START_ADDR(packet) = 0xFF&delay;
	USB_SH_NUM_BYTES(packet) = num_bytes;
	memmove( USB_SERIAL_DATA(packet), values, num_bytes);
	return usbTx(packet);
}

UsbError UsbBoxUsingCyApi::SWI_Write_AddressFn(BYTE outputPinSelect, BYTE registerAddress , BYTE value , BYTE delay )
{
	BYTE packet[USB_PACKET_SIZE];
	USB_TASK(packet) = TSK_SWI;
	USB_STATE(packet) = USB_STATE_NEW;
	USB_SH_DEV_ADDR(packet) = outputPinSelect;
	USB_SH_R_NW(packet) = registerAddress;
	USB_PIO_DATA(packet) = value;
	USB_SH_START_ADDR(packet) = delay;
	
	return usbTx(packet);
}

UsbError UsbBoxUsingCyApi::SWI_FLASHFn(int OnTime)                // no Transmission Just 2 Bytes stacked in the Header
{
	BYTE packet[USB_PACKET_SIZE];
	USB_TASK(packet) = TSK_SWI_FLASH;
	USB_STATE(packet) = USB_STATE_NEW;
	USB_SH_START_ADDR(packet) = OnTime>>8;
	USB_SH_NUM_BYTES(packet) = OnTime&0xFF;
	return usbTx(packet);
}


UsbError UsbBoxUsingCyApi::FusePPTrimFn( BYTE inverted , BYTE num_bytes )
{
	BYTE packet[USB_PACKET_SIZE];
	USB_SH_NUM_BYTES(packet) = num_bytes;
	USB_SH_START_ADDR(packet) = inverted;
	return sendReceive( TSK_PPTRIM_FUSE, packet, packet );
}

UsbError UsbBoxUsingCyApi::ReadSRAMFn( BYTE start_addr,BYTE sram_address,BYTE* values,BYTE num_bytes)
{
	BYTE packet[USB_PACKET_SIZE];
	UsbError error = ERR_GENERIC; /* unknown interface */

	if ( num_bytes > USB_MAX_SERIAL_SIZE )
		return ERR_ILLEGAL_ARGUMENTS; 

	USB_SH_DEV_ADDR(packet) = i2c_device_address_;
	USB_SH_R_NW(packet) = sram_address;                  
	USB_SH_START_ADDR(packet) = start_addr;
	USB_SH_NUM_BYTES(packet) = num_bytes;

	error = sendReceive( TSK_I2C_READSRAM, packet, packet );
	memmove( values, USB_SERIAL_DATA(packet), num_bytes );
	return error;
}

bool UsbBoxUsingCyApi::openDriver( )
{
	if ( itsDevice->DeviceCount() > 0 )
	{
		if ( itsEp0 == 0 )
		{
			itsDevice->Open( 0 ); /* open 0th device */
			itsEp0 = itsDevice->ControlEndPt;
			itsEp2In = 0;
		}
		return true;
	}
	return false;
}

bool UsbBoxUsingCyApi::closeDriver( )
{
	usbDisconnectFn( );
	return true;
}

#define CPUCS_REG_EZUSB    0x7F92

bool UsbBoxUsingCyApi::control8051( bool reset8051 )
{
	if ( itsEp0 )
	{
		itsEp0->Target = TGT_DEVICE;
		itsEp0->ReqType = REQ_VENDOR;
		itsEp0->Direction = DIR_TO_DEVICE;
		itsEp0->ReqCode = 0xA0;
		itsEp0->Value = CPUCS_REG_EZUSB;
		itsEp0->Index = 0;
		unsigned char buf[ USB_PACKET_SIZE ];
		buf[0] = static_cast< unsigned char >( reset8051 ? 1 : 0 );
		long buflen = 1;
		return itsEp0->XferData( buf, buflen );
	}
	return false;
}

bool UsbBoxUsingCyApi::download( bool toEeprom, char * buffer, int startAddress, int size )
{
	DWORD numberBytes = 0;
	int maxValue;
	if ( itsEp0 )
	{
		itsEp0->Target = TGT_DEVICE;
		itsEp0->ReqType = REQ_VENDOR;
		itsEp0->Direction = DIR_TO_DEVICE;
		itsEp0->Value = startAddress;
		unsigned char * buf = reinterpret_cast< unsigned char *>( buffer ); 
		if ( toEeprom )
		{
			itsEp0->ReqCode = 0xA2; /* specific request -> needs firmware support */
			itsEp0->Index = 0xBEEF;
			maxValue = 512; /* cyApi driver can only handle smaller requests */
		}
		else
		{
			if ( startAddress >= 0x2000 ) /* end of program xdata */
			{
				return false;
			}
			maxValue = USB_PACKET_SIZE; /* must force to split up request into 64-byte packets for ez-usb hw */
			itsEp0->ReqCode = 0xA0; 
			itsEp0->Index = 0; 
		}
		bool result = true;
		while ( size > 0 && result )
		{
			/* split up packet into smaller packets (either for ezusb hw - request 0xA0, or for cyapi driver - request 0xA2 */
			long buflen = MIN( maxValue, size );
			result = itsEp0->XferData( buf, buflen );
			buf += buflen; /* adjust pointer */
			size -= buflen;
			startAddress += buflen;
			itsEp0->Value = startAddress;
		}
		return result;
	}
	return false;
}

bool UsbBoxUsingCyApi::upload( char * buffer, short startAddress, short size, DWORD & numberBytes )
{
	if ( itsEp0 )
	{
		itsEp0->Target = TGT_DEVICE;
		itsEp0->ReqType = REQ_VENDOR;
		itsEp0->Direction = DIR_FROM_DEVICE;
		itsEp0->Value = startAddress;
		itsEp0->ReqCode = 0xA0;
		itsEp0->Index = 0; 
		long buflen = size;
		unsigned char * buf = reinterpret_cast< unsigned char *>( buffer ); 
		memset( buf, 0, size );
		bool result = itsEp0->XferData( buf, buflen );
		numberBytes = buflen;
		return result;
	}
	return false;
}



