/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   ams EZ-USB Box Firmware Loader
 *      $Revision: $
 *      LANGUAGE:  C++
 */

/*! \file   UsbBoxUsbBoxFirmwareLoad.cpp
 *  \author M. Arpa 
 *
 *  \brief  Implementation to download 8051 files to the XDATA in the ez usb box
 *          or to the i2c eeprom in the ez usb box.
 *
 *          The implementation is based on one of Cypress - modified by M. 
 *          Schickbichler. However most of the information was lost and had
 *          to be reverse-engineered by the author.
 */

#include "UsbBoxFirmwareLoad.h"
#include "UsbBoxReporter.h"
#include "ezusbsys.h"
#include "Vend_Ax.h"
#include <QByteArray>
#include <QByteArrayMatcher>
#include <QFileInfo>
#include <QString>
#include <QTime>
#include "UsbBox.hxx"
#include "AmsFirmwareCheck.h"

/* make a default instanciation of the void itsReporter */
UsbBoxReporter UsbBoxFirmwareLoad::theDummyReporter;

UsbBoxFirmwareLoad::UsbBoxFirmwareLoad ( UsbBoxReporter * report, bool useAmsVidPid ) 
: itsReporter( report ? *report : theDummyReporter )
, itsUseAmsVidPid( useAmsVidPid )
{
}

UsbBoxFirmwareLoad::~UsbBoxFirmwareLoad ( )
{
}


bool UsbBoxFirmwareLoad::downloadToXdata ( const QString & fullFilename )
{
	if ( fullFilename.isEmpty() )
	{
	}
	else /* start converting, downloading ... */
	{
		itsFilename = &fullFilename;
		return loadControl( false );
	}
	return false;
}

bool UsbBoxFirmwareLoad::downloadToEeprom ( const QString & fullFilename )
{
	if ( fullFilename.isEmpty() )
	{
	}
	else /* start converting, downloading ... */
	{
		itsFilename = &fullFilename;
		return loadControl( true );
	}
	return false;
}

bool UsbBoxFirmwareLoad::downloadIntelHexRecords ( INTEL_HEX_RECORD * toDownload, bool toEeprom, int maxProgress )
{
	unsigned char * data = static_cast< unsigned char * >( toDownload->Data );
	int downloadLength = 0;
	int downloadOffset = toDownload->Address; /* start of segment */
	bool result = true;
	int progressSteps = ( toDownload->Length + ( MAX_EP0_XFER_SIZE - 8 ) - 1 ) / ( MAX_EP0_XFER_SIZE - 8 );
	int progress = maxProgress / progressSteps;

	while ( result && toDownload->Length > ( MAX_EP0_XFER_SIZE - 8 ) )
	{ /* record data is longer than we can pack into one usb packet -> split it */
		downloadLength = ( MAX_EP0_XFER_SIZE - 8 );
		memcpy( itsBlockBuffer, data, downloadLength );
		result = UsbBox::instance().download( toEeprom, itsBlockBuffer, downloadOffset, downloadLength );
		itsReporter.report( progress );
		data += downloadLength;
		toDownload->Length -= downloadLength;
		downloadOffset += downloadLength;
	}
	downloadLength = toDownload->Length;
	memcpy( itsBlockBuffer, data, downloadLength );
	result = UsbBox::instance().download( toEeprom, itsBlockBuffer, downloadOffset, downloadLength );
	itsReporter.report( maxProgress - (progress * (progressSteps - 1)) );
	return result;
}

bool UsbBoxFirmwareLoad::uploadFromXdata ( const QString & startAddress, const QString & uploadSize, bool reset8051, bool ascii )
{
	DWORD receivedBytes;
	QString str = startAddress;
	bool success;
	int address;
	int size;
	int i;
	int j;
	address = str.toInt( &success, 16 );
	if ( !success ) 
	{
		address = str.toInt( &success, 10 ); 
	}
	if ( !success )
	{		/* bail out */
		return false;
	}
	str = uploadSize;
	size = str.toInt( &success, 16 );
	if ( !success )
	{
		size = str.toInt( &success, 10 );
	}
	if ( !success )
	{
		return false;
	}
	if ( size > MAX_EP0_XFER_SIZE - 1 )
	{
		size = MAX_EP0_XFER_SIZE - 1; /* reduce size - need 1 byte for /0 terminator */
		itsReporter.report( QObject::tr( "WARNING at upload: Can only read up to %1 bytes" ).arg( size ) );
		itsReporter.report( QObject::tr( "") );
	}	
	/* now do the upload */
	if ( ! UsbBox::instance().openDriver( ) )
	{
		itsReporter.report( "ERROR at upload: Connect to Usb Driver failed. Aborting" );
		return false; /* bail out */
	}
	if ( reset8051 )
	{
		if ( ! UsbBox::instance().control8051( true ) )
		{
			itsReporter.report( "ERROR at upload: Could not reset 8051 in Usb Box. Aborting" );
			return false; /* bail out */
		}
	}
	if ( ! UsbBox::instance().upload( itsBlockBuffer, address, size, receivedBytes ) )
	{
		itsReporter.report( QObject::tr( "ERROR at upload: upload failed" ) );
		itsReporter.report( QObject::tr( "" ) );
		success = false;
	}
	if ( reset8051 )
	{
		if ( ! UsbBox::instance().control8051( false ) )
		{
			itsReporter.report( QObject::tr( "ERROR at upload: Could not let 8051 out of Reset." ) );
			itsReporter.report( QObject::tr( "") );
			success = false;
		}
	}
	if ( ! UsbBox::instance().closeDriver( ) )
	{
		itsReporter.report( QObject::tr( "ERROR at upload: Could not disconnect from usb driver." ) );
		itsReporter.report( QObject::tr( "") );
		success = false;
	}
	itsBlockBuffer[ receivedBytes ] = '\0'; /* add terminator */
	itsReporter.report2( QObject::tr( "Uploaded from Address 0x%1 0x%2 Bytes" ).arg( address, 0, 16 ).arg( receivedBytes, 0, 16 ) );

	if ( ascii )
	{
		str.clear();
		for ( i = 0; i < static_cast< int >( receivedBytes ); ++i )
		{
			if ( itsBlockBuffer[ i ] < 20 || itsBlockBuffer[ i ] > 127 )
			{
				itsBlockBuffer[ i ] = '.'; /* substitute for a printable character */
			}
		}
		str = itsBlockBuffer;
		i = str.size();
		for ( i = 0; i < (size + 63 ) / 64; i++ )
		{
			QString str2 = str.left( 64 );
			str.remove( 0, 64 );
			itsReporter.report2( str2 );
		}
	}
	else /* show hex formatted data */
	{
		for ( i = 0; i < static_cast<int>((receivedBytes + 31 ) / 32); i++ )
		{
			str.clear();
			for ( j = 0; j < 32; ++j )
			{
				QString str2;
				unsigned int number = itsBlockBuffer[i*64+j];
				str2.setNum( number, 16 );
				if ( number < 16 )
				{
					str2.prepend( "0" );
				}
				str2 = str2.right( 2 ); /* cut out leading F's */
				str2.append( " " );
				str.append( str2 );
			}
			itsReporter.report2( str );
		}
	}
	itsReporter.report2( "" );
	return success;
}


bool UsbBoxFirmwareLoad::loadControl ( bool toEeprom )
{
	QFileInfo info( *itsFilename );
	QString file = info.fileName();
	QTime duration;
	itsReporter.report( -1 ); /* reset itsReporter */
	duration.start();
	if ( toEeprom )
	{
		itsReporter.report( QObject::tr("Start of download to EEPROM at %1").arg( getTime() ) );
		itsReporter.report( 2 );
	}
	else
	{
		itsReporter.report( QObject::tr( "Start of download to XDATA at %1").arg( getTime() ) );
		itsReporter.report( QObject::tr( "Start converting file %1 at %2").arg( file ).arg( getTime() ) );
		itsReporter.report( 2 );
		if ( !convert( ) ) 
		{
			itsReporter.report( "ERROR: Conversion of file failed. Aborting download" );
			itsReporter.report( -1 ); 
			UsbBox::instance().closeDriver( );
			return false; /* bail out */
		}
		itsReporter.report( 38 );
	}

	itsReporter.report( QObject::tr( "Connect to Usb Driver at %1" ).arg( getTime() ) );
	if ( ! UsbBox::instance().openDriver( ) )
	{ /* connect to usb failed -> bail out */
		itsReporter.report( "ERROR: Connect to Usb Driver failed. Aborting download" );
		itsReporter.report( -1 ); 
		UsbBox::instance().closeDriver( );
		return false; /* bail out */
	}
	itsReporter.report( 2 );

	itsReporter.report( QObject::tr( "Reset and Halt 8051 in Usb Box at %1" ).arg( getTime() ) );
	if ( ! UsbBox::instance().control8051( true ) )
	{ /* putting 8051 into reset failed -> bail out */
		itsReporter.report( "ERROR: Could not reset 8051 in Usb Box. Aborting download" );
		itsReporter.report( -1 ); 
		UsbBox::instance().closeDriver( );
		return false; /* bail out */
	}
	itsReporter.report( 2 );

	if ( toEeprom )
	{
		itsReporter.report( QObject::tr( "Start downloading EEPROM Programmer to XDATA at %1" ).arg( getTime() ) );
		itsReporter.report( 2 );
		if ( !downloadIntelHexRecords ( Vend_Ax, false, 10 ) ) /* download the programmer to xdata */
		{ /* downloading programmer failed -> bail out */
			itsReporter.report( "ERROR: Could not load EEPROM programmer to 8051 in Usb Box. Aborting download" );
			itsReporter.report( -1 ); 
			UsbBox::instance().closeDriver( );
			return false; /* bail out */
		}
		itsReporter.report( QObject::tr( "Start converting file %1 at %2").arg( file ).arg( getTime() ) );
		itsReporter.report( 2 );
		/* usb driver does not block until transferred so we do the conversion here to spend some time */
		if ( !convert( ) )
		{
			itsReporter.report( "ERROR: Conversion of file failed. Aborting download" );
			itsReporter.report( -1 ); 
			UsbBox::instance().closeDriver( );
			return false; /* bail out */
		}
		itsReporter.report( QObject::tr( "Restart 8051 in Usb Box at %1" ).arg( getTime() ) );
		itsReporter.report( 6 );
		if ( ! UsbBox::instance().control8051( false ) )
		{ /* let 8051 go failed -> bail out */
			itsReporter.report( "ERROR: Could not let 8051 in Usb Box out of reset. Aborting download." );
			itsReporter.report( -1 ); 
			UsbBox::instance().closeDriver( );
			return false; /* bail out */
		}
		itsReporter.report( QObject::tr( "Start downloading converted data to EEPROM at %1" ).arg( getTime() ) );
		itsReporter.report( 2 );
		downloadIntelHexRecords( &(itsIntelHexRecord[1]), true, 70 );
	}	
	else /* download to xdata */
	{
		itsReporter.report( QObject::tr( "Start downloading converted data to XDATA at %1" ).arg( getTime() ) );
		itsReporter.report( 2 );
		downloadIntelHexRecords( &(itsIntelHexRecord[0]), false, 50 );

		itsReporter.report( QObject::tr( "Restart 8051 in Usb Box at %1" ).arg( getTime() ) );
		if ( ! UsbBox::instance().control8051( false ) )
		{ /* let 8051 go failed -> bail out */
			itsReporter.report( "ERROR: Could not let 8051 in Usb Box out of reset. Aborting download." );
			itsReporter.report( -1 ); 
			UsbBox::instance().closeDriver( );
			return false; /* bail out */
		}
		itsReporter.report( 2 );
	}

	itsReporter.report( QObject::tr( "Disconnect from Usb Driver at %1" ).arg( getTime() ) );
	if ( ! UsbBox::instance().closeDriver( ) )
	{ /* disconnect to usb failed inform user but ignore otherwise */
		itsReporter.report( "ERROR: Connect to Usb Driver failed. Download finished anyway." );
		UsbBox::instance().closeDriver( );
	}
	itsReporter.report( 2 );
	itsReporter.report( QObject::tr( "Finished at %1, total Time: %2 milli-seconds" ).arg( getTime() ).arg( duration.elapsed() ) );
	if ( toEeprom )
	{
		itsReporter.report( QObject::tr( "You must disconnect and reconnect your usb box to reboot with the downloaded application" ) );
	}
	itsReporter.report( QObject::tr( "") );
	itsReporter.report( 100 );
	return true;
}

bool UsbBoxFirmwareLoad::convert ( bool writeIICFile )
{
	unsigned char amsVidPidStart[7] = { 0xb2, 0x25, 0x13, 0x01, 0x00, 0x00, 0x00 }; /* ams vid/pid */
	unsigned char cypressVidPidStart[7] = { 0xb2, 0x47, 0x05, 0x31, 0x21, 0x00, 0x00 }; /* cypress vid/pid */
	unsigned char * start = ( itsUseAmsVidPid ? amsVidPidStart : cypressVidPidStart );
	unsigned char end [5] = { 0x80, 0x01, 0x7f, 0x92, 0x00 };
	unsigned char record[4] = { 0x00 /* size HighByte */
							  , 0x00 /* size LowByte */
							  , 0x00 /* start address LowByte */
							  , 0x00 /* start address HighByte */
							  };
	int length; /* length of the segment (for eeprom -> max size of 0x3ff) */
	int address = 0; /* start address of this segment (for eeprom -> with max size of 0x3FF) */
	int maxLength = 0;
	int type;
	int index;
	unsigned char data;
	bool success;
	int i;
	QString str;
	QFile file( *itsFilename );
	if ( itsUseAmsVidPid )
	{
		itsReporter.report( QObject::tr( "Using ams VID/PID" ) );
	}
	else
	{
		itsReporter.report( QObject::tr( "Using alternate VID/PID" ) );
	}

	itsIntelHexRecord[0].Length = 0;
	itsIntelHexRecord[0].Address = 0;
	itsIntelHexRecord[0].Type = 0; 
	itsIntelHexRecord[1].Length = 0;
	itsIntelHexRecord[1].Address = 0;
	itsIntelHexRecord[1].Type = 0; 
	/* intel hex record 0 contains the file for xdata */
	/* intel hex record 1 contains the file for eeprom */
	for ( i = 0; i < MAX_INTEL_HEX_RECORD_LENGTH; ++i )
	{
		itsIntelHexRecord[0].Data[i] = 0xAA;
		itsIntelHexRecord[1].Data[i] = 0xAA;
	}

	if ( !file.exists( ) )
	{
		itsReporter.report( QObject::tr( "ERROR: Could not find file %1" ).arg( *itsFilename ) );
		return false; /* bail out */
	}

	if ( !file.open( QIODevice::ReadOnly ) )
	{
		itsReporter.report( QObject::tr( "ERROR: Could not open file %1" ).arg( *itsFilename ) );
		return false; /* bail out */
	}

	/* intel hex record 0 contains the file for xdata */
	/* intel hex record 1 contains the file for eeprom */

	/* ------------------------ now generate the intel hex record for xdata download  ------------ */
	while ( ! file.atEnd( ) )
	{
		/* intel hex records we interpret as follow: 
		   :llaaaattdddd...dddcc
		   l=length a=address t=type d=data c=checksum */
		if ( 1 != file.readLine( itsBlockBuffer, 2 ) ) /* read colon */
		{
		}
		if ( 2 != file.readLine( itsBlockBuffer, 3 ) ) /* read length */
		{
		}
		str = itsBlockBuffer;
		length = str.toInt( &success, 16 );
		if ( !success )
		{ /* malformated intel hex record */
		}
		if ( 4 != file.readLine( itsBlockBuffer, 5 ) ) /* read Address */
		{
		}
		str = itsBlockBuffer;
		address = str.toInt( &success, 16 );
		if ( !success )
		{ /* malformated intel hex record */
		}
		if ( length + address > maxLength )
		{
			maxLength = length + address;
		}
		if ( 2 != file.readLine( itsBlockBuffer, 3 ) ) /* read Type */
		{
		}
		str = itsBlockBuffer;
		type = str.toInt( &success, 16 );
		if ( !success || !( type == 0 || type == 1 ) )
		{ /* malformated intel hex record */
		}
		for ( i = 0; i < length; ++i )
		{ /* fill data into hex record */
			if ( 2 != file.readLine( itsBlockBuffer, 3 ) ) /* read Data */
			{
			}
			str = itsBlockBuffer;
			data = str.toInt( &success, 16 );
			if ( !success )
			{
			}
			itsIntelHexRecord[0].Data[ address + i ] = data;
		}

		if ( 2 < file.readLine( itsBlockBuffer, 5 ) ) /* read CRC + cr/lf */
		{
		}
	}
	file.close( );
	itsIntelHexRecord[0].Length = maxLength;

	/* ------------- now generate the intel hex record for eeprom download ------------- */
	/* add prefix to file */
	for ( index = 0, i = 0; i < sizeof( amsVidPidStart ); ++index, ++i )
	{
		itsIntelHexRecord[1].Data[index] = start[i];
	}
	/* split up content of file into segments with max size of 0x3FF */
	while ( maxLength > 0 )
	{
		length = maxLength;
		if ( length > 0x3FF )
		{ /* we split up into 0x3FF sized segments */
			length = 0x3FF;
		}
		record[0] = 0xFF & (length >>  8); /* size HighByte */
		record[1] = 0xFF &  length;        /* size LowByte */
		record[2] = 0xFF & (address >> 8); /* address HighByte */
		record[3] = 0xFF &  address;       /* address LowByte */
		for ( i = 0; i < 4; ++index, ++i )
		{ /* write out record info */
			itsIntelHexRecord[1].Data[index] = record[i];
		}
		for ( i = 0; i < length; ++index, ++i )
		{ /* write out data */
			itsIntelHexRecord[1].Data[index] = itsIntelHexRecord[0].Data[address+i];
		}
		address += length; /* next segment will have this as its start address */
		maxLength -= length; /* we already wrote this amount of data to the file */
	}

	/* add suffix to file */
	for ( i = 0; i < sizeof( end ); ++index, ++i )
	{
		itsIntelHexRecord[1].Data[index] = end[i];
	}
	itsIntelHexRecord[1].Length = index;

	/* ---------------- if the user has selected -> we generate an iic file --------------- */
	if ( writeIICFile )
	{
		str = *itsFilename;
		QFileInfo info( str );
		QString basename = info.baseName( ).append( ".iic" );
		str = info.canonicalPath( ).append( "/" ).append( basename );
		QFile outFile( str );
		if ( !outFile.open( QIODevice::WriteOnly ) )
		{
		}
		for ( i = 0; i < itsIntelHexRecord[1].Length; ++i )
		{
			outFile.putChar( itsIntelHexRecord[1].Data[i] );
		}
		outFile.close( );
	}

	return true;
}


bool UsbBoxFirmwareLoad::versions ( const QString & fullFilename, QString & fromBox, QString & fromFile, QString & result )
{

	QByteArrayMatcher match ( QByteArray( "Firmware Ver." ) );
	const char * data = reinterpret_cast< const char *>( itsIntelHexRecord[0].Data );
	int found = -1;
	int address = 0;
	DWORD i;
	DWORD receivedBytes = 0;

	fromFile = "No Version found";
	fromBox = "No Version found";

	/* read out xdata first -> search for version string */
	if ( UsbBox::instance().openDriver( ) )
	{
		while ( address < 0x2000 ) /* cypress has only 8 kb */
		{
			if (  UsbBox::instance().upload( itsBlockBuffer, address, 0x100, receivedBytes ) 
			   && receivedBytes > 0 
			   )
			{
				for ( i = 0; i < receivedBytes; i++ )
				{
					itsIntelHexRecord[ 0 ].Data[ address + i ] = itsBlockBuffer[ i ];
				}
				address += receivedBytes;
			}
			if ( receivedBytes == 0 )
			{
				break; 
			}
		}
		UsbBox::instance().closeDriver( );
		itsIntelHexRecord[ 0 ].Length = address;
		found = match.indexIn( data, itsIntelHexRecord[0].Length, 0 );
		if ( found >= 0 )
		{
			fromBox = &(data[ found ]);
			addMajorNumber( fromBox );
		}
	}

	/* convert hex file -> search for version string */
	if ( fullFilename.isEmpty() )
	{
	}
	else /* start converting, downloading ... */
	{
		itsFilename = &fullFilename;
		if ( convert ( false ) )
		{
			found = match.indexIn( data, itsIntelHexRecord[0].Length, 0 );
			if ( found >= 0 )
			{
				fromFile = &(data[ found ]);	
				addMajorNumber( fromFile );
			}
		}
	}



	QString boxV = version( fromBox );
	QString fileV = version( fromFile );
    return AmsFirmwareCheck::isCompatible(fileV, boxV, result);
}

void UsbBoxFirmwareLoad::addMajorNumber( QString & versionString )
{
	if ( versionString.contains( "Firmware Ver. " ) )
	{
		versionString.remove( "Firmware Ver. " );
		versionString.prepend( "1." );
		versionString.prepend("Firmware Ver. " );
	}
}

QString UsbBoxFirmwareLoad::version ( const QString & versionString )
{
	if ( versionString.contains( "Firmware Ver." ) )
	{
		QString v( versionString );
		v.remove( "Firmware Ver." );
        return v;
	}
	return versionString;
}


void UsbBoxFirmwareLoad::changeVidPid ( bool useAmsVidPid )
{
	itsUseAmsVidPid = useAmsVidPid;
}