/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  
 *
 */

#include "UsbBoxPwm.h"

bool UsbBoxPwm::addPoint ( DWORD ticks, unsigned char signal, bool isHigh ) 
	{
		bool result = false;
		unsigned int i;
		if ( ticks == itsCycle )
		{
			ticks = 0;
		}
		for ( i = 0; i < itsMax; ++i )
		{
			if ( ticks < itsTicks[ i ] )
			{ /* move all bigger elements up one position */
				for ( unsigned int j = itsMax; j > i; j-- )
				{
					itsTicks     [ j ] = itsTicks     [ j - 1 ];
					itsSignalHigh[ j ] = itsSignalHigh[ j - 1 ];
					itsSignalLow [ j ] = itsSignalLow [ j - 1 ];
				}
				break; /* leave loop at current index i */
			}
			else if ( ticks == itsTicks[ i ] )
			{
				if ( isHigh )
				{
					itsSignalHigh[ i ] |= signal;
				}
				else
				{
					itsSignalLow [ i ] |= signal;
				}
				return true;
			}
		}
		/* add signal at the current position */
		itsTicks[ i ] = ticks; 
		if ( isHigh )
		{
			itsSignalHigh[ i ] = signal;
			itsSignalLow [ i ] = 0;
		}
		else
		{
			itsSignalHigh[ i ] = 0;
			itsSignalLow [ i ] = signal;
		}

		if ( itsMax < PWM_MAX_SIGNAL_CHANGES )
		{
			itsMax++; 
			result = true;
		}
		itsTicks[ itsMax ] = itsCycle; /* for getDuration */
		return result;
	};


bool UsbBoxPwm::getDuration ( unsigned int index, DWORD & ticks, unsigned char & signal ) const 
	{ 
		if ( index < itsMax )
		{
			signal = 0;
			ticks = itsTicks[ index + 1 ] - itsTicks[ index ];
			for ( unsigned int i = 0; i <= index; ++i )
			{
				signal &= ~itsSignalLow[ i ];
				signal |= itsSignalHigh[ i ];
			}
			return true;
		}
		return false;
	};
