/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "UsbBoxUsingUsbConDll.hxx"
#include <windows.h>
#include "ezusbsys.h"

#define WRITE_MODE true
#define READ_MODE false

bool UsbBoxUsingUsbConDll::isDriverAvailable ( )
{
	bool result = false;
    char * device = "\\\\.\\ezusb-0";
    HANDLE handle = CreateFileA( device, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL );
	if ( handle != INVALID_HANDLE_VALUE )
	{
		HINSTANCE hinstLib = LoadLibraryA("./libraries/usbcon.dll");
		if ( hinstLib != NULL )
		{
			result = true; /* driver and usb con dll are available */
			FreeLibrary(hinstLib);
		}
		CloseHandle( handle );
	}
	return result;
}


UsbBoxUsingUsbConDll::UsbBoxUsingUsbConDll( )
: UsbBox( )
, hinstLib( NULL )
, itsUsbDriver( INVALID_HANDLE_VALUE )
{
	hinstLib = LoadLibraryA("./libraries/usbcon.dll");
	if ( hinstLib == NULL )
		return;

	usbConnect        =	(AMSusbConnect)				GetProcAddress(hinstLib, "@AMSusbConnect$qv");
	usbDisconnect     = (AMSusbDisconnect)			GetProcAddress(hinstLib, "@AMSusbDisconnect$qv");
	readByteFunction  = (AMSusbReadByte)			GetProcAddress(hinstLib, "@AMSreadByte$qucpuc");
	writeByteFunction = (AMSusbWriteByte)			GetProcAddress(hinstLib, "@AMSwriteByte$qucuc");
	setI2CDeviceAddressFunction = (AMSusbSetI2CDevAddr) GetProcAddress(hinstLib, "@AMSsetI2CDevAddr$quc");
	setPortAsOutput	  = (AMSsetPortAsOutput)		GetProcAddress(hinstLib, "@AMSsetPortAsOutput$q4Portuc");
	setPortAsInput	  = (AMSsetPortAsInput)			GetProcAddress(hinstLib, "@AMSsetPortAsInput$q4Portuc");
	writePort		  = (AMSwritePort)				GetProcAddress(hinstLib, "@AMSwritePort$quc4Portuc");
	readPort		  = (AMSreadPort)				GetProcAddress(hinstLib, "@AMSreadPort$qpuc4Portuc");
	usbIsConnected    = (AMSusbIsConnected)         GetProcAddress(hinstLib, "@AMSusbIsConnected$qv");
	usbFirmwareID     = (AMSusbFirmwareID)          GetProcAddress(hinstLib, "@AMSusbFirmwareID$qpuc");
	usbDebugMsg       = (AMSusbDebugMsg)            GetProcAddress(hinstLib, "@AMSusbDebugMsg$qpuc");
	setInterface      = (AMSsetInterface)           GetProcAddress(hinstLib, "@AMSsetInterface$q9Interface");
	blkRead           = (AMSblkRead)                GetProcAddress(hinstLib, "@AMSblkRead$qucpucuc");
	blkWrite          = (AMSblkWrite)               GetProcAddress(hinstLib, "@AMSblkWrite$qucpucuc");
	readADC           = (AMSreadADC)                GetProcAddress(hinstLib, "@AMSreadADC$qucpui");
	getPortDirection  = (AMSgetPortDirection)       GetProcAddress(hinstLib, "@AMSgetPortDirection$q4Portpuc");
	sendPacketDLL     = (AMSsendPacket)             GetProcAddress(hinstLib, "@AMSsendPacket$qpucit1i");
	singleWireWrite   = (AMS_SWI_Write_Address)     GetProcAddress(hinstLib, "@AMS_SWI_Write_Address$qucucucuc");
}

UsbBoxUsingUsbConDll::~UsbBoxUsingUsbConDll()
{
    if ( hinstLib != NULL )
		FreeLibrary(hinstLib);
}

UsbError UsbBoxUsingUsbConDll::blkWriteFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes) 
{
	return blkWrite( start_addr, values, num_bytes );
}

UsbError UsbBoxUsingUsbConDll::blkReadFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes) 
{
	return blkWrite( start_addr, values, num_bytes );
}

UsbError UsbBoxUsingUsbConDll::getPortDirectionFn(Port io_port,unsigned char* direction) 
{
	return getPortDirection( io_port, direction );
}

UsbError UsbBoxUsingUsbConDll::readADCFn(unsigned char channel,unsigned* value) 
{
	return readADC( channel, value );
}

UsbError UsbBoxUsingUsbConDll::readPortFn(unsigned char *data, int ioPort, unsigned char mask) 
{
	return readPort( data, ioPort, mask );
}

UsbError UsbBoxUsingUsbConDll::sendPacketFn(char *in,int in_length, const char *out, int out_length) 
{
	return sendPacketDLL( in, in_length, out, out_length );
}

UsbError UsbBoxUsingUsbConDll::setPortAsInputFn(int ioPort, unsigned char mask) 
{
	return setPortAsInput( ioPort, mask );
}

UsbError UsbBoxUsingUsbConDll::setPortAsOutputFn(int ioPort, unsigned char mask)
{
	return setPortAsOutput( ioPort, mask );
}

UsbError UsbBoxUsingUsbConDll::setInterfaceFn(Interface iface) 
{
	itsInterface = iface; 
	return setInterface( iface );
}

bool UsbBoxUsingUsbConDll::usbIsConnectedFn() 
{
	return usbIsConnected( );
}

UsbError UsbBoxUsingUsbConDll::readFirmwareIdFn(unsigned char * str) 
{
	return usbFirmwareID( str );
}

UsbError UsbBoxUsingUsbConDll::usbDebugMsgFn(unsigned char* str) 
{
	return usbDebugMsg( str );
}

UsbError UsbBoxUsingUsbConDll::usbConnectFn(void) 
{
	return usbConnect( );
}

UsbError UsbBoxUsingUsbConDll::usbDisconnectFn(void) 
{
	return usbDisconnect( );
}

UsbError UsbBoxUsingUsbConDll::readByteFn(unsigned char registerAddress, unsigned char* registerValue) 
{
	return readByteFunction( registerAddress, registerValue );
}

UsbError UsbBoxUsingUsbConDll::setI2CDevAddrFn(unsigned char address) 
{
	return setI2CDeviceAddressFunction( address );
}

UsbError UsbBoxUsingUsbConDll::writePortFn(unsigned char data, int ioPort, unsigned char mask) 
{
	return writePort( data, ioPort, mask );
}

UsbError UsbBoxUsingUsbConDll::writeByteFn(unsigned char registerAddress, unsigned char registerValue) 
{
	if ( itsInterface == SWI )
	{
		return singleWireWrite( itsSingleWirePin, registerAddress, registerValue, itsSingleWireDelay );
	}
	else
	{
		return writeByteFunction( registerAddress, registerValue );
	}
}

bool UsbBoxUsingUsbConDll::openDriver ( )
{
    char * device = "\\\\.\\ezusb-0";
    itsUsbDriver = CreateFileA( device, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL );
    return ( itsUsbDriver != INVALID_HANDLE_VALUE );
}

bool UsbBoxUsingUsbConDll::closeDriver ( )
{
	bool result = true;
	if ( itsUsbDriver != INVALID_HANDLE_VALUE )
	{
		result = CloseHandle( itsUsbDriver );
		itsUsbDriver = INVALID_HANDLE_VALUE;
	}
	return result;
}

bool UsbBoxUsingUsbConDll::control8051 ( bool reset8051 )
{
	if ( itsUsbDriver != INVALID_HANDLE_VALUE )
	{
		VENDOR_REQUEST_IN request;
		/* mar: need to clean the complete memory (also the holes betweeen the elements, 
		   otherwise garbage is written on the line */
		memset( &request, 0, sizeof( request ) );
		DWORD numberBytes;
		request.bRequest = ANCHOR_LOAD_INTERNAL;
		request.wValue = CPUCS_REG_EZUSB;
		request.wIndex = 0x00;
		request.wLength = 0x01;
		request.bData = static_cast< char >( reset8051 ? 0x01 : 0x00 );
		request.direction = 0x00;
		return DeviceIoControl( itsUsbDriver, IOCTL_Ezusb_VENDOR_REQUEST , &request,	sizeof( VENDOR_REQUEST_IN ), NULL, 0, &numberBytes,	NULL );
	}
	return false;
}


bool UsbBoxUsingUsbConDll::download ( bool toEeprom, char * buffer, int startAddress, int size )
{
	DWORD numberBytes = 0;
	if ( toEeprom )
	{
		VENDOR_OR_CLASS_REQUEST_CONTROL request;
		/* mar: need to clean the complete memory (also the holes betweeen the elements, 
		   otherwise garbage is written on the line */
		memset( &request, 0, sizeof( request ) );
		if ( itsUsbDriver != INVALID_HANDLE_VALUE )
		{	
			request.request=0xA2; /* specific request -> needs firmware support */
			request.value=startAddress;
			request.index=0xBEEF;
			request.direction=0x00; /* out */
			request.requestType=2; /* vendor specific request type (2) */
			request.recepient=0; /* recepient is device (0) */
			return DeviceIoControl( itsUsbDriver, IOCTL_EZUSB_VENDOR_OR_CLASS_REQUEST, &request, sizeof(VENDOR_OR_CLASS_REQUEST_CONTROL), buffer, size, &numberBytes, NULL );
		}
	}
	else
	{
		ANCHOR_DOWNLOAD_CONTROL downloadControl;
		/* mar: need to clean the complete memory (also the holes betweeen the elements, 
		   otherwise garbage is written on the line */
		memset( &downloadControl, 0, sizeof( downloadControl ) );
		downloadControl.Offset = startAddress;  
		if ( itsUsbDriver != INVALID_HANDLE_VALUE )
		{
			if ( downloadControl.Offset < 0x2000 ) /* end of program xdata */
			{
				return DeviceIoControl( itsUsbDriver, IOCTL_EZUSB_ANCHOR_DOWNLOAD, &downloadControl, sizeof(ANCHOR_DOWNLOAD_CONTROL), buffer, size, &numberBytes, NULL );
			}
		}
	}
	return false;
}


bool UsbBoxUsingUsbConDll::upload ( char * buffer, short startAddress, short size, DWORD & numberBytes )
{
	if ( itsUsbDriver != INVALID_HANDLE_VALUE )
	{
		VENDOR_REQUEST_IN request;
		/* mar: need to clean the complete memory (also the holes betweeen the elements, 
		   otherwise garbage is written on the line */
		memset( &request, 0, sizeof( request ) );
		request.bRequest = 0xA0;
		request.wValue = startAddress;
		request.wIndex = 0x00; /* must be zero */
		request.wLength = size;
		request.bData = 0x00;
		request.direction = 0x01;
		return DeviceIoControl( itsUsbDriver, IOCTL_Ezusb_VENDOR_REQUEST , &request, sizeof( VENDOR_REQUEST_IN ), buffer, size, &numberBytes, NULL );
	}
	return false;
}
