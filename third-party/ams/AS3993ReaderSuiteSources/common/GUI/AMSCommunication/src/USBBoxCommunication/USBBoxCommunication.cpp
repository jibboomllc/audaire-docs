/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "USBBoxCommunication.hxx"
#include "UsbBox.hxx"
#include "UsbBoxPwm.h"

#define DIMM_SIGNAL_0 0x4
#define DIMM_SIGNAL_1 0x1

	/* the following functions only exist to have the above funtion pointers point to them.
	   Now fn-poiners are old c-style - but as I cannot change the interface (i.e. the fn-pointers)
	   as it is used by several applications directly - I have to keep them. The real
	   redirction is done by the UsbBox::instance() which either uses class UsbBoxUsingCyApi
	   or UsbBoxUsingUsbConDll. */
	UsbError usbDisconnectFn(void);
	UsbError readByteFn(unsigned char registerAddress, unsigned char* registerValue);
	UsbError writeByteFn(unsigned char registerAddress, unsigned char registerValue);
	UsbError setI2CDevAddrFn(unsigned char registerAddress);
	UsbError setPortAsInputFn(int ioPort, unsigned char mask);
	UsbError setPortAsOutputFn(int ioPort, unsigned char mask);
	UsbError writePortFn(unsigned char data, int ioPort, unsigned char mask); 
	UsbError readPortFn(unsigned char *data, int ioPort, unsigned char mask);
	bool     usbIsConnectedFn();
	UsbError readFirmwareIdFn (unsigned char * str);
	UsbError usbDebugMsgFn(unsigned char* str);
	UsbError setInterfaceFn(Interface iface);
	UsbError blkWriteFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes);
	UsbError blkReadFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes);
	UsbError readADCFn(unsigned char channel,unsigned* value);
	UsbError getPortDirectionFn(Port io_port,unsigned char* direction);
	UsbError sendPacketFn(char *in,int in_length, const char *out, int out_length);

USBBoxCommunication::USBBoxCommunication(unsigned char devAddr)
	: itsDimmer( DIMM_SIGNAL_0, DIMM_SIGNAL_1 ) 
{
	connected = false;
	this->devAddr = devAddr;

	/* set up function pointers to point to global functions -> the
	   function body then redirects them to the correct instance */
	setPortAsInput = setPortAsInputFn;
	setPortAsOutput = setPortAsOutputFn;
	writePort = writePortFn;
	readPort = readPortFn;
	readByteFunction = readByteFn;
	writeByteFunction = writeByteFn;
	readFirmwareId = readFirmwareIdFn;
	usbIsConnected = usbIsConnectedFn;
	usbFirmwareID = readFirmwareIdFn;
	usbDebugMsg = usbDebugMsgFn;
	setInterface = setInterfaceFn;
	writeByte = writeByteFn;
	blkWrite = blkWriteFn;
	blkRead = blkReadFn;
	readADC = readADCFn;
	getPortDirection = getPortDirectionFn;
	sendPacketDLL = sendPacketFn;
}

USBBoxCommunication::~USBBoxCommunication()
{
}

void USBBoxCommunication::setDevAddr ( unsigned char devAddr )
{
	this->devAddr = devAddr;
}

AMSCommunication::Error USBBoxCommunication::hwConnect ( )
{
	Error usbError = NoError;

	if ( UsbBox::instance().usbConnectFn( ) != ERR_NO_ERR )
	{
		usbError = ConnectionError;
	}
	else
	{
		connected = true;
	}
	return usbError;
}

void USBBoxCommunication::hwDisconnect ( )
{
	UsbBox::instance().usbDisconnectFn( );
	connected = false;
}

DWORD USBBoxCommunication::firmwareVersionNumber( ) 
{
	unsigned char inPacket[ USB_PACKET_SIZE ];
	UsbError res = readFirmwareId( inPacket );
	DWORD firmwareNumber = 0;
	if ( res == ERR_NO_ERR ) /* could read in firmware number */
	{
		char * str = reinterpret_cast< char *>( inPacket );

		QString v( str );
		if ( v.contains( "Firmware Ver." ) )
		{
			v.remove( "Firmware Ver." );
			int end = v.indexOf( ' ' );
			if ( end > 3 ) 
			{ /* a version number should at least look like: a.b */
				v.truncate( end );
			}
			/* we now should have only the version number as a.b */
			QStringList versionBytes = v.split( '.' );
			if ( versionBytes.size() == 2 )
			{ /* exactly the format that we needed 2 bytes */
				bool result;
				int minor = versionBytes.at( 0 ).toInt( &result, 10 );
				if ( result )
				{
					int releaseMarker = versionBytes.at( 1 ).toInt( &result, 10 );
					if ( result )
					{
						firmwareNumber = 0x01; /* the major number is always 1 for the usb box */
						firmwareNumber = ( firmwareNumber << 8 ) | ( minor & 0xFF );
						firmwareNumber = ( firmwareNumber << 8 ) | (releaseMarker & 0xFF );
					}
				}
			}
		}
	}
	return firmwareNumber;
}

DWORD USBBoxCommunication::frequencyTo8051Ticks ( DWORD frequencyInHertz )
{
	/* the 8051 timer 2 counts up to 0xFFFF */
	DWORD ticks = 0;

	if ( frequencyInHertz > 0 )
	{ /* 1/freq = time per pulse. As 8051 runs with 2MHz timer-ticks
	     we get: ticks = 1/freq * 2 (for ticks per usec) */
		DWORD microsecs = ( 1000000 / frequencyInHertz ); 
		ticks =  microsecs * 2; /* we have 2 ticks per microsecond */
	}
	return ticks;
}

UsbError USBBoxCommunication::sendAS112xPacket( QByteArray * data, bool isOn, bool latchLd )
{
	char outPacket[ USB_PACKET_SIZE ];

	/* build packet */
	USB_TASK(outPacket) = TSK_AS1121_SPI;
	USB_STATE(outPacket) = USB_STATE_NEW;
	USB_AS1121_MODE(outPacket) = data->size(); 
	USB_AS1121_LD(outPacket) = latchLd;

	if ( latchLd )
	{
		USB_AS1121_GCLK(outPacket) = isOn;
	}
	else /* still not all data sent to the as112x, so we do not latch the ld */
	{
		USB_AS1121_GCLK(outPacket) = false;
	}
	/* the gclk clock must not run more than once, as this would reset the oen */
	USB_AS1121_AUTO_RUN(outPacket) = false;
	memcpy( USB_AS1121_DATA_PTR(outPacket), data->constData(), data->size() );

	/* make qt-arrays from the plain data */
	QByteArray tempIn;
	QByteArray tempOut( outPacket, USB_PACKET_SIZE );

	/* send and receive */
	UsbError res = sendReceivePacket( &tempIn, &tempOut );

	return res;
}

UsbError USBBoxCommunication::sendAS1121Packet( bool modeIsDotCorrection, QByteArray * data, bool isOn, bool autoRepeat, bool latchLd )
{
	char outPacket[ USB_PACKET_SIZE ];

	/* build packet */
	USB_TASK(outPacket) = TSK_AS1121_SPI;
	USB_STATE(outPacket) = USB_STATE_NEW;
	USB_AS1121_MODE(outPacket) = modeIsDotCorrection;
	USB_AS1121_LD(outPacket) = latchLd;

	if ( latchLd )
	{
		USB_AS1121_GCLK(outPacket) = isOn;
		USB_AS1121_AUTO_RUN(outPacket) = autoRepeat;
	}
	else /* still not all data sent to the as1121, so we do not start the gclk */
	{
		USB_AS1121_GCLK(outPacket) = false;
		USB_AS1121_AUTO_RUN(outPacket) = false;
	}
	memcpy( USB_AS1121_DATA_PTR(outPacket), data->constData(), data->size() );

	/* make qt-arrays from the plain data */
	QByteArray tempIn;
	QByteArray tempOut( outPacket, USB_PACKET_SIZE );

	/* send and receive */
	UsbError res = sendReceivePacket( &tempIn, &tempOut );

	return res;
}


bool USBBoxCommunication::startDimming( bool isFirstSignal, int startInPercent, int stopInPercent, int timeInMs )
{
	/* whenever we start dimming -> add a dummy packet, that is discarded by the usb-box */
	/* every change in dimming is like starting a new */
	/* the state-machine always starts at bottom */ 
	return itsDimmer.startOrStop( isFirstSignal, startInPercent, stopInPercent, timeInMs );
}

UsbError USBBoxCommunication::sendDimmPacket( bool repeatLastPacket )
{
	if ( ! repeatLastPacket )
	{
 		char * afterHeader = USB_DIMM_DATA(itsPacket);
		/* generate header */
		char * header = itsPacket;
		while ( header < afterHeader )
		{ /* clear header first */
			*header = 0;
			++header;
		}
		USB_TASK(itsPacket) = TSK_DIMM;


		/* fill packet with dimming packet */
		int size = ( afterHeader - itsPacket ) + itsDimmer.generatePacket( afterHeader );
	}

	QByteArray tempIn;
	QByteArray tempOut( itsPacket, USB_PACKET_SIZE );

	/* send and receive */
	UsbError res = sendReceivePacket( &tempIn, &tempOut );

	return res;

}

void USBBoxCommunication::setPwmSignal ( char * outPacket, int index, DWORD ticks, int signal )
{
	if ( ticks > PWM_SWITCH_OVERHEAD_8051 )
	{
		ticks -= PWM_SWITCH_OVERHEAD_8051;
	}
	/* the 8051 timer 2 counts up to 0xFFFF */
	ticks = 0xFFFF - ticks;

	unsigned char ticksHigh = static_cast< unsigned char >( ticks >> 8 ); 
	unsigned char ticksLow  = static_cast< unsigned char >( ticks & 0xFF ); 
	unsigned char sig       = static_cast< unsigned char >( signal );
	switch ( index )
	{
	case 0:
		USB_PWM_RELOAD_HIGH_0( outPacket )   = ticksHigh; 
		USB_PWM_RELOAD_LOW_0( outPacket )    = ticksLow;
		USB_PWM_RELOAD_SIGNAL_0( outPacket ) = sig;
		break;
	case 1:
		USB_PWM_RELOAD_HIGH_1( outPacket )   = ticksHigh; 
		USB_PWM_RELOAD_LOW_1( outPacket )    = ticksLow;
		USB_PWM_RELOAD_SIGNAL_1( outPacket ) = sig;
		break;
	case 2:
		USB_PWM_RELOAD_HIGH_2( outPacket )   = ticksHigh; 
		USB_PWM_RELOAD_LOW_2( outPacket )    = ticksLow;
		USB_PWM_RELOAD_SIGNAL_2( outPacket ) = sig;
		break;
	case 3:
		USB_PWM_RELOAD_HIGH_3( outPacket )   = ticksHigh; 
		USB_PWM_RELOAD_LOW_3( outPacket )    = ticksLow;
		USB_PWM_RELOAD_SIGNAL_3( outPacket ) = sig;
		break;
	}
}

void USBBoxCommunication::setMultiLevelI2cProperites ( unsigned char halfSpikeSupTime, unsigned char halfEnTime )
{
	UsbBox::instance().multiLevelI2cPropertiesFn( halfSpikeSupTime, halfEnTime );
}


UsbError USBBoxCommunication::setPwmSignals ( DWORD frequencyInHertz, unsigned char dutyCycleInPercent, unsigned char secondDutyCycleInPercent , unsigned char delayInPercent, unsigned char pwmSignal )
{
	char outPacket[ USB_PACKET_SIZE ];
	UsbError usbError;
	memset( outPacket, 0, USB_PACKET_SIZE );
	USB_TASK( outPacket ) = TSK_PWM;
	USB_STATE( outPacket ) = 0;

	if ( delayInPercent >= 100 )
	{
		delayInPercent = 0;
	}
	if ( dutyCycleInPercent > 100 )
	{
		dutyCycleInPercent = 100;
	}
	if ( secondDutyCycleInPercent >= 100 )
	{
		secondDutyCycleInPercent = 100;
		delayInPercent = 0;
	}
	DWORD ticks = frequencyTo8051Ticks( frequencyInHertz );

	if ( frequencyInHertz == 0 || dutyCycleInPercent == 0 )
	{ /* switch off */
		USB_PWM_NUM_RELOAD_VALUES( outPacket ) = 0;
	}
	else
	{
		if ( secondDutyCycleInPercent == 0 )
		{ /* only PWM signal */
			if ( dutyCycleInPercent == 100 ) /* constant high */
			{
				USB_PWM_NUM_RELOAD_VALUES( outPacket ) = 1;
				setPwmSignal( outPacket, 0, ticks, pwmSignal );
			}
			else
			{
				DWORD highTicks = ( ticks * dutyCycleInPercent ) / 100 ;
				DWORD lowTicks = ( ticks - highTicks ); 
				USB_PWM_NUM_RELOAD_VALUES( outPacket ) = 2;
				setPwmSignal( outPacket, 0, highTicks, pwmSignal );
				setPwmSignal( outPacket, 1, lowTicks, 0 );
			}
		}
		else
		{ /* both 3D_PWM* signals are used */

			DWORD signal3D_PWM12IsLowAt  = ( ticks * dutyCycleInPercent ) / 100;
			DWORD signal3D_PWM34IsHighAt = ( ticks * delayInPercent ) / 100;
			DWORD signal3D_PWM34IsLowAt  = ( ticks * ( delayInPercent + secondDutyCycleInPercent ) ) / 100;
			DWORD percentSum = delayInPercent + secondDutyCycleInPercent;
			if ( percentSum == 100 )
			{ /* make sure there are no rounding errors */
				signal3D_PWM34IsLowAt = ticks;
			}
			if ( dutyCycleInPercent == 100 )
			{
				signal3D_PWM12IsLowAt = ticks; /* make sure there are no rounding errors */
			}

			UsbBoxPwm sorter( ticks );
			if ( percentSum > 100 ) 
			{	/* signal starts with high since delay + duty cycle > cycle */
				signal3D_PWM34IsLowAt -= ticks;
				sorter.addPoint( 0, USB_PWM_SIGNAL_3D_PWM34, true );
			}

			/* signal 3D_PWM12 starts at 0 */
			sorter.addPoint( 0, USB_PWM_SIGNAL_3D_PWM12, true );
			sorter.addPoint( signal3D_PWM12IsLowAt,  USB_PWM_SIGNAL_3D_PWM12, false );
			sorter.addPoint( signal3D_PWM34IsHighAt, USB_PWM_SIGNAL_3D_PWM34, true );
			sorter.addPoint( signal3D_PWM34IsLowAt,  USB_PWM_SIGNAL_3D_PWM34, false );
			
			USB_PWM_NUM_RELOAD_VALUES( outPacket ) = sorter.getNumberOfPoints( );
			for ( unsigned int i = 0; i < sorter.getNumberOfPoints( ); ++i )
			{
				DWORD tic;
				unsigned char sig;
				if ( sorter.getDuration( i, tic, sig ) )
				{
					setPwmSignal( outPacket, i, tic, sig );
				}
			}
		}
	}

	QByteArray tempIn;
	QByteArray tempOut( outPacket, USB_PACKET_SIZE );

	/* send and receive */
	usbError = sendReceivePacket( &tempIn, &tempOut );

	return usbError;
}

UsbError USBBoxCommunication::setVsyncFrequency ( DWORD frequencyInHertz )
{
	UsbError usbError = ERR_NO_ERR;
	char outPacket[ USB_PACKET_SIZE ]; 
	DWORD ticks = frequencyTo8051Ticks( frequencyInHertz ) + 2 * 10; /* pulse is 10 microseconds wide -> 2 ticks per microsecond */
	/* the 8051 timer 2 counts up to 0xFFFF */
	ticks = 0xFFFF - ticks;

	/* build a new usb packet */
	USB_TASK( outPacket ) = TSK_VSYNC;
	USB_STATE( outPacket ) = 0;
	USB_VSYNC_RELOAD_HIGH( outPacket ) = static_cast<BYTE>( ticks >> 8 );
	USB_VSYNC_RELOAD_LOW( outPacket ) = static_cast<BYTE>( ticks & 0xFF );
	USB_VSYNC_REPEAT_RELOAD( outPacket ) = ( frequencyInHertz ? 1 : 0 );
	
	QByteArray tempIn;
	QByteArray tempOut( outPacket, USB_PACKET_SIZE );


	/* send and receive */
	usbError = sendReceivePacket( &tempIn, &tempOut );

	return usbError;
}

UsbError USBBoxCommunication::sendReceiveI2CPacket (char startRegister, QByteArray * in, const QByteArray * out )
{
	UsbError usbError = ERR_NO_ERR;
	/* as the size of the data must be stored in a byte, the outpacket can have
	   a maximum of 256 data bytes*/
	char outPacket[ 256 ]; 
	char * outData = outPacket;
	int outSize = out->size();
	int inSize = in->size();

	if ( inSize > UsbBox::instance().maxSize() || outSize > UsbBox::instance().maxSize() )
	{
		return ERR_ILLEGAL_ARGUMENTS;
	}


	/* clear in array -> to be filled later */
	in->clear();

	/* setup chip specific data (like i2c slave address and register address */
	outData[ 0 ] = devAddr << 1; /* first is always a write */
	outData[ 1 ] = startRegister; /* second is always the register we want to access */
	if ( inSize > 0 ) /* we have a write-read combination */
	{	
		outData[ 2 ] = ( devAddr << 1 ) | 1; /* indicate an i2c-read */
		outData += 3;
		outSize += 3;/* +1 for slave address, +1 for register address, +1 for repeated slave address */
	}
	else /* we have a write only */
	{
		outData += 2;
		outSize += 2; /* +1 for slave address, +1 for register address */
	}

	memcpy( outData, out->data(), out->size() );/* now the complete data is in "packet" */
	outData = outPacket; /* reset out pointer, to use in loop */
	do 
	{
		const int tempOutSize = MIN( USB_PLAIN_I2C_MAX_DATA, outSize );
		char header[ USB_PLAIN_I2C_HEADER_SIZE ];
		QByteArray tempIn;

		/* build a new usb packet */
		USB_TASK( header ) = TSK_PLAIN_I2C;
		USB_STATE( header ) = 0;
		USB_PLAIN_I2C_MOSI_NBYTES( header ) = outSize; /* is only evaluated in first packet of a series */
		USB_PLAIN_I2C_MISO_NBYTES( header ) = UsbBox::instance().getInSize( inSize ); /* is only evaluated in first packet of a series */
		QByteArray tempOut( header, USB_PLAIN_I2C_HEADER_SIZE );
		tempOut.append( outData, tempOutSize );

		/* send and receive */
		usbError = sendReceivePacket( &tempIn, &tempOut );

		outSize -= tempOutSize;  /* correct size and pointer , since we already */
		outData += tempOutSize;     /* sent+received some bytes */
		if ( inSize > 0 )
		{ /* we are waiting for input -> concatenate it */
            if ( USB_STATE( tempIn ) )
            { /* i2c failed at communication between usb box and ic */
                usbError = ERR_CMD_FAILED;
            }
			tempIn.remove( 0, USB_PLAIN_I2C_HEADER_SIZE );
			in->append( tempIn );
			inSize -= USB_PLAIN_I2C_MAX_DATA; /* we always receive a full usb-packet */
		}
	} while ( ( outSize > 0 || inSize > 0 ) && usbError == ERR_NO_ERR );
	return usbError;
}

UsbError USBBoxCommunication::sendReceiveSPIPacket ( QByteArray * in, const QByteArray * out )
{
	UsbError usbError = ERR_NO_ERR;
	/* as the size of the data must be stored in a byte, the outpacket can have
	   a maximum of 256 data bytes*/
	char outPacket[ 256 ]; 
	int outSize = out->size();
	int inSize = in->size();

	if ( inSize > USB_PLAIN_SPI_MAX_DATA || outSize > USB_PLAIN_SPI_MAX_DATA )
	{
		return ERR_ILLEGAL_ARGUMENTS;
	}

	/* build a new usb packet */
	USB_TASK( outPacket ) = TSK_PLAIN_SPI;
	USB_STATE( outPacket ) = 0;
	USB_PLAIN_SPI_MOSI_NBYTES( outPacket ) = outSize;
	USB_PLAIN_SPI_MISO_NBYTES( outPacket ) = inSize;
	char * outData = USB_PLAIN_SPI_DATA_PTR( outPacket );

	memcpy( outData, out->data(), out->size() );/* now the complete data is in "packet" */
	QByteArray tempOut( outPacket, outSize + USB_PLAIN_SPI_HEADER_SIZE );
	in->resize( inSize + USB_PLAIN_SPI_HEADER_SIZE );
	/* send and receive */
	usbError = sendReceivePacket( in, &tempOut );
	if ( usbError == ERR_NO_ERR )
	{
		in->remove( 0, USB_PLAIN_SPI_HEADER_SIZE );
	}
	return usbError;
}

UsbError USBBoxCommunication::sendTrimmerPacket ( TrimmerProtocol protocol, unsigned char highTimeInMicroSec, unsigned char lowTimeInMicroSec, const QByteArray * out )
{
	UsbError usbError = ERR_NO_ERR;
	/* as the size of the data must be stored in a byte, the outpacket can have
	   a maximum of 256 data bytes*/
	char outPacket[ 256 ]; 
	int outSize = out->size();

	if ( outSize > USB_TRIMMER_MAX_DATA )
	{
		return ERR_ILLEGAL_ARGUMENTS;
	}
	if ( highTimeInMicroSec > TRIMMER_MINIMUM_HIGH_TIME_8051 )
	{
		highTimeInMicroSec -= TRIMMER_MINIMUM_HIGH_TIME_8051;
	}
	else
	{
		highTimeInMicroSec = 0;
	}
	if ( lowTimeInMicroSec > TRIMMER_MINIMUM_LOW_TIME_8051 )
	{
		lowTimeInMicroSec -= TRIMMER_MINIMUM_LOW_TIME_8051;
	}
	else
	{
		lowTimeInMicroSec = 0;
	}

	/* build a new usb packet */
	USB_TASK( outPacket ) = TSK_TRIMMER;
	USB_STATE( outPacket ) = 0;
	USB_TRIMMER_NBYTES( outPacket ) = outSize;
	USB_TRIMMER_GET_PROTOCOL( outPacket ) = protocol;
	USB_TRIMMER_HIGH_TIME( outPacket ) = highTimeInMicroSec;
	USB_TRIMMER_LOW_TIME( outPacket ) = lowTimeInMicroSec;
	char * outData = USB_TRIMMER_DATA_PTR( outPacket );

	memcpy( outData, out->data(), out->size() );/* now the complete data is in "packet" */
	QByteArray tempOut( outPacket, outSize + USB_TRIMMER_HEADER_SIZE );
	QByteArray tempIn;
	/* send and receive */
	usbError = sendReceivePacket( &tempIn, &tempOut );

	return usbError;
}

UsbError USBBoxCommunication::sendReceivePacket (QByteArray *in, const QByteArray *out)
{
	UsbError usberror = ERR_NO_ERR;

	in->resize(64); // make space for complete USB packet
	
	usberror = UsbBox::instance().sendPacketFn(in->data(),in->size(),out->constData(),out->size());

	return usberror;
}

bool USBBoxCommunication::isConnected()
{
	unsigned char	fwIdBuf[20] = {0};
	UsbError		usberror;
	Error			comError = NoError;

	if ( !connected )
	{
		comError = hwConnect();
	}
	
	if ( comError == NoError )
	{
		connected = false;
		usberror = UsbBox::instance().readFirmwareIdFn(&fwIdBuf[0]);
		if (usberror == ERR_NO_ERR)
		{
			if (strncmp((const char *)&fwIdBuf[0], "Firmware", 8) == 0)
			{
				connected = true;
			}
		}
	}
	return connected;
}

AMSCommunication::Error USBBoxCommunication::hwReadRegister(unsigned char registerAddress, unsigned char* registerValue)
{
	if ( UsbBox::instance().canUseBlkCommands( ) )
	{
		UsbError usbError = ERR_NO_ERR;
		AMSCommunication::Error err = NoError;

		if (connected)
		{
			usbError = UsbBox::instance().setI2CDevAddrFn(devAddr);
			if ( !usbError )
			{
				usbError = UsbBox::instance().readByteFn(registerAddress, registerValue);
			}
		}
		else
		{
			usbError = ERR_NOT_CONNECTED;
		}

		if (usbError  == ERR_NOT_CONNECTED)
			err = ConnectionError;
		else if (usbError == ERR_NO_ERR)
			err = NoError;
		else
			err = ReadError;
		return err;
	}
	else
	{
		QByteArray tempIn;
		QByteArray tempOut;
		tempIn.resize( 1 );
		UsbError usbError = sendReceiveI2CPacket( registerAddress, &tempIn, &tempOut );
		if ( usbError == ERR_NO_ERR )
		{
			* registerValue = tempIn.at( 0 );
			return NoError;
		}
		return ReadError;
	}
}

  /**
   * This function sends one Byte to the specified address
   * In case of success ERR_NO_ERR is returned
   */
AMSCommunication::Error USBBoxCommunication::hwWriteRegister(unsigned char registerAddress, unsigned char registerValue)
{
	if ( UsbBox::instance().canUseBlkCommands( ) )
	{
		UsbError usbError = ERR_NO_ERR;
		AMSCommunication::Error err = NoError;

		if (connected)
		{
			usbError = UsbBox::instance().setI2CDevAddrFn(devAddr);
			if (!usbError)
			{
				usbError = UsbBox::instance().writeByteFn(registerAddress, registerValue);
			}
		}
		else
		{
			usbError = ERR_NOT_CONNECTED;
		}
		if (usbError  == ERR_NOT_CONNECTED)
			err = ConnectionError;
		else if (usbError == ERR_NO_ERR)
			err = NoError;
		else
			err = WriteError;
		return err;
	}
	else
	{
		QByteArray tempIn;
		QByteArray tempOut;
		tempOut.append( registerValue );

		UsbError usbError = sendReceiveI2CPacket( registerAddress, &tempIn, &tempOut );
		if ( usbError == ERR_NO_ERR )
		{
			return NoError;
		}
		return WriteError;
	}
}

  /**
   * This function can be used to send special commands to the device
   */
AMSCommunication::Error USBBoxCommunication::hwSendCommand(QString command, QString * answer)
{
	unsigned char buffer[256];
	int openIdx, closeIdx, i, nrOfBlockWrites;
	unsigned char start_reg_address;
	bool ok;

	UsbError usbError = ERR_NO_ERR;
	AMSCommunication::Error err = NoError;

	if(command.contains("Bulk_Write"))
	{
		openIdx = command.indexOf('(');
		closeIdx = command.indexOf(')', openIdx);
		start_reg_address = command.mid(openIdx+1, 2).toUInt(&ok, 16);
		command = command.mid(openIdx + 3, closeIdx - openIdx - 1);
		i = 0;
		while((i*2 < command.size()) && (i < 256))
		{
			buffer[i++] = command.mid(i*2, 2).toInt(&ok, 16);
		}

		usbError = UsbBox::instance().setI2CDevAddrFn(devAddr);
		if (!usbError)
		{
			nrOfBlockWrites = ((command.size()-1)/2) / BLOCK_WRITE_MAX_BYTES;

			for (i=0; i<nrOfBlockWrites; i++)
			{
				usbError = UsbBox::instance().blkWriteFn((BYTE)start_reg_address, (BYTE*)(buffer + i*BLOCK_WRITE_MAX_BYTES), BLOCK_WRITE_MAX_BYTES );
				start_reg_address += BLOCK_WRITE_MAX_BYTES;
			}

			usbError = UsbBox::instance().blkWriteFn((BYTE)start_reg_address, (BYTE*)(buffer + nrOfBlockWrites*BLOCK_WRITE_MAX_BYTES), (((command.size()-1)/2) % BLOCK_WRITE_MAX_BYTES) );

		}
	}

	if (usbError  == ERR_NOT_CONNECTED)
		err = ConnectionError;
	else if (usbError == ERR_NO_ERR)
		err = NoError;
	else
		err = WriteError;
	return err;
}

UsbError USBBoxCommunication::setI2CDeviceAddress(unsigned char registerAddress)
{
	UsbError usbError = ERR_NO_ERR;

	if (connected)
	{
		usbError = UsbBox::instance().setI2CDevAddrFn(registerAddress);
	}
	else
	{
		usbError = ERR_NOT_CONNECTED;
	}
	return usbError;
}

void USBBoxCommunication::setConnectionProperties(void *in)
{
	if ( in )
	{
		devAddr = reinterpret_cast<usbBoxConfigStructure*>(in)->deviceI2CAddress;
		UsbBox::instance().setI2CDevAddrFn( devAddr );
	}
}

void USBBoxCommunication::getConnectionProperties(void *out)
{
	if ( out )
	{
		reinterpret_cast<usbBoxConfigStructure*>(out)->deviceI2CAddress = devAddr;
	}
}

void USBBoxCommunication::setSingleWireProperties ( unsigned char delay, unsigned char pin )
{
	UsbBox::instance().singleWirePropertiesFn( delay, pin );
}


UsbError readByteFn(unsigned char registerAddress, unsigned char* registerValue)
{
	return UsbBox::instance().readByteFn( registerAddress, registerValue );
}

UsbError writeByteFn(unsigned char registerAddress, unsigned char registerValue)
{
	return UsbBox::instance().writeByteFn( registerAddress, registerValue );
}

UsbError setI2CDevAddrFn(unsigned char registerAddress)
{
	return UsbBox::instance().setI2CDevAddrFn( registerAddress );
}

UsbError setPortAsInputFn(int ioPort, unsigned char mask)
{
	return UsbBox::instance().setPortAsInputFn( ioPort, mask );
}

UsbError setPortAsOutputFn(int ioPort, unsigned char mask)
{
	return UsbBox::instance().setPortAsOutputFn( ioPort, mask );
}

UsbError writePortFn(unsigned char data, int ioPort, unsigned char mask)
{
	return UsbBox::instance().writePortFn( data, ioPort, mask );
}

UsbError readPortFn(unsigned char *data, int ioPort, unsigned char mask)
{
	return UsbBox::instance().readPortFn( data, ioPort, mask );
}

bool usbIsConnectedFn()
{
	return UsbBox::instance().usbIsConnectedFn( );
}

UsbError readFirmwareIdFn (unsigned char * str)
{
	return UsbBox::instance().readFirmwareIdFn( str );
}

UsbError usbDebugMsgFn(unsigned char* str)
{
	return UsbBox::instance().usbDebugMsgFn( str );
}

UsbError setInterfaceFn(Interface iface)
{
	UsbBox::instance().switchInterface( iface );
	return UsbBox::instance().setInterfaceFn( iface );
}

UsbError blkWriteFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes)
{
	return UsbBox::instance().blkWriteFn( start_addr, values, num_bytes );
}

UsbError blkReadFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes)
{
	return UsbBox::instance().blkReadFn( start_addr, values, num_bytes );
}

UsbError readADCFn(unsigned char channel,unsigned* value)
{
	return UsbBox::instance().readADCFn( channel, value );
}

UsbError getPortDirectionFn(Port io_port,unsigned char* direction)
{
	return UsbBox::instance().getPortDirectionFn( io_port, direction );
}

UsbError sendPacketFn(char *in,int in_length, const char *out, int out_length)
{
	return UsbBox::instance().sendPacketFn( in, in_length, out, out_length );
}

