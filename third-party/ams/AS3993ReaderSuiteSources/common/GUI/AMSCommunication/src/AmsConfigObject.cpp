/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams Stream Communication
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsConfigObject.cpp
 *
 *  \author M. Arpa
 *
 *  \brief Implementation of the config object and the pic parser.
 */

#include "AmsConfigObject.h"
#include "ams_stream.h"
#include <QTextStream>

AmsRegisterParser::AmsRegisterParser( const QString & registerDefinitionFileName ) 
    : itsFile( registerDefinitionFileName ) 
{ 
    itsIsOpen = itsFile.open( QIODevice::ReadOnly | QIODevice::Text );  
}

AmsPicRegisterParser::AmsPicRegisterParser( const QString & registerDefinitionFileName ) 
    : AmsRegisterParser( registerDefinitionFileName ) 
{
    bool found = false;
    if ( itsIsOpen ) /* generate dictonary */
    {
        QTextStream in( &itsFile );
        while ( ! in.atEnd() )
        {
            QString line = in.readLine();
            if ( !found && line.contains( "WREG" ) )
            {
                /* read until we find "WREG" - first register in p24fj64gb004.gld */
                found = true; /* found our first register definition */
            }
            if ( found )
            {
                if ( line.contains( "= 0x" ) )
                { /* add to dictonary */
                    QStringList list = line.split( "=", QString::SkipEmptyParts );
                    bool success = true;
                    if ( list.size() == 2 )
                    {
                        QString name = list.at(0);
                        QString addr = list.at(1);
                        addr.remove( ';' );
                        addr.remove( ' ' );
                        name.remove( ' ' );
                        unsigned int address = addr.toUInt( &success, 0 );
                        if ( success )
                        {
                            itsRegisterMap[ name ] = address;
                            qDebug() << name << address << "\n";
                        }

                    } /* otherwise the list is malformed */
                }
            }
        }
        itsFile.close(); /* no longer needed -> we have our dictionary */
    }
}


bool AmsPicRegisterParser::registerAddress ( const QString & registerName, unsigned int & registerAddress )
{
    if ( itsRegisterMap.contains( registerName ) )
    {
        registerAddress = itsRegisterMap.value( registerName );
        return true;
    }
    return false; /* could not open file - so we could not search for the name */
}


AmsConfigObject::AmsConfigObject 
    ( AmsRegisterParser & parser
    , unsigned int tracePattern
    )
    : AmsComObject( AMS_COM_CONFIG, AMS_CONFIG_WRITE_REQUEST_LENGTH(AMS_CONFIG_PIC_WORD_SIZE), AMS_CONFIG_READ_REQUEST_LENGTH(AMS_CONFIG_PIC_WORD_SIZE), AMS_CONFIG_READ_RESPONSE_LENGTH(AMS_CONFIG_PIC_WORD_SIZE), tracePattern )
    , itsParser( parser )
    , itsRegisterNameValid( false )
    , itsRegisterName( )
    , itsRegisterAddress( 0xFFFFFFFF )
    , itsRegisterValue( 0 )
    , itsValueMask( 0xFFFFFFFF )
{ 
}


AmsConfigObject::AmsConfigObject 
    ( AmsRegisterParser & parser
    , const QString & registerName
    , unsigned int tracePattern
    )
    : AmsComObject( AMS_COM_CONFIG, AMS_CONFIG_WRITE_REQUEST_LENGTH(AMS_CONFIG_PIC_WORD_SIZE), AMS_CONFIG_READ_REQUEST_LENGTH(AMS_CONFIG_PIC_WORD_SIZE), AMS_CONFIG_READ_RESPONSE_LENGTH(AMS_CONFIG_PIC_WORD_SIZE), tracePattern )
    , itsParser( parser )
    , itsRegisterNameValid( false )
    , itsRegisterName( registerName )
    , itsRegisterAddress( 0xFFFFFFFF )
    , itsRegisterValue( 0 )
    , itsValueMask( 0xFFFFFFFF )
{
    if ( itsParser.registerAddress( itsRegisterName, itsRegisterAddress ) )
    {
        itsRegisterNameValid = true;
    }
}


