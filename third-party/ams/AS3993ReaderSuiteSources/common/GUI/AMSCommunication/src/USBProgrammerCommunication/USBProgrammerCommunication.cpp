/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#if QT_VERSION < 0x050000
#else
#include <QWidget>
#endif

#include "USBProgrammerCommunication.hxx"
#include "HIDDevice.h"


AMSCommunication::Error USBProgrammerCommunication::inReport(unsigned char buffer[64])
{
	BYTE result;

	UsbHidError usbHidError = ERR_USBHID_NO_ERR;
	AMSCommunication::Error err = NoError;

	if (!connected )
		return ConnectionError;

	result = this->hidDevice->GetReport_Control((BYTE*)buffer, 64);
	if(result != HID_DEVICE_SUCCESS)
	{
		usbHidError = ERR_USBHID_GENERIC;
	}
	if (usbHidError  == ERR_USBHID_NOT_CONNECTED)
		err = ConnectionError;
	else if (usbHidError == ERR_USBHID_NO_ERR)
		err = NoError;
	else
		err = ReadError;
	return err;
}

AMSCommunication::Error USBProgrammerCommunication::outReport(const unsigned char buf[64])
{
	BYTE result;

	UsbHidError usbHidError = ERR_USBHID_NO_ERR;
	AMSCommunication::Error err = NoError;

	if (!connected )
		return ConnectionError;

	result = this->hidDevice->SetReport_Control((BYTE*)buf, 64);
	if(result != HID_DEVICE_SUCCESS)
	{
		usbHidError = ERR_USBHID_GENERIC;
	}
	if (usbHidError  == ERR_USBHID_NOT_CONNECTED)
		err = ConnectionError;
	else if (usbHidError == ERR_USBHID_NO_ERR)
		err = NoError;
	else
		err = ReadError;
	return err;
}

AMSCommunication::Error USBProgrammerCommunication::getFirmwareInfo(unsigned char buffer[64])
{
	AMSCommunication::Error err;

	buffer[0] = 1;  // firmware info
	err = inReport(buffer);

	return err;
}
AMSCommunication::Error USBProgrammerCommunication::getReadMem(unsigned char mem[16])
{
	AMSCommunication::Error err;
	unsigned char buffer[64] = {0};

	buffer[0] = 2;  // readmem
	err = inReport(buffer);
	memcpy(mem,buffer+1,16);

	return err;
}
AMSCommunication::Error USBProgrammerCommunication::setReadMemAddress(unsigned short address)
{
	unsigned char buffer[64] = {0};

	buffer[0] = 3;  // setreadmem
	buffer[1] = address;
	buffer[2] = address>>8;
	return outReport(buffer);
}
AMSCommunication::Error USBProgrammerCommunication::setWriteMem(unsigned short address,const unsigned char mem[16])
{
	unsigned char buffer[64] = {0};

	buffer[0] = 4;  // setreadmem
	buffer[1] = address;
	buffer[2] = address>>8;
	memcpy(buffer+3,mem,16);
	return outReport(buffer);
}

AMSCommunication::Error USBProgrammerCommunication::setRemoteMode(int on)
{
	unsigned char buffer[64] = {0};

	buffer[0] = 6;  // remotemode
	buffer[1] = on; 
	return outReport(buffer);
}
AMSCommunication::Error USBProgrammerCommunication::setExecuteCommand(unsigned short command)
{
	AMSCommunication::Error err = NoError;

	unsigned char buffer[64] = {0};

	buffer[0] = 5;  // Execute command
	buffer[1] = command; 
	buffer[2] = command>>8;
	err = outReport(buffer);

	if(!waitForCommandFinished())
		err = ReadError;

	return err;
}


AMSCommunication::Error USBProgrammerCommunication::hwReadRegister(unsigned char registerAddress, unsigned char* registerValue)
{
	unsigned char buffer[64];
	AMSCommunication::Error err = NoError;

	switch(this->activeType)
	{
	case SPI:
		if (!connected)
			return ConnectionError;

		buffer[0] = 2; // number of bytes for spi command
		buffer[1] = (registerAddress<<1)|1; // values
		buffer[2] = 0xaa; // dummy value
		if ( (err = setWriteMem(0,buffer)) != NoError) return err; 

		if ( (err = setExecuteCommand(73)) != NoError) return err; 

		//{
		//	unsigned char in_progress = 1;
		//	if ( (err = setReadMemAddress(15)) != NoError) return err; 
		//	while ( in_progress)
		//	{
		//		Sleep(1);
		//		if ( (err = getReadMem(buffer)) != NoError) return err; 
		//		in_progress = buffer[0];
		//	}
		//}

		if ( (err = setReadMemAddress(0)) != NoError) return err; 

		if ( (err = getReadMem(buffer)) != NoError) return err; 
		*registerValue = buffer[2];
		break;

        case UART:
		    
            buffer[0] = 2; // number of bytes
		    buffer[1] = registerAddress;

            //if ( (err = setWriteMem(0,buffer) ) != NoError) 
            //    return err; 

		    if ( (err = setExecuteCommand(84)) != NoError) 
                return err; 

            // warning: there is no synchronization here

		    if ( (err = setReadMemAddress(0)) != NoError) 
                return err; 

		    if ( (err = getReadMem(buffer)) != NoError) 
                return err; 
		    
            *registerValue = buffer[2];
		    
            break;
    
	default:
		// if invalid communication method is tried
		err = ReadError;
		break;
	}
	return err;
}


  /**
   * This function sends one Byte to the specified address
   * In case of success ERR_NO_ERR is returned
   */
AMSCommunication::Error USBProgrammerCommunication::hwWriteRegister(unsigned char registerAddress, unsigned char registerValue)
{
	unsigned char buffer[64];
	AMSCommunication::Error err = NoError;
	switch(this->activeType)
	{
	case SPI:
		if (!connected)
			return ConnectionError;

		buffer[0] = 2; // number of bytes for spi command
		buffer[1] = (registerAddress<<1)|0; // values
		buffer[2] = registerValue;

		if ( (err = setWriteMem(0,buffer)) != NoError) return err; 

		if ( (err = setExecuteCommand(73)) != NoError) return err; 

		//{
		//	unsigned char in_progress = 1;
		//	if ( (err = setreadmemaddress(15)) != noerror) return err; 
		//	while ( in_progress)
		//	{
		//		if ( (err = getreadmem(buffer)) != noerror) return err; 
		//		in_progress = buffer[0];
		//	}
		//}

		if ( (err = setReadMemAddress(0)) != NoError) return err; 
		break;
	default:
		// if invalid communication method is tried
		err = WriteError;
		break;
	}
	return err;
}
AMSCommunication::Error USBProgrammerCommunication::connect()
{
	AMSCommunication::Error err;
	err = USBHIDCommunication::connect();
	if (!err) 
	{
		setRemoteMode(1);
	}
    return err;
}

void USBProgrammerCommunication::disconnect()
{
	if (connected) setRemoteMode(0);
	
	if(typeInitialized[PDB])
	{
		pdbDeinitialize();
		typeInitialized[PDB] = false;
	}

	for(int mapIter = 0; mapIter < 4; mapIter ++)
	{
		typeInitialized[(CommunicationType)mapIter] = false;
	}

	activeType = NoType;
	
	USBHIDCommunication::disconnect();
}

USBProgrammerCommunication::USBProgrammerCommunication(int pid) : USBHIDCommunication( 0 )
{
    hidconfig.pid = pid;
    hidconfig.vid = 0x1325;
    hidconfig.inReportID = 2;
    hidconfig.outReportID = 5;
    hidconfig.readRegisterCommand = 73;
    hidconfig.writeRegisterCommand = 73;

    DAC_VDD_GAIN = DEFAULT_DAC_VDD_GAIN;
    DAC_VZAP_GAIN = DEFAULT_DAC_VZAP_GAIN;
    DAC_VREF = DEFAULT_DAC_VREF;

	setConnectionProperties(&hidconfig);

	for(int mapIter = 0; mapIter < 5; mapIter ++)
	{
		typeInitialized[(CommunicationType)mapIter] = false;
	}

	activeType = NoType;
}

AMSCommunication::Error USBProgrammerCommunication::ssiInitialize()
{
	if (!connected)
		return ConnectionError;

	return setExecuteCommand(0x2b);
}

AMSCommunication::Error USBProgrammerCommunication::ssiRead(unsigned char length, unsigned char valueBuffer[16])
{
	unsigned char memBuffer[16] = {0};
	AMSCommunication::Error err = NoError;

	QMutex mutex;
	QMutexLocker mutexLocker(&mutex);

	if(activeType != SSI)
		setCommunicationType(SSI);

	if(!typeInitialized[SSI])
	{
		err = ssiInitialize();
		if(err != AMSCommunication::NoError)
			return err;
		typeInitialized[SSI] = true;
	}

	memBuffer[0] = length;
	err = setWriteMem(0,memBuffer);
	if(err != AMSCommunication::NoError)
			return err;

	err = setExecuteCommand(0x2c);
	if(err != AMSCommunication::NoError)
			return err;

	// check this again
	err = this->setReadMemAddress(0x1);
	if(err != AMSCommunication::NoError)
			return err;

	err = this->getReadMem(valueBuffer);

	return err;
}

AMSCommunication::Error USBProgrammerCommunication::easy2ZappInitialize()
{
	if (!connected)
		return ConnectionError;

	if(activeType != easy2Zapp)
		setCommunicationType(easy2Zapp);

	return setExecuteCommand(0x16);
}

AMSCommunication::Error USBProgrammerCommunication::easy2ZappRead(unsigned char numbits, unsigned char valueBuffer[])
{
	unsigned char memBuffer[16] = {0};
	AMSCommunication::Error err = NoError;

	QMutex mutex;
	QMutexLocker mutexLocker(&mutex);
	
	if(activeType != easy2Zapp)
		setCommunicationType(easy2Zapp);

	if(!typeInitialized[easy2Zapp])
	{
		err = easy2ZappInitialize();
		if(err != AMSCommunication::NoError)
			return err;
		typeInitialized[easy2Zapp] = true;
	}

	memBuffer[0] = numbits;
	err = setWriteMem(0,memBuffer);
	if(err != AMSCommunication::NoError)
			return err;

	err = setExecuteCommand(0x18);
	if(err != AMSCommunication::NoError)
			return err;

	err = this->setReadMemAddress(0x1);
	if(err != AMSCommunication::NoError)
			return err;

	err = this->getReadMem(valueBuffer);
	
	return err;
}

bool USBProgrammerCommunication::waitForCommandFinished()
{
	unsigned char memoryStatus[16] = {0};

	AMSCommunication::Error err = this->setReadMemAddress(511);
	if(err != AMSCommunication::NoError)
			return false;

    do
    {
        // TODO: think about the memory position of the flag.
        // If the Ubox Firmware defines APPLICATION_MEMORY smaller than 512 Bytes,
        // we have a problem. By the way: the flag is 26 if it is not zero.
		err = this->getReadMem(memoryStatus);
		if(err != AMSCommunication::NoError)
			return false;
        // The amount of milliseconds, we are waiting, is essential
        // for the USB Communication speed. Too many checks per time interval (small sleep time) result in
        // large communication overhead. To large time here may wait unnecessarily.
		Sleep(5);
    }
    while (memoryStatus[0] != 0);
    
	return true;
}

AMSCommunication::Error USBProgrammerCommunication::easy2ZappWrite(unsigned char numbits, unsigned char valueBuffer[])
{
	unsigned char memBuffer[16] = {0};
	AMSCommunication::Error err = NoError;
	
	QMutex mutex;
	QMutexLocker mutexLocker(&mutex);

	if(activeType != easy2Zapp)
		setCommunicationType(easy2Zapp);

	if(!typeInitialized[easy2Zapp])
	{
		err = easy2ZappInitialize();
		if(err != AMSCommunication::NoError)
			return err;
		typeInitialized[easy2Zapp] = true;
	}

	memBuffer[0] = numbits;
	memcpy(memBuffer + 1, valueBuffer, ceil(numbits/8.0) > 15 ? 15 : ceil(numbits/8.0));
	err = setWriteMem(0,memBuffer);
	if(err != AMSCommunication::NoError)
			return err;

	err = setExecuteCommand(0x19);

	return err;
}

AMSCommunication::Error USBProgrammerCommunication::adcInitialize()
{
	if (!connected)
		return ConnectionError;

	if(activeType != ADC)
		setCommunicationType(ADC);

	return setExecuteCommand(0x01);
}

AMSCommunication::Error USBProgrammerCommunication::adcSample(unsigned char channel, unsigned short* value)
{
	unsigned char memBuffer[16] = {0};
	AMSCommunication::Error err = NoError;
	
	QMutex mutex;
	QMutexLocker mutexLocker(&mutex);
		
	if(activeType != ADC)
		setCommunicationType(ADC);

	if(!typeInitialized[ADC])
	{
		err = adcInitialize();
		if(err != AMSCommunication::NoError)
			return err;
		typeInitialized[ADC] = true;
	}

	memBuffer[0] = channel;

	err = setWriteMem(0,memBuffer);
	if(err != AMSCommunication::NoError)
			return err;

	err = setExecuteCommand(0x02);
	if(err != AMSCommunication::NoError)
			return err;
	
	err = setReadMemAddress(0x0002);
	if(err != AMSCommunication::NoError)
			return err;

	err = this->getReadMem(memBuffer);
	*value = *(unsigned short*)memBuffer;

	return err;

}

void USBProgrammerCommunication::setCommunicationType(CommunicationType type)
{
	if(activeType == PDB && typeInitialized[PDB])
	{
		pdbDeinitialize();
		typeInitialized[PDB] = false;
	}
	activeType = type;
}

AMSCommunication::Error USBProgrammerCommunication::pdbInitialize()
{
	if (!connected)
		return ConnectionError;

	if(activeType != PDB)
		setCommunicationType(PDB);

	return setExecuteCommand(0x41);
}

AMSCommunication::Error USBProgrammerCommunication::pdbDeinitialize()
{
	if(activeType == PDB)
		return setExecuteCommand(0x42);
	else
		return NoError;
}
AMSCommunication::Error USBProgrammerCommunication::pdbRead(unsigned char die, unsigned char adresses[2], unsigned char values[2])
{
	unsigned char memBuffer[16] = {0};
	unsigned char parityCheck[16] = {0};
	AMSCommunication::Error err = NoError;

	QMutex mutex;
	QMutexLocker mutexLocker(&mutex);
	
	if(activeType != PDB)
		setCommunicationType(PDB);

	if(!typeInitialized[PDB])
	{
		err = pdbInitialize();
		if(err != AMSCommunication::NoError)
			return err;
		typeInitialized[PDB] = true;
	}

	memBuffer[0] = die;
	memBuffer[1] = 0xb;
	memBuffer[2] = adresses[0];
	memBuffer[3] = adresses[1];
	
	err = setWriteMem(0,memBuffer);
	if(err != AMSCommunication::NoError)
		return err;

	err = setExecuteCommand(0x3b);
	if(err != AMSCommunication::NoError)
		return err;

	err = this->setReadMemAddress(0x1);
	if(err != AMSCommunication::NoError)
		return err;

	err = this->getReadMem(memBuffer);
	if(err != AMSCommunication::NoError)
		return err;

	err = this->setReadMemAddress(0x11);
	if(err != AMSCommunication::NoError)
		return err;

	/* FIXME: add parity check, not supported in current Firmware-Verion*/
	err = this->getReadMem(parityCheck);

	values[0] = 0;
	values[1] = 0;

	//value for adress comes first
	for(int i = 0; i < 8; i ++)
		values[1] |= memBuffer[i] << i;

	for(int i = 0; i < 8; i ++)
		values[0] |= memBuffer[i+8] << i;

	return err;
}

AMSCommunication::Error USBProgrammerCommunication::pdbWrite(unsigned char die, unsigned char adress, unsigned char value)
{
	unsigned char memBuffer[16] = {0};
	AMSCommunication::Error err = NoError;

	QMutex mutex;
	QMutexLocker mutexLocker(&mutex);
	
	if(activeType != PDB)
		setCommunicationType(PDB);

	if(!typeInitialized[PDB])
	{
		err = pdbInitialize();
		if(err != AMSCommunication::NoError)
			return err;
		typeInitialized[PDB] = true;
	}

	memBuffer[0] = die;
	memBuffer[1] = 0xc;
	memBuffer[2] = adress;
	memBuffer[3] = value;
	
	err = setWriteMem(0,memBuffer);
	if(err != AMSCommunication::NoError)
		return err;

	err = setExecuteCommand(0x3c);
	if(err != AMSCommunication::NoError)
		return err;

	return err;
}

AMSCommunication::Error USBProgrammerCommunication::pdbRead128(unsigned char die, unsigned char buffer[16])
{
	unsigned char memBuffer[16] = {0};
	unsigned char parityCheck[16] = {0}; 

	AMSCommunication::Error err = NoError;

	QMutex mutex;
	QMutexLocker mutexLocker(&mutex);

	if(activeType != PDB)
		setCommunicationType(PDB);

	if(!typeInitialized[PDB])
	{
		err = pdbInitialize();
		if(err != AMSCommunication::NoError)
			return err;
		typeInitialized[PDB] = true;
	}

	memBuffer[0] = die;
	
	err = setWriteMem(0,memBuffer);
	if(err != AMSCommunication::NoError)
		return err;

	err = setExecuteCommand(0x39);
	if(err != AMSCommunication::NoError)
		return err;

	//set all buffervalues to zero if not already done
	for(int i = 0; i < 16; i++)
		buffer[i] = 0;

	for(int i = 0; i < 8; i++)
	{
		unsigned char numberOfBits = 0;
		// + i*20 because buffer is a field with blocks of 2 bytes + 4 bits for parity
		// check and each bit is stored in one byte
		// + i* 2 because two bytes are used as padding between two frames
		err = this->setReadMemAddress(0x1 + i*20 + 2*i);
		if(err != AMSCommunication::NoError)
			return err;

		err = this->getReadMem(memBuffer);
		if(err != AMSCommunication::NoError)
			return err;

		err = this->setReadMemAddress(0x11+ i*20 + 2*i);
		if(err != AMSCommunication::NoError)
			return err;

		err = this->getReadMem(parityCheck);
		if(err != AMSCommunication::NoError)
			return err;

		for(int byteIter = 0; byteIter < 8; byteIter ++)
		{
			// byte with higher adress comes first
			buffer[2*i + 1] |= memBuffer[byteIter ] << byteIter;
			if(memBuffer[byteIter])
				numberOfBits ++;
		}

		for(int byteIter = 0; byteIter < 8; byteIter ++)
		{
			buffer[2*i ] |= memBuffer[byteIter + 8] << byteIter;
			if(memBuffer[byteIter + 8])
				numberOfBits ++;
		}

		// parity check (but not for the last frame, as this is not supported by
		// firmware-version)
		if((i < 7) && (numberOfBits + parityCheck[3]) % 2)
		{
			qDebug("parity check failed");
			return ReadError;	
		}
	}

	return err;
}


AMSCommunication::Error USBProgrammerCommunication::pdbWrite128(unsigned char die, unsigned char buffer[16])
{
	unsigned char memBuffer[32] = {0};
	unsigned char write128Command = 0x9;
	unsigned char writeContinueCommand = 0x1;

	AMSCommunication::Error err = NoError;

	QMutex mutex;
	QMutexLocker mutexLocker(&mutex);

	if(activeType != PDB)
		setCommunicationType(PDB);

	if(!typeInitialized[PDB])
	{
		err = pdbInitialize();
		if(err != AMSCommunication::NoError)
			return err;
		typeInitialized[PDB] = true;
	}

	memBuffer[0] = die;
	
	for( int i = 0 ; i < 8; i++)
	{
		memBuffer[1 + 3*i] = i == 0 ? write128Command : writeContinueCommand;
		memBuffer[1 + 3*i + 1] = buffer[2*i + 1];
		memBuffer[1 + 3*i + 2] = buffer[2*i];
	}

	err = this->setWriteMem(0, memBuffer);
	if(err != AMSCommunication::NoError)
		return err;

	err = this->setWriteMem(0x10, memBuffer + 16);
	if(err != AMSCommunication::NoError)
		return err;

	err = this->setExecuteCommand(0x3A);

	return err;
}

AMSCommunication::Error USBProgrammerCommunication::pdbSendCommand(unsigned char die, unsigned char command)
{
	unsigned char memBuffer[16] = {0};
	AMSCommunication::Error err = NoError;

	QMutex mutex;
	QMutexLocker mutexLocker(&mutex);
	
	if(activeType != PDB)
		setCommunicationType(PDB);

	if(!typeInitialized[PDB])
	{
		err = pdbInitialize();
		if(err != AMSCommunication::NoError)
			return err;
		typeInitialized[PDB] = true;
	}

	memBuffer[0] = die;
	memBuffer[1] = command;
	
	err = setWriteMem(0,memBuffer);
	if(err != AMSCommunication::NoError)
		return err;

	err = setExecuteCommand(0x4d);
	if(err != AMSCommunication::NoError)
		return err;

	return err;
}

AMSCommunication::Error USBProgrammerCommunication::enterBootloaderMode()
{
    return setExecuteCommand(78);
}

AMSCommunication::Error USBProgrammerCommunication::spiConfigure(unsigned int spiFrequency
                                                                 , unsigned char clkPhase
                                                                 , unsigned char clkPolarity
                                                                 , unsigned char csIsActiveLow
                                                                 , unsigned char use3WireSpi)
{
    AMSCommunication::Error err = NoError;
    unsigned char memBuffer[16] = {0};

    if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}

	if (NoError == err)
	{
        memBuffer[0] = spiFrequency & 0xFF;
        memBuffer[1] = (spiFrequency & 0xFF00) >> 8;
        memBuffer[2] = (spiFrequency & 0xFF0000) >> 16;
        memBuffer[3] = (spiFrequency & 0xFF000000) >> 24;

        memBuffer[4] = clkPhase;
        memBuffer[5] = clkPolarity;
        memBuffer[6] = csIsActiveLow;
        memBuffer[7] = use3WireSpi;

#if (DEBUG_MUTEX_USAGE == 1)
	    qDebug() << "mutex->lock() spiConfigure";
#endif
        mutex->lock();

        err = setWriteMem(0x00, memBuffer);
        if(err != AMSCommunication::NoError)
            goto out;

        err = setExecuteCommand(72);
	    if(err != AMSCommunication::NoError)
		    goto out;

out:
        mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	    qDebug() << "mutex->unlock()";
#endif

    }

    return err;
}

AMSCommunication::Error USBProgrammerCommunication::spiReadWrite(unsigned char * in_buf, unsigned char * out_buf, unsigned char nrOfBytes)
{
    AMSCommunication::Error err = NoError;
    unsigned char memBuffer[16] = {0};
    unsigned int i = 0;
    unsigned int startAddr = 0;

    if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}

#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() spiReadWrite(" << in_buf[0] << ")";
#endif
	mutex->lock();

	if (NoError == err)
	{
        memBuffer[i++] = nrOfBytes;
        while(i < ((unsigned int)nrOfBytes + 1))
        {
            memBuffer[i-startAddr] = in_buf[i-1];
            i++;
            if ((i%16) == 0)
            {
                err = setWriteMem(startAddr, memBuffer);
                if(err != AMSCommunication::NoError)
		            goto out;
                startAddr += 16;
            }
        }
        if (i%16)
        {
            setWriteMem(startAddr, memBuffer);
            if(err != AMSCommunication::NoError)
		        goto out;
        }

        err = setExecuteCommand(73);
	    if(err != AMSCommunication::NoError)
		    goto out;

        unsigned char in_progress = 1;
	    if ( (err = setReadMemAddress(15)) != NoError) goto out;
	    while ( in_progress)
	    {
		    if ( (err = getReadMem(memBuffer)) != NoError) goto out;
		    in_progress = memBuffer[0];
	    }

        // read out all bytes, starting at address 0x03
        startAddr = 0x01;
        i = 0;
        while(i < (nrOfBytes))
        {
            i++;
            if ((i%16) == 0)
            {
                err = this->setReadMemAddress(startAddr);
	            if(err != AMSCommunication::NoError)
		            goto out;
                err = this->getReadMem(&out_buf[startAddr-1]);
	            if(err != AMSCommunication::NoError)
		            goto out;
                startAddr += 16;
            }
        }
        if (i%16)
        {
            err = this->setReadMemAddress(startAddr);
            if(err != AMSCommunication::NoError)
		        goto out;
            err = this->getReadMem(&out_buf[startAddr-1]);
	        if(err != AMSCommunication::NoError)
		        goto out;
        }
    }
out:
    mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock()";
#endif

    return err;
}

AMSCommunication::Error USBProgrammerCommunication::AS5401EepromTransfer(unsigned short addr
                                                                         , unsigned char * in_buf
                                                                         , unsigned char * out_buf
                                                                         , unsigned char nrOfBytes
                                                                         , bool write)
{
    AMSCommunication::Error err = NoError;
    unsigned char memBuffer[16] = {0};
    unsigned int i = 0;
    unsigned int bufIdx = 0;
    unsigned int startAddr = 0;

    if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}

#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() AS5401EepromTransfer(" << in_buf[0] << ")";
#endif
	mutex->lock();

	if (NoError == err)
	{
        memBuffer[i++] = nrOfBytes;
        memBuffer[i++] = (unsigned char)(write ? 1 : 0);
        memBuffer[i++] = (addr >> 8) & 0xFF;
        memBuffer[i++] = (addr & 0xFF);
        if (write)
        {
            while(bufIdx < nrOfBytes)
            {
                memBuffer[i-startAddr] = in_buf[bufIdx];
                i++;
                bufIdx++;
                if ((i%16) == 0)
                {
                    err = setWriteMem(startAddr, memBuffer);
                    if(err != AMSCommunication::NoError)
		                goto out;
                    startAddr += 16;
                }
            }
        }
        if (i%16)
        {
            setWriteMem(startAddr, memBuffer);
            if(err != AMSCommunication::NoError)
		        goto out;
        }

        err = setExecuteCommand(83);
	    if(err != AMSCommunication::NoError)
		    goto out;

        unsigned char in_progress = 1;
	    if ( (err = setReadMemAddress(0)) != NoError) goto out;
	    while ( in_progress)
	    {
		    if ( (err = getReadMem(memBuffer)) != NoError) goto out;
		    in_progress = memBuffer[0];
	    }

        err = this->setReadMemAddress(0x04);
	    if(err != AMSCommunication::NoError)
		    goto out;

	    err = this->getReadMem(out_buf);
	    if(err != AMSCommunication::NoError)
		    goto out;
    }
out:
    mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock()";
#endif

    return err;
}

AMSCommunication::Error USBProgrammerCommunication::spiAS5400WritePage(unsigned short startAddr, unsigned char * in_buf, unsigned char nrOfBytes)
{
    AMSCommunication::Error err = NoError;
    unsigned char memBuffer[19] = {0};
    unsigned int i = 0;
    unsigned int memAddr = 0;

    if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() spiAS5400WritePage(" << startAddr << ")";
#endif
	mutex->lock();

	if (NoError == err)
	{
        memBuffer[i++] = startAddr & 0xFF;
        memBuffer[i++] = (startAddr & 0xFF00) >> 8;
        memBuffer[i++] = nrOfBytes;
        while(i < ((unsigned int)nrOfBytes + 3))
        {
            memBuffer[i] = in_buf[i-3];
            i++;
            if ((i%16) == 0)
            {
                err = setWriteMem(memAddr, memBuffer);
                if(err != AMSCommunication::NoError)
		            goto out;
                memAddr += 16;
            }
        }
        /* FIXME: not very nice, needs to be reworked */
        /* check if number of bytes where greater than 13 */
        if (i%16)
        {
            memBuffer[0] = memBuffer[memAddr];
            memBuffer[1] = memBuffer[memAddr+1];
            memBuffer[2] = memBuffer[memAddr+2];
            err = setWriteMem(memAddr, memBuffer);
            if(err != AMSCommunication::NoError)
		        goto out;
        }

        err = setExecuteCommand(81);
	    if(err != AMSCommunication::NoError)
		    goto out;
    }
out:
    mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock()";
#endif

    return err;
}

AMSCommunication::Error USBProgrammerCommunication::spiAS5400ReadHallData(unsigned short startAddr, unsigned char * out_buf, unsigned char nrOfBytes, unsigned char readSel)
{
    AMSCommunication::Error err = NoError;
    unsigned char memBuffer[19] = {0};
    unsigned int i = 0;
    unsigned int memAddr = 0;

    if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() spiAS5400ReadHallData(" << startAddr << ")";
#endif
	mutex->lock();

	if (NoError == err)
	{
        memBuffer[i++] = startAddr & 0xFF;
        memBuffer[i++] = (startAddr & 0xFF00) >> 8;
        memBuffer[i++] = nrOfBytes;
        memBuffer[i++] = readSel;
        
        err = setWriteMem(memAddr, memBuffer);
        if(err != AMSCommunication::NoError)
		    goto out;

        err = setExecuteCommand(82);
	    if(err != AMSCommunication::NoError)
		    goto out;

        // FIXME add wait for read-back

        err = this->setReadMemAddress(0x03);
	    if(err != AMSCommunication::NoError)
		    goto out;

	    err = this->getReadMem(out_buf);
	    if(err != AMSCommunication::NoError)
		    goto out;
    }

out:
    mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->unlock()";
#endif

    return err;
}

unsigned short USBProgrammerCommunication::dacVzapVoltageToValue(double voltage)
{
    unsigned short value;

    value = ((unsigned short)(voltage / DAC_VZAP_GAIN / DAC_VREF * 4096.0));
    if (value > 4095)
        value = 4095;
    return value;
}

unsigned short USBProgrammerCommunication::dacVddVoltageToValue(double voltage)
{
    unsigned short value;

    value = ((unsigned short)(voltage / DAC_VDD_GAIN / DAC_VREF * 4096.0));
    if (value > 4095)
        value = 4095;
    return value;
}

AMSCommunication::Error USBProgrammerCommunication::dacUpdateChannel(unsigned char dac, unsigned char channel, double voltage)
{
    AMSCommunication::Error err = NoError;
    unsigned char memBuffer[16] = {0};
    unsigned short value = 0;

if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() dacUpdateChannel";
#endif
	mutex->lock();

	if (NoError == err)
	{
        if ((dac == 1) || (dac == 0 && channel != 0))
            value = dacVzapVoltageToValue(voltage);
        else
            value = dacVddVoltageToValue(voltage);

        memBuffer[0] = dac;
        memBuffer[1] = channel;
        memBuffer[2] = value & 0xFF;
        memBuffer[3] = ((value & 0xFF00) >> 8);

        err = setWriteMem(0x00, memBuffer);
        if(err != AMSCommunication::NoError)
            goto out;

        err = setExecuteCommand(12);
        if(err != AMSCommunication::NoError)
	        goto out;
    }

out:
        mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	    qDebug() << "mutex->unlock()";
#endif

    return err;
}

AMSCommunication::Error USBProgrammerCommunication::portsInit()
{
    AMSCommunication::Error err = NoError;

if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() portsInit";
#endif
	mutex->lock();

	if (NoError == err)
	{
        err = setExecuteCommand(51);
        if(err != AMSCommunication::NoError)
	        goto out;
    }

out:
        mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	    qDebug() << "mutex->unlock()";
#endif

    return err;
}

AMSCommunication::Error USBProgrammerCommunication::vzapMuxInit()
{
    AMSCommunication::Error err = NoError;

if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() vzapMuxInit";
#endif
	mutex->lock();

	if (NoError == err)
	{
        err = setExecuteCommand(15);
        if(err != AMSCommunication::NoError)
	        goto out;
    }

out:
        mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	    qDebug() << "mutex->unlock()";
#endif

    return err;
}

AMSCommunication::Error USBProgrammerCommunication::vzapMuxSelect(unsigned char channel)
{
    AMSCommunication::Error err = NoError;
    unsigned char memBuffer[16] = {0};

if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() vzapMuxSelect";
#endif
	mutex->lock();

	if (NoError == err)
	{
        memBuffer[0] = channel;

        err = setWriteMem(0x00, memBuffer);
        if(err != AMSCommunication::NoError)
            goto out;

        err = setExecuteCommand(16);
        if(err != AMSCommunication::NoError)
	        goto out;
    }

out:
        mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	    qDebug() << "mutex->unlock()";
#endif

    return err;
}

AMSCommunication::Error USBProgrammerCommunication::vzapSwitch(bool state)
{
    AMSCommunication::Error err = NoError;
    unsigned char memBuffer[16] = {0};

if(!connected)
	{
		/* try to connect */
		err = this->connect();
	}
#if (DEBUG_MUTEX_USAGE == 1)
	qDebug() << "mutex->lock() vzapSwitch";
#endif
	mutex->lock();

	if (NoError == err)
	{
        memBuffer[0] = (unsigned char)(state ? 1 : 0);

        err = setWriteMem(0x00, memBuffer);
        if(err != AMSCommunication::NoError)
            goto out;

        err = setExecuteCommand(14);
        if(err != AMSCommunication::NoError)
	        goto out;
    }

out:
        mutex->unlock();
#if (DEBUG_MUTEX_USAGE == 1)
	    qDebug() << "mutex->unlock()";
#endif

    return err;
}


AMSCommunication::Error USBProgrammerCommunication::uartConfigure()
{
    AMSCommunication::Error err = NoError;

    return err;
}


USBProgrammerCommunication::~USBProgrammerCommunication()
{
    QWidget::disconnect(this, 0, 0, 0);
}

AMSCommunication::Error USBProgrammerCommunication::AS5162UartRegisterRead(unsigned char RegAddr, unsigned short* RegValuePointer)
{

  /* 
    cmdUartAS5162RegisterReadID,               // 84
    cmdUartAS5162RegisterWriteID,              // 85
  */

  unsigned char buffer[64];
  unsigned char usbprogrammer_uart_error;

	AMSCommunication::Error err = NoError;

#if (DEBUG_MUTEX_USAGE == 1)
  qDebug() << "USBProgrammerCommunication::AS5162UartRegisterRead";
#endif

  // scoped locking object
  QMutexLocker scoped_lock(mutex);

  // fill first byte with RegAddr we want to read
  buffer[0] = RegAddr;

  if ((err = setWriteMem(0,buffer)) != NoError) return err; 

  if ((err = setExecuteCommand(84)) != NoError) return err; 

  if ((err = setReadMemAddress(0)) != NoError) return err; 

  if ((err = getReadMem(buffer)) != NoError) return err; 
  
  /* 
    get error state from usbprogrammers' uart communication
    #define UART_AS5162_OK                0
    #define UART_AS5162_ERROR_TIMEOUT     1
    #define UART_AS5162_ERROR_PARITY      2
  */
  
  usbprogrammer_uart_error = buffer[0];

  // timeout - no chip connected
  if(usbprogrammer_uart_error != 0)
    {
      if(usbprogrammer_uart_error == 1)
        return AMSCommunication::Timeout;

      if(usbprogrammer_uart_error == 1)
        return AMSCommunication::Parity;
    }

  // get the value
  *RegValuePointer = (unsigned short) (buffer[2] << 8);
  *RegValuePointer |= buffer[1];

  return NoError;
}

AMSCommunication::Error USBProgrammerCommunication::AS5162UartRegisterWrite(unsigned char RegAddr, unsigned short RegValue)
{

  unsigned char buffer[64];
	AMSCommunication::Error err = NoError;

#if (DEBUG_MUTEX_USAGE == 1)
  qDebug() << "USBProgrammerCommunication::AS5162UartRegisterRead";
#endif

  QMutexLocker scoped_lock(mutex);

  buffer[0] = RegAddr;
  buffer[1] = (unsigned char) (RegValue >> 8);
  buffer[2] = (unsigned char) (RegValue);

  if ((err = setWriteMem(0,buffer)) != NoError) return err; 

  if ((err = setExecuteCommand(85)) != NoError) return err; 

  if ((err = setReadMemAddress(0)) != NoError) return err; 

  if ((err = getReadMem(buffer)) != NoError) return err; 
  
  return NoError;
}