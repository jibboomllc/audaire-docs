/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */


/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  AmsComStreamWrapper class header file
 *
 *  AmsComStreamWrapper is a derived class of AMSCommunication, so that it fulfills the interface
 * needed by the current version of the register map.
 */


#include "AmsComStreamWrapper.hxx"

AmsComStreamWrapper::AmsComStreamWrapper( unsigned short pid, unsigned short vid )
	: AMSCommunication( )
	, itsDriver( pid, vid )
	, itsStream( itsDriver )
{
}

AmsComStreamWrapper::~AmsComStreamWrapper( )
{
}

AMSCommunication::Error AmsComStreamWrapper::hwReadRegister(unsigned char reg, unsigned char *val)
{
	/* currently no split read is used */
	unsigned int value = 0;
	I2cRegisterObject obj( devAddr, reg, value );
	itsStream >> obj >> AMS_FLUSH;
	*val = static_cast< unsigned char >(obj.get());
	return ( itsStream.lastError() == 0 ? ( obj.status() ==  AMS_STREAM_NO_ERROR ? NoError : ReadError ) : ReadError );
}

AMSCommunication::Error AmsComStreamWrapper::hwWriteRegister(unsigned char reg, unsigned char val)
{
	I2cRegisterObject obj( devAddr, reg, val );
	itsStream << obj << AMS_FLUSH;
	return ( itsStream.lastError() == 0 ? NoError : WriteError );
}

AMSCommunication::Error AmsComStreamWrapper::hwSendCommand(QString command, QString * answer)
{
	return WriteError; /* not implemented */
}

AMSCommunication::Error AmsComStreamWrapper::hwConnect()
{
    AMSCommunication::Error result = ConnectionError;
    if ( itsStream.open( ) )
    {
        if ( itsStream.isProtocolVersionCompatible( false ) )
        {
            result = NoError;
        }
        else
        {
            if ( itsStream.lastError() == AmsComDriver::NoError )
            {
                result = IncompatibleVersion;
            }
            /* else disconnected while talking to the device */
        }
    }
	return result;
}

void AmsComStreamWrapper::hwDisconnect()
{
	itsStream.close( );
}

