/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */


/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  AmsComStreamI2CSPIInterface class header file
 *
 */


#include "AmsComStream.h"
#include "AmsComStreamI2CSPIInterface.h"
#include "I2cComObjects.h"

AmsComStreamI2CSPIInterface::AmsComStreamI2CSPIInterface( AmsComStream & stream )
    : AmsI2CSPIInterface( )
    , itsStream( stream )
{
}


bool AmsComStreamI2CSPIInterface::i2cConfigure( unsigned int frequencyInHz, bool useDefaultI2C )
{
    itsIsI2CConfigured = false;
    itsIsSPIConfigured = false; 

    itsI2CFrequencyInHz = frequencyInHz;
    itsI2CUseDefault = useDefaultI2C;

    I2cConfigObject obj( ( itsI2CUseDefault ? 1 : 2 ) , 0xFF, itsI2CFrequencyInHz );
    itsStream << obj << AMS_FLUSH;

    if ( itsStream.lastError() == 0 )
    {
        itsIsI2CConfigured = true;
    }
    return itsIsI2CConfigured;
}

bool AmsComStreamI2CSPIInterface::i2cTxRx ( unsigned char deviceAddress, unsigned int sendLen, const unsigned char * send, unsigned int receiveLen, unsigned char * receive )
{
    itsStream.lastError(); /* clear last error */
    if ( sendLen )
    {
        if ( itsIsI2CConfigured || i2cConfigure( itsI2CFrequencyInHz, itsI2CUseDefault ) )
        {
            if ( receiveLen == 0 ) /* no receive - transmit only */
            {

                I2cBlockWriteObject tx( deviceAddress, unsigned int( send[ 0 ] ), int( sendLen - 1 ), const_cast< unsigned char *>( &( send[ 1 ])  ));
                itsStream >> tx >> AMS_FLUSH;
                return ( itsStream.lastError() == 0 && tx.valid() && ( tx.status() == AMS_STREAM_NO_ERROR ) );
            }
            else if ( sendLen == 1 )
            {
                I2cBlockObject rx( deviceAddress, unsigned int( send[ 0 ] ) /*reg-addr*/, int( receiveLen ) /* block size */ );
                itsStream >> rx >> AMS_FLUSH;
                if ( itsStream.lastError() == 0 && rx.valid() )
                {
                    receiveLen = ( receiveLen > unsigned int ( rx.deserialiseSize() ) ? rx.deserialiseSize() : receiveLen );
                    memcpy( receive, rx.get(), receiveLen );
                    return true;
                }
            }
            else if ( sendLen > 1 )
            {
                I2cBlockReadObject rx( deviceAddress, int( sendLen ), int( receiveLen ), const_cast< unsigned char *>( send ) );
                itsStream >> rx >> AMS_FLUSH;
                if ( itsStream.lastError() == 0 && rx.valid() )
                {
                    receiveLen = ( receiveLen > unsigned int ( rx.deserialiseSize() ) ? rx.deserialiseSize() : receiveLen );
                    memcpy( receive, rx.rxGet(), receiveLen );
                    return true;
                }
            }
            /* else not supported yet */
        }
    }
    return false;
}


/* function to configure spi for a specific frequency (range somewhat 250000 - 8000000), for polarity and phase */
bool AmsComStreamI2CSPIInterface::spiConfigure( unsigned int frequencyInHz, int polarity, int phase )
{
    itsIsI2CConfigured = false;
    itsIsSPIConfigured = false; 

    itsSPIFrequencyInHz = frequencyInHz;
    itsSPIPolarity = polarity;
    itsSPIPhase = phase;

    SpiConfigObject config( SpiConfigObject::theSpi1Module, 0 /* ignored by firmware */, itsSPIFrequencyInHz, itsSPIPhase, itsSPIPolarity);
    itsStream << config << AMS_FLUSH;

    if ( itsStream.lastError( ) == 0 )
    {
        itsIsSPIConfigured = true;
    }
    return itsIsSPIConfigured;
}

bool AmsComStreamI2CSPIInterface::spiTxRx( unsigned int sendLen, const unsigned char * send, unsigned char * receive )
{
    itsStream.lastError(); /* clear last error */
    if ( itsIsSPIConfigured || spiConfigure( itsSPIFrequencyInHz, itsSPIPolarity, itsSPIPhase ) )
    {
        SpiFlexObject obj( sendLen, send, sendLen );
        itsStream >> obj >> AMS_FLUSH;
        if ( itsStream.lastError() == 0 )
        {
            memcpy ( receive, obj.rxData(), obj.dataSize() );
            return true;
        }
    }
    memset( receive, 0, sendLen );
    return false;
}
