/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComCapabilities.cpp
 *
 *  \author M. Arpa
 *
 *  \brief A communication can have different connection states, connection modes
 * and device detection states. With these classes we try to provide a common
 * access point to these capabilities.
 */

#include "AmsComCapabilities.hxx"


const char * AmsComConnectionState::stateStrings[ ] = 
{ "unknown"
, "disconnecting"
, "disconnected"
, "connecting"
, "connected"
, "out-of-bound value"
};

const char * AmsComConnectionState::stateToString ( int state )
{
    int max = ( sizeof( stateStrings ) / sizeof( const char * ) ) - 1;
    if ( state > max || state < 0 )
    {
        state = max;
    }
    return stateStrings[ state ];
}


const char * AmsComDeviceState::deviceStateStrings[ ] =
{ "unknown"
, "disconnecting"
, "disconnected"
, "connecting"
, "connected"
, "out-of-bound value"
};

const char * AmsComDeviceState::deviceStateToString ( int state )
{
    int max = ( sizeof( deviceStateStrings ) / sizeof( const char * ) ) - 1;
    if ( state > max || state < 0 )
    {
        state = max;
    }
    return deviceStateStrings[ state ];

}

bool AmsComDetectionMode::set ( bool autoDetectionMode ) 
{ 
    if ( autoDetectionMode )
    {
        if ( itsIsAutoDetectionPossible )
        {
            itsAutoDetection = true;
        }
        return itsIsAutoDetectionPossible;
    }
    else
    {
        itsAutoDetection = false;
        return true;
    }
}

