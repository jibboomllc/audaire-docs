/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams Stream Communication
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComObject.cpp
 *
 *  \author M. Arpa
 *
 *  \brief Implementation of the base class.
 */

#include "AmsComObject.h"
#include <QString>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>


AmsComObject::AmsComObject 
	( unsigned char protocol
	, int serialSize
	, int rxSerialSize
	, int deserialSize
    , unsigned int tracePattern
	) 
	: itsTracePattern( tracePattern )
    , itsSerialSize( serialSize )
	, itsRxSerialSize( rxSerialSize)
	, itsDeserialSize( deserialSize ) 
	, itsProtocol( protocol )
	, itsStatus ( Ok )
	, itsNext( 0 )
    , itsIsValid( true )
{ 
}

AmsComObject::AmsComObject 
	( const AmsComObject & other 
	) 
	: itsProtocol( other.itsProtocol )
	, itsSerialSize( other.itsSerialSize )
	, itsRxSerialSize( other.itsRxSerialSize )
	, itsDeserialSize( other.itsDeserialSize )
	, itsStatus ( other.itsStatus )
	, itsNext( 0 ) /* no copyed object can be in the line for reading */
    , itsIsValid( other.itsIsValid )
{ 
}


bool AmsComObject::serialising( unsigned char * buffer, unsigned int size, unsigned int value ) 
{
	if ( size == 1 )
	{
		buffer[ 0 ] = static_cast< unsigned char >( value & 0xFF ); 
	}
	else if ( size == 2 )
	{
		buffer[ 0 ] = static_cast< unsigned char >( ( value >> 8 ) & 0xFF ); /* MSB */
		buffer[ 1 ] = static_cast< unsigned char >( value  & 0xFF );		  /* LSB */
	}
	else if ( size == 4 )
	{
		buffer[ 0 ] = static_cast< unsigned char >( ( value >> 24 ) & 0xFF ); /* MSB */
		buffer[ 1 ] = static_cast< unsigned char >( ( value >> 16 ) & 0xFF );	
		buffer[ 2 ] = static_cast< unsigned char >( ( value >> 8 ) & 0xFF ); 
		buffer[ 3 ] = static_cast< unsigned char >( value  & 0xFF );		  /* LSB */
	}
	else if ( size != 0 ) /* if not empty -> this is an error */
	{
		return false;
	}
    return true;
}

bool AmsComObject::deserialising( unsigned char * buffer, unsigned int size, unsigned int & value ) 
{
	if ( size == 1 )
	{
		value = buffer[ 0 ]; 
	}
	else if ( size == 2 )
	{
		value = buffer[ 0 ];					/* MSB */
		value = ( value << 8 ) | buffer[ 1 ];	/* LSB */
	}
	else if ( size == 4 )
	{
		value = buffer[ 0 ];					/* MSB */
		value = ( value << 8 ) | buffer[ 1 ];	
		value = ( value << 8 ) | buffer[ 2 ];	
		value = ( value << 8 ) | buffer[ 3 ];	/* LSB */
	}
	else if ( size != 0 ) /* if not empty -> this is an error */
	{
		return false;
	}
    return true;
}

void AmsComObject::writeOpeningTag( QXmlStreamWriter * xml, const char * direction ) const
{
    if ( xml )
    {
        xml->writeStartElement( AMS_COM_XML_TAG );
        xml->writeAttribute( AMS_COM_XML_PROTOCOL_ID, QString::number( itsProtocol, AMS_COM_XML_NUMBER_BASE ) );
        if ( ! direction )
        {
            direction = AMS_COM_XML_NO_DIRECTION;
        }
        xml->writeAttribute( AMS_COM_XML_DIRECTION, QString( direction ) );
        xml->writeAttribute( AMS_COM_XML_TRACE_PATTERN, QString::number( itsTracePattern, AMS_COM_XML_NUMBER_BASE ) );
    }
}

void AmsComObject::writeClosingTag( QXmlStreamWriter * xml )
{
    if ( xml )
    {
        xml->writeEndElement( );
    }
}


bool AmsFlushObject::fill( QXmlStreamReader * xml )
{
    if ( xml )
    {
        xml->skipCurrentElement();
        return true;
    }
    return false;
}

