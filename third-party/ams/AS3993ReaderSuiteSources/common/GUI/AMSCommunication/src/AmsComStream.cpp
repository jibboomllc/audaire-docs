/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams Streaming Communication
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComStream.cpp
 *
 *  \author M. Arpa
 *
 *  \brief  Communication classes for streaming communication 
 */

#include "ams_stream.h"
#include "AmsComStream.h"
#include "AmsComPolicy.h"
#include "AMSTrace.hxx"


/* instanciate the flush so that everybody can use it */
AmsFlushObject AmsComStream::theFlush; 
const LateFlushPolicy AmsComStream::theLateFlushPolicy;
const ImmediateFlushPolicy AmsComStream::theImmediateFlushPolicy;

AmsComMutex::AmsComMutex( ) 
	: itsCounter( 0 )
	, itsLock( QMutex::Recursive )
{ 
}

AmsComMutex::~AmsComMutex( ) 
{ 
	while ( unlock( ) );
}

void AmsComMutex::lock( ) 
{ 
	itsLock.lock( ); 
	itsCounter++; 
}

bool AmsComMutex::unlock( ) 
{
	bool result = false;
	if ( itsCounter ) 
	{
		result = true;
		itsCounter--;
		itsLock.unlock( );
	}
	return result;
}


AmsComStream::AmsComStream 
	( AmsComDriver & driver 
	, const AmsComPolicy & policy
	) 
    : itsLastError ( AmsComDriver::NoError ) 
	, itsDriver( driver )
	, itsPolicy( &policy )
	, itsHook( new unsigned char [ AMS_STREAM_BUFFER_SIZE ] )
	, itsBufferSize( AMS_STREAM_MAX_DATA_SIZE )
	, itsBuffer( itsHook + AMS_STREAM_HEADER_SIZE )
	, itsLock( )
    , itsReadQueueHead( 0 )
    , itsReadQueueTail( 0 )
{ 
}

AmsComStream::~AmsComStream( )
{
    delete itsHook;
}

void AmsComStream::reset( )
{
    bool open = itsDriver.isOpened( );
    if ( open )
    {
        itsDriver.close( ); /* close the communication stream */
    }
	while ( itsLock.unlock( ) ); /* free all locks of this thread */
    itsReadQueueHead = 0;
    itsReadQueueTail = 0;
    lastError(); /* clear any pending error from previous open */
    if ( open )
    {
        itsDriver.open( );
    }
}

bool AmsComStream::open ( )
{ 
   	if ( ! isOpened( ) )
	{
        reset( );
		return itsDriver.open( ); 
	}
	return true;
}

bool AmsComStream::close ( )
{ 
	if ( isOpened( ) )
	{   /* no longer access unread objects */
        bool res = itsDriver.close( );
        reset( );
        return res;
	}
	return true;
} 


AmsComStream & AmsComStream::operator<< 
	( AmsFlushObject & flush
	) 
{
	itsLock.lock();
    QString telegram;
    QXmlStreamWriter xml( &telegram );
    xml.setAutoFormatting( true );
    flush.serialise( 0, 0, &xml );
	/* initiate an immediate transmit */
	itsLastError = itsDriver.flush( );
    AMSTrace::trc( telegram, itsLastError );
    while ( itsReadQueueHead )
    { /* have data to read in -> do it now */
         itsReadQueueTail = itsReadQueueHead->itsNext; /* get next object */
         itsReadQueueHead->itsNext = 0; /* allow object reuse */ 
         rxPartTwo( *itsReadQueueHead ); /* collect read data */
         itsReadQueueHead = itsReadQueueTail; /* move on to next object */
    }
	itsLock.unlock();
	return *this; 
}

AmsComStream & AmsComStream::operator<< 
	( AmsComObject & toTx 
	) 
{
	itsLock.lock();
    if ( toTx.protocol() != AMS_COM_FLUSH )
    {
        QString telegram;
        QXmlStreamWriter xml( &telegram );
        xml.setAutoFormatting( true );
	    if ( toTx.serialise( itsBuffer, itsBufferSize, &xml ) )
	    {
		    AMS_STREAM_HT_SET_PROTOCOL( itsHook, toTx.protocol( ) | AMS_COM_WRITE_READ_NOT ); /* protocol */
		    AMS_STREAM_HT_SET_TX_LENGTH( itsHook, toTx.serialiseSize( ) );     /* tx-size */
		    AMS_STREAM_HT_SET_RX_LENGTH( itsHook, 0 );                         /* rx-size */
		    itsLastError = itsDriver.txBuffer( itsHook, toTx.serialiseSize() + AMS_STREAM_HEADER_SIZE );
            AMSTrace::trc( telegram, itsLastError );
            if ( itsLastError == AmsComDriver::NoError && itsPolicy->doFlush( ) )
		    {
			    *this << theFlush;
		    }
	    }
    }
    else
    {
        *this << theFlush;
    }	
	itsLock.unlock();
	return *this;
}

AmsComStream & AmsComStream::operator>> 
	( AmsFlushObject & flush
	) 
{
    return *this << flush;
}

AmsComStream & AmsComStream::operator>> 
	( AmsComObject & toRx  
	)
{
	itsLock.lock();

    if ( toRx.itsNext == 0 && itsLastError == AmsComDriver::NoError )
    { /* object not in use for reading -> insert object in queue */
  
        if ( itsReadQueueTail ) /* insert at the tail */
        { /* already have an object in the queue */
            itsReadQueueTail->itsNext = &toRx;
            itsReadQueueTail = &toRx; /* adjust tail to new object */
        }
        else /* insert at the head = tail */
        {
            itsReadQueueHead = &toRx;
            itsReadQueueTail = &toRx;
        }

        /* now prepare outgoing data for reading */
		rxPartOne( toRx );
		if ( itsPolicy->doFlush( ) )
		{
			*this >> theFlush; 
		}
    }
    /* this is an error, cannot read back the same object twice -> reject it */
	itsLock.unlock();
	return *this;
}

void AmsComStream::rxPartOne 
	( AmsComObject & toRx 
	)
{
	/* increment the lock counter that we need another object to be received before
	   allowing other tx's or rx's */
	itsLock.lock();

    toRx.setValid( false ); /* object is not valid anymore -> need data to fill it */

	/* build header */
	AMS_STREAM_HT_SET_PROTOCOL( itsHook, toRx.protocol( ) & ~AMS_COM_WRITE_READ_NOT );/* protocol */
	AMS_STREAM_HT_SET_TX_LENGTH( itsHook, toRx.rxSerialiseSize( ) );   /* tx-size */
	AMS_STREAM_HT_SET_RX_LENGTH( itsHook, toRx.deserialiseSize( ) );   /* rx-size */
    
    QString telegram;
    QXmlStreamWriter xml( &telegram );
    xml.setAutoFormatting( true );
	if ( toRx.rxSerialise( itsBuffer, itsBufferSize, &xml ) )
	{
		itsLastError = itsDriver.txBuffer( itsHook, toRx.rxSerialiseSize() + AMS_STREAM_HEADER_SIZE );
        AMSTrace::trc( telegram, itsLastError );
	}
}

void AmsComStream::rxPartTwo
	( AmsComObject & toRx 
	)
{
	/* ( AMS_STREAM_HR_GET_PRTOCOL( itsHook )  == toRx.protocol( ) ) */
	/* ( AMS_STREAM_HR_GET_STATUS( itsHook )   == status == 0 == okay ) */
	/* ( AMS_STREAM_HR_GET_RX_LENGTH( itsHook) == toRx.deserialiseSize( ) ) */

	/* rx header - to get received length */
	itsLastError = itsDriver.rxBuffer( itsHook, AMS_STREAM_HEADER_SIZE );
	if ( itsLastError == AmsComDriver::NoError )
    {
        if ( AMS_STREAM_HR_GET_PROTOCOL( itsHook ) == ( toRx.protocol( ) & ~AMS_COM_WRITE_READ_NOT ) ) /* some simple sanity check */
	    { /* now get the size out of the header */
		    int rxSize = AMS_STREAM_HR_GET_RX_LENGTH( itsHook );  
		    /* read in the data */
		    itsLastError = itsDriver.rxBuffer( itsBuffer, rxSize );

            if ( itsLastError == AmsComDriver::NoError )
            { 
                toRx.setStatus( AMS_STREAM_HR_GET_STATUS( itsHook ) );
                QString telegram;
                QXmlStreamWriter xml( &telegram );
                xml.setAutoFormatting( true );

                toRx.setValid( true ); /* object is valid again -> data received */

                /* now get the data out of the buffer */
                toRx.deserialise( itsBuffer, rxSize, &xml );
                AMSTrace::trc( telegram, itsLastError );
            }
            else
            {
                toRx.setStatus( AmsComObject::RxError );
            }
        }
        else /* wrong protocol received */
        {
            toRx.setStatus( AmsComObject::WrongProtocol ); 
            if ( ! itsLastError )
            {
                itsLastError = AmsComDriver::RxDataMismatch; 
            }
        }
	}
    else
    {
        toRx.setStatus( AmsComObject::RxError );
    }

	/* object is totally received (or errornous), so we decrement the receive counter */
	itsLock.unlock(); 
}


const AmsComPolicy * AmsComStream::setPolicy
    ( const AmsComPolicy & policy 
    )
{ 
    const AmsComPolicy * old;
    itsLock.lock();
    old = itsPolicy; 
    itsPolicy = & policy;
    itsLock.unlock();
    return old; 
}

