/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */


/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  AmsUsbIssI2CSPIInterface class header file
 *
 */


#include "AmsUsbIssI2CSPIInterface.h"

bool AmsUsbIssI2CSPIInterface::i2cConfigure( unsigned int frequencyInHz, bool useDefaultI2C /* ignored on usb iss */ )
{
    itsIsI2CConfigured = false;
    itsIsSPIConfigured = false; 

    itsI2CFrequencyInHz = frequencyInHz;
    itsI2CUseDefault = useDefaultI2C;

    if ( itsIss.setI2CMode( itsI2CFrequencyInHz, true ) )
    {
        itsIsI2CConfigured = true;
    }
    return itsIsI2CConfigured;

}


bool AmsUsbIssI2CSPIInterface::i2cTxRx( unsigned char deviceAddress, unsigned int sendLen, const unsigned char * send, unsigned int receiveLen, unsigned char * receive ) 
{ 
    if ( itsIsI2CConfigured || i2cConfigure( itsI2CFrequencyInHz, itsI2CUseDefault ) )
    {
        return itsIss.i2cTxRx( deviceAddress, sendLen, send, receiveLen, receive );
    }
    return false;
}

bool AmsUsbIssI2CSPIInterface::spiConfigure( unsigned int frequencyInHz, int polarity, int phase )
{
    itsIsI2CConfigured = false;
    itsIsSPIConfigured = false; 

    itsSPIFrequencyInHz = frequencyInHz;
    itsSPIPolarity = polarity;
    itsSPIPhase = phase;

    if ( itsIss.setSpiMode( itsSPIPolarity, itsSPIPhase, itsSPIFrequencyInHz ) )
    {
        itsIsSPIConfigured = true;
    }
    return itsIsSPIConfigured;
}

bool AmsUsbIssI2CSPIInterface::spiTxRx( unsigned int sendLen, const unsigned char * send, unsigned char * receive ) 
{ 
    if ( itsIsSPIConfigured || spiConfigure( itsSPIFrequencyInHz, itsSPIPolarity, itsSPIPhase ) )
    {
        if ( itsIss.spiTxRx( sendLen, send, receive ) )
        {
            return true;
        }
    }
    memset( receive, 0, sendLen );
    return false;
}

