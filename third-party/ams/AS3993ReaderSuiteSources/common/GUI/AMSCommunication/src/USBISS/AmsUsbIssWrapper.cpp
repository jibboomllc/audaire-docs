/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */


/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  AmsUsbIssWrapper class header file
 *
 *  AmsUsbIssWrapper is a derived class of AMSCommunication, so that it fulfills the interface
 * needed by the current version of the register map.
 */


#include "AmsUsbIssWrapper.hxx"


AmsUsbIssWrapper::AmsUsbIssWrapper( int comPort )
	: AMSCommunication( )
	, itsIss( comPort )
{
}

AmsUsbIssWrapper::~AmsUsbIssWrapper( )
{
}

AMSCommunication::Error AmsUsbIssWrapper::hwReadRegister(unsigned char reg, unsigned char *val)
{
    return ( itsIss.i2cTxRx( devAddr, 1, &reg, 1, val ) ? NoError : ReadError );
}

AMSCommunication::Error AmsUsbIssWrapper::hwWriteRegister(unsigned char reg, unsigned char val)
{
    unsigned char tx[ 2 ];
    tx[ 0 ] = reg;
    tx[ 1 ] = val;
    return ( itsIss.i2cTx( devAddr, 2, tx ) ? NoError : WriteError );
}

AMSCommunication::Error AmsUsbIssWrapper::hwSendCommand(QString command, QString * answer)
{
	return WriteError; /* not implemented */
}

AMSCommunication::Error AmsUsbIssWrapper::hwConnect()
{
    AMSCommunication::Error result = ConnectionError;
    if ( itsIss.open( ) )
    {
        itsIss.setI2CMode( 100000UL, true );
        result = NoError;
    }
	return result;
}

void AmsUsbIssWrapper::hwDisconnect()
{
	itsIss.close( );
}
