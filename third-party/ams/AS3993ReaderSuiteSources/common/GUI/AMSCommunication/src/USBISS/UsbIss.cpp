/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "UsbIss.h"
#include <windows.h>
#include <string.h>

#define USB_ISS_MAX_I2C_SIZE          ( USB_ISS_MAX_I2C_TRANSFER_SIZE + 10 )
#define USB_ISS_MAX_SPI_SIZE          ( USB_ISS_MAX_SPI_TRANSFER_SIZE + 10 )

/* USB ISS Ack and Nack */
#define USB_ISS_ACK             0xFF
#define USB_ISS_NAK             0x00

/* USB ISS COmmands */
#define USB_ISS_CMD             0x5A

/* USB ISS SubCommands */
#define USB_ISS_VERSION         0x01
#define USB_ISS_MODE            0x02
#define USB_ISS_GET_SER_NUM     0x03


/* ISS_MODE: currently not handled here */
#define USB_ISS_IO_MODE         0x00
#define USB_ISS_IO_CHANGE       0x10

/* ISS_MODE: handled modes here */
#define USB_ISS_I2C_S_20KHZ     0x20
#define USB_ISS_I2C_S_50KHZ     0x30
#define USB_ISS_I2C_S_100KHZ    0x40
#define USB_ISS_I2C_S_400KHZ    0x50
#define USB_ISS_I2C_H_100KHZ    0x60
#define USB_ISS_I2C_H_400KHZ    0x70
#define USB_ISS_I2C_H_1000KHZ   0x80
#define USB_ISS_SPI_MODE        0x90
#define USB_ISS_SERIAL          0x01


/* USB ISS - I2C Mode Commands et. al. */
#define USB_ISS_I2C_DIRECT      0x57

#define USB_ISS_I2C_START       0x01
#define USB_ISS_I2C_RESTART     0x02
#define USB_ISS_I2C_STOP        0x03
#define USB_ISS_I2C_NACK        0x04 /* send nak instead of ack for read */
#define USB_ISS_I2C_READ        0x20
#define USB_ISS_I2C_WRITE       0x30

/* USB ISS - SPI Mode Command */
#define USB_ISS_SPI_DIRECT      0x61

UsbIss::UsbIss ( unsigned char comPortNumber ) 
    : itsHandle( INVALID_HANDLE_VALUE )
    , itsBaudRate( 19200 )
    , itsI2cMode( USB_ISS_I2C_S_100KHZ )
    , itsComPort( comPortNumber )
{
}

UsbIss::~UsbIss ( )
{
    close( );
}

void UsbIss::setComPort( unsigned char port )
{
    itsComPort = port;
}

void UsbIss::reset( )
{
    close( );
}

bool UsbIss::open ( )
{
    if ( itsHandle == INVALID_HANDLE_VALUE )
    {
        if ( itsComPort >= 0 && itsComPort < 10 )
        { /* try to open com port */
            char device [] = "\\\\.\\COMx";
            device[ strlen(device) - 1 ] = '0' + itsComPort;
            itsHandle = CreateFileA( device, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL );
        }
        if ( itsHandle != INVALID_HANDLE_VALUE )
        {
            DCB      commDcb;
            COMSTAT  commStat;
            DWORD    dwCommError;

            if ( ! ClearCommError( itsHandle, &dwCommError, &commStat))
            {
                close();
                return false;
            }

            if ( ! GetCommState( itsHandle, &commDcb ) )
            {
                close();
                return false;
            }

#if 0 /* only for information purpose */
            commDcb.DCBlength        = sizeof(DCB);              /* sizeof(DCB)                     */
            commDcb.BaudRate         = 19200;                    /* Baudrate at which running       */
            commDcb.fBinary          = true;                     /* Binary Mode (skip EOF check)    */
            commDcb.fParity          = 0;                        /* Enable parity checking          */
            commDcb.fOutxCtsFlow     = false;                    /* CTS handshaking on output       */
            commDcb.fOutxDsrFlow     = false;                    /* DSR handshaking on output       */
            commDcb.fDtrControl      = DTR_CONTROL_DISABLE;      /* DTR Flow control                */
            commDcb.fDsrSensitivity  = false;                    /* DSR Sensitivity              */
            commDcb.fTXContinueOnXoff= true;                     /* Continue TX when Xoff sent */
            commDcb.fOutX            = false;                    /* Enable output X-ON/X-OFF        */
            commDcb.fInX             = false;                    /* Enable input X-ON/X-OFF         */
            commDcb.fErrorChar       = false;                    /* Enable Err Replacement          */
            commDcb.fNull            = false;                    /* Enable Null stripping           */
            commDcb.fRtsControl      = RTS_CONTROL_DISABLE;      /* Rts Flow control                */
            commDcb.fAbortOnError    = false;                    /* Abort all reads and writes on Error */
            commDcb.fDummy2          = 0;   
            commDcb.wReserved        = 0;                        /* Reserved                        */
            commDcb.XonLim           = 0;
            commDcb.XoffLim          = 0;
            commDcb.ByteSize         = 8;
            commDcb.Parity           = 0;
#endif


        }
    }
    return ( itsHandle != INVALID_HANDLE_VALUE );
}

void UsbIss::close ( )
{
    if ( itsHandle != INVALID_HANDLE_VALUE )
    {
        CloseHandle( itsHandle );
        itsHandle = INVALID_HANDLE_VALUE;
    }
}

/* return number of available bytes in COM port if any */
unsigned int UsbIss::rxAvailable ( )
{
    if ( itsHandle != INVALID_HANDLE_VALUE)
    {
        COMSTAT commStat;
        DWORD error;
        ClearCommError( itsHandle, &error, &commStat);
        return unsigned int( commStat.cbInQue );
    }
    return 0;
}

/* Write sendLen Bytes to USB-ISS */
unsigned int UsbIss::tx ( unsigned int sendLen, const BYTE * send )
{
    if ( itsHandle != INVALID_HANDLE_VALUE)
    {
        DWORD written;
        /* written is set zero before doing any real transmit and error checking in 
           the function WriteFile */
        WriteFile( itsHandle, send, sendLen, &written, NULL);

        qDebug( ) << "UsbIss: tx(" << unsigned int( written ) << ")" << QByteArray( reinterpret_cast< const char *>( send ), written ).toHex();

        return unsigned int( written );
    }
    return 0;
}

/* clear any received bytes - good before sending a new command */
void UsbIss::rxClear(  )
{

    if ( itsHandle != INVALID_HANDLE_VALUE)
    {
        BYTE   buffer[10];
        DWORD  read;
        // Empty receive buffer to remove junk
        while( rxAvailable() > 0)
        {
            ReadFile( itsHandle, buffer, 1, &read, NULL);
            qDebug( ) << "UsbIss: rxClear " << unsigned int( buffer[0] ) ;
        }
    }
}

/* receive with timeout in msec*/
unsigned int UsbIss::rx ( unsigned int receiveLen, BYTE * receive, unsigned int timeoutInMs )
{
    if ( itsHandle != INVALID_HANDLE_VALUE && receiveLen > 0 )
    {
        unsigned int i = 1;
        //wait for receive data, but fall into timeout if no answer within given time
        do                                                            
        {
            if ( rxAvailable() >= receiveLen )
            {
                DWORD  read;
                /* read is set zero before doing any real receive and error checking in 
                   the function ReadFile */
                ReadFile( itsHandle, receive, receiveLen, &read, NULL );

                qDebug() << "UsbIss: rx(" << unsigned int( read ) << ")" << QByteArray( reinterpret_cast< const char *>(receive ), read ).toHex();

                return unsigned int( read );
            }
            if ( i < timeoutInMs )
            {
                Sleep( USB_ISS_TIMEOUT_STEPS_IN_MS );
                i += USB_ISS_TIMEOUT_STEPS_IN_MS;
            }
            else
            {
                return 0;
            }
        } while ( 1 );
    }
    return 0;
}

unsigned int UsbIss::txRx ( unsigned int sendLen, const BYTE * send, unsigned int receiveLen, BYTE * receive, unsigned int timeoutInMs )
{
    rxClear( ); /* clear any old stuff */
    if ( tx( sendLen, send ) == sendLen ) /* tx successful*/
    { 
        return rx( receiveLen, receive, timeoutInMs );
    }
    return 0;
}

const BYTE * UsbIss::version( )
{
    BYTE tx[ ] = { USB_ISS_CMD, USB_ISS_VERSION };

    if ( txRx( sizeof( tx ), tx, 3, itsRx ) )
    {
        itsRx[ 3 ] = '\0'; /* zero-terminator */
        return itsRx;
    }
    return 0;
}

bool UsbIss::setBaudRate( unsigned int baudRate )
{
    baudRate = ( baudRate < 300 ? 300 : ( baudRate > 1000000 ? 1000000 : baudRate ));
    unsigned int divisor = ( 48000000UL/ ( 16 + baudRate ));
    BYTE tx [ ] = { USB_ISS_CMD, USB_ISS_MODE, itsI2cMode | USB_ISS_SERIAL, static_cast<BYTE>(divisor >> 8 ), static_cast<BYTE>(divisor) };
    itsBaudRate = baudRate;
    if ( txRx( sizeof( tx ), tx, 2, itsRx ) )
    {
        if ( itsRx[ 0 ] == USB_ISS_ACK )
        {
            return true;
        }
        /* reason is in itsRx[ 1 ] */
    }
    return false;
}

bool UsbIss::resetI2C( )
{
    BYTE tx [ ] = { USB_ISS_CMD, USB_ISS_MODE, itsI2cMode, 0x50 /* SCL and SDA idle high */ };
    if ( txRx( sizeof( tx ), tx, 2, itsRx ) )
    {
        return ( itsRx[ 0 ] == USB_ISS_ACK );
    }
    return false;

}

bool UsbIss::setI2CMode( unsigned int frequencyInHz, bool standardMode )
{
    BYTE mode = USB_ISS_I2C_S_20KHZ;
    if ( frequencyInHz >= 1000000UL )
    {
        mode = USB_ISS_I2C_H_1000KHZ;
    }
    else if ( frequencyInHz >= 400000UL )
    {
        if ( standardMode )
            mode = USB_ISS_I2C_S_400KHZ;
        else
            mode = USB_ISS_I2C_H_400KHZ;
    }
    else if ( frequencyInHz >= 100000UL )
    {
        if ( standardMode )
            mode = USB_ISS_I2C_S_100KHZ;
        else
            mode = USB_ISS_I2C_H_100KHZ;
    }
    else if ( frequencyInHz >= 50000UL )
    {
        mode = USB_ISS_I2C_S_50KHZ;
    }
    itsI2cMode = mode;
    return resetI2C( );
}


/* table for spi:
   polarity | phase                                                        idle-state-is-low | tx-active-to-idle 
   ---------+------                                                     ---------------------+------------------
      0     | 0     = capture on rising edge, provide on falling edge =                  1   |     1             = tx- active-to-idle
      0     | 1     = capture on falling edge, provide on rising edge =                  1   |     0             = tx- idle-to-active
      1     | 0     = capture on falling edge, provide on rising edge =                  0   |     1             = tx- active-to-idle
      1     | 1     = capture on rising edge, provide on falling edge =                  0   |     0             = tx- idle-to-active
      */


bool UsbIss::setSpiMode(  int polarity, int phase, unsigned int frequencyInHz )
{
    BYTE mode;
    bool idleStateIsLow = ( polarity == 0 ); 
    bool txOnActiveToIdle = ( phase == 0 ); 
    if ( idleStateIsLow ) /* either 0x90 or 0x92 */
    {
        if ( txOnActiveToIdle )
        { /* 0x90 */
            mode = 0x90;
        }
        else 
        { /* 0x92 */
            mode = 0x92;
        }
    }
    else /* idle is high -> either 0x91 or 0x93 */
    {
        if ( txOnActiveToIdle )
        { /* 0x91 */
            mode = 0x91;
        }
        else 
        { /* 0x93 */
            mode = 0x93;
        }
    }

    unsigned int divisor = ( 6000000UL / frequencyInHz );
    if ( divisor )
    {
        divisor--;
    }
    if ( divisor > 255 )
    {
        divisor = 255;
    }
    BYTE tx[ ] = { USB_ISS_CMD, USB_ISS_MODE, mode, divisor };
    
    if ( txRx( sizeof( tx ), tx, 2, itsRx ) )
    {
        if ( itsRx[ 0 ] == USB_ISS_ACK )
        {
            return true;
        }
        /* reason is in itsRx[ 1 ] */
    }
    return false;
}

bool UsbIss::i2cTxRx ( BYTE deviceAddress, unsigned int sendLen, const BYTE * send, unsigned int receiveLen, BYTE * receive, unsigned int timeoutInMs )
{
    if (  sendLen < 1 
       || sendLen > USB_ISS_MAX_I2C_TRANSFER_SIZE 
       || receiveLen > USB_ISS_MAX_I2C_TRANSFER_SIZE
       )
    {
        return false;
    }

    BYTE tx[ USB_ISS_MAX_I2C_SIZE ];
    unsigned int index = 0;
    
    tx[ index++ ] = USB_ISS_I2C_DIRECT;
    tx[ index++ ] = USB_ISS_I2C_START;
    tx[ index++ ] = USB_ISS_I2C_WRITE + sendLen;
    tx[ index++ ] = deviceAddress << 1; /* shift 7-bit address so that LSB becomes R/W bit (0=Write) */
    for ( unsigned int i = 0; i < sendLen; ++i )
    {
        tx[ index++ ] = send[ i ];
    }
    if ( receiveLen ) /* data to be received, so send repeated-start, etc. */
    {
        tx[ index++ ] = USB_ISS_I2C_RESTART;
        tx[ index++ ] = USB_ISS_I2C_WRITE; /* for device address */
        tx[ index++ ] = ( deviceAddress << 1 ) | 1;  /* shift 7-bit address so that LSB becomes R/W bit (1=Read) */

        if ( receiveLen > 1 )
        {
            /* USB_ISS_I2C_READ reads in 1 byte, if we add 1 we read in 2 bytes,
            therefore we read in receiveLen - 1 bytes, and as we want the
            last byte read to be nak-ed, we subtract another byte from the
            receive len */
            tx[ index++ ] = USB_ISS_I2C_READ + receiveLen - 2;
        }
        tx[ index++ ] = USB_ISS_I2C_NACK; /* send nak for last read */
        tx[ index++ ] = USB_ISS_I2C_READ; /* read in 1 byte */
        tx[ index++ ] = USB_ISS_I2C_STOP;
    }
    else /* tx only */
    {
        tx[ index++ ] = USB_ISS_I2C_STOP;
    }
 
    if ( txRx( index, tx, 2, itsRx, timeoutInMs ) )
    {
        if ( itsRx[ 0 ] == USB_ISS_ACK )
        {
            if ( receiveLen )
            { /* need to receive data, so read in: itsRx[ 1 ] contains the number of bytes to read */
                if ( itsRx[ 1 ] >= receiveLen )
                {
                    unsigned int rxed = rx( receiveLen, receive, timeoutInMs );
                    return ( rxed == receiveLen );
                }
                else
                {
                    return false; /* too little bytes read */
                }
            }
            else /* no receive, so command is done and was successful */
            {
                return true;
            }
        }
        /* reason is in itsRx[ 1 ]:
           0x01 = no ack from device
           0x02 = buffer overflow in USB-ISS 
           0x03 = buffer underflow - command expected more data 
           0x04 = unknown command - probably your write count is wrong
            */
    }
    resetI2C( ); /* make sure master does not hang with scl low */
    return false;
}

bool UsbIss::spiTxRx( unsigned int sendLen, const BYTE * send, BYTE * receive, unsigned int timeoutInMs )
{
    if (  sendLen < 1 || sendLen > USB_ISS_MAX_SPI_TRANSFER_SIZE )
    {
        return false;
    }

    BYTE tx[ USB_ISS_MAX_SPI_SIZE ];
    
    tx[ 0 ] = USB_ISS_SPI_DIRECT;
    for ( unsigned int i = 0; i < sendLen; ++i )
    {
        tx[ 1 + i ] = send[ i ];
    }
 
    if ( txRx( 1 + sendLen, tx, 1 , itsRx, timeoutInMs ) )
    {
        if ( itsRx[ 0 ] == USB_ISS_ACK )
        {   /* now read in the rest of the data */
            unsigned int rxed = rx( sendLen, receive, timeoutInMs );
            return ( rxed == sendLen );
        }
        /* reason is in itsRx[ 1 ]:
           0x01 = no ack from device
           0x02 = buffer overflow in USB-ISS 
           0x03 = buffer underflow - command expected more data 
           0x04 = unknown command - probably your write count is wrong
            */
    }
    return false;
}