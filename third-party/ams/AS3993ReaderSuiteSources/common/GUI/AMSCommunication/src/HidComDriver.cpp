/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file HidComDriver.cpp
 *
 *  \author M. Arpa
 *
 *  \brief  Communication classes for hid streaming communication 
 */

#include "HidComDriver.h"
#include <windows.h>
#include "HIDDevice.h"
#include "ams_stream.h"


HidComDriver::HidComDriver 
	( unsigned short pid
	, unsigned short vid
	)
	: AmsComDriver( ) 
	, itsHid( NULL )
	, itsVid( vid )
    , itsNumberOfPidsInList( 1 )
	, itsPidList( new unsigned short [ 1 ] )
	, itsTxHookReal( new unsigned char [ USB_HID_REPORT_SIZE + 1 ] ) /* add 1 for a windows specifica: if report id is 0 -> we need 65 bytes */
    , itsTxHook( itsTxHookReal + 1 )
	, itsToTx( 0 )
	, itsTxBuffer( itsTxHook + USB_HID_HEADER_SIZE )
	, itsRxHookReal( new unsigned char [ USB_HID_REPORT_SIZE + 1 ] )
    , itsRxHook( itsRxHookReal + 1 )
	, itsRxed( 0 )
	, itsRxBuffer( itsRxHook + USB_HID_HEADER_SIZE )
	, itsTid( 0 )
    , itsTxTimeout( 1000 )
    , itsRxTimeout( 1000 )
{
    itsPidList[ 0 ] = pid;

    // do a buffer memset using patern 0xCB (for clean buffer) 
    itsTxHookReal[ 0 ] = USB_HID_REPORT_ID; /* must be 0 */ 
    itsRxHookReal[ 0 ] = USB_HID_REPORT_ID;
    memset(itsTxHook, 0xCB, USB_HID_REPORT_SIZE);
}

HidComDriver::HidComDriver 
	( const unsigned short * pidList
    , unsigned short numberOfPidsInList
	, unsigned short vid
	)
	: AmsComDriver( ) 
	, itsHid( NULL )
	, itsVid( vid )
    , itsNumberOfPidsInList( numberOfPidsInList )
	, itsPidList( new unsigned short [ numberOfPidsInList ] )
	, itsTxHookReal( new unsigned char [ USB_HID_REPORT_SIZE + 1 ] ) /* add 1 for a windows specifica: if report id is 0 -> we need 65 bytes */
    , itsTxHook( itsTxHookReal + 1 )
	, itsToTx( 0 )
	, itsTxBuffer( itsTxHook + USB_HID_HEADER_SIZE )
	, itsRxHookReal( new unsigned char [ USB_HID_REPORT_SIZE + 1 ] )
    , itsRxHook( itsRxHookReal + 1 )
	, itsRxed( 0 )
	, itsRxBuffer( itsRxHook + USB_HID_HEADER_SIZE )
	, itsTid( 0 )
    , itsTxTimeout( 1000 )
    , itsRxTimeout( 1000 )
{
    unsigned short i;
    for ( i = 0; i < numberOfPidsInList; ++i )
    {
        itsPidList[ i ] = pidList[ i ];
    }

    // do a buffer memset using patern 0xCB (for clean buffer) 
    itsTxHookReal[ 0 ] = USB_HID_REPORT_ID; /* must be 0 */ 
    itsRxHookReal[ 0 ] = USB_HID_REPORT_ID;
    memset(itsTxHook, 0xCB, USB_HID_REPORT_SIZE);
}

HidComDriver::~HidComDriver ( )
{
    if(isOpened()) close();
	delete [] itsTxHookReal;
	delete [] itsRxHookReal;
    delete [] itsPidList;
}

void HidComDriver::clearError ( )
{
    itsRxed = 0;
    itsToTx = 0;
}

void HidComDriver::clearFirmwareError ( )
{
}

bool HidComDriver::open ( )
{
	if ( itsHid )
	{ /* we are already connected */
		itsIsChannelOpen = true;
		return itsIsChannelOpen;
	}
    /* clear any internal states in the base class before trying to connect */
    internalReset( );
    unsigned short i = 0;
    while ( i < itsNumberOfPidsInList && ! open( itsPidList[ i ] ) ) 
    {
        ++i;
    }
    return ( i < itsNumberOfPidsInList );
}

bool HidComDriver::open ( unsigned short pid )
{
    int hidDeviceCount = GetConnectedDeviceNum( itsVid, pid);   // number of HID devices connected to the computer
	if ( hidDeviceCount > 0 )   // at least one board found, and we are not already connected to this board
	{
		itsHid = new CHIDDevice();

		if ( itsHid->Open( 0, itsVid, pid ) == HID_DEVICE_SUCCESS )   // try to open the device
		{
			if ( itsHid->IsOpened( ) )
			{
				// hid driver packets are too small
				if (  itsHid->GetInputReportBufferLength() >= USB_HID_REPORT_SIZE 
				   && itsHid->GetOutputReportBufferLength() >= USB_HID_REPORT_SIZE
				   )
				{
					unsigned int readReportTimeout, writeReportTimeout;
					itsIsChannelOpen = true;
					lastError( );
                    lastFirmwareError ( );
					itsHid->GetTimeouts(&readReportTimeout, &writeReportTimeout);
					itsHid->SetTimeouts(itsRxTimeout, itsTxTimeout);
					return itsIsChannelOpen;
				}
			}
		}

		delete itsHid;
		itsHid = NULL;
	}
	itsIsChannelOpen = false;
	return itsIsChannelOpen;
}

bool HidComDriver::close ( )
{
	if ( itsHid )
	{
		if ( itsHid->IsOpened( ) )
		{
			itsHid->Close(); 
		}
		itsIsChannelOpen = false;
		delete itsHid; 
		itsHid = NULL;
		lastError( );
        lastFirmwareError( );
	}
	return true;
}

int HidComDriver::flush ( )
{
	if ( itsIsChannelOpen )
	{   /* add the header now */
		if ( itsLastError == NoError && itsToTx > 0 )
		{
			/* do transmit */
			tx( );
		}
	}
	else
	{
		itsLastError = ChannelOpenError; /* channel not open */
	}
	return itsLastError;
}

void HidComDriver::tx ( )
{
	if ( itsIsChannelOpen )
	{   /* add the header now */
		itsTid++;
		USB_HID_TID( itsTxHook ) = itsTid;           /* transaction id */
		USB_HID_PAYLOAD_SIZE( itsTxHook ) = itsToTx; /* tell the peer how many payload data there is really in the report */
        USB_HID_STATUS( itsTxHook ) = 0; /* reserved */
		itsToTx = 0;
		if ( HID_DEVICE_SUCCESS == itsHid->SetReport_Interrupt( itsTxHookReal, itsHid->GetOutputReportBufferLength() ) )
		{
			itsLastError = NoError; /* no error */
		}
		else
		{
			itsLastError = TxError; /* tx failed */
		}
        memset( itsTxHook, 0xCB, USB_HID_REPORT_SIZE );
	}
	else
	{
		itsLastError = ChannelOpenError; /* channel not open */
	}
}



int HidComDriver::txBuffer ( unsigned char * buffer, int size )
{
	if ( itsIsChannelOpen )
	{
		int toCopy;
		while (  itsLastError == NoError 
			  && ( toCopy = ( size > USB_HID_MAX_PAYLOAD_SIZE - itsToTx ? USB_HID_MAX_PAYLOAD_SIZE - itsToTx : size ) ) > 0 
			  )
		{
			/* copy part of the data */
			memcpy( itsTxBuffer + itsToTx, buffer, toCopy );
			buffer += toCopy;
			size -= toCopy;
			itsToTx += toCopy;
			if ( itsToTx == USB_HID_MAX_PAYLOAD_SIZE )
			{ /* send packet immediately */
				tx( );
			}
		}
	}
	else
	{
		itsLastError = ChannelOpenError; /* channel not open */
	}
	return itsLastError;
}

void HidComDriver::rx ( )
{
	DWORD bytesReturned;
	if ( itsIsChannelOpen )
	{   /* add the header now */
		if ( HID_DEVICE_SUCCESS == itsHid->GetReport_Interrupt( itsRxHookReal, itsHid->GetInputReportBufferLength(), 1, &bytesReturned ) ) 
	 	{
			/* remove the header before returning the buffer */
			if ( bytesReturned > 0 )
			{
				/* ( USB_HID_TID( itsRxHook ) ~~ Tid )	   */
				itsRxed = USB_HID_PAYLOAD_SIZE( itsRxHook );
                unsigned char fStatus = USB_HID_STATUS( itsRxHook );
                if ( fStatus != 0 )
                {
                    itsLastFirmwareError = fStatus;
                }
				itsLastError = NoError; /* no error */
				return;
			}
		}
		itsLastError = RxError; /* rx failed */
	}
	else
	{
		itsLastError = ChannelOpenError; /* channel not open */
	}
}

int HidComDriver::rxBuffer ( unsigned char * buffer, int size )
{
	int toCopy;
	if ( itsLastError == NoError && itsRxed == 0 && size != 0 )
	{
		rx( );
	}

	while (  itsLastError == NoError 
		  && itsRxed > 0 
		  && ( toCopy = ( size > itsRxed ? itsRxed : size ) ) > 0 
		  )
	{
		memcpy( buffer, itsRxBuffer, toCopy );
		buffer += toCopy;
		size -= toCopy;
		itsRxed -= toCopy;
		if ( itsRxed > 0 )
		{ /* move the remaining part of the buffer to the front -> easier code */
			memmove( itsRxBuffer, itsRxBuffer + toCopy, itsRxed );
		}
		else if ( size > 0 ) /* need to load another buffer */
		{
			rx( );
		} 
	}
	return itsLastError;
}

/* Tid, payload, protocol-id, host-tx/device-rx-length, host-rx/device-tx-length, firmware-number */
const unsigned char oldHidFirmwareNumberRequest[ ] = 
{ 0x07, 0x04, 0x03, 0x01, 0x04, 0x04 };
/* old firmware returns a tid like this: 0x7z where z is any nibble */
/* new firmware returns a tid like this: 0xde */

/* Tid, payload, protocol-id, host-tx/device-rx-length, host-rx/device-tx-length, enable-bootloader */
const unsigned char oldHidEnableBootloaderRequest[ ] =
{ 0x08, 0x04, 0x03, 0x01, 0x00, 0xEB };
/* old firmware returns a tid like this: 0x8z where z is any nibble */
/* new firmware returns a tid like this: 0xde */


bool HidComDriver::isProtocolVersionCompatible ( bool enableBootloaderIfNotCompatible )
{
    bool toClose = ! isOpened( );
    /* 1. make sure we have communication channel */
    if ( ! isOpened( ) )
    {
        if ( ! open( ) )
        {
            itsLastError = ChannelOpenError;
            return false; /* failed to open a channel */
        }
    }

    /* 2. fill in a request in the old hid/protocol format */
    int size = sizeof( oldHidFirmwareNumberRequest );
    memcpy( itsTxHook, oldHidFirmwareNumberRequest, size );

    /* 3. send the request in the old hid format */
	if ( HID_DEVICE_SUCCESS != itsHid->SetReport_Interrupt( itsTxHookReal, itsHid->GetOutputReportBufferLength() ) )
	{
		itsLastError = TxError;
        if ( toClose )
        {
            close( );
        }
        return false;
    }

    /* 3. read back the result of the request */
    DWORD bytesReturned;
	if (  HID_DEVICE_SUCCESS != itsHid->GetReport_Interrupt( itsRxHookReal, itsHid->GetInputReportBufferLength(), 1, &bytesReturned ) 
       || bytesReturned == 0
       ) 
	{
        itsLastError = RxError;
        if ( toClose )
        {
            close( );
        }
        return false;
    }

	/* 4. interpret the received result: if the received TID has a special pattern, than it is the new firmware */
    if ( itsRxHook[ 0 ] == AMS_STREAM_COMPATIBILITY_TID )
    {
        if ( toClose )
        {
            close( );
        }
        return true;
    }
    
    if ( enableBootloaderIfNotCompatible )
    {     /* 5. if enable bootloader was requested, fill in the request in the old format */
        size = sizeof( oldHidEnableBootloaderRequest );
        memcpy( itsTxHook, oldHidEnableBootloaderRequest, size );

        /* 6. send the request in the old hid format */
	    if ( HID_DEVICE_SUCCESS != itsHid->SetReport_Interrupt( itsTxHookReal, itsHid->GetOutputReportBufferLength() ) )
	    {
		    itsLastError = TxError; 
        }
    }

    if ( toClose )
    {
        close( );
    }
    return false;
}

void HidComDriver::setTimeouts( unsigned int rxTimeout, unsigned int txTimeout )
{
    itsRxTimeout = rxTimeout;
    itsTxTimeout = txTimeout;
}
