/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  AmsUsbIssWrapper class header file
 *
 *  AmsUsbIssWrapper is a derived class of AMSCommunication, so that it fullfills the interface
 * needed by the current version of the register map.
 */

#ifndef AMS_USB_ISS_WRAPPER_H
#define AMS_USB_ISS_WRAPPER_H

#include "AMSCommunication.hxx"
#include "UsbIss.h"

typedef struct AmsUsbIssWrapperProperties_
{
	unsigned char i2cSlaveAddress;
    int comPort;
} AmsUsbIssWrapperProperties;

class AmsUsbIssWrapper : public AMSCommunication
{
public:
	AmsUsbIssWrapper( int comPort = -1 );
	~AmsUsbIssWrapper( );

    /* direct access to member functions */
    UsbIss & iss( ) { return itsIss; };

	void setDevAddr(unsigned char i2cSlaveAddress) { devAddr = i2cSlaveAddress; }

	virtual void setConnectionProperties(void* properties) 
	{
		AmsUsbIssWrapperProperties * props = reinterpret_cast<AmsUsbIssWrapperProperties *>( properties );
		setDevAddr( props->i2cSlaveAddress );
        itsIss.setComPort( props->comPort );
	};

protected:

	/*!
	 *****************************************************************************
	 *  \brief  Read from a register
	 *
	 *  hwReadRegister is called from #readRegister and is the actual call to the
	 *  underlying hardware layer, i.e. this function needs to be implemented
	 *  by the specific implementation (e.g. USBBoxCommunication class)
	 *
	 *  \param[in]  reg : Address of the register to read from
	 *  \param[out] val : Pointer to a variable where the read value should be stored
	 *
	 *  \return ConnectionError : Not connected
	 *  \return ReadError : Error during read
	 *  \return NoError : No error
	 *****************************************************************************
	 */
	virtual Error hwReadRegister(unsigned char reg, unsigned char *val);
	/*!
	 *****************************************************************************
	 *  \brief  Write to a register
	 *
	 *  hwWriteRegister is called from #writeRegister and is the actual call to the
	 *  underlying hardware layer, i.e. this function needs to be implemented
	 *  by the specific implementation (e.g. USBBoxCommunication class)
	 *
	 *  \param[in] reg : Address of the register to write
	 *  \param[in] val : Value to write
	 *
	 *  \return ConnectionError : Not connected
	 *  \return WriteError : Error during write
	 *  \return NoError : No error
	 *****************************************************************************
	 */
	virtual Error hwWriteRegister(unsigned char reg, unsigned char val);
	/*!
	 *****************************************************************************
	 *  \brief  Sends a command
	 *
	 *  hwSendCommand sends the command #command and returns a possible answer in #answer
	 *
	 *  \param[in] command : command to send
	 *  \param[out] answer : possible answer returned
	 *
	 *  \return ConnectionError : Not connected
	 *  \return ReadError : Error during read
	 *  \return WriteError : Error during write
	 *  \return NoError : No error
	 *****************************************************************************
	 */
	virtual Error hwSendCommand(QString command, QString * answer);

	/*!
	 *****************************************************************************
	 *  \brief  Establish a connection to the board
	 *
	 *  hwConnect is called from #connect and is the actual call to the
	 *  underlying hardware layer, i.e. this function needs to be implemented
	 *  by the specific implementation (e.g. USBBoxCommunication class)
	 *
	 *  \return ConnectionError : Connection can't be established
	 *  \return NoError : No error
	 *****************************************************************************
	 */
	virtual Error hwConnect();

	/*!
	 *****************************************************************************
	 *  \brief  Close an open connection
	 *
	 *  hwDisconnect is called from #disconnect and is the actual call to the
	 *  underlying hardware layer, i.e. this function needs to be implemented
	 *  by the specific implementation (e.g. USBBoxCommunication class)
	 *
	 *****************************************************************************
	 */
	virtual void hwDisconnect();

protected:
	UsbIss itsIss;
};

#endif
