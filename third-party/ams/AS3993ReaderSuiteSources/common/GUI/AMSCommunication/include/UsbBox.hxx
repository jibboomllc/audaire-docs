/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  Base class - does nothing really - is used as a dummy instance as long 
 *          as we do not know wheter to use UsbBoxUsingUsbConDll or 
 *          UsbBoxUsingCyApi class.
 *
 */

#ifndef USB_BOX_H
#define USB_BOX_H

#include "USBBoxCommunication.hxx"
#include <QString>
#include <QByteArray>
#include "usb.h"

class UsbBox 
{
	static UsbBox * itsInstance;
	static UsbBox itsDummyInstance; 
public:

	static UsbBox & instance( );

	virtual UsbError blkWriteFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes) { return ERR_NOT_CONNECTED; };
	virtual UsbError blkReadFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes) { return ERR_NOT_CONNECTED; };
	virtual UsbError getPortDirectionFn(Port io_port,unsigned char* direction) { return ERR_NOT_CONNECTED; };
	virtual UsbError readADCFn(unsigned char channel,unsigned* value) { return ERR_NOT_CONNECTED; };
	virtual UsbError readPortFn(unsigned char *data, int ioPort, unsigned char mask) { return ERR_NOT_CONNECTED; };
	virtual UsbError sendPacketFn(char *in,int in_length, const char *out, int out_length) { return ERR_NOT_CONNECTED; };
	virtual UsbError setPortAsInputFn(int ioPort, unsigned char mask) { return ERR_NOT_CONNECTED; };
	virtual UsbError setPortAsOutputFn(int ioPort, unsigned char mask) { return ERR_NOT_CONNECTED; };
	virtual UsbError setInterfaceFn(Interface iface) { return ERR_NOT_CONNECTED; };
	virtual bool     usbIsConnectedFn() { return false; };
	virtual UsbError readFirmwareIdFn(unsigned char * str) { return ERR_NOT_CONNECTED; };
	virtual UsbError usbDebugMsgFn(unsigned char* str) { return ERR_NOT_CONNECTED; };
	virtual UsbError usbConnectFn(void) { return ERR_NOT_CONNECTED; };
	virtual UsbError usbDisconnectFn(void) { return ERR_NOT_CONNECTED; };
	virtual UsbError readByteFn(unsigned char registerAddress, unsigned char* registerValue) { return ERR_NOT_CONNECTED; };
	virtual UsbError setI2CDevAddrFn(unsigned char registerAddress) { return ERR_NOT_CONNECTED; };
	virtual UsbError writePortFn(unsigned char data, int ioPort, unsigned char mask) { return ERR_NOT_CONNECTED; }; 
	virtual UsbError writeByteFn(unsigned char registerAddress, unsigned char registerValue) { return ERR_NOT_CONNECTED; };
	
	void singleWirePropertiesFn ( unsigned char delay, unsigned char pin ) { itsSingleWireDelay = delay; itsSingleWirePin = pin; };
	int getInSize ( int inSize ) const { return ( itsInterface == OPTO || itsInterface == MULTI_LEVEL ? inSize + 1 : inSize ); };
	int maxSize ( ) const { return ( itsInterface == OPTO || itsInterface == MULTI_LEVEL ? 254 : 255 ); }; /* must store the size in a byte */
	UsbError switchInterface( Interface iface );
	bool canUseBlkCommands ( ) const { return ( itsInterface != OPTO ); };
	void multiLevelI2cPropertiesFn ( unsigned char halfSpikeSupTime, unsigned char halfEnTime ) { itsMultiLevelI2cHalfSpikeSupTime = halfSpikeSupTime; itsMultiLevelI2cHalfEnTime = halfEnTime; };

	/* firmware load */
	virtual bool openDriver ( )  { return false; };
	virtual bool closeDriver ( ) { return true; };
	virtual bool control8051 ( bool reset8051 ) { return false; };
	virtual bool download ( bool toEeprom, char * buffer, int startAddress, int size )  { return false; };
	virtual bool upload ( char * buffer, short startAddress, short size, DWORD & numberBytes )  { return false; };

protected:
	/* disallow to directly instanciate */
	UsbBox( ) : itsSingleWireDelay( 0x1 ) /* delay means approximately in microseconds */
	          , itsSingleWirePin( 0xFF ) /* 0xFF is the default port C Bit 1 = Usb Box 1.2 Small Connector pin 2 */
			  , itsInterface( I2C )
			  , itsMultiLevelI2cHalfSpikeSupTime( 5 ) /* wait time for spike sup is 2* 5us */
			  , itsMultiLevelI2cHalfEnTime( 5 )       /* wait time for En is 2* 5us */
				{ }; 
	~UsbBox( ){ };

	unsigned char itsSingleWireDelay;
	unsigned char itsSingleWirePin;
	Interface itsInterface;
	unsigned char itsMultiLevelI2cHalfSpikeSupTime;
	unsigned char itsMultiLevelI2cHalfEnTime;
};

#endif 
