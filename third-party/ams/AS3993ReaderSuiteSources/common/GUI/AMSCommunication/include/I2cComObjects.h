/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file I2cComObjects.h
 *
 *  \author M. Arpa
 *
 *  \brief Classes to read and write using i2c as communication means.
 */

#ifndef I2C_COM_OBJECTS_H
#define I2C_COM_OBJECTS_H

#include "AmsCom.h"
#include "AmsComObject.h"

/* forward declaration ------------------------------------ */

class QxmlStreamReader;
class QXmlStreamWriter;


/* class declaration -------------------------------------- */

class I2cComXmlReader
{
public:
    /* interprets the opening tag information and others and generates a new
       object of the corresponding type with new-operator */
    static AmsComObject * clone( QXmlStreamReader * xml, unsigned char protocol );

    I2cComXmlReader( ) { };
    ~I2cComXmlReader( ) { };
    I2cComXmlReader( const I2cComXmlReader & other ) { };
};


/* class to read and write registers using i2c 
 */
class I2cRegisterObject : public AmsComObject
{
public:
	/* c'tor */
	I2cRegisterObject 
	  ( unsigned char i2cSlaveAddress
	  , unsigned int registerAddress
	  , unsigned int data = 0
      , unsigned int tracePattern = AMS_COM_TRACE_ALLWAYS
	  , int addressSize = 1
	  , int dataSize = 1 
	  );

	/* copy c'tor */
	I2cRegisterObject ( const I2cRegisterObject & other );
	
	/* d'tor */
	~I2cRegisterObject ( ) { };

	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * xml );

	unsigned int get( ) const { return itsData; };

    const unsigned char i2cSlaveAddress( ) { return itsI2CSlaveAddress; };

    unsigned int address( ) { return itsAddress; };

	void set( unsigned int value ) { itsData = value; };

	void set( unsigned int registerAddress, unsigned int value ) 
	{ 
	  itsAddress = registerAddress; 
	  set( value ); 
	};

private:
   	/* forbid default c'tor */
	I2cRegisterObject( );


protected:
	/* the address of the i2c-slave we use for communication */
	unsigned char itsI2CSlaveAddress;

	int itsAddressSize; /* whether the address is 8bit, 16bit or 32bit in bytes (so 1,2 or 4) */

	int itsDataSize; /* whether the data is 8bit, 16bit or 32bit in bytes (so 1,2 or 4) */

	unsigned int itsAddress;

	unsigned int itsData;
};

/* Class to read a block of registers from a device using the 
 * AmsComStream class. 
 */
class I2cBlockObject : public AmsComObject
{
public:
	/* default c'tor */
	I2cBlockObject 
	  ( unsigned char i2cSlaveAddress
	  , unsigned int startRegisterAddress
	  , int blockSize = 1
	  , unsigned char * data = 0
	  , int addressSize = 1
	  , int maxBlockSize = AMS_STREAM_MAX_DATA_SIZE 
	  );

	/* copy c'tor */
	I2cBlockObject ( const I2cBlockObject & other );
	
	/* d'tor */
	~I2cBlockObject ( );

	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * xml );

	unsigned char * get( ) { return itsData; };

    int size( ) { return itsBlockSize; };

    const unsigned char i2cSlaveAddress( ) { return itsI2CSlaveAddress; };

    unsigned int address( ) { return itsAddress; };

	void set( const unsigned char * data );
 
	void set( int size ) { itsBlockSize = size; };

	void set( const unsigned char * data, int size );

	void set( int startRegisterAddress, const unsigned char * data, int size );

protected:
	unsigned char itsI2CSlaveAddress;
	int itsAddressSize; /* whether the address is 8bit, 16bit or 32bit in bytes (so 1,2 or 4) */
	unsigned int itsAddress;
	int itsBlockSize;
	int itsMaxBlockSize;
	unsigned char * const itsData; /* deep copy */
};

/* Class to write with ack/nak a block of registers from a device using the 
 * AmsComStream class. 
 */
class I2cBlockWriteObject : public AmsComObject
{
public:
	/* default c'tor */
	I2cBlockWriteObject 
	  ( unsigned char i2cSlaveAddress
	  , unsigned int startRegisterAddress
	  , int blockSize = 1
	  , unsigned char * data = 0
	  , int addressSize = 1
	  , int maxBlockSize = AMS_STREAM_MAX_DATA_SIZE 
	  );

	/* copy c'tor */
	I2cBlockWriteObject ( const I2cBlockWriteObject & other );
	
	/* d'tor */
	~I2cBlockWriteObject ( );

	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * xml );

	unsigned char * get( ) { return itsData; };

    int size( ) { return itsBlockSize; };

    const unsigned char i2cSlaveAddress( ) { return itsI2CSlaveAddress; };

    unsigned int address( ) { return itsAddress; };

	void set( const unsigned char * data );
 
	void set( int size ) { itsBlockSize = size; };

	void set( const unsigned char * data, int size );

	void set( int startRegisterAddress, const unsigned char * data, int size );

protected:
	unsigned char itsI2CSlaveAddress;
	int itsAddressSize; /* whether the address is 8bit, 16bit or 32bit in bytes (so 1,2 or 4) */
	unsigned int itsAddress;
	int itsBlockSize;
	int itsMaxBlockSize;
	unsigned char * const itsData; /* deep copy */
};

class I2cBlockReadObject : public AmsComObject
{
public:
	/* default c'tor */
	I2cBlockReadObject 
	  ( unsigned char i2cSlaveAddress
	  , int txBlockSize = 1
	  , int rxBlockSize = 1
	  , unsigned char * txData = 0
	  , int maxBlockSize = AMS_STREAM_MAX_DATA_SIZE 
	  );

	
	/* d'tor */
	~I2cBlockReadObject ( );

	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * xml );

	unsigned char * rxGet( ) { return itsRxData; };

    int rxSize( ) { return itsRxBlockSize; };

private:
	/* copy c'tor */
	I2cBlockReadObject ( const I2cBlockReadObject & other );

protected:
	unsigned char itsI2CSlaveAddress;
	int itsTxBlockSize;
	int itsRxBlockSize;
	int itsMaxBlockSize;
	unsigned char * const itsTxData; /* deep copy */
	unsigned char * const itsRxData; /* deep copy */
};


/*  Configuration Object used for i2c configuration. 
 */
class I2cConfigObject : public AmsComObject
{
public:
    static const char * clockModeToFrequency( unsigned char clockMode );

	static const unsigned char theClockMode100KHz = 1;
	static const unsigned char theClockMode400KHz = 2;
	static const unsigned char theClockMode1MHz = 3;
    static const unsigned char theClockMode3_4MHz = 4;
	static const unsigned char theClockModeCustomSpeed = 0xFF;

public:
	/* default c'tor */
	I2cConfigObject ( unsigned char module, unsigned char clockMode = theClockMode100KHz, unsigned int customClockSpeed = 0 );

	/* copy c'tor */
	I2cConfigObject ( const I2cConfigObject & other );
	
	/* d'tor */
	~I2cConfigObject ( ) { };

    const unsigned char module( ) { return itsModule; };

    const unsigned char clockMode( ) { return itsClockMode; };

	const unsigned int customClockSpeed( ) { return itsCustomClockSpeed; };

	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * xml );

    void set( unsigned char module, unsigned int frequencyInHz )
    {
        itsModule = ( module == 1 || module == 2 ? module : itsModule );
        itsClockMode = theClockModeCustomSpeed;
        itsCustomClockSpeed = frequencyInHz;
    };

protected:
	unsigned char itsModule; 
	unsigned char itsClockMode;
	unsigned int  itsCustomClockSpeed;
};

#endif /* I2C_COM_OBJECTS_H */
