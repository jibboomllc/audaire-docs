/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle
 *
 *  \brief  Communication class for BU comms USB Box
 *
 *  This controls the communication to the USB box used in the
 *  COMMS BU. The USB Box is based on a Cypress AN2131 (8051 compatible)
 *  controller. Actual USB access is handled via usbcon.dll.
 */

#ifndef USBBOXCOMMUNICATION_H
#define USBBOXCOMMUNICATION_H

#include <windows.h>
#include <stdio.h>
#include "AMSCommunication.hxx"
#include "usb.h"
#include "UsbBoxDimming.h"

#define BLOCK_WRITE_MAX_BYTES	0x3A	/*<! maximal number of bytes transferred with one block write command */

typedef enum {
  ERR_NO_ERR, /**< No error occurred. */
  ERR_CMD_FAILED, /**< The uC encountered an error. */
  ERR_CANNOT_CONNECT, /**< The demoboard is probably not plugged in. */
  ERR_NOT_CONNECTED, /**< Transfer function was called on a closed connection */
  ERR_TRANSMIT, /**< An error occurred during the transmission of a command. */
  ERR_RECEIVE, /**< An error occurred during reception. */
  ERR_ILLEGAL_ARGUMENTS, /**< The arguments are out of bounds. */
  ERR_GENERIC /**< An unspecified error occurred. */
  
} UsbError;

struct usbBoxConfigStructure
{
	unsigned char deviceI2CAddress;
};

/** Interface definition **/
typedef enum { I2C, SPI, SWI , PPTRIM , I2C_HardClock , Standard_SPI , SSPI, OPTO, MULTI_LEVEL } Interface;

typedef UsbError (*AMSusbConnect)(void);
typedef UsbError (*AMSusbDisconnect)(void);
typedef UsbError (*AMSusbReadByte) (unsigned char registerAddress, unsigned char* registerValue);
typedef UsbError (*AMSusbWriteByte) (unsigned char registerAddress, unsigned char registerValue);
typedef UsbError (*AMSusbSetI2CDevAddr) (unsigned char registerAddress);
typedef UsbError (*AMSsetPortAsInput) (int ioPort, unsigned char mask);
typedef UsbError (*AMSsetPortAsOutput) (int ioPort, unsigned char mask);
typedef UsbError (*AMSwritePort) (unsigned char data, int ioPort, unsigned char mask); 
typedef UsbError (*AMSreadPort) (unsigned char *data, int ioPort, unsigned char mask);
typedef bool     (*AMSusbIsConnected) ();
typedef UsbError (*AMSusbFirmwareID) (unsigned char * str);
typedef UsbError (*AMSusbDebugMsg) (unsigned char* str);
typedef UsbError (*AMSsetInterface) (Interface iface);
typedef UsbError (*AMSwriteByte) (unsigned char register_address,unsigned char value);
typedef UsbError (*AMSblkWrite) (unsigned char start_addr,unsigned char* values,unsigned char num_bytes);
typedef UsbError (*AMSblkRead) (unsigned char start_addr,unsigned char* values,unsigned char num_bytes);
typedef UsbError (*AMSreadADC) (unsigned char channel,unsigned* value);
typedef UsbError (*AMSgetPortDirection) (Port io_port,unsigned char* direction);
typedef UsbError (*AMSsendPacket) (char *in,int in_length, const char *out, int out_length);
typedef UsbError (*AMS_SWI_Write_Address) (unsigned char outputPinSelect, unsigned char registerAddress, unsigned char value ,unsigned char delay); 


class USBBoxCommunication : public AMSCommunication
{
	Q_OBJECT

public:
	static DWORD frequencyTo8051Ticks( DWORD frequencyInHertz );

public:
	USBBoxCommunication(unsigned char devAddr);
	~USBBoxCommunication();

	void setDevAddr(unsigned char registerAddress);
	AMSCommunication::Error hwConnect();
	void hwDisconnect();
	bool isConnected();
	AMSCommunication::Error hwReadRegister(unsigned char reg, unsigned char *val);
	AMSCommunication::Error hwWriteRegister(unsigned char reg, unsigned char val);
	AMSCommunication::Error hwSendCommand(QString command, QString * answer);
	UsbError sendReceivePacket(QByteArray *in, const QByteArray *out);
	/*!
	 *****************************************************************************
	 *  \brief Sends a Plain SPI packet to the usb box
	 *
	 *  \param in : the size of this byte array is used to determine the number
	 *		of bytes to be received. The received bytes are put into this
	 *      packet. 
	 *  \param out : the byte array to be transmitted over Plain SPI. Note that
	 *		all bytes are transmitted via Plain SPI. The Usb Box specific header
	 *      is added by this function.
	 *  \return whether transmit/receive was successful or not
	 *****************************************************************************
	 */
	UsbError sendReceiveSPIPacket(QByteArray * in, const QByteArray * out);
	/*!
	 *****************************************************************************
	 *  \brief Sends a Plain I2C packet to the usb box
	 *
	 *  \param in : the size of this byte array is used to determine the number
	 *		of bytes to be received. The received bytes are put into this
	 *      packet. 
	 *  \param out : the byte array to be transmitted over Plain I2C. Note that
	 *		the device address set by setDevAddr function and the startRegister
	 *      will be properly encoded by this function. After this i2c specific
	 *      header all bytes in the out array are transmitted. 
	 *  \return whether transmit/receive was successful or not
	 *****************************************************************************
	 */
	UsbError sendReceiveI2CPacket(char startRegister, QByteArray * in, const QByteArray * out);
	/*!
	 *****************************************************************************
	 *  \brief Sends a Trimmer packet to the usb box
	 *
	 *  \param protocol : the transmission protocol the trimmer in the Usb box
	 *		shall use. E.g. PlainSPI
	 *  \param highTimeInMicroSec : the time the clock shall be high
	 *  \param lowTimeInMicroSec : the time the clock shall be low
	 *  \param out : the byte array to be transmitted by the trimmer with the
	 *		specified clock timing
	 *  \return whether transmit was successful or not
	 *****************************************************************************
	 */
	UsbError sendTrimmerPacket( TrimmerProtocol protocol, unsigned char highTimeInMicroSec, unsigned char lowTimeInMicroSec, const QByteArray * out );
	/*!
	 *****************************************************************************
	 *  \brief Instructs the Usb Box to transmit a VSync signal with the
	 *		specified frequency in hertz
	 *
	 *  \param frequencyInHertz : the desired frequency for th VSync signal
	 *  \return whether transmit was successful or not
	 *****************************************************************************
	 */
	UsbError setVsyncFrequency ( DWORD frequencyInHertz );
	/*!
	 *****************************************************************************
	 *  \brief  Sets the Pwm Signals
	 *
	 *  \param frequencyInHertz : is the desired frequency for signal PWM exclusive
	 *         or 3D_PWM34 and 3D_PWM12. If set to 0 all PWM signals are turned off.
	 *         Note that we have a maximum range of 1 - 500 Hertz.
	 *  \param dutyCycleInPercent : define the duty cycle for PWM or 3D_PWM_12 in 
	 *         percent. If 0 or >=100 is given all pwm signals are turned off.
	 *  \param secondDutyCycleInPercent : define the duty cycle for 3D_PWM_34 if  
	 *         a value > 0 and < 100 is given. If a value of 0 or >=100 is given 
	 *         this selects that only the PWM signal is sent. 
	 *  \param delayInPercent : if a value < 100 is given than it specifies the delay
	 *         for the signal 3D_PWM_34 to 3D_PWM_12. If a value >= 100 is given
	 *         this is equal to giving 0 as value (=no delay).
	 *  \param pwmSignal : the corresponding port B pin is used for generating
	 *         the pwm signal (default is PWM. E.g. to get Dig2 use 0x40).
	 *****************************************************************************
	 */
	UsbError setPwmSignals( DWORD frequencyInHertz, unsigned char dutyCycleInPercent, unsigned char secondDutyCycleInPercent = 0, unsigned char delayInPercent = 100, unsigned char pwmSignal = USB_PWM_SIGNAL_PWM );
	/*!
	 *****************************************************************************
	 *  \brief  Set delay for single wire and select pin on small connector
	 *
	 *  \param delay : is approximaltely the delay for the t0H and t1L in microseconds
	 *				   the t0L and t1H are twice this value
	 *  \param pin : 0 selects small connector pin 4, any other value selects 
	 *					small connector pin 2
	 *****************************************************************************
	 */
	void setSingleWireProperties( unsigned char delay, unsigned char pin );
	/*!
	 *****************************************************************************
	 *  \brief Instructs the Usb Box to configure the Multi-level i2c protocol
	 *
	 *  \param halfSpikeSupTime - the spike sup time is twice this value (one half
	 *   for high and the other half for low )
	 *  \param halfEnTime - the En Time is twice this value (one half for high and
	 *  the other half for low )
	 *****************************************************************************
	 */
	void setMultiLevelI2cProperites( unsigned char halfSpikeSupTime, unsigned char halfEnTime );
	/*!
	 *****************************************************************************
	 *  \brief Instructs the Usb Box to transmit a AS1121 specific packet
	 *
	 *  \param modeIsDotCorrection - if true the data is for dot correction mode,
     *   if false, it is for pwm mode
	 *  \param data - the bytes to transmit to the as1121 via i2c (size depends
	 *   on the mode 
	 *  \param isOn - if true the chip is enabled so that it operates, if false
	 *  the chip is disabled
	 *  \param autoRepeat - if true the chip is feeded continually with the gclk
	 *  (so that the selected leds stay on), if false the leds are on for only
	 *  one gclk cycle
	 *  \param latchLd - whether after the last transmitted data byte the ld signal
	 *  shall be latched or not (if there is more than 1 as1121 connected, only
	 *  when the data to the last as1121 is sent, the ld should be latched).
	 *  \return whether transmit was successful or not
	 *****************************************************************************
	 */
	UsbError sendAS1121Packet( bool modeIsDotCorrection, QByteArray * data, bool isOn, bool autoRepeat, bool latchLd = true );
	/*!
	 *****************************************************************************
	 *  \brief Instructs the Usb Box to transmit a AS112x packet. This is very
	 *  similar to the sendAS1121Packet function, but with less functionality,
	 * as the AS112x derivates, need less control from the usb box.
	 *
	 *  \param data - the bytes to transmit to the as112x via i2c
	 *  \param isOn - if true the chip is enabled so that it operates, if false
	 *  the chip is disabled
	 *  \param latchLd - whether after the last transmitted data byte the ld signal
	 *  shall be latched or not (if there is more than 1 as1121 connected, only
	 *  when the data to the last as1121 is sent, the ld should be latched).
	 *  \return whether transmit was successful or not
	 *****************************************************************************
	 */
	UsbError sendAS112xPacket( QByteArray * data, bool isOn, bool latchLd = true );
	/*!
	 *****************************************************************************
	 *  \brief Configures the dimming algorithm for one instance
	 *
	 *  \param isFirstSignal - true means we are configuring instance 1, else
     *  instance 2
     *  \param startInPercent - from 0 to 100 percent (where we should start dimming)
     *  \param stopInPercent - from 0 to 100 percent (where we should stop dimming)
     *  \param timeInMs - how long it shall take to dimm from 0 to 100 percent in 
     *  milli-seconds
	 *  \return whether all parameters were within the allowed range
	 *****************************************************************************
	 */
	bool startDimming( bool isFirstSignal, int startInPercent, int stopInPercent, int timeInMs );
	/*!
	 *****************************************************************************
	 *  \brief this function must be called cyclic to transmit a set of dimm
     *  packets to the usb box. The content of the dimm packets is generated
     *  automatically from the configuration done with startDimming  
	 *
	 *  \param repeatLastPacket : this is an artefact in the usb-box that 
     *   needs the first of all packets after startDimming twice
	 *  \return whether transmit was successful or not
	 *****************************************************************************
	 */
	UsbError sendDimmPacket( bool repeatLastPacket = false );
	/*!
	 *****************************************************************************
	 *  \brief returns the 3 byte firmware version number encoded in an
     *  DWORD ( MajorNumber [bits:23-16] | MinorNumber [bits:15-8] | ReleaseMarker [bits:7-0])
     *  If no connection is possible or no firmware version number is found
	 *  it returns 0 as version number. Note that the MajorNumber for the
	 *  Usb box firmware is always 1 (as the usb box firmware number on the
	 *  device has only 2 numbers).
	 *  
	 *  \return the firmware version number encoded in a DWORD
	 *****************************************************************************
	 */
	DWORD firmwareVersionNumber( ); 

	AMSsetPortAsInput		  setPortAsInput;
	AMSsetPortAsOutput		  setPortAsOutput;
	AMSwritePort			  writePort;
	AMSreadPort				  readPort;
    AMSusbReadByte			  readByteFunction;
    AMSusbWriteByte			  writeByteFunction;
	AMSusbFirmwareID		  readFirmwareId;
	AMSusbIsConnected         usbIsConnected;
	AMSusbFirmwareID          usbFirmwareID;
	AMSusbDebugMsg            usbDebugMsg;
	AMSsetInterface           setInterface;
	AMSwriteByte              writeByte;
	AMSblkWrite               blkWrite;
	AMSblkRead                blkRead;
	AMSreadADC                readADC;
	AMSgetPortDirection       getPortDirection;
	AMSsendPacket             sendPacketDLL;
	
protected:
	void setConnectionProperties(void *in);
	void getConnectionProperties(void *out);
	void setPwmSignal( char * outPacket, int index, DWORD ticks, int signal );


private:
	AMS_SWI_Write_Address singleWire;	 
	UsbError setI2CDeviceAddress(unsigned char registerAddress);
	UsbBoxDimming itsDimmer;
	char itsPacket[ USB_PACKET_SIZE ];

};


#endif // USBBOXCOMMUNICATION_H
