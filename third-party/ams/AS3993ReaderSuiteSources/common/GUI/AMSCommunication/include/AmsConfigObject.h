/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComObject.h
 *
 *  \author M. Arpa
 *
 *  \brief  Base class of all classes that implement objects that can
 * be transmitted and received by using the AmsComStream class.
 */

#ifndef AMS_CONFIG_OBJECT_H
#define AMS_CONFIG_OBJECT_H

/* includes --------------------------------------------------------- */

#include "AmsComObject.h"
#include <QString>
#include <QFile>
#include <QMap>

/* forward declarations ---------------------------------------------- */


/* class declarations ------------------------------------------------ */

class AmsRegisterParser 
{
public:
    AmsRegisterParser( const QString & registerDefinitionFileName );
    ~AmsRegisterParser( ) {};

    virtual bool registerAddress( const QString & registerName, unsigned int & registerAddress ) = 0;

private:
    /* forbid other c'tors */
    AmsRegisterParser( );
    AmsRegisterParser( const AmsRegisterParser & );

protected:
    QFile itsFile;
    bool itsIsOpen;
};

class AmsPicRegisterParser : public AmsRegisterParser
{
public:
    AmsPicRegisterParser( const QString & registerDefinitionFileName );
    ~AmsPicRegisterParser( ) {} ;

    virtual bool registerAddress( const QString & registerName, unsigned int & registerAddress );

private:
    /* forbid other c'tors */
    AmsPicRegisterParser( );
    AmsPicRegisterParser( const AmsPicRegisterParser & );

protected:
    QMap< QString, unsigned int > itsRegisterMap;
};


class AmsConfigObject : public AmsComObject
{
public:
	/* c'tor */
	AmsConfigObject 
	  ( AmsRegisterParser & parser
      , unsigned int tracePattern = AMS_COM_TRACE_ALLWAYS
	  );

    AmsConfigObject 
        ( AmsRegisterParser & parser
        , const QString & registerName
        , unsigned int tracePattern = AMS_COM_TRACE_ALLWAYS
        );

	/* copy c'tor */
	AmsConfigObject ( const AmsConfigObject & other );
	
	/* d'tor */
	~AmsConfigObject ( ) { };

	/* serialises the object in the buffer, bufferSize is the number of
	   bytes available for serialisation (must be >= serialiseSize, otherwise
		nothing will be serialised and the functions returns false 
	   returns true if something was serialised, false if not */
	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	/* serialises something that is required by the peer to
		provide the data necessary for deserialisiation 
	   returns true if something was serialised, false if not */
	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	/* deserialises the object in the buffer, bufferSize is the number
	   of bytes in the buffer ( must be >= deserialiseSize). Note that
	   before receiving anything, the owner of this class should call
	   rxSerialise first (so send out something if required for receiving
	   data )
	   returns true if something was deserialised, false if not */
	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    /* deserialises the object from the XML string str into the object members 
	   returns true if something was deserialised, false if not */
    virtual bool fill( QXmlStreamReader * xml );

    bool registerName( const QString & regName );
    unsigned int registerAddress( ) const { return itsRegisterAddress; };
    unsigned int registerValue( ) const { return itsRegisterValue; };
    void registerValue( unsigned int regValue ) { itsRegisterValue = regValue; };
    unsigned int valueMask( ) const { return itsValueMask; };
    void valueMask( unsigned int valMask ) { itsValueMask = valMask; };

private:
    
    /* forbid c'tor */
	AmsConfigObject( );

protected:
    AmsRegisterParser & itsParser;
    bool itsRegisterNameValid;
    QString itsRegisterName;
    unsigned int itsRegisterAddress;
    unsigned int itsRegisterValue;
    unsigned int itsValueMask;
};

#endif /* AMS_CONFIG_OBJECT_H */
