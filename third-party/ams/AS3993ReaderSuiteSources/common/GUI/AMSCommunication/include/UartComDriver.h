/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file UartComDriver.h
 *
 *  \author Bernhard Breinbauer
 *          M. Arpa
 *
 *  \brief  Communication class for UART streaming communication 
 */

#ifndef UART_COM_DRIVER_H
#define UART_COM_DRIVER_H

#include "AmsComDriver.h"
#include "qextserialport.h"

/* forward declaration of for serial port communication class */
class QextSerialPort;
struct PortSettings;

class UartComDriver : public AmsComDriver  
{
public:
    /* c'tor */
	UartComDriver ( unsigned int comPort );

	/* d'tor */
	~UartComDriver ( );

	/* opens the communication channel (if not yet opened) 
	   returns true if channel was successfully opened */
	virtual bool open ( );

	/* closes the communication channel (if not yet closed) 
	   returns true if channel was successfully closed */
	virtual bool close ( );

	/* initiates an immediate transmit */
	virtual int flush ( );

	/* send data via communication channel */
	virtual int txBuffer ( unsigned char * buffer, int size );

	/* receive data via communication channel */
	virtual int rxBuffer ( unsigned char * buffer, int size );

    /* find out if the firmware takes the same stream protocol version as the gui */
    virtual bool isProtocolVersionCompatible( bool enableBootloaderIfNotCompatible ); 

    /* get current uart settings */
    PortSettings portSettings ( ) const;

    /* set new uart settings */
    void setPortSettings ( const PortSettings &settings );

protected:
    virtual void clearError( );

    virtual void clearFirmwareError( );


private:
	/* service function for transmit */
	void tx ( );

	/* service function for receive */
    void rx ();

	/* forbid the default c'tor */
	UartComDriver ( ); 

	/* forbid the copy c'tor */
	UartComDriver ( const UartComDriver & other ); 

protected:
	QextSerialPort * itsUart;
	const unsigned int itsPort;
    PortSettings itsPortSettings;

	/* buffer and ad-ons for transmit */
	unsigned char * const itsTxHook;
	int itsToTx; /* number of bytes to transmit as payload */
	unsigned char * const itsTxBuffer;

	/* buffer and ad-ons for transmit */
	unsigned char * const itsRxHook;
    int itsRxed;      /* number of received bytes */
	unsigned char * const itsRxBuffer;
	
	/* the transaction identifier */
	unsigned char itsTid;
};

#endif /* UART_COM_DRIVER_H */
