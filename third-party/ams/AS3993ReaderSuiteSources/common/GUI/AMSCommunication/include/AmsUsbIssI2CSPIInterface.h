/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  AmsUsbIssI2CSPIInterface class header file
 *
 *  AmsUsbIssI2CSPIInterface is a derived class of AMSI2CSPIInterface to talk i2c or spi through the usb iss.
 */

#ifndef AMS_USB_ISS_I2C_SPI_INTERFACE_H
#define AMS_USB_ISS_I2C_SPI_INTERFACE_H

#include "UsbIss.h"
#include "AmsI2CSPIInterface.h"


class AmsUsbIssI2CSPIInterface : public AmsI2CSPIInterface
{
public:
    AmsUsbIssI2CSPIInterface( UsbIss & iss ) : AmsI2CSPIInterface( ), itsIss( iss ) { };
    ~AmsUsbIssI2CSPIInterface( ) { };

    void setComPort( unsigned char port ) { itsIss.setComPort( port ); };

    virtual bool isOpened( ) const { return itsIss.isOpened( ); };

    /* function to reset the interface - i.e. the stream is also closed, you must re-open it afterwards manually */
    virtual void reset( ) { itsIss.reset( ); };

    /* function to open the interface */
    virtual bool open( ) { return itsIss.open( ); };

    /* function to close the interface */
    virtual void close( ) { itsIss.close( ); };

    /* function to configure i2c for a specific frequency (range somewhat 20000 - 1000000) */
    virtual bool i2cConfigure( unsigned int frequencyInHz, bool useDefaultI2C /* ignored on usb iss */ );

    /* function to configure spi for a specific frequency (range somewhat 250000 - 8000000), for polarity and phase */
    virtual bool spiConfigure( unsigned int frequencyInHz, int polarity, int phase );
    
    /* function to transmit / and receive the given number of bytes in i2c format. The device address is the 7-bit address. If you
       just want to transmit set the receiveSize to 0. The function returns true if it was successful (i.e. an I2C Ack has been
       received for transmit and data has been read for receive */
    virtual bool i2cTxRx( unsigned char deviceAddress, unsigned int sendLen, const unsigned char * send, unsigned int receiveLen, unsigned char * receive );

    /* function to transmit / and receive the given number of bytes in SPI format. The function receives as many
       bytes as have been sent. The function returns true if it was successful. */
    virtual bool spiTxRx( unsigned int sendLen, const unsigned char * send, unsigned char * receive );

private:
    /* forbid default c'tors */
    AmsUsbIssI2CSPIInterface ( const AmsUsbIssI2CSPIInterface & other );

protected:
	UsbIss & itsIss;
};

#endif
