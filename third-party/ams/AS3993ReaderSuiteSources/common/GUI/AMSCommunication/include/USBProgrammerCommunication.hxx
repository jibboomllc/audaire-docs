/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author U. Herrmann
 *
 *  \brief  
 */

#ifndef USBPROGRAMMERCOMMUNICATION_H
#define USBPROGRAMMERCOMMUNICATION_H

#include <windows.h>
#include <stdio.h>
#include "AMSCommunication.hxx"
#include "USBHIDCommunication.hxx"

#define DEFAULT_DAC_VZAP_GAIN   3.7
#define DEFAULT_DAC_VDD_GAIN    2.5
#define DEFAULT_DAC_VREF        2.5

class USBProgrammerCommunication : public USBHIDCommunication
{
	Q_OBJECT

public:

	enum CommunicationType
	{
		SPI = 0,
		SSI = 1,
		easy2Zapp = 2,
		ADC = 3,
		PDB = 4,
        UART = 5, 
		NoType = 100
	};

	void setCommunicationType(CommunicationType type);
	
	USBProgrammerCommunication(int pid);
    ~USBProgrammerCommunication();
	AMSCommunication::Error hwReadRegister(unsigned char reg, unsigned char *val);
	AMSCommunication::Error hwWriteRegister(unsigned char reg, unsigned char val);
	AMSCommunication::Error connect();
	void disconnect();

    AMSCommunication::Error vzapMuxInit();
    AMSCommunication::Error portsInit();
    AMSCommunication::Error vzapMuxSelect(unsigned char channel);
    AMSCommunication::Error vzapSwitch(bool state);
    AMSCommunication::Error dacUpdateChannel(unsigned char dac, unsigned char channel, double voltage);

	AMSCommunication::Error ssiRead(unsigned char length, unsigned char valueBuffer[16]);
	AMSCommunication::Error easy2ZappRead(unsigned char numbits, unsigned char valueBuffer[16]);
	AMSCommunication::Error easy2ZappWrite(unsigned char numbits, unsigned char valueBuffer[16]);
	AMSCommunication::Error adcSample(unsigned char channel, unsigned short* value);
	AMSCommunication::Error pdbInitialize();
	AMSCommunication::Error pdbRead(unsigned char die, unsigned char adresses[2], unsigned char values[2]);
	AMSCommunication::Error pdbWrite(unsigned char die, unsigned char adress, unsigned char value);
	AMSCommunication::Error pdbRead128(unsigned char die, unsigned char buffer[16]);
	AMSCommunication::Error pdbWrite128(unsigned char die, unsigned char buffer[16]);
	AMSCommunication::Error pdbSendCommand(unsigned char die, unsigned char command);

	AMSCommunication::Error setReadMemAddress(unsigned short address);
	AMSCommunication::Error setWriteMem(unsigned short address,const unsigned char[16]);
	AMSCommunication::Error setExecuteCommand(unsigned short command);
	AMSCommunication::Error setRemoteMode(int on);
	AMSCommunication::Error getFirmwareInfo(unsigned char[64]);
	AMSCommunication::Error getReadMem(unsigned char[16]);

    AMSCommunication::Error enterBootloaderMode();

    AMSCommunication::Error spiConfigure(unsigned int spiFrequency, unsigned char clkPhase
                                        , unsigned char clkPolarity, unsigned char csIsActiveLow, unsigned char use3WireSpi);
    AMSCommunication::Error spiReadWrite(unsigned char * in_buf, unsigned char * out_buf, unsigned char nrOfBytes);
    AMSCommunication::Error spiAS5400WritePage(unsigned short startAddr, unsigned char * in_buf, unsigned char nrOfBytes);
    AMSCommunication::Error spiAS5400ReadHallData(unsigned short startAddr, unsigned char * out_buf, unsigned char nrOfBytes, unsigned char readSel =  0);
    AMSCommunication::Error AS5401EepromTransfer(unsigned short addr, unsigned char * in_buf, unsigned char * out_buf, unsigned char nrOfBytes, bool write);

    AMSCommunication::Error AS5162UartRegisterRead(unsigned char RegAddr, unsigned short* RegValuePointer);
    AMSCommunication::Error AS5162UartRegisterWrite(unsigned char RegAddr, unsigned short RegValue);

    // UART Communication
    AMSCommunication::Error uartConfigure();

private:
	AMSCommunication::Error ssiInitialize();
	AMSCommunication::Error easy2ZappInitialize();
	AMSCommunication::Error adcInitialize();
	AMSCommunication::Error pdbDeinitialize();

    unsigned short dacVzapVoltageToValue(double voltage);
    unsigned short dacVddVoltageToValue(double voltage);

	AMSCommunication::Error outReport(const unsigned char buf[64]);
	AMSCommunication::Error inReport(unsigned char buf[64]);

	bool waitForCommandFinished();

	CommunicationType activeType;

	QMap<CommunicationType,bool> typeInitialized;

    struct usbhidConfigStructure hidconfig;

    double DAC_VDD_GAIN;
    double DAC_VZAP_GAIN;
    double DAC_VREF;
};

#endif // USBProgrammerCommunication_H
