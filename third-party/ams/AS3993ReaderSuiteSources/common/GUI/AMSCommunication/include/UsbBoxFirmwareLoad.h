/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   ams EZ-USB Box Firmware Loader
 *      $Revision: $
 *      LANGUAGE:  C++
 */

/*! \file   UsbBoxFirmwareLoad.h
 *  \author M. Arpa, 
 *
 *  \brief  Header file to download 8051 files to the XDATA in the ez usb box
 *          or to the i2c eeprom in the ez usb box.
 *          Use this class with a reporter class to have a gui version. ->
 *          See class FirmwareLoadGui.
 */

#ifndef _USB_BOX_FIRMWARE_LOAD_H_
#define _USB_BOX_FIRMWARE_LOAD_H_

#include <windows.h>
#include <QString>
#include <QTime>
#include "UsbBoxReporter.h"
#include "ezusbsys.h"


class UsbBoxFirmwareLoad 
{
	static UsbBoxReporter theDummyReporter;

public:
	static QString version ( const QString & versionString );
	static void addMajorNumber( QString & versionString );

	UsbBoxFirmwareLoad ( UsbBoxReporter * report = 0, bool useAmsVidPid = true );
	~UsbBoxFirmwareLoad ( );

	/* returns true if download was successfull */
	bool downloadToXdata ( const QString & fullFilename );
	/* returns true if download was successfull */
	bool downloadToEeprom ( const QString & fullFilename );
	/* returns true if upload was successfull */
	bool uploadFromXdata ( const QString & startAddress, const QString & size, bool reset8051, bool ascii );
	
	/* returns true if the version of the box is higher or equal to the version of the file */
	bool versions ( const QString & fullFilename, QString & fromBox, QString & fromFile, QString & result = QString("") );

	void changeVidPid ( bool useAmsVidPid );

protected:
	bool downloadIntelHexRecords ( INTEL_HEX_RECORD * toDownload, bool toEeprom, int maxProgress );
	bool loadControl ( bool toEeprom );
	bool convert ( bool writeIICFile = false );

	QString getTime( )
	{
		QTime current = QTime::currentTime( );
		return current.toString( "hh:mm:ss.zzz" );
	};

private:
	UsbBoxFirmwareLoad ( const UsbBoxFirmwareLoad & );

private:
	UsbBoxReporter & itsReporter; /* to report information and warnings */
	bool itsUseAmsVidPid; /* whether to use ams vid/pid or cypress */
	const QString * itsFilename; /* pointer to filename */
	char itsBlockBuffer[ MAX_EP0_XFER_SIZE ]; /* buffer for transfer to usb driver */
	INTEL_HEX_RECORD itsIntelHexRecord[ 2 ]; /* converted intel hex record storage */
};



#endif /* _USB_BOX_FIRMWARE_LOAD_H_ */