#ifndef AMSDeviceDetector_H_
#define AMSDeviceDetector_H_

#include <QObject>
#include <QString>
#include <QStringList>
#include <QMap>

#include <windows.h>
#include <stdlib.h>
#include "./winhid/usbiodef.h"

//#define AMSDEVICEDETECTOR_DEBUG

#ifdef AMSDEVICEDETECTOR_DEBUG
#include <AMSTrace.hxx>
#endif

#include "AMSDeviceChangeDetector.hxx"

class AMSDeviceDetector : public QObject
#ifdef AMSDEVICEDETECTOR_DEBUG
, AMSTraceModule
#endif
{
Q_OBJECT

public:
	AMSDeviceDetector(QObject* parent = 0)	;
	virtual~ AMSDeviceDetector();

	virtual bool registerForUSBDevice(quint16 vendorID, quint16 productID);
	virtual bool registerForHIDDevice(quint16 vendorID, quint16 productID);

	virtual QStringList getConnectedUSBDevicePaths(quint16 vid, quint16 pid);
	virtual QStringList getConnectedHIDDevicePaths(quint16 vid, quint16 pid);

signals:
	void hidDeviceAttached ( QString devicePath, quint16 vendorID, quint16 productID );
	void hidDeviceRemoved ( QString devicePath, quint16 vendorID, quint16 productID );

	void usbDeviceAttached ( QString devicePath, quint16 vendorID, quint16 productID );
	void usbDeviceRemoved ( QString devicePath, quint16 vendorID, quint16 productID );


protected:
	QMap<quint16, quint16> m_usbDevices;
	QMap<quint16, quint16> m_hidDevices;

private slots:
	void win_usbDeviceAttached (QString devicePath, quint16 vendorID, quint16 productID);
	void win_usbDeviceRemoved (QString devicePath, quint16 vendorID, quint16 productID);

	void win_hidDeviceAttached (QString devicePath, quint16 vendorID, quint16 productID);
	void win_hidDeviceRemoved (QString devicePath, quint16 vendorID, quint16 productID);

private:
	AMSDeviceChangeDetector m_detector;
};

#endif /* AMSDeviceDetector_H_ */
