/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComDriver.h
 *
 *  \author M. Arpa
 *
 *  \brief  This is the base class of all steam communication drivers. I.e. 
 * any driver that shall be used for stream communication must be derived
 * from this class.
 */

#ifndef AMS_COM_DRIVER_H
#define AMS_COM_DRIVER_H


class AmsComDriver
{

    static const char * errorStrings[ ];
    static const int minErrorCode;
    static const int maxErrorCode;

public:
    
    static const int NoError = 0;
    static const int ChannelOpenError = -1;
    static const int TxError = -2;
    static const int RxError = -3;
    static const int RxDataMismatch = -4; /* e.g. the expected protocol is not the one received */

    static const char * errorToString( int code ); 

public:
    /* c'tor */
	AmsComDriver( ) : itsIsChannelOpen( false ), itsLastError( NoError ), itsLastFirmwareError( 0 ) { };
	
	/* d'tor */  
	~AmsComDriver( ) { };

	/* opens the communication channel (if not yet opened) 
	   returns true if channel was successfully opened */
	virtual bool open ( ) = 0;

	/* closes the communication channel (if not yet closed) 
	   returns true if channel was successfully closed */
	virtual bool close ( ) = 0;

	/* returns true if channel is open, and false if channel is closed */
	bool isOpened ( ) { return itsIsChannelOpen; };

	/* send any not yet sent data immediately, returns the last error */
	virtual int flush( ) = 0;
	   
	/* send data via communication channel, returns the last error */
	virtual int txBuffer( unsigned char * buffer, int size ) = 0;

	/* receive data via communication channel, size is required size,
	   returns the last error */	   
	virtual int rxBuffer( unsigned char * buffer, int size ) = 0;

	/* returns the last error and clears it */
    int lastError( ) 
        { 
            int temp = itsLastError; 
            if ( itsLastError != NoError ) 
            { 
                clearError( ); 
                itsLastError = NoError; 
            }
            return temp; 
        };

    /* if the firmware detects an error in a write request (without a status response) the
       next time a response is sent back to the host, the "firmware error" is carried back
       in the packet header (e.g. hid header, uart header, not the protocol header) */
    int lastFirmwareError( ) 
        { 
            int temp = itsLastFirmwareError; 
            if ( itsLastFirmwareError )
            {
                clearFirmwareError( ); 
                itsLastFirmwareError = 0; 
            }
            return temp; 
        };

    /* function returns true if firmware and pc talk the same protocols, 
       otherwise the function returns false. If the firmware and GUI do not share the same 
       protocol version, the bootloader of the firmware can be enabled to download a newer
       firmware. 
       If the pc cannot talk to the firmware suddenly (cause a cable was disconnected),
       we do not know if they are compatible. In this case you must check lastError(). 
     */
    virtual bool isProtocolVersionCompatible( bool enableBootloaderIfNotCompatible = true ) = 0; 

protected:
    /* clear any internal variables that are set due to the error */
    virtual void clearError( ) = 0;

    /* clear any internal variables that are set due to the error */
    virtual void clearFirmwareError( ) = 0;

    /* call this function before calling open if driver was not open */
    void internalReset( ) { lastError(); lastFirmwareError(); };

private:
	/* forbid the copy c'tor */
	AmsComDriver( const AmsComDriver & other );
		   
protected:
	/* whether we have a valid communication channel or not */
	bool itsIsChannelOpen;

	/* the last error that occured */
	int itsLastError;

    /* the last error reported by the firmware side */
    int itsLastFirmwareError;
};


#endif /* AMS_COM_DRIVER_H */
