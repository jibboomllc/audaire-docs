/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file SpiComObjects.h
 *
 *  \author M. Arpa,
 *          F. Lobmaier
 *
 *  \brief Classes to read and write  using spi as communication means.
 */

#ifndef SPI_COM_OBJECTS_H
#define SPI_COM_OBJECTS_H

#include "AmsCom.h"
#include "AmsComObject.h"

/* forward declaration ------------------------------------ */

class QXmlStreamWriter;
class QxmlStreamReader;


/* class declaration -------------------------------------- */

/* class to clone a spi communication object of a specific type from the information
   found in the xml stream.
 */
class SpiComXmlReader
{
public:
    /* interprets the opening tag information and others and generates a new
       object of the corresponding type with new-operator */
    static AmsComObject * clone( QXmlStreamReader * xml, unsigned char protocol );

    SpiComXmlReader( ) { };
    ~SpiComXmlReader( ) { };
    SpiComXmlReader( const SpiComXmlReader & other ) { };
};

/* class to read and write registers using spi 
 */
class SpiRegisterObject : public AmsComObject
{
public:

	/* c'tor */
	SpiRegisterObject 
	  ( unsigned int slaveReadAddress
	  , unsigned int slaveWriteAddress
      , int slaveAddressSize    /* set this to 0 if you do not use the slave address */
      , unsigned int readRegisterAddress   
      , unsigned int writeRegisterAddress  /* set this to the same value as readRegisterAddress if you use slave addressing */
	  , unsigned int data
	  , int addressSize = 1
	  , int dataSize = 1 
	  );

	/* copy c'tor */
	SpiRegisterObject ( const SpiRegisterObject & other );
	
	/* d'tor */
	~SpiRegisterObject ( ) { };

	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * xml );



	unsigned int get( ) const { return itsData; };

	void set( unsigned int value ) { itsData = value; };

	void set( unsigned int registerAddress, unsigned int value ) 
	{ 
	  itsReadAddress = registerAddress; 
      itsWriteAddress = registerAddress;
	  set( value ); 
	};

   	void set( unsigned int readRegisterAddress, unsigned int writeRegisterAddress, unsigned int value ) 
	{ 
	  itsReadAddress = readRegisterAddress; 
      itsWriteAddress = writeRegisterAddress;
	  set( value ); 
	};

private:
    /* forbit the default c'tor */
    SpiRegisterObject();

protected:
    int itsSlaveAddressSize;/* whether the slave is 8bit, 16bit or 32bit in bytes (so 1,2 or 4) */
	int itsAddressSize; /* whether the address is 8bit, 16bit or 32bit in bytes (so 1,2 or 4) */
	int itsDataSize; /* whether the data is 8bit, 16bit or 32bit in bytes (so 1,2 or 4) */
    unsigned int itsSlaveReadAddress;
    unsigned int itsSlaveWriteAddress;
	unsigned int itsReadAddress;
	unsigned int itsWriteAddress;
	unsigned int itsData;
};

/*  Configuration Object used for spi configuration. 
 */
class SpiConfigObject : public AmsComObject
{
public:
    static const unsigned char theSpi1Module = 0;
    static const unsigned char theSpi2Module = 1;
    static const unsigned char theClockPhase0 = 0;
    static const unsigned char theClockPhase1 = 1;
    static const unsigned char theClockPolarity0 = 0;
    static const unsigned char theClockPolarity1 = 1;
    static const unsigned int theClockFrequency1MHz = 1000000;

public:
	/* default c'tor */
	SpiConfigObject 
        ( unsigned char module = theSpi1Module
        , unsigned char deviceId = 0
        , unsigned int clockFrequency = theClockFrequency1MHz
        , unsigned char clockPhase = theClockPhase0
        , unsigned char clockPolarity = theClockPolarity0
        );

	/* copy c'tor */
	SpiConfigObject ( const SpiConfigObject & other );
	
	/* d'tor */
	~SpiConfigObject ( ) { };

	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * xml );

    void set( unsigned char deviceId ) { itsDeviceId = deviceId; };

protected:
	unsigned char itsModule;
    unsigned char itsDeviceId;
	unsigned int itsClockFrequency;
    unsigned char itsClockPhase;
    unsigned char itsClockPolarity;
};

class SpiFlexObject : public AmsComObject
{
public:
    /* default c'tor */
    SpiFlexObject ( unsigned int maxDataSize = AMS_STREAM_MAX_DATA_SIZE, const unsigned char * data = 0, unsigned int dataSize = 0 );

    /* d'tor */
    ~SpiFlexObject ( ) { delete [] itsTxData; delete [] itsRxData; };

    virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * xml );

    bool set( const unsigned char * txData, unsigned int dataSize );
    unsigned char dataSize ( ) const { return itsDataSize; };
    const unsigned char * rxData ( ) const { return itsRxData; };

private:
    /* forbid copy c'tor */
    SpiFlexObject ( const SpiFlexObject & other );

protected:
    const unsigned int itsMaxDataSize;
    unsigned int itsDataSize;
    unsigned char * itsTxData;
    unsigned char * itsRxData;
};



#endif /* SPI_COM_OBJECTS_H */
