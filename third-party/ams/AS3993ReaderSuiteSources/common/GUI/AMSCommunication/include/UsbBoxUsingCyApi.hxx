/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  Abstract Base class - implemented either as UsbBoxUsingUsbConDll
 *          or as UsbBoxUsingCyApi class.
 *
 */

#ifndef USB_BOX_USING_CY_API_H
#define USB_BOX_USING_CY_API_H

#include "USBBoxCommunication.hxx"
#include <QString>
#include <QByteArray>
#include "UsbBox.hxx"
#include "usb.h"
#include <cyapi.h>

class UsbBoxUsingCyApi : public UsbBox
{
public:

	static bool isDriverAvailable ( );

	UsbBoxUsingCyApi();
	~UsbBoxUsingCyApi();

	virtual UsbError blkWriteFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes) ;
	virtual UsbError blkReadFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes) ;
	virtual UsbError getPortDirectionFn(Port io_port,unsigned char* direction) ;
	virtual UsbError readADCFn(unsigned char channel,unsigned* value) ;
	virtual UsbError readPortFn(unsigned char *data, int ioPort, unsigned char mask) ;
	virtual UsbError sendPacketFn(char *in,int in_length, const char *out, int out_length) ;
	virtual UsbError setPortAsInputFn(int ioPort, unsigned char mask) ;
	virtual UsbError setPortAsOutputFn(int ioPort, unsigned char mask);
	virtual UsbError setInterfaceFn(Interface iface) ;
	virtual bool     usbIsConnectedFn() ;
	virtual UsbError readFirmwareIdFn(unsigned char * str) ;
	virtual UsbError usbDebugMsgFn(unsigned char* str) ;
	virtual UsbError usbConnectFn(void) ;
	virtual UsbError usbDisconnectFn(void) ;
	virtual UsbError readByteFn(unsigned char registerAddress, unsigned char* registerValue) ;
	virtual UsbError setI2CDevAddrFn(unsigned char registerAddress) ;
	virtual UsbError writePortFn(unsigned char data, int ioPort, unsigned char mask) ; 
	virtual UsbError writeByteFn(unsigned char registerAddress, unsigned char registerValue) ;

	/* the following functions were not originally exported through the UsbBoxCommunication class.
	Therefore they are not currently implemented in UsbBoxUsingUsbConDll */
	virtual UsbError SWI_Write_EN1Fn(BYTE num_bytes,BYTE* values,int delay);
	virtual UsbError SWI_Write_EN2Fn(BYTE num_bytes,BYTE* values,int delay);
	virtual UsbError SWI_Write_AddressFn(BYTE chipAddress , BYTE registerAddress , BYTE value , BYTE delay );
	virtual UsbError SWI_FLASHFn(int OnTime);
	virtual	UsbError FusePPTrimFn( BYTE inverted , BYTE num_bytes );
	virtual UsbError ReadSRAMFn( BYTE start_addr,BYTE sram_address,BYTE* values,BYTE num_bytes);

	/* firmware load */
	virtual bool openDriver ( );
	virtual bool closeDriver ( );
	virtual bool control8051 ( bool reset8051 );
	virtual bool download ( bool toEeprom, char * buffer, int startAddress, int size );
	virtual bool upload ( char * buffer, short startAddress, short size, DWORD & numberBytes );

protected:
	UsbError usbTx(BYTE* data, LONG num_bytes = USB_PACKET_SIZE);
	UsbError usbRx(BYTE* data);
	bool getTaskFromInterface( Interface desiredInterface, BYTE & task );
	UsbError sendReceive ( BYTE task, BYTE * out, BYTE * in );

	CCyUSBDevice * itsDevice;
	CCyControlEndPoint * itsEp0;
	CCyBulkEndPoint * itsEp2In;
	CCyBulkEndPoint * itsEp2Out;
	BYTE i2c_device_address_;
};
#endif 
