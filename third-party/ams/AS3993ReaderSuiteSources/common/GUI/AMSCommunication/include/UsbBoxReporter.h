/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   ams EZ-USB Box Firmware Loader
 *      $Revision: $
 *      LANGUAGE:  C++
 */

/*! \file   reporter.h
 *  \author M. Arpa 
 *
 *  \brief  Header file for a reporter. Derive your own reporter class from
 *          this class to report anything.
 */

#ifndef _USB_BOX_REPORTER_H_
#define _USB_BOX_REPORTER_H_

#include <QString>

class UsbBoxReporter
{
public:
	UsbBoxReporter ( );
	~UsbBoxReporter ( );

	void virtual report ( const QString & str ); /* report string to screen 1 */
	void virtual report2 ( const QString & str ); /* report string to screen 2 */
	void virtual report ( int value ); /* add this value to your current progress value */

protected:
	UsbBoxReporter ( const UsbBoxReporter & );
};

#endif /* _REPORTER_H_ */