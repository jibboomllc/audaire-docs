/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComCapabilities.hxx
 *
 *  \author M. Arpa
 *
 *  \brief A communication can have different connection states, connection modes
 * and device detection states. With these classes we try to provide a common
 * access point to these capabilities.
 */

#ifndef AMS_COM_CAPABILITIES_HXX
#define AMS_COM_CAPABILITIES_HXX

#include <QObject>

/* forward declaration ------------------------------------ */

class AmsComCapablities;

/* class declaration -------------------------------------- */

class AmsComConnectionState : public QObject
{
    friend class AmsComCapablities;

    /* array containing the human readable version of the state */
    static const char * stateStrings[ ];

public:
    static const int unknown = 0;       /* state not known */
    static const int disconnecting = 1; /* trying to disconnect */
    static const int disconnected = 2;  /* no connection */
    static const int connecting = 3;    /* trying to establish connection */
    static const int connected = 4;     /* connection established */

    /* use this function to get a text string for the given state */
    static const char * stateToString( int state );

public:
    AmsComConnectionState( ) : itsState( unknown ) { };
    AmsComConnectionState( const AmsComConnectionState & other ) : itsState( other.itsState ) { };
    ~AmsComConnectionState( );

    /* returns the current state */
    int state( ) const { return itsState; };

protected:
    int itsState;
};

class AmsComDeviceState : public QObject
{
    friend class AmsComCapablities;
    
    /* array containing the human readable version of the state */
    static const char * deviceStateStrings[ ];

public:
    static const int unknown = 0;       /* device state not known */
    static const int disconnecting = 1; /* trying to disconnect device */
    static const int disconnected = 2;  /* no device connected or manually disconnected */
    static const int connecting = 3;    /* trying to find device */
    static const int connected = 4;     /* device found */

    /* use this function to get a text string for the given state */
    static const char * deviceStateToString( int state );

public:
    AmsComDeviceState( ) : itsDeviceState( unknown ) { };
    AmsComDeviceState( const AmsComDeviceState & other ) : itsDeviceState( other.itsDeviceState ) { };
    ~AmsComDeviceState( );

    int deviceState( ) const { return itsDeviceState; };

protected:

    int itsDeviceState;
};


/* base class for detection mode -> there is one derived for those that have auto-detection (e.g. USB), 
   and another for those that do not have auto-detection (e.g. UART) */
class AmsComDetectionMode
{
public:

    bool autoDetectionMode( ) const { return itsAutoDetection; };
    bool set( bool autoDetectionMode );

    AmsComDetectionMode( const AmsComDetectionMode & other ) : itsIsAutoDetectionPossible( other.itsIsAutoDetectionPossible ), itsAutoDetection( other.itsAutoDetection ) { };
    ~AmsComDetectionMode( ) { };

protected:
    AmsComDetectionMode( bool isAutoDetectionPossible, bool autoDetection ) : itsIsAutoDetectionPossible( isAutoDetectionPossible ), itsAutoDetection( autoDetection ) { };

    const bool itsIsAutoDetectionPossible;
    bool itsAutoDetection;
};

/* class capable of auto-detection - like USB */
class AmsComAutoDetectionMode : public AmsComDetectionMode
{
public:
    AmsComAutoDetectionMode ( bool autoDetection = true ) : AmsComDetectionMode( true, autoDetection ) { };
    AmsComAutoDetectionMode ( const AmsComAutoDetectionMode & other ) : AmsComDetectionMode( other ) { };
    ~AmsComAutoDetectionMode ( ) { };
};

/* class capable only of manual -detection - like UART */
class AmsComManualDetectionMode : public AmsComDetectionMode
{
public:
    AmsComManualDetectionMode ( ) : AmsComDetectionMode( false, false ) { };
    AmsComManualDetectionMode ( const AmsComManualDetectionMode & other ) : AmsComDetectionMode( other ) { };
    ~AmsComManualDetectionMode ( ) { };
};

#define AMS_VID 0x1325

class AmsComCapabilities : public QObject
{
    Q_OBJECT

public:

    AmsComCapabilities ( AmsComDetectionMode & detectionMode ) : itsConnectionState(), itsDeviceState(), itsDetectionMode( detectionMode ) {};
    ~AmsComCapabilities ( ) { };

    int connectionState( ) const { return itsConnectionState.state(); };
    int deviceState( ) const { return itsDeviceState.deviceState(); };

    bool set ( bool autoDetectionMode ) { return itsDetectionMode.set( autoDetectionMode ); };
    bool autoDetection( ) const { return itsDetectionMode.autoDetectionMode(); };

signals:
    void connectionStateChanged( int state );
    void deviceStateChanged( int state );

private:
    /* forbid c'tors */
    AmsComCapabilities ( );

protected:

    AmsComConnectionState itsConnectionState;
    AmsComDeviceState itsDeviceState;
    AmsComDetectionMode itsDetectionMode;
};
#endif /* AMS_COM_CAPABILITIES_HXX */
