
/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author R. Veigl
 *
 *  \brief  AmsRegisterCache class header file
 *
 *  AmsRegisterCache is a class used for caching values
 *  which can be written to the device's memory as once.
 */

#ifndef AMS_REGISTER_CACHE_H
#define AMS_REGISTER_CACHE_H

#include "AMSCommunication.hxx"



class AmsRegisterCache : public AMSCommunication
{
    Q_OBJECT

public:
	AmsRegisterCache(AMSCommunication *com);
	~AmsRegisterCache(void);
	
    void setConnectionProperties(void* properties);
    Error hwReadRegister(unsigned char reg, unsigned char *val);
    Error hwWriteRegister(unsigned char reg, unsigned char val);
    Error hwSendCommand(QString command, QString * answer);
    Error hwConnect();
    void hwDisconnect();


	QMap<unsigned char, unsigned char> *writeMask() { return &itsWriteMask; }
	QMap<unsigned char, unsigned char> *readMask() { return &itsReadMask; }

	int setRegisterValue(unsigned char address, unsigned char mask, unsigned char value);	
	int getRegisterValue(unsigned char address, unsigned char mask, unsigned char &value);
	int getRegisterValue(unsigned char address, unsigned char mask, bool &value);
	
	void blockCommunication(bool block);
		
	bool copyFrom(AmsRegisterCache* source);	
	bool copyFrom(AmsRegisterCache* source, const unsigned char startAddress, const unsigned char mask = 0xFF, const int length = -1);
	
	void loadFromDevice(bool disableMask = false);
	bool writeToDevice(bool disableMask = false);
	QList<unsigned char> getAddresses();
	QList<unsigned char> getChangedRegisters();
    
private slots:
        void on_com_dataChanged(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char value);
        void on_com_dataChanged(unsigned char reg, unsigned char subreg, bool isSubreg, unsigned char mask, unsigned char value, unsigned char access, int err, QString any);

private:	
    AMSCommunication *itsCom;

	QMap<unsigned char, unsigned char> itsRegisterMap; //current
	QMap<unsigned char, unsigned char> itsChanges; //current
	QMap<unsigned char, unsigned char> itsWriteMask; //only write bits which match the mask
	QMap<unsigned char, unsigned char> itsReadMask; //only read bits which match the mask
	bool itsIsComBlocked;
};


class AmsRegisterCacheProperties
{
public:
    AmsRegisterCacheProperties();
    AmsRegisterCacheProperties(unsigned char startRegister, unsigned char count);
    ~AmsRegisterCacheProperties();


    QList<unsigned char> itsAvailableRegisterAddresses;
};

#endif //AMS_REGISTER_CACHE_H