/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  
 *
 */


#ifndef _PWM_SIGNALS_H_
#define _PWM_SIGNALS_H_

#define PWM_MAX_SIGNAL_CHANGES 4

class UsbBoxPwm
{
public:
	UsbBoxPwm ( DWORD cycleInTicks ) : itsCycle( cycleInTicks ), itsMax( 0 ) { };
	~UsbBoxPwm ( ) { };

	bool addPoint( DWORD ticks, unsigned char signal, bool isHigh ); 

	unsigned char getNumberOfPoints ( ) const { return itsMax; };

	bool getDuration( unsigned int index, DWORD & ticks, unsigned char & signal ) const;

private:
	UsbBoxPwm( const UsbBoxPwm & ); /* forbid copy constructor */

	const DWORD itsCycle;
	unsigned char itsMax;
	DWORD itsTicks[ PWM_MAX_SIGNAL_CHANGES + 1 ];
	unsigned char itsSignalHigh[ PWM_MAX_SIGNAL_CHANGES ];
	unsigned char itsSignalLow[ PWM_MAX_SIGNAL_CHANGES ];
};

#endif /* _PWM_SIGNALS_H_ */