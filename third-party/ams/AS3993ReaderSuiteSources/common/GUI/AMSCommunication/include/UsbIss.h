/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  class 
 *
 */

#ifndef USB_ISS_H
#define USB_ISS_H

#include <windows.h>

#define USB_ISS_MAX_I2C_TRANSFER_SIZE 16
#define USB_ISS_MAX_SPI_TRANSFER_SIZE 63

#define USB_ISS_TIMEOUT_IN_MS     100
#define USB_ISS_TIMEOUT_STEPS_IN_MS 1
 
class UsbIss
{
public:

	UsbIss( unsigned char comPortNumber = 0xFF );
	~UsbIss();

    void setComPort( unsigned char port );

    void reset( );
    bool open( );
    void close( );

    bool isOpened( ) const { return ( itsHandle != INVALID_HANDLE_VALUE ); };

    /* read out version - zero terminated string */
    const BYTE * version( );

    /* sets the baud rate to the nearest possible one */
    bool setBaudRate( unsigned int baudRate );

    /* sets the i2c mode to the next possible one */
    bool setI2CMode( unsigned int frequencyInHz, bool standardMode );

    /* if ( polarity == 0 ) this means polarity == 0, else polarity == 1*/
    /* if ( phase == 0 ) this means phase == 0, else phase == 1*/
    bool setSpiMode( int polarity, int phase, unsigned int frequencyInHz );

    /* to transmit via i2c only - convenience function */
    bool i2cTx( BYTE deviceAddress, unsigned int sendLen, const BYTE * send, unsigned int timeoutInMs = USB_ISS_TIMEOUT_IN_MS )
        { return i2cTxRx( deviceAddress, sendLen, send, 0, 0, timeoutInMs ); };

    /* to transmit and receive via i2c - if you set receiveLen to 0 it is just an transmit */
    bool i2cTxRx( BYTE deviceAddress, unsigned int sendLen, const BYTE * send, unsigned int receiveLen, BYTE * receive, unsigned int timeoutInMs = USB_ISS_TIMEOUT_IN_MS );

    /* to transmit and receive via spi you need to send as many bytes as you want to receive */
    bool spiTxRx( unsigned int sendLen, const BYTE * send, BYTE * receive, unsigned int timeoutInMs = USB_ISS_TIMEOUT_IN_MS );

protected:
    /* when the i2c slave does not react, the i2c master gets confused, therefore we reset the i2c master in this case */
    bool resetI2C( );

    /* return number of available bytes in COM port if any */
    unsigned int rxAvailable( );
    
    /* clear any received bytes - good before sending a new command */
    void rxClear( );

    /* Write sendLen Bytes to USB-ISS */
    unsigned int tx( unsigned int sendLen, const BYTE * send );

    /* receive with timout in msec*/
    unsigned int rx( unsigned int receiveLen, BYTE * receive, unsigned int timeoutInMs = USB_ISS_TIMEOUT_IN_MS );

    unsigned int txRx( unsigned int sendLen, const BYTE * send, unsigned int receiveLen, BYTE * receive, unsigned int timeoutInMs = USB_ISS_TIMEOUT_IN_MS );

public:
    HANDLE itsHandle;
    unsigned int itsBaudRate;
    BYTE itsI2cMode;
    BYTE itsRx [ USB_ISS_MAX_I2C_TRANSFER_SIZE ];
    unsigned char itsComPort;
};

#endif 
