/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsCom.h
 *
 *  \author M. Arpa
 *
 *  \brief All protocol numbers etc. are here defined.
 */

#ifndef AMS_COM_H
#define AMS_COM_H

#include "ams_stream.h"
#include <QQueue>

/* constants for trace pattern --------------------------------------------------------------- */
#define AMS_COM_TRACE_ALLWAYS              0xFFFFFFFF
#define AMS_COM_TRACE_NEVER                0x00000000
#define AMS_COM_TRACE_FLUSH                0x80000000   /* the flush trace pattern, do not use for normal objects */
#define AMS_COM_TRACE_CTRL_CMD             0x00000001   /* control commands */


/* XML tags and defines ---------------------------------------------------------------------- */

/* xml file information tags */
#define AMS_COM_XML_SESSION             "session"
#define AMS_COM_XML_INFORMATION         "information"
#define AMS_COM_XML_GUI_VERSION         "guiversion"
#define AMS_COM_XML_FW_VERSION          "fwversion"

/* telegram tags */
#define AMS_COM_XML_TAG                 "telegram"
#define AMS_COM_XML_PROTOCOL_ID         "pid"
#define AMS_COM_XML_NUMBER_BASE         16
#define AMS_COM_XML_DIRECTION           "direction"
#define AMS_COM_XML_TRACE_PATTERN       "trace"
#define AMS_COM_XML_APPLICATION_TAG     "application" /* all application specific objects must set this tag */



/* --- as xml direction values we use --------------------- */
#define AMS_COM_XML_WRITE           "write"
#define AMS_COM_XML_READ            "read"
#define AMS_COM_XML_READ_RESULT     "result"
 
#define AMS_COM_XML_NO_DIRECTION    "none"


/* forward declaration ------------------------------------ */

class AmsComStream;
class AmsComObject;
class QXmlStreamReader;
class QString;


/* class declaration -------------------------------------- */

class AmsComXmlPlayer
{
public:
    AmsComXmlPlayer( QXmlStreamReader * xml = 0, AmsComStream * stream = 0 );
    AmsComXmlPlayer( const AmsComXmlPlayer & other ) : itsXml( other.itsXml ), itsStream( other.itsStream ) { };
    ~AmsComXmlPlayer( );

    void reset( );

    void set( QXmlStreamReader * xml, AmsComStream * stream );
    bool playNext( );

private:
    QXmlStreamReader * itsXml;
    AmsComStream * itsStream;
    QQueue<AmsComObject*> itsObjToRead;
};

/* class to clone a specific object derived from class AmsComObject depending on the 
   read xml telegram */
class AmsComXmlReader
{
public:
    /* interprets the opening tag information and others and generates a new
       object of the corresponding type with new-operator */
    static AmsComObject * clone( QXmlStreamReader * xml, const char * & direction );

    AmsComXmlReader( ) { };
    ~AmsComXmlReader( ) { };
    AmsComXmlReader( const AmsComXmlReader & other ) { };


    /* service functions */
    static AmsComObject * clone( QXmlStreamReader * xml, unsigned char protocol );
    static const char * getDirection( const QString & direction );

};



#endif /* AMS_COM_H */
