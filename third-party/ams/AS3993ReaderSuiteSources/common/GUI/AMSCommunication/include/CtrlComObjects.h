/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file CtrlComObjects.h
 *
 *  \author M. Arpa
 *
 *  \brief Classes for control commands.
 */

#ifndef CTRL_COM_OBJECTS_H
#define CTRL_COM_OBJECTS_H

#include "AmsCom.h"
#include "AmsComObject.h"

/* forward declaration ------------------------------------ */

class QxmlStreamReader;
class QXmlStreamWriter;


/* class declaration -------------------------------------- */

/* class to clone an control communication object of a specific type from the information
   found in the xml stream.
 */
class CtrlComXmlReader
{
public:
    /* interprets the opening tag information and others and generates a new
       object of the corresponding type with new-operator */
    static AmsComObject * clone( QXmlStreamReader * xml, unsigned char protocol );

    CtrlComXmlReader( ) { };
    ~CtrlComXmlReader( ) { };
    CtrlComXmlReader( const CtrlComXmlReader & /*other*/ ) { };
};


/* class to reset MCU, peripherals, ... */
class ResetObject : public AmsComObject
{
public:
	/* default c'tor */
	ResetObject ( unsigned char objectsToReset, unsigned int tracePattern = AMS_COM_TRACE_CTRL_CMD ) 
        : AmsComObject( AMS_COM_CTRL_CMD_RESET, 1, 0, 0, tracePattern ), itsObjectsToReset( objectsToReset ) { };

	/* copy c'tor */
	ResetObject ( const ResetObject & other ) : AmsComObject( other ) { };
	
	/* d'tor */
	~ResetObject ( ) { };

    unsigned char objectsToReset( ) { return itsObjectsToReset; };

	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

	virtual bool rxSerialise( unsigned char * /*buffer*/, int /*bufferSize*/, QXmlStreamWriter * /*xml*/ ) { return false; }; 

	virtual bool deserialise( unsigned char * /*buffer*/, int /*bufferSize*/, QXmlStreamWriter * /*xml*/ ) { return false; };

    virtual bool fill( QXmlStreamReader * xml );

protected:
	unsigned char itsObjectsToReset; /* bit mask of objects to reset */
};


/* the class to enter the bootloader code at the mcu */
class EnterBootloaderObject : public AmsComObject
{
public:
    /* default c'tor */
    EnterBootloaderObject ( unsigned int tracePattern = AMS_COM_TRACE_CTRL_CMD ) : AmsComObject( AMS_COM_CTRL_CMD_ENTER_BOOTLOADER, 0, 0, 0, tracePattern ) { };

    /* copy c'tor */
    EnterBootloaderObject ( const EnterBootloaderObject & other ) : AmsComObject( other ) { };

    /* d'tor */
    ~EnterBootloaderObject ( ) { };

    virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

    virtual bool rxSerialise( unsigned char * /*buffer*/, int /*bufferSize*/, QXmlStreamWriter * /*xml*/ ) { return false; }; 

    virtual bool deserialise( unsigned char * /*buffer*/, int /*bufferSize*/, QXmlStreamWriter * /*xml*/ ) { return false; };

    virtual bool fill( QXmlStreamReader * xml );
};

/* the class to read out the firmware version */
class FirmwareNumberObject : public AmsComObject
{
public:
    /* default c'tor */
    FirmwareNumberObject ( unsigned int tracePattern = AMS_COM_TRACE_CTRL_CMD );

	/* copy c'tor */
    FirmwareNumberObject ( const FirmwareNumberObject & other );

    /* d'tor */
    ~FirmwareNumberObject ( ) { };

    virtual bool serialise( unsigned char * /*buffer*/, int /*bufferSize*/, QXmlStreamWriter * /*xml*/ ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );
    
    virtual bool fill( QXmlStreamReader * xml );

    unsigned int get( ) const { return itsVersionNumber; };

protected:
    unsigned int itsVersionNumber;
};

/* the class to read out the firmware description string */
class GetFirmwareInfoObject : public AmsComObject
{
public:
    /* default c'tor */
    GetFirmwareInfoObject ( unsigned int tracePattern = AMS_COM_TRACE_CTRL_CMD );

	/* copy c'tor */
    GetFirmwareInfoObject ( const GetFirmwareInfoObject & other );

    /* d'tor */
    ~GetFirmwareInfoObject ( ) { };

    virtual bool serialise( unsigned char * /*buffer*/, int /*bufferSize*/, QXmlStreamWriter * /*xml*/ ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );
    
    virtual bool fill( QXmlStreamReader * xml );

    unsigned char * get( ) { return itsData; };

protected:
    unsigned char itsData [ AMS_STREAM_SHORT_STRING ]; /* deep copy */
};


/* the class to read and write 16-bit values to defined addresses. Can be used to configure the pic */
class AmsConfigObject : public AmsComObject
{
public:
    /* default c'tor */
    AmsConfigObject ( unsigned short address, unsigned short data = 0, unsigned short mask = 0xFFFF, unsigned int tracePattern = AMS_COM_TRACE_CTRL_CMD );

    /* copy c'tor */
    AmsConfigObject ( const AmsConfigObject & other ) : AmsComObject( other ), itsAddress( other.itsAddress ), itsMask( other.itsMask ), itsData( other.itsData ) { };

    /* d'tor */
    ~AmsConfigObject ( ) { };

    virtual bool serialise( unsigned char * /*buffer*/, int /*bufferSize*/, QXmlStreamWriter * /*xml*/ ); 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * xml );

    /* functions to get values */
    unsigned short address( ) const { return itsAddress; };
    unsigned short data( ) const { return itsData; };
    unsigned short mask( ) const { return itsMask; };

    /* functions to set values */
    void address( unsigned short address ) { itsAddress = address; };
    void data( unsigned short data ) { itsData = itsMask & data; };
    void mask( unsigned short mask ) { itsMask = mask; itsData = itsData & itsMask; };

protected:
    unsigned short itsAddress;
    unsigned short itsMask;
    unsigned short itsData;
};

#endif /* CTRL_COM_OBJECTS_H */
