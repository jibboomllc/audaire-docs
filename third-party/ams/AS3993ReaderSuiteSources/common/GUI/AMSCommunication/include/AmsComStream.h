/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComStream.h
 *
 *  \author M. Arpa
 *
 *  \brief  This class provides a communication stream that can transmit and
 * receive objects that are derived from class AmsComObject. The communication
 * stream itself takes an instance of class AmsComDriver for the transport of the
 * AmsComObjects.
 */

#ifndef AMS_COM_STREAM_H
#define AMS_COM_STREAM_H

/* ------------------------------ includes -------------------------------------------- */

#include "AmsCom.h"
#include "AmsComObject.h"
#include "AmsComDriver.h"
#include "AmsComPolicy.h"
#include <QMutex>


/* ------------------------------ forward declaration --------------------------------- */

class QXmlStreamWriter;


/* ------------------------------ defines --------------------------------------------- */


/* use this shortcut for flushing, that just makes the written code shorter. Now you can
   write:
     stream << AMS_FLUSH;
   instead 
     stream << AmsComStream::theFlush;
*/
#define AMS_FLUSH AmsComStream::theFlush;



/* ------------------------------ class declarations ---------------------------------- */

/* Class that implements a counting mutex. The standard QT Mutex did not provide the
   feature to do an unlock of all locks. */
class AmsComMutex 
{
public:
	/* c'tor */
	AmsComMutex( );

	/* d'tor */
	~AmsComMutex( ); 

	/* to lock the stream use this function */
	void lock( );

	/* to unlock the stream use this function */
	bool unlock( );

private:
	/* forbid the copy c'tor */
	AmsComMutex( const AmsComMutex & other );

private:
	/* counts the number of times this thread has called lock */
	unsigned int itsCounter;

	/* the lock itself */
	QMutex itsLock;
};

/* Class that implements the communication stream */
class AmsComStream
{
	
public:
	/* call operator<< with this object as parameter, than an immediate send will
	   be done */
	static AmsFlushObject theFlush;  

	/* constants to be used for flushing policy */
	static const LateFlushPolicy theLateFlushPolicy;
	static const ImmediateFlushPolicy theImmediateFlushPolicy;

 public:
	/* c'tor */
	AmsComStream 
	  ( AmsComDriver & driver
	  , const AmsComPolicy & policy = theImmediateFlushPolicy
	  );

	/* d'tor */
	~AmsComStream( );

	/* close the communication channel, and free all locks of this thread */
	void reset( );

	/* change how the sending of buffers shall be done */
	const AmsComPolicy * setPolicy ( const AmsComPolicy & policy );

	/* open the communication connection */
	bool open( ); 

	/* is the communication connection opened */
	bool isOpened ( ) const { return itsDriver.isOpened(); };

	/* close the communication connection */
	bool close( ); 

	/* get the last error that occured and clear it */
	int lastError( ) { int temp = itsLastError | itsDriver.lastError(); itsLastError = 0; return temp; };

    /* function returns true if firmware and pc talk the same protocols, 
       otherwise the function returns false. If the firmware and GUI do not share the same 
       protocol version, the bootloader of the firmware can be enabled to download a newer
       firmware */
    bool isProtocolVersionCompatible( bool enableBootloaderIfNotCompatible = true ) { return itsDriver.isProtocolVersionCompatible( enableBootloaderIfNotCompatible ); };

    /* function returns true if firmware and pc can talk and have different protocols. 
       If the two cannot talk (communication is disconnected) or can talk and use 
       the same protocols the function returns false. If they are incompatible
       the function can enable the bootloader.
       */
    bool isProtocolVersionIncompatible( bool enableBootloaderIfNotCompatible = true ) 
    { 
        bool result = ! itsDriver.isProtocolVersionCompatible( enableBootloaderIfNotCompatible ); 
        int error = lastError(); /* get and clear any error */
        if ( error != 0 )
        {
            return false; /* communication failed, do not know compatibility, so return false */
        }
        return result;
    };

	/* streaming operations -------------------------------------------------- */

	/* use this operator to prepare an object for transmit - depending on the set policy
	   the object is either sent immediately or later on */
	AmsComStream & operator<< ( AmsComObject & toTx );

	/* all not yet sent com objects are immediatly sent after calling this function 
	   - no matter what your current policy is */
	AmsComStream & operator<< ( AmsFlushObject & flush );

       /* this function does the read back of all objects previously requested with operator>>
       If you use LateFlushPolicy you need to call the flush manually. */
	AmsComStream & operator>> ( AmsFlushObject & flush );

	/* use this operator to receive an object. The object is only valid after it has
       been received (i.e. after the next call of operator<< with the AmsFlushObject
       has returned. */
	AmsComStream & operator>> ( AmsComObject & toRx );


	/* alternate functions ( instead of the streaming operations ) ----------- */

	/* this function prepares an object for transmit - depending on the set policy
	   the object is either sent immediately or later */
	void tx( AmsComObject & toTx ) { *this << toTx; };

	/* this function receives an object - if you have choosen split-read for the
       object you have to call this function twice to really receive the object. 
	   You can ask each object of type AmsComObject if it is valid, by calling
       the valid() function. If this function returns true the object is received. */
	void rx( AmsComObject & toRx ) { *this >> toRx; };

	/* to initiate a flush you can call also this function */
	void flush( ) { *this << theFlush; };


private:
	/* forbid the copy c'tor */
	AmsComStream ( const AmsComStream & other ); 

	/* for split object, the receiving is done in two steps. this is the first 
           part */
	void rxPartOne( AmsComObject & toRx );

	/* for split object, the receiving is done in two steps. this is the 2nd 
           part */
	void rxPartTwo( AmsComObject & toRx );

private:
	/* if this is 0 no error occured. Values > 0 are warnings, <0 are errors */
	int itsLastError;

	/* use this driver to communicate with the io */
	AmsComDriver & itsDriver;

	/* the selected policy */
	const AmsComPolicy * itsPolicy;

	/* pointer to the beginning of the buffer. I.e. the point where this
           class inserts the stream header bytes */
	unsigned char * const itsHook;

	/* the size of the buffer in bytes. this is the size given in the c'tor */
	const int itsBufferSize;

	/* simply 1 buffer is used for transmit and for receive */ 
	unsigned char * const itsBuffer;

	/* a lock to make sure no function call is interrupted by another thread 
	   and to disallow to any function calls during split reads */
	AmsComMutex itsLock;

    /* a "queue" to hold the objects for reading: insert at the tail, remove from the head */
    AmsComObject * itsReadQueueHead;
    AmsComObject * itsReadQueueTail;
};

#endif /* AMS_COM_STREAM_H */
