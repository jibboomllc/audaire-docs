/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file HidComDriver.h
 *
 *  \author M. Arpa
 *
 *  \brief  Communication class for HID streaming communication 
 */

#ifndef HID_COM_DRIVER_H
#define HID_COM_DRIVER_H

#include "AmsComDriver.h"

#define AMS_VID 0x1325

/* forward declaration of Microsoft Usb Hid class */
class CHIDDevice;

class HidComDriver : public AmsComDriver  
{
public:
        /* c'tor */
	HidComDriver 
	  ( unsigned short pid
	  , unsigned short vid = AMS_VID
	  );

    HidComDriver
      ( const unsigned short * pidList
      , unsigned short numberOfPidsInList
      , unsigned short vid = AMS_VID
      );

	/* d'tor */
	~HidComDriver ( );

	/* opens the communication channel (if not yet opened) 
	   returns true if channel was successfully opened */
	virtual bool open ( );

	/* closes the communication channel (if not yet closed) 
	   returns true if channel was successfully closed */
	virtual bool close ( );

	/* initiates an immediate transmit */
	virtual int flush ( );

	/* send data via communication channel */
	virtual int txBuffer ( unsigned char * buffer, int size );

	/* receive data via communication channel */
	virtual int rxBuffer ( unsigned char * buffer, int size );

    virtual bool isProtocolVersionCompatible( bool enableBootloaderIfNotCompatible ); 

    /* set timeouts for USB reports, values are applied when open() is called. */
    void setTimeouts ( unsigned int rxTimeout, unsigned int txTimeout );


protected:
    virtual void clearError( );

    virtual void clearFirmwareError( );

    bool open( unsigned short pid );

private:
	/* service function for transmit */
	void tx ( );

	/* service function for receive */
	void rx ( );

	/* forbid the copy c'tor */
	HidComDriver ( const HidComDriver & other ); 

protected:
	CHIDDevice * itsHid;
	const unsigned short itsVid;
    const unsigned short itsNumberOfPidsInList;
    unsigned short * itsPidList;

	/* buffer and ad-ons for transmit */
	unsigned char * const itsTxHookReal;
	unsigned char * const itsTxHook;
	int itsToTx;
	unsigned char * const itsTxBuffer;

	/* buffer and ad-ons for transmit */
	unsigned char * const itsRxHookReal;
	unsigned char * const itsRxHook;
	int itsRxed;
	unsigned char * const itsRxBuffer;
	
	/* the transaction identifier */
	unsigned char itsTid;

    /* timeouts for USB reports */
    unsigned int itsRxTimeout, itsTxTimeout;
};

#endif /* HID_COM_DRIVER_H */
