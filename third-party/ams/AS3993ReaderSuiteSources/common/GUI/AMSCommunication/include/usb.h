/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   ams EZ-USB Firmware
 *      $Revision: 2114 $
 *      LANGUAGE:  C
 */

/*! \file   spi.h
 *
 *  \author M. Arpa, C. Wurzinger
 *
 *  \brief  defines and macros for interfacing with the PC
 */


#ifndef _USB_H_
#define _USB_H_


#include "taskids.h"


#ifndef MIN
#define MIN(a,b)   ((a) < (b) ? (a) : (b))
#endif


#ifndef MAX
#define MAX(a,b)   ((a) > (b) ? (a) : (b))
#endif


/* from usb-box verion 2.65 on we do no longer support PORTA -> only the led is connected to it */
/* typedef enum { PORTA = 0, PORTB = 1, PORTC = 2} Port;  */
typedef enum { PORTB = 1, PORTC = 2 } Port;

typedef enum { PLAIN_I2C = 0, OPTO_I2C = 1, MULTI_LEVEL_I2C = 2 } Protocol;

typedef enum { PLAIN_SPI = 2 } TrimmerProtocol;


#define PIPE2IN (ULONG)  1
#define PIPE2OUT (ULONG) 2


#define USB_STATE_PROCESSED    0x01
#define USB_STATE_NEW          0x00


#define USB_TASK(buf)          ((buf)[0])
#define USB_STATE(buf)         ((buf)[1])

#define USB_SERIAL_WRITE       0x00
#define USB_SERIAL_READ        0x01
#define USB_SH_DEV_ADDR(buf)   ((buf)[2])
#define USB_SH_R_NW(buf)       ((buf)[3])
#define USB_SH_START_ADDR(buf) ((buf)[4])
#define USB_SH_NUM_BYTES(buf)  ((buf)[5])
#define USB_SERIAL_DATA(buf)   ((buf)+6)

#define USB_FW_ID_DATA(buf)    ((buf)+2)

#define USB_DBG_DATA(buf)      ((buf)+2)

#define USB_ADC_CHANNEL(buf)   ((buf)[2])
#define USB_ADC_DATA(buf)      ((buf)+3)

#define USB_PIO_WRITE          0x00
#define USB_PIO_READ           0x01
#define USB_PIO_SET_IN         0x02
#define USB_PIO_SET_OUT        0x03
#define USB_PIO_GET_DIR        0x04
#define USB_PIO_TSK(buf)       ((buf)[2])
#define USB_PIO_PORT(buf)      ((buf)[3])
#define USB_PIO_MASK(buf)      ((buf)[4])
#define USB_PIO_DATA(buf)      ((buf)[5])

#define USB_OTP_NUM_BITS_L(buf)   ((buf)[2])
#define USB_OTP_NUM_BITS_H(buf)   ((buf)[3])
#define USB_OTP_READ_DO_LOAD(buf) ((buf)[4])

//scratchpad definitions
#define USB_SP_START_L(buf)    ((buf)[2])
#define USB_SP_START_H(buf)    ((buf)[3])
#define USB_SP_NUM_BYTES(buf)  ((buf)[4])
#define USB_SP_DATA(buf)       ((buf)+5)
#define USB_SP_MAX_DATA        (USB_PACKET_SIZE - 6)

/* plain spi */
#define USB_PLAIN_SPI_MOSI_NBYTES(buf) ((buf)[2]) /* number of bytes to send over mosi */
#define USB_PLAIN_SPI_MISO_NBYTES(buf) ((buf)[3]) /* last <number of bytes> received over miso that are reported back over usb */
#define USB_PLAIN_SPI_DATA_PTR(buf)    ((buf)+4) /* user data pointer */
#define USB_PLAIN_SPI_HEADER_SIZE        4 
#define USB_PLAIN_SPI_MAX_DATA         ( USB_PACKET_SIZE - USB_PLAIN_SPI_HEADER_SIZE )

/* plain i2c */
#define USB_PLAIN_I2C_MOSI_NBYTES(buf) ((buf)[2]) /* number of bytes to send over mosi */
#define USB_PLAIN_I2C_MISO_NBYTES(buf) ((buf)[3]) /* last <number of bytes> received over miso that are reported back over usb */
#define USB_PLAIN_I2C_DATA_PTR(buf)    ((buf)+4) /* user data pointer */
#define USB_PLAIN_I2C_HEADER_SIZE       4         /* number of bytes we need for the header */
#define USB_PLAIN_I2C_MAX_DATA         (USB_PACKET_SIZE - USB_PLAIN_I2C_HEADER_SIZE) /* maximum number of bytes to be sent with plain i2c in usb packet */

/* vsync */
#define USB_VSYNC_RELOAD_HIGH(buf) ((buf)[2]) /* high byte of the reload value for the vsync signal */
#define USB_VSYNC_RELOAD_LOW(buf)  ((buf)[3]) /* low byte of reload value */
#define USB_VSYNC_REPEAT_RELOAD(buf) ((buf)[4]) /* how often to repeat the reloading before changing the signal polarity, 0 = off */

/* pwm - on the 8051 it saves some code when we do use 4 instances of the macros compared to a
     single macro that uses calcualted index */
#define USB_PWM_NUM_RELOAD_VALUES(buf)   ((buf)[2]) /* how many reload values we get */

#define USB_PWM_RELOAD_HIGH_0(buf)   ((buf)[3]) /* get reload high byte for reload value (index) */
#define USB_PWM_RELOAD_LOW_0(buf)    ((buf)[4]) /* get reload  low byte for reload value (index) */
#define USB_PWM_RELOAD_SIGNAL_0(buf) ((buf)[5]) /* get reload signal as a bitmask for (index) */ 

#define USB_PWM_RELOAD_HIGH_1(buf)   ((buf)[6]) /* get reload high byte for reload value (index) */
#define USB_PWM_RELOAD_LOW_1(buf)    ((buf)[7]) /* get reload  low byte for reload value (index) */
#define USB_PWM_RELOAD_SIGNAL_1(buf) ((buf)[8]) /* get reload signal as a bitmask for (index) */  

#define USB_PWM_RELOAD_HIGH_2(buf)   ((buf)[9]) /* get reload high byte for reload value (index) */
#define USB_PWM_RELOAD_LOW_2(buf)    ((buf)[10]) /* get reload  low byte for reload value (index) */
#define USB_PWM_RELOAD_SIGNAL_2(buf) ((buf)[11]) /* get reload signal as a bitmask for (index) */  

#define USB_PWM_RELOAD_HIGH_3(buf)   ((buf)[12]) /* get reload high byte for reload value (index) */
#define USB_PWM_RELOAD_LOW_3(buf)    ((buf)[13]) /* get reload  low byte for reload value (index) */
#define USB_PWM_RELOAD_SIGNAL_3(buf) ((buf)[14]) /* get reload signal as a bitmask for (index) */ 

#define USB_PWM_SIGNAL_PWM           0x02
#define USB_PWM_SIGNAL_3D_PWM12      0x04
#define USB_PWM_SIGNAL_3D_PWM34      0x01

#define PWM_SWITCH_OVERHEAD_8051     32  /* how many timer ticks we need to switch the pwm signals and wind up timer */

/* switch protocol */
#define USB_SWITCH_GET_PROTOCOL(buf) ((buf)[2])
#define USB_SWITCH_GET_SPIKE_SUP_TIME(buf) ((buf)[3])
#define USB_SWITCH_GET_EN_TIME(buf)        ((buf)[4])

/* trimmer */
#define USB_TRIMMER_GET_PROTOCOL(buf) ((buf)[2])
#define USB_TRIMMER_NBYTES(buf)       ((buf)[3]) /* number of bytes to send over mosi */
#define USB_TRIMMER_HIGH_TIME(buf)    ((buf)[4]) /* number of microseconds for high clock */
#define USB_TRIMMER_LOW_TIME(buf)     ((buf)[5]) /* number of microseconds for low clock */
#define USB_TRIMMER_DATA_PTR(buf)     ((buf)+6)  /* user data pointer */
#define USB_TRIMMER_HEADER_SIZE         6
#define USB_TRIMMER_MAX_DATA          ( USB_PACKET_SIZE - USB_TRIMMER_HEADER_SIZE )

#define TRIMMER_MINIMUM_HIGH_TIME_8051     5 /* minimum high time for trimming we can generate with the 8051 */
#define TRIMMER_MINIMUM_LOW_TIME_8051      7 /* minimum high time for trimming we can generate with the 8051 */


/* as1121 spi */
/* mode = 1 = dot correction, mode = 0 = pwm control */
/* ld = 1 = latch ld, ld = 0 = do not latch ld yet */
#define USB_AS1121_MODE(buf)         ((buf)[2]) /* 1 = dot correction, 0 = pwm control */
#define USB_AS1121_GCLK(buf)         ((buf)[3]) /* 0 = off, otherwise freq indicator for gclk */
#define USB_AS1121_AUTO_RUN(buf)     ((buf)[4]) /* 0 = run only once, 1 = repeat many times */
#define USB_AS1121_LD(buf)           ((buf)[5]) /* 0 = no latch of ld, <>0 = latch ld */
#define USB_AS1121_DATA_PTR(buf)     ((buf)+6)  /* user data pointer */


/* dimm */

/* number of times we repeat the same signal */
#define DIMM_REPEAT					 5

/* number of dimm-packets packed in 1 usb-packet. One Dimm Packet has a maximum size of 4*4 = 16 bytes. 
   The maximum of 4 dimm-packets has the size 4*16 = 48 bytes. Plus a header of: byte-0 = task-id, 
   byte-1 = "status" = always 0, byte-2, byte-3 are protocol reserved = header-size = 4 -> total
   size maximal 52 bytes which is less than the allowed 64.
   One dimm packet consists of num-reload times the set: { num-reloads, reload-high, reload-low, signal }. 
   E.g for num-reload=3 we get:
   { { 3, reload-high, reload-low, signal }, { 3, reload-high, ... }, { 3, reload-high, ... } } 
    */
#define DIMM_PACKETS 				4 

#define USB_DIMM_DATA(buf)			((buf)+4)	/* buf must be a byte-ptr */
#define USB_DIMM_INC(buf)			((buf) += 4)
#define USB_DIMM_DEC(buf)           ((buf) -= (*(buf) << 2) )

#define USB_DIMM_NUM_RELOAD(buf)	((buf)[0])
#define	USB_DIMM_RELOAD_HIGH(buf)	((buf)[1])
#define USB_DIMM_RELOAD_LOW(buf)	((buf)[2])
#define USB_DIMM_SIGNAL(buf)		((buf)[3])


#define USB_PACKET_SIZE        64
#define USB_MAX_SERIAL_SIZE    (USB_PACKET_SIZE - 6)

#endif /* _USB_H_ */
