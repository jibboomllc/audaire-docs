/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  
 *
 */


#ifndef _USB_BOX_DIMMING_H_
#define _USB_BOX_DIMMING_H_

#define USB_BOX_DIMMING_FREQUENCY 220 /* Hertz*/

class UsbBoxDimming
{
	static const char theSkip [ 6 ];
	typedef struct _dimming{
		short start;			/* dimming lower value = dimming starts at this value */
		short stop;				/* dimming upper value = dimming stops at this value */
		short currValue;		/* the actual dimm value */
		signed char	skip;		/* how many elements we skip in the calculation */
		unsigned char signal;	/*  the pattern that represents the signal e.g. 0x04 = bit 3 */
	} dimming;
		
	static short upNextValue( short value, short limit, signed char skip );
	static short downNextValue( short value, short limit, signed char skip );
	static short roundToTicks( short value );

	static const int theMs [ 7 ];		/* the index in the ms-Combo box is the index into this table also */
	static const int theValues [ 21 ]; /* the values corresponding to 0%, 5% ... 100% */

public:
	static int convertToValue( int percent );

public:
	UsbBoxDimming ( unsigned char signal0, unsigned char signal1 );
	~UsbBoxDimming ( );

	bool startOrStop( bool isFirstSignal, int startInPercent, int stopInPercent, int skipIndex );

	/* fill the packet with the next dimm packets and return the size of bytes used */
	int generatePacket( char * buffer );

private:
	UsbBoxDimming( const UsbBoxDimming & ); /* forbid copy constructor */
	
private:
	dimming itsDimm[ 2 ];
	DWORD itsDutyCycle;
	int itsState;			/* the state is either bottom, up, middle, up2nd, top, down2nd, middle2nd or down */
	int itsCounter;			/* how many times we send the same packet to generate the top/bottom */
};

#endif /* _PWM_SIGNALS_H_ */