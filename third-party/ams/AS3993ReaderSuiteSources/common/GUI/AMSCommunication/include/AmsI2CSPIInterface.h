/*
 *****************************************************************************
 * Copyright @ 2013 by austriamicrosystems AG                                *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       * 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams interpreter for i2c and spi commands
 *      $Revision: $
 *      LANGUAGE: QT C++
 */
/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  Class to interpret text strings into i2c command and spi commands.
 *
 */

#ifndef AMS_I2C_SPI_INTERFACE_H
#define AMS_I2C_SPI_INTERFACE_H

#include "AmsI2CSPIInterpreter.h"

/* interface class to have a common interface for e.g. AmsComStreamWrapper and AmsUsbIssWrapper */
class AmsI2CSPIInterface
{
public:
    /* provide default c'tors and d'tors */
    AmsI2CSPIInterface ( ) 
        : itsIsI2CConfigured( false ), itsIsSPIConfigured( false )
        , itsI2CFrequencyInHz( 100000UL ), itsI2CUseDefault( false )
        , itsSPIFrequencyInHz( 1000000UL ), itsSPIPolarity( 0 ), itsSPIPhase( 0 )
        { };
    ~AmsI2CSPIInterface ( ) { };
    AmsI2CSPIInterface ( const AmsI2CSPIInterface & other ) 
        : itsIsI2CConfigured( other.itsIsI2CConfigured ), itsIsSPIConfigured( other.itsIsSPIConfigured ) 
        , itsI2CFrequencyInHz( other.itsI2CFrequencyInHz ), itsI2CUseDefault( other.itsI2CUseDefault )
        , itsSPIFrequencyInHz( other.itsSPIFrequencyInHz ), itsSPIPolarity( other.itsSPIPolarity ), itsSPIPhase( other.itsSPIPhase ) 
        { };
 
    virtual bool isOpened( ) const = 0;

    /* function to reset the interface - i.e. the stream is also closed, you must re-open it afterwards manually */
    virtual void reset( ) = 0;

    /* function to open the interface */
    virtual bool open( ) = 0;

    /* function to close the interface */
    virtual void close( )= 0;

    /* function to configure i2c for a specific frequency (range somewhat 20000 - 1000000) */
    virtual bool i2cConfigure( unsigned int frequencyInHz, bool useDefaultI2C = true /* either can be 1=default or 2=alternate I2C block on PIC24 */ ) = 0;

    /* function to configure spi for a specific frequency (range somewhat 250000 - 8000000), for polarity and phase */
    virtual bool spiConfigure( unsigned int frequencyInHz, int polarity, int phase ) = 0;


    /* function to transmit / and receive the given number of bytes in i2c format. The device address is the 7-bit address. If you
       just want to transmit set the receiveSize to 0. The function returns true if it was successful (i.e. an I2C Ack has been
       received for transmit and data has been read for receive */
    virtual bool i2cTxRx( unsigned char deviceAddress, unsigned int sendLen, const unsigned char * send, unsigned int receiveLen, unsigned char * receive ) = 0;
    
    /* function to transmit / and receive the given number of bytes in SPI format. The function receives as many
       bytes as have been sent. The function returns true if it was successful. */
    virtual bool spiTxRx( unsigned int sendLen, const unsigned char * send, unsigned char * receive ) = 0;

protected:
    bool itsIsI2CConfigured;
    bool itsIsSPIConfigured;
    
    unsigned int itsI2CFrequencyInHz;
    bool itsI2CUseDefault; /* default HW I2C block on PIC is number 1, number 2 is alternate */
    unsigned int itsSPIFrequencyInHz;
    int itsSPIPhase;
    int itsSPIPolarity;

};

#endif /* AMS_I2CSPI_INTERFACE_H */
