/*
 * AMSDeviceChangeDetector.h
 *
 *  Created on: 25.11.2008
 *      Author: stefan.detter
 */

#ifndef AMSDeviceChangeDetector_H_
#define AMSDeviceChangeDetector_H_

#include <QObject>
#include <QAbstractEventDispatcher>
#include <QTimer>
#include <QMainWindow>

#include <windows.h>

#ifdef QrfeDEVICEDETECTOR_DEBUG
#include <QrfeTrace.h>
#endif

class AMSDeviceChangeDetector: public QWidget
#ifdef QrfeDEVICEDETECTOR_DEBUG
, QrfeTraceModule
#endif
{
Q_OBJECT
public:
	AMSDeviceChangeDetector ()	;
~	AMSDeviceChangeDetector ( );

#if QT_VERSION < 0x050000
    virtual bool winEvent ( MSG * msg, long * result );
#else
    virtual bool nativeEvent (const QByteArray &eventType, void * message, long * result );
#endif
	
	bool registerNotification();
	bool unregisterNotification();

	signals:
	void usbDeviceAttached(QString devicePath, quint16 vendorID, quint16 productID);
	void usbDeviceRemoved(QString devicePath, quint16 vendorID, quint16 productID);

	void hidDeviceAttached(QString devicePath, quint16 vendorID, quint16 productID);
	void hidDeviceRemoved(QString devicePath, quint16 vendorID, quint16 productID);

private:

	HANDLE m_notifyDeviceInterface_USB;
	HANDLE m_notifyDeviceInterface_HID;
};

#endif /* AMSDeviceChangeDetector_H_ */
