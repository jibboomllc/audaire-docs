/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComPolicy.h
 *
 *  \author M. Arpa
 *
 *  \brief  Streaming communication policies that define when to flush com objects.
 */

#ifndef AMS_COM_POLICY_H
#define AMS_COM_POLICY_H


/* this class implements a policy for the streaming class. */
class AmsComPolicy
{
public:
        /* c'tor */
	AmsComPolicy ( ) { };
	
	/* copy c'tor */
	AmsComPolicy ( const AmsComPolicy & /*other*/ ) { };

	/* d'tor */
	~AmsComPolicy ( ) { };

	/* this class always does immediate flashing. */
	virtual bool doFlush ( ) const = 0; 
};

class ImmediateFlushPolicy : public AmsComPolicy
{
public:
    /* c'tor */
    ImmediateFlushPolicy ( ) : AmsComPolicy( ) { };
	
	/* copy c'tor */
	ImmediateFlushPolicy ( const ImmediateFlushPolicy & other ) : AmsComPolicy( other ) { };

	/* d'tor */
	~ImmediateFlushPolicy ( ) { };

	/* this class always does immediate flushing. */
	virtual bool doFlush ( ) const { return true; }
};
  

/* WARNING: if you use LateFlushPolicy you must do a manual flush for objects
   that you want to read but are not yet valid. I.e. any object for read 
   becomes only valid after it is flushed. If you delete any object that is
   not yet valid, this will raise an exception because the object will be
   accessed from AmsComStream when the data arrives. */
class LateFlushPolicy : public AmsComPolicy
{
public:
    /* c'tor */
    LateFlushPolicy ( ) : AmsComPolicy( ) { };
	
	/* copy c'tor */
	LateFlushPolicy ( const LateFlushPolicy & other ) : AmsComPolicy( other ) { };

	/* d'tor */
	~LateFlushPolicy ( ) { };

	/* this class never does automatic flushing. The user must call an explicit 
           flush on objects */
	virtual bool doFlush ( ) const { return false; };
};

#endif /* AMS_COM_POLICY_H */


