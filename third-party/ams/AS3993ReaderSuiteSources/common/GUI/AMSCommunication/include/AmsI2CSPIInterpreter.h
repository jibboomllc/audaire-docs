/*
 *****************************************************************************
 * Copyright @ 2013 by austriamicrosystems AG                                *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       * 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams interpreter for i2c and spi commands
 *      $Revision: $
 *      LANGUAGE: QT C++
 */
/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  Class to interpret text strings into i2c command and spi commands.
 *
 */

#ifndef AMS_I2C_SPI_INTERPRETER_H
#define AMS_I2C_SPI_INTERPRETER_H

#include <QString>

/* text patterns for an valid i2c string */
#define I2C_START           "S"
#define I2C_REPEATED_START  "Sr"
#define I2C_STOP            "P"
#define I2C_READ            "R"
#define I2C_WRITE           "W"
#define I2C_ACK             "A"
#define I2C_NAK             "N"
    /* command structure is as follows:
       S devAddr W regAddr Value0 Value1 ... P - use this to write n bytes
       S devAddr W regAddr ... Sr devAddr R A A A ... N P - use this to read n bytes, the number is defined by the number of A for ack you add + 1 for a Nak 
       */
#define I2C_SEPERATOR " -> "



/* text patterns for the spi interpreter */
#define SPI_SEPERATOR " -> "

class AmsI2CSPIInterpreter 
{

public:
	AmsI2CSPIInterpreter ( );
	~AmsI2CSPIInterpreter ( );

    /* function reads in an i2c string list and transforms this into an i2c command sequence (without device address included) */
    static bool interpretI2CCommand( const QString & command, unsigned char & deviceAddress, unsigned char & txSize, unsigned char * tx, unsigned char & rxSize );

    /* this function takes the command string from the above function, and inserts the read in bytes at the positions of the I2C_ACK and I2C_NAK */
    static void fillInI2CResult ( const QString & command, QString & result, unsigned char rxSize, unsigned char * rx );

    /* function reads in an spi string list and transforms this into an SPI command sequence, note that the spi command always will receive as many bytes
       as has been transmitted */
    static bool interpretSPICommand( const QString & command, unsigned char & txSize, unsigned char * tx );

    /* this function takes the command string from the above function, and inserts the read in bytes at the positions of the send bytes */
    static void fillInSPIResult ( const QString & command, QString & result, unsigned char rxSize, unsigned char * rx );


protected:
};


#endif /* AMS_I2CSPI_INTERPRETER_H */
