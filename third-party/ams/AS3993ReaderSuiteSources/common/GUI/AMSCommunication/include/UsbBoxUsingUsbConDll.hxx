/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  Communication class for USB Box using usbcon.dll.
 *          See also UsbBoxUsingCyApi class.
 *
 *  This controls the communication to the USB box used in the
 *  COMMS BU. The USB Box is based on a Cypress AN2131 (8051 compatible)
 *  controller. Actual USB access is handled via usbcon.dll.
 */

#ifndef USB_BOX_USING_USB_CON_DLL_H
#define USB_BOX_USING_USB_CON_DLL_H

#include "USBBoxCommunication.hxx"
#include <QString>
#include <QByteArray>
#include "usb.h"
#include "UsbBox.hxx"

typedef UsbError (*AMS_SWI_Write_Address) (unsigned char outputPinSelect, unsigned char registerAddress, unsigned char value ,unsigned char delay); 

class UsbBoxUsingUsbConDll : public UsbBox
{

public:
	static bool isDriverAvailable ( );

	UsbBoxUsingUsbConDll( );
	~UsbBoxUsingUsbConDll();

	virtual UsbError blkWriteFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes) ;
	virtual UsbError blkReadFn(unsigned char start_addr,unsigned char* values,unsigned char num_bytes) ;
	virtual UsbError getPortDirectionFn(Port io_port,unsigned char* direction) ;
	virtual UsbError readADCFn(unsigned char channel,unsigned* value) ;
	virtual UsbError readPortFn(unsigned char *data, int ioPort, unsigned char mask) ;
	virtual UsbError sendPacketFn(char *in,int in_length, const char *out, int out_length) ;
	virtual UsbError setPortAsInputFn(int ioPort, unsigned char mask) ;
	virtual UsbError setPortAsOutputFn(int ioPort, unsigned char mask);
	virtual UsbError setInterfaceFn(Interface iface) ;
	virtual bool     usbIsConnectedFn() ;
	virtual UsbError readFirmwareIdFn(unsigned char * str) ;
	virtual UsbError usbDebugMsgFn(unsigned char* str) ;
	virtual UsbError usbConnectFn(void) ;
	virtual UsbError usbDisconnectFn(void) ;
	virtual UsbError readByteFn(unsigned char registerAddress, unsigned char* registerValue) ;
	virtual UsbError setI2CDevAddrFn(unsigned char registerAddress) ;
	virtual UsbError writePortFn(unsigned char data, int ioPort, unsigned char mask) ; 
	virtual UsbError writeByteFn(unsigned char registerAddress, unsigned char registerValue) ;

	/* firmware load */
	virtual bool openDriver ( );
	virtual bool closeDriver ( );
	virtual bool control8051 ( bool reset8051 );
	virtual bool download ( bool toEeprom, char * buffer, int startAddress, int size );
	virtual bool upload ( char * buffer, short startAddress, short size, DWORD & numberBytes );

protected:
	HINSTANCE				  hinstLib;
	AMSusbConnect			  usbConnect;
	AMSusbDisconnect		  usbDisconnect;
	AMSsetPortAsInput		  setPortAsInput;
	AMSsetPortAsOutput		  setPortAsOutput;
	AMSusbSetI2CDevAddr		  setI2CDeviceAddressFunction;
	AMSwritePort			  writePort;
	AMSreadPort				  readPort;
    AMSusbReadByte			  readByteFunction;
    AMSusbWriteByte			  writeByteFunction;
	AMSusbFirmwareID		  readFirmwareId;
	AMSusbIsConnected         usbIsConnected;
	AMSusbFirmwareID          usbFirmwareID;
	AMSusbDebugMsg            usbDebugMsg;
	AMSsetInterface           setInterface;
	AMSwriteByte              writeByte;
	AMSblkWrite               blkWrite;
	AMSblkRead                blkRead;
	AMSreadADC                readADC;
	AMSgetPortDirection       getPortDirection;
	AMSsendPacket             sendPacketDLL;
	AMS_SWI_Write_Address	  singleWireWrite;

	HANDLE itsUsbDriver;
};

#endif 
