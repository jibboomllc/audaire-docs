/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsFirmwareCheck.h
 *
 *  \author M. Arpa
 *
 *  \brief  This class provides a means to check if your GUI and firmware are
 * compatible according to ams firmware compatible rules.
 */

#ifndef AMS_FIRMWARE_CHECK_H
#define AMS_FIRMWARE_CHECK_H

/* ------------------------------ includes -------------------------------------------- */

#include <QString>

/* ------------------------------ defines --------------------------------------------- */



/* ------------------------------ class declarations ---------------------------------- */

/* Class that implements the comptibility checks */
class AmsFirmwareCheck 
{

public:
	/* checks if the FW version number stored in the GUI is compatible with
	   the FW version number reported by the FW itself. 
	   The result string can be displayed in a dialog window if result is false*/
	static bool isCompatible( const char * guiStoredFwVersion, unsigned int fwVersion, QString & result );

	/* checks if the FW version number stored in the GUI is compatible with
	   the FW version number reported by the FW itself 
   	   The result string can be displayed in a dialog window if result is false*/
	static bool isCompatible( const QString & guiStoredFwVersion, const QString & fwVersion, QString & result );

	/* checks if the FW version number stored in the GUI is compatible with
	   the FW version number reported by the FW itself 
   	   The result string can be displayed in a dialog window if result is false*/
	static bool isCompatible( const QString & guiStoredFwVersion, unsigned int fwVersion, QString & result );

	/* checks if the FW version number stored in the GUI is compatible with
	   the FW version number reported by the FW itself 
   	   The result string can be displayed in a dialog window if result is false*/
	static bool isCompatible( unsigned int guiStoredFwVersion, unsigned int fwVersion, QString & result );

	/* checks if the FW version number stored in the GUI is compatible with
	   the FW version number reported by the FW itself. 
	   The result string can be displayed in a dialog window if result is false*/
	static bool isCompatible( const char * guiStoredFwVersion, const char * fwVersion, QString & result );


	/* function to convert an unsigned integer version number to a qstring X.Y.Z */
	static void AmsFirmwareCheck::convert( unsigned int fromVersion, QString & toVersion );

	/* function to convert a qstring version number X.Y.Z to an unsigned integer */
	static bool AmsFirmwareCheck::convert( const QString & fromVersion, unsigned int & toVersion );

	/* function to convert a qstring version number X.Y.Z to an unsigned integer */
	static unsigned int AmsFirmwareCheck::convert( const QString & fromVersion, bool & success );
public:
	/* default c'tor */
	AmsFirmwareCheck( ) { };

	/* copy c'tor */
	AmsFirmwareCheck( const AmsFirmwareCheck & /*other*/ ) { };

	/* d'tor */
	~AmsFirmwareCheck( ) { };

};


#endif /* AMS_FIRMWARE_CHECK_H */
