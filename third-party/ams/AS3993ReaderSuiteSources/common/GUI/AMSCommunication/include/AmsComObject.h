/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMS Streaming Communication 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AmsComObject.h
 *
 *  \author M. Arpa
 *
 *  \brief  Base class of all classes that implement objects that can
 * be transmitted and received by using the AmsComStream class.
 */

#ifndef AMS_COM_OBJECT_H
#define AMS_COM_OBJECT_H

/* includes --------------------------------------------------------- */

#include "AmsCom.h"


/* forward declarations ---------------------------------------------- */

class QString;
class QXmlStreamReader; 
class QXmlStreamWriter;
class AmsComStream;


/* class declarations ------------------------------------------------ */

class AmsComObject
{
	/* give the stream class privileged access to some members that are
           used for split streaming */
	friend class AmsComStream; 


protected:
    /* service function to write a 1,2 or 4-byte value to the buffer */
    static bool serialising( unsigned char * buffer, unsigned int size, unsigned int value );

    /* service function to read a 1,2 or 4-byte value from the buffer */
    static bool deserialising( unsigned char * buffer, unsigned int size, unsigned int & value );

    /* converts the direction from an xml attribute to the const char * string defined in AmsCom.h */
    static const char * AmsComObject::getDirection( const QString & direction );


public:
    /* write the opening tag for a telegram */
    void writeOpeningTag( QXmlStreamWriter * xml, const char * directon ) const;

    /* write the closing tag for a telegram */
    static void writeClosingTag( QXmlStreamWriter * xml );

    /* status values */
    static const char Ok = 0;
    static const char RxError = 1; /* read from buffer failed */
    static const char WrongProtocol = 2; /* wrong protocol read from buffer */

public:
	/* c'tor */
	AmsComObject 
	  ( unsigned char protocol
	  , int serialSize = 0
	  , int rxSerialSize = 0
	  , int deserialSize = 0
      , unsigned int tracePattern = AMS_COM_TRACE_ALLWAYS
	  );

	/* copy c'tor */
	AmsComObject ( const AmsComObject & other);
	
	/* d'tor */
	~AmsComObject ( ) { };

	/* the protocol byte is returned */
	unsigned char protocol( ) const { return itsProtocol; };

	/* returns the size in bytes that are required for serialisation */
	virtual int serialiseSize ( ) const { return itsSerialSize; }; 

	/* returns the size in bytes that are required for receiving a buffer for deserialisation */
	virtual int rxSerialiseSize ( ) const { return itsRxSerialSize; }; 

	/* returns the size in bytes that are required for deserialisation */
	virtual int deserialiseSize ( ) const { return itsDeserialSize; }; 

	/* serialises the object in the buffer, bufferSize is the number of
	   bytes available for serialisation (must be >= serialiseSize, otherwise
		nothing will be serialised and the functions returns false 
	   returns true if something was serialised, false if not */
	virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) = 0; 

	/* serialises something that is required by the peer to
		provide the data necessary for deserialisiation 
	   returns true if something was serialised, false if not */
	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) = 0; 

	/* deserialises the object in the buffer, bufferSize is the number
	   of bytes in the buffer ( must be >= deserialiseSize). Note that
	   before receiving anything, the owner of this class should call
	   rxSerialise first (so send out something if required for receiving
	   data )
	   returns true if something was deserialised, false if not */
	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) = 0;

	/* if you use read in several objects at once use this function to find out 
	   if reading was completely done. I.e. if the object has a valid 
	   read in data */
	bool valid ( ) const { return itsIsValid; };

	/*get the status from firmware (aka of information how well operating the firmware is) */
	char status() const { return itsStatus; };

    /* deserialises the object from the XML string str into the object members 
	   returns true if something was deserialised, false if not */
    virtual bool fill( QXmlStreamReader * xml ) = 0;

    /* sets the trace pattern to the desired value */
    void setTracePattern( unsigned int tracePattern ) { itsTracePattern = tracePattern; };

private:
    /* allow the AmsStream to set the status through a function */
    void setStatus( char status ) { itsStatus = status; };
    void setStatus( unsigned char status ) { setStatus( static_cast< char >( status ) ); };

    /* allow the AmsStream to set the valid flag through a function */
    void setValid( bool isValid ) { itsIsValid = isValid; };
    
    /* forbid c'tor */
	AmsComObject( );

protected:
    unsigned int itsTracePattern; /* defines when this object shall be traced */   
	int itsSerialSize;
	int itsRxSerialSize;
	int itsDeserialSize;
	unsigned char itsProtocol;
    /* information of the firmware for this and maybe other objects */
    char itsStatus;

private:
	/* do not allow direct access to this variable. The stream communication
	   class uses this for internal state of this object */
    bool itsIsValid; 

    /* if we do read several objects "at once" (without an interleaving flush object)
       we chain the objects together to have them all when the data arrives */
    AmsComObject * itsNext; 
};

/* the AmsFlushObject is used to trigger a flush in the ams stream class */
class AmsFlushObject : public AmsComObject
{
public:
	/* default c'tor */
	AmsFlushObject ( ) : AmsComObject( AMS_COM_FLUSH, 0, 0, 0, AMS_COM_TRACE_FLUSH ) { };

	/* copy c'tor for flush */
	AmsFlushObject ( const AmsFlushObject & other ) : AmsComObject( other ) { };
	
	/* d'tor */
	~AmsFlushObject ( ) { };

	virtual bool serialise( unsigned char * /*buffer*/, int /*bufferSize*/, QXmlStreamWriter * xml ) 
        { 
            writeOpeningTag( xml, AMS_COM_XML_WRITE );
            writeClosingTag( xml );
            return false; 
        }; 

	virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) { return serialise( buffer, bufferSize, xml ); };

	virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) { return serialise( buffer, bufferSize, xml ); };

        virtual bool fill( QXmlStreamReader * xml ); 

};

#endif /* AMS_COM_OBJECT_H */
