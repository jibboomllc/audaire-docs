/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSVersionInfo
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Dedek
 *
 *  \brief  AMSVersionInfo class definition file
 *
 *  AMSVersionInfo is a static class which provides global access to version information
 */


#include <windows.h> 
#include <tchar.h> 
#include "GetVersion.hxx"
#include "globals.h"


AMSVersionInfo::AMSVersionInfo() : isGUIVersionValid(false), isFWVersionValid(false), versionFWBuffer(NULL), versionGUIBuffer(NULL)
{
	init();
}

AMSVersionInfo::~AMSVersionInfo()
{
	if(versionGUIBuffer)
		delete versionGUIBuffer;
	if(versionFWBuffer)
		delete versionFWBuffer;
}

AMSVersionInfo& AMSVersionInfo::Instance()
{
	static AMSVersionInfo instance;
	return instance;
}

void AMSVersionInfo::init()
{
	char	szResult[256] = {0};
	DWORD   dwVerHnd=0;        // An 'ignored' parameter, always '0'
	HGLOBAL hgResData;
	char* pBuffer = NULL;
	DWORD resLen;

	HRSRC hResource = FindResource(NULL,MAKEINTRESOURCE(VS_VERSION_INFO),MAKEINTRESOURCE(16));
	resLen=SizeofResource(NULL,hResource);

	// Resource laden
	if (!(hgResData = LoadResource(NULL,hResource)))
		return; //  "";
	if (!(pBuffer=(char*)LockResource(hgResData)))
		return; //  "";

	resLen+=512; // increase the buffer size as the result is also stored in this buffer !
	versionGUIBuffer = new char[resLen];
	memcpy(versionGUIBuffer, pBuffer,resLen);

	isGUIVersionValid = true;
	hResource = FindResource(NULL,MAKEINTRESOURCE(VS_VERSION_INFO_FW),MAKEINTRESOURCE(16));
	resLen=SizeofResource(NULL,hResource);
	// Resource laden
	if (!(hgResData = LoadResource(NULL,hResource)))
		return; //  "";
	if (!(pBuffer=(char*)LockResource(hgResData)))
		return; //  "";

	resLen+=512; // increase the buffer size as the result is also stored in this buffer !
	versionFWBuffer = new char[resLen+512];
	memcpy(versionFWBuffer, pBuffer,resLen);
	isFWVersionValid = true;
}

QString AMSVersionInfo::GetInfoFromResource(const QString & info, int versionInfo)
{
	char	szResult[256] = {0};
	char    szGetName[256];
	LPSTR   lpVersion;        // String pointer to Item text
	DWORD   dwVerHnd=0;        // An 'ignored' parameter, always '0'
	UINT    uVersionLen;
	BOOL    bRetCode;
	char*	pBuffer = NULL;

	if(versionInfo == VS_VERSION_INFO && isGUIVersionValid)
		pBuffer = (char*)versionGUIBuffer;
	else if(versionInfo == VS_VERSION_INFO_FW && isFWVersionValid)
		pBuffer = (char*)versionFWBuffer;
	else
		return "";

	// Get a codepage from base_file_info_sctructure
	lstrcpy(szGetName, "\\VarFileInfo\\Translation");

	uVersionLen   = 0;
	lpVersion     = NULL;
	bRetCode = VerQueryValue((LPVOID)pBuffer,
		(LPSTR)szGetName,
		(void **)&lpVersion,
		(UINT *)&uVersionLen);
	if ( bRetCode && uVersionLen && lpVersion) {
		sprintf_s(szResult, "%04x%04x", (WORD)(*((DWORD *)lpVersion)),
			(WORD)(*((DWORD *)lpVersion)>>16));
	}
	else {
		// 041904b0 is a very common one, because it means:
		//   US English/Russia, Windows MultiLingual characterset
		// Or to pull it all apart:
		// 04------        = SUBLANG_ENGLISH_USA
		// --09----        = LANG_ENGLISH
		// --19----        = LANG_RUSSIA
		// ----04b0 = 1200 = Codepage for Windows:Multilingual
		lstrcpy(szResult, "041904b0");
	}

	// Add a codepage to base_file_info_sctructure
	sprintf_s (szGetName, "\\StringFileInfo\\%s\\", szResult);
	// Get a specific item
	QString tmp(szGetName);
	tmp += info;

#if QT_VERSION < 0x050000
    bRetCode = VerQueryValue((LPVOID)pBuffer,
        (LPSTR)tmp.toAscii().data(), /*(LPSTR)szGetName,*/
        (void **)&lpVersion,
        (UINT *)&uVersionLen);
#else
    bRetCode = VerQueryValue((LPVOID)pBuffer,
        (LPSTR)tmp.toLatin1().data(), /*(LPSTR)szGetName,*/
        (void **)&lpVersion,
        (UINT *)&uVersionLen);
#endif

	return QString(lpVersion);
}


//Returns a String with the file version Info of the binary during runtime
QString AMSVersionInfo::GetFileVersionString( )
{
	return GetInfoFromResource("FileVersion", VS_VERSION_INFO);
}

//Returns a String with the file version Info of the required FW
QString AMSVersionInfo::GetFileVersionFWString( )
{
	return GetInfoFromResource("FileVersion", VS_VERSION_INFO_FW);
}


//Returns a String with the product file version Info of the binary during runtime
QString AMSVersionInfo::GetProductVersionString( )
{
	return GetInfoFromResource("ProductVersion", VS_VERSION_INFO);
}

//Returns a String with the product file version Info of the required FW
QString AMSVersionInfo::GetProductVersionFWString( )
{
	return GetInfoFromResource("ProductVersion", VS_VERSION_INFO_FW);
}

//Returns a String with the product file version Info of the binary during runtime
QString AMSVersionInfo::GetProductFolderString( )
{
	return GetInfoFromResource("InternalName", VS_VERSION_INFO);
}

//Returns a String with the product file version Info of the binary during runtime
QString AMSVersionInfo::GetProductNameString( )
{
	return GetInfoFromResource("ProductName", VS_VERSION_INFO);
}

//Returns a String with the product file version Info of the binary during runtime
QString AMSVersionInfo::GetProductNameFWString( )
{
	return GetInfoFromResource("ProductName", VS_VERSION_INFO_FW);
}
//Returns a String with the product file version Info of the binary during runtime
QString AMSVersionInfo::GetCompanyNameString( )
{
	return GetInfoFromResource("CompanyName", VS_VERSION_INFO);
} 

bool AMSVersionInfo::IsInternalVersion()
{
	QRegExp rx("(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)");

	QString VersionInfo = GetProductVersionString();
	if(rx.indexIn((VersionInfo))>-1)
		return (bool)(rx.cap(3).toUShort() % 2);
	return true;
}

QString AMSVersionInfo::GetWorkingDirectory( )
{
    /* generate the working directory - and path -> and than hand 
       the string "workingDir" in to the main-window if your application needs 
	    to store data */
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName() );
    QString workingDir = QFileInfo(ini.fileName()).absolutePath();
    /* now add our application folder to the path */
    workingDir.append( '/' ).append( AMSVersionInfo::Instance().GetProductFolderString() ).append( '/' );
    QDir temp;
    /* build the path if it does not yet exist */
    temp.mkpath( workingDir );
    /* the path is: %APPDATA%\Roaming\ams\ASXXXXFolder
       which than looks like e.g. for mar under windows 7: 
       C:\Users\mar\AppData\Roaming\ams\ASXXXXFolder
       */
    return workingDir;
}