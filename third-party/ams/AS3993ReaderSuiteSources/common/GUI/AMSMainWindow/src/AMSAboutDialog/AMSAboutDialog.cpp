/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#if QT_VERSION < 0x050000
#else
#include <QMessageBox>
#endif

#include <AMSAboutDialog.hxx>
#include "hg_info.h"

AMSAboutDialog::AMSAboutDialog(void)
{
	this->setupUi(this);
}

AMSAboutDialog::~AMSAboutDialog(void)
{
}

void AMSAboutDialog::setApplicationName(QString name)
{
	this->applicationNameLabel->setText(name);
}

void AMSAboutDialog::setApplicationVersion(QString version)
{
	this->applicationVersionLabel->setText(version);
}

void AMSAboutDialog::setPluginVersion(QString version)
{
	this->pluginVersionLabel->setText(version);
}

void AMSAboutDialog::on_btShowDetailInfo_clicked()
{
    QString buildInfo(  "<b>Common repository:</b> <br>"
                        "Hash: " + QString(HG_COMMON_HASH) + "  <br><br>"
                        "<b>Project repository:</b> <br>"
                        "Hash: " + QString(HG_PROJECT_HASH) + "  <br><br>"
                        "<b>Qt version:</b> <br>"
                        "Qt " + QString(QT_VERSION_STR)
                     );
    QString buildInfoStripped = buildInfo;
    buildInfoStripped.replace("<br>", "\n").replace( QRegExp("<[^>]*>"), "" );
    buildInfo.append("  <br><br><small>This information has been copied to your clipboard</small>");
    qApp->clipboard()->setText(buildInfoStripped);
    QMessageBox::information(this, 
            tr("Build Info"), 
            buildInfo, 
            QMessageBox::Close);
}
