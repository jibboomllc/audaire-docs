/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

 #include "AMSPluginInterface.hxx"
 #include "AMSPluginDialog.hxx"

 #include <QPluginLoader>
 #include <QStringList>
 #include <QDir>

 #include <QLabel>
 #include <QGridLayout>
 #include <QPushButton>
 #include <QTreeWidget>
 #include <QTreeWidgetItem>
 #include <QHeaderView>

 PluginDialog::PluginDialog(const QString &path, QWidget *parent) :
     QDialog(parent),
     label(new QLabel),
     treeWidget(new QTreeWidget),
     okButton(new QPushButton(tr("OK")))
 {
     treeWidget->setAlternatingRowColors(false);
     treeWidget->setSelectionMode(QAbstractItemView::NoSelection);
     treeWidget->setColumnCount(1);
     treeWidget->header()->hide();

     okButton->setDefault(true);

     connect(okButton, SIGNAL(clicked()), this, SLOT(close()));

     QGridLayout *mainLayout = new QGridLayout;
     mainLayout->setColumnStretch(0, 1);
     mainLayout->setColumnStretch(2, 1);
     mainLayout->addWidget(label, 0, 0, 1, 3);
     mainLayout->addWidget(treeWidget, 1, 0, 1, 3);
     mainLayout->addWidget(okButton, 2, 1);
     setLayout(mainLayout);

	 featureIcon.addPixmap(style()->standardPixmap(QStyle::SP_FileDialogInfoView));

     setWindowTitle(tr("Plugin Information"));
     findPlugins(path);
 }

 void PluginDialog::findPlugins(const QString &path)
 {
     label->setText(tr("The following plugins were found\n"
                       "(looked in %1):")
                    .arg(QDir::toNativeSeparators(path)));

     const QDir dir(path);

	 foreach (QString fileName, dir.entryList(QDir::Files))
	 {
         QPluginLoader loader(dir.absoluteFilePath(fileName));
		 AMSPluginInterface *plugin = qobject_cast<AMSPluginInterface*>(loader.instance());
         if (plugin)
             populateTreeWidget(plugin, fileName);
     }
 }

 void PluginDialog::populateTreeWidget(AMSPluginInterface *plugin, const QString &text)
 {
     QTreeWidgetItem *pluginItem = new QTreeWidgetItem(treeWidget);
     pluginItem->setText(0, text);

     QFont boldFont = pluginItem->font(0);
     boldFont.setBold(true);
     pluginItem->setFont(0, boldFont);

     if (plugin)
	 {
		 addItems(pluginItem, "AMSPluginInterface", plugin->pluginInfo());
     }
 }

 void PluginDialog::addItems(QTreeWidgetItem *pluginItem,
                             const char *interfaceName,
                             const QStringList &features)
 {
     foreach (QString feature, features) {
         if (feature.endsWith("..."))
             feature.chop(3);
         QTreeWidgetItem *featureItem = new QTreeWidgetItem(pluginItem);
         featureItem->setText(0, feature);
         featureItem->setIcon(0, featureIcon);
     }
 }