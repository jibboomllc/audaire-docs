/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

 #include "ChoosePluginDialog.hxx"

 #include <QStringList>

 #include <QLabel>
 #include <QGridLayout>
 #include <QPushButton>
 #include <QComboBox>

 ChoosePluginDialog::ChoosePluginDialog(const QStringList &fileNames, QString *desiredPlugin, QWidget *parent) :
     QDialog(parent),
     label(new QLabel),
     comboBox(new QComboBox),
     okButton(new QPushButton(tr("OK")))
 {

     okButton->setDefault(true);

     connect(okButton, SIGNAL(clicked()), this, SLOT(close()));

     QGridLayout *mainLayout = new QGridLayout;
     mainLayout->setColumnStretch(0, 1);
     mainLayout->setColumnStretch(2, 1);
     mainLayout->addWidget(label, 0, 0, 1, 3);
     mainLayout->addWidget(comboBox, 1, 0, 1, 3);
     mainLayout->addWidget(okButton, 2, 1);
     setLayout(mainLayout);

     setWindowTitle(tr("Choose Plugin"));
     label->setText(tr("Which AMS chip would you like to use?"));

	 comboBox->addItems(fileNames);

	 this->desiredPlugin = desiredPlugin;
 }

 void ChoosePluginDialog::closeEvent(QCloseEvent *)
 {
	*desiredPlugin = comboBox->currentText();
 }
