/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       * 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams simple XML writer + reader
 *      $Revision: $
 *      LANGUAGE: QT C++
 */
/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  Class to perform all xml read/write for a project.
 *
 */

#include "AMSSimpleXml.h"

AmsSimpleXml::AmsSimpleXml ( const QString & filename, const QString & version )
: itsDoc( "AmsSimpleXml" )
, itsFilename( filename )
, itsVersion( version )
{
}

AmsSimpleXml::~AmsSimpleXml ( )
{
}

void AmsSimpleXml::createRoot ( const QString & name )
{
	itsRoot = itsDoc.createElement( name );
	itsRoot.setAttribute( "Version", itsVersion );
	itsDoc.appendChild( itsRoot );
}

QDomElement AmsSimpleXml::createMainElement ( const QString & name )
{
	QDomElement el = itsDoc.createElement( name );
	return itsRoot.appendChild( el ).toElement();
}

void AmsSimpleXml::createNode ( QDomElement & parent, const QString & name )
{
	QDomElement el = itsDoc.createElement( name );
    parent.appendChild( el );
}	

void AmsSimpleXml::createNodeAndValue ( QDomElement & parent, const QString & name, const QString & value )
{
	QDomElement el = itsDoc.createElement( name );
	QDomText val = itsDoc.createTextNode( value );
	parent.appendChild( el );
	el.appendChild( val );
}	

void AmsSimpleXml::createNodeAndValue ( QDomElement & parent, const QString & name, double value )
{
	QString val;
	val.setNum( value, 'f', 3 );
	createNodeAndValue( parent, name, val );
}

void AmsSimpleXml::createNodeAndValue ( QDomElement & parent, const QString & name, int value )
{
	QString val;
	val.setNum( value, 10);
	createNodeAndValue( parent, name, val );
}

void AmsSimpleXml::addAttribute ( QDomElement & element, const QString & attribute, int value )
{
	element.setAttribute( attribute, value );
}

void AmsSimpleXml::addAttribute ( QDomElement & element, const QString & attribute, const QString & value )
{
	element.setAttribute( attribute, value );
}


bool AmsSimpleXml::writeToFile ( )
{
	QString xml = itsDoc.toString( );

	QFile file( itsFilename );
	if ( file.open( QFile::WriteOnly | QFile::Truncate ) )
	{
		QTextStream output( &file );
		output << xml;
		output.flush( );
		file.close( );
		return true;
	}
	return false;
}

bool AmsSimpleXml::readFromFile ( )
{
	bool result = false;
	QFile file( itsFilename );
	if ( file.open( QFile::ReadOnly | QFile::Text ) )
	{
		if ( itsDoc.setContent( &file ) )
		{ 
			result = true;
		}
		file.close( );
	}
	return result;
}

bool AmsSimpleXml::getMainElement ( const QString & name, QDomElement & element, const QString & attribute, int attributeValue )
{
	QDomNodeList list = itsDoc.documentElement().elementsByTagName( name );

	if ( list.size() == 0 )
	{
		return false;
	}

	if ( attribute.isEmpty() )
	{ /* always return first element */
		element = list.item( 0 ).toElement( );
		return true;
	}
	else 
	{ /* find element with given attribute */
		QString value;
		value.setNum( attributeValue, 10 );
		for ( int i = 0; i < list.size(); ++i )
		{
			element = list.item( i ).toElement( );
			QString val = element.attribute( attribute );
			if ( val.compare( value, Qt::CaseInsensitive ) == 0 )
			{
				return true;
			}
		}
	}

	return false;
}

bool AmsSimpleXml::getNodeValue ( QDomElement & parent, const QString & name, QString & value )
{
	QDomNodeList list = parent.elementsByTagName( name );
	if ( list.size() >= 1 )
	{ /* always return first element */
		value = list.item( 0 ).toElement( ).text( );
		return true;
	}
	return false;
}

bool AmsSimpleXml::getNodeValue ( QDomElement & parent, const QString & name, double & value )
{
	QString val;
	if ( getNodeValue( parent, name, val ) )
	{
		bool result;
		value = val.toDouble( &result );
		return result;
	}
	return false;
}

bool AmsSimpleXml::getNodeValue ( QDomElement & parent, const QString & name, int & value )
{
	QString val;
	if ( getNodeValue( parent, name, val ) )
	{
		bool result;
		value = val.toInt( &result );
		return result;
	}
	return false;
}

bool AmsSimpleXml::getAttribute ( QDomElement & element, const QString & attribute, int & value )
{
	QString val;
	if ( getAttribute( element, attribute, val ) )
	{
		bool result;
		value = val.toInt( &result );
		return result;
	}
	return false;
}

bool AmsSimpleXml::getAttribute ( QDomElement & element, const QString & attribute, QString & value )
{
	value = element.attribute( attribute );
	return !value.isEmpty( );
}

