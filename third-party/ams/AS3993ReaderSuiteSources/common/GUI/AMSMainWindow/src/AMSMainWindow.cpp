/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSMainWindow
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author C. Eisendle, M. Dedek, et. al.
 *
 *  \brief  AMSMainWindow class definition file
 *
 *  AMSMainWindow represents the standard window used by ams framework
 *  It includes basic functions which are common to most applications
 */

#include "AMSMainWindow.hxx"
#include "AMSAboutDialog.hxx"
#include "AMSTrace.hxx"
#include "AMSAboutDialog.hxx"
#include "hg_info.h"
#include "GetVersion.hxx"
#include "AmsCom.h"
#include "AMSSimpleXml.h"
#include "register_map.hxx"
#include "AMSCommunication.hxx"
#include "AmsComStream.h"

#if QT_VERSION < 0x050000
#else
#include <QSplashScreen>
#include <QMenuBar>
#include <QMenu>
#include <QStatusBar>
#include <QToolBar>
#include <QDesktopWidget>
#endif

const QSize NORMAL_WINDOW_SIZE = QSize(1016, 700); // This is 1024x768 with window frame on Windows
const int MIN_SCREEN_HIGH = 700;


AMSMainWindow::AMSMainWindow() : superUser(false)
{
  initAMSMainWindow();
}

AMSMainWindow::AMSMainWindow(QString VersionInfoMain) : superUser(false), itsInternalRelease(true) 
{
  this->itsVersionInfo = VersionInfoMain;
  initAMSMainWindow();
}

void AMSMainWindow::initAMSMainWindow()
{
	applicationName = AMSVersionInfo::Instance().GetProductNameString();
	
	if(this->itsVersionInfo.isEmpty())
		this->itsVersionInfo = AMSVersionInfo::Instance().GetProductVersionString();
	itsInternalRelease = AMSVersionInfo::Instance().IsInternalVersion();

	QPixmap logo(":/logos/ams_quadr");

	// load and apply ams stylesheet
    QFile file(":/css/css/styles.css");
    file.open(QIODevice::ReadOnly);
    QTextStream in(&file); 
    QString cssStyleSheet = in.readAll();
    qApp->setStyleSheet(qApp->styleSheet() + cssStyleSheet);

    // add internal release overlay if required
    if(itsInternalRelease)
    {
        applicationName += " \nINTERNAL RELEASE!!!";
        // retreive current stylesheet and add internal release overlay
        qApp->setStyleSheet(qApp->styleSheet() + 
            "QTabWidget > *, .QWidget {"
            "background-image:url(:/internal_bg.png);"
            "background-position: center center;"
            "background-repeat: none;"
            "}");
    }
	
    // create and init about dialog
	amsAboutDlg = NULL;
	amsAboutDlg = new AMSAboutDialog();
	amsAboutDlg->setApplicationName(applicationName);
    amsAboutDlg->setWindowTitle(applicationName);
  
    amsAboutDlg->setApplicationVersion("Product Version: " + this->itsVersionInfo);
	
    // show splash screen
	splashAlignment = Qt::AlignHCenter | Qt::AlignBottom;
	splash = new QSplashScreen(logo);
	splash->setWindowOpacity(0);
	if(!itsInternalRelease)
	{
		splash->show();
		for(double i = 0; i < 1; i += 0.05)
		{
			splash->setWindowOpacity(i);
			SleepThread::msleep(75);
		}
	}
	else // if(!internalWasOnceStarted())
	{
		QMessageBox::critical(this, "I N T E R N A L  R E L E A S E !!!", 
			"Please note that you're using an internal version of this software!<br> \
									This version <b>MUST NOT</b> be sent to any customer!");
	}

	updateSplash(tr("Initializing trace module..."));

    createActions();
    createMenus();
    createToolBar();
	createStatusBar();

	updateSplash(tr("Initializing communication module..."));

	if(!itsInternalRelease)
		SleepThread::msleep(1000);
  
	setWindowTitle(applicationName);
	updateSplash(tr("Initializing GUI..."));

	mainWidget = new QWidget(this); // the main widget which is available in derived classes
	setCentralWidget(mainWidget);
    readSettings();
}


AMSMainWindow::~AMSMainWindow()
{
	delete amsAboutDlg;
}

QMenu* AMSMainWindow::addMenu(const QString name)
{
	return this->menuBar()->addMenu(name);
}

QMenu* AMSMainWindow::insertMenu(QAction* before, const QString name)
{
	QMenu *newMenu = new QMenu(name);
	this->menuBar()->insertMenu(before, newMenu);
	return newMenu;
}

void AMSMainWindow::setPluginVersion(QString version)
{
	if(amsAboutDlg)
	{
		amsAboutDlg->setPluginVersion(version);		
	}
}

void AMSMainWindow::createMenus()
{
    fileMenu = this->menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(resetAct);
    fileMenu->addSeparator();
	fileMenu->addAction(readOutRegsAct);
	fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

	viewMenu = this->menuBar()->addMenu("&View");
	viewMenu->addAction(viewRegMapAct);
	viewMenu->addAction(viewTraceWindowAct);
   
	settingsMenu = this->menuBar()->addMenu(tr("&Settings"));
	settingsMenu->addAction(onOffTraceAct);
	settingsMenu->addSeparator();
	settingsMenu->addAction(autoUpdateAct);
	settingsMenu->addAction(chipDetectionAct);

	helpMenu = this->menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
	helpMenu->addSeparator();
	helpMenu->addAction(firmwareUpgradeAct);

    addAction(superUserAct); // Non visible main window action
    this->statusBar()->showMessage(tr("Ready"));
}

void AMSMainWindow::createActions()
{
    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(exit()));

    aboutAct = new QAction(tr("&About"), this);
	aboutAct->setIcon(QIcon(":/icons/gray/info"));
    aboutAct->setStatusTip(tr("Show application about box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    superUserAct = new QAction(this);
    superUserAct->setShortcut(tr("Ctrl+Alt+S"));
    connect(superUserAct, SIGNAL(triggered()), this, SLOT(superUserMode()));

    resetAct = new QAction(tr("Re&set"), this);
    resetAct->setStatusTip(tr("Chip reset"));
    resetAct->setVisible(false);
    connect(resetAct, SIGNAL(triggered()), this, SLOT(reset()));
    
	onOffTraceAct =  new QAction(tr("Switch Trace on"), this);
	onOffTraceAct->setShortcut(tr("Ctrl+Shift+T"));
	onOffTraceAct->setEnabled(true);
	onOffTraceAct->setStatusTip(tr("Use to turn on/off tracer"));
	onOffTraceAct->setCheckable(true);
	onOffTraceAct->setChecked(true);
	connect(onOffTraceAct, SIGNAL(triggered(bool)), this, SLOT(onOffTrace(bool)));

	viewRegMapAct = new QAction(tr("Register &Map"), this);
	viewRegMapAct->setShortcut(tr("Ctrl+M"));
	viewRegMapAct->setStatusTip(tr("Show the register map window"));
	viewRegMapAct->setIcon(QIcon(":/icons/gray/database"));
	connect(viewRegMapAct, SIGNAL(triggered()), this, SLOT(viewRegMap()));

	viewTraceWindowAct = new QAction(tr("Trace &Window"), this);
	viewTraceWindowAct->setShortcut(tr("Ctrl+T"));
	viewTraceWindowAct->setStatusTip(tr("Show trace window"));
	viewTraceWindowAct->setIcon(QIcon(":/icons/gray/table"));
	connect(viewTraceWindowAct, SIGNAL(triggered()), this, SLOT(viewTraceWindow()));

	autoUpdateAct = new QAction(tr("Automatic &Update"), this);
    autoUpdateAct->setShortcut(tr("Ctrl+U"));
    autoUpdateAct->setStatusTip(tr("Turn on/off automatic GUI update functionality"));
    autoUpdateAct->setCheckable(true);
    connect(autoUpdateAct, SIGNAL(triggered(bool)), this, SLOT(autoUpdate(bool)));

    readOutRegsAct = new QAction(tr("&Readout Registers"), this);
    readOutRegsAct->setShortcut(tr("Ctrl+R"));
    readOutRegsAct->setStatusTip(tr("Readout all the registers once"));
	readOutRegsAct->setIcon(QIcon(":/icons/gray/reload"));
    connect(readOutRegsAct, SIGNAL(triggered()), this, SLOT(readoutRegisters()));

	firmwareUpgradeAct = new QAction(tr("&Firmware Update"), this);
	firmwareUpgradeAct->setShortcut(tr("Ctrl+F"));
	firmwareUpgradeAct->setStatusTip(tr("Start update procedure for connected Demo Board"));
	firmwareUpgradeAct->setIcon(QIcon(":/icons/gray/chip"));

	chipDetectionAct = new QAction(tr("&Enable Chip Detection"), this);
	chipDetectionAct->setShortcut(tr("Ctrl+Shift+C"));
	chipDetectionAct->setStatusTip(tr("En/Disable automatic chip detection"));
	chipDetectionAct->setCheckable(true);
	chipDetectionAct->setChecked(false);
}


void AMSMainWindow::createToolBar()
{
    // create toolbar which contains the ams logo at its right side, defined by stylesheet
    toolBar = new QToolBar("Toolbar", this);
    toolBar->setObjectName(QString::fromUtf8("Toolbar"));
    toolBar->setAllowedAreas(Qt::TopToolBarArea);
    toolBar->setMovable(false);
    toolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->setMinimumHeight(65);
    addToolBar(Qt::TopToolBarArea, toolBar);
    toolBar->addAction(viewRegMapAct);
    toolBar->addAction(readOutRegsAct);
}

void AMSMainWindow::createStatusBar()
{
	usbStateStatusBar = new QLabel(" USB ", this);
	usbStateStatusBar->setFixedWidth(40);
	usbStateStatusBar->setAlignment(Qt::AlignHCenter);
	statusBar()->addPermanentWidget(usbStateStatusBar);

    chipIdStatusBar = new QLabel(tr("Not identified"), this);
	chipIdStatusBar->setFixedWidth(80);
	chipIdStatusBar->setAlignment(Qt::AlignHCenter);
    statusBar()->addPermanentWidget(chipIdStatusBar);
}



void AMSMainWindow::exit()
{
    close();
}

void AMSMainWindow::about()
{
	if(amsAboutDlg)
		amsAboutDlg->exec();
}

void AMSMainWindow::superUserMode()
{
    superUser = !superUser;
	emit modeChanged(superUser); // Emit the superuser mode signal
}

void AMSMainWindow::closeEvent(QCloseEvent *event)
{
        writeSettings();
        event->accept();
}

void AMSMainWindow::writeSettings()
{
    QSettings settings("ams", applicationName);

    settings.beginGroup("AMSMainWindow");
    settings.setValue("pos", pos());
    settings.endGroup();
}

bool AMSMainWindow::internalWasOnceStarted()
{
	bool ret = false;	
	QSettings settings("ams", applicationName);
	settings.beginGroup("InternalRelease");
	if(settings.contains("InternalOnceStarted"))
		ret = true;
	else
	{
		settings.setValue("InternalOnceStarted", 1);
		ret = false;
	}
	settings.endGroup();
	return ret;
}

void AMSMainWindow::readSettings()
{
    QSettings settings("ams", applicationName);
	settings.beginGroup("AMSMainWindow");
	QPoint zeroPos = QPoint(0, 0);   // set position to 0/0 in case 2nd screen is disabled and stored position is on 2nd screen
	QPoint p = settings.value("pos", zeroPos).toPoint();

	if((QApplication::desktop()->screenCount() == 1) && (p.x() > QApplication::desktop()->availableGeometry(-1).x()))
    {
		p = zeroPos;
    }
	
	move(p);
	settings.endGroup();
}


void AMSMainWindow::onOffTrace(bool on)
{
	if(on)
    {
		AMSTrace::setTrcLevel(10);
        AMSTrace::setTrcPattern( AMS_COM_TRACE_ALLWAYS - AMS_COM_TRACE_FLUSH );
    }
	else
    {
		AMSTrace::setTrcLevel(-1);
        AMSTrace::setTrcPattern( AMS_COM_TRACE_NEVER );
    }
}

void AMSMainWindow::reinitConnection()
{
}

void AMSMainWindow::reset()
{
}

void AMSMainWindow::updateStatusBar()
{
}

void AMSMainWindow::showHelp()
{
	/* show the help window */
	procHelp = new QProcess();
	QStringList args;
	args << QLatin1String("-collectionFile")
		<< QLatin1String("help/help.qhc")
		<< QLatin1String("-enableRemoteControl");
	procHelp->start(QLatin1String("assistant"), args);
}

void AMSMainWindow::viewRegMap()
{
	/* show the register map */
}

void AMSMainWindow::viewTraceWindow()
{
	/* show the trace window */
}

void AMSMainWindow::readoutRegisters()
{
}

void AMSMainWindow::autoUpdate(bool update)
{
}

void AMSMainWindow::upgradeFirmware()
{
}

void AMSMainWindow::closeSplashScreen()
{
	updateSplash(tr("Starting up..."));
	SleepThread::msleep(1000);
	splash->close();
}

void AMSMainWindow::updateSplash( const QString text )
{
    splash->showMessage(text, splashAlignment, Qt::white);
}

void AMSMainWindow::storeRegisterMapValues( const QString & fileName, RegisterMap & regMap, AMSCommunication * com )
{
    QList<unsigned char> regList;
    QDomElement mainEl;
    int i;

    if ( !com || !com->isConnected() )
    {
        QMessageBox::warning(0, tr("Store register error"), tr("Please connect to device before storing can be executed!"));
        return;
    }

    /* create an xml header */
    AmsSimpleXml xml( fileName, AMSVersionInfo::Instance().GetProductVersionString() );

    /* create xml root element */
    xml.createRoot( QString("RegisterMap"));

    /* create a main element */
    mainEl = xml.createMainElement(QString("Registers"));

    regList = regMap.getRegisters();
    for ( i = 0; i < regList.size(); i ++)
    {
        /* create a node for each register under the main element node = flat tree */
        unsigned char regAddr = regList.value(i);
        unsigned char regVal = 0;

        if ( com )
        { /* get value from hw if possible */
            com->readRegister( regAddr, &regVal );
        }

        /* create for each register a node with 2 attributes */
        xml.createNode( mainEl, QString( "Register" ) );
        QDomElement el( mainEl.lastChildElement( QString( "Register" ) ) ); /* inserted at the end */
        xml.addAttribute( el, QString("Address"), regAddr );
        xml.addAttribute( el, QString("Value"), regVal );
    }

    /* flush to disk */
    xml.writeToFile();
}

void AMSMainWindow::loadRegisterMapValues( const QString & fileName, RegisterMap & regMap, AMSCommunication * com )
{
    QDomElement mainEl;
    int i;

    AmsSimpleXml xml( fileName, AMSVersionInfo::Instance().GetProductVersionString() );

    if ( !com || (AMSCommunication::NoError != com->connect()) )
    {
        QMessageBox::warning(0, tr("Load register error"), tr("Please connect to device before loading can be executed!"));
        return;
    }

    if (!xml.readFromFile())
    {
        QMessageBox::warning(0, tr("Load register error"), tr("Cannot read register value file."));
        return;
    }

    /* get the main element under which all other elements are attached */
    if ( xml.getMainElement( QString("Registers"), mainEl ) )   
    {
    	QDomNodeList nodeList = mainEl.elementsByTagName( QString( "Register" ) );
	    
        for ( i = 0; i < nodeList.size(); i++ )
        {
            QDomElement el( nodeList.at( i ).toElement() );

            /* read each node -> represents one register */
            int value;
            if ( xml.getAttribute( el, QString( "Address" ), value ) )
            {
                unsigned char regAddr = static_cast< unsigned char >( value );
                if ( xml.getAttribute( el, QString( "Value" ), value ) )
                {
                    unsigned char regVal = static_cast< unsigned char >( value );

                    if ( com )
                    { /* set value to hw if possible */
                        com->writeRegister( regAddr, regVal );
                    }
                }
            }
        }
    }
}

