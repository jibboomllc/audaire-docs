@ECHO Off
:: author Paul Rudelstorfer, modified by Bernhard Breinbauer
:: date: 2011-08.10
:: generates hg_info.h
:: works also if hg fails

:BEGIN
:: $1: path to AMSMainWindow project dir (and common repository)
:: $2: path to parent/main repository (containing the subrepos)
:: $3: path to project repository (Solution directory)
if "%1" == "" goto END      
if "%2" == "" goto END
if "%3" == "" goto END

IF EXIST %1\generated\include goto SKIP_MD
md "%1\generated\include"
:SKIP_MD

::define Filename
set H_FILE=%1\generated\include\hg_info.h
:: name of tempfile and out file if error accurs
set ERF=%1\versioninfo\hgErr.txt
set COMMON_HASH=0
set PARENT_HASH=0
set PARENT_MODIFIED=""
set PROJECT_HASH=0
set CWD="%CD%"

:SET_COMMON_INFO
cd "%1"
::Write hash in file
hg identify -i > %ERF%
if NOT ERRORLEVEL 0 goto SET_PARENT_INFO 
::use info from tmpFile
set /p COMMON_HASH=< %ERF%
del %ERF%

:SET_PARENT_INFO
cd "%2"
::Write hash in file
hg identify -i > %ERF%
if NOT ERRORLEVEL 0 goto SET_PROJECT_INFO 
::use info from tmpFile
set /p PARENT_HASH=< %ERF%
del %ERF%

:SET_PROJECT_INFO
cd "%3"
::Write hash in file
hg identify -i > %ERF%
if NOT ERRORLEVEL 0 goto FINISH 
::use info from tmpFile
set /p PROJECT_HASH=< %ERF%
del %ERF%


:FINISH
cd "%CWD%"
::print variables in h file
ECHO #define HG_PARENT_HASH "%PARENT_HASH%" > %H_FILE%.tmp
ECHO #define HG_COMMON_HASH "%COMMON_HASH%" >> %H_FILE%.tmp
ECHO #define HG_PROJECT_HASH "%PROJECT_HASH%" >> %H_FILE%.tmp

FC %H_FILE% %H_FILE%.tmp

IF ERRORLEVEL 1 COPY %H_FILE%.tmp %H_FILE%

:END 