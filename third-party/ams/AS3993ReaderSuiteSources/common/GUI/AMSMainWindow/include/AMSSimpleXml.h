/*
 *****************************************************************************
 * Copyright @ 2009 by austriamicrosystems AG                                *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       * 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   Ams serivces for projects
 *      $Revision: $
 *      LANGUAGE: QT C++
 */
/*! \file
 *
 *  \author M. Arpa
 *
 *  \brief  Class to save and load projects in XML formatted files.
 *
 */

#ifndef AMS_SIMPLE_XML_H
#define AMS_SIMPLE_XML_H

#include <QString>
#include <QDomDocument>
#include <QDomElement>

class AmsSimpleXml 
{

public:
	AmsSimpleXml ( const QString & filename, const QString & version );
	~AmsSimpleXml ( );

	/* writer routines */
	void createRoot( const QString & name );
	QDomElement createMainElement( const QString & name );
	void createNode( QDomElement & parent, const QString & name );
	void createNodeAndValue( QDomElement & parent, const QString & name, const QString & value );
	void createNodeAndValue( QDomElement & parent, const QString & name, double value );
	void createNodeAndValue( QDomElement & parent, const QString & name, int value );
	void addAttribute( QDomElement & element, const QString & attribute, const QString & value );
	void addAttribute( QDomElement & element, const QString & attribute, int value );
	bool writeToFile( );

	/* reader routines */
	bool readFromFile( );
	bool getMainElement( const QString & name, QDomElement & element, const QString & attribute = QString(), int attributeValue = 0 );
	bool getNodeValue( QDomElement & parent, const QString & name, QString & value );
	bool getNodeValue( QDomElement & parent, const QString & name, double & value );
	bool getNodeValue( QDomElement & parent, const QString & name, int & value );
	bool getAttribute( QDomElement & element, const QString & attribute, QString & value );
	bool getAttribute( QDomElement & element, const QString & attribute, int & value );

protected:
	QDomDocument itsDoc;
	QDomElement itsRoot;
	const QString itsFilename;
	const QString itsVersion;
};

#endif /* AMS_SIMPLE_XML_H */
