/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

 #ifndef PLUGINDIALOG_H
 #define PLUGINDIALOG_H

 #include <QDialog>
 #include <QIcon>

 class AMSPluginInterface;
 class QLabel;
 class QPushButton;
 class QStringList;
 class QTreeWidget;
 class QTreeWidgetItem;

 class PluginDialog : public QDialog
 {
     Q_OBJECT

 public:
     PluginDialog(const QString &path,
                  QWidget *parent = 0);

 private:
     void findPlugins(const QString &path);
     void populateTreeWidget(AMSPluginInterface *plugin, const QString &text);
     void addItems(QTreeWidgetItem *pluginItem, const char *interfaceName,
                   const QStringList &features);

     QLabel *label;
     QTreeWidget *treeWidget;
     QPushButton *okButton;
     QIcon featureIcon;
 };

 #endif