/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

 #ifndef CHOOSEPLUGINDIALOG_H
 #define CHOOSEPLUGINDIALOG_H

#if QT_VERSION < 0x050000
#else
#include <QLabel>
#endif

 #include <QDialog>

 class QLabel;
 class QPushButton;
 class QStringList;
 class QComboBox;

 class ChoosePluginDialog : public QDialog
 {
     Q_OBJECT

 public:
     ChoosePluginDialog(const QStringList &fileNames, QString *desiredPlugin, QWidget *parent = 0);
     void setLabelText(const QString &labelText) { if (label) label->setText(labelText); };

 private:
	 void closeEvent(QCloseEvent *);

     QLabel *label;
     QComboBox *comboBox;
     QPushButton *okButton;
	 QString *desiredPlugin;
 };

 #endif