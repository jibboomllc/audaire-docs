/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef AMSPLUGIN_INTERFACE_H
#define AMSPLUGIN_INTERFACE_H

#include "AMSCommunication.hxx"
#include <QtPlugin>

class AMSMainWindow;
class RegisterMap;
class AMSTrace;

class AMSPluginInterface
{
public:
	virtual ~AMSPluginInterface() {}

	virtual void initialize(quint16 vid, quint16 pid) = 0;

	virtual QStringList pluginInfo() const = 0;

	virtual AMSCommunicationChannel communicationChannel() const = 0;
	virtual AMSCommunication* communicationClass() const = 0;

	virtual QString registerMapFile() const = 0;
	virtual void setRegisterMap(RegisterMap* map) = 0;
    virtual RegisterMap* getRegisterMap() { return static_cast<RegisterMap*>(NULL); }

	virtual bool firmwareUpdateParameters(QString& binFile, QString& uController, unsigned int& startAddr) const = 0;
	virtual bool enterBootLoader() = 0;

	virtual void setMainWindow(AMSMainWindow* mainWindow) = 0;

	virtual void setTrace(AMSTrace* trace) = 0;

	virtual AMSTrace* getTrace() = 0; 
	virtual QStandardItemModel* getTraceDataModel() = 0; 

	virtual void setAutoUpdate(bool) = 0;

	virtual void setSuperUserMode(bool) = 0;
};

Q_DECLARE_INTERFACE(AMSPluginInterface, "com.ams.AMSPluginInterface/2.1")

#endif // AMSPLUGIN_INTERFACE_H