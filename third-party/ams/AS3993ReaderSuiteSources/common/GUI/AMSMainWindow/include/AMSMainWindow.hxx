/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef AMS_MAIN_WINDOW_H
#define AMS_MAIN_WINDOW_H

#include "globals.h"
#include <Windows.h>

#include <QMainWindow>

class AMSAboutDialog;
class QLabel;
class QProcess;
class QVBoxLayout;
class QSplashScreen;
class RegisterMap;
class AMSCommunication;
class AmsComStream;


//! Application main window class. 
/*!
  This is an application main window class. It holds tab pages of all
  sub windows and handles Update Thread and Register Map functioning.
*/
class AMSMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	AMSMainWindow();
    AMSMainWindow(QString VersionInfoMain);
	~AMSMainWindow();
	QMenu*	addMenu(const QString name);
	QMenu*	insertMenu(QAction* before, const QString name);

    // make the following members public because we need to access them from the plugins.
	QMenu* 						fileMenu;
	QMenu* 						traceMenu;
	QMenu*						settingsMenu;
	QMenu*						viewMenu;
	QMenu* 						helpMenu;

    QToolBar*                   toolBar;

    QLabel						*chipIdStatusBar; //!< Chip version detection information.
    QLabel						*usbStateStatusBar;
    QLabel						*i2cStateStatusBar;
    QString						chipID; //!< Holds current chip id or empty

	QAction* 					exitAct; //!< Application exit action.
	QAction* 					aboutAct; //!< About-box window action.
    QAction* 					superUserAct; //!< Toggle super-user mode action (only keyboard action).
    QAction* 					resetAct; //!< Reset action. Available only in super-user mode.
    QAction* 					powerOnOffAct; //!< Chip power-off action.
	QAction* 					viewRegMapAct;     //!< Open/close register map window action.
	QAction* 					viewTraceWindowAct; //!< Open/close trace window action.
	QAction* 					autoUpdateAct;     //!< Start/stop automatic update action.
    QAction* 					readOutRegsAct;    //!< Readout registers once action.
    QAction* 					setTraceLevelAct;    //!< execute commands of a loaded file
    QAction* 					onOffTraceAct;    //!< execute commands of a loaded file
	QAction						*showHelpAct;		//!< Show Help Window action.
	QAction						*firmwareUpgradeAct;//!< firmware upgrade action
	QAction						*chipDetectionAct;//!< chip detection action
	QString						applicationName;

    QWidget*					mainWidget; //!< Main Widegt used as base for any GUI

signals:
    //! Signal is emitted when mode changed from/to super user mode.
    void modeChanged(bool newMode);

protected slots:
	virtual void reset();
	virtual void onOffTrace(bool);
	virtual void updateStatusBar();
    virtual void readoutRegisters();
	virtual void autoUpdate(bool status);
	virtual void viewRegMap();
	virtual void viewTraceWindow();
	virtual void upgradeFirmware();
	virtual void showHelp();
    void initAMSMainWindow();
    void updateSplash(const QString text);
    void superUserMode();

private slots:
	void exit();
	void about();
    //! Slot is called on interrupt event. Only action done by this method
    //! is to display the status bar message for the occurred interrupt.
	
protected:
	QProcess					*procHelp;			//!< Help Window process.

	QVBoxLayout*				verticalLayout;
	
	QSplashScreen *				splash;
	int							splashAlignment;
    QString			            itsVersionInfo;    //!< contains version info extracted from resource file
	bool						itsInternalRelease;

private:
	AMSAboutDialog*				amsAboutDlg;
    bool			superUser; //!< True for superuser mode. Mode is changed by superUserAct keyboard action.
    unsigned short	intPollingCounter; //!< Counter of sources which disables interruptPollingAct menu option.
	
	bool			internalWasOnceStarted();
protected:
	void setPluginVersion(QString);

	//! Creates window menus and connects actions to the menus.
    virtual void createMenus(); 
    //! Creates actions used by menu options, toolbar buttons and a keyboard.
    //! This method must be called before the createMenus() and createActions().
	virtual void createActions(); 
    //! Creates window toolbar and its buttons.
    virtual void createToolBar();
	//!< Writes window settings to the configuration file.
    virtual void writeSettings(); 
    //!< Reads window settings from the configuration file.
	virtual void readSettings(); 

	//!< Reinitialize connection on the USB or I2C connection detected.
	virtual void	reinitConnection(); 
	virtual void	createStatusBar();
	virtual void	closeEvent(QCloseEvent *event);
	virtual void	closeSplashScreen();

    /* service function to store and load the register map values */
    void storeRegisterMapValues( const QString & fileName, RegisterMap & regMap, AMSCommunication * com );
    void loadRegisterMapValues( const QString & fileName, RegisterMap & regMap, AMSCommunication * com ); 
};

#endif // AMS_MAIN_WINDOW_H
