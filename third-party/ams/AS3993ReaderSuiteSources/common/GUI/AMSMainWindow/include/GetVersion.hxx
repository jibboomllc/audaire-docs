/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */


#ifndef VERSION_H_
#define VERSION_H_

#pragma comment(lib, "Version.lib")

#define VS_VERSION_INFO_FW	101


class AMSVersionInfo : public QObject
{
	Q_OBJECT

public	:

	static AMSVersionInfo& Instance();
	QString GetFileVersionString( );
	QString GetProductVersionString( );
	QString GetProductNameString( );
	QString GetCompanyNameString( );
	QString GetProductNameFWString( );
	QString GetFileVersionFWString( );
	QString GetProductVersionFWString( );
	QString GetVersionResource(char *, int);
	QString GetProductFolderString( );
	QString GetInfoFromResource(const QString &, int);
	bool    IsInternalVersion();
    QString GetWorkingDirectory( );

private:
	AMSVersionInfo();
	AMSVersionInfo(const AMSVersionInfo&) {}
	AMSVersionInfo& operator=(const AMSVersionInfo&) {}
	~AMSVersionInfo();
	void init( );
	bool isGUIVersionValid;
	bool isFWVersionValid;
	void *versionGUIBuffer;
	void *versionFWBuffer;
};
#endif