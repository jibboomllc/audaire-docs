/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef GRAPHPOINTLIST_H
#define GRAPHPOINTLIST_H

#include "GraphWidget.hxx"
#include <QGraphicsItem>
#include <QList>

class GraphLineItem : public QObject
{
public:
    GraphLineItem(GraphWidget *graphWidget, QGraphicsScene * scene);

	GraphNode * addNode(qreal x, qreal y);
	QList<GraphNode *> nodes() const;
	QList<GraphEdge *> edges() const;
	void setLineItemStyle(QColor &nodeColor, QColor &edgeColor, Qt::PenStyle edgePenStyle, qreal nodeSize, qreal edgeWidth, qreal edgeArrowSize);
	void addEdge(GraphNode *sourceNode, GraphNode *destNode);
	void addNodeXAxisDependency(GraphNode *node, GraphNode *prevNode, GraphNode *nextNode);
	void addNodeYAxisDependency(GraphNode *node, GraphNode *prevNode, GraphNode *nextNode);
	void makeNodeMoveable(GraphNode *node, bool XDirectionMoveable, bool YDirectionMoveable);
	void removeLineItem();

protected:
    
private:
	QGraphicsScene * scene;
    GraphWidget *graph;
	QList<GraphNode *> nodeList;
	QList<GraphEdge *> edgeList;
	QColor nodeColor;
	QColor edgeColor;
	Qt::PenStyle edgePenStyle;
	qreal nodeSize;
	qreal edgeWidth;
	qreal edgeArrowSize;
};

#endif
