/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QtGui>
#include <QtGui/QGraphicsView>

class GraphLineItem;

class GraphWidget : public QGraphicsView
{
    Q_OBJECT

public:
    GraphWidget(QWidget *parent);

	void setScale(qreal x_min, qreal y_min, qreal x_max, qreal y_max, int graphWidth, int graphHeight, bool isLogXScale, bool isLogYScale, bool showScale, bool showGrid);
	void setXAxisTitle(const char * title);
	void setYAxisTitle(const char * title);
	QList<GraphLineItem *> lineItems() const;
	GraphLineItem * addLineItem();
	void getGraphBoundings(qreal *x1, qreal *y1, qreal *x2, qreal *y2);
	void getGraphRanges(qreal *x_min, qreal *y_min, qreal *x_max, qreal *y_max);
	QGraphicsScene * getGraphicsScenePointer();
	qreal getXScaleFactor();
	qreal getYScaleFactor();
	bool getIsLogXScale();
	bool getIsLogYScale();
	void updateLineItems();

protected:
    void wheelEvent(QWheelEvent *event);
    void drawBackground(QPainter *painter, const QRectF &rect);

    void scaleView(qreal scaleFactor);

private:
	void drawScale();		// function which draws the x and y axis and its scaleTicks
	void calcDrawScaleTicks(qreal minvalue, qreal maxvalue, bool isXaxis);
	void calcDrawLogScaleTicks(qreal minvalue, qreal maxvalue, bool isXaxis);
	QGraphicsScene *scene;  // pointer to the scene in which the graph is drawn
	qreal x_screen_range;	// variables for the screen range (graph range + space for text)
	qreal y_screen_range;
	qreal x_graph_range;	// variables for the graph range (where points are allowed)
	qreal y_graph_range;
	qreal x_min;			// axis minimum and maximum variables
	qreal y_min;
	qreal x_max;
	qreal y_max;
	qreal x_range;			// axis ranges (min to max)
	qreal y_range;
	qreal x_scale_factor;	// axis scale factor to display points accordingly
	qreal y_scale_factor;
	qreal x1_graph;			// variables of the graph bounding rectangle
	qreal y1_graph;
	qreal x2_graph;
	qreal y2_graph;
	bool showScale;
	bool showGrid;
	bool isLogXScale;
	bool isLogYScale;
	bool x_log_neg;
	bool y_log_neg;
	QList<GraphLineItem *> lineItemList;
	QList<QGraphicsItem *> scaleItemsList;
};

#endif