/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef GRAPHNODE_H
#define GRAPHNODE_H

#include <QtGui>
#include <QGraphicsWidget>
#include <QList>

class GraphEdge;
class GraphWidget;
QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

class GraphNode : public QGraphicsWidget
{
	Q_OBJECT

public:
	enum LockDirection
	{
		NONE,
		Y,
		X,
		POS_LOCKED,
		X_EQUAL_Y
	};

    GraphNode(GraphWidget *graphWidget);

    void addEdge(GraphEdge *edge);
    QList<GraphEdge *> edges() const;

    enum { Type = UserType + 1 };
    int type() const { return Type; }

    bool advance();

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
	void setPosLock(int value);
	void setXPrevNode(GraphNode * prev);
	void setXNextNode(GraphNode * next);
	void setYPrevNode(GraphNode * prev);
	void setYNextNode(GraphNode * next);
	void setStyle(QColor &color, int size);
	void setName(const QString & name);
	void setGraphScalePos(qreal x, qreal y);
	QPointF getGraphScalePos();
	QPointF getInitialPos();
	QString getName();
	void emitNodePos();

signals:
    void signalNodePosition(qreal x, qreal y);

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    
private:
	void displayNodePosition(qreal x, qreal y);
	GraphNode *prevNode_X, *nextNode_X, *prevNode_Y, *nextNode_Y;
    QList<GraphEdge *> edgeList;
	QGraphicsItem * itemNodePos;
    QPointF newPos;
    GraphWidget *graph;
	int PosLock;
	QColor nodeColor;
	int nodeSize;
	QString name;
	QPointF graphScalePos;
	QPointF initialPos;
	QGraphicsSimpleTextItem * text;
};

#endif
