/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>

#include "GraphEdge.h"
#include "GraphNode.hxx"
#include "GraphWidget.hxx"

#include <math.h>

GraphNode::GraphNode(GraphWidget *graphWidget)
	: graph(graphWidget)
{
    setFlag(ItemIsMovable);
	setFlag(ItemSendsGeometryChanges);
	prevNode_X = NULL;
	nextNode_X = NULL;
	prevNode_Y = NULL;
	nextNode_Y = NULL;
	itemNodePos = NULL;
	PosLock = GraphNode::POS_LOCKED;
    setCacheMode(DeviceCoordinateCache);
    setZValue(1);
	nodeColor = QColor(0, 82, 89, 255);
}

void GraphNode::addEdge(GraphEdge *edge)
{
    edgeList << edge;
    edge->adjust();
}

QList<GraphEdge *> GraphNode::edges() const
{
    return edgeList;
}

bool GraphNode::advance()
{
	QPointF point;
	point = pos();
    if (newPos == point)
        return false;

	if (PosLock == GraphNode::Y)
	{
		newPos.setX(point.x());
		setPos(newPos);
	}
	else if (PosLock == GraphNode::X)
	{
		newPos.setY(point.y());
		setPos(newPos);
	}
	else
	{
		setPos(newPos);
	}
    return true;
}

void GraphNode::setPosLock(int value)
{
	PosLock = value;
}

void GraphNode::setXPrevNode(GraphNode * prev)
{
	prevNode_X = prev;
}

void GraphNode::setXNextNode(GraphNode * next)
{
	nextNode_X = next;
}

void GraphNode::setYPrevNode(GraphNode * prev)
{
	prevNode_Y = prev;
}

void GraphNode::setYNextNode(GraphNode * next)
{
	nextNode_Y = next;
}

void GraphNode::setStyle(QColor &color, int size)
{
	nodeColor = color;
	nodeSize = size;
}

void GraphNode::setGraphScalePos(qreal x, qreal y)
{
	qreal xPos, yPos;
	qreal x1, x2, y1, y2;

	/* get bounding area of the graph itself */
	graph->getGraphBoundings(&x1, &y1, &x2, &y2);

	graphScalePos.setX(x);
	graphScalePos.setY(y);

	if (graph->getIsLogXScale())
	{
		if (abs(x) == 1)
		{
			x = 1.1;
		}
		if (x < 0)
		{
			xPos = (-1)*log10(abs(x));
		}
		else
		{
			xPos = log10(x);
		}
	}
	else
	{
		xPos = x;
	}
	if (graph->getIsLogYScale())
	{
		if (abs(y) == 1)
		{
			y = 1.1;
		}
		if (y < 0)
		{
			yPos = (-1)*log10(abs(y));
		}
		else
		{
			yPos = log10(y);
		}
	}
	else
	{
		yPos = y;
	}

	xPos *= graph->getXScaleFactor();
	yPos *= graph->getYScaleFactor();

	/* check if node is still within the graph area, if not block further movement */
	if (xPos < x1)
	{
		xPos = x1;
	}
	else if (xPos > x2)
	{
		xPos = x2;
	}

	if (yPos < y1)
	{
		yPos = y1;
	}
	else if (yPos > y2)
	{
		yPos = y2;
	}
	/* set new node position */
	this->setPos(xPos, yPos);
}

QPointF GraphNode::getGraphScalePos()
{
	return graphScalePos;
}

QPointF GraphNode::getInitialPos()
{
	return initialPos;
}

void GraphNode::setName(const QString & name)
{
	this->name = name;
}

QString GraphNode::getName()
{
	return name;
}

void GraphNode::emitNodePos()
{
	emit signalNodePosition(graphScalePos.x(), graphScalePos.y());
}

QRectF GraphNode::boundingRect() const
{
    qreal adjust = 0;
	return QRectF(-(nodeSize/2) - adjust, -(nodeSize/2) - adjust,
                  nodeSize+1 + adjust, nodeSize+1 + adjust);
}

QPainterPath GraphNode::shape() const
{
    QPainterPath path;
	path.addRoundedRect(-(nodeSize/2), -(nodeSize/2), nodeSize, nodeSize, 40.0, 40.0, Qt::RelativeSize);
    //path.addEllipse(-10, -10, 20, 20);
    return path;
}

void GraphNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::darkGray);
	//painter->drawRoundedRect(-(nodeSize/2)+1, -(nodeSize/2)+1, nodeSize, nodeSize, 40.0, 40.0, Qt::RelativeSize);
    //painter->drawEllipse(-7, -7, 20, 20);

    QRadialGradient gradient(-3, -3, 10);
    if (option->state & QStyle::State_Sunken) {
        gradient.setCenter(3, 3);
        gradient.setFocalPoint(3, 3);
		gradient.setColorAt(1, QColor(201, 38, 20, 255));
		gradient.setColorAt(0, QColor(251, 38, 20, 255));
    } else {
		gradient.setColorAt(0, nodeColor.lighter(120));
        gradient.setColorAt(1, nodeColor);
    }
    painter->setBrush(gradient);
	painter->setPen(QPen(Qt::NoPen));
	painter->drawRoundedRect(-(nodeSize/2), -(nodeSize/2), nodeSize, nodeSize, 40.0, 40.0, Qt::RelativeSize);
    //painter->drawEllipse(-10, -10, 20, 20);
}

QVariant GraphNode::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
    case ItemPositionHasChanged:
        foreach (GraphEdge *edge, edgeList)
            edge->adjust();
        break;
    default:
        break;
    };

    return QGraphicsItem::itemChange(change, value);
}

void GraphNode::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	/* display position in graph */
	displayNodePosition(this->pos().x(), this->pos().y()); 
	QGraphicsItem::update();
    QGraphicsItem::mousePressEvent(event);
}

void GraphNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//QGraphicsScene * scene;
	/*if (itemNodePos != NULL)
	{
		scene = graph->getGraphicsScenePointer();
		scene->removeItem(itemNodePos);
		itemNodePos = NULL;
	}*/
	QGraphicsItem::update();
    QGraphicsItem::mouseReleaseEvent(event);
}

void GraphNode::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	QPointF currentParentPos;
	qreal x1, y1, x2, y2;

	/* get bounding area of the graph itself */
	graph->getGraphBoundings(&x1, &y1, &x2, &y2);

	/* get initial position (before mouse movement) */
	initialPos = this->pos();

	/* get new position within scene */
	currentParentPos = this->mapToParent(this->mapFromScene(event->scenePos()));
	
	/* check position locks and modify new position if movement is not allowed */
	if (PosLock == GraphNode::Y)
	{
		currentParentPos.setY(initialPos.y());
	}
	else if (PosLock == GraphNode::X)
	{
		currentParentPos.setX(initialPos.x());
	}
	else if (PosLock == GraphNode::POS_LOCKED)
	{
		currentParentPos.setX(initialPos.x());
		currentParentPos.setY(initialPos.y());
	}
	else if (PosLock == GraphNode::X_EQUAL_Y)
	{
		currentParentPos.setY((-1)*currentParentPos.x());
	}

	/* check X-axis node dependencies */
	if (this->prevNode_X != NULL)
	{
		if (currentParentPos.x() < this->prevNode_X->pos().x())
		{
			currentParentPos.setX(this->prevNode_X->pos().x());
		}
	}
	if (this->nextNode_X != NULL)
	{
		if (currentParentPos.x() > this->nextNode_X->pos().x())
		{
			currentParentPos.setX(this->nextNode_X->pos().x());
		}
	}
	/* check Y-axis node dependencies */
	if (this->prevNode_Y != NULL)
	{
		if (currentParentPos.y() < this->prevNode_Y->pos().y())
		{
			currentParentPos.setY(this->prevNode_Y->pos().y());
			if (PosLock == GraphNode::X_EQUAL_Y)
			{
				currentParentPos.setX((-1)*currentParentPos.y());
			}
		}
	}
	if (this->nextNode_Y != NULL)
	{
		if (currentParentPos.y() > this->nextNode_Y->pos().y())
		{
			currentParentPos.setY(this->nextNode_Y->pos().y());
			if (PosLock == GraphNode::X_EQUAL_Y)
			{
				currentParentPos.setX((-1)*currentParentPos.y());
			}
		}
	}

	/* check if node is still within the graph area, if not block further movement */
	if (currentParentPos.x() < x1)
	{
		currentParentPos.setX(x1);
	}
	else if (currentParentPos.x() > x2)
	{
		currentParentPos.setX(x2);
	}

	if (currentParentPos.y() < y1)
	{
		currentParentPos.setY(y1);
	}
	else if (currentParentPos.y() > y2)
	{
		currentParentPos.setY(y2);
	}

	/* only 45 degrees direction is allowed */
	if (PosLock == GraphNode::X_EQUAL_Y)
	{
		currentParentPos.setY((-1)*currentParentPos.x());
	}

	/* set new position */
	this->setPos(currentParentPos);// - buttonDownParentPos);

	/* display position in graph */
	displayNodePosition(currentParentPos.x(), currentParentPos.y()); 
}

void GraphNode::displayNodePosition(qreal x, qreal y)
{
	QFont * font = new QFont();
	QString message;
	QString x_val;
	QString y_val;
	QGraphicsScene * scene;
	qreal x_scale_factor, y_scale_factor;
	qreal xPos, yPos;

	scene = graph->getGraphicsScenePointer();
	x_scale_factor = graph->getXScaleFactor();
	y_scale_factor = graph->getYScaleFactor();

	/*if (itemNodePos != NULL)
		scene->removeItem(itemNodePos);*/

	font->setPointSize(6);
	if (graph->getIsLogXScale())
	{
		if (x < 0)
		{
			x_val.setNum((-1)*(int)pow(10, (abs(x)/x_scale_factor)));
			xPos = (-1)*(int)pow(10, (abs(x)/x_scale_factor));
		}
		else
		{
			x_val.setNum((int)pow(10, (x/x_scale_factor)));
			xPos = (int)pow(10, (x/x_scale_factor));
		}
	}
	else
	{
		x_val.setNum(floor((x/x_scale_factor)*100)/100);
		xPos = x/x_scale_factor;
	}
	message.append("(");
	message.append(&x_val);
	message.append(", ");

	if (graph->getIsLogYScale())
	{
		if (y < 0)
		{
			y_val.setNum((-1)*(int)pow(10, (abs(y)/y_scale_factor)));
			yPos = (-1)*(int)pow(10, (abs(y)/y_scale_factor));
		}
		else
		{
			y_val.setNum((int)pow(10, (y/y_scale_factor)));
			yPos = (int)pow(10, (y/y_scale_factor));
		}
	}
	else
	{
		y_val.setNum(floor((y/y_scale_factor)*100)/100);
		yPos = y/y_scale_factor;
	}
	message.append(&y_val);
	message.append(")");
	/*text = scene->addSimpleText(message, *font);
	text->setPos(x+10, y+10);
	itemNodePos = text;*/

	graphScalePos.setX(xPos);
	graphScalePos.setY(yPos);

	// signal new position
	emit signalNodePosition(xPos, yPos);
}
