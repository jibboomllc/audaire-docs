/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>

#include "GraphEdge.h"
#include "GraphNode.hxx"
#include "GraphWidget.hxx"
#include "GraphLineItem.h"

#include <math.h>

GraphLineItem::GraphLineItem(GraphWidget *graphWidget, QGraphicsScene * scene)
{
	this->graph = graphWidget;
    this->scene = scene;
	nodeColor = QColor(Qt::black);
	edgeColor = QColor(Qt::gray);
}

GraphNode * GraphLineItem::addNode(qreal x, qreal y)
{
	qreal x_min, y_min, x_max, y_max;
	GraphNode *node = new GraphNode(this->graph);

	graph->getGraphRanges(&x_min, &y_min, &x_max, &y_max);

	if ((x > x_max) || (x < x_min) || (y > y_max) || (y < y_min))
	{

		return NULL;
	}

	if (graph->getIsLogXScale())
	{
		if ((x > -1) && (x < 1))
		{
			return NULL;
		}
	}
	if (graph->getIsLogYScale())
	{
		if ((y > -1) && (y < 1))
		{
			return NULL;
		}
	}

	node->setGraphScalePos(x, y);

	scene->addItem(node);
	// add node to list
	nodeList << node;
	// set node style
	node->setStyle(nodeColor, nodeSize);

	return node;
}

void GraphLineItem::addEdge(GraphNode *sourceNode, GraphNode *destNode)
{
	GraphEdge * edge = new GraphEdge(sourceNode, destNode);
	if ((sourceNode != NULL) && (destNode != NULL))
	{
		scene->addItem(edge);
		edge->setStyle(edgeColor, edgePenStyle, edgeWidth, edgeArrowSize);
		edgeList << edge;
	}
}

void GraphLineItem::addNodeXAxisDependency(GraphNode *node, GraphNode *prevNode, GraphNode *nextNode)
{
	if (node != NULL)
	{
		node->setXPrevNode(prevNode);
		node->setXNextNode(nextNode);
	}
}

void GraphLineItem::addNodeYAxisDependency(GraphNode *node, GraphNode *prevNode, GraphNode *nextNode)
{
	if (node != NULL)
	{
		node->setYPrevNode(prevNode);
		node->setYNextNode(nextNode);
	}
}

void GraphLineItem::makeNodeMoveable(GraphNode *node, bool XDirectionMoveable, bool YDirectionMoveable)
{
	if (node != NULL)
	{
		if (XDirectionMoveable && YDirectionMoveable)
			node->setPosLock(0);
		else if (XDirectionMoveable)
			node->setPosLock(1);
		else if (YDirectionMoveable)
			node->setPosLock(2);
		else
			node->setPosLock(3);
	}
}

void GraphLineItem::setLineItemStyle(QColor &nodeColor, QColor &edgeColor, Qt::PenStyle edgePenStyle, qreal nodeSize, qreal edgeWidth, qreal edgeArrowSize)
{
	this->nodeColor = nodeColor;
	this->edgeColor = edgeColor;
	this->edgePenStyle = edgePenStyle;
	this->nodeSize = nodeSize;
	this->edgeWidth = edgeWidth;
	this->edgeArrowSize = edgeArrowSize;
	foreach (GraphNode *node, nodeList)
	{
		node->setStyle(nodeColor, nodeSize);
	}
	foreach (GraphEdge *edge, edgeList)
	{
		edge->setStyle(edgeColor, edgePenStyle, edgeWidth, edgeArrowSize);
	}
}

QList<GraphNode *> GraphLineItem::nodes() const
{
    return nodeList;
}

QList<GraphEdge *> GraphLineItem::edges() const
{
    return edgeList;
}

void GraphLineItem::removeLineItem()
{
	foreach(GraphEdge* edge, edgeList)
	{
		scene->removeItem(edge);
	}
	edgeList.clear();
	foreach(GraphNode* node, nodeList)
	{
		scene->removeItem(node);
	}
	nodeList.clear();
}
