/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "GraphWidget.hxx"
#include "GraphNode.hxx"
#include "GraphEdge.h"
#include "GraphLineItem.h"

#include <QDebug>
#include <QGraphicsScene>
#include <QWheelEvent>

#include <math.h>

#define SCALE_AREA_WIDTH_X 50
#define SCALE_AREA_WIDTH_Y 50

GraphWidget::GraphWidget(QWidget *parent) : QGraphicsView(parent)
{
    scene = new QGraphicsScene(this);
	
	scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    setScene(scene);
    setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    setResizeAnchor(AnchorViewCenter);
	showScale = false;
	isLogXScale = false;
	isLogYScale = false;
	x_log_neg = false;
	y_log_neg = false;
}

GraphLineItem * GraphWidget::addLineItem()
{
	GraphLineItem *lineItem = new GraphLineItem(this, scene);
	lineItemList << lineItem;
	return lineItem;
}

QList<GraphLineItem *> GraphWidget::lineItems() const
{
	return lineItemList;
}

void GraphWidget::setScale(qreal x_min, qreal y_min, qreal x_max, qreal y_max, int graphWidth, int graphHeight, bool isLogXScale, bool isLogYScale, bool showScale, bool showGrid)
{
	qreal x_range, y_range;	

	this->isLogXScale = isLogXScale;
	this->isLogYScale = isLogYScale;
	this->showScale = showScale;
	this->showGrid = showGrid;

	x_screen_range = graphWidth;
	y_screen_range = graphHeight;
	x_graph_range = x_screen_range - 2*SCALE_AREA_WIDTH_X;
	y_graph_range = y_screen_range - 2*SCALE_AREA_WIDTH_Y;

	if (isLogXScale)
	{
		if (x_max <= -1)
		{
			x_log_neg = true;
		}
		if ((x_min > -1) && (x_min < 1))
		{
			if (x_max > 0)
			{
				x_min = 1;
			}
			else
			{
				x_min = -1;
			}
		}
		if ((x_max > -1) && (x_max < 0))
		{
			x_max = -1;
		}
		if ((x_max >= 0) && (x_max < 1))
		{
			x_max = 1;
		}
	}
	if (isLogYScale)
	{
		if (y_max <= -1)
		{
			y_log_neg = true;
		}
		if ((y_min > -1) && (y_min < 1))
		{
			if (y_max > 0)
			{
				y_min = 1;
			}
			else
			{
				y_min = -1;
			}
		}
		if ((y_max > -1) && (y_max < 0))
		{
			y_max = -1;
		}
		if ((y_max >= 0) && (y_max < 1))
		{
			y_max = 1;
		}
	}

	this->x_min = x_min;
	this->y_min = y_min;
	this->x_max = x_max;
	this->y_max = y_max;

	if (isLogXScale)
	{
		if (x_log_neg)
		{
			x_range = abs((-1)*log10(abs(x_min)) - (-1)*log10(abs(x_max)));
		}
		else
		{
			x_range = abs(log10(x_min) - log10(x_max));
		}
	}
	else
	{
		x_range = abs(x_min - x_max);
	}
		
	if (isLogYScale)
	{
		if (y_log_neg)
		{
			y_range = abs((-1)*log10(abs(y_min)) - (-1)*log10(abs(y_max)));
		}
		else
		{
			y_range = abs(log10(y_min) - log10(y_max));
		}
	}
	else
	{
		y_range = abs(y_min - y_max);
	}

	x_scale_factor = x_graph_range/x_range;
	y_scale_factor = (-1)*y_graph_range/y_range;

	if (isLogXScale)
	{
		if (x_log_neg)
		{
			x1_graph = (-1)*log10(abs(this->x_min))*x_scale_factor;
			x2_graph = (-1)*log10(abs(this->x_max))*x_scale_factor;
		}
		else
		{
			x1_graph = log10(this->x_min)*x_scale_factor;
			x2_graph = log10(this->x_max)*x_scale_factor;
		}		
	}
	else
	{
		x1_graph = this->x_min*x_scale_factor;
		x2_graph = this->x_max*x_scale_factor;
	}
	if (isLogYScale)
	{
		if (y_log_neg)
		{
			y1_graph = (-1)*log10(abs(this->y_max))*y_scale_factor;	
			y2_graph = (-1)*log10(abs(this->y_min))*y_scale_factor;
		}
		else
		{
			y1_graph = log10(this->y_max)*y_scale_factor;	
			y2_graph = log10(this->y_min)*y_scale_factor;
		}		
	}
	else
	{
		y1_graph = this->y_max*y_scale_factor;	
		y2_graph = this->y_min*y_scale_factor;
	}

	// clear old scale items, if scale was drawn before
	for (int i=0; i < scaleItemsList.length(); i++)
	{
		scene->removeItem(scaleItemsList.at(i));
	}
	scaleItemsList.clear();

	// reset scene, so that also negativ values can be shown
	scene->setSceneRect(x1_graph - SCALE_AREA_WIDTH_X, y1_graph - SCALE_AREA_WIDTH_Y, x_screen_range, y_screen_range);

	drawScale();
}

void GraphWidget::drawScale()
{
	QPen * pen = new QPen();
	QBrush * brush = new QBrush();
	QLine * line = new QLine();
	QRectF * rect = new QRectF();
	QFont * font = new QFont();
	QString message;
	QGraphicsSimpleTextItem * text;

	font->setPointSize(8);
	pen->setWidth(2);
	// draw x axis
	line->setLine(x1_graph, y2_graph, x2_graph, y2_graph);
	scaleItemsList.append(scene->addLine(*line, *pen));
	// draw y axis
	line->setLine(x1_graph, y2_graph, x1_graph, y1_graph);
	scaleItemsList.append(scene->addLine(*line, *pen));
	//draw box around graph area
	pen->setWidth(1);
	brush->setStyle(Qt::NoBrush);
	rect->setCoords(x1_graph, y1_graph, x2_graph, y2_graph);
	scaleItemsList.append(scene->addRect(*rect, *pen, *brush));
	/* draw 0 lines */
	if ((y_min <= 0) && (y_max >= 0))
	{
		line->setLine(x1_graph, 0, x2_graph, 0);
		scaleItemsList.append(scene->addLine(*line, *pen));
		message.setNum(0);
		text = scene->addSimpleText(message, *font);
		scaleItemsList.append(text);
		if (isLogXScale)
		{
			text->setPos(log10(x_min)*x_scale_factor - message.length()*5 - 10, 0 - 7);
		}
		else
		{
			text->setPos(x_min*x_scale_factor - message.length()*5 - 10, 0 - 7);
		}
	}
	if ((x_min <= 0) && (x_max >= 0))
	{
		line->setLine(0, y2_graph, 0, y1_graph);
		scaleItemsList.append(scene->addLine(*line, *pen));
		message.setNum(0);
		text = scene->addSimpleText(message, *font);
		scaleItemsList.append(text);
		if (isLogYScale)
		{
			text->setPos(0 - (message.length()*5)/2, log10(y_min)*y_scale_factor + 5);
		}
		else
		{
			text->setPos(0 - (message.length()*5)/2, y_min*y_scale_factor + 5);
		}
	}

	if (showScale)
	{		
		if (isLogXScale)
			calcDrawLogScaleTicks(x_min, x_max, true);
		else
			calcDrawScaleTicks(x_min, x_max, true);

		if (isLogYScale)
			calcDrawLogScaleTicks(y_min, y_max, false);
		else
			calcDrawScaleTicks(y_min, y_max, false);
	}
}

void GraphWidget::calcDrawScaleTicks(qreal minvalue, qreal maxvalue, bool isXaxis)
{
	QLine * line = new QLine();
	QFont * font = new QFont();
	QPen * pen = new QPen();
	QString message;
	QGraphicsSimpleTextItem * text;
	qreal number;
	qreal number_factor_min, number_factor_max;
	int digits;
	int maxTicks;
	qreal tick, tick_min, tick_max, tick_help;
	int tick_try;

	font->setPointSize(8);

	number_factor_min = 1;
	number_factor_max = 1;

	/* get scale ticks for negative range */
	number = minvalue;
	if (number != 0)
	{
		if (abs(number) < 1000)
		{			
			while (abs(number) < 1000)
			{
				number_factor_min *= 10;
				number *= 10;
			}
		}
		digits = 0;
		while(abs(number) >= 10)
		{
			number /= 10;
			digits++;
		}

		tick_try = number * (int)pow((float)10, digits);
		while(tick_try%10 == 0)
		{
			tick_try /= 2;
		}
		tick_try *= 2;
		if (isXaxis)
			maxTicks = abs(minvalue*x_scale_factor)/(digits * 12);
		else
			maxTicks = abs(minvalue*y_scale_factor)/(digits * 10);

		if (maxTicks == 0)
		{
			maxTicks = 1;
		}
		while (abs(minvalue/(tick_try/number_factor_min)) > maxTicks)
		{
			tick_try *= 2;
		}
		tick_min = tick_try;
	}
	else
	{
		tick_min = 0;
	}
	
	/* get scale ticks for positive range */
	number = maxvalue;
	if (number != 0)
	{
		if (abs(number) < 1000)
		{
			while (abs(number) < 1000)
			{
				number_factor_max *= 10;
				number *= 10;
			}
		}
		digits = 0;
		while(abs(number) >= 10)
		{
			number /= 10;
			digits++;
		}

		tick_try = number * (int)pow((float)10, digits);
		while(tick_try%10 == 0)
		{
			tick_try /= 2;
		}
		tick_try *= 2;

		if (isXaxis)
			maxTicks = abs(maxvalue*x_scale_factor)/(digits * 12);
		else
			maxTicks = abs(maxvalue*y_scale_factor)/(digits * 10);

		if (maxTicks == 0)
		{
			maxTicks = 1;
		}
		while (abs(maxvalue/(tick_try/number_factor_max)) > maxTicks)
		{
			tick_try *= 2;
		}
		tick_max = tick_try;
	}
	else
	{
		tick_max = 0;
	}

	/* check which scale is smaller and use this for the complete axis */
	if ((abs(tick_max) >= abs(tick_min)) && (tick_min != 0))
	{
		tick = abs(tick_min);
		tick_try = abs(tick_min);
	}
	else if (tick_max == 0)
	{
		tick = abs(tick_min);
		tick_try = abs(tick_min);
	}
	else
	{
		tick = abs(tick_max);
		tick_try = abs(tick_max);
	}

	if (tick > 0)
	{
		tick_help = tick;
		tick /= number_factor_min;
		while ((tick) <= abs(minvalue))
		{
			if (isXaxis)
			{
				if (showGrid)
				{
					pen->setWidthF(0.5);
					pen->setStyle(Qt::DotLine);
					if (isLogYScale)
					{
						line->setLine(tick*(-1)*x_scale_factor, log10(y_min)*y_scale_factor, tick*(-1)*x_scale_factor, log10(y_max)*y_scale_factor);
					}
					else
					{
						line->setLine(tick*(-1)*x_scale_factor, y_min*y_scale_factor, tick*(-1)*x_scale_factor, y_max*y_scale_factor);
					}
					scaleItemsList.append(scene->addLine(*line, *pen));
				}
				pen->setWidth(2);
				pen->setStyle(Qt::SolidLine);
				if (isLogYScale)
				{
					line->setLine(tick*(-1)*x_scale_factor, log10(y_min)*y_scale_factor - 5, tick*(-1)*x_scale_factor, log10(y_min)*y_scale_factor + 5);
				}
				else
				{
					line->setLine(tick*(-1)*x_scale_factor, y_min*y_scale_factor - 5, tick*(-1)*x_scale_factor, y_min*y_scale_factor + 5);
				}
			}
			else
			{
				if (showGrid)
				{
					pen->setWidthF(0.5);
					pen->setStyle(Qt::DotLine);
					if (isLogXScale)
					{
						line->setLine(log10(x_min)*x_scale_factor, tick*(-1)*y_scale_factor, log10(x_max)*x_scale_factor, tick*(-1)*y_scale_factor);
					}
					else
					{
						line->setLine(x_min*x_scale_factor, tick*(-1)*y_scale_factor, x_max*x_scale_factor, tick*(-1)*y_scale_factor);
					}
					scaleItemsList.append(scene->addLine(*line, *pen));
				}
				pen->setWidth(2);
				pen->setStyle(Qt::SolidLine);
				if (isLogXScale)
				{
					line->setLine(log10(x_min)*x_scale_factor - 5, tick*(-1)*y_scale_factor, log10(x_min)*x_scale_factor + 5, tick*(-1)*y_scale_factor);
				}
				else
				{
					line->setLine(x_min*x_scale_factor - 5, tick*(-1)*y_scale_factor, x_min*x_scale_factor + 5, tick*(-1)*y_scale_factor);
				}
			}
			scaleItemsList.append(scene->addLine(*line, *pen));
			message.setNum(tick*(-1));
			text = scene->addSimpleText(message, *font);
			scaleItemsList.append(text);
			if (isXaxis)
			{
				if (isLogYScale)
				{
					text->setPos(tick*(-1)*x_scale_factor - (message.length()*5)/2, log10(y_min)*y_scale_factor + 5);
				}
				else
				{
					text->setPos(tick*(-1)*x_scale_factor - (message.length()*5)/2, y_min*y_scale_factor + 5);
				}
			}
			else
			{
				if (isLogXScale)
				{
					text->setPos(log10(x_min)*x_scale_factor - message.length()*5 - 10, tick*(-1)*y_scale_factor - 7);
				}
				else
				{
					text->setPos(x_min*x_scale_factor - message.length()*5 - 10, tick*(-1)*y_scale_factor - 7);
				}
			}
			tick += (qreal)tick_try/number_factor_min;
		}
		tick = tick_help;
		tick /= number_factor_max;
		while ((tick) <= abs(maxvalue))
		{
			if (isXaxis)
			{
				if (showGrid)
				{
					pen->setWidthF(0.5);
					pen->setStyle(Qt::DotLine);
					if (isLogYScale)
					{
						line->setLine(tick*x_scale_factor, log10(y_min)*y_scale_factor, tick*x_scale_factor, log10(y_max)*y_scale_factor);
					}
					else
					{
						line->setLine(tick*x_scale_factor, y_min*y_scale_factor, tick*x_scale_factor, y_max*y_scale_factor);
					}
					scaleItemsList.append(scene->addLine(*line, *pen));
				}
				pen->setWidth(2);
				pen->setStyle(Qt::SolidLine);
				if (isLogYScale)
				{
					line->setLine(tick*x_scale_factor, log10(y_min)*y_scale_factor - 5, tick*x_scale_factor, log10(y_min)*y_scale_factor + 5);
				}
				else
				{
					line->setLine(tick*x_scale_factor, y_min*y_scale_factor - 5, tick*x_scale_factor, y_min*y_scale_factor + 5);
				}
			}
			else
			{
				if (showGrid)
				{
					pen->setWidthF(0.5);
					pen->setStyle(Qt::DotLine);
					if (isLogXScale)
					{
						line->setLine(log10(x_min)*x_scale_factor, tick*y_scale_factor, log10(x_max)*x_scale_factor, tick*y_scale_factor);
					}
					else
					{
						line->setLine(x_min*x_scale_factor, tick*y_scale_factor, x_max*x_scale_factor, tick*y_scale_factor);
					}
					scaleItemsList.append(scene->addLine(*line, *pen));
				}
				pen->setWidth(2);
				pen->setStyle(Qt::SolidLine);
				if (isLogXScale)
				{
					line->setLine(log10(x_min)*x_scale_factor - 5, tick*y_scale_factor, log10(x_min)*x_scale_factor + 5, tick*y_scale_factor);
				}
				else
				{
					line->setLine(x_min*x_scale_factor - 5, tick*y_scale_factor, x_min*x_scale_factor + 5, tick*y_scale_factor);
				}
			}
			scaleItemsList.append(scene->addLine(*line, *pen));
			message.setNum(tick);
			text = scene->addSimpleText(message, *font);
			scaleItemsList.append(text);
			if (isXaxis)
			{
				if (isLogYScale)
				{
					text->setPos(tick*x_scale_factor - (message.length()*5)/2, log10(y_min)*y_scale_factor + 5);
				}
				else
				{
					text->setPos(tick*x_scale_factor - (message.length()*5)/2, y_min*y_scale_factor + 5);
				}
			}
			else
			{
				if (isLogXScale)
				{
					text->setPos(log10(x_min)*x_scale_factor - message.length()*5 - 10, tick*y_scale_factor - 7);
				}
				else
				{
					text->setPos(x_min*x_scale_factor - message.length()*5 - 10, tick*y_scale_factor - 7);
				}
			}
			tick += (qreal)tick_try/number_factor_max;
		}
	}
}

void GraphWidget::calcDrawLogScaleTicks(qreal minvalue, qreal maxvalue, bool isXaxis)
{
	QLine * line = new QLine();
	QFont * font = new QFont();
	QPen * pen = new QPen();
	QString message;
	QGraphicsSimpleTextItem * text;
	double tick;
	bool was_bigger;

	// calculate negative scale
	tick = 1.0;
	/* if maxvalue of scale is smaller than 0 */
	while((-1)*tick > maxvalue)
	{
		tick *= 10;
	}	
	was_bigger = false;

	if (minvalue > -1)
	{
		was_bigger = true;	// skip negative scale as there is none
	}

	while (was_bigger == false)
	{
		tick *= 10;
		if (tick*10 >= abs(minvalue))
		{
			was_bigger = true;
		}
		if (isXaxis)
		{
			if (showGrid)
			{
				pen->setWidthF(0.5);
				pen->setStyle(Qt::DotLine);
				if (isLogYScale)
				{
					if (y_log_neg)
					{
						line->setLine(log10(tick)*(-1)*x_scale_factor, (-1)*log10(abs(y_min))*y_scale_factor, log10(tick)*(-1)*x_scale_factor, (-1)*log10(abs(y_max))*y_scale_factor);
					}
					else
					{
						line->setLine(log10(tick)*(-1)*x_scale_factor, log10(y_min)*y_scale_factor, log10(tick)*(-1)*x_scale_factor, log10(y_max)*y_scale_factor);
					}
				}
				else
				{
					line->setLine(log10(tick)*(-1)*x_scale_factor, y_min*y_scale_factor, log10(tick)*(-1)*x_scale_factor, y_max*y_scale_factor);
				}
				scaleItemsList.append(scene->addLine(*line, *pen));
			}
			pen->setWidth(2);
			pen->setStyle(Qt::SolidLine);
			if (isLogYScale)
			{
				if (y_log_neg)
				{
					line->setLine(log10(tick)*(-1)*x_scale_factor, (-1)*log10(abs(y_min))*y_scale_factor - 5, log10(tick)*(-1)*x_scale_factor, (-1)*log10(abs(y_min))*y_scale_factor + 5);
				}
				else
				{
					line->setLine(log10(tick)*(-1)*x_scale_factor, log10(y_min)*y_scale_factor - 5, log10(tick)*(-1)*x_scale_factor, log10(y_min)*y_scale_factor + 5);
				}
			}
			else
			{
				line->setLine(log10(tick)*(-1)*x_scale_factor, y_min*y_scale_factor - 5, log10(tick)*(-1)*x_scale_factor, y_min*y_scale_factor + 5);
			}
		}
		else
		{
			if (showGrid)
			{
				pen->setWidthF(0.5);
				pen->setStyle(Qt::DotLine);
				if (isLogXScale)
				{
					if (x_log_neg)
					{
						line->setLine((-1)*log10(abs(x_min))*x_scale_factor, log10(tick)*(-1)*y_scale_factor, (-1)*log10(abs(x_max))*x_scale_factor, log10(tick)*(-1)*y_scale_factor);
					}
					else
					{
						line->setLine(log10(x_min)*x_scale_factor, log10(tick)*(-1)*y_scale_factor, log10(x_max)*x_scale_factor, log10(tick)*(-1)*y_scale_factor);
					}
				}
				else
				{
					line->setLine(x_min*x_scale_factor, log10(tick)*(-1)*y_scale_factor, x_max*x_scale_factor, log10(tick)*(-1)*y_scale_factor);
				}
				scaleItemsList.append(scene->addLine(*line, *pen));
			}
			pen->setWidth(2);
			pen->setStyle(Qt::SolidLine);
			if (isLogXScale)
			{
				if (x_log_neg)
				{
					line->setLine((-1)*log10(abs(x_min))*x_scale_factor - 5, log10(tick)*(-1)*y_scale_factor, (-1)*log10(abs(x_min))*x_scale_factor + 5, log10(tick)*(-1)*y_scale_factor);
				}
				else
				{
					line->setLine(log10(x_min)*x_scale_factor - 5, log10(tick)*(-1)*y_scale_factor, log10(x_min)*x_scale_factor + 5, log10(tick)*(-1)*y_scale_factor);
				}
			}
			else
			{
				line->setLine(x_min*x_scale_factor - 5, log10(tick)*(-1)*y_scale_factor, x_min*x_scale_factor + 5, log10(tick)*(-1)*y_scale_factor);
			}
		}
		scaleItemsList.append(scene->addLine(*line, *pen));
		message.setNum(tick*(-1));
		text = scene->addSimpleText(message, *font);
		scaleItemsList.append(text);
		if (isXaxis)
		{
			if (isLogYScale)
			{
				if (y_log_neg)
				{
					text->setPos(log10(tick)*(-1)*x_scale_factor - (message.length()*5)/2, (-1)*log10(abs(y_min))*y_scale_factor + 5);
				}
				else
				{
					text->setPos(log10(tick)*(-1)*x_scale_factor - (message.length()*5)/2, log10(y_min)*y_scale_factor + 5);
				}
			}
			else
			{
				text->setPos(log10(tick)*(-1)*x_scale_factor - (message.length()*5)/2, y_min*y_scale_factor + 5);
			}
		}
		else
		{
			if (isLogXScale)
			{
				if (x_log_neg)
				{
					text->setPos((-1)*log10(abs(x_min))*x_scale_factor - message.length()*5 - 5, log10(tick)*(-1)*y_scale_factor);
				}
				else
				{
					text->setPos(log10(x_min)*x_scale_factor - message.length()*5 - 5, log10(tick)*(-1)*y_scale_factor);
				}
			}
			else
			{
				text->setPos(x_min*x_scale_factor - message.length()*5 - 5, log10(tick)*(-1)*y_scale_factor);
			}
		}
	}
	// calculate positive scale
	tick = 1.0;
	/* if minvalue of scale is greater than 0 */
	while (tick < minvalue)
	{
		tick *= 10;
	}
	was_bigger = false;
	while (was_bigger == false)
	{
		tick *= 10;
		if (tick*10 >= abs(maxvalue))
		{
			was_bigger = true;
		}
		pen->setWidth(2);
		if (isXaxis)
		{
			if (showGrid)
			{
				pen->setWidthF(0.5);
				pen->setStyle(Qt::DotLine);
				if (isLogYScale)
				{
					if (y_log_neg)
					{
						line->setLine(log10(tick)*x_scale_factor, (-1)*log10(abs(y_min))*y_scale_factor, log10(tick)*x_scale_factor, (-1)*log10(abs(y_max))*y_scale_factor);
					}
					else
					{
						line->setLine(log10(tick)*x_scale_factor, log10(y_min)*y_scale_factor, log10(tick)*x_scale_factor, log10(y_max)*y_scale_factor);
					}
				}
				else
				{
					line->setLine(log10(tick)*x_scale_factor, y_min*y_scale_factor, log10(tick)*x_scale_factor, y_max*y_scale_factor);
				}
				scaleItemsList.append(scene->addLine(*line, *pen));
			}
			pen->setWidth(2);
			pen->setStyle(Qt::SolidLine);
			if (isLogYScale)
			{
				if (y_log_neg)
				{
					line->setLine(log10(tick)*x_scale_factor, (-1)*log10(abs(y_min))*y_scale_factor - 5, log10(tick)*x_scale_factor, (-1)*log10(abs(y_min))*y_scale_factor + 5);
				}
				else
				{
					line->setLine(log10(tick)*x_scale_factor, log10(y_min)*y_scale_factor - 5, log10(tick)*x_scale_factor, log10(y_min)*y_scale_factor + 5);
				}
			}
			else
			{
				line->setLine(log10(tick)*x_scale_factor, y_min*y_scale_factor - 5, log10(tick)*x_scale_factor, y_min*y_scale_factor + 5);
			}
		}
		else
		{
			if (showGrid)
			{
				pen->setWidthF(0.5);
				pen->setStyle(Qt::DotLine);
				if (isLogXScale)
				{
					if (x_log_neg)
					{
						line->setLine((-1)*log10(abs(x_min))*x_scale_factor, log10(tick)*y_scale_factor, (-1)*log10(abs(x_max))*x_scale_factor, log10(tick)*y_scale_factor);
					}
					else
					{
						line->setLine(log10(x_min)*x_scale_factor, log10(tick)*y_scale_factor, log10(x_max)*x_scale_factor, log10(tick)*y_scale_factor);
					}
				}
				else
				{
					line->setLine(x_min*x_scale_factor, log10(tick)*y_scale_factor, x_max*x_scale_factor, log10(tick)*y_scale_factor);
				}
				scaleItemsList.append(scene->addLine(*line, *pen));
			}
			pen->setWidth(2);
			pen->setStyle(Qt::SolidLine);
			if (isLogXScale)
			{
				if (x_log_neg)
				{
					line->setLine((-1)*log10(abs(x_min))*x_scale_factor - 5, log10(tick)*y_scale_factor, (-1)*log10(abs(x_min))*x_scale_factor + 5, log10(tick)*y_scale_factor);
				}
				else
				{
					line->setLine(log10(x_min)*x_scale_factor - 5, log10(tick)*y_scale_factor, log10(x_min)*x_scale_factor + 5, log10(tick)*y_scale_factor);
				}
			}
			else
			{
				line->setLine(x_min*x_scale_factor - 5, log10(tick)*y_scale_factor, x_min*x_scale_factor + 5, log10(tick)*y_scale_factor);
			}
		}
		scaleItemsList.append(scene->addLine(*line, *pen));
		message.setNum(tick);
		text = scene->addSimpleText(message, *font);
		scaleItemsList.append(text);
		if (isXaxis)
		{
			if (isLogYScale)
			{
				if (y_log_neg)
				{
					text->setPos(log10(tick)*x_scale_factor - (message.length()*5)/2, (-1)*log10(abs(y_min))*y_scale_factor + 5);
				}
				else
				{
					text->setPos(log10(tick)*x_scale_factor - (message.length()*5)/2, log10(y_min)*y_scale_factor + 5);
				}
			}
			else
			{
				text->setPos(log10(tick)*x_scale_factor - (message.length()*5)/2, y_min*y_scale_factor + 5);
			}
		}
		else
		{
			if (isLogXScale)
			{
				if (x_log_neg)
				{
					text->setPos((-1)*log10(abs(x_min))*x_scale_factor - message.length()*5 - 5, log10(tick)*y_scale_factor);
				}
				else
				{
					text->setPos(log10(x_min)*x_scale_factor - message.length()*5 - 5, log10(tick)*y_scale_factor);
				}
			}
			else
			{
				text->setPos(x_min*x_scale_factor - message.length()*5 - 5, log10(tick)*y_scale_factor);
			}
		}
	}
}

void GraphWidget::setXAxisTitle(const char * title)
{
	QFont * font = new QFont();
	QPen * pen = new QPen();
	QString message;
	QGraphicsSimpleTextItem * text;
	qreal xPos;

	pen->setWidth(2);
	font->setPointSize(10);
	font->setBold(true);
	message.append(title);
	text = scene->addSimpleText(message, *font);
	scaleItemsList.append(text);
	if (isLogXScale)
	{
		xPos = (log10(x_max)*x_scale_factor - log10(x_min)*x_scale_factor) / 2;
		xPos = log10(x_max)*x_scale_factor - xPos;
	}
	else
	{
		xPos = (x_max*x_scale_factor - x_min*x_scale_factor) / 2;
		xPos = x_max*x_scale_factor - xPos;
	}
	if (isLogYScale)
	{
		text->setPos(xPos - message.length()*4, log10(y_min)*y_scale_factor + 25);
	}
	else
	{
		text->setPos(xPos - message.length()*4, y_min*y_scale_factor + 25);
	}
}

void GraphWidget::setYAxisTitle(const char * title)
{
	QFont * font = new QFont();
	QPen * pen = new QPen();
	QString message;
	QGraphicsSimpleTextItem * text;
	qreal yPos;

	pen->setWidth(2);
	font->setPointSize(10);
	font->setBold(true);
	message.append(title);
	text = scene->addSimpleText(message, *font);
	scaleItemsList.append(text);
	text->rotate(270);
	if (isLogYScale)
	{
		yPos = (log10(y_max)*y_scale_factor*(-1) - log10(y_min)*y_scale_factor*(-1)) / 2;
		yPos = log10(y_max)*y_scale_factor*(-1) - yPos;
		yPos *= (-1);
	}
	else
	{
		yPos = (y_max*y_scale_factor*(-1) - y_min*y_scale_factor*(-1)) / 2;
		yPos = y_max*y_scale_factor*(-1) - yPos;
		yPos *= (-1);
	}
	if (isLogXScale)
	{
		text->setPos(log10(x_min)*x_scale_factor - 50, yPos + message.length()*4);	
	}
	else
	{
		text->setPos(x_min*x_scale_factor - 50, yPos + message.length()*4);	
	}
}

void GraphWidget::updateLineItems()
{
	qreal x, y;
	for (int i=0; i < lineItemList.length(); i++)
	{
		for (int k=0; k < lineItemList.at(i)->nodes().length(); k++)
		{
			x = lineItemList.at(i)->nodes().at(k)->getGraphScalePos().x();
			y = lineItemList.at(i)->nodes().at(k)->getGraphScalePos().y();
			lineItemList.at(i)->nodes().at(k)->setGraphScalePos(x, y);
		}
	}
}

QGraphicsScene * GraphWidget::getGraphicsScenePointer()
{
	return scene;
}

qreal GraphWidget::getXScaleFactor()
{
	return x_scale_factor;
}

qreal GraphWidget::getYScaleFactor()
{
	return y_scale_factor;
}

bool GraphWidget::getIsLogXScale()
{
	return isLogXScale;
}

bool GraphWidget::getIsLogYScale()
{
	return isLogYScale;
}



void GraphWidget::getGraphBoundings(qreal *x1, qreal *y1, qreal *x2, qreal *y2)
{
	*x1 = x1_graph;
	*y1 = y1_graph;
	*x2 = x2_graph;
	*y2 = y2_graph;
}

void GraphWidget::getGraphRanges(qreal *x_min, qreal *y_min, qreal *x_max, qreal *y_max)
{
	*x_min = this->x_min;
	*y_min = this->y_min;
	*x_max = this->x_max;
	*y_max = this->y_max;
}

void GraphWidget::wheelEvent(QWheelEvent *event)
{
	scaleView(pow((double)2, -event->delta() / 480.0));
}

void GraphWidget::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);
    
    QRectF sceneRect = this->sceneRect();

    // Fill
	//painter->fillRect(rect.intersect(sceneRect), QColor(226, 226, 226, 255));
	painter->fillRect(rect.intersect(sceneRect), QColor(Qt::white));
	painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(sceneRect);
}

void GraphWidget::scaleView(qreal scaleFactor)
{
    qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.07 || factor > 100)
        return;

    scale(scaleFactor, scaleFactor);
}
