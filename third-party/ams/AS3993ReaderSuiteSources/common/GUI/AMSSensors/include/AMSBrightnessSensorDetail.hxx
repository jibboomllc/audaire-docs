/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef AMSBRIGHTNESSSENSORDETAIL_H
#define AMSBRIGHTNESSSENSORDETAIL_H

#include <QtGui>
#include "ui_AMSBrightnessSensorDetail.h"
#include "AMSSensorWidget.hxx"

class AMSBrightnessSensorDetail : public QWidget, private Ui_AMSBrightnessSensorDetail
{
    Q_OBJECT

public:
    AMSBrightnessSensorDetail(QWidget *parent, /*AMSBrightnessSensor*/void *creator);
	~AMSBrightnessSensorDetail();
	void paintEvent(QPaintEvent *);
	
	void setTitle(QString title);
	void setValue(int value, QColor color = Qt::black);
	void setBattery(int value, QColor color = Qt::black);
	void setRssi(int value, QColor color = Qt::black);
	void setErrorCount(int value, QColor color = Qt::black);
	void setPackageCount(int value, QColor color = Qt::black);
	void setPer(QString value, QColor color = Qt::black);
	void setLeds(int value, int mask = 0xFFFFFFFF);

private slots:
    void closeEvent(QCloseEvent *);

private:
	/*AMSBrightnessSensor*/void *creator;
	AMSSensorWidget *brightness;
	AMSSensorWidget *battery;
	AMSSensorWidget *rssi;

	int leds;
};

#endif // AMSBRIGHTNESSSENSORDETAIL_H
