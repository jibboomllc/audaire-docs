/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSSensorWidget
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Puri
 *
 *  \brief  AMSSensorWidget class
 *
 *  AMSSensorWidget is an extension of the QWidget class.
 *  It contains common logic of all AMSSensorWidgets.
 */

#ifndef AMSSENSORWIDGET_H
#define AMSSENSORWIDGET_H

#include "AMSSensorDetailInterface.hxx"

class AMSSensorWidget : public QWidget
{
	Q_OBJECT

public:
	AMSSensorWidget(QWidget * parent);

	virtual int setValue(int value, QColor color = Qt::black);
	virtual void setBattery(int value, QColor color = Qt::black);
	virtual void setRssi(int value, QColor color = Qt::black);
	virtual void setFrequency(int value, QColor color = Qt::black);
	virtual void setLeds(int value, int mask = 0xFFFFFFFF);
	virtual void setErrorCount(int value, QColor color = Qt::black);
	virtual void setPackageCount(int value);
	virtual void setPer(int value, int mode);
	virtual void setPer(QString value);
	virtual void setMessageNumber(int messageNumber);
	virtual void setTimestamp(int timestamp);
	virtual void setLedSingleshot(int led, int timeout);
	virtual void setDetailViewTitle(const QString);
	void setDetailView(AMSSensorDetailInterface* detail) { this->detail = detail; }

protected:
	AMSSensorDetailInterface *detail;
	int frequency;
	int batteryValue;
	QColor batteryColor;


protected:
	virtual void mouseDoubleClickEvent(QMouseEvent *);
};

#endif /* AMSSENSORWIDGET_H */