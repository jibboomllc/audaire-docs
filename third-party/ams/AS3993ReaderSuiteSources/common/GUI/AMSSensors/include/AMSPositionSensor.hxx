/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef AMSPOSITIONSENSOR_H
#define AMSPOSITIONSENSOR_H

#include <QtGui>
#include "ui_AMSPositionSensor.h"
#include "AMSSensorWidget.hxx"

/* FIXME/TODO: use QGraphicsEllipseItem  */
class PositionCursor : public QGraphicsItem
{
public:
    PositionCursor();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void setCenter(int xpos, int ypos);
    void setColor(QColor & newColor) { color = newColor; };

private:
    int radius;
    QColor color;
    QPoint center;
};

class AMSPositionSensor : public AMSSensorWidget, private Ui_AMSPositionSensor
{
    Q_OBJECT

public:
    AMSPositionSensor(QWidget *parent);
	~AMSPositionSensor();

	//void paintEvent(QPaintEvent *);

	int setValue(int value, QColor color = Qt::black);
	void setBattery(int value, QColor color = Qt::black);
	void setRssi(int value, QColor color = Qt::black);
	void setLeds(int value, int mask = 0xFFFFFFFF);
	void setErrorCount(int value, QColor color = Qt::black);
	//void setPackageCount(int value);
	void setPer(int per10, int perInfinite);
	//void setPer(QString value);
    
public slots:

protected:
	void mousePressEvent(QMouseEvent *);
	
private:
    signed char xPos;
    signed char yPos;
    signed char button;
    int leds;
	QGraphicsScene * scene;
    QGraphicsPixmapItem * background;
    PositionCursor * posCursor;

};

#endif // AMSPOSITIONSENSOR_H
