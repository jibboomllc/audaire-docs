/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef AMSBRIGHTNESSSENSOR_H
#define AMSBRIGHTNESSSENSOR_H

#include <QtGui>
#include "ui_AMSBrightnessSensor.h"
#include "AMSSensorWidget.hxx"
#include "AMSBrightnessSensorDetail.hxx"

class AMSBrightnessSensor : public AMSSensorWidget, private Ui_AMSBrightnessSensor
{
    Q_OBJECT

public:
    AMSBrightnessSensor(QWidget *parent);
	~AMSBrightnessSensor();

	void paintEvent(QPaintEvent *);

	int setValue(int value, QColor color = Qt::black);
	void setBattery(int value, QColor color = Qt::black);
	void setRssi(int value, QColor color = Qt::black);
	void setLeds(int value, int mask = 0xFFFFFFFF);
	void setErrorCount(int value, QColor color = Qt::black);
	void setPackageCount(int value);
	void setPer(int value, int mode);
	void setPer(QString value);

protected:
	void mouseDoubleClickEvent(QMouseEvent *);
	
private:
	AMSBrightnessSensorDetail *detail;
	int brightness;
	int leds;
};

#endif // AMSBRIGHTNESSSENSOR_H
