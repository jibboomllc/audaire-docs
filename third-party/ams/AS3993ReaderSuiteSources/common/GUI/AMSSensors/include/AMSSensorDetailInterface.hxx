/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef AMSSENSORDETAILINTERFACE_H
#define AMSSENSORDETAILINTERFACE_H

#include "AMSLed.hxx"

class AMSSensorWidget;

class AMSSensorDetailInterface : public QWidget
{
    Q_OBJECT

public:
    AMSSensorDetailInterface(QWidget *parent);
	~AMSSensorDetailInterface();

	virtual void setTitle(QString title)=0;
	virtual void setValue(int,  QColor color = Qt::black) = 0;
	virtual void setBattery(int,  QColor color = Qt::black)=0;;
	virtual void setRssi(int,  QColor color = Qt::black)=0;;
	virtual void setFrequency(int,  QColor color = Qt::black)=0;;
	virtual void setLeds(int, int)=0;;
	virtual void setErrorCount(int, QColor color = Qt::black) = 0;
	virtual void setPackageCount(int)=0;;
	virtual void setPer(int, int)=0;
    virtual void setPer(int, int, int)=0;
	virtual void setPer(QString)=0;
	virtual void setMessageNumber(int)=0;;
	virtual void setTimestamp(int)=0;;
	virtual void setLedSingleshot(int, int)=0;;

private:
	AMSSensorWidget *sensor;
	AMSSensorWidget *battery;
	AMSSensorWidget *rssi;
	AMSSensorWidget *frequency;
	QList<AMSLed*>  ledsList;
};

#endif // AMSSENSORDETAILINTERFACE_H
