/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSSensorWidget
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Puri
 *
 *  \brief  AMSSensorWidget class
 *
 *  AMSSensorWidget is an extension of the QWidget class.
 *  It contains common logic of all AMSSensorWidgets.
 */
#include "AMSSensorWidget.hxx"

AMSSensorWidget::AMSSensorWidget(QWidget * parent) : QWidget(parent)
{
	this->detail = NULL;

}

int AMSSensorWidget::setValue(int value, QColor color)
{
	if(detail)
	{
		detail->setValue(value, color);
	}
	return value;
}

void AMSSensorWidget::setBattery(int value, QColor color)
{
	if(detail)
		detail->setBattery(value, color);
}

void AMSSensorWidget::setRssi(int value, QColor color)
{

	if(detail)
		detail->setRssi(value, color);

}

void AMSSensorWidget::setFrequency(int value, QColor color)
{
	if(detail)
		detail->setFrequency(value, color);
}

void AMSSensorWidget::setLeds(int value, int mask)
{
	if(detail)
		detail->setLeds(value, mask);
}

void AMSSensorWidget::setErrorCount(int value, QColor color)
{
	if(detail)
		detail->setErrorCount(value, color);
}

void AMSSensorWidget::setLedSingleshot(int led, int timeout)
{
	if(detail)
		detail->setLedSingleshot(led, timeout);
}

void AMSSensorWidget::setPackageCount(int value)
{
	if(detail)
		detail->setPackageCount(value);
}

void AMSSensorWidget::setPer(int value, int mode)
{
	if(detail)
		detail->setPer(value, mode);
}

void AMSSensorWidget::setPer(QString value)
{
	if(detail)
		detail->setPer(value);
}

void AMSSensorWidget::setMessageNumber(int messageNumber)
{
	if(detail)
		detail->setMessageNumber(messageNumber);
}

void AMSSensorWidget::setTimestamp(int timestamp)
{
	if(detail)
		detail->setTimestamp(timestamp);
}

void AMSSensorWidget::mouseDoubleClickEvent(QMouseEvent *)
{
	if(detail == NULL) // cast failed
		return;

	this->detail->show();
	this->detail->activateWindow();
}

void AMSSensorWidget::setDetailViewTitle(const QString name)
{
	if(detail)
		detail->setTitle(name);
}
