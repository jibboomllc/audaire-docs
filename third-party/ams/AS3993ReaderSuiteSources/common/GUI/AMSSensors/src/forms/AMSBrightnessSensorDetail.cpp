/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "AMSBrightnessSensorDetail.hxx"
#include "AMSBrightnessSensor.hxx"
#include "AMSSmallBatterySensor.hxx"
#include "AMSSmallRssiSensor.hxx"

AMSBrightnessSensorDetail::AMSBrightnessSensorDetail(QWidget *parent, /*AMSBrightnessSensor*/void *creator) : QWidget(parent)
{
	this->setupUi(this);

	if(((QWidget*)creator)->objectName() == "AMSBrightnessSensor")
		this->brightness = new AMSBrightnessSensor(this->sensorWidget);
	else
		this->brightness = NULL;

	this->battery = new AMSSmallBatterySensor(this->batteryWidget);
	this->rssi = new AMSSmallRssiSensor(this->rssiWidget);

	this->creator = creator;
	this->setFixedSize(450,365);
	this->leds = 0;
}

AMSBrightnessSensorDetail::~AMSBrightnessSensorDetail()
{
	this->close();
}

void AMSBrightnessSensorDetail::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	QRect led(0, 0, 10, 10);
	QColor greenOff(0, 170, 0);
	QColor redOff(180, 0, 0);

	// TRANS PACK
	led.setRect(35, 55, 20, 10);
	if(this->leds & 0x01)
		painter.fillRect(led, Qt::green);
	else
		painter.fillRect(led, greenOff);

	// RVC PACK
	led.setRect(35, 85, 20, 10);
	if(this->leds & 0x02)
		painter.fillRect(led, Qt::green);
	else
		painter.fillRect(led, greenOff);

	// LOST PACK
	led.setRect(35, 115, 20, 10);
	if(this->leds & 0x04)
		painter.fillRect(led, Qt::red);
	else
		painter.fillRect(led, redOff);
}

void AMSBrightnessSensorDetail::setTitle(QString title)
{
	this->setWindowTitle(title);
}

void AMSBrightnessSensorDetail::setValue(int value, QColor color)
{
	this->brightness->setValue(value, color);
}

void AMSBrightnessSensorDetail::setBattery(int value, QColor color)
{
	this->battery->setValue(value, color);
}

void AMSBrightnessSensorDetail::setRssi(int value, QColor color)
{
	this->rssi->setValue(value, color);
}

void AMSBrightnessSensorDetail::setErrorCount(int value, QColor color)
{
	this->errorCount_lb->setText(QString::QString("%1").arg(value));
}

void AMSBrightnessSensorDetail::setPackageCount(int value, QColor color)
{
	this->packageCount_lb->setText(QString::QString("%1").arg(value));
}

void AMSBrightnessSensorDetail::setPer(QString value, QColor color)
{
	this->per10Count_lb->setText(QString::QString("%1").arg(value));
}


void AMSBrightnessSensorDetail::setLeds(int value, int mask)
{
	this->leds &= ~mask;
	this->leds |= (value & mask);
	this->update();
}


void AMSBrightnessSensorDetail::closeEvent(QCloseEvent *)
{
	// delete yourself from your creator
		//	((AMSBrightnessSensor*)creator)->detail = NULL;
	// after the closeEvent, the object is still alive
	// if we don't NULL the pointer here, we can still access it, but don't see it
	// when we don't set the pointer to NULL we can use ->show() to make it visible again
}
