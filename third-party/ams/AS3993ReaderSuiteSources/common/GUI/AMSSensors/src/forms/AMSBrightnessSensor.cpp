/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "AMSBrightnessSensor.hxx"

AMSBrightnessSensor::AMSBrightnessSensor(QWidget *parent) : AMSSensorWidget(parent)
{
	this->brightness = 0;
	this->leds = -1;
	this->detail = NULL;
	this->setupUi(this);
}

AMSBrightnessSensor::~AMSBrightnessSensor()
{
	if(detail)
	{
		delete ((AMSBrightnessSensorDetail*)detail);
		detail = NULL;
	}
}

void AMSBrightnessSensor::paintEvent(QPaintEvent *)
{
	QPainter *painter;
	painter = new QPainter(this);

	QColor *ellipseColor;
	ellipseColor = new QColor();
	ellipseColor->setRgb(this->brightness, this->brightness, this->brightness);

	painter->setBrush(QBrush::QBrush(*ellipseColor));
	painter->drawEllipse(10, 45, 45, 45);

	// connectivity LED
	if(this->leds >= 0)
	{
		QRect led(130, 6, 20, 10);
		QColor greenOff(0, 170, 0);
		if(this->leds == 0x04)
			painter->fillRect(led, Qt::red);
		else if(this->leds == 0x01)
			painter->fillRect(led, Qt::green);
		else
			painter->fillRect(led, greenOff);
	}
	
	delete ellipseColor;
	delete painter;
}

int AMSBrightnessSensor::setValue(int value, QColor color)
{
	this->brightness = value;
	this->update();

	// is there a detailed view we have to update too?
	if(detail)
	{
		((AMSBrightnessSensorDetail*)detail)->setValue(value, color);
	}

	return value;
}

void AMSBrightnessSensor::setBattery(int value, QColor color)
{
	if(detail)
		((AMSBrightnessSensorDetail*)detail)->setBattery(value, color);
}

void AMSBrightnessSensor::setRssi(int value, QColor color)
{
	if(detail)
		((AMSBrightnessSensorDetail*)detail)->setRssi(value, color);
}

void AMSBrightnessSensor::setLeds(int value, int mask)
{
	this->leds &= ~mask;
	this->leds |= (value & mask);
	this->update();

	// is there a detailed view we have to update too?
	if(detail)
		((AMSBrightnessSensorDetail*)detail)->setLeds(value, mask);
}

void AMSBrightnessSensor::setErrorCount(int value, QColor color)
{
	if(detail)
		((AMSBrightnessSensorDetail*)detail)->setErrorCount(value, color);
}

void AMSBrightnessSensor::setPackageCount(int value)
{
	if(detail)
		((AMSBrightnessSensorDetail*)detail)->setPackageCount(value, Qt::black);
}

void AMSBrightnessSensor::setPer(int value, int mode)
{
}

void AMSBrightnessSensor::setPer(QString value)
{
	if(detail)
		((AMSBrightnessSensorDetail*)detail)->setPer(value, Qt::black);
}


void AMSBrightnessSensor::mouseDoubleClickEvent(QMouseEvent *)
{
	// got double clicked, react on it by opening detail window

	// is there already a window open?
	if(detail)
	{
		((AMSBrightnessSensorDetail*)detail)->show();
		((AMSBrightnessSensorDetail*)detail)->activateWindow();
	}
	else
	{
		// pass creator, not parent, to have a new stand-alone window
		detail = new AMSBrightnessSensorDetail(0, this);
		((AMSBrightnessSensorDetail*)detail)->setTitle(this->windowTitle());
		((AMSBrightnessSensorDetail*)detail)->show();
	}
}
