/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "AMSPositionSensor.hxx"
#include <QModelIndex>

/*
 *****************************************************************************
 * Classe PositionCursor
 *****************************************************************************
 */
PositionCursor::PositionCursor()
    : radius(3), color(0, 0, 0), center(0,0)
{
    setRotation(0);
}

void PositionCursor::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setBrush(color);
    painter->drawEllipse(center, radius, radius);
}

QRectF PositionCursor::boundingRect() const
{
    return QRectF(-radius, -radius, radius, radius);
}

void PositionCursor::setCenter(int xpos, int ypos)
{
    center.setX(xpos);
    center.setY(ypos);
    //qDebug() << "setCenter()" << center;
}


/*
 *****************************************************************************
 * Classe AMSPositionSensor
 *****************************************************************************
 */
AMSPositionSensor::AMSPositionSensor(QWidget *parent) 
    : AMSSensorWidget(parent),
      posCursor(NULL),
      scene(NULL),
      background(NULL),
      xPos(0),
      yPos(0),
      button(0)
{
	this->leds = -1;
	this->detail = NULL;

	this->setupUi(this);
    
    this->targetGv->setRenderHint(QPainter::Antialiasing);
    this->targetGv->setCacheMode(QGraphicsView::CacheBackground);
    this->targetGv->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    //this->targetGv->setDragMode(QGraphicsView::ScrollHandDrag);
    //this->targetGv->setBackgroundBrush(QPixmap(":/Application/target_80x80.png"));

    this->scene = new QGraphicsScene(this);
    this->background = new QGraphicsPixmapItem(QPixmap(":/Application/target_80x80.png"));
    this->background->setTransformOriginPoint(
        static_cast<double>(this->background->pixmap().width()) / 2.0,
        static_cast<double>(this->background->pixmap().height()) / 2.0);
    this->scene->addItem(background);
    this->posCursor = new PositionCursor();
    this->scene->addItem(posCursor);
    this->targetGv->setScene(scene);

    this->posCursor->setCenter(this->background->pixmap().width() / 2, this->background->pixmap().height() / 2);
    this->targetGv->show();
}

AMSPositionSensor::~AMSPositionSensor()
{
    if (posCursor)
    {
        delete posCursor;
        posCursor = NULL;
    }
	if (detail)
	{
		delete detail;
		detail = NULL;
	}
    if (scene)
    {
        delete scene;
        scene = NULL;
    }
}
//
//void AMSPositionSensor::paintEvent(QPaintEvent *)
//{
//	QPainter *painter;
//	painter = new QPainter(this);
//
//	QColor *dotColor;
//	dotColor = new QColor();
//    dotColor->setRgb(button ? 0 : 255, button ? 0 : 255, button ? 0 : 255); // set cursor depending on easypoint button
//
//	painter->setBrush(QBrush::QBrush(*dotColor));
//
//	painter->drawEllipse(cursorPos, 3, 3);
//
//	// connectivity LED
//	if(this->leds >= 0)
//	{
//		QRect led(130, 6, 20, 10);
//		QColor greenOff(0, 170, 0);
//		if(this->leds == 0x04)
//			painter->fillRect(led, Qt::red);
//		else if(this->leds == 0x01)
//			painter->fillRect(led, Qt::green);
//		else
//			painter->fillRect(led, greenOff);
//	}	
//	delete dotColor;
//	delete painter;
//}

int AMSPositionSensor::setValue(int value, QColor color)
{
    int xOffset = this->background->pixmap().width() / 2;
    int yOffset = this->background->pixmap().height() / 2;
	this->xPos   = (signed char)((value & 0x00FF0000) >> 16);
    this->yPos   = (signed char)((value & 0x0000FF00) >>  8);
    this->button = (signed char)(value & 0x000000FF);

    this->xPosLcd->display(this->xPos);
    this->yPosLcd->display(this->yPos);
    this->onOffLcd->display(this->button);

    this->posCursor->setCenter(xOffset + (int)((double)this->xPos / 128 * xOffset), yOffset + (int)((double)this->yPos / 128 * yOffset));
    this->posCursor->setColor(QColor(button ? 0 : 255, button ? 0 : 255, button ? 0 : 255));

	this->update();

	// is there a detailed view we have to update too?
	if (detail)
	{
		detail->setValue(value, color);
	}

	return 0;
}

void AMSPositionSensor::setBattery(int value, QColor color)
{
	if (detail)
		detail->setBattery(value, color);
}

void AMSPositionSensor::setRssi(int value, QColor color)
{
	if (detail)
		detail->setRssi(value, color);
}

void AMSPositionSensor::setLeds(int value, int mask)
{
	this->leds &= ~mask;
	this->leds |= (value & mask);
	this->update();

	// is there a detailed view we have to update too?
	if (detail)
		detail->setLeds(value, mask);
}

void AMSPositionSensor::setErrorCount(int value, QColor color)
{
	if (detail)
		detail->setErrorCount(value, color);
}

void AMSPositionSensor::setPer(int per10, int perInfinite)
{
    if (detail)
        detail->setPer(per10, perInfinite);
}

//void AMSPositionSensor::setPer(QString value)
//{
//	if (detail)
//		detail->setPer(value, Qt::black);
//}


void AMSPositionSensor::mousePressEvent(QMouseEvent *)
{
	// got double clicked, react on it by opening detail window

	// is there already a window open?
	if (detail)
	{
		detail->show();
		detail->activateWindow();
	}
}

