/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "AMSSmallRssiSensor.hxx"

AMSSmallRssiSensor::AMSSmallRssiSensor(QWidget *parent) : AMSSensorWidget(parent)
{
	this->value = -1000;
	this->color = Qt::black;
	this->setupUi(this);
}

int AMSSmallRssiSensor::setValue(int value, QColor color)
{
	// fill out the bars
	this->value = value;
	this->color = color;
	this->update();

	return value;
}

void AMSSmallRssiSensor::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	QRect bar(0, 0, 100, 50);

	// do we already have a valid value for RSSI?
	if((this->value < 0) | (this->value > 8))
	{
		this->label->setVisible(true);
	}
	else
	{
		this->label->setVisible(false);

		//painter.drawRect(bar);
		// draw the bars from left to right
		if(this->value > 0)
		{
			bar.setRect(10, 32, 7, 17);
			painter.fillRect(bar, this->color);
		}
		else
		{
			bar.setRect(10, 32, 6, 16);
			painter.drawRect(bar);
		}
		if(this->value > 1)
		{
			bar.setRect(20, 28, 7, 21);
			painter.fillRect(bar, this->color);
		}
		else
		{
			bar.setRect(20, 28, 6, 20);
			painter.drawRect(bar);
		}
		if(this->value > 2)
		{
			bar.setRect(30, 24, 7, 25);
			painter.fillRect(bar, this->color);
		}
		else
		{
			bar.setRect(30, 24, 6, 24);
			painter.drawRect(bar);
		}
		if(this->value > 3)
		{
			bar.setRect(40, 20, 7, 29);
			painter.fillRect(bar, this->color);
		}
		else
		{
			bar.setRect(40, 20, 6, 28);
			painter.drawRect(bar);
		}
		if(this->value > 4)
		{
			bar.setRect(50, 16, 7, 33);
			painter.fillRect(bar, this->color);
		}
		else
		{
			bar.setRect(50, 16, 6, 32);
			painter.drawRect(bar);
		}
		if(this->value > 5)
		{
			bar.setRect(60, 12, 7, 37);
			painter.fillRect(bar, this->color);
		}
		else
		{
			bar.setRect(60, 12, 6, 36);
			painter.drawRect(bar);
		}
		if(this->value > 6)
		{
			bar.setRect(70, 8, 7, 41);
			painter.fillRect(bar, this->color);
		}
		else
		{
			bar.setRect(70, 8, 6, 40);
			painter.drawRect(bar);
		}
		if(this->value > 7)
		{
			bar.setRect(80, 4, 7, 45);
			painter.fillRect(bar, this->color);
		}
		else
		{
			bar.setRect(80, 4, 6, 44);
			painter.drawRect(bar);
		}
	}
}
