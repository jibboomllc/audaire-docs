/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "AMSSmallBatterySensor.hxx"

AMSSmallBatterySensor::AMSSmallBatterySensor(QWidget *parent) : AMSSensorWidget(parent)
{
	this->value = -1;
	this->color = Qt::black;
	this->setupUi(this);
}

int AMSSmallBatterySensor::setValue(int value, QColor color)
{
	// draw the battery state
	this->value = value;
	this->color = color;
	this->update();

	return value;
}

void AMSSmallBatterySensor::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	QRect battery(0, 0, 100, 50);

	// do we already have a valid value for BATT?
	if((this->value < 0) | (this->value > 4))
	{
		this->label->setVisible(true);
	}
	else
	{
		this->label->setVisible(false);

		//painter.drawRect(battery);
		// draw the small top
		battery.setRect(85, 20, 6, 10);
		painter.drawRect(battery);
		// draw the big block
		battery.setRect(10, 13, 75, 25);
		painter.drawRect(battery);

		switch(value)
		{
		case 4:
			battery.setRect(66, 15, 17, 22);
			painter.fillRect(battery, this->color);
			// no break, we fall through the others too ;-)
		case 3:
			battery.setRect(48, 15, 17, 22);
			painter.fillRect(battery, this->color);
			// no break, we fall through the others too ;-)
		case 2:
			battery.setRect(30, 15, 17, 22);
			painter.fillRect(battery, this->color);
			// no break, we fall through the others too ;-)
		case 1:
			battery.setRect(12, 15, 17, 22);
			painter.fillRect(battery, this->color);
			// no break, we fall through the others too ;-)
		default:
			break;
		}
	}
}
