/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "AMSRotationSensor.hxx"


AMSRotationSensor::AMSRotationSensor(QWidget *parent) : AMSSensorWidget(parent)
{
	this->detail = NULL;
	this->setupUi(this);
}

AMSRotationSensor::~AMSRotationSensor()
{
	if(detail)
	{
		delete this->detail;
		detail = NULL;
	}
}

int AMSRotationSensor::setValue(int value, QColor color)
{
	int degree;

	if(value < 230)
	{
		degree = 135;
		degree -= value / 2;
	}
	else
	{
		//degree = 225;
		degree = 360 + (230 / 2);
		degree -= value / 2;
	}
	this->dial->setValue(degree);

	// is there a detailed view we have to update too?
	if(detail)
	{
		this->detail->setValue(value, color);
	}
	return value;
}
