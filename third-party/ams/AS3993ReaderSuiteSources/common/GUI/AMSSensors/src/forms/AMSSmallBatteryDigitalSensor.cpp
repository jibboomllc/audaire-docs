/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "AMSSmallBatteryDigitalSensor.hxx"

AMSSmallBatteryDigitalSensor::AMSSmallBatteryDigitalSensor(QWidget *parent) : AMSSensorWidget(parent)
{
	this->value = -1;
	this->color = Qt::black;
	this->setupUi(this);

	this->voltage_lb->setVisible(false);
}

int AMSSmallBatteryDigitalSensor::setValue(int value, QColor color)
{
	// show the voltage state
	this->value = value;
	this->color = color;
	this->update();

	return value;
}

void AMSSmallBatteryDigitalSensor::paintEvent(QPaintEvent *)
{
	QPalette labelPalette;

	labelPalette.setColor(QPalette::WindowText, this->color);
	this->voltage_lb->setPalette(labelPalette);
	this->label->setPalette(labelPalette);
	if((value >= 1950) && (value <= 3500))
	{	
		this->label->setVisible(false);
		this->voltage_lb->setVisible(true);
		this->voltage_lb->setText(QString::QString("%1 mV").arg(value));
	}
	else if(value == -2)   // ok
	{
		this->label->setVisible(false);
		this->voltage_lb->setVisible(true);
		this->voltage_lb->setText("Batt OK");
	}
	else if(value == -3)   // replace
	{
		this->voltage_lb->setVisible(false);
		this->label->setVisible(true);
		this->label->setText("    Replace\n     Batteries!");
	}
	else
	{
		labelPalette.setColor(QPalette::WindowText, Qt::black);
		this->label->setPalette(labelPalette);
		this->voltage_lb->setVisible(false);
		this->label->setText("... gathering BATT\n     information ...");
		this->label->setVisible(true);
	}
}
