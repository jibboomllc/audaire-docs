/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
#ifndef _AMSTRACEWINDOW_H
#define _AMSTRACEWINDOW_H

#include <QWidget>
#include <../generated/include/ui_AMSTraceWindow.h>
#include <AMSTrace.hxx>
#include <AMSCommunication.hxx>
#include <register_map.hxx>
#include "AMSTraceSerializer.h"
#include "AMSTracePlayer.hxx"
#include "AMSTraceWindowCommandPlugin.hxx"

class AMSTraceWindow : public QWidget, public Ui_AMSTraceWindow
{
	Q_OBJECT

public:
	AMSTraceWindow(QWidget* parent, AMSCommunication* com=0, QStandardItemModel* model=0, RegisterMap* map=0);
	~AMSTraceWindow(void);
	void setHorizontalHeaderView(QHeaderView*);
	void setHorizontalHeaderLabels(const QStringList &);
	void setCommunication(AMSCommunication* com) { itsCom = com; }
	void setTracePlayer(AMSTracePlayerInterface* player);
	void setTraceSerializer(AMSTraceSerializerInterface* serializer);
    bool enableProgramMode(bool enable);
	bool addCommandPlugin(AMSTraceWindowCommandPlugin *commandPlugin);

public slots:
	void addLog(QStringList);
	void rowsAboutToBeInserted(const QModelIndex &, int, int);
	void clearTrace();


protected:
	void AMSTraceWindow::closeEvent(QCloseEvent *event);

private:
	void addModelEntry(QStandardItemModel*, const QStringList);

private:
	AMSCommunication			        *itsCom;
	QStandardItemModel			        *itsDataModel;
	QStandardItemModel			        *itsProgramModel;
	QItemSelectionModel 		        *itsTraceSelectionModel;
	QItemSelectionModel 		        *itsProgramSelectionModel;
	AMSTraceSerializerInterface	        *itsSerializer;
	AMSTracePlayerInterface		        *itsTracePlayer;
	RegisterMap					        *itsRegisterMap;
	QStringList					        itsTableHeader;
	QSettings					        itsTraceSettings;
	bool						        itsCustomHeaderUsed;
    bool                                itsProgramModeEnabled;
	QList<QList<QStandardItem*>>	    itsClipboard;
	QList<AMSTraceWindowCommandPlugin*> itsCommandPluginList;

private slots:
	void on_pbCopyClipboard_clicked();
	void on_pbWriteRegister_clicked();
	void on_pbModifyRegister_clicked();
	void on_pbReadRegister_clicked();
	void on_stayOnTopCB_clicked(bool on);
	void on_pbLineUp_clicked(bool clicked);
	void on_pbLineDown_clicked(bool clicked);
	void on_pbClearTrace_clicked(bool on);
	void on_pbClearProgram_clicked(bool on);
	void on_pbAddDelay_clicked(bool clicked);
	void on_pbAddComment_clicked(bool clicked);
	void on_pbRun_clicked(bool clicked);
	void on_pbLoadProgram_clicked(bool clicked);
	void on_pbSaveTrace_clicked(bool clicked);
	void on_pbSaveProgram_clicked(bool clicked);
	void on_pbSaveChipStatus_clicked(bool clicked);
	void on_pbDeleteLine_clicked(bool clicked);
	void on_pbExport_pressed();
	void on_pbImport_pressed();
	void on_pbCopySelection_clicked(bool clicked);
	void on_sbTraceLevel_valueChanged(int value);
	void on_sbTraceBufferSize_valueChanged(int value);
	void on_pbCopyLines_clicked(bool clicked);
	void on_pbPasteLines_clicked(bool clicked);
	void on_pbAddCustomCommand_clicked(bool clicked);
};

#endif // _AMSTraceWindow_H