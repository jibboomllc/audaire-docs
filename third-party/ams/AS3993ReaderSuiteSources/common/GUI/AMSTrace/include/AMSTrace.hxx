/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 *      PROJECT:   AMSLogger
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Detter, M. Dedek
 *
 *  \brief  AMSTrace class
 *
 *  AMSTrace allows to log messages into a file, serial port, GUI or a QT signal
 *  One can select the level of logging or define special filters for messages
 *   
 *   
 */

#ifndef AMSTrace_H_
#define AMSTrace_H_

#include <QStandardItemModel>
#include <QMutex>
#include <QFile>
#include <QXmlStreamWriter>

/* forward declaration */
class AmsComObject;

#define AMS_TRACE_FILE_NAME "trace.txt"

// Interface for trace data processor

class AMSTraceInterface
{
public:
	virtual ~AMSTraceInterface() {};
    /* this is a prototype for non-AmsCom tracing */
	virtual bool processTraceData( const int, const void*, QStringList* ) = 0;
   
    /* this function implements the common part for AmsCom tracing */
    bool processTraceData( unsigned int trcPattern, int comStatus, const QString & telegram, QStringList & list );

    /* interprets AmsCom Objects -overload this function to get your project specific interpretation */
    virtual void interpretAmsComObject( const QString & direction, int comStatus, AmsComObject * obj, QStringList & out );

};


class AMSTraceStandardDataProcessor : public AMSTraceInterface
{
public:
	AMSTraceStandardDataProcessor() {};
	~AMSTraceStandardDataProcessor() {};
	bool processTraceData(const int,const void*, QStringList*);

};

/* this class is the tracer itself. It is implemented as a singleton - i.e. there is only
   one instance of this class */
class AMSTrace: public QObject
{
	Q_OBJECT

private:
	AMSTrace();

public:
	~AMSTrace();

    /* checks to see if the given trace pattern is an activated pattern */
    static bool isToTrace( unsigned int trcPattern, unsigned int otherTrcPattern );

    /* return global instance of trace */
	static AMSTrace* getInstance();

    /* return global instance of data model */
	static QStandardItemModel* getDataModel();

    /* initialise tracer to this trace level and trace mode */
	static void init( quint8 trcLevel, quint32 mode = Trace2Stdout );

    /* get the global trace pattern */
    static unsigned int trcPattern( );
    /* set the global trace pattern */
    static void setTrcPattern( unsigned int trcPattern );

    /* get the global trace level */
	static quint8 trcLevel ( );
    /* set the global trace level */
	static void setTrcLevel( qint8 trcLevel );

    /* trace the given message (in the various formats) if trcLevel is less than the global trace level */
	static void trc (const int trcLevel, const QString & );
	static void trc (const int trcLevel, const QStringList & );
	static void trc (const int trcLevel, const void*);

    /* trace with this function AmsCom Objects */
    static void trc ( const QString & telegram, int comStatus );

    /* constants that represent the different trace modes */
	static quint32 Trace2Stdout;
	static quint32 Trace2Signal;
	static quint32 Trace2Model;
	static quint32 Trace2Buffer;

private:
        /* the one instance of this class */
	static AMSTrace globalInstance;
        /* the one instance of the data model */
    static QStandardItemModel* globalDataModel;

    /* the current data interpreter or none */
	AMSTraceInterface *itsDataProcessor;

    /* the current trace pattern */
    unsigned int itsTracePattern;

    /* the current trace level */
	qint8	m_trcLevel;
    /* in which mode (stdout, file, etc.) the tracer is currently */
	quint32 m_trcMode;
    /* whenever the number of rows in the data model is bigger than this, the trace screen is cleared */
	int		itsBufferSize;
    /* mutex to make access to trace atomic */
	QMutex  itsTraceMutex;

    /* handle to the trace file in the working directory of the application */
    QFile itsTraceFile;

    QXmlStreamWriter itsXmlWriter;

public slots:
	void trace ( const int trcLevel, const QString msg );
	void trace ( const int trcLevel, const QStringList msgs );

signals:
	void traceSignal( QString str );
	void traceBufferClear(bool=true);
	void clearTrace();
public:
	void setDataProcessor(AMSTraceInterface*);
	void setBufferSize(int size) { this->itsBufferSize = size; }
};

// AMSTraceModule class

#define MAX_MODULE_NAME_SIZE 25

class AMSTraceModule
{
private:
	QString traceModuleName;
	static uint maxModuleNameLength;

protected:
	void warning(QString msg);
	void error(QString msg);
	void fatal(QString msg);

	void trc(const int trcLevel, const QString msg);
	void trcBytes(const int trcLevel, const QString msg,
		const QByteArray bytes, const QString pattern = "0x%1 ");

public:
	static void strc(const int trcLevel, const QString msg);
	static void strcBytes(const int trcLevel, const QString msg,
			const QByteArray bytes, const QString pattern = "0x%1 ");

	static void strc(const QString moduleName, const int trcLevel,
			const QString msg);
	static void strcBytes(const QString moduleName, const int trcLevel,
			const QString msg, const QByteArray bytes, const QString pattern =
					"0x%1 ");

public:
	AMSTraceModule(QString name);
	virtual ~AMSTraceModule();
};


#endif /*AMSTrace_H_*/
