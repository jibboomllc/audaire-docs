/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 *      PROJECT:   AMSLogger
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Dedek
 *
 *  \brief  AMSTracePlayer class
 *
 *   
 *   
 */

#ifndef AMSTRACEPLAYER_H_
#define AMSTRACEPLAYER_H_

#include "AMSCommunication.hxx"
#include <AMSTraceWindowCommandPlugin.hxx>

class AMSTracePlayerInterface
{

public:
	virtual ~AMSTracePlayerInterface() {};
	virtual void executeCommand(int) = 0;
	virtual void executeProgram() = 0;
	virtual void setModel(const QStandardItemModel*) = 0;
	virtual void setCommunication(AMSCommunication*) = 0;
	virtual void setVerify(bool on=false) { this->itsVerify = on;}
	virtual void setCommandPluginList(QList<AMSTraceWindowCommandPlugin*> commandPluginList) = 0;
protected:
	bool itsVerify;
};

class AMSStandardTracePlayer : public QObject, public AMSTracePlayerInterface
{
	Q_OBJECT

public	:
	AMSStandardTracePlayer(QStandardItemModel* itsModel=0, AMSCommunication *com = 0);
	~AMSStandardTracePlayer() {};
	void executeProgram();
	void executeCommand(int row);
	void setModel(const QStandardItemModel* model) { itsModel = model; }
	void setCommunication(AMSCommunication* com) { itsCom = com; }
	void setCommandPluginList(QList<AMSTraceWindowCommandPlugin*> commandPluginList);

protected:
	const QStandardItemModel* itsModel; 
	QItemSelectionModel* itsSelectionModel;
	AMSCommunication* itsCom;
	QList<AMSTraceWindowCommandPlugin*> itsCommandPluginList;
};

#endif /*AMSTRACEPLAYER_H_*/
