/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
#ifndef _TRACESERIALIZE_H
#define _TRACESERIALIZE_H

class AMSTraceSerializerInterface
{

public:
	virtual ~AMSTraceSerializerInterface() {};

	virtual void serializeToXml(const QString="", QStandardItemModel* model=NULL)=0;
	virtual void serializeToCSV(const QString="", QStandardItemModel* model=NULL)=0;
	virtual void serializeToAMS(const QString="", QStandardItemModel* model=NULL)=0;

	virtual void loadFromXml(const QString="", QStandardItemModel* model=NULL)=0;
	virtual void loadFromCSV(const QString="", QStandardItemModel* model=NULL)=0;
	virtual void loadFromAMS(const QString="", QStandardItemModel* model=NULL)=0;
};

class AMSTraceSerializer : public QObject, public AMSTraceSerializerInterface
{

public:
	 AMSTraceSerializer();
	 ~AMSTraceSerializer() {};

	 void serializeToXml(const QString="", QStandardItemModel* model=NULL);
	 void serializeToCSV(const QString="", QStandardItemModel* model=NULL);
	 void serializeToAMS(const QString="", QStandardItemModel* model=NULL);
	 
	 void loadFromXml(const QString="", QStandardItemModel* model=NULL);
	 void loadFromCSV(const QString="", QStandardItemModel* model=NULL);
	 void loadFromAMS(const QString="", QStandardItemModel* model=NULL);
};
#endif