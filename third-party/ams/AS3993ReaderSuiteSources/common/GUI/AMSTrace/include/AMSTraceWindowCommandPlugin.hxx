/*
 *****************************************************************************
 * Copyright @ 2014 by austriamicrosystems AG                                *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       * 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 *      PROJECT:   AMSTraceWindow
 *      $Revision: $
 *      LANGUAGE:  C++ / Qt
 */

/*! \file
 *
 *  \author Martin Pelzmann
 *
 *  \brief  AMSTraceWindow command plugin class
 *
 *  Class that acts as as plugin to implement custom commands in the trace window.
 *  
 */

#ifndef _AMSTRACEWINDOWCOMMANDPLUGIN_H
#define _AMSTRACEWINDOWCOMMANDPLUGIN_H

#include "AMSCommunication.hxx"
#include "register_map.hxx"

class AMSTraceWindowCommandPlugin : public QObject
{
	Q_OBJECT

public:
	AMSTraceWindowCommandPlugin();
	virtual ~AMSTraceWindowCommandPlugin();

	QString        getPluginCommandName();
	QStringList    getParameterNames();
	
	void           setCommunication(AMSCommunication *com);
	void           setRegisterMap(RegisterMap *registerMap);
	
	void           setTraceOutputTableView(QTableView *tvTraceOutput);
	void           setProgramTableView(QTableView *tvProgram);
	void           setDataModel(QStandardItemModel *dataModel);
	void           setProgramModel(QStandardItemModel *programModel);
	
	virtual void   executeCommand(QStringList parameters) = 0;
	virtual void   createProgramTableEntry() = 0;

protected:
	QString             itsPluginCommandName;
	QList<QString>      itsParameterNames;
	AMSCommunication   *itsCom;
	RegisterMap        *itsRegisterMap;
	QTableView         *itsTvTraceOutput;
	QTableView         *itsTvProgram;
	QStandardItemModel *itsDataModel;
	QStandardItemModel *itsProgramModel;
	
};

#endif // _AMSTRACEWINDOWCOMMANDPLUGIN_H