/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 *      PROJECT:   AMSLogger
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Dedek
 *
 *  \brief  AMSStandardTracePlayer class
 *
 *  AMSStandardTracePlayer is used to playback a command file. It opens corresponding
 *  XML files and parses them and executes the commands via the configured communication 
 *  channel.
 */

#include <AMSTracePlayer.hxx>
#include <AMSTrace.hxx>
#include <globals.h>

AMSStandardTracePlayer::AMSStandardTracePlayer(QStandardItemModel* model, AMSCommunication* com)
	: itsModel(model), itsCom(com) 
{
}

void AMSStandardTracePlayer::executeCommand(int row)
{
	unsigned char dummy;
	unsigned char mask;
	unsigned char val;
	unsigned char reg;
	int delay;
	bool ok;

	if(row >= itsModel->rowCount())
		return;

	QString cmd = itsModel->item(row,0)->data(Qt::DisplayRole).toString();
	reg = itsModel->item(row,1)->data(Qt::DisplayRole).toString().toShort(&ok,16);
	if(cmd == "Read")
	{
		itsCom->readRegister(reg, &dummy);
	}
	else if(cmd == "Write")
	{
		val = itsModel->item(row,3)->data(Qt::DisplayRole).toString().toShort(&ok,16);
		itsCom->writeRegister(reg, val,itsVerify);
	}

	else if(cmd == "Modify")
	{
		mask = itsModel->item(row,2)->data(Qt::DisplayRole).toString().toShort(&ok,16);
		val = itsModel->item(row,3)->data(Qt::DisplayRole).toString().toShort(&ok,16);
		itsCom->modifyRegister(reg,mask,val,itsVerify);
	}
	else if(cmd == "Delay")
	{
		delay = itsModel->item(row,3)->data(Qt::DisplayRole).toString().toShort(&ok);
		if(delay > 1000) // max 1 sec delay !
			delay = 1000;
		SleepThread::msleep(delay);
	}
	else
	{
		if (!itsCommandPluginList.isEmpty())
		{
			QStringList pluginParameterList;
			
			for (int currentColumn = 0; currentColumn < itsModel->columnCount();++currentColumn)
			{
				pluginParameterList.append(itsModel->item(row,currentColumn)->data(Qt::DisplayRole).toString());
			}
			
			foreach(AMSTraceWindowCommandPlugin* currentPlugin,itsCommandPluginList)
			{
				if (cmd == currentPlugin->getPluginCommandName())
				{
					currentPlugin->executeCommand(pluginParameterList);
				}
			}
		}
	}
}

void AMSStandardTracePlayer::executeProgram()
{
	if(itsModel == NULL)
		return;

	int cnt = itsModel->rowCount();

	for(int r=0;r<cnt;r++)
	{
		executeCommand(r);
	}
}

void AMSStandardTracePlayer::setCommandPluginList(QList<AMSTraceWindowCommandPlugin*> commandPluginList)
{
	itsCommandPluginList = commandPluginList;
}
