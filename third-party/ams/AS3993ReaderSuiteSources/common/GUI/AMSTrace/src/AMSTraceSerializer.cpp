/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSTraceSerializer
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Dedek
 *
 *  \brief  AMSTraceWindow class
 *
 *  Class which implements a line edit window to be used for trace comments
 *  Also shows how to derivate from the Trace class and how to use it
 *  
 */

#include "AMSTraceSerializer.h"

AMSTraceSerializer::AMSTraceSerializer() 
{
}


void AMSTraceSerializer::serializeToXml(const QString fileName, QStandardItemModel* model)
{
	if(model == NULL || fileName.isEmpty())
		return;
	
	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
	{
		qDebug() << file.error();
		return;
	}

	QTextStream out(&file);
	QDomDocument doc("LogFile");
	QDomElement root = doc.createElement("LogFile");
	doc.appendChild(root);
	root.setAttribute("Version", "1.0.1");
	
	for(int row=0;row < model->rowCount();row++)
	{
		QDomElement line = doc.createElement("LOG");
		root.appendChild(line);
		//line.setAttribute("Command", itsModel->item(row,0)->text());
		QDomElement cmd = doc.createElement("Command");
		line.appendChild(cmd);
		cmd.appendChild(doc.createTextNode(model->item(row,0)->text()));
		 cmd = doc.createElement("Register");
		line.appendChild(cmd);
		cmd.appendChild(doc.createTextNode(model->item(row,1)->text()));
		 cmd = doc.createElement("Mask");
		line.appendChild(cmd);
		cmd.appendChild(doc.createTextNode(model->item(row,2)->text()));
		cmd = doc.createElement("Value");
		line.appendChild(cmd);
		cmd.appendChild(doc.createTextNode(model->item(row,3)->text()));
		cmd = doc.createElement("Status");
		line.appendChild(cmd);
		cmd.appendChild(doc.createTextNode(model->item(row,4)->text()));
		cmd = doc.createElement("Comment");
		line.appendChild(cmd);
		cmd.appendChild(doc.createTextNode(model->item(row,5)->text()));
	}
	doc.save(out, 4);
	file.close();
}


void AMSTraceSerializer::serializeToCSV(const QString fileName, QStandardItemModel* model)
{
	if(model == NULL || fileName.isEmpty())
		return;

	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
	{
		qDebug() << file.error();
		return;
	}

	QTextStream out(&file);

	out << "Version 1.0.1" << "\n";

	for(int row=0;row < model->rowCount();row++)
	{
		out << model->item(row,0)->text();
		out << ";";
		out << model->item(row,1)->text();
		out << ";";
		out << model->item(row,2)->text();
		out << ";";
		out << model->item(row,3)->text();
		out << ";";
		out << model->item(row,5)->text();
		out << ";\n";
	}
	file.close();
}

void AMSTraceSerializer::serializeToAMS(const QString fileName, QStandardItemModel* model)
{
	if(model == NULL || fileName.isEmpty())
		return;

	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
	{
		qDebug() << file.error();
		return;
	}

	QTextStream out(&file);

	out << "/* Version 1.0.1 */" << "\n";

	QString tmp("%1(%2,%3);");
	QString entry("");

	for(int row=0;row < model->rowCount();row++)
	{
		if(model->item(row,0)->text() == "Write" || model->item(row,0)->text() == "Modify")
		{
			entry = tmp.arg("AMSWriteByte").arg(model->item(row,1)->text()).arg(model->item(row,3)->text());
		}
		else if(model->item(row,0)->text() == "Read")
		{
			entry = tmp.arg("AMSReadByte").arg(model->item(row,1)->text()).arg("Dummy");
		}
		else if(model->item(row,0)->text() == "Delay")
		{
			entry = QString("Delay(%1);").arg(model->item(row,3)->text());
		}
		out << entry << "\n";
		entry = "";
	}
	file.close();
}


void AMSTraceSerializer::loadFromXml(const QString fileName, QStandardItemModel *model)
{
	if(model == NULL || fileName.isEmpty() || model == NULL)
		return;

	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		qDebug() << file.error();
		return;
	}
	
	QDomDocument doc("LogFile");

	if (!doc.setContent(&file))
	{
		file.close();
		return;
	}
	file.close();
	QDomElement docElem = doc.documentElement();

	QDomNode n = docElem.firstChild();
	QList<QStandardItem*> modelEntry;

	while(!n.isNull())
	{
		QDomNode child = n.firstChild(); // this is the log entry
		while(!child.isNull())
		{
			if(!child.toElement().isNull())
			{
				modelEntry.append(new QStandardItem(child.toElement().text()));
			}
			child = child.nextSibling();
		}
		model->appendRow(modelEntry);
		modelEntry.clear();
		n = n.nextSibling();
	}
}



void AMSTraceSerializer::loadFromCSV(const QString fileName, QStandardItemModel* model)
{
	if(model == NULL || fileName.isEmpty())
		return;

	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		qDebug() << file.error();
		return;
	}
	QList<QStandardItem*> modelEntry;
	QString line;
	QStringList entries;
	QTextStream inStream(&file);
	
	do
	{
		line = inStream.readLine();
		entries = line.split(";");
		foreach(QString entry, entries)
		{
			modelEntry.append(new QStandardItem(entry));
		}
		model->appendRow(modelEntry);
		modelEntry.clear();
	} while (!line.isNull());
	file.close();
}


void AMSTraceSerializer::loadFromAMS(const QString fileName, QStandardItemModel* model)
{
	if(model == NULL || fileName.isEmpty())
		return;

	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		qDebug() << file.error();
		return;
	}
	QList<QStandardItem*> modelEntry;
	QString line;
	QStringList entries;
	QTextStream inStream(&file);
	QRegExp rx("\\((.+),(.+)\\)");
	QRegExp rxDelay("\\((.+)\\)");
	do
	{
		line = inStream.readLine();
		if(line.startsWith("/*") || line.startsWith("//") || line.isEmpty())
			continue;
	
		modelEntry.append(new QStandardItem(""));
		modelEntry.append(new QStandardItem(""));
		modelEntry.append(new QStandardItem(""));
		modelEntry.append(new QStandardItem(""));
		modelEntry.append(new QStandardItem(""));
		modelEntry.append(new QStandardItem(""));

		if(line.contains("WriteByte"))
		{
			modelEntry[0]->setText("Write");
			if (rx.indexIn(line) > -1)
			{
				modelEntry[1]->setText(rx.cap(1));
				modelEntry[2]->setText("0xFF");
				modelEntry[3]->setText(rx.cap(2));
			}
			modelEntry[5]->setText("Write Register");
		}
		else if(line.contains("Delay"))
		{
			modelEntry[0]->setText("Delay");
			if (rxDelay.indexIn(line) > -1)
			{
				modelEntry[3]->setText(rxDelay.cap(1));
			}
			modelEntry[5]->setText("Delay Execution by ms");
		}
		else if(line.contains("Read"))
		{
			modelEntry[0]->setText("Read");
			if (rx.indexIn(line) > -1)
			{
				modelEntry[1]->setText(rx.cap(1));
			}
			modelEntry[5]->setText("Read Register");
		}
		model->appendRow(modelEntry);
		modelEntry.clear();
	} while (!line.isNull());
	file.close();
}




