/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 *      PROJECT:   AMSLogger
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Dedek
 *
 *  \brief  AMSTrace class
 *
 *  AMSTrace allows to log messages into a file, serial port or GUI 
 *  One can select the level of logging or define special filters for messages
 *  This class should be derivated and message handler should be overwritten to
 *  meet specific requirements.
 */
#include <AMSTrace.hxx>
#include "AmsCom.h"
#include "I2cComObjects.h"
#include "CtrlComObjects.h"
#include "GetVersion.hxx"


struct trcInput;

AMSTrace AMSTrace::globalInstance;

quint32 AMSTrace::Trace2Stdout		= 0x00000001;
quint32 AMSTrace::Trace2Signal		= 0x00000004;
quint32 AMSTrace::Trace2Model		= 0x000000010;
quint32 AMSTrace::Trace2Buffer		= 0x000000010;
QStandardItemModel *AMSTrace::globalDataModel = NULL;

AMSTrace::AMSTrace()
    : itsTracePattern( AMS_COM_TRACE_ALLWAYS )
{
	m_trcLevel = 1;
	itsDataProcessor = (AMSTraceInterface*) new AMSTraceStandardDataProcessor(); // set default data processor
}

AMSTrace::~AMSTrace( )
{
    if ( globalInstance.itsTraceFile.isOpen( ) )
    {
        globalInstance.itsXmlWriter.writeEndElement( );
        globalInstance.itsXmlWriter.writeEndDocument( );
        globalInstance.itsTraceFile.close();
    }
}

AMSTrace* AMSTrace::getInstance()
{
	return &globalInstance;
}

void AMSTrace::init(quint8 trcLevel, quint32 mode)
{
    QString name = AMSVersionInfo::Instance().GetWorkingDirectory( );
    name.append( "\\" );
    name.append( AMS_TRACE_FILE_NAME );

	globalInstance.m_trcLevel = trcLevel;
	globalInstance.m_trcMode = mode;
	if ((mode & Trace2Model) != 0)
	{
		globalInstance.globalDataModel = new QStandardItemModel();
	}

    globalInstance.itsTraceFile.setFileName( name );    
    globalInstance.itsTraceFile.open( QIODevice::WriteOnly );
    globalInstance.itsXmlWriter.setDevice( &globalInstance.itsTraceFile );
    globalInstance.itsXmlWriter.setAutoFormatting( true );
    globalInstance.itsXmlWriter.writeStartDocument( );
    globalInstance.itsXmlWriter.writeStartElement( AMS_COM_XML_SESSION );
    globalInstance.itsXmlWriter.writeStartElement( AMS_COM_XML_INFORMATION );
    globalInstance.itsXmlWriter.writeAttribute( AMS_COM_XML_GUI_VERSION, AMSVersionInfo::Instance().GetProductVersionString() );
    globalInstance.itsXmlWriter.writeAttribute( AMS_COM_XML_FW_VERSION, AMSVersionInfo::Instance().GetProductVersionFWString() );
    globalInstance.itsXmlWriter.writeEndElement( );

}

unsigned int AMSTrace::trcPattern( )
{
    return globalInstance.itsTracePattern;
}

void AMSTrace::setTrcPattern( unsigned int trcPattern )
{
    globalInstance.itsTracePattern = trcPattern;
}

quint8 AMSTrace::trcLevel ( )
{
	return globalInstance.m_trcLevel;
}

void AMSTrace::setTrcLevel(qint8 trcLevel)
{
	globalInstance.m_trcLevel = trcLevel;
}

void AMSTrace::trc( const QString & telegram, int comStatus )
{
    /* 1. write string to raw data file */
    globalInstance.itsTraceFile.write( telegram.toLocal8Bit() );

    /* 2. interpret telegram */
    QStringList traceData;
    if ( globalInstance.itsDataProcessor->processTraceData( globalInstance.itsTracePattern, comStatus, telegram, traceData ) )
		globalInstance.trace( 0, traceData );
}

void AMSTrace::trc(const int trcLevel, const QString & msg)
{
	if (&globalInstance == 0 || (trcLevel > globalInstance.m_trcLevel))
		return;
	globalInstance.trace(trcLevel, msg);
}

void AMSTrace::trc(const int trcLevel, const QStringList & msgs)
{
	if ((&globalInstance == 0 || (globalInstance.m_trcMode & Trace2Model)==0) || (trcLevel > globalInstance.m_trcLevel))
		return;
	globalInstance.trace(trcLevel, msgs);
}

void AMSTrace::trc(const int trcLevel, const void* logmsg)
{
	QStringList traceData;
	if ((&globalInstance == 0 || (globalInstance.m_trcMode & Trace2Model)==0) || (trcLevel > globalInstance.m_trcLevel))
		return;

	if(globalInstance.itsDataProcessor->processTraceData(trcLevel, logmsg, &traceData))
		globalInstance.trace(trcLevel, traceData);
}

void AMSTrace::trace(const int trcLevel, const QStringList msg)
{
	QList<QStandardItem*> list;
	itsTraceMutex.lock();
	foreach(QString str,msg)
	{
		list.append(new QStandardItem(str));
	}
    if(globalDataModel)
    {
        if(globalDataModel->rowCount() >= itsBufferSize)
        {
            emit clearTrace(); // this has to be done in the GUI thread otherwise the app crashes !
        }
        globalDataModel->appendRow(list);
    }
	itsTraceMutex.unlock();
}

void AMSTrace::trace(const int trcLevel, const QString msg)
{
	if (trcLevel > globalInstance.m_trcLevel)
		return;

	static QMutex mutex;
	mutex.lock();
	if ((m_trcMode & Trace2Signal) != 0)
		emit traceSignal(msg);

	if ((m_trcMode & Trace2Stdout) != 0)
		qDebug() << msg;

	mutex.unlock();
}

QStandardItemModel* AMSTrace::getDataModel()
{
	return globalInstance.globalDataModel;
}

void AMSTrace::setDataProcessor(AMSTraceInterface* dataProcessor)
{
	if(NULL != dataProcessor)
		itsDataProcessor = dataProcessor;
}

bool AMSTrace::isToTrace( unsigned int trcPattern, unsigned int otherTrcPattern )
{
    return ( ( trcPattern & otherTrcPattern ) 
        & ( ~AMS_COM_TRACE_FLUSH )     /* do not view the flush in the trace window */
        ); 
}

#define TIME_INFO   "[" + QTime::currentTime().toString("hh:mm:ss.zzz") + "] "
#define THREAD_INFO "|" + QString("0x%1").arg((ulong)QThread::currentThreadId(), 4, 16, QChar('0')) + "| - "

uint AMSTraceModule::maxModuleNameLength = 0;

AMSTraceModule::AMSTraceModule(QString name)
{
	this->traceModuleName = name;
	maxModuleNameLength = MAX_MODULE_NAME_SIZE;
}

AMSTraceModule::~AMSTraceModule()
{
}

void AMSTraceModule::warning(const QString msg)
{
	if ((uint) traceModuleName.size() < maxModuleNameLength)
		traceModuleName += QString(
				maxModuleNameLength - traceModuleName.size(), QChar(' '));

	QString trc = TIME_INFO + traceModuleName + THREAD_INFO;
	trc += "WARNING: " + msg;
	AMSTrace::trc(0, trc);

#if QT_VERSION < 0x050000
    qFatal(trc.toAscii().data());
#else
    qFatal(trc.toLatin1().data());
#endif
	
}

void AMSTraceModule::error(const QString msg)
{
	if ((uint) traceModuleName.size() < maxModuleNameLength)
		traceModuleName += QString(
				maxModuleNameLength - traceModuleName.size(), QChar(' '));

	QString trc = TIME_INFO + traceModuleName + THREAD_INFO;
	trc += "ERROR: " + msg;
	AMSTrace::trc(0, trc);

#if QT_VERSION < 0x050000
    qFatal(trc.toAscii().data());
#else
    qFatal(trc.toLatin1().data());
#endif
	
}

void AMSTraceModule::fatal(const QString msg)
{
	if ((uint) traceModuleName.size() < maxModuleNameLength)
		traceModuleName += QString(
				maxModuleNameLength - traceModuleName.size(), QChar(' '));

	QString trc = TIME_INFO + traceModuleName + THREAD_INFO;
	trc += "FATAL: " + msg;
	AMSTrace::trc(0, trc);
	
#if QT_VERSION < 0x050000
    qFatal(trc.toAscii().data());
#else
    qFatal(trc.toLatin1().data());
#endif
}

void AMSTraceModule::trc(const int trcLevel, const QString msg)
{
	if ((uint) traceModuleName.size() < maxModuleNameLength)
	{
		traceModuleName += QString(maxModuleNameLength - traceModuleName.size(), QChar(' '));
	}
	QString trc = TIME_INFO + traceModuleName + THREAD_INFO;
	trc += msg;
	AMSTrace::trc(trcLevel, trc);
}

void AMSTraceModule::trcBytes(const int trcLevel, const QString msg,
		const QByteArray bytes, const QString pattern)
{
	QString data;
	for (int i = 0; i < bytes.size(); i++)
		data += QString(pattern).arg((unsigned char) bytes[i], 2, 16,
				QChar('0'));
	trc(trcLevel, msg + " " + QString::number(bytes.size()) + " Bytes: " + data);
}

void AMSTraceModule::strc(const int trcLevel, const QString msg)
{
	QString trc = TIME_INFO + "STATIC              " + THREAD_INFO;
	trc += msg;
	AMSTrace::trc(trcLevel, trc);
}

void AMSTraceModule::strcBytes(const int trcLevel, const QString msg,
		const QByteArray bytes, const QString pattern)
{
	QString data;
	for (int i = 0; i < bytes.size(); i++)
		data += QString(pattern).arg((unsigned char) bytes[i], 2, 16,
				QChar('0'));
	strc(trcLevel, msg + " " + QString::number(bytes.size()) + " Bytes: "
			+ data);
}

void AMSTraceModule::strc(const QString moduleName, const int trcLevel,
		const QString msg)
{
	QString traceModuleName = moduleName;
	if ((uint) traceModuleName.size() < MAX_MODULE_NAME_SIZE)
		traceModuleName += QString(MAX_MODULE_NAME_SIZE
				- traceModuleName.size(), QChar(' '));

	QString trc = TIME_INFO + traceModuleName + THREAD_INFO;
	trc += msg;
	AMSTrace::trc(trcLevel, trc);
}

void AMSTraceModule::strcBytes(const QString moduleName, const int trcLevel,
		const QString msg, const QByteArray bytes, const QString pattern)
{
	QString data;
	for (int i = 0; i < bytes.size(); i++)
		data += QString(pattern).arg((unsigned char) bytes[i], 2, 16,
				QChar('0'));
	strc(moduleName, trcLevel, msg + " " + QString::number(bytes.size())
			+ " Bytes: " + data);
}

bool AMSTraceStandardDataProcessor::processTraceData(const int level,const void* in, QStringList* out)
{
	QString trace("");
	QStringList *msgs = (QStringList*)in; // can be any format (QList, struct ...) - Note: qboject_cast is not possible with QStringList!

	QString cmd((*msgs)[0]); // command
	QString reg = QString("0x%1").arg(((*msgs)[1]).toUInt(),0,16); // register
	QString mask = QString("0x%1").arg(((*msgs)[2]).toUInt(),0,16); // mask
	QString value = QString("0x%1").arg(((*msgs)[3]).toUInt(),0,16); // value
	QString err = ((*msgs)[4]); // error msg

	QString comment;

	if (cmd == "Read")
	{
		comment = QString("Read Register %1").arg(reg);
		*out << "Read" << reg << mask << (*msgs)[3] << err << comment;
	}
	else if (cmd == "Write")
	{
		comment = QString("Write Register %1").arg(reg);
		*out << "Write" << reg << mask << value << err << comment;
	}
	else if (cmd == "Modify")
	{
		comment = QString("Modify Register %1, mask %2").arg(reg).arg(mask);
		*out << "Modify"  << reg << mask << value << err << comment;
	}
	return true;
}

bool AMSTraceInterface::processTraceData( unsigned int tracePattern, int comStatus, const QString & telegram, QStringList & list )
{
    list.clear();
    /* get information from the telegram string and write to stringlist */
    QXmlStreamReader xml( telegram );
    while ( ! xml.atEnd() && ! xml.hasError() )
    {
        /* we need at a telegram */
        if ( xml.isStartElement() && xml.name() == AMS_COM_XML_TAG )
        {  /* get all attributes and its value of a tag in attrs variable.			*/			
            QXmlStreamAttributes attrs = xml.attributes();
            /*get value of each attribute from QXmlStreamAttributes */
            QStringRef pid = attrs.value( AMS_COM_XML_PROTOCOL_ID );
            QStringRef dir = attrs.value( AMS_COM_XML_DIRECTION );
            QStringRef trace = attrs.value( AMS_COM_XML_TRACE_PATTERN );
            if ( ! pid.isEmpty() && ! dir.isEmpty() && ! trace.isEmpty() )
            { 
                bool result;
                unsigned char protocol  = static_cast< unsigned char >( pid.toString().toInt( & result, AMS_COM_XML_NUMBER_BASE ) );
                QString direction       = dir.toString();
                unsigned int trcPattern = trace.toString().toUInt( 0, AMS_COM_XML_NUMBER_BASE );
                if ( result && AMSTrace::isToTrace( trcPattern, tracePattern ) )
                {
                   AmsComObject * obj = AmsComXmlReader::clone( &xml, protocol );
                   if ( obj && obj->fill( &xml ) ) /* read out the data from the xml-file */
                   {
                       interpretAmsComObject( direction, comStatus, obj, list ); /* this function can be project specific implemented */
                   }
                }
            }
        }
        xml.readNext();
    }
    return ! list.isEmpty();
}


void AMSTraceInterface::interpretAmsComObject( const QString & direction, int comStatus, AmsComObject * obj, QStringList & list )
{
    // layout of columns:
    // |-----------+------------+--------+---------+----------+-----------+
    // | "Command" | "Register" | "Mask" | "Value" | "Status" | "Comment" |
    // |-----------+------------+--------+---------+----------+-----------+

    QString slaveAddress; /* is not part of the default trace window */ 
    QString regAddress;
    QString mask = ""; /* ams-com never uses a mask, as modify is never used */
    QString value;
    QString status( ( comStatus ? "NOK" : "OK" ) ); /* if you want your error-codes in number -> change this */
    QString command;
    QString comment( "" );

    if ( direction == AMS_COM_XML_WRITE )
    {
        command = "Write";
    }
    else if ( direction == AMS_COM_XML_READ_RESULT )
    {
        command = "Read";
    }
    else
    {
        return; /* read is not traced, as the real info is in read-result */
    }

    /* here starts the protocol specific part */
    if ( dynamic_cast< I2cRegisterObject * >( obj ) )
    { /* we read in an register -> not a block */
        I2cRegisterObject * reg = dynamic_cast< I2cRegisterObject * >( obj );
        slaveAddress.setNum( reg->i2cSlaveAddress(), 16 );
        slaveAddress.prepend( "0x" );
        value.setNum( reg->get(), 16 );
        value.prepend( "0x" );
        regAddress.setNum( reg->address(), 16 );
        regAddress.prepend( "0x" );

    } 
    else if ( dynamic_cast< I2cBlockObject * >( obj ) )
    {
        I2cBlockObject * blk = dynamic_cast< I2cBlockObject * >( obj );
        if ( blk )
        {/* else this is an block read/write */
            command.append( "Block" );
            slaveAddress.setNum( blk->i2cSlaveAddress(), 16 );
            slaveAddress.prepend( "0x" );
            regAddress.setNum( blk->address(), 16 );
            regAddress.prepend( "0x" );
            unsigned char * p = blk->get();
            value.append( "{" );
            int i;
            for ( i = 0; i < blk->size( ); ++i )
            {
                unsigned int j = p[i];
                value.append( QString("0x%1,").arg( j, 2, 16, QChar( '0' ) ) );
            }
            /* remove trailing ',' */
            value.resize( value.size() - 1 );
            value.append( "}" );
        }
    }
    else if ( dynamic_cast< I2cConfigObject * >( obj ) )
    {
        I2cConfigObject * cnf = dynamic_cast< I2cConfigObject * >( obj );
        command = "I2CConfig";
        slaveAddress = "";
        regAddress = "";
        value = QString("Module=%1, Frequency=%2").arg(cnf->module()).arg(I2cConfigObject::clockModeToFrequency(cnf->clockMode()));
    }
    else if ( dynamic_cast< ResetObject * >( obj ) )
    {
        ResetObject * res = dynamic_cast< ResetObject * >( obj );
        command = "Reset";
        slaveAddress = "";
        regAddress = "";
        value.clear();
        unsigned int resObjs = res->objectsToReset();
        if ( resObjs & AMS_COM_RESET_PERIPHERAL )
        {
            resObjs -= AMS_COM_RESET_PERIPHERAL;
            value = "Peripheral ";
        }
        if ( resObjs & AMS_COM_RESET_MCU )
        {
            resObjs -= AMS_COM_RESET_MCU;
            value.append( "MCU " );
        }
        if ( resObjs )
        {
            value.append( "0x" );
            QString temp;
            temp.setNum( resObjs, 16 );
            value.append( temp );
        }
    }
    else if ( dynamic_cast< GetFirmwareInfoObject * >( obj ) )
    {
        command = "Version";
        GetFirmwareInfoObject * vers = dynamic_cast< GetFirmwareInfoObject * >( obj );
        const char * p = reinterpret_cast< const char * >( vers->get() );
        value = p;
        slaveAddress = "";
        regAddress = "";
    }
    /* add any other protocols here */

    /* here ends the protocol specific part */

    if ( !value.isEmpty() )
    {
        list << command 
             /* default layout does not report the slave address, so we do not insert: << slaveAddress */
             << regAddress 
             << mask 
             << value 
             << status 
             << comment;
    }
}
