/*
 *****************************************************************************
 * Copyright @ 2014 by austriamicrosystems AG                                *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       * 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 *      PROJECT:   AMSTraceWindow
 *      $Revision: $
 *      LANGUAGE:  C++ / Qt
 */

/*! \file
 *
 *  \author Martin Pelzmann
 *
 *  \brief  AMSTraceWindow command plugin class
 *
 *  Class that acts as as plugin to implement custom commands in the trace window.
 *  
 */

#include "AMSTraceWindowCommandPlugin.hxx"

AMSTraceWindowCommandPlugin::AMSTraceWindowCommandPlugin() : 
itsCom(NULL), 
itsRegisterMap(NULL),
itsTvTraceOutput(NULL),
itsTvProgram(NULL),
itsDataModel(NULL),
itsProgramModel(NULL)
{
}

AMSTraceWindowCommandPlugin::~AMSTraceWindowCommandPlugin()
{
}

QStringList AMSTraceWindowCommandPlugin::getParameterNames()
{
	return itsParameterNames;
}

void AMSTraceWindowCommandPlugin::setCommunication(AMSCommunication *com)
{
	itsCom = com;
}

void AMSTraceWindowCommandPlugin::setRegisterMap(RegisterMap *registerMap)
{
	itsRegisterMap = registerMap;
}

QString AMSTraceWindowCommandPlugin::getPluginCommandName()
{
	return itsPluginCommandName;
}

void AMSTraceWindowCommandPlugin::setTraceOutputTableView(QTableView *tvTraceOutput)
{
	itsTvTraceOutput = tvTraceOutput;
}

void AMSTraceWindowCommandPlugin::setProgramTableView(QTableView *tvProgram)
{
	itsTvProgram = tvProgram;
}

void AMSTraceWindowCommandPlugin::setDataModel(QStandardItemModel *dataModel)
{
	itsDataModel = dataModel;
}

void AMSTraceWindowCommandPlugin::setProgramModel(QStandardItemModel *programModel)
{
	itsProgramModel = programModel;
}

