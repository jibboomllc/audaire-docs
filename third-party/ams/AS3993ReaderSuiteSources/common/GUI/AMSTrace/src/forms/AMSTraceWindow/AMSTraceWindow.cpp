/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSTraceWindow
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author M. Dedek
 *
 *  \brief  AMSTraceWindow class
 *
 *  Class which implements a line edit window to be used for trace comments
 *  Also shows how to derivate from the Trace class and how to use it
 *  
 */

#if QT_VERSION < 0x050000
#else
#include <QFileDialog>
#endif

#include "AMSTraceWindow.hxx"
#include <QClipBoard>

AMSTraceWindow::AMSTraceWindow(QWidget* parent, AMSCommunication *com, QStandardItemModel* model, RegisterMap* map) 
: QWidget (parent)
{
	itsCustomHeaderUsed = false;
    itsProgramModeEnabled = true;
	
	this->setupUi(this);
	this->itsCom = com;
	itsDataModel = model;
	itsRegisterMap = map;
	
	if(itsRegisterMap == NULL)
		this->pbSaveChipStatus->setEnabled(false);
	
	if(itsDataModel==NULL)
		itsDataModel = AMSTrace::getDataModel();
	
	itsTableHeader << "Command" << "Register" << "Mask" << "Value" << "Status" << "Comment"; 
	
	if(itsDataModel != NULL)
	{
		tvTraceOutput->verticalHeader()->setDefaultSectionSize(20); //setHeight

#if QT_VERSION < 0x050000
        tvTraceOutput->verticalHeader()->setResizeMode(QHeaderView::Fixed);
#else
        tvTraceOutput->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
#endif
                
		tvTraceOutput->verticalHeader()->hide();
		tvTraceOutput->horizontalHeader()->setStretchLastSection(true);
		tvTraceOutput->setColumnWidth(0,60);
		tvTraceOutput->setColumnWidth(1,40);
		tvTraceOutput->setColumnWidth(2,40);
		tvTraceOutput->setColumnWidth(3,60);
		tvTraceOutput->setColumnWidth(3,60);
		tvTraceOutput->setModel(itsDataModel);
		tvTraceOutput->setSelectionBehavior(QAbstractItemView::SelectRows);
		itsTraceSelectionModel = tvTraceOutput->selectionModel();
		itsDataModel->setHorizontalHeaderLabels(itsTableHeader) ;
		connect(itsDataModel,SIGNAL(rowsAboutToBeInserted(const QModelIndex &, int, int)), this, SLOT(rowsAboutToBeInserted(const QModelIndex &, int, int)));
		connect(AMSTrace::getInstance(), SIGNAL(clearTrace()), this, SLOT(clearTrace()), Qt::DirectConnection);
	}
	itsProgramModel = new QStandardItemModel();
	tvProgram->setModel(itsProgramModel);
	tvProgram->setSelectionBehavior(QAbstractItemView::SelectRows);
	itsProgramSelectionModel = tvProgram->selectionModel();
//	connect(this->pbCopyClipboard,SIGNAL(clicked()),this,SLOT(on_copyPB_clicked()));
	itsProgramModel->setHorizontalHeaderLabels(itsTableHeader);

#if QT_VERSION < 0x050000
    tvProgram->verticalHeader()->setResizeMode(QHeaderView::Fixed);
#else
    tvProgram->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
#endif
        
	tvProgram->verticalHeader()->setDefaultSectionSize(20); //setHeight
	tvProgram->horizontalHeader()->setStretchLastSection(true);
	itsSerializer = new AMSTraceSerializer();
	itsTracePlayer = new AMSStandardTracePlayer(itsProgramModel, itsCom);
	AMSTrace::getInstance()->setBufferSize(this->sbTraceBufferSize->value());
	connect(AMSTrace::getInstance(), SIGNAL(traceBufferClear(bool)), this, SLOT(on_pbClearTrace_clicked(bool)));

	coCustomCommand->hide();
	pbAddCustomCommand->hide();
}

AMSTraceWindow::~AMSTraceWindow()
{
}

void AMSTraceWindow::closeEvent(QCloseEvent *event)
{
	if(itsCustomHeaderUsed)
	{
		itsTraceSettings.beginGroup("CustomTraceHeaders");
		QString settingsKeyTrace("/HT%1");
		QString settingsKeyProgram("/HP%1");
		for(int i=0;i<itsTableHeader.length();i++)
		{
			itsTraceSettings.setValue(settingsKeyTrace.arg(i),(int)tvTraceOutput->columnWidth(i));
			itsTraceSettings.setValue(settingsKeyProgram.arg(i),(int)tvProgram->columnWidth(i));
		}
		itsTraceSettings.endGroup();
	}
	event->accept();
}


void AMSTraceWindow::addLog(QStringList lst)
{
	QList<QStandardItem*> list;
	foreach(QString str,lst)
	{
		list.append(new QStandardItem(str));
	}
	itsDataModel->appendRow(list);
}

void AMSTraceWindow::on_pbCopyClipboard_clicked()
{
	bool isEmpty = false;
	int row=0;
	QString clipBoardText;

	int colCount = itsDataModel->columnCount(); // -1 to remove status
	QModelIndexList rowList = itsTraceSelectionModel->selectedRows();
	if(rowList.count() == 0)
	{
		QItemSelection selection(itsDataModel->index(0,0),itsDataModel->index(itsDataModel->rowCount()-1,0));
		itsTraceSelectionModel->select(selection, QItemSelectionModel::Select);
		isEmpty = true;
	}
	foreach(QModelIndex rowIndex, rowList)
	{
		row = rowIndex.row();
		for(int i=0;i< colCount;i++)
		{
			clipBoardText.append(QString("%1;").arg(itsDataModel->item(row,i)->text()));
		}
		clipBoardText.append("\n");
	}
	if(isEmpty)
		itsTraceSelectionModel->clearSelection();
	QApplication::clipboard()->setText(clipBoardText);
}

void AMSTraceWindow::on_pbWriteRegister_clicked()
{
	QString reg = this->leRegister->text();
	QString val = this->leValue->text();
	unsigned char regAddr=0x0, regVal = 0x0;
	bool ok = false;

	if(reg.contains("0x") || cbHexFormat->isChecked())
	{
		regAddr = reg.toShort(&ok, 16);
	}
	else
		regAddr = reg.toShort(&ok, 10);
	
	if(val.contains("0x") || cbHexFormat->isChecked())
	{
		regVal = val.toShort(&ok, 16);
	}
	else
		regVal = val.toShort(&ok, 10);

	if(itsCom)
		this->itsCom->writeRegister(regAddr, regVal, cbVerifyEnable->isChecked(), true, 3);
	
	if(cbAddToProgram->isChecked())
	{
		addModelEntry(itsProgramModel,QStringList() << "Write" << reg << "0xFF" << val << "" << leComment->text());
	}
}

void AMSTraceWindow::on_pbModifyRegister_clicked()
{
	QString reg = this->leRegister->text();
	QString val = this->leValue->text();
	QString mask = this->leMask->text();
	unsigned char regAddr=0x0, regVal = 0x0, regMask = 0x0;
	bool ok = false;

	if(reg.contains("0x") || cbHexFormat->isChecked())
	{
		regAddr = reg.toShort(&ok, 16);
	}
	else
		regAddr = reg.toShort(&ok, 10);

	if(val.contains("0x") || cbHexFormat->isChecked())
	{
		regVal = val.toShort(&ok, 16);
	}
	else
		regVal = val.toShort(&ok, 10);


	if(mask.isEmpty())
		mask = "0xff";

	if(mask.contains("0x") || cbHexFormat->isChecked())
	{
		regMask = mask.toShort(&ok, 16);
	}
	else
		regMask = mask.toShort(&ok, 10);
	
	if(itsCom)
		this->itsCom->modifyRegister(regAddr, regMask, regVal, cbVerifyEnable->isChecked(), true, 2);

	if(cbAddToProgram->isChecked())
	{
		addModelEntry(itsProgramModel,QStringList() << "Modify" << reg << mask << val << "" << leComment->text());
	}
}

void AMSTraceWindow::on_pbReadRegister_clicked()
{
	QString reg = this->leRegister->text();
	unsigned char regAddr=0x0, regVal = 0x0;
	bool ok = false;

	if(reg.contains("0x") || cbHexFormat->isChecked())
	{
		regAddr = reg.toShort(&ok, 16);
	}
	else
		regAddr = reg.toShort(&ok, 10);
	
	if(itsCom)
		this->itsCom->readRegister(regAddr, &regVal,true, 1);
	
	this->leValue->setText(QString("0x%1").arg(QString::number(regVal,16)));

	if(cbAddToProgram->isChecked())
	{
		addModelEntry(itsProgramModel, QStringList() << "Read" << reg << "0xFF" << QString::number(regVal) << "" << leComment->text());
	}
}


void AMSTraceWindow::on_stayOnTopCB_clicked(bool on)
{
	QPoint pos = this->pos();
	if(on)
		this->setWindowFlags(Qt::WindowStaysOnTopHint);
	else
		this->setWindowFlags(0);
	this->move(pos);
	this->show();
}

void AMSTraceWindow::clearTrace()
{
	on_pbClearTrace_clicked(true);
}


void AMSTraceWindow::on_pbClearTrace_clicked(bool on)
{
	itsDataModel->clear();	
	itsDataModel->setHorizontalHeaderLabels(itsTableHeader);
}	

void AMSTraceWindow::on_pbClearProgram_clicked(bool on)
{
	itsProgramModel->clear();	
	itsProgramModel->setHorizontalHeaderLabels(itsTableHeader);
}


void AMSTraceWindow::on_pbLineUp_clicked(bool clicked)
{
	QModelIndex mi = itsProgramSelectionModel->currentIndex();
	if(mi.row() >= 0)
	{
		itsTracePlayer->executeCommand(mi.row());
		itsProgramSelectionModel->setCurrentIndex(mi.sibling(mi.row()-1,mi.column()),QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect);
	}
	if(mi.row() == 0)
		itsProgramSelectionModel->setCurrentIndex(itsProgramModel->index(itsProgramModel->rowCount()-1,0),QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect);
}

void AMSTraceWindow::on_pbLineDown_clicked(bool clicked)
{
	QModelIndex mi = itsProgramSelectionModel->currentIndex();

	if(!mi.isValid())
		itsProgramSelectionModel->setCurrentIndex(itsProgramModel->index(0,0,QModelIndex()),QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect);
	else if(mi.row() < itsProgramModel->rowCount())
	{
		itsTracePlayer->executeCommand(mi.row());
		itsProgramSelectionModel->setCurrentIndex(mi.sibling(mi.row()+1,mi.column()),QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect);
	}
	int tmp = mi.row();
	if((mi.row()+1) == itsProgramModel->rowCount())
		itsProgramSelectionModel->setCurrentIndex(itsProgramModel->index(0,0),QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect);

}

void AMSTraceWindow::on_pbAddDelay_clicked(bool clicked)
{
	QList<QStandardItem*> list;
	QStandardItem* item;
	QStringList lst;
	lst << "Delay" << "" << "" << "100" << "" << "Delay Execution by x ms";
	
	foreach(QString str, lst)
	{
		item = new QStandardItem(str);
		item->setEditable(false);
		list.append(item);
	}
	list.at(3)->setEditable(true);
	if(!itsProgramSelectionModel->hasSelection())
		itsProgramModel->appendRow(list);
	else
		itsProgramModel->insertRow(itsProgramSelectionModel->currentIndex().row()+1,list);
}

void AMSTraceWindow::on_pbDeleteLine_clicked(bool clicked)
{
	QList<QPersistentModelIndex*> tmpList;
	QModelIndexList selected = itsProgramSelectionModel->selectedRows();
	if(selected.isEmpty())
	{
		itsProgramModel->removeRow(itsProgramModel->rowCount()-1);
		goto Exit;
	}
	foreach(QModelIndex idx, selected)
	{
		tmpList.append(new QPersistentModelIndex(idx));
	}
	foreach(QPersistentModelIndex *idx, tmpList)
	{
		itsProgramModel->removeRow(idx->row());
	}
	Exit:
	itsProgramSelectionModel->clearSelection();
}

void AMSTraceWindow::on_pbRun_clicked(bool clicked)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	itsProgramSelectionModel->clearSelection();
	if(itsTracePlayer == NULL)
		return;
	itsTracePlayer->setVerify(cbVerifyEnable->isChecked());
	itsTracePlayer->executeProgram();
	itsProgramSelectionModel->clearSelection();
	QApplication::restoreOverrideCursor();
}


void AMSTraceWindow::on_pbLoadProgram_clicked(bool clicked)
{
	QString fileName =
		QFileDialog::getOpenFileName(this, tr("Open Command File"),
		QDir::currentPath(),
		tr("Command Files (*.xml)"));

	if (fileName.isEmpty())
		return;
	itsSerializer->loadFromXml(fileName, itsProgramModel);
//	tvProgram->setColumnHidden(4,true);
}

void AMSTraceWindow::on_pbSaveProgram_clicked(bool clicked)
{
	QString fileName =
		QFileDialog::getSaveFileName(this, tr("Save Command File"),
		QDir::currentPath(),
		tr("Command Files (*.xml)"));

	if (fileName.isEmpty())
		return;
	itsSerializer->serializeToXml(fileName, itsProgramModel);
}

void AMSTraceWindow::on_pbSaveTrace_clicked(bool clicked)
{
	QString fileName =
		QFileDialog::getSaveFileName(this, tr("Save Command File"),
		QDir::currentPath(),
		tr("Command Files (*.xml)"));

	if (fileName.isEmpty())
		return;
	itsSerializer->serializeToXml(fileName, itsDataModel);
}

void AMSTraceWindow::on_pbSaveChipStatus_clicked(bool clicked)
{
	QList<QStandardItem*> list;
	unsigned char dummy;
	if(itsRegisterMap == NULL)
		return;

//	itsProgramModel->clear();	
	QList<unsigned char> registerList = itsRegisterMap->getRegisters();
	foreach(unsigned char item, registerList)
	{
		list.append(new QStandardItem("Write"));
		list.append(new QStandardItem(""));
		list.append(new QStandardItem("0xFF"));
		list.append(new QStandardItem(""));
		list.append(new QStandardItem(""));
		list.append(new QStandardItem(""));
		this->itsCom->readRegister(item,&dummy,false,99);
		list[1]->setData(QString("0x%1").arg(QString::number(item,16)),Qt::DisplayRole);
		list[3]->setData(QString("0x%1").arg(QString::number(dummy,16)),Qt::DisplayRole);
		list[5]->setText(QString("Write Register 0x%1").arg(QString::number(item,16)));
		itsProgramModel->appendRow(list);
		list.clear();
	}
}

void AMSTraceWindow::on_pbCopySelection_clicked(bool clicked)
{
	bool isEmpty = false;
	int row=0;
	QList<QStandardItem*> list;
	int colCount = itsDataModel->columnCount();
	QModelIndexList selected = itsTraceSelectionModel->selectedRows();
	if(selected.count() == 0)
	{
		QItemSelection selection(itsDataModel->index(0,0),itsDataModel->index(itsDataModel->rowCount()-1,itsDataModel->columnCount()-1));
		itsTraceSelectionModel->select(selection, QItemSelectionModel::Select);
		selected = itsTraceSelectionModel->selectedRows();
		isEmpty = true;
	}

	foreach(QModelIndex rowIndex, selected)
	{
		list.clear();
		row = rowIndex.row();
		for(int i=0;i< colCount;i++)
		{
			list.append(itsDataModel->item(row,i)->clone());
		}
		itsProgramModel->appendRow(list);
	}
	if(isEmpty)
		itsTraceSelectionModel->clearSelection();
}

void AMSTraceWindow::addModelEntry(QStandardItemModel* model, const QStringList lst)
{
	QList<QStandardItem*> list;
	foreach(QString str,lst)
	{
		list.append(new QStandardItem(str));
	}
	model->appendRow(list);
}

void AMSTraceWindow::on_sbTraceLevel_valueChanged(int value)
{
	AMSTrace::setTrcLevel(value);
}

void AMSTraceWindow::setHorizontalHeaderView(QHeaderView* header)
{
	this->tvTraceOutput->setHorizontalHeader(header);
}

void AMSTraceWindow::setHorizontalHeaderLabels(const QStringList & headerLabels)
{
	itsCustomHeaderUsed = true;
	this->itsDataModel->setHorizontalHeaderLabels(headerLabels);
	this->itsProgramModel->setHorizontalHeaderLabels(headerLabels);
	this->itsTableHeader = headerLabels;

	itsTraceSettings.beginGroup("CustomTraceHeaders");
	if(itsTraceSettings.contains("HT0"))
	{
		QString settingsKeyTrace("HT%1");
		QString settingsKeyProgram("HP%1");
		for(int i=0;i<headerLabels.length();i++)
		{
			tvTraceOutput->setColumnWidth(i,itsTraceSettings.value(settingsKeyTrace.arg(i)).toInt());
			tvProgram->setColumnWidth(i,itsTraceSettings.value(settingsKeyProgram.arg(i)).toInt());
		}
	}
	itsTraceSettings.endGroup();
}

void AMSTraceWindow::on_sbTraceBufferSize_valueChanged(int size)
{
	AMSTrace::getInstance()->setBufferSize(size);
}

void AMSTraceWindow::rowsAboutToBeInserted(const QModelIndex &, int, int)
{
	tvTraceOutput->clearSelection();
}

void AMSTraceWindow::setTracePlayer(AMSTracePlayerInterface* tracePlayer)
{
	if(NULL != tracePlayer)
	{
		if(itsTracePlayer)
			delete itsTracePlayer;
		itsTracePlayer = tracePlayer;
	}
	itsTracePlayer->setCommunication(itsCom);
	itsTracePlayer->setModel(itsProgramModel);
}

void AMSTraceWindow::setTraceSerializer(AMSTraceSerializerInterface* traceSerializer)
{
	if(NULL != traceSerializer)
	{
		if(itsSerializer)
			delete itsSerializer;
		itsSerializer = traceSerializer;
	}
}

bool AMSTraceWindow::enableProgramMode(bool enable)
{
    bool tmp = itsProgramModeEnabled;
    gbProgram->setEnabled(enable);
    regReadWriteGB->setEnabled(enable);
    gbDebug->setEnabled(enable);
    tvProgram->setEnabled(enable);
    pbCopySelection -> setEnabled(enable);
    itsProgramModeEnabled = enable;
    return tmp;
}

void AMSTraceWindow::on_pbExport_pressed()
{
	QString fileName =
		QFileDialog::getSaveFileName(this, tr("Export File"),
		QDir::currentPath(),
		tr("Command Files (*.txt)"));

	if (fileName.isEmpty())
		return;
	itsSerializer->serializeToAMS(fileName, itsProgramModel);
}

void AMSTraceWindow::on_pbImport_pressed()
{
	QString fileName =
		QFileDialog::getOpenFileName(this, tr("Import Command File"),
		QDir::currentPath(),
		tr("Command Files (*.txt)"));

	if (fileName.isEmpty())
		return;
	itsSerializer->loadFromAMS(fileName, itsProgramModel);
}

void AMSTraceWindow::on_pbAddComment_clicked(bool clicked)
{
	QList<QStandardItem*> list;
	QStandardItem* item;
	QStringList lst;
	lst << "Comment" << "" << "" << "" << "" << "Double click this cell and add your comment here!";
	
	foreach(QString str, lst)
	{
		item = new QStandardItem(str);
		item->setEditable(false);
		list.append(item);
	}
	list.at(5)->setEditable(true);
	if(!itsProgramSelectionModel->hasSelection())
	{
		itsProgramModel->appendRow(list);
	}
	else
	{
		itsProgramModel->insertRow(itsProgramSelectionModel->currentIndex().row()+1,list);
	}
}

void AMSTraceWindow::on_pbCopyLines_clicked(bool clicked)
{
	QList<QStandardItem*> currentRowCopy;
	QModelIndexList       selectedRows = itsProgramSelectionModel->selectedRows();

	itsClipboard.clear();

	if(selectedRows.isEmpty())
	{
		return;
	}
	
	foreach(QModelIndex currentRow, selectedRows)
	{
		currentRowCopy.clear();

		for (int currentColumn=0;currentColumn < itsProgramModel->columnCount();++currentColumn)
		{
			currentRowCopy.append(itsProgramModel->item(currentRow.row(),currentColumn)->clone());
		}

		itsClipboard.append(currentRowCopy);
	}
}

void AMSTraceWindow::on_pbPasteLines_clicked(bool clicked)
{
	QList<QList<QStandardItem*>> clipboardCopy;

	QList<QStandardItem*> currentRow;
	QList<QStandardItem*> copyRow;
	QStandardItem*        currentColumn;
	QStandardItem*        copyColumn;
	
	// deep copy the clipboard first, otherwise it can be inserted only once!
	foreach(currentRow,itsClipboard)
	{
		copyRow.clear();

		foreach(currentColumn,currentRow)
		{
			copyColumn = currentColumn->clone();
			copyRow.append(copyColumn);
		}

		clipboardCopy.append(copyRow);
	}

	// insert clipboard copy into program list
	foreach(currentRow,clipboardCopy)
	{
		if(!itsProgramSelectionModel->hasSelection())
		{
			itsProgramModel->appendRow(currentRow);
		}
		else
		{
			itsProgramModel->insertRow(itsProgramSelectionModel->currentIndex().row()+1,currentRow);
		}
	}
}

bool AMSTraceWindow::addCommandPlugin(AMSTraceWindowCommandPlugin *commandPlugin)
{
	if (commandPlugin != NULL)
	{
		itsCommandPluginList.append(commandPlugin);
		
		commandPlugin->setRegisterMap(itsRegisterMap);
		commandPlugin->setCommunication(itsCom);
		commandPlugin->setProgramTableView(tvProgram);
		commandPlugin->setTraceOutputTableView(tvTraceOutput);
		commandPlugin->setDataModel(itsDataModel);
		commandPlugin->setProgramModel(itsProgramModel);
		
		coCustomCommand->show();
		pbAddCustomCommand->show();

		coCustomCommand->addItem(commandPlugin->getPluginCommandName());
		itsTracePlayer->setCommandPluginList(itsCommandPluginList);

		return true;
	}
	else
	{
		return false;
	}	
}

void AMSTraceWindow::on_pbAddCustomCommand_clicked(bool clicked)
{
	if (coCustomCommand->count() != itsCommandPluginList.count())
	{
		return;
	}

	AMSTraceWindowCommandPlugin *selectedPlugin = itsCommandPluginList.at(coCustomCommand->currentIndex());

	selectedPlugin->createProgramTableEntry();
}