/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 * AmsReader.h
 *
 *      Author: Bernhard Breinbauer (based on work of stefan detter)
 */

#ifndef AMSREADER_H_
#define AMSREADER_H_

#include "../ReaderInterface.h"
#include "../Gen2ReaderInterface.h"
#include "../epc/EPC_Defines.h"
#include "TagObjects.h"

#include <QMutex>
#include <QTimer>

#include <QrfeTrace.h>
#include <QMap>

#define NOT_RESPONDING_WAIT_TIME_MS			200

class Gen2Parameters;
class InventoryRunner;
struct RawTagInfo;
struct RawInvInfo;

class AmsReader : public ReaderInterface, public Gen2ReaderInterface
	, QrfeTraceModule
{
	Q_OBJECT

public:
	AmsReader(AmsComDriver * driver, DriverType type, QObject* parent = 0);
	virtual ~AmsReader();

public:
	virtual Result getHardwareDescription ( QString &hardwareRevision );
	virtual Result getSoftwareDescription ( QString &softwareRevision );
    virtual Result getSoftwareVersion( unsigned int &versionNumber );
    virtual QStringList supportedReaderList();
    
    virtual Result updateReaderState();

	virtual Result reboot ( );
    virtual Result enterBootloader ( );
	virtual Result setAntennaPower ( bool on, bool eval );

	virtual Result readRegisters (QMap<int, quint8> &value );
	virtual Result readRegister ( quint8 address, quint8 &value );
	virtual Result writeRegister ( quint8 address, quint8 value );
    virtual Result setGen2Config ( int lf_khz, int coding, int session, bool longPilot, int tari, int qbegin, int target );
    virtual Result getGen2Config ( int &lf_khz, int &coding, int &session, bool &longPilot, int &tari, int &qbegin, int &target );
    virtual Result setPowerDownMode( quint8 mode );
    virtual Result getPowerDownMode( quint8 &mode );

    virtual Result getTuner (quint8 id, quint8 &cin, quint8 &clen,quint8 &cout);
    virtual Result setTuner (quint8 id, quint8 cin, quint8 clen, quint8 cout);
    virtual Result startAutoTuner (quint8 autoTune, quint8 &cin, quint8 &clen, quint8 &cout);
    virtual Result getMaxTunerTableSize(quint8 &tableSize);
    virtual Result clearTunerTable();
    virtual Result addTunerTableEntry(ulong frequencyKHz, bool tuneEnable1, quint8 cin1, quint8 clen1, quint8 cout1, quint16 iq1, 
                                                          bool tuneEnable2, quint8 cin2, quint8 clen2, quint8 cout2, quint16 iq2);
    virtual Result setGetTxRxParams( signed char &sensitivity, bool set_sens, unsigned char &antenna, bool set_a );

	virtual Result startSingleInventory ( );

	virtual Result startCyclicInventory ( );
	virtual Result stopCyclicInventory ( );

    virtual Gen2Result readFromTag ( Gen2Tag &tag, quint8 mem_bank, quint32 address, ComByteArray passwd, quint8 count, bool bulkMode, ComByteArray &data );
    virtual Gen2Result writeToTag ( Gen2Tag &tag, quint8 mem_bank, quint32 address, ComByteArray passwd, ComByteArray data, int *writtenBytes=0 );

	virtual Gen2Result writeTagId ( Gen2Tag &tag_Before, QString tagId_After, ComByteArray passwd, int *writtenBytes = 0 );

	virtual Gen2Result lockTag ( Gen2Tag &tag, LOCK_MODE mode, LOCK_MEMORY_SPACE memory, ComByteArray passwd );
	virtual Gen2Result killTag ( Gen2Tag &tag, ComByteArray killPassword  );

    virtual QStringList getRssiChildren();

    virtual QList<struct rssi_val> rssiCommand ( unsigned char measurements, ulong freq_kHz ); 

    virtual Result addFrequency ( bool clear, ulong freqKHz, uchar profile );
    virtual Result getFrequencies ( uchar &profile, ulong &minFreqKHz, ulong &maxFreqKHz, uchar &numFreq, uchar &numFreqGui);
    virtual Result getRssi( ulong freqKHz, int &i, int &q, int &dBm );
    virtual Result getReflectedPower( ulong freqKHz, bool set_tune, int &i, int &q );
    virtual Result setHoppingParams( ushort listenTime_ms, ushort allocationTime_ms, ushort idleTime_ms, signed char rssi );
    virtual Result getHoppingParams( ushort &listenTime_ms, ushort &allocationTime_ms, ushort &idleTime_ms, signed char &rssi );
    virtual Result continuousSend( ulong freqKHz, ushort timeout_ms, uchar random );

	virtual double getG_general(void);
	virtual double getG_rfp(void);
	virtual quint32 intervalTime();
	virtual void 	setIntervalTime(quint32 time);

	virtual Gen2ReaderInterface* getGen2Reader();

	virtual QString errorToString( Gen2Result result);

    Gen2ReaderInterface::Gen2Result TxRxNBytes ( const QByteArray &tx, QByteArray &rx, unsigned int &rxlength );

    Gen2ReaderInterface::Gen2Result genericCommand(Gen2Tag &tag, QByteArray genericCommand, QByteArray password, QByteArray &rxBuffer, unsigned int &rxLength);

    Gen2Result selectTag( Gen2Tag &tag );
    ReaderInterface::Result changePASetting( quint8 &g8, quint8 &g16, bool readwrite);
    void setGen2Select(int option);

public slots:
	virtual void deviceWasRemoved();

    void inventoryResultFromRunner();                           // slot is called from inventory runner
    void runnerFinishedSingleInventory();                       // slot is called from inventory runner

protected:
    bool ReaderInterface::isAutoAckSupported();

private:
    struct Rssi interpretRSSI(quint8 rssi);

    bool getReaderVersion();

	uint m_notRespondingPauseCounter;
    double m_currentInventoryGValue;

	InventoryRunner * m_runner;

	const static QStringList m_rssiChildNames;
    QString m_hwInfo;
    QString m_fwInfo;
    unsigned int m_fwVersion;

    static QStringList theSupportedReaderList;
    int m_Gen2SelectWithTID;
};


#endif /* AMSREADER_H_ */
