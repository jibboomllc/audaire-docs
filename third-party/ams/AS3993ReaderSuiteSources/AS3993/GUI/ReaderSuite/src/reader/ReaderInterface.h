/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 * QrfeProtocollHandlerBase.h
 *
 *  Created on: 15.01.2009
 *      Author: stefan.detter
 */

#ifndef READERINTERFACE_H_
#define READERINTERFACE_H_

#include <QObject>
#include <QIODevice>
#include <QStringList>
#include <QMutex>
#include <QThread>

#include "AmsComStream.h"
#include "ComGlobals.h"

#define AMS_VID 0x1325
#define AMS_PID 0xC02E

#define NO_TUNER    0x00
#define TUNER_CIN   0x01
#define TUNER_CLEN  0x02
#define TUNER_COUT  0x04

class Gen2ReaderInterface;
class AmsComDriver;

struct Rssi
{
    double pin;
    double pinMin;
    double pinMax;
    quint8 i;
    quint8 q;
};

struct InventoryTagInfo
{
    QString epc;
    Rssi rssi;
    int agc;
    bool subCarrierInPhase;
    bool qChannel;
    int freq;
    QString tid;
};

class ReaderInterface : public QObject
{   
	Q_OBJECT
	Q_ENUMS( DriverType Result TagType HandlerState HandlerAction )

public:
    enum DriverType;
	ReaderInterface(AmsComDriver * driver, DriverType type, QObject* parent = 0);
	virtual ~ReaderInterface();

    enum DriverType {
        HID,
        UART
    };

    enum Result {
		OK,
		Error,
		ComError,
		NoResponse,
		NA
	};

	enum TagType {
		TAG_ISO6B,
		TAG_GEN2,
        TAG_GEN2_FAST,
        TAG_GEN2_TID,
		TAG_RFE_AUR1,
		TAG_UNKWON
	};

	enum HandlerState {
		STATE_ONLINE,
		STATE_OFFLINE,
		STATE_NOTRESPONDING,
        STATE_ERROR,
		STATE_UNKOWN_ERROR
	};

	enum HandlerAction {
		ACTION_OFFLINE,
		ACTION_IDLE,
		ACTION_SCANNING,
		ACTION_WAITING
	};
    enum RssiModes {
        RSSI_REALTIME = 0x00,
        RSSI_QUIET    = 0x02,
        RSSI_PILOT    = 0x04,
        RSSI_2ndBYTE  = 0x06,
        RSSI_PEAK     = 0x08
    };
    enum InitError { NoError=0, SpiError, EnError, IrqError, CrystalError, PllError, UnknownError=0xFF };
    enum VCO { IntVco=0, ExtVco, VcoError=0xFF };
    enum PA { IntPa=0, ExtPa, PaError=0xFF };
    enum InputConfig { Balanced=0, Single, InputError=0xFF };
    enum ChipId { InvalidChip = 0, AS3980=3980, AS3993=3993 };
    enum HardwareId { InvalidHardware = 0, Eval, Fermi, FermiBig, Femto1v0, Femto1v1, Femto2v0, Mega, Radon, Femto2v1, Newton };
    /* AS3994 does not have an ID of its own as it is not different from the SW point of view. */

public:
	virtual Result initDevice( );						    					/*!<@brief Pure virtual function to initialize the device */
    virtual QString driverTypeString();
    virtual DriverType driverType();
    virtual QStringList supportedReaderList() = 0;

	virtual Result getReaderID ( QString &readerID );   						/*!<@brief Pure virtual function to get reader id */
	virtual Result getHardwareDescription ( QString &hardwareDescription ) = 0;	/*!<@brief Pure virtual function to get hardware description (containing version) */
	virtual Result getSoftwareDescription ( QString &softwareDescription ) = 0;	/*!<@brief Pure virtual function to get software description (containing version) */
    virtual Result getSoftwareVersion( unsigned int &versionNumber ) = 0;       /*!<@brief Pure virtual function to get software version number */

    virtual Result updateReaderState() = 0;
    virtual void getReaderInitStatus( bool &wrongChip, InitError &initerror);
    virtual bool getReaderAntennaSwitch();
    virtual quint16 getReaderTunerConfig();
    virtual ChipId getChipId();
    virtual HardwareId getHardwareId();

	virtual Result reboot ( ) = 0;												/*!<@brief Pure virtual function to reboot the reader */
    virtual Result enterBootloader ( ) = 0;                                     /*!<@brief Pure virtual function to enter bootloader of the reader */
   	virtual Result setAntennaPower ( bool on, bool eval ) = 0;					/*!<@brief Pure virtual function to set antenna power */

    virtual Result readRegister ( quint8 address, quint8 &value ) = 0;		/*!<@brief Pure virtual function to read a register */
	virtual Result writeRegister ( quint8 address, quint8 value ) = 0;		/*!<@brief Pure virtual function to write a register */
    virtual Result readRegisters ( QMap<int, quint8> &value ) = 0;                     /*!<@brief Pure virtual function to read all registers from reader */
    virtual Result setGen2Config ( int lf_khz, int coding, int session, bool longPilot, int tari, int qbegin, int target ) = 0;
    virtual Result getGen2Config ( int &lf_khz, int &coding, int &session, bool &longPilot, int &tari, int &qbegin, int &target ) = 0;
    virtual Result setPowerDownMode( quint8 mode ) = 0;
    virtual Result getPowerDownMode( quint8 &mode ) = 0;

    virtual Result getTuner (quint8 id, quint8 &cin, quint8 &clen, quint8 &cout) = 0;
    virtual Result setTuner (quint8 id, quint8 cin, quint8 clen, quint8 cout) = 0;
    virtual Result startAutoTuner (quint8 autoTune, quint8 &cin, quint8 &clen, quint8 &cout) = 0;
    virtual Result getMaxTunerTableSize(quint8 &tableSize) = 0;
    virtual Result clearTunerTable() = 0;
    virtual Result addTunerTableEntry(ulong frequencyKHz, bool tuneEnable1, quint8 cin1, quint8 clen1, quint8 cout1, quint16 iq1,
                                                          bool tuneEnable2, quint8 cin2, quint8 clen2, quint8 cout2, quint16 iq2) = 0;
    virtual Result setGetTxRxParams( signed char &sensitivity, bool set_sens, unsigned char &antenna, bool set_a ) = 0;
    virtual double getG_general(void) = 0;
    virtual double getG_rfp(void) = 0;

    virtual Result addFrequency ( bool clear, ulong freqKHz, uchar profile ) = 0;
    virtual Result getFrequencies ( uchar &profile, ulong &minFreqKHz, ulong &maxFreqKHz, uchar &numFreq, uchar &numFreqGui) = 0;
    virtual Result getRssi( ulong freqKHz, int &i, int &q, int &dBm ) = 0;
    virtual Result getReflectedPower( ulong freqKHz, bool set_tune, int &i, int &q ) = 0;
    virtual Result setHoppingParams( ushort listenTime_ms, ushort allocationTime_ms, ushort idleTime_ms, signed char rssi ) = 0;
    virtual Result getHoppingParams( ushort &listenTime_ms, ushort &allocationTime_ms, ushort &idleTime_ms, signed char &rssi ) = 0;
    virtual Result continuousSend( ulong freqKHz, ushort timeout_ms, uchar random ) = 0;

	virtual Result setTagType(TagType type);    								/*!<@brief virtual function to set the tag type */
    virtual Result setAutoAckMode(bool on);

  	virtual QStringList getRssiChildren();										/*!<@brief Function to get the names (and count) of the Rssi entries of the reader */

    virtual quint32 intervalTime() = 0;
    virtual void 	setIntervalTime(quint32 time) = 0;

    virtual bool rssiEnabled ();
    virtual void setRssiEnabled ( bool on );
    void setTIDEnabled ( bool on );

    virtual RssiModes rssiMode();
    virtual void setRssiMode ( RssiModes mode );

	virtual Result startSingleInventory ( ) = 0;            					/*!<@brief Pure virtual function to do a single inventory */

	virtual Result startCyclicInventory ( ) = 0;								/*!<@brief Pure virtual function to start cyclic inventory */
	virtual Result stopCyclicInventory ( ) = 0;									/*!<@brief Pure virtual function to stop cyclic inventory */

	virtual Gen2ReaderInterface* getGen2Reader();							    /*!<@brief Virtual function to get the pointer to the gen2 reader */
    virtual ReaderInterface::Result changePASetting( quint8 &g8, quint8 &g16, bool readwrite ) = 0;

    virtual AmsComStream & stream();

public:
	QString 		readerId();
	TagType			tagType();

	HandlerState 	currentState();
	QString 		currentStateString();
    virtual QString currentStateDescription();

	HandlerAction 	currentAction();
	QString 		currentActionString();

    static QString epcToString  ( const QByteArray epc );
    static QString epcToString  ( const ComByteArray epc );
    static ComByteArray stringToEpc  ( const QString epc, bool * ok = 0  );

protected:
	void setState(HandlerState state);
	void setAction(HandlerAction action);
    Result checkStreamError();
    virtual bool isAutoAckSupported() = 0;

public slots:
	virtual void deviceWasRemoved();

	void releaseDeviceImmediatly();

signals:
    void inventoryRoundDone(QString readerId);
	void inventoryResult(QString readerId, QList<InventoryTagInfo>, quint32 freq, quint8 status);
    
	void changedState(ReaderInterface::HandlerState state, QString stateName, QString stateDescription);
	void changedAction(QString action);

	void changedRssiEnable(QString readerId, bool on, uchar rssiChildCount, QStringList rssiChildNames);
    void changedTIDEnable(QString,bool);

    void lostConnection(ReaderInterface*);

protected:
    AmsComDriver*   m_driver;
    AmsComStream    m_stream;
    bool            m_isConfigured;
    DriverType      m_driverType;

	QString 		m_readerID;
	TagType			m_tagType;
    bool            m_autoAckMode;
    static int      theReaderCount;     //number of instantiated readers

	HandlerState 	m_currentState;
	HandlerAction 	m_currentAction;

    bool 	m_rssiActive;
    RssiModes m_rssiMode;

    bool m_wrongChipId;
    ChipId m_chipId;
    HardwareId m_hardwareId;
    InitError m_initError;
    VCO m_vco;
    PA m_pa;
    InputConfig m_input;
    bool m_antennaSwitch;
    quint16 m_tunerConfig;
    
};


#endif /* READERINTERFACE_H_ */
