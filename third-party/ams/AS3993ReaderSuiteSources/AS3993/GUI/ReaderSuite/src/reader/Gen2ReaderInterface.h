/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*!
 * @file	Gen2ReaderInterface.h
 * @brief	Interface extension for gen2 reader
 */

#ifndef GEN2READERINTERFACE_H_
#define GEN2READERINTERFACE_H_

#include "epc/EPC_Defines.h"
#include "ComGlobals.h"
#include <QHash>
/* Firmware error codes, taken from errno.h and errno_as3993.h */

#define ERR_NONE     0 /*!< no error occured */
#define ERR_NOMEM   -1 /*!< not enough memory to perform the requested operation */
#define ERR_BUSY    -2 /*!< device or resource busy */
#define ERR_IO      -3 /*!< generic IO error */
#define ERR_TIMEOUT -4 /*!< error due to timeout */
#define ERR_REQUEST -5 /*!< invalid request or requested function can't be executed at the moment */
#define ERR_NOMSG   -6 /*!< No message of desired type */
#define ERR_PARAM   -7 /*!< Parameter error */

#define ERR_LAST_ERROR -32

#define ERR_FIRST_AS3993_ERROR (ERR_LAST_ERROR-1)

/* Errors primarily raised by AS3993 itself, however codes like ERR_CHIP_CRCERROR can be reused by ISO6B */
#define ERR_CHIP_NORESP               (ERR_FIRST_AS3993_ERROR - 0)
#define ERR_CHIP_HEADER               (ERR_FIRST_AS3993_ERROR - 1)
#define ERR_CHIP_PREAMBLE             (ERR_FIRST_AS3993_ERROR - 2)
#define ERR_CHIP_RXCOUNT              (ERR_FIRST_AS3993_ERROR - 3)
#define ERR_CHIP_CRCERROR             (ERR_FIRST_AS3993_ERROR - 4)
#define ERR_CHIP_FIFO                 (ERR_FIRST_AS3993_ERROR - 5)

#define ERR_REFLECTED_POWER           (ERR_FIRST_AS3993_ERROR - 16)

/* Upper level protocol errors, e.g. a Write can fail when access command has failed... */
#define ERR_GEN2_SELECT               (ERR_FIRST_AS3993_ERROR - 32)
#define ERR_GEN2_ACCESS               (ERR_FIRST_AS3993_ERROR - 33)
#define ERR_GEN2_REQRN                (ERR_FIRST_AS3993_ERROR - 34)
#define ERR_GEN2_CHANNEL_TIMEOUT      (ERR_FIRST_AS3993_ERROR - 35)

/* ISO6b errors */
#define ERR_ISO6B_NACK                (ERR_FIRST_AS3993_ERROR - 64)

/* Gen2 Tag Structure*/
struct Gen2Tag
{
    QString epc;
    QString tid;
};

inline bool operator==(const Gen2Tag & element1, const Gen2Tag & element2)
{
    return ((element1.epc ==element2.epc)&&(element1.tid ==element2.tid));
}

inline bool operator<(const Gen2Tag & element1, const Gen2Tag & element2)
{
    if(element1.epc == element2.epc)
        return element1.tid < element2.tid;

    return element1.epc < element2.epc;
}

inline uint qHash(const Gen2Tag & key)
{
    return qHash(key.epc) ^ qHash(key.tid);
}



class Gen2ReaderInterface
{
public:
	enum Gen2Result {
		Gen2_OK = ERR_NONE,
		Gen2_NOMEM = ERR_NOMEM,
		Gen2_BUSY = ERR_BUSY,
		Gen2_IO = ERR_IO,
		Gen2_TIMEOUT = ERR_TIMEOUT,
		Gen2_ERR_REQEUEST = ERR_REQUEST,
		Gen2_ERR_NOMSG = ERR_NOMSG,
		Gen2_ERR_PARAM = ERR_PARAM,
		Gen2_NORESPONSE = ERR_CHIP_NORESP,
		Gen2_HEADER = ERR_CHIP_HEADER,
		Gen2_PREAMBLE = ERR_CHIP_PREAMBLE,
		Gen2_RXCOUNT = ERR_CHIP_RXCOUNT,
		Gen2_CRCERROR = ERR_CHIP_CRCERROR,
		Gen2_FIFO_ERROR = ERR_CHIP_FIFO,
		Gen2_TAG_UNREACHABLE = ERR_GEN2_SELECT,
		Gen2_WRONG_PASSW = ERR_GEN2_ACCESS,
		Gen2_REQRN_FAILED = ERR_GEN2_REQRN,
		Gen2_CHANNEL_TIMEOUT = ERR_GEN2_CHANNEL_TIMEOUT,
		/* Error codes returned from tag. It is the actual 8-bit code + 256. 
		   The following 5 from the 256 have names, the others not */
		Gen2_Tag_OTHER = 0x100 + 0,
		Gen2_Tag_MEM_OVERRUN = 0x100 + 0x3,
		Gen2_Tag_MEM_LOCKED = 0x100 + 0x4,
		Gen2_Tag_INSUFFICIENT_POWER = 0x100 + 0xa,
		Gen2_Tag_NON_SPECIFIC = 0x100 + 0xf,
	};


	Gen2ReaderInterface(){};
	virtual ~Gen2ReaderInterface(){};

	virtual Gen2Result readFromTag ( Gen2Tag &tag, quint8 mem_bank, quint32 address, ComByteArray passwd, quint8 count, bool bulkMode, ComByteArray &data ) = 0;
	/*!<@brief Pure virtual function that allows to read data from tag */
	virtual Gen2Result writeToTag ( Gen2Tag &tag, quint8 mem_bank, quint32 address, ComByteArray passwd, ComByteArray data, int *writtenBytes = 0 ) = 0;
	/*!<@brief Pure virtual function that allows to write data to tag */

	virtual Gen2Result writeTagId ( Gen2Tag &tag_Before, QString tagId_After, ComByteArray passwd, int *writtenBytes = 0 ) = 0;
	/*!<@brief Pure virtual function that allows to write the tag id */

	virtual Gen2Result lockTag ( Gen2Tag &tag, LOCK_MODE mode, LOCK_MEMORY_SPACE memory, ComByteArray passwd ) = 0;
	/*!<@brief Pure virtual function that allows to lock a tag*/
	virtual Gen2Result killTag ( Gen2Tag &tag, ComByteArray killPassword ) = 0;
	/*!<@brief Pure virtual function that allows to kill a tag */

	virtual QString errorToString( Gen2Result result) = 0;

    virtual Gen2Result genericCommand (Gen2Tag &tag, QByteArray command, QByteArray password, QByteArray &result_data, unsigned int &resultLength) = 0;
};

#endif /* GEN2READERINTERFACE_H_ */
