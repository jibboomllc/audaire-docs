/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 * InventoryRunner.h
 *
 *  Created on: 06.09.2011
 *      Author: Bernhard Breinbauer
 */

#ifndef INVENTORYRUNNER_H_
#define INVENTORYRUNNER_H_

#include "AmsReader.h"
#include "ProtocolObjects.h"

#include <QThread>
#include <QLinkedList>

class InventoryRunner : public QThread
{
	Q_OBJECT

public:
	InventoryRunner(AmsReader * reader);
	~InventoryRunner();

    void stopRunner();
    void setInventoryDelay(uint ms);
    uint inventoryDelay();
    bool isCyclicInventoryRunning();
    bool isSingleInventoryRunning();
    bool inventoryResult(QList<RawTagInfo> &result, quint32 &frequency_khz, Gen2ReaderInterface::Gen2Result &status);

signals:
    void inventoryResultAvailable();
    void singleInventoryDone();

public slots:
    void startSingleInventory(bool autoAck, bool fast, quint8 rssi, bool tid);
    void startCyclicInventory(bool autoAck, bool fast, quint8 rssi,bool tid);
    void stopCyclicInventory();

protected:
    virtual void run();

private:
    void singleInventory();
    void cyclicInventory();
    ReaderInterface::Result gen2SingleInventory(QList<RawTagInfo> &tags, quint32 &frequency_khz, Gen2ReaderInterface::Gen2Result &status);
    ReaderInterface::Result gen2CyclicInventory(QList<RawTagInfo> &tags, quint32 &frequency_khz, quint8 &status);

    AmsReader* m_reader;
    enum CyclicInventoryState { Start, Running, Stopping, Stop };
    CyclicInventoryState m_cyclicState;
    bool m_runnerRunning;
    bool m_singleInventoryRunning;
    uint m_inventoryDelay;
    QLinkedList<RawInvInfo> m_inventoryFifo;
    bool m_fastMode;
    quint8 m_rssiMode;
    bool m_autoAckMode;
    bool m_TidMode;
};


#endif /* INVENTORYRUNNER_H_ */
