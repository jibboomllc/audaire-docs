/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "AmsReader.h"
#include "InventoryRunner.h"

#include "../epc/EPC_Defines.h"
#include "AntennaObjects.h"
#include "RegisterObjects.h"
#include "ProtocolObjects.h"
#include "FrequencyObjects.h"
#include "TagObjects.h"
#include "ReaderObjects.h"
#include "AmsComObject.h"
#include "CtrlComObjects.h"
#include "AmsComStream.h"
#include "AmsFirmwareCheck.h"

#include <globals.h>
#include <QCoreApplication>
#include <QDebug>
#include <QMetaType>
#include <math.h>

const QStringList AmsReader::m_rssiChildNames = QStringList() << "RSSI Channel Q" << "RSSI Channel I";
QStringList AmsReader::theSupportedReaderList = QStringList() << "EVAL" << "FERMI" << "FEMTO" << "MEGA" << "RADON" << "NEWTON";

/*!
 * @brief 	Constructor of the AMS reader object
 */
AmsReader::AmsReader(AmsComDriver * driver, DriverType type, QObject* parent)
	: ReaderInterface(driver, type, parent)
	, QrfeTraceModule("AmsReader")
{
	// initialize variables
	m_notRespondingPauseCounter = 0;
    m_fwVersion = 0;
    m_currentInventoryGValue = 0.0;
    m_Gen2SelectWithTID = 0;

	/* Create cyclic inventory thread */
	m_runner = new InventoryRunner(this);

    m_rssiActive = true;

	m_tagType = TAG_GEN2_FAST;

    m_driverType = type;

	/* Start thread */
	m_runner->start(QThread::TimeCriticalPriority);
	while(!m_runner->isRunning());

    qRegisterMetaType<QList<RawTagInfo>>("QList<RawTagInfo>");
    connect(m_runner, SIGNAL(inventoryResultAvailable()), this, SLOT(inventoryResultFromRunner()));
    connect(m_runner, SIGNAL(singleInventoryDone()),      this, SLOT(runnerFinishedSingleInventory()));
}

/*!
 * @brief 	Desturctor of the AMS reader object
 */
AmsReader::~AmsReader()
{
	/* Stop runner thread */
	m_runner->stopRunner();

	/* Wait for thread finished */
	while(m_runner->isRunning());

	/* Delete thread */
	m_runner->deleteLater();

	/* Set state to offline */
	setState(STATE_OFFLINE);

}

QStringList AmsReader::supportedReaderList()
{
    return theSupportedReaderList;
}

/*!
 * @brief	Implementation of the virtual function to get the hardware description (containing version number)
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::getHardwareDescription ( QString &hardwareDescription )
{
    if (getReaderVersion())
    {
        hardwareDescription = m_hwInfo;
        return OK;
    } 
    else
    {
        hardwareDescription.clear();
        return ComError;
    }
}

/*!
 * @brief	Implementation of the virtual function to get the software description (containing version number)
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::getSoftwareDescription ( QString &softwareDescription )
{
    QString version;
    if (getReaderVersion())
    {
        AmsFirmwareCheck::convert(m_fwVersion, version);
        softwareDescription = m_fwInfo + " " + version;
        return OK;
    } 
    else
    {
        softwareDescription.clear();
        return ComError;
    }
}

/*!
 * @brief	Implementation of the virtual function to get the software version number
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::getSoftwareVersion( unsigned int &versionNumber )
{
    if (getReaderVersion())
    {
        versionNumber = m_fwVersion;
        return OK;
    } 
    else
    {
        versionNumber = 0;
        return ComError;
    }
}

/*!
 * @brief	Gets reader version information and stores it in member
 * @return	true on success, false on failure
 */
bool AmsReader::getReaderVersion()
{
    GetFirmwareInfoObject info;
    FirmwareNumberObject version;
    QStringList splitInfo;

    if (m_fwVersion != 0)       //info has been retrieved already
        return true;

    trc(0x06, "getting version information from reader");
    m_hwInfo.clear();
    m_fwInfo.clear();

    m_stream >> info >> AMS_FLUSH;
    if (m_stream.lastError() == 0)
    {
        splitInfo = QString((char*)info.get()).split("||");
        if(splitInfo.size() < 2)
            return false;

        m_fwInfo = splitInfo.at(0);
        m_hwInfo = splitInfo.at(1);
    }
    else
    {
        return false;
    }

    m_stream >> version >> AMS_FLUSH;
    if (m_stream.lastError() == 0)
    {
        m_fwVersion = version.get();
    } 
    else
    {
        return false;
    }
    return true;
}

/*!
 * @brief	Reads reader configuration from the device and updates the according members.
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::updateReaderState()
{
    ReaderConfig ro;
    quint8 initError;
    quint8 vco;
    quint8 pa;
    quint8 input;
    int chipId;
    quint8 hwId;

    m_stream >> ro >> AMS_FLUSH;
    if (m_stream.lastError() != 0)
    {
        setState(STATE_ERROR);
        return Error;
    } 

    ro.getData(m_wrongChipId, chipId, initError, vco, pa, input, m_antennaSwitch, m_tunerConfig, hwId );
    switch (initError)
    {
    case 0: m_initError = NoError;
    	break;
    case 1: m_initError = SpiError;
        break;
    case 2: m_initError = EnError;
        break;
    case 3: m_initError = IrqError;
        break;
    case 4: m_initError = CrystalError;
        break;
    case 5: m_initError = PllError;
        break;
    default: m_initError = UnknownError;
    }

    switch (vco)
    {
    case 0: m_vco = IntVco;
        break;
    case 1: m_vco = ExtVco;
        break;
    default: m_vco = VcoError;
        break;
    }

    switch (pa)
    {
    case 0: m_pa = IntPa;
        break;
    case 1: m_pa = ExtPa;
        break;
    default: m_pa = PaError;
        break;
    }

    switch (input)
    {
    case 0: m_input = Balanced;
        break;
    case 1: m_input = Single;
        break;
    default: m_input = InputError;
        break;
    }

    switch (chipId)
    {
    case AS3980: m_chipId = AS3980;
        break;
    case AS3993: m_chipId = AS3993;
        break;
    default: m_chipId = InvalidChip;
    }

    switch (hwId)
    {
    case Eval: m_hardwareId = Eval;
        break;
    case Fermi: m_hardwareId = Fermi;
        break;
    case FermiBig: m_hardwareId = FermiBig;
        break;
    case Femto1v0: m_hardwareId = Femto1v0;
        break;
    case Femto1v1: m_hardwareId = Femto1v1;
        break;
    case Femto2v0: m_hardwareId = Femto2v0;
        break;
    case Mega: m_hardwareId = Mega;
        break;
    case Radon: m_hardwareId = Radon;
        break;
    case Femto2v1: m_hardwareId = Femto2v1;
        break;
    case Newton: m_hardwareId = Newton;
        break;
    default: m_hardwareId = InvalidHardware;
    }

    if (m_wrongChipId || m_initError > NoError || m_vco >= VcoError || m_pa >= PaError || m_input >= InputError )
        setState(STATE_ERROR);
    else
        setState(STATE_ONLINE);

    return OK;
}

/*!
 * @brief	Implementation of the virtual function to reboot the reader
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::reboot ( )
{
	trc(0x06, "Trying to reset CPU");

	ResetObject reset(AMS_COM_RESET_MCU);
    m_stream << reset;

	setState(STATE_OFFLINE);

	return ReaderInterface::OK;
}


/*!
 * @brief   Implementation of the virtual function to enter bootloader of reader.
 * @return  Returns the result of the operation coded in an enum.
*/
ReaderInterface::Result AmsReader::enterBootloader()
{
    trc(0x06, "Trying to enter bootloader");

    EnterBootloaderObject eb;
    m_stream << eb;
    m_stream.flush();

    setState(STATE_OFFLINE);

    return ReaderInterface::OK;
}

/*!
 * @brief	Implementation of the virtual function to set the antenna power of the reader
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::setAntennaPower ( bool on, bool eval )
{
	QString status = (on)?"ON":"OFF";
	trc(0x06, "Trying to set antenna power " + status);

    AntennaPowerObject ao;
    ao.setData(on, eval);
    m_stream >> ao >> AMS_FLUSH;

	return checkStreamError();
}

/*!
 * @brief	Implementation of the virtual function to get a param of the reader
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::readRegisters( QMap<int, quint8> &value )
{
    trc(0x04, "Reading all registers from reader");
    ReadAllRegistersObject readAll;
    m_stream >> readAll >> AMS_FLUSH;

    value = readAll.get();
	return checkStreamError();
}

/*!
 * @brief	Implementation of the virtual function to get a param of the reader
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::readRegister ( quint8 address, quint8 &value )
{
    trc(0x04, QString("Reading register 0x") + QByteArray(1, address).toHex());
    ReadRegisterObject readReg;
    readReg.setAddress( address );
    m_stream >> readReg >> AMS_FLUSH;

    value = readReg.get();
    return checkStreamError();
}

/*!
 * @brief	Implementation of the virtual function to set a param of the reader
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::writeRegister ( quint8 address, quint8 value )
{
    trc(0x04, QString("Writing value 0x") + QByteArray(1, value).toHex() + QString(" to register 0x") + QByteArray(1, address).toHex());
    WriteRegisterObject writeRegs;
    if (!writeRegs.setData( address, value ))
        return Error;

    m_stream >> writeRegs >> AMS_FLUSH;
    return checkStreamError();
}

ReaderInterface::Result AmsReader::setGen2Config( int lf_khz, int coding, int session, bool longPilot, int tari, int qbegin, int target)
{
    trc(0x16, "Writing Gen2 configuration of reader");
    Gen2Parameters gen2;
    gen2.setLinkFreq(lf_khz);
    gen2.setCoding(coding);
    gen2.setSession(session);
    gen2.setLongPilot(longPilot);
    gen2.setTari(tari);
    gen2.setQbegin(qbegin);
    gen2.setTarget(target);
    m_stream >> gen2 >> AMS_FLUSH;

    return checkStreamError();
}


ReaderInterface::Result AmsReader::getGen2Config( int &lf_khz, int &coding, int &session, bool &longPilot, int &tari, int &qbegin, int &target )
{
    trc(0x16, "Reading Gen2 configuration of reader");
    Gen2Parameters gen2;
    m_stream >> gen2 >> AMS_FLUSH;

    lf_khz = gen2.linkFreq();
    coding = gen2.coding();
    session = gen2.session();
    longPilot = gen2.longPilot();
    tari = gen2.tari();
    qbegin = gen2.qbegin();
    target = gen2.target();
    return checkStreamError();
}

ReaderInterface::Result AmsReader::getTuner( quint8 id, quint8 &cin, quint8 &clen,quint8 &cout )
{
    trc(0x06, "Reading tuner values from reader");
    AntennaTuner getTuner;
    getTuner.setID(id);

    m_stream >> getTuner >> AMS_FLUSH;
    getTuner.getData(cin, clen, cout);
    return checkStreamError();
}

ReaderInterface::Result AmsReader::setTuner( quint8 id, quint8 cin, quint8 clen, quint8 cout )
{
    trc(0x06, "Writing tuner values to reader");
    AntennaTuner setTuner;

    setTuner.setID(id);
    setTuner.setCin(cin);
    setTuner.setClen(clen);
    setTuner.setCout(cout);

    m_stream >> setTuner >> AMS_FLUSH;
    return checkStreamError();
}

ReaderInterface::Result AmsReader::startAutoTuner( quint8 autoTune, quint8 &cin, quint8 &clen, quint8 &cout )
{
    trc(0x06, "Start auto tuning cycle");
    AntennaTuner getTuner;
    AutoTuner algoTune;
    ReaderInterface::Result res;

    algoTune.setAutoTune(autoTune);
    m_stream << algoTune << AMS_FLUSH;
    res = checkStreamError();
    if (res != ReaderInterface::OK)
        return res;

    if (autoTune == 1)
        SleepThread::msleep(1000);
    else
        SleepThread::msleep(3000);

    getTuner.setID(autoTune >> 4);
    m_stream >> getTuner >> AMS_FLUSH;
    getTuner.getData(cin, clen, cout);
    //qDebug() << "cin: " << cin << " clen: " << clen << " cout: " << cout;

    return checkStreamError();
}


ReaderInterface::Result AmsReader::getMaxTunerTableSize( quint8 &tableSize )
{
    trc(0x06, "Get max tuner table size from reader");
    MaxTunerTableSize tts;

    m_stream >> tts >> AMS_FLUSH;
    tableSize = tts.tunerTableSize();
    return checkStreamError();
}

ReaderInterface::Result AmsReader::clearTunerTable()
{
    trc(0x06, "Clearing tuner table on reader");
    ClearTunerTable ctt;

    m_stream >> ctt >> AMS_FLUSH;
    return checkStreamError();
}

ReaderInterface::Result AmsReader::addTunerTableEntry( ulong frequencyKHz, bool tuneEnable1, quint8 cin1, quint8 clen1, quint8 cout1, quint16 iq1, 
                                                                           bool tuneEnable2, quint8 cin2, quint8 clen2, quint8 cout2, quint16 iq2 )
{
    trc(0x06, "Add entry to tuning table on reader");
    AddTunerTableEntry att;
    ReaderInterface::Result res;

    att.setFreq(frequencyKHz);
    if (tuneEnable1)
        att.setTuningAntenna1(cin1, clen1, cout1, iq1);
    if (tuneEnable2)
        att.setTuningAntenna2(cin2, clen2, cout2, iq2);

    m_stream >> att >> AMS_FLUSH;
    res = checkStreamError();
    if (res != ReaderInterface::OK)
        return res;
    if (att.status() == ERR_NOMEM)
        return ReaderInterface::NA;
    return ReaderInterface::OK;
}

ReaderInterface::Result AmsReader::setGetTxRxParams( signed char &sensitivity, bool set_sens, unsigned char &antenna, bool set_a )
{
    trc(0x06, "Reading/Writing Tx/Rx parameters");
    TxParameters txp;
    if (set_sens)
        txp.setSensitivity(sensitivity);
    if (set_a)
        txp.setAntenna(antenna);

    m_stream >> txp >> AMS_FLUSH;

    antenna = txp.getAntenna();
    sensitivity = txp.getSensitivity();

    return checkStreamError();
}

/*!
 * @brief	Implementation of the virtual function to do a single inventory
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::startSingleInventory ( )
{
    trc(0x01, "Start single inventory");
    m_currentInventoryGValue = getG_general();
    bool fastMode = false;
    if (m_tagType == TAG_GEN2_FAST)
        fastMode = true;
    bool tid = false;
    if (m_tagType == TAG_GEN2_TID)
        tid = true;
    setAction(ReaderInterface::ACTION_SCANNING);

    m_runner->startSingleInventory(m_autoAckMode, fastMode, m_rssiMode, tid);

	return ReaderInterface::OK;
}

/*!
 * @brief	Implementation of the virtual function to start a cyclic inventory
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::startCyclicInventory ( )
{
    trc(0x01, "Start cyclic inventory");
    m_currentInventoryGValue = getG_general();
    bool fastMode = false;
    if (m_tagType == TAG_GEN2_FAST)
        fastMode = true;
    bool tid = false;
    if (m_tagType == TAG_GEN2_TID)
        tid = true;
    /* If is already running skip */
	if(m_runner->isCyclicInventoryRunning())
		return ReaderInterface::OK;

	setAction(ReaderInterface::ACTION_SCANNING);

	m_runner->startCyclicInventory(m_autoAckMode, fastMode, m_rssiMode, tid);

	return ReaderInterface::OK;
}

/*!
 * @brief	Implementation of the virtual function to stop the cyclic inventory
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::stopCyclicInventory ( )
{
    trc(0x01, "Stop cyclic inventory");
    int count = 0;
	/* Set variable to stop and wait until finished */
	m_runner->stopCyclicInventory();

	while(m_runner->isCyclicInventoryRunning())
    {
		SleepThread::mwait(5);
        count++;
        if (count > 100)
            return ReaderInterface::ComError;
    }
    setAction(ReaderInterface::ACTION_IDLE);

	return ReaderInterface::OK;
}


/*!
 * @brief 	Implementation of the select tag command
 * @param	epc		The epc of the tag that should be selected
 * @return	Returns the result of the operation coded in an enum
 */
Gen2ReaderInterface::Gen2Result AmsReader::selectTag( Gen2Tag &tag )
{
	trc(0x1C, "Trying to select the tag " + tag.epc);
    Gen2ReaderInterface::Gen2Result res = Gen2ReaderInterface::Gen2_OK;
	bool ok = false;
	ComByteArray epcBytes = stringToEpc(tag.epc, &ok);
    ComByteArray tidBytes = stringToEpc(tag.tid, &ok);
    /* AS3980/81 does not support Select command */
    if (getChipId() == AS3980)
        return Gen2_OK;
    // Select EPC
	// build up the command byte array and send it
    if ((m_Gen2SelectWithTID == 0 ) || (m_Gen2SelectWithTID == 2 ))
    {
	    TagSelect selCmd(TagSelect::Clear_and_add);
        selCmd.setEpc(epcBytes);
        m_stream >> selCmd >> AMS_FLUSH;
        res = selCmd.getGen2Result();
    }
    if (res !=Gen2ReaderInterface::Gen2_OK)
    {
        return res;
    }
    // Select TID
    // build up the command byte array and send it
    if (((m_Gen2SelectWithTID == 1) || (m_Gen2SelectWithTID == 2)) && (tidBytes.length()<=2))
    {

        return Gen2ReaderInterface::Gen2_TAG_UNREACHABLE;
    }
    if  (m_Gen2SelectWithTID == 1 )
    {
        TagSelect selCmd(TagSelect::Clear_and_add);
        selCmd.setTID(tidBytes);
        m_stream >> selCmd >> AMS_FLUSH;
        res = selCmd.getGen2Result();
    }
    if  (m_Gen2SelectWithTID == 2 )
    {
        TagSelect selCmd(TagSelect::Add);
        selCmd.setTID(tidBytes);
        m_stream >> selCmd >> AMS_FLUSH;
        res = selCmd.getGen2Result();
    }

	return res;
}

/*!
 * @brief	Implementation of the virtual function to read data from a tag
 * @return	Returns the result of the operation coded in an enum
 */
Gen2ReaderInterface::Gen2Result AmsReader::readFromTag( Gen2Tag &tag, quint8 mem_bank, quint32 address, ComByteArray passwd, quint8 count, bool bulkMode, ComByteArray &data )
{
	Gen2ReaderInterface::Gen2Result gen2Result;
	ComByteArray result_data;

	data.clear();
	/* Select tag */
	gen2Result = selectTag(tag);
	if(gen2Result != Gen2ReaderInterface::Gen2_OK){
		return Gen2_TAG_UNREACHABLE;
	}

    trc(0x1C, "Trying to read from tag " + tag.epc);
	do 
	{
		/* Read from tag */
		TagRead readCmd;
		readCmd.setMemoryAddress(mem_bank, address, count, bulkMode);
        readCmd.setPassword(passwd);
		m_stream >> readCmd >> AMS_FLUSH;

        if(m_stream.lastError() == NoError)
        {
            gen2Result = readCmd.getGen2Result();
            readCmd.getData(result_data);
		    data.append(result_data);
		    address += data.size()/2;
        }
        else
            gen2Result = Gen2_TIMEOUT;
	} while (gen2Result == Gen2ReaderInterface::Gen2_OK && count == 0);
	
	return gen2Result;
}

/*!
 * @brief	Implementation of the virtual function to write data to a tag
 * @return	Returns the result of the operation coded in an enum
 */
Gen2ReaderInterface::Gen2Result AmsReader::writeToTag( Gen2Tag &tag, quint8 mem_bank, quint32 address, ComByteArray passwd, ComByteArray data, int *writtenBytes/*=0 */ )
{
	int wb = 0;
	int to_write = data.size();
	Gen2ReaderInterface::Gen2Result gen2Result, lastError = Gen2ReaderInterface::Gen2_OK;

    /* Select tag */
    gen2Result = selectTag(tag);
    if(gen2Result != Gen2ReaderInterface::Gen2_OK){
        return Gen2_TAG_UNREACHABLE;
    }

    trc(0x1C, "Trying to write to tag " + tag.epc);
	/* Write to tag in maximum byte count of 14 byte */
	quint32 tempAddr = address;
	int tries = 0;
    int maxtries = data.size()/8+2+data.size()/24;
    int tmpWb;
	while(data.size() > wb && tries++ < maxtries)
	{
		ComByteArray ba = data.mid(wb,qMax(16,data.size()-wb));

        TagWrite writer;
        writer.setMemoryAddress(mem_bank,tempAddr);
        writer.setPassword(passwd);
        writer.setData(ba);
        m_stream >> writer >> AMS_FLUSH;
        if(m_stream.lastError() != NoError)
            return Gen2_TIMEOUT;
        tmpWb = writer.getWordsWritten() * 2;
		gen2Result = writer.getGen2Result();
        tempAddr += tmpWb/2;
        wb += tmpWb;

		if (gen2Result) lastError = gen2Result;

		trc(0x1C,QString("Wrote at %1 %2 bytes.").arg(tempAddr).arg(tmpWb));
	}
	if ( writtenBytes != NULL) *writtenBytes = wb;
	if ( wb != to_write )
	{
		gen2Result = lastError;
	}
    return gen2Result;
}

/*!
 * @brief	Implementation of the virtual function to set the epc of a tag
 * @return	Returns the result of the operation coded in an enum
 */
Gen2ReaderInterface::Gen2Result AmsReader::writeTagId ( Gen2Tag &tag_Before, QString tagId_After, ComByteArray passwd, int *writtenBytes )
{
	trc(0x18, "Trying to set tag id from " + tag_Before.epc + " to " + tagId_After + ".");

	ComByteArray epc = stringToEpc(tagId_After);

	return writeToTag(tag_Before, MEM_EPC, MEMADR_EPC, passwd, epc, writtenBytes);
}

/*!
 * @brief	Implementation of the virtual function to lock a tag
 * @return	Returns the result of the operation coded in an enum
 */
Gen2ReaderInterface::Gen2Result AmsReader::lockTag ( Gen2Tag &tag, LOCK_MODE mode, LOCK_MEMORY_SPACE memory, ComByteArray passwd )
{
    Gen2ReaderInterface::Gen2Result gen2Result;
	
	/* Select tag */
	gen2Result = selectTag(tag);
	if(gen2Result != Gen2ReaderInterface::Gen2_OK){
		return gen2Result;
	}

    trc(0x1C, "Trying to lock tag " + tag.epc);
	/* Lock the tag */
    TagLock locker;
    locker.setMemBankLock(static_cast<TagLock::MemBank>(memory), static_cast<TagLock::Protection>( mode));
    locker.setPassword(passwd);
    m_stream >> locker >> AMS_FLUSH;
    if(m_stream.lastError() != NoError)
        return Gen2_TIMEOUT;

    gen2Result = locker.getGen2Result();
	return gen2Result;
}

/*!
 * @brief	Implementation of the virtual function to kill a tag
 * @return	Returns the result of the operation coded in an enum
 */
Gen2ReaderInterface::Gen2Result AmsReader::killTag ( Gen2Tag &tag, ComByteArray killPassword )
{
    Gen2ReaderInterface::Gen2Result gen2Result;

    /* Select tag */
    gen2Result = selectTag(tag);
    if(gen2Result != Gen2ReaderInterface::Gen2_OK){
        return gen2Result;
    }

    trc(0x1C, "Trying to kill tag " + tag.epc);
    /* Kill the tag */
    TagKill killer;
    killer.setPassword(killPassword);
    killer.setRecomRfu(0);
    m_stream >> killer >> AMS_FLUSH;
    if(m_stream.lastError() != NoError)
        return Gen2_TIMEOUT;

    gen2Result = killer.getGen2Result();
    return gen2Result;
}

QList<struct rssi_val> AmsReader::rssiCommand( unsigned char measurements, ulong freq_kHz )
{
    RssiObject rssi(measurements);

    rssi.setFreq(freq_kHz);

    m_stream >> rssi >> AMS_FLUSH;

    return rssi.getRssis();
}
/*!
 * @brief	Function to get the G value for reflected power according app note tbd.
 * @return	Returns the G value for subsequent usage.
 */
double AmsReader::getG_general(void)
{
	double G = 0.0;
	quint8 reg0a, reg09, reg0d;
    QString hw;
    bool error = false;

    if (!(m_chipId == AS3980 || m_chipId == AS3993))
        return 0.0;
    if (getHardwareDescription(hw) != ReaderInterface::NoError)
        error = true;
    if (readRegister(0x0A, reg0a) != ReaderInterface::NoError)
        error = true;
    if (readRegister(0x09, reg09) != ReaderInterface::NoError)
        error = true;
    if (readRegister(0x0d, reg0d) != ReaderInterface::NoError)
        error = true;
	if (error)
		return 0.0;

    // rx gain setting
    int gain = ((reg0a >> 6) & 0x03) * 3;
    if ((reg0a & 0x20) == 0x00)
        gain *= (-1);

    if (getHardwareId() == Fermi)
    {   // balanced input
        switch (reg0a & 0x03)
        {
        case 0x00:
            break;
        case 0x01:
            gain -= 8;
            break;
        case 0x02:
            gain += 10;
            break;
        default:
            return 0.0;
        }
        // get G for mixer setting (BLF setting)
        switch (reg09)
        {
        case 0xFF:      //BLF 40kHz
            G=60;
            break;
        case 0x3F:      //BLF 160kHz
        case 0xBF:
            G=61;
            break;
        case 0x34:      //BLF 250kHz
        case 0x37:
            G=63;
            break;
        case 0x24:      //BLF 320kHz
        case 0x27:
            G=62;
            break;
        case 0x04:      //BLF 640kHz
        case 0x07:
            G=64;
            break;
        default:
            return 0.0; /* error: should not be used */
        }
        // according to measurements we have a gradient of 1.0
        G = G + gain*1.0;
    }
    else /* if (getHardwareId() == Femto1v1) */
    {
        // FIXME: no measurement data yet, using data of Fermi atm.
        if (reg0d & 0x04)
        { /* single ended input */
            switch (reg0a & 0x03)
            {
            case 0x00:
                gain -= 6;
                break;
            case 0x01:
                /* nominal */
                break;
            case 0x03:
                gain += 6;
                break;
            default:
                return 0.0;
            }
        }
        else
        { /* differential input */
            switch (reg0a & 0x03)
            {
            case 0x00:
                break;
            case 0x01:
                gain -= 8;
                break;
            case 0x02:
                gain += 10;
                break;
            default:
                return 0.0;
            }
        }
        // get G for mixer setting (BLF setting)
        switch (reg09)
        {
        case 0xFF:      //BLF 40kHz
            G=60;
            break;
        case 0x3F:      //BLF 160kHz
        case 0xBF:
            G=61;
            break;
        case 0x34:      //BLF 250kHz
        case 0x37:
            G=63;
            break;
        case 0x24:      //BLF 320kHz
        case 0x27:
            G=62;
            break;
        case 0x04:      //BLF 640kHz
        case 0x07:
            G=64;
            break;
        default:
            return 0.0; /* error: should not be used */
        }
        // according to measurements we have a gradient of 1.0
        G = G + gain*1.0;
    }

    return G;
}

/*!
 * @brief	Function to get the G value for reflected power
 * @return	Returns the G value for subsequent usage.
 */
double AmsReader::getG_rfp(void)
{
	double G = 0.0;
	quint8 reg04, reg0a, reg22;
    QString hw;
    bool error = false;

    if (!(m_chipId == AS3980 || m_chipId == AS3993))
        return 0.0;
    if (getHardwareDescription(hw) != ReaderInterface::NoError)
        error = true;
    if (readRegister(0x04, reg04) != ReaderInterface::NoError)
        error = true;
    if (readRegister(0x0A, reg0a) != ReaderInterface::NoError)
        error = true;
    if (readRegister(0x22, reg22) != ReaderInterface::NoError)
        error = true;
    if (error)
		return 0.0;

    reg0a &= 0x03;// take for comparison only the last bits 
    reg04 &= 0xF0;// take only the first bits for comparison

    if (getHardwareId() == Fermi)
    {
        if ( (reg0a == 0x01 ) && (reg04 == 0x00) )
        {
            G = 16;
        }
        else if ( (reg0a == 0x00 ) && (reg04 == 0x00) )
        {
            G = 37;
        }
        else if ( (reg0a == 0x02 ) && (reg04 == 0x00) ) 
        {
            G = 110;
        }
        else if ( (reg0a == 0x02 ) && (reg04 == 0x40) ) 
        {
            G = 280;
        }
        else
        {
            G = 1000000;
        }
    }
    else if (getHardwareId() == Radon)
    {
        if ( (reg0a == 0x01 ) && (reg04 == 0x00) )
        {
            G = 18;
        }
        else if ( (reg0a == 0x00 ) && (reg04 == 0x00) )
        {
            G = 41;
        }
        else if ( (reg0a == 0x02 ) && (reg04 == 0x00) ) 
        {
            G = 133;
        }
        else if ( (reg0a == 0x02 ) && (reg04 == 0x40) ) 
        {
            G = 310;
        }
        else
        {
            G = 1000000;
        }
    }
    else if ((getHardwareId() == Femto1v0) || (getHardwareId() == Femto1v1) || (getHardwareId() == Femto2v0) || (getHardwareId() == Femto2v1) )
    {
        if ( (reg0a == 0x00 ) && (reg22 == 0x17) )
        {
            G = 22;
        }
        else if ( (reg0a == 0x01 ) && (reg22 == 0x14) )
        {
            G = 43;
        }
        else if ( (reg0a == 0x03 ) && (reg22 == 0x12) ) 
        {
            G = 80;
        }
        else if ( (reg0a == 0x03 ) && (reg22 == 0x00) ) 
        {
            G = 160;
        }
        else
        {
            G = 1000000;
        }
    }
    else // take Femto Values in other cases
    {
        G = 1000000;
        if ( (reg0a == 0x00 ) && (reg22 == 0x17) )
        {
            G = 22;
        }
        else if ( (reg0a == 0x01 ) && (reg22 == 0x14) )
        {
            G = 43;
        }
        else if ( (reg0a == 0x03 ) && (reg22 == 0x12) ) 
        {
            G = 80;
        }
        else if ( (reg0a == 0x03 ) && (reg22 == 0x00) ) 
        {
            G = 160;
        }
        else
        {
            G = 1000000;
        }
    }
    return G;
}

/*!
 * @brief	Function to send continuously a modulation into air
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::continuousSend( ulong freqKHz, ushort timeout_ms, uchar random )
{
    trc(0x0C, "Starting continuous modulation");
    ContinuousModulationCom con(freqKHz, timeout_ms, random);
    m_stream << con;
    m_stream.flush();
    return checkStreamError();
}


/*!
 * @brief	Function to get the configured cyclic inventory interval
 * @returns	Returns the configured cyclic inventory interval
 */
quint32 AmsReader::intervalTime()
{
	return m_runner->inventoryDelay();
}

/*!
 * @brief	Function to set the cyclic inventory interval
 * @param	time	The cyclic inventory interval
 */
void AmsReader::setIntervalTime(quint32 ms)
{
	m_runner->setInventoryDelay(ms);
}


/*!
 * @brief	Slot that is called if the used device was removed
 */
void AmsReader::deviceWasRemoved()
{
	m_runner->stopCyclicInventory();
	ReaderInterface::deviceWasRemoved();
}


/*!
 * @brief	Function to return the pointer to this gen2 reader
 * @return 	Returns the pointer to this gen2 reader.
 */
Gen2ReaderInterface* AmsReader::getGen2Reader()
{
	return dynamic_cast<Gen2ReaderInterface*>(this);
}

struct Rssi AmsReader::interpretRSSI(quint8 iq)
{
    struct Rssi rssi;
	uchar q = (iq & 0xF0) >> 4;
	uchar i = (iq & 0x0F);
	
    /* The following formula is taken from the proper RSSI related application notes.
     * meanRssi = (qRSSI + iRSSI) / 2;
     * Pin = 2.1 * meanRSSI - G;
     * As the maximum of meanRSSI is 15 and 2.1 * 15 = 31.5 the range of Pin is 
     * therefore between -G and -G+31.5
     */

    double meanRSSI = ((double)(i + q))/2.0;
    rssi.pin = 2.1 * meanRSSI - m_currentInventoryGValue;
    rssi.pinMin = 0 - m_currentInventoryGValue;
    rssi.pinMax = 31.5 - m_currentInventoryGValue;
    rssi.i = i;
    rssi.q = q;
    //qDebug() << QString("Pin: %1, q:%2, i:%3, G:%4").arg(rssi.pin).arg(q).arg(i).arg(m_currentInventoryGValue);

	return rssi;
}

/*!
 * @brief   Function to deliver names of rssi entries, eg: I and Q channel
 */
QStringList AmsReader::getRssiChildren()
{
	return m_rssiChildNames;
}

/** gets info from inventory runner, translates it and emits signal */
void AmsReader::inventoryResultFromRunner()
{
    QList <RawTagInfo> rawList;
    QList<InventoryTagInfo> tagList;
    quint32 freq;
    Gen2ReaderInterface::Gen2Result status;

    if (!m_runner->inventoryResult(rawList, freq, status))
        return;
    foreach( RawTagInfo raw, rawList )
    {
        InventoryTagInfo tag;
        tag.freq = raw.freq;
        tag.epc = epcToString(raw.epc);
        tag.tid = epcToString(raw.tid);
        if ((raw.agc & 0x80) != 0)
            tag.subCarrierInPhase = true;
        else
            tag.subCarrierInPhase = false;
        tag.agc = ((raw.agc >> 4) & 0x07) * 3;
        if ((raw.agc & 0x08) != 0)
            tag.qChannel = true;
        else
            tag.qChannel = false;
        //tag.rssi = interpretRSSItoPercent(raw.rssi);
        tag.rssi = interpretRSSI(raw.rssi);
        tagList.append(tag);
        trc(0x08, QString("Got tag from reader >> %1 << frequency:%2, epc: %3, rssi: %4").arg(m_readerID).arg(tag.freq).arg(tag.epc).arg(raw.rssi));
    }
    emit inventoryResult(m_readerID, tagList, freq, status);
}

void AmsReader::runnerFinishedSingleInventory()
{
    trc(0x01, QString("Single inventory finished on reader >> %1 <<").arg(m_readerID));
    emit inventoryRoundDone(m_readerID);
}

/** adds frequency to the frequency list on the reader. If clear is set the existing frequency list will be cleared before adding
 * the new frequency.
 * The rssi threshold and profile values are global values in the FW and not stored for every frequency.
 */
ReaderInterface::Result AmsReader::addFrequency( bool clear, ulong freqKHz, uchar profile )
{
    trc(0x06, "Add frequency to hopping list on reader");
    AddFrequencyCom af;
    af.setFreqParameters(clear, freqKHz, profile);
    m_stream >> af >> AMS_FLUSH;
    if (checkStreamError() == OK && af.success())
        return OK;
    else
        return Error;
}

/** retrieves information about the currently active frequency list from the reader */
ReaderInterface::Result AmsReader::getFrequencies( uchar &profile, ulong &minFreqKHz, ulong &maxFreqKHz, uchar &numFreq, uchar &numFreqGui )
{
    trc(0x06, "Get frequencies from hopping list on reader");
    GetFrequencyCom gf;
    m_stream >> gf >> AMS_FLUSH;
    gf.freqData(minFreqKHz, maxFreqKHz, numFreq, numFreqGui);
    profile = gf.profile();
    return checkStreamError();
}

/** performs a rssi measurement on the given frequency and returns the result in i, q, and dBm */
ReaderInterface::Result AmsReader::getRssi( ulong freqKHz, int &i, int &q, int &dBm )
{
    trc(0x05, "Measure rssi value");
    RssiCom rc(freqKHz);
    m_stream >> rc >> AMS_FLUSH;
    rc.rssiData(i, q, dBm);
    return checkStreamError();
}

/** performs a reflected power measurement on the given frequency and returns the result in i and q. */
ReaderInterface::Result AmsReader::getReflectedPower( ulong freqKHz, bool set_tune, int &i, int &q )
{
    trc(0x05, "Measure reflected power value");
    ReaderInterface::Result res;
    ReflectedPowerCom rf(freqKHz, set_tune);
    m_stream >> rf >> AMS_FLUSH;
    rf.reflectedPower(i, q);
    res = checkStreamError();
    //qDebug() << "reader: i= " << i << " q= " << q << " res= " << res;
    return res;
}

/** set frequency hopping related parameters */
ReaderInterface::Result AmsReader::setHoppingParams( ushort listenTime_ms, ushort allocationTime_ms, ushort idleTime_ms, signed char rssi )
{
    trc(0x06, "Set hopping parameters");
    SetFreqHoppingCom fh;
    fh.setFreqParameters(listenTime_ms, allocationTime_ms, idleTime_ms, rssi);
    m_stream >> fh >> AMS_FLUSH;
    if (checkStreamError() == OK && fh.success())
        return OK;
    else
        return Error;
}

/** get frequency hopping related parameters */
ReaderInterface::Result AmsReader::getHoppingParams( ushort &listenTime_ms, ushort &allocationTime_ms, ushort &idleTime_ms, signed char &rssi )
{
    trc(0x06, "Get hopping parameters");
    GetFreqHoppingCom gh;
    m_stream >> gh >> AMS_FLUSH;
    gh.freqParameters(listenTime_ms, allocationTime_ms, idleTime_ms, rssi);
    return checkStreamError();
}

QString AmsReader::errorToString( Gen2Result result)
{
	QString t;
	switch (result)
	{
	case Gen2_OK                    : t = "O.k.";                                                 break;
	case Gen2_NOMEM                 : t = "Not enough memory";                                    break;
	case Gen2_BUSY                  : t = "Device is busy";                                       break;
	case Gen2_IO                    : t = "I/O error";                                            break;
	case Gen2_TIMEOUT               : t = "Timeout error";                                        break;
	case Gen2_ERR_REQEUEST          : t = "Invalid request";                                      break;
	case Gen2_ERR_NOMSG             : t = "No message of this type";                              break;
	case Gen2_ERR_PARAM             : t = "Wrong parameters";                                     break;
	case Gen2_NORESPONSE            : t = "No response from tag";                                 break;
	case Gen2_HEADER                : t = "Header bit was set in response";                       break;
	case Gen2_PREAMBLE              : t = "Preamble error in response";                           break;
	case Gen2_RXCOUNT               : t = "Invalid size of response";                             break;
	case Gen2_CRCERROR              : t = "CRC error";                                            break;
	case Gen2_FIFO_ERROR            : t = "FIFO under/overflow";                                  break;
	case Gen2_TAG_UNREACHABLE       : t = "Tag is not in the range of the reader";                break;
	case Gen2_WRONG_PASSW           : t = "Access failed, probably wrong password";               break;
	case Gen2_REQRN_FAILED          : t = "ReqRN failed.";                                        break;
	case Gen2_CHANNEL_TIMEOUT       : t = "Channel timed out, command took too long.";            break;
	case Gen2_Tag_MEM_OVERRUN       : t = "Memory overrun";                                       break;
	case Gen2_Tag_MEM_LOCKED        : t = "Memory is locked, you must specify a access password"; break;
	case Gen2_Tag_INSUFFICIENT_POWER: t = "Tag has insufficient power";                           break;
	case Gen2_Tag_NON_SPECIFIC      : t = "Tag returned the non-specific error.";                 break;
	case Gen2_Tag_OTHER             : t = "Tag returned the so called \"other\" error.";          break;
	}

	if (t.isEmpty() && (result & Gen2_Tag_OTHER))
	{
		t = QString("Tag returned the unknown error %1").arg(result & 0xff,2,16);
	}

	if (t.isEmpty())
	{
		t = QString("Unknown error: 0x%1").arg(result,0,16);
	}
	return t;
}

ReaderInterface::Result AmsReader::setPowerDownMode( quint8 mode )
{
    trc(0x06, QString("Change power down mode on reader >> %1 << to:%2").arg(m_readerID).arg(mode));
    ReaderConfig rc;
    ReaderInterface::Result res;
    rc.setPowerDownMode(mode);
    m_stream >> rc >> AMS_FLUSH;
    res = checkStreamError();
    if (res != ReaderInterface::OK)
        return res;
    if (rc.status() == ERR_PARAM)   //invalid power down mode
        return ReaderInterface::NA;
    return res;
}

ReaderInterface::Result AmsReader::getPowerDownMode( quint8 &mode )
{
    trc(0x06, QString("Trying to get power down mode from reader >> %1 <<").arg(m_readerID));
    ReaderConfig rc;
    m_stream >> rc >> AMS_FLUSH;
    mode = rc.powerDownMode();
    return checkStreamError();
}

bool AmsReader::isAutoAckSupported()
{
    quint8 version;
    if (readRegister(0x33, version) != ReaderInterface::OK)
        return false;

    if (version == 0x60)        //autoACK supported in >=0x61
        return false;
    return true;
}

Gen2ReaderInterface::Gen2Result AmsReader::TxRxNBytes ( const QByteArray &tx, QByteArray &rx, unsigned int &rxlength )
{
    Gen2ReaderInterface::Gen2Result gen2res = Gen2ReaderInterface::Gen2_OK;

    QByteArray receivedBytes;
    GenericCMDObject obj(AS3993_CMD_GENERIC,tx.size(), rx.size());
    //qDebug() <<"Transmitted Bytes"<< tx.toHex();
    obj.set(tx);
    m_stream >> obj;
    m_stream << AMS_FLUSH;

    gen2res = obj.getGen2Result();

    receivedBytes = obj.get();
    qDebug() << receivedBytes.toHex();

    rxlength = (receivedBytes[1] & 0xFF);
    rx = receivedBytes.mid(2, rx.size() - 2);
    qDebug() <<"Received Bytes"<< rx.toHex();

    return gen2res;
}

Gen2ReaderInterface::Gen2Result AmsReader::genericCommand (Gen2Tag &tag, QByteArray genericCommand, QByteArray password, QByteArray &rxBuffer, unsigned int &rxLength)
{
    Gen2ReaderInterface::Gen2Result gen2res = Gen2ReaderInterface::Gen2_OK;
    trc(0x0C, "Trying to set Generic Command");


    gen2res = selectTag(tag);
    if(gen2res != Gen2ReaderInterface::Gen2_OK)
        return Gen2ReaderInterface::Gen2_TAG_UNREACHABLE;
    
    if(password.size() != 4)
        return Gen2ReaderInterface::Gen2_WRONG_PASSW;

    // build up the command byte array
    QByteArray buf;
    buf.append(password);
    buf.append(genericCommand);

    if (buf.size() > AMS_STREAM_MAX_DATA_SIZE)
        return Gen2ReaderInterface::Gen2_Tag_OTHER;

    gen2res = TxRxNBytes(buf, rxBuffer, rxLength);

    return gen2res;
}

/*!
 * @brief	Function to change PA Settings
 * @param   readwrite read == false, write == true
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result AmsReader::changePASetting( quint8 &g8, quint8 &g16, bool readwrite )
{
    trc(0x06, "Change PA Settings");
    PAConfig obj(readwrite);
    obj.setData(g8, g16);

    m_stream >> obj >> AMS_FLUSH;
    ReaderInterface::Result err = static_cast<ReaderInterface::Result>(obj.status());

    if (!readwrite)
    {
       g8 = obj.getG8();
       g16 = obj.getG16();
    }

    //    qDebug() << err;
    return err;
}

void AmsReader::setGen2Select(int option)
{
    m_Gen2SelectWithTID= option;
}
