/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "InventoryRunner.h"
#include "TagObjects.h"

/*!
 * @brief Constructor of the thread that initiates cyclic the inventory procedure
 */
InventoryRunner::InventoryRunner(AmsReader* parent)
	: QThread(parent)
	, m_reader(parent)
    , m_runnerRunning( true )
    , m_singleInventoryRunning( false )
    , m_cyclicState( Stop )
{
    m_inventoryDelay = 10;      // default to 10ms
    m_inventoryFifo.clear();

    m_fastMode = false;
    m_rssiMode = false;
    m_autoAckMode = false;
    m_TidMode = false;
}

/*!
 * @brief Destructor of the thread
 */
InventoryRunner::~InventoryRunner()
{
}

/*!
 * @brief Thread main function calls cyclic inventory procedure
 */
void InventoryRunner::run()
{
	while(m_runnerRunning)
	{
		cyclicInventory();
        singleInventory();
        msleep(1);
	}
}

void InventoryRunner::singleInventory( )
{
    QList<RawTagInfo> tags;
    ReaderInterface::Result res;
    quint32 freq;
    Gen2ReaderInterface::Gen2Result gen2Res;

    if (!m_singleInventoryRunning)
        return;

    m_singleInventoryRunning = false;

    if(m_reader->tagType() == ReaderInterface::TAG_GEN2 || m_reader->tagType() == ReaderInterface::TAG_GEN2_FAST  || m_reader->tagType() == ReaderInterface::TAG_GEN2_TID)
    {
        res = gen2SingleInventory(tags, freq, gen2Res);
        emit singleInventoryDone();
        if (res == ReaderInterface::OK)
        {
            RawInvInfo rii;
            rii.tags = tags;
            rii.freq = freq;
            rii.result = gen2Res;
            m_inventoryFifo.append(rii);
            emit inventoryResultAvailable();
        }
    }
    //TODO: ISO 6B not yet supported
    //else if(m_tagType == TAG_ISO6B)
    //	res = m_ph->doISO6BSingleInventory(epcs);

    //handleProtocolHandlerError(res);

    //   /* If everything is ok, handle the result and emit the found tags */
    //   if(res == QrfeProtocolHandler::OK)
    //   {
    //       trc(9, "Found " +  QString::number(epcs.size()) + " tags: " + epcs.join(" "));

    //       /* Emit the results */
    //       foreach(QString epc, epcs){
    //           emit cyclicInventory(m_readerID, epc);
    //           if(m_rssiActive)
    //               emit cyclicRSSIInformation(m_readerID, epc, interpretRSSItoPercent(rssi.value(epc)));
    //       }
    //       emit inventoryRoundDone(m_readerID);
    //   }

    //   setAction(ReaderInterface::ACTION_IDLE);
    msleep(m_inventoryDelay);
}

void InventoryRunner::startSingleInventory( bool autoAck, bool fast, quint8 rssi, bool tid )
{
    m_fastMode = fast;
    m_rssiMode = rssi;
    m_autoAckMode = autoAck;
    m_singleInventoryRunning = true;
    m_TidMode = tid;

}

ReaderInterface::Result InventoryRunner::gen2SingleInventory( QList<RawTagInfo> &tags, quint32 &frequency_khz, Gen2ReaderInterface::Gen2Result &status )
{
    Gen2SingleInventoryStart invStart;
    Gen2GetInventoryData invData;
    invStart.setFastMode(m_fastMode);
    invStart.setRssiMode(m_rssiMode);
    invStart.setAutoAckMode(m_autoAckMode);
    invStart.setTidMode(m_TidMode);
    invData.setTidMode(m_TidMode);
    tags.clear();

    m_reader->stream() << TagSelect(TagSelect::Clear) << invStart << AMS_FLUSH;
    do 
    {
        m_reader->stream() >> invData >> AMS_FLUSH;
        if (m_reader->stream().lastError() != 0)
            return ReaderInterface::ComError;

    } while (!invData.receivedInventoryRoundData());

    tags.append(invData.tags());
    //Remove Tags with wrong TID
    for (int it = tags.size()-1; it >=0; it --)
    {
        if(tags.at(it).tid.length() == 1 && tags.at(it).tid[0] <=0xFF)
        {
            tags.removeAt(it);
        }
    }
    return ReaderInterface::OK;
}

void InventoryRunner::cyclicInventory()
{
    QList<RawTagInfo> tags;
    ReaderInterface::Result res;
    quint32 freq;
    quint8 status;

    if (m_cyclicState == Stop)
        return;

    if(m_reader->tagType() == ReaderInterface::TAG_GEN2 || m_reader->tagType() == ReaderInterface::TAG_GEN2_FAST  || m_reader->tagType() == ReaderInterface::TAG_GEN2_TID)
    {
        res = gen2CyclicInventory(tags, freq, status);
        if (res == ReaderInterface::OK)
        {
            RawInvInfo rii;
            rii.tags = tags;
            rii.freq = freq;
            rii.result = status;
            m_inventoryFifo.append(rii);
            emit inventoryResultAvailable();
        }
    }
    //TODO: ISO 6B not yet supported
    //else if(m_tagType == TAG_ISO6B)
    //	res = m_ph->doISO6BSingleInventory(epcs);

    //   /* If everything is ok, handle the result and emit the found tags */


    //msleep(m_inventoryDelay);
}

ReaderInterface::Result InventoryRunner::gen2CyclicInventory( QList<RawTagInfo> &tags, quint32 &frequency_khz, quint8 &status )
{
    Gen2GetInventoryData invData;
    invData.setTidMode(m_TidMode);

    if(m_cyclicState == Start)
    {   //Start cyclic inventories
        Gen2CyclicInventoryStart invStart;
        invStart.setFastMode(m_fastMode);
        invStart.setRssiMode(m_rssiMode);
        invStart.setAutoAckMode(m_autoAckMode);
        invStart.setTidMode(m_TidMode);
        m_reader->stream() << TagSelect(TagSelect::Clear) << invStart << AMS_FLUSH;
        m_cyclicState = Running;
    }
    if(m_cyclicState == Stopping)
    {   //stop cyclic inventories
        Gen2CyclicInventoryStop invStop;
        m_reader->stream() << invStop << AMS_FLUSH;
        int lE = m_reader->stream().lastError();
        if (lE != 0)
            return ReaderInterface::ComError;
    }

    do 
    {
        m_reader->stream() >> invData >> AMS_FLUSH;
        int lE = m_reader->stream().lastError();
        if (lE != 0)
        {
            if (!invData.cyclicInventoryRunning())
            {   //all data received, no cyclic inventory is running --> stop
                m_cyclicState = Stop;
            }
            return ReaderInterface::ComError;
        }

    } while (!invData.receivedInventoryRoundData());

    if (!invData.cyclicInventoryRunning())
    {   //all data received, no cyclic inventory is running --> stop
        m_cyclicState = Stop;
    }
    tags.append(invData.tags());
    //Remove Tags with wrong TID
    for (int it = tags.size()-1; it >=0; it --)
    {
        if(tags.at(it).tid.length() == 1 && tags.at(it).tid[0] <=0xFF)
        {
            tags.removeAt(it);
        }
    }
    
    status = invData.getStatus();
    frequency_khz = invData.getFrequency();
    return ReaderInterface::OK;
}

void InventoryRunner::setInventoryDelay( uint ms )
{
    m_inventoryDelay = ms;
}

uint InventoryRunner::inventoryDelay()
{
    return m_inventoryDelay;
}

void InventoryRunner::stopRunner()
{
    m_runnerRunning = false;
}

bool InventoryRunner::isCyclicInventoryRunning()
{
    if (m_cyclicState == Stop)
        return false;
    return true;
}

bool InventoryRunner::isSingleInventoryRunning()
{
    return m_singleInventoryRunning;
}

void InventoryRunner::startCyclicInventory( bool autoAck, bool fast, quint8 rssi, bool tid )
{
    if (!isCyclicInventoryRunning())
    {
        m_fastMode = fast;
        m_rssiMode = rssi;
        m_autoAckMode = autoAck;
        m_cyclicState = Start;
        m_TidMode = tid;
    }
}

void InventoryRunner::stopCyclicInventory()
{
    if (isCyclicInventoryRunning())
        m_cyclicState = Stopping;
}

bool InventoryRunner::inventoryResult( QList<RawTagInfo> &result, quint32 &frequency_khz, Gen2ReaderInterface::Gen2Result &status )
{
    if (m_inventoryFifo.isEmpty())
        return false;
    result = m_inventoryFifo.first().tags;
    frequency_khz = m_inventoryFifo.first().freq;
    status = static_cast<Gen2ReaderInterface::Gen2Result>(m_inventoryFifo.first().result);
    m_inventoryFifo.removeFirst();
    return true;
}
