/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*!
 * @file	ReaderInterface.cpp
 * @brief	The base implementation of a reader object
 */

#include "ReaderInterface.h"
#include <QCoreApplication>

int ReaderInterface::theReaderCount = 1;

/*!
 * @brief Constructor of the Reader Object
 * The constructor initializes variables and stores the used protocol handler.
 * @param 	ph		Pointer to the used communications object. The class takes ownership of it.
 * @param	parent	Pointer to the parent object.
 */
ReaderInterface::ReaderInterface(AmsComDriver * driver, DriverType type, QObject* parent)
    : QObject( parent )
    , m_driver( driver )
    , m_stream( *driver )
    , m_driverType( type )
    , m_isConfigured( false )
{
    m_stream.setPolicy( AmsComStream::theLateFlushPolicy );

    // initialize the variables
	m_currentState = STATE_OFFLINE;
	m_currentAction = ACTION_IDLE;

	m_readerID 			= "Unknown";
	m_tagType			= TAG_UNKWON;
    m_autoAckMode       = false;
    m_rssiMode          = RSSI_2ndBYTE;
    m_rssiActive  = true;
    m_wrongChipId = false;
    m_initError = UnknownError;
    m_vco = VcoError;
    m_pa = PaError;
    m_input = InputError;
    m_antennaSwitch = false;
    m_tunerConfig = 0;
    m_chipId = InvalidChip;
    m_hardwareId = InvalidHardware;
}

/*! @brief Destructor deletes the used protocol handler */
ReaderInterface::~ReaderInterface ( )
{
    m_stream.close();
    m_driver->close();
    delete m_driver;
    m_driver = 0;
}

/*!
 * @brief	Implementation of the virtual function to initialize the device
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result ReaderInterface::initDevice( )
{
    m_readerID.clear();
    QString hw;

    if (!m_stream.open())
        return ComError;

    if (!m_stream.isProtocolVersionCompatible(true))
        return NA;

    Result res = getHardwareDescription(hw);
    if (res != OK)
    {
        m_readerID = "UNKNOWN";
    }
    else
    {
        foreach(QString s, supportedReaderList())
        {
            if(hw.contains(s, Qt::CaseInsensitive))
                m_readerID = s;
        }
        if (m_readerID.isEmpty())
            m_readerID = "UNKNOWN " + hw;
    }
    m_readerID = QString(m_readerID + " -- %1").arg(theReaderCount);  //add numeric identifier to ID
    theReaderCount++;

    updateReaderState();

    return ReaderInterface::OK;
}

/*!
 * @brief	Implementation of the virtual function to get the reader id
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result ReaderInterface::getReaderID ( QString &readerID )
{
	readerID = m_readerID;
	return ReaderInterface::OK;
}

/*! @brief Returns the gen2 reader. Default 0 = no gen2 reader available */
Gen2ReaderInterface* ReaderInterface::getGen2Reader()
{
	return 0;
}

/*! @brief Getter for Reader ID */
QString ReaderInterface::readerId()
{
	return m_readerID;
}

/*! @brief Getter for used Tag Type */
ReaderInterface::TagType ReaderInterface::tagType()
{
	return m_tagType;
}

/*! @brief Getter for current state */
ReaderInterface::HandlerState ReaderInterface::currentState()
{
	return m_currentState;
}

/*! @brief Getter for current action */
ReaderInterface::HandlerAction ReaderInterface::currentAction()
{
	return m_currentAction;
}

/*! @brief Getter for current state as string */
QString ReaderInterface::currentStateString()
{
	// return the state as human readable string
	switch(m_currentState){
	case STATE_ONLINE:
		return "Online";
	case STATE_OFFLINE:
		return "Offline";
	case STATE_NOTRESPONDING:
		return "Not Responding";
    case STATE_ERROR:
        return "Error";
	case STATE_UNKOWN_ERROR:
		return "Unknown Error";
	default:
		return "Unknown";
	}
}

/*! @brief Getter for current action as string */
QString ReaderInterface::currentActionString()
{
	// return the state as human readable string
	switch(m_currentAction){
	case ACTION_OFFLINE:
		return "Offline";
	case ACTION_IDLE:
		return "Idle";
	case ACTION_SCANNING:
		return "Scanning";
	case ACTION_WAITING:
		return "Waiting";
	default:
		return "Unknown";
	}
}

/*! @brief Setter for the current state */
void ReaderInterface::setState(ReaderInterface::HandlerState state)
{
	// set state and emit it only if it is different to the state before
	if(m_currentState != state){
		m_currentState = state;

		emit changedState(m_currentState, currentStateString(), currentStateDescription());

		if(m_stream.isOpened() && m_currentState == STATE_OFFLINE)
			emit lostConnection(this);
	}
}

/*! @brief Setter for the current action */
void ReaderInterface::setAction(ReaderInterface::HandlerAction action)
{
	// set state and emit it only if it is different to the state before
	if(m_currentAction != action){
		m_currentAction = action;
		emit changedAction(currentActionString());

		// process events so everyone can get the signal
		//qApp->processEvents();
	}
}

/*! @brief Slot that is called if the reader should immediately release the used device */
void ReaderInterface::releaseDeviceImmediatly()
{
	setState(STATE_OFFLINE);
}

/*! @brief Slot that is called if the used device was removed */
void ReaderInterface::deviceWasRemoved()
{
	setState(STATE_OFFLINE);
}

/*!
 * @brief	Function to get if rssi is enabled
 * @returns	Returns true if rssi is enabled
 */
bool ReaderInterface::rssiEnabled ( )
{
	return m_rssiActive;
}

/*!
 * @brief	Function to set if rssi should be enabled
 * @param	on		If the rssi should be enabled
 */
void ReaderInterface::setRssiEnabled ( bool on )
{
    if (m_rssiActive == on)
        return;

	m_rssiActive = on;
    emit changedRssiEnable(m_readerID, on, this->getRssiChildren().size(), this->getRssiChildren());
}

void ReaderInterface::setTIDEnabled ( bool on )
{
    emit changedTIDEnable(m_readerID, on);
}
/*!
 * @brief	Function to set if rssi mode
 * @param	mode is register 0x29 (bit0-3) setting of AS3993
 */
void ReaderInterface::setRssiMode( RssiModes mode )
{
    m_rssiMode = mode;
}

ReaderInterface::RssiModes ReaderInterface::rssiMode()
{
    return m_rssiMode;
}

/*!
 * @brief   Function to deliver names of rssi entries. In the default implementation no rssi is available.
 */
QStringList ReaderInterface::getRssiChildren()
{
    QStringList sl;
    return sl;
}

/*!
 * @brief	Simple helper function to transform epc byte array to a string
 */
QString ReaderInterface::epcToString ( const QByteArray epc )
{
	QString epcString;
	for(int i = 0; i < epc.size(); i++)
	{
		if(i != 0)
			epcString += "-";
		epcString += QString("%1").arg((unsigned char)epc.at(i), 2, 16, QChar('0'));
	}
	return epcString;
}

/*!
 * @brief	Simple helper function to transform epc byte array to a string
 */
QString ReaderInterface::epcToString ( const ComByteArray epc )
{
	QString epcString;
	for(int i = 0; i < epc.size(); i++)
	{
		if(i != 0)
			epcString += "-";
		epcString += QString("%1").arg(epc.at(i), 2, 16, QChar('0'));
	}
	return epcString;
}

/*!
 * @brief	Simple helper function to transform a string to epc byte array
 */
ComByteArray ReaderInterface::stringToEpc ( const QString epcString, bool * ok )
{
	ComByteArray epc;
	QStringList bytes = epcString.split(QChar('-'));
	bool local_ok = false;
	if(!ok)
		ok = &local_ok;
	for(int i = 0; i < bytes.size(); i++)
	{
		(*ok) = false;
		epc.append((uchar) bytes.at(i).toUShort(ok, 16 ) );
		if(!(*ok))
			return ComByteArray();
	}
	return epc;
}

QString ReaderInterface::driverTypeString()
{
    switch (m_driverType)
    {
    case HID :
        return tr("HID");
    case UART :
        return tr("UART");
    default :
        return tr("Unknown Driver");
    }

}

ReaderInterface::DriverType ReaderInterface::driverType()
{
    return m_driverType;
}

ReaderInterface::Result ReaderInterface::checkStreamError()
{
    if (m_stream.lastError() == 0)
        return OK;
    else
        return ComError;
}

/*!
 * @brief	Implementation of the virtual function to set the tag type
 * @return	Returns the result of the operation coded in an enum
 */
ReaderInterface::Result ReaderInterface::setTagType(ReaderInterface::TagType type)
{
	if(type != TAG_GEN2 && type != TAG_ISO6B && type != TAG_GEN2_FAST && type != TAG_GEN2_TID){
		return ReaderInterface::Error;
	}

	m_tagType = type;

	return ReaderInterface::OK;
}

ReaderInterface::Result ReaderInterface::setAutoAckMode( bool on )
{
    if (!isAutoAckSupported())
        return ReaderInterface::NA;

    m_autoAckMode = on;
    return ReaderInterface::OK;
}

AmsComStream & ReaderInterface::stream()
{
    return m_stream;
}

void ReaderInterface::getReaderInitStatus( bool &wrongChip, InitError &initerror )
{
    wrongChip = m_wrongChipId;
    initerror = m_initError;
}

QString ReaderInterface::currentStateDescription()
{
    QString errorMsg;

    errorMsg.clear();
    if (m_wrongChipId)
        errorMsg.append("*) Firmware is not compatible with AS3993.");
    switch (m_initError)
    {
    case NoError:
    	break;
    case SpiError: errorMsg.append("\n*) SPI communication with AS3993 failed."
                                   "\n Hint: Connect DC power and restart GUI/replug USB.");
        break;
    case EnError: errorMsg.append("\n*) Check of the EN pin failed.");
        break;
    case IrqError: errorMsg.append("\n*) IRQ line failed.");
        break;
    case CrystalError: errorMsg.append("\n*) Crystal is not stable.");
        break;
    case PllError: errorMsg.append("\n*) PLL is not locked.");
        break;
    default: errorMsg.append("\n*) An unknown error happened while initializing AS3993.");
        break;
    }

    if (m_vco == VcoError)
        errorMsg.append("\n*) VCO configuration error.");
    if (m_pa == PaError)
        errorMsg.append("\n*) PA configuration error.");
    if (m_input == InputError)
        errorMsg.append("\n*) Input selection error.");
    if (m_tunerConfig == 0xFF)
        errorMsg.append("\n*) Tuner configuration error.");

    return errorMsg;
}

bool ReaderInterface::getReaderAntennaSwitch()
{
    return m_antennaSwitch;
}

quint16 ReaderInterface::getReaderTunerConfig()
{
    return m_tunerConfig;
}

ReaderInterface::ChipId ReaderInterface::getChipId()
{
    return m_chipId;
}

ReaderInterface::HardwareId ReaderInterface::getHardwareId()
{
    return m_hardwareId;
}






