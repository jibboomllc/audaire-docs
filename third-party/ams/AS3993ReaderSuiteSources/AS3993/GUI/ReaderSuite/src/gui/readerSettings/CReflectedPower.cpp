/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "CReflectedPower.h"
#include "../../reader/ReaderInterface.h"
#include <QMessageBox>
#include <QDebug>
#include "qwt_dial_needle.h"
#define _USE_MATH_DEFINES
#include "math.h"

class DynamicNeedle : public QwtDialSimpleNeedle
{
public:
	DynamicNeedle() : QwtDialSimpleNeedle(Arrow,true,QColor(0xE6, 0x6C, 0x00),QColor(0xE6, 0x6C, 0x00))
        , min(0), max(0), current(0)
        {}
	double min,max,current;
	virtual void draw 	(	QPainter * 	painter, 
		const QPoint & 	center, 
		int 	length, 
		double 	direction, 
		QPalette::ColorGroup 	cg = QPalette::Active	 
	) const
	{
		length = (int) (((double)length) * (current-min)/(max-min));
		QwtDialSimpleNeedle::draw(painter,center,length,direction,cg);
	}
};

CReflectedPower::CReflectedPower(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);
	needle = new DynamicNeedle();
	ui.vertexDial->setNeedle(needle);
	ui.vertexDial->setReadOnly(true);
	ui.vertexDial->setWrapping(true);
	ui.vertexDial->setOrigin(0);
	ui.vertexDial->setDirection(QwtDial::CounterClockwise);
	
//	QObject::connect(ui.buttonBox, 						SIGNAL(accepted()), 		this, SLOT(tryAccept()));

	timer.setInterval(50);
	timer.setSingleShot(false);

    QObject::connect(ui.antBox,	        SIGNAL(currentIndexChanged(int)),   this, SLOT(setAntenna(int)));
    QObject::connect(ui.defaultButton,	SIGNAL(clicked()),                  this, SLOT(setDefaultValue()));
	QObject::connect(&timer,            SIGNAL(timeout()),                  this, SLOT(updateDial()));
    QObject::connect(ui.tunerCBox, SIGNAL(currentIndexChanged (int)), this, SLOT(tunerChanged(int)));

}

CReflectedPower::~CReflectedPower()
{
	// delete needle; widget will delete its needle on its own
}
void CReflectedPower::updateDial()
{
	ulong freq = ui.frequencyBox->value() * 1000;
    int ichannel, qchannel;
	double i,q,dBm;
	double angle;
	ReaderInterface::Result res = ReaderInterface::Error;

	if (ui.tunerBox->isEnabled())
	{
		if((ui.cinSlider->value() != oldCin) || (ui.coutSlider->value() != oldCout) || (ui.clenSlider->value() != oldClen))
		{
			oldCin = ui.cinSlider->value();
            oldCout = ui.coutSlider->value();
            oldClen = ui.clenSlider->value();
			res = m_ph->setTuner(ui.tunerCBox->currentIndex(), oldCin, oldClen, oldCout);
			ui.cinVal->setText(QString("(%1pF)").arg(1.3+oldCin*0.131));
			ui.coutVal->setText(QString("(%1pF)").arg(1.3+oldCout*0.131));
			ui.clenVal->setText(QString("(%1pF)").arg(1.3+oldClen*0.131));
		}
		if ( ui.autoTuneBox->currentIndex() != 0 )
		{
			QString s(ui.autoTuneBox->currentText());
			s = s.split(QRegExp("[\\(\\)]"),QString::SkipEmptyParts).last();

            QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
			m_ph->startAutoTuner(s.toInt() | (ui.tunerCBox->currentIndex() << 4), oldCin, oldClen, oldCout);
            QApplication::restoreOverrideCursor();

            ui.autoTuneBox->setCurrentIndex(0);
			ui.cinSlider->setSliderPosition(oldCin);
			ui.cinVal->setText(QString("(%1pF)").arg(1.3+oldCin*0.131));
			ui.clenSlider->setSliderPosition(oldClen);
			ui.clenVal->setText(QString("(%1pF)").arg(1.3+oldClen*0.131));
			ui.coutSlider->setSliderPosition(oldCout);
			ui.coutVal->setText(QString("(%1pF)").arg(1.3+oldCout*0.131));
		}
	}

	res = m_ph->getReflectedPower(freq, 0, ichannel, qchannel);

	if (res != ReaderInterface::OK)
	{
		return;
	}

	i = ichannel;
	q = qchannel;
	
	dBm = sqrt(i*i+q*q);
	if (dBm == 0) dBm = 0.5;
	dBm = 20*log10(dBm/G);

	angle = atan2(i,q);

	if (angle < 0 ) angle += 2 * M_PI;

	angle = (angle / (2*M_PI)) *360.0;

    //qDebug() << "i= " << ichannel << " q= " << qchannel << " dBm= " << dBm << " angle= " << angle;

	ui.ivalBox->setText(QString("%1").arg(i));
	ui.qvalBox->setText(QString("%1").arg(q));
	ui.angleBox->setText(QString("%1").arg(angle)+QChar(0x00b0));
	ui.dBmBox->setText(QString("%1 dBm").arg(dBm));
	this->needle->current = dBm;
	QPalette pal = ui.dBmBox->palette();
	if (dBm >= 5.0){
		pal.setColor(QPalette::Text,Qt::red);
	}
	else
	{
		pal.setColor(QPalette::Text,this->palette().color(QPalette::Text));
	}
	ui.dBmBox->setPalette(pal);
	ui.vertexDial->setValue(angle);
	ui.vertexDial->repaint();
}

int CReflectedPower::exec(ReaderInterface* ph, double freq)
{
	int ret;
    unsigned char ant;
    signed char sens;
    quint16 tunerConfig;
    quint8 reg15;
    qint8 sensitivity;
    // Give Register 22 a right value if required
    quint8 reg04,reg0A, reg22;
    quint8 reg04Orig,reg0AOrig, reg22Orig;   
	m_ph = ph;
	if( m_ph == 0 )
		return 0;

    tunerConfig = m_ph->getReaderTunerConfig();
    if (tunerConfig == 0)
        return 0;

    if (m_ph->getHardwareId() == ReaderInterface::Radon)
    { /* For Radon we have to limit output power and sensitivity, otherwise we will see wraparounds of measured reflected power */
        sens = -68;
        m_ph->readRegister(0x15, reg15);
        m_ph->writeRegister(0x15, 0x0a); /* -10 */
        m_ph->setGetTxRxParams(sensitivity, false, ant, false);
        m_ph->setGetTxRxParams(sens, true, ant, false);
    }

    m_ph->readRegister(0x04, reg04);
    m_ph->readRegister(0x0A, reg0A);
    m_ph->readRegister(0x22, reg22);
    reg04Orig=reg04;
    reg0AOrig=reg0A;
    reg22Orig=reg22;

    reg0A &= 0x03;// take for comparison only the last bits
    reg04 &= 0xF0;// take for comparison only the first bits

    if (m_ph->getHardwareId() == ReaderInterface::Fermi || m_ph->getHardwareId() == ReaderInterface::Radon )
    { /* Only these two readers have balanced input mixer */
        if (reg0A == 0x00)
        {
            if (reg04 != 0x00) m_ph->writeRegister(0x04, 0x02);
        }
        else if (reg0A == 0x01)
        {
            if (reg04 != 0x00) m_ph->writeRegister(0x04, 0x02);
        }
        else if (reg0A == 0x02)
        {
            if ( (reg04 != 0x40) && (reg04 != 0x00) ) m_ph->writeRegister(0x04, 0x02);
        }
        else
        {
            ui.infoEdit->setText("Error Register 0x0A has the wrong value");
        }
    }
    else // Take Femto Vals
    {
        if (reg0A == 0x00)
        {
            if (reg22 != 0x17) m_ph->writeRegister(0x22, 0x17);
        }
        else if (reg0A == 0x01)
        {
            if (reg22 != 0x14) m_ph->writeRegister(0x22, 0x14);
        }
        else if (reg0A == 0x03)
        {
            if ( (reg22 != 0x12) && (reg22 != 0x00) ) m_ph->writeRegister(0x22, 0x00);
        }
        else
        {

            ui.infoEdit->setText(QString("Error Register 0x0A has the wrong value"));

        }
    }
    
    if (!(tunerConfig & 0xff) != !(tunerConfig & 0xff00))
    { /* XOR: if only one is activated hide the combo box */
        ui.tunerLabel->hide();
        ui.tunerCBox->hide();
    }
    else
    {
        ui.tunerLabel->show();
        ui.tunerCBox->show();
    }

    ui.tunerCBox->setCurrentIndex(0);
    
    ui.frequencyBox->setValue(freq);

	G = m_ph->getG_rfp();
	needle->max = 20.0;
	needle->min = 20*log10(0.5/G);

    tunerChanged(0);

    //set selected antenna
    ret = m_ph->setGetTxRxParams(sens, false, ant, false);
    if (ret)
    {
        ui.antBox->setEnabled(false);
    }
    else
    {
        ui.antBox->setEnabled(true);
        ui.antBox->setCurrentIndex(ant - 1);
    }

	timer.start();

	ret = QDialog::exec();

    m_ph->writeRegister(0x04, reg04Orig); // Set Register 04 back
    m_ph->writeRegister(0x22, reg22Orig); // Set Register 22 back
    if (m_ph->getHardwareId() == ReaderInterface::Radon)
    { /* For Radon we had to limit output power and sensitivity, restore now */
        m_ph->writeRegister(0x15, reg15);
        m_ph->setGetTxRxParams(sensitivity, true, ant, false);
    }

	timer.stop();

	return ret;
}

void CReflectedPower::setAntenna( int index )
{
    unsigned char ant;
    signed char sens;

    ant = (unsigned char)(index + 1);

    m_ph->setGetTxRxParams(sens, false, ant, true);

    if (m_ph->getHardwareId() == ReaderInterface::Radon)
    {
        if ( index == 0 ) // Antenna 1
        {
            ui.tunerCBox->setEnabled(true);
        }
        else if (index == 1) // Antenna 2
        {
            ui.tunerCBox->setCurrentIndex(0);
            ui.tunerCBox->setEnabled(false);
        }
    }

}

void CReflectedPower::tunerChanged( int index )
{
    if (index ==0)
    { 
        ui.tunerBox->setTitle("System Tuner");
    }
    else
    {
        ui.tunerBox->setTitle("Antenna Tuner");
    }
    
    int ret;
    quint8 c;
    quint16 cfg = m_ph->getReaderTunerConfig();

    c = (0==index)?(cfg&0xff):(cfg>>8);

    ui.cinLabel   ->setVisible(c & TUNER_CIN);
    ui.cinSlider  ->setVisible(c & TUNER_CIN);
    ui.cinSpinBox ->setVisible(c & TUNER_CIN);
    ui.cinVal     ->setVisible(c & TUNER_CIN);

    ui.clenLabel  ->setVisible(c & TUNER_CLEN);
    ui.clenSlider ->setVisible(c & TUNER_CLEN);
    ui.clenSpinBox->setVisible(c & TUNER_CLEN);
    ui.clenVal    ->setVisible(c & TUNER_CLEN);

    ui.coutLabel  ->setVisible(c & TUNER_COUT);
    ui.coutSlider ->setVisible(c & TUNER_COUT);
    ui.coutSpinBox->setVisible(c & TUNER_COUT);
    ui.coutVal    ->setVisible(c & TUNER_COUT);

    ret = m_ph->getTuner(ui.tunerCBox->currentIndex(), oldCin, oldClen, oldCout);

    QPalette pal;
    if (0==index)
    {
        pal.setColor(QPalette::Mid,QColor(0xE6, 0x6C, 0x00));
        pal.setColor(QPalette::Base,QColor(0xE6, 0x6C, 0x00));
    }
    else
    {
        pal.setColor(QPalette::Mid,QColor(0x00, 0x6C, 0xe6));
        pal.setColor(QPalette::Base,QColor(0x00, 0x6C, 0xe6));
    }
    ui.vertexDial->needle()->setPalette(pal);


    if (ret)
    {
        ui.tunerBox->setEnabled(false);
    }
    else
    {
        ui.tunerBox->setEnabled(true);
        ui.cinSlider->setSliderPosition(oldCin);
        ui.cinVal->setText(QString("(%1pF)").arg(1.3+oldCin*0.131));
        ui.clenSlider->setSliderPosition(oldClen);
        ui.clenVal->setText(QString("(%1pF)").arg(1.3+oldClen*0.131));
        ui.coutSlider->setSliderPosition(oldCout);
        ui.coutVal->setText(QString("(%1pF)").arg(1.3+oldCout*0.131));
    }
}

int CReflectedPower::currentAntenna()
{
    return ui.antBox->currentIndex();
}

void CReflectedPower::setDefaultValue()
{
    quint8 tunerConfig;
    tunerConfig = m_ph->getReaderTunerConfig();

    if ((tunerConfig & TUNER_CIN) == TUNER_CIN)
        ui.cinSlider->setSliderPosition(15);

    if ((tunerConfig & TUNER_CLEN) == TUNER_CLEN)
        ui.clenSlider->setSliderPosition(15);

    if ((tunerConfig & TUNER_COUT) == TUNER_COUT)
        ui.coutSlider->setSliderPosition(15);

}
