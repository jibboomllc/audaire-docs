/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "SL900AMainDialog.hxx"
#include <QMessageBox>
#include "qdebug.h"


SL900AMainDialog::SL900AMainDialog ( QWidget *parent )
{
    setupUi(this);

    itsCoolLogTab = new SL900ACoolLogCommands(this);
    itsDemoTab    = new SL900ADemo(this);
    tabWidget->addTab(itsDemoTab,    tr("Demo")); 
    tabWidget->addTab(itsCoolLogTab, tr("Cool Log Commands"));

    //connect(itsCoolLogTab, SIGNAL(displayMessage(QString)), this, SLOT(logMessage(QString)));
    //connect(itsDemoTab,    SIGNAL(displayMessage(QString)), this, SLOT(logMessage(QString)));
    
}

SL900AMainDialog::~SL900AMainDialog ( )
{
    delete itsCoolLogTab;
    delete itsDemoTab;
}

int SL900AMainDialog::exec ( ReaderInterface* ph, QString tagId )
{
    itsReaderInterface = ph;

    if(!(itsReaderInterface->tagType() == ReaderInterface::TAG_GEN2 || itsReaderInterface->tagType() == ReaderInterface::TAG_GEN2_FAST || itsReaderInterface->tagType() == ReaderInterface::TAG_GEN2_TID))
        return 0;

    if((itsGen2Reader = itsReaderInterface->getGen2Reader()) == 0)
        return 0;
    
    itsTagId = tagId;

    tagIdEdit->setText(itsTagId);
    readerIdEdit->setText(ph->readerId());

    le_infoEdit->clear();

    readRegistersforOptimalSettings();
    writeOptimalValuesToReader();
    cB_AdvancedRegSet->setChecked(true);
    itsCoolLogTab->calibrationDataToSetCalibration();
    return QDialog::exec();
}
void SL900AMainDialog::closeEvent ( QCloseEvent * e )
{
    writeOriginalValuesToReader();
}

void SL900AMainDialog::on_buttonBox_accepted ( )
{
    QDialog::accept();
}

void SL900AMainDialog::on_pb_Clearlogs_clicked ( )
{
    le_infoEdit->clear();
}

void SL900AMainDialog::logMessage ( QString message )
{
    le_infoEdit->append(message);
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::executeCommand ( int commandCode, unsigned int rxDataSize, QByteArray rxData,unsigned int rxAnswerSize, QByteArray &answer )
{
    if(!le_Password->hasAcceptableInput())
    {
        QMessageBox::critical(this, "Error", "The password is not a valid!");
        return Gen2ReaderInterface::Gen2_OK;
    }
    bool ok = false;
    ComByteArray passw = itsReaderInterface->stringToEpc(le_Password->text(), &ok);
    if(!ok)
    {
        QMessageBox::critical(this, "Error", "The password is not a valid!");
        return Gen2ReaderInterface::Gen2_OK;
    }

    // Create Values
    unsigned char tmp[3] = { 0x00 };
    tmp[0]  = ((static_cast<int>(rxDataSize + RN16SIZE) >> 8) & 0x0F) << 4;      // TX Bit Length of command upper nibble
    tmp[0] |= (static_cast<int>(rxDataSize + RN16SIZE) & 0xF0) >> 4;             // TX Bit Length of command middle nibble
    tmp[1]  = (static_cast<int>(rxDataSize + RN16SIZE) & 0x0F) << 4;             // TX Bit Length of command lower nibble
    tmp[1] |= (static_cast<int>(rxAnswerSize + RN16SIZE + CRC16SIZE) >> 8) & 0x0F; // RX Bit Length of command upper nibble
    tmp[2]  = static_cast<int>(rxAnswerSize + RN16SIZE + CRC16SIZE) & 0xFF;        // RX Bit Length of command middle and lower nibble

    int respTime = COOL_LOG_RESPONSE_TIME;
    int customCMD = COOL_LOG_CUSTOM_CMD;
    int directCMD = AS399xCMDTRANSMCRCEHEAD;
    QByteArray commandData;
    commandData.append(static_cast<char>(tmp[0]));
    commandData.append(static_cast<char>(tmp[1]));
    commandData.append(static_cast<char>(tmp[2]));
    commandData.append(directCMD);
    commandData.append(respTime);
    commandData.append(customCMD);
    commandData.append(commandCode);
    commandData.append(rxData);

    unsigned int rxBitLength;
    QString strSend(commandData.toHex());
    // logMessage(QString("Request: %1").arg(strSend));
    Gen2Tag tag;
    tag.epc = tagIdEdit->text();
    Gen2ReaderInterface::Gen2Result gen2res = itsGen2Reader->genericCommand(tag, commandData, passw.toQByteArray(), answer, rxBitLength); // pass an empty tagID (EPC) to bypass tag select command

    if ( (gen2res == Gen2ReaderInterface::Gen2_OK) && (answer.isEmpty() || (answer.length() > 300)) )
    {
        gen2res = Gen2ReaderInterface::Gen2_Tag_OTHER;
    }

    //rxBitLength -= RN16SIZE; //remove handle
    int bytelengthAnswer = answer.length();
    answer.remove(bytelengthAnswer-2,2);
    QString strAnswer(answer.toHex());

    return gen2res;
}

float SL900AMainDialog::tempfromCode ( unsigned int code )
{
    return (code * 0.18 - 89.3);
}

unsigned int SL900AMainDialog::codefromTemp ( double temperature )
{
    return ( static_cast<unsigned int>((temperature + 89.3) / 0.18) );
}

float SL900AMainDialog::battVfromCode ( unsigned int code, SL900Battype battType )
{
    float battV = 0.0;
    if (battType == SL900Battype::Bat1V5)
    {
        battV = (code * 0.00085 + 0.873);
    } 
    else if (battType == SL900Battype::Bat3V0)
    {
        battV = (code * 0.00165 + 1.69);
    }

    return battV;
}

unsigned int SL900AMainDialog::codefrombattV ( double battVoltage , SL900Battype battType )
{
    unsigned int code = 0;
    if (battType == SL900Battype::Bat1V5)
    {
        code = ( static_cast<unsigned int>((battVoltage - 0.873) / 0.00085) );
    } 
    else if (battType == SL900Battype::Bat3V0)
    {
        code = ( static_cast<unsigned int>((battVoltage - 1.69) / 0.00165) );
    }

    return code;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogSetPassword ( CoolLogError &error, unsigned char passwordLevel, unsigned long password )
{
    itsExecutedCommand = CMD_SET_PASSWORD;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    unsigned char pwByte1 = (password & 0x000000FF);
    unsigned char pwByte2 = (password & 0x0000FF00) >> 8;
    unsigned char pwByte3 = (password & 0x00FF0000) >> 16;
    unsigned char pwByte4 = (password & 0xFF000000) >> 24;

    itsRequest.append(passwordLevel);
    itsRequest.append(pwByte4);
    itsRequest.append(pwByte3);
    itsRequest.append(pwByte2);
    itsRequest.append(pwByte1);

    itsGen2Res = executeCommand(CMD_SET_PASSWORD, 56, itsRequest, 1, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogSetLogMode ( CoolLogError &error, SL900AGroupLogMode logMode, unsigned int logInterval )
{
    itsExecutedCommand = CMD_SET_LOG_MODE;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    unsigned char logModeByte1, logModeByte2, logModeByte3;

    logModeByte1  = (logInterval & 0x007F) << 1;
    logModeByte2  = (logInterval & 0x7F80) >> 7;
    logModeByte3  = (logMode.battCheckEnable & 0x1);
    logModeByte3 |= (logMode.tempSenEnable & 0x1) << 1;
    logModeByte3 |= (logMode.extSen2Enable & 0x1) << 2;
    logModeByte3 |= (logMode.extSen1Enable & 0x1) << 3;
    logModeByte3 |= (logMode.storageRule & 0x1) << 4;
    logModeByte3 |= (logMode.loggingForm & 0x7) << 5;

    itsRequest.append(logModeByte3);
    itsRequest.append(logModeByte2); 
    itsRequest.append(logModeByte1);

    itsGen2Res = executeCommand(CMD_SET_LOG_MODE, 40, itsRequest, 1, itsAnswer);
    
    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogSetLogLimits ( CoolLogError &error, SL900AGroupLimits limits )
{
    itsExecutedCommand = CMD_SET_LOG_LIMITS;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    unsigned char logLimByte1, logLimByte2, logLimByte3, logLimByte4, logLimByte5;
    logLimByte1 =  (limits.extremeUpperLimit & 0x0FF);
    logLimByte2 =  (limits.extremeUpperLimit & 0x300) >> 8;
    logLimByte2 += (limits.upperLimit & 0x03F) << 2;
    logLimByte3 =  (limits.upperLimit & 0x3C0) >> 6;
    logLimByte3 += (limits.lowerLimit & 0x00F) << 4;
    logLimByte4 =  (limits.lowerLimit & 0x3F0) >> 4;
    logLimByte4 += (limits.extremeLowerLimit & 0x003) << 6;
    logLimByte5 =  (limits.extremeLowerLimit & 0x3FC) >> 2;

    itsRequest.append(logLimByte5);
    itsRequest.append(logLimByte4); 
    itsRequest.append(logLimByte3);
    itsRequest.append(logLimByte2);
    itsRequest.append(logLimByte1);
    
    itsGen2Res = executeCommand(CMD_SET_LOG_LIMITS, 56, itsRequest, 1, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogGetMeasurementSetup ( CoolLogError &error, QDate &date, QTime &time, SL900AGroupLimits &limits, SL900AGroupLogMode &logMode,
                                                                               unsigned int &logInterval, SL900AGroupDelayTime &delay, SL900AGroupUserData &userData )
{
    itsExecutedCommand = CMD_GET_MEASUREMENT_SETUP;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    itsGen2Res = executeCommand(CMD_GET_MEASUREMENT_SETUP, 16, itsRequest, 129, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
    {
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));
        return itsGen2Res;
    }
    QByteArray baStartTime   = itsAnswer.mid(0, 4);
    QByteArray baLogLimits   = itsAnswer.mid(4, 5);
    QByteArray baLogMode     = itsAnswer.mid(9, 1);
    QByteArray baLogInterval = itsAnswer.mid(10, 2);
    QByteArray baDelayTime   = itsAnswer.mid(12, 2);
    QByteArray baAppData     = itsAnswer.mid(14, 2);

    //Start Time
    unsigned char sec, min, hour, day, month, year;
    sec    = (baStartTime[3] & 0x3F);
    min    = (baStartTime[3] & 0xC0) >> 6;
    min   |= (baStartTime[2] & 0x0F) << 2;
    hour   = (baStartTime[2] & 0xF0) >> 4;
    hour  |= (baStartTime[1] & 0x01) << 4;
    day    = (baStartTime[1] & 0x3E) >> 1;
    month  = (baStartTime[1] & 0xC0) >> 6;
    month |= (baStartTime[0] & 0x03) << 2;
    year   = (baStartTime[0] & 0xFC) >> 2;
    int year2000 = year + 2000;
    date.setDate(year2000, month, day);
    time.setHMS(hour, min, sec);

    //Limits
    limits.extremeUpperLimit  = (baLogLimits[4] & 0xFF);
    limits.extremeUpperLimit |= (baLogLimits[3] & 0x03) << 8;
    limits.upperLimit         = (baLogLimits[3] & 0xFC) >> 2;
    limits.upperLimit        |= (baLogLimits[2] & 0x0F) << 6;
    limits.lowerLimit         = (baLogLimits[2] & 0xF0) >> 4;
    limits.lowerLimit        |= (baLogLimits[1] & 0x3F) << 4;
    limits.extremeLowerLimit  = (baLogLimits[1] & 0xC0) >> 6;
    limits.extremeLowerLimit |= (baLogLimits[0] & 0xFF) << 2;
    
    //Log Mode
    logMode.loggingForm     = (baLogMode[0] & 0xE0) >> 5;
    logMode.storageRule     = (baLogMode[0] & 0x10) >> 4; 
    logMode.extSen1Enable   = (baLogMode[0] & 0x08) >> 3;
    logMode.extSen2Enable   = (baLogMode[0] & 0x04) >> 2;
    logMode.tempSenEnable   = (baLogMode[0] & 0x02) >> 1;
    logMode.battCheckEnable = (baLogMode[0] & 0x01);

    //Log Interval
    logInterval  = (baLogInterval[0] & 0xFF) << 7;
    logInterval |= (baLogInterval[1] & 0xFE) >> 1;
    
    //Delay Time
    delay.delayTime  = (baDelayTime[0] & 0xFF) << 4;
    delay.delayTime |= (baDelayTime[1] & 0xF0) >> 4;
    delay.delayMode  = (baDelayTime[1] & 0x02) >> 1;
    delay.irqEnalbe  = (baDelayTime[1] & 0x01);

    //Application Data
    userData.numWordsAppData    = (baAppData[0] & 0xFF) << 1;
    userData.numWordsAppData   |= (baAppData[1] & 0x80) >> 7;
    userData.brokenWordPointer  = (baAppData[1] & 0x7);

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogSetSFEParameters ( CoolLogError &error, SL900AGroupSFEParameters sfeParameters )
{
    itsExecutedCommand = CMD_SET_SFE_PARAMETERS;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    unsigned char sfeByte1, sfeByte2;
    sfeByte2  = (sfeParameters.rang     & 0x1F) << 3;
    sfeByte2 |= (sfeParameters.seti     & 0x1C) >> 2;
    sfeByte1  = (sfeParameters.seti     & 0x03) << 6;
    sfeByte1 |= (sfeParameters.ext1     & 0x03) << 4;
    sfeByte1 |= (sfeParameters.ext2     & 0x01) << 3;
    sfeByte1 |= (sfeParameters.autodis  & 0x01) << 2;
    sfeByte1 |= (sfeParameters.verifyId & 0x03);

    itsRequest.append(sfeByte2);
    itsRequest.append(sfeByte1);

    itsGen2Res = executeCommand(CMD_SET_SFE_PARAMETERS, 32, itsRequest, 1, itsAnswer);
    
    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogSetCalibrationData ( CoolLogError &error, SL900AGroupCalibration calibration )
{
    itsExecutedCommand = CMD_SET_CALIBRATION_DATA;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    unsigned char calByte0, calByte1, calByte2, calByte3, calByte4, calByte5, calByte6;

    calByte0  = (calibration.ad1       & 0x1F) << 3;
    calByte0 |= (calibration.coars1    & 0x07);
    calByte1  = (calibration.ad2       & 0x1F) << 3;
    calByte1 |= (calibration.coars2    & 0x07);
    calByte2  = (calibration.gnd_sw    & 0x01) << 7;
    calByte2 |= (calibration.selp      & 0x03) << 5;
    calByte2 |= (calibration.adf       & 0x1F);
    calByte3  = (calibration.df        & 0xFF);
    calByte4  = (calibration.sw_ext_en & 0x01) << 7;
    calByte4 |= (calibration.selp22    & 0x03) << 5;
    calByte4 |= (calibration.irlev     & 0x03) << 3;
    calByte4 |= (calibration.ring_call & 0x1C) >> 2;
    calByte5  = (calibration.ring_call & 0x03) << 6;
    calByte5 |= (calibration.off_int   & 0x7E) >> 1;
    calByte6  = (calibration.off_int   & 0x01) << 7;
    calByte6 |= (calibration.reftc     & 0x0F) << 3;
    calByte6 |= (calibration.exc_res   & 0x01) << 2;

    itsRequest.append(calByte0);
    itsRequest.append(calByte1);
    itsRequest.append(calByte2);
    itsRequest.append(calByte3);
    itsRequest.append(calByte4);
    itsRequest.append(calByte5);
    itsRequest.append(calByte6);

    itsGen2Res = executeCommand(CMD_SET_CALIBRATION_DATA, 72, itsRequest, 1, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogEndLog ( CoolLogError &error )
{
    itsExecutedCommand = CMD_SET_END_LOG;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    itsGen2Res = executeCommand(CMD_SET_END_LOG, 16, itsRequest, 1, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogStartLog ( CoolLogError &error, QDate startDate, QTime startTime )
{
    itsExecutedCommand = CMD_SET_START_LOG;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    unsigned char timeByte1, timeByte2, timeByte3, timeByte4;
    int startYear2000 = startDate.year() - 2000;

    timeByte1  = (startTime.second() & 0x3F);
    timeByte1 |= (startTime.minute() & 0x03) << 6;
    timeByte2  = (startTime.minute() & 0x3C) >> 2;
    timeByte2 |= (startTime.hour()   & 0x0F) << 4;
    timeByte3  = (startTime.hour()   & 0x10) >> 4;
    timeByte3 |= (startDate.day()    & 0x1F) << 1;
    timeByte3 |= (startDate.month()  & 0x03) << 6;
    timeByte4  = (startDate.month()  & 0x0C) >> 2;
    timeByte4 |= (startYear2000      & 0x3F) << 2;

    itsRequest.append(timeByte4);
    itsRequest.append(timeByte3); 
    itsRequest.append(timeByte2);
    itsRequest.append(timeByte1);

    itsGen2Res = executeCommand(CMD_SET_START_LOG, 48, itsRequest, 1, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogGetLogState ( CoolLogError &error, SL900AGroupLimitsCounter &limitCounter, SL900AGroupSystemStatus &systemStatus,
                                                                       SL900AStatusFlags &flags, SL900AGroupShelfLife &shelfLife, unsigned long int &currentShelfLife )
{
    itsExecutedCommand = CMD_GET_LOG_STATE;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();
    bool answerWithShelfLife = false;

    itsGen2Res = executeCommand(CMD_GET_LOG_STATE, 16, itsRequest, 73, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK) //Try again with Shelf Life
    {
        itsAnswer.clear();
        itsGen2Res = executeCommand(CMD_GET_LOG_STATE, 16, itsRequest, (73 + 88), itsAnswer);
        answerWithShelfLife = true;
    }

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
    {
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));
        return itsGen2Res;
    }

    QByteArray baLimitCounter, baSystemStatus, baStatusFlag;
    QByteArray baSLBlock01, baCurrShelfLife;
    if (answerWithShelfLife)
    {
         baLimitCounter  = itsAnswer.mid(0,  4);
         baSystemStatus  = itsAnswer.mid(4,  4);
         baSLBlock01     = itsAnswer.mid(8,  8);
         baCurrShelfLife = itsAnswer.mid(16, 3);
         baStatusFlag    = itsAnswer.mid(19, 1);
    }
    else
    {
         baLimitCounter = itsAnswer.mid(0, 4);
         baSystemStatus = itsAnswer.mid(4, 4);
         baStatusFlag   = itsAnswer.mid(8, 1);
    }
    
    // Limit Counter
    limitCounter.extremeLowerCounter = (baLimitCounter[0] & 0xFF);
    limitCounter.lowerCounter        = (baLimitCounter[1] & 0xFF);
    limitCounter.upperCounter        = (baLimitCounter[2] & 0xFF);
    limitCounter.extremeUpperCounter = (baLimitCounter[3] & 0xFF);
    // System Status
    systemStatus.measAddressPointer  = (baSystemStatus[0] & 0xFF) << 2;
    systemStatus.measAddressPointer |= (baSystemStatus[1] & 0x03) >> 6;
    systemStatus.memoryReplacements  = (baSystemStatus[1] & 0x3F);
    systemStatus.measurements        = (baSystemStatus[2] & 0xFF) << 7;
    systemStatus.measurements       |= (baSystemStatus[3] & 0xFE) >> 1;
    systemStatus.active              = (baSystemStatus[3] & 0x01);
    //Status Flag
    flags.activeLogging      = (baStatusFlag[0] & 0x80) >> 7;
    flags.areaFull           = (baStatusFlag[0] & 0x40) >> 6;
    flags.overwritten        = (baStatusFlag[0] & 0x20) >> 5;
    flags.adError            = (baStatusFlag[0] & 0x10) >> 4;
    flags.lowBattery         = (baStatusFlag[0] & 0x08) >> 3;
    flags.shelfLifeLowError  = (baStatusFlag[0] & 0x04) >> 2;
    flags.shelfLifeHighError = (baStatusFlag[0] & 0x02) >> 1;
    flags.shelfLifeExpired   = (baStatusFlag[0] & 0x01);
    // ShelfLife

    if (answerWithShelfLife)
    {
        shelfLife.tmax    = (baSLBlock01[0] & 0xFF);
        shelfLife.tmin    = (baSLBlock01[1] & 0xFF);
        shelfLife.tstd    = (baSLBlock01[2] & 0xFF);
        shelfLife.ea      = (baSLBlock01[3] & 0xFF);
        shelfLife.slinit  = (baSLBlock01[4] & 0xFF) << 8;
        shelfLife.slinit |= (baSLBlock01[5] & 0xFF);
        shelfLife.tinit   = (baSLBlock01[6] & 0xFF) << 2;
        shelfLife.tinit  |= (baSLBlock01[7] & 0xC0) >> 6;
        shelfLife.shelfLifeSensor = (baSLBlock01[7] & 0x30) >> 4;
        shelfLife.negShelfLife    = (baSLBlock01[7] & 0x08) >> 3;
        shelfLife.algoShelfLife   = (baSLBlock01[7] & 0x04) >> 2;
        
        currentShelfLife  = (baCurrShelfLife [0] & 0xFF) << 16;
        currentShelfLife |= (baCurrShelfLife [1] & 0xFF) << 8;
        currentShelfLife |= (baCurrShelfLife [2] & 0xFF);
    }
    else
    {
        shelfLife.tmax    = 0;
        shelfLife.tmin    = 0;
        shelfLife.tstd    = 0;
        shelfLife.ea      = 0;
        shelfLife.slinit  = 0;
        shelfLife.tinit   = 0;
        shelfLife.shelfLifeSensor = 0;
        shelfLife.negShelfLife    = 0;
        shelfLife.algoShelfLife   = 0;
        currentShelfLife  = 0;
    }

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogGetCalibrationData ( CoolLogError &error, SL900AGroupCalibration &calibration, SL900AGroupSFEParameters &sfeParameters )
{
    itsExecutedCommand = CMD_GET_CALIBRATION_DATA;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    itsGen2Res = executeCommand(CMD_GET_CALIBRATION_DATA, 16, itsRequest, 73, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
    {
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));
        return itsGen2Res;
    }

    //Calibration Data
    calibration.ad1        = (itsAnswer[0] & 0xF8) >> 3;
    calibration.coars1     = (itsAnswer[0] & 0x07);
    calibration.ad2        = (itsAnswer[1] & 0xF8) >> 3;
    calibration.coars2     = (itsAnswer[1] & 0x07);
    calibration.gnd_sw     = (itsAnswer[2] & 0x80) >> 7;
    calibration.selp       = (itsAnswer[2] & 0x60) >> 5;
    calibration.adf        = (itsAnswer[2] & 0x1F);
    calibration.df         = (itsAnswer[3] & 0xFF);
    calibration.sw_ext_en  = (itsAnswer[4] & 0x80) >> 7;
    calibration.selp22     = (itsAnswer[4] & 0x60) >> 5;
    calibration.irlev      = (itsAnswer[4] & 0x18) >> 3;
    calibration.ring_call  = (itsAnswer[4] & 0x07) << 2;
    calibration.ring_call |= (itsAnswer[5] & 0xC0) >> 6;
    calibration.off_int    = (itsAnswer[5] & 0x3F) << 1;
    calibration.off_int   |= (itsAnswer[6] & 0x80) >> 7;
    calibration.reftc      = (itsAnswer[6] & 0x7F) >> 3;
    calibration.exc_res    = (itsAnswer[6] & 0x04) >> 2;

    //SFE
    sfeParameters.rang     = (itsAnswer[7] & 0xF8) >> 3;
    sfeParameters.seti     = (itsAnswer[7] & 0x07) << 2;
    sfeParameters.seti    |= (itsAnswer[8] & 0xC0) >> 6;
    sfeParameters.ext1     = (itsAnswer[8] & 0x30) >> 4;
    sfeParameters.ext2     = (itsAnswer[8] & 0x08) >> 3;
    sfeParameters.autodis  = (itsAnswer[8] & 0x04) >> 2;
    sfeParameters.verifyId = (itsAnswer[8] & 0x03);
    
    return itsGen2Res;

}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogGetBatteryLevel ( CoolLogError &error, unsigned char batteryRetrigger, bool &adError,
                                                                           SL900Battype &sl900batType, unsigned int &batLevel )
{
    itsExecutedCommand = CMD_GET_BATTERY_LEVEL;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    itsRequest.append(batteryRetrigger);

    itsGen2Res = executeCommand(CMD_GET_BATTERY_LEVEL, 24, itsRequest, 17, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
    {
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));
        return itsGen2Res;
    }

    unsigned char battType;

    adError   = (itsAnswer[0] & 0x80) >> 7;
    battType  = (itsAnswer[0] & 0x40) >> 6;  
    batLevel  = (itsAnswer[0] & 0x03) << 8;
    batLevel |= (itsAnswer[1] & 0xFF) ;
    
    if (battType == 0) 
    {
        sl900batType = SL900Battype::Bat1V5;
    }
    else if  (battType == 1) 
    {
        sl900batType = SL900Battype::Bat3V0;
    }
    
    return itsGen2Res;

}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogSetShelfLife( CoolLogError &error, SL900AGroupShelfLife shelfLife )
{
    itsExecutedCommand = CMD_SET_SHELF_LIFE;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    unsigned char slBByte1, slBByte2, slBByte3, slBByte4;

    slBByte1  = (shelfLife.slinit & 0xFF00) >> 8;
    slBByte2  = (shelfLife.slinit & 0x00FF);
    slBByte3  = (shelfLife.tinit  & 0x3FC) >> 2;
    slBByte4  = (shelfLife.tinit  & 0x003) << 6;
    slBByte4 |= (shelfLife.shelfLifeSensor & 0x03) << 4;
    slBByte4 |= (shelfLife.negShelfLife    & 0x01) << 3;
    slBByte4 |= (shelfLife.algoShelfLife   & 0x01) << 2;
    slBByte4 |= (shelfLife.logInterval     & 0x03);

    itsRequest.append(shelfLife.tmax);
    itsRequest.append(shelfLife.tmin);
    itsRequest.append(shelfLife.tstd);
    itsRequest.append(shelfLife.ea);
    itsRequest.append(slBByte1);
    itsRequest.append(slBByte2);
    itsRequest.append(slBByte3);
    itsRequest.append(slBByte4);

    itsGen2Res = executeCommand(CMD_SET_SHELF_LIFE, 80, itsRequest, 1, itsAnswer);
    
    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
            error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;

}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogInitilize ( CoolLogError &error, SL900AGroupDelayTime delayTime, SL900AGroupUserData userdata )
{
    itsExecutedCommand = CMD_INITIALIZE;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    unsigned char byteDelayTime1, byteDelayTime2, byteAppData1, byteAppData2;

    byteDelayTime1  = (delayTime.irqEnalbe & 0x01);
    byteDelayTime1 |= (delayTime.delayMode & 0x01) << 1;
    byteDelayTime1 |= (delayTime.delayTime & 0x0F) << 4;
    byteDelayTime2  = (delayTime.delayTime & 0xFF0) >> 4;
    byteAppData1    = (userdata.brokenWordPointer &  0x07);
    byteAppData1   |= (userdata.numWordsAppData   & 0x001) << 7;
    byteAppData2    = (userdata.numWordsAppData   & 0x1FE) >> 1;

    itsRequest.append(byteDelayTime2);
    itsRequest.append(byteDelayTime1);
    itsRequest.append(byteAppData2);
    itsRequest.append(byteAppData1);

    itsGen2Res = executeCommand(CMD_INITIALIZE, 48, itsRequest, 1, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;

}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogGetSensorValue ( CoolLogError &error, unsigned char sensorType, bool &adError,
                                                                          unsigned char &range, unsigned int &sensorValue )
{
    itsExecutedCommand = CMD_GET_SENSOR_VALUE;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    itsRequest.append(sensorType);

    itsGen2Res = executeCommand(CMD_GET_SENSOR_VALUE, 24, itsRequest, 17, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
    {
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));
        return itsGen2Res;
    }

    adError      = (itsAnswer[0] & 0x80) >> 7;
    range        = (itsAnswer[0] & 0x7C) >> 2;
    sensorValue  = (itsAnswer[0] & 0x03) << 8;
    sensorValue |= (itsAnswer[1] & 0xFF);
    
    return itsGen2Res;

}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogOpenArea ( CoolLogError &error, unsigned char passwordLevel, unsigned long password )
{
    itsExecutedCommand = CMD_OPEN_AREA;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    unsigned char pwByte1 = (password & 0x000000FF);
    unsigned char pwByte2 = (password & 0x0000FF00) >> 8;
    unsigned char pwByte3 = (password & 0x00FF0000) >> 16;
    unsigned char pwByte4 = (password & 0xFF000000) >> 24;

    itsRequest.append(passwordLevel);
    itsRequest.append(pwByte4);
    itsRequest.append(pwByte3);
    itsRequest.append(pwByte2);
    itsRequest.append(pwByte1);

    itsGen2Res = executeCommand(CMD_OPEN_AREA, 56, itsRequest, 1, itsAnswer);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::coolLogAccessFIFO ( CoolLogError &error, unsigned char subcommand, QByteArray writePayload, QByteArray &readPayload )
{
    itsExecutedCommand = CMD_ACCESS_FIFO;
    error = CoolLogError::CoolLogNoError;
    itsRequest.clear();
    itsAnswer.clear();

    int appendedBytes = writePayload.length();
    unsigned int numBytesAvail = 0;
    switch (subcommand)
    {
    case 0xA0: 
        itsRequest[0] = 0xA0 + appendedBytes;
        itsRequest.append(writePayload);
        itsGen2Res = executeCommand(CMD_ACCESS_FIFO , (24 + appendedBytes * 8), itsRequest, 1, itsAnswer);
        break;
    case 0x80: 
        //read Bytes Available
        itsRequest.append((char)0xC0);
        itsGen2Res = executeCommand(CMD_ACCESS_FIFO , 24 , itsRequest, 9, readPayload);
        numBytesAvail = (readPayload[0] & 0x0F);
        //perform read
        itsRequest[0] = 0x80 | (numBytesAvail & 0x0F);
        itsGen2Res = executeCommand(CMD_ACCESS_FIFO , 24 , itsRequest, (1 + numBytesAvail * 8) , readPayload);
        break;
    
    case 0xC0: // Status
              itsRequest.append(subcommand);
              itsGen2Res = executeCommand(CMD_ACCESS_FIFO , 24 , itsRequest, 9, readPayload);
              break;
              
    }
    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        error = static_cast<CoolLogError>(static_cast<unsigned char>(itsAnswer[0]));

    return itsGen2Res;
}

Gen2ReaderInterface::Gen2Result SL900AMainDialog::readUserMemory ( CoolLogError &error, unsigned int address, QByteArray &answer )
{
    ComByteArray mem;
    bool ok;
    ComByteArray passw = ReaderInterface::stringToEpc(le_Password->text(), &ok);
    Gen2Tag tag;
    tag.epc = tagIdEdit->text();
    itsGen2Res = itsGen2Reader->readFromTag(tag, MEMORY_BANK::MEM_USER, address, passw, 4, false, mem);

    answer = mem.toQByteArray();
    
    return itsGen2Res;
}

void SL900AMainDialog::logRequestAndAnswer ( )
{
    QByteArray baAnswer = itsAnswer.toHex();
    logMessage( QString("  Request: %1 ").arg(itsExecutedCommand, 2  , 16 , QLatin1Char('0')) + itsRequest.toHex() );

    if (baAnswer.length() > 300)
    {
        logMessage( "  Answer : ERROR OCCURED!"  );
    }
    else
    {
        logMessage( "  Answer : " + baAnswer  );
    }
}

QString SL900AMainDialog::decodeCoolLogError ( CoolLogError error )
{
    QString answer;
    switch(error)
    {
    case CoolLogError::CoolLogNoError                  : answer = " No Error                  "  ; break;
    case CoolLogError::CoolLogOtherError               : answer = " Other Error               "  ; break;
    case CoolLogError::CoolLogMemoryOverrun            : answer = " Memory Overrun            "  ; break;
    case CoolLogError::CoolLogMemoryLocked             : answer = " Memory Locked             "  ; break;
    case CoolLogError::CoolLogInsufficentPower         : answer = " Insufficient Power        "  ; break;
    case CoolLogError::CoolLogIncorrectPassword        : answer = " Incorrect Password        "  ; break;
    case CoolLogError::CoolLogBatteryMeasurementError  : answer = " Battery Measurement Error "  ; break;
    case CoolLogError::CoolLogCommandNotAllowed        : answer = " Command Not Allowed       "  ; break;
    case CoolLogError::CoolLogEEPROMBusyError          : answer = " EEPROM Busy Error         "  ; break;
    default                                            : answer = QString("Unknown Error %1").arg(error); break;
    }
    return answer;

}

void SL900AMainDialog::readRegistersforOptimalSettings()
{
    
    itsReaderInterface->readRegister(0x02, itsOrigRegVal0x02);
    itsReaderInterface->readRegister(0x08, itsOrigRegVal0x08);
    itsReaderInterface->readRegister(0x14, itsOrigRegVal0x14);
    unsigned char ant;
    itsReaderInterface->setGetTxRxParams(itsOrigSens, false, ant, false);
}

void SL900AMainDialog::writeOriginalValuesToReader()
{
    itsReaderInterface->writeRegister(0x02, itsOrigRegVal0x02);
    itsReaderInterface->writeRegister(0x08, itsOrigRegVal0x08);
    itsReaderInterface->writeRegister(0x14, itsOrigRegVal0x14);
    unsigned char ant;
    itsReaderInterface->setGetTxRxParams(itsOrigSens, true, ant, false);
}

void SL900AMainDialog::writeOptimalValuesToReader()
{
    itsReaderInterface->writeRegister(0x02, 0x31);
    itsReaderInterface->writeRegister(0x08, 0x08);
    itsReaderInterface->writeRegister(0x14, 0x9D);
    signed char optSensVal = -48;
    unsigned char ant;
    itsReaderInterface->setGetTxRxParams(optSensVal, true, ant, false);
}

void SL900AMainDialog::on_cB_AdvancedRegSet_clicked ( bool checkState )
{
    if (checkState)
    {
        writeOptimalValuesToReader();
    }
    else
    {
        writeOriginalValuesToReader();
    }

}