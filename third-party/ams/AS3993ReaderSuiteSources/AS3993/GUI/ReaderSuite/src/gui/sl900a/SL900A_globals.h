/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef SL900A_GLOBALS_H
#define SL900A_GLOBALS_H


#define RN16SIZE      16
#define CRC16SIZE     16

#define CMD_SET_PASSWORD           0xA0
#define CMD_SET_LOG_MODE           0xA1
#define CMD_SET_LOG_LIMITS         0xA2
#define CMD_GET_MEASUREMENT_SETUP  0xA3
#define CMD_SET_SFE_PARAMETERS     0xA4
#define CMD_SET_CALIBRATION_DATA   0xA5
#define CMD_SET_END_LOG            0xA6
#define CMD_SET_START_LOG          0xA7
#define CMD_GET_LOG_STATE          0xA8
#define CMD_GET_CALIBRATION_DATA   0xA9
#define CMD_GET_BATTERY_LEVEL      0xAA
#define CMD_SET_SHELF_LIFE         0xAB
#define CMD_INITIALIZE             0xAC
#define CMD_GET_SENSOR_VALUE       0xAD
#define CMD_OPEN_AREA              0xAE
#define CMD_ACCESS_FIFO            0xAF

#define COOL_LOG_CUSTOM_CMD        0xE0
#define COOL_LOG_RESPONSE_TIME     0xFF
#define AS399xCMDTRANSMCRCEHEAD    0x91

#define COOL_LOG_NO_ERROR                   0x99
#define COOL_LOG_OTHER_ERROR                0x00
#define COOL_LOG_MEMORY_OVERRUN             0x03
#define COOL_LOG_MEMORY_LOCKED              0x04
#define COOL_LOG_INSUFFICIENT_POWER         0x0A
#define COOL_LOG_INCORRECT_PASSWORD         0xA0
#define COOL_LOG_BATTERY_MEASUREMENT_ERROR  0xA2
#define COOL_LOG_COMMAND_NOT_ALLOWED        0xA3
#define COOL_LOG_EEPROM_BUSY_ERROR          0xA6


#define SL900A_MAX_ADRESS_POINTER  0x82  //Memory Size 1052 Bytes // Stop at 0x82

#pragma warning( disable: 4482) // disable nonstandard enum Extension warnings

typedef enum SL900Battype
{
    Bat1V5 = 1,
    Bat3V0 = 2
} SL900Battype_;

typedef enum CoolLogError
{
    CoolLogNoError                  = COOL_LOG_NO_ERROR,
    CoolLogOtherError               = COOL_LOG_OTHER_ERROR,
    CoolLogMemoryOverrun            = COOL_LOG_MEMORY_OVERRUN,
    CoolLogMemoryLocked             = COOL_LOG_MEMORY_LOCKED,
    CoolLogInsufficentPower         = COOL_LOG_INSUFFICIENT_POWER,
    CoolLogIncorrectPassword        = COOL_LOG_INCORRECT_PASSWORD,
    CoolLogBatteryMeasurementError  = COOL_LOG_BATTERY_MEASUREMENT_ERROR,
    CoolLogCommandNotAllowed        = COOL_LOG_COMMAND_NOT_ALLOWED,
    CoolLogEEPROMBusyError          = COOL_LOG_EEPROM_BUSY_ERROR

} CoolLogError_;

// Group Structures
struct SL900AGroupCalibration 
{
    unsigned char ad1;
    unsigned char coars1;
    unsigned char ad2;
    unsigned char coars2;
    unsigned char gnd_sw;
    unsigned char selp;
    unsigned char adf;
    unsigned char df;
    unsigned char sw_ext_en;
    unsigned char selp22;
    unsigned char irlev;
    unsigned char ring_call;
    unsigned char off_int;
    unsigned char reftc;
    unsigned char exc_res;
};

struct SL900AGroupSFEParameters
{
    unsigned char rang;
    unsigned char seti;
    unsigned char ext1;
    unsigned char ext2;
    unsigned char autodis;
    unsigned char verifyId;
};

struct SL900AGroupLimits 
{
    unsigned int extremeLowerLimit;
    unsigned int lowerLimit;
    unsigned int upperLimit;
    unsigned int extremeUpperLimit;
};

struct SL900AGroupLimitsCounter 
{
    unsigned char extremeLowerCounter;
    unsigned char lowerCounter;
    unsigned char upperCounter;
    unsigned char extremeUpperCounter;
};

struct SL900AGroupSystemStatus
{
    unsigned int  measAddressPointer;
    unsigned char memoryReplacements;
    unsigned int  measurements;
    unsigned char active;
};

struct SL900AGroupLogMode
{
    unsigned char loggingForm;
    unsigned char storageRule;
    unsigned char extSen1Enable;
    unsigned char extSen2Enable;
    unsigned char tempSenEnable;
    unsigned char battCheckEnable;
};

struct SL900AGroupDelayTime
{
    unsigned int  delayTime;
    unsigned char delayMode;
    unsigned char irqEnalbe;
};

struct SL900AGroupUserData
{
    unsigned int numWordsAppData;
    unsigned int brokenWordPointer;
};

struct SL900AGroupShelfLife
{
    unsigned char tmax;
    unsigned char tmin;
    unsigned char tstd;
    unsigned char ea;
    unsigned int slinit;
    unsigned int tinit;

    unsigned char shelfLifeSensor;
    unsigned char negShelfLife;
    unsigned char algoShelfLife;
    unsigned char logInterval;
};

struct SL900AStatusFlags
{
    unsigned char activeLogging;
    unsigned char areaFull;
    unsigned char overwritten;
    unsigned char adError;
    unsigned char lowBattery;
    unsigned char shelfLifeLowError;
    unsigned char shelfLifeHighError;
    unsigned char shelfLifeExpired;
};

#endif //SL900A_GLOBALS_H
