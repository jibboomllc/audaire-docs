/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef SL900A_MAIN_DIALOG_HXX
#define SL900A_MAIN_DIALOG_HXX

#include <QtGui/QDialog>

#include "ui_SL900AMainDialog.h"
#include "SL900A_globals.h"

#include "../../reader/ReaderInterface.h"
#include "../../reader/Gen2ReaderInterface.h"
#include "../../reader/epc/EPC_Defines.h"

#include "SL900ACoolLogCommands.hxx"
#include "SL900ADemo.hxx"


class SL900ACoolLogCommands;
class SL900ADemo;

class SL900AMainDialog : public QDialog, public Ui_SL900AMainDialogClass
{
    Q_OBJECT

public:
    SL900AMainDialog ( QWidget *parent = NULL );
    ~SL900AMainDialog ( );
    int exec ( ReaderInterface* ph, QString tagId );

    Gen2ReaderInterface::Gen2Result executeCommand ( int commandCode, unsigned int rxDataSize, QByteArray rxData,unsigned int rxAnswerSize, QByteArray &answer );

    float tempfromCode ( unsigned int code );
    unsigned int codefromTemp ( double temperature );
    float battVfromCode ( unsigned int code, SL900Battype battType );
    unsigned int codefrombattV ( double battVoltage , SL900Battype battType );

    void logMessage ( QString message );
    void logRequestAndAnswer ( );
    QString decodeCoolLogError ( CoolLogError error );

    SL900ACoolLogCommands *itsCoolLogTab;
    SL900ADemo            *itsDemoTab;
    Gen2ReaderInterface   *itsGen2Reader;

    // Cool Log Commands
    Gen2ReaderInterface::Gen2Result coolLogSetPassword ( CoolLogError &error, unsigned char passwordLevel, unsigned long password );
    Gen2ReaderInterface::Gen2Result coolLogSetLogMode ( CoolLogError &error, SL900AGroupLogMode logMode, unsigned int logInterval );
    Gen2ReaderInterface::Gen2Result coolLogSetLogLimits ( CoolLogError &error, SL900AGroupLimits limits);
    Gen2ReaderInterface::Gen2Result coolLogGetMeasurementSetup ( CoolLogError &error, QDate &date, QTime &time, SL900AGroupLimits &limits, SL900AGroupLogMode &logMode, unsigned int &logInterval, SL900AGroupDelayTime &delay, SL900AGroupUserData &userData );
    Gen2ReaderInterface::Gen2Result coolLogSetSFEParameters ( CoolLogError &error, SL900AGroupSFEParameters sfeParameters );
    Gen2ReaderInterface::Gen2Result coolLogSetCalibrationData ( CoolLogError &error, SL900AGroupCalibration calibration );
    Gen2ReaderInterface::Gen2Result coolLogEndLog ( CoolLogError &error );
    Gen2ReaderInterface::Gen2Result coolLogStartLog ( CoolLogError &error, QDate startDate, QTime startTime );
    Gen2ReaderInterface::Gen2Result coolLogGetLogState ( CoolLogError &error, SL900AGroupLimitsCounter &limitCounter, SL900AGroupSystemStatus &systemStatus, SL900AStatusFlags &flags, SL900AGroupShelfLife &shelfLife, unsigned long int &currentShelfLife );
    Gen2ReaderInterface::Gen2Result coolLogGetCalibrationData ( CoolLogError &error, SL900AGroupCalibration &calibration, SL900AGroupSFEParameters &sfeParameters );
    Gen2ReaderInterface::Gen2Result coolLogGetBatteryLevel ( CoolLogError &error, unsigned char batteryRetrigger, bool &adError, SL900Battype &sl900batType, unsigned int &batLevel );
    Gen2ReaderInterface::Gen2Result coolLogSetShelfLife ( CoolLogError &error, SL900AGroupShelfLife shelfLife );
    Gen2ReaderInterface::Gen2Result coolLogInitilize ( CoolLogError &error, SL900AGroupDelayTime delayTime, SL900AGroupUserData userdata );
    Gen2ReaderInterface::Gen2Result coolLogGetSensorValue ( CoolLogError &error, unsigned char sensorType, bool &adError, unsigned char &range, unsigned int &sensorValue );
    Gen2ReaderInterface::Gen2Result coolLogOpenArea ( CoolLogError &error, unsigned char passwordLevel, unsigned long password );
    Gen2ReaderInterface::Gen2Result coolLogAccessFIFO ( CoolLogError &error, unsigned char subcommand, QByteArray writePayload, QByteArray &readPayload );
    Gen2ReaderInterface::Gen2Result readUserMemory ( CoolLogError &error, unsigned int address, QByteArray &answer );

private slots:
    void on_buttonBox_accepted ( );
    void on_pb_Clearlogs_clicked ( );
    void on_cB_AdvancedRegSet_clicked ( bool checkState );
    void  	closeEvent ( QCloseEvent * e );
private:
    void readRegistersforOptimalSettings();   
    void writeOriginalValuesToReader();
    void writeOptimalValuesToReader();

    ReaderInterface *itsReaderInterface;
    Gen2ReaderInterface::Gen2Result itsGen2Res;

    QString itsTagId;

    QByteArray itsRequest;
    QByteArray itsAnswer;
    unsigned int itsExecutedCommand;

    quint8 itsOrigRegVal0x02;
    quint8 itsOrigRegVal0x08;
    quint8 itsOrigRegVal0x14;
    signed char itsOrigSens;


};

#endif // SL900A_MAIN_DIALOG_HXX
