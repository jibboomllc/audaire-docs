/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef SL900A_COOL_LOG_COMMANDS_HXX
#define SL900A_COOL_LOG_COMMANDS_HXX

#include <QtGui/QDialog>
#include "SL900AMainDialog.hxx"

#include "ui_SL900ACoolLogCommands.h"

#include "../../reader/ReaderInterface.h"
#include "../../reader/Gen2ReaderInterface.h"
#include "../../reader/epc/EPC_Defines.h"
#include "../helper/QrfeProgressBar.h"

class SL900AMainDialog;

typedef enum CoolLogCommand
{
    CoolLogSetPassword     = CMD_SET_PASSWORD,
    CoolLogSetLogMode      = CMD_SET_LOG_MODE,
    CoolLogSetLogLimits    = CMD_SET_LOG_LIMITS,
    CoolLogGetMeasSetup    = CMD_GET_MEASUREMENT_SETUP,
    CoolLogSetSfeParams    = CMD_SET_SFE_PARAMETERS,
    CoolLogSetCalibration  = CMD_SET_CALIBRATION_DATA,
    CoolLogEndLog          = CMD_SET_END_LOG,
    CoolLogStartLog        = CMD_SET_START_LOG,
    CoolLogGetLogState     = CMD_GET_LOG_STATE,
    CoolLogGetCalibration  = CMD_GET_CALIBRATION_DATA,
    CoolLogGetBatLevel     = CMD_GET_BATTERY_LEVEL,
    CoolLogSetShelfLife    = CMD_SET_SHELF_LIFE,
    CoolLogInitialize      = CMD_INITIALIZE,
    CoolLogGetSensorValue  = CMD_GET_SENSOR_VALUE,
    CoolLogOpenArea        = CMD_OPEN_AREA,
    CoolLogAccessFIFO      = CMD_ACCESS_FIFO,
    CoolLogUnknown         = 0x99
} CoolLogCommand_; 


class SL900ACoolLogCommands : public QDialog, public Ui_SL900ACoolLogCommands
{
    Q_OBJECT

public:
    SL900ACoolLogCommands ( SL900AMainDialog *parent = NULL );
    ~SL900ACoolLogCommands ( ) { };
    bool calibrationDataToSetCalibration ( );

private slots:

    // cool-Log Commands
    void on_rb_SetPassword_clicked ( );
    void on_rb_SetLogMode_clicked ( );
    void on_rb_SetLogLimits_clicked ( );
    void on_rb_GetMeasurementSetup_clicked ( );
    void on_rb_SetSFEParameters_clicked ( );
    void on_rb_SetCalibrationData_clicked ( );
    void on_rb_EndLog_clicked ( );
    void on_rb_StartLog_clicked ( );
    void on_rb_GetLogState_clicked ( );
    void on_rb_GetCalibrationData_clicked ( );
    void on_rb_GetBatteryLevel_clicked ( );
    void on_rb_SetShelfLife_clicked ( );
    void on_rb_Initialize_clicked ( );
    void on_rb_GetSensorValue_clicked ( );
    void on_rb_OpenArea_clicked ( );
    void on_rb_AccessFIFO_clicked ( );

    void on_pb_Execute_clicked ( );

    void on_pb_NowDateTime_clicked ( );
    void on_pb_LoadCalData_clicked ( );
    void on_pb_Enable_All_Calibrations_clicked ( );

    void on_cB_Rang_currentIndexChanged ( int index );
    void on_dsp_Seti_valueChanged( double value );
    void on_cB_Ext1_currentIndexChanged ( int index );
    void on_cB_Ext2_currentIndexChanged ( int index );
    void on_cB_verifySensorID_currentIndexChanged ( int index );

    void on_sp_extLowLim_valueChanged ( int value );
    void on_sp_LowLim_valueChanged ( int value );
    void on_sp_UpperLim_valueChanged ( int value );
    void on_sp_extUpperLim_valueChanged ( int value );
    void on_sp_extLowLimDeg_valueChanged ( double value );
    void on_sp_LowLimDeg_valueChanged ( double value );
    void on_sp_UpperLimDeg_valueChanged ( double value );
    void on_sp_extUpperLimDeg_valueChanged ( double value );


private:

    SL900AMainDialog *itsParent;

    Gen2ReaderInterface::Gen2Result itsGen2Res;
    CoolLogError itsCoolLogError; 

    bool itsCalDataLoaded;

    void executeSetPassword ( );
    void executeSetLogMode ( );
    void executeSetLogLimits ( );
    void executeGetMeasurementSetup ( );
    void executeSetSFEParameters ( );
    void executeSetCalibrationData ( );
    void executeEndLog ( );
    void executeStartLog ( );
    void executeGetLogState ( );
    void executeGetCalibrationData ( );
    void executeGetBatteryLevel ( );
    void executeSetShelfLife ( );
    void executeInitialize ( );
    void executeGetSensorValue ( );
    void executeOpenArea ( );
    void executeAccessFIFO ( );

    CoolLogCommand getCoolLogCommandFromIndex ( unsigned int index );
    QString decodeCoolLogCommand ( CoolLogCommand command );
};

#endif // SL900A_COOL_LOG_COMMANDS_HXX
