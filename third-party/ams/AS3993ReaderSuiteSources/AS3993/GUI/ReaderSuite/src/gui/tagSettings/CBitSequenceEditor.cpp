/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */


#include "CBitSequenceEditor.hxx"
#include <QMessageBox>
#include "qdebug.h"

CBitSequenceEditor::CBitSequenceEditor(QPair<QString, int> * result, QWidget *parent)
    : m_result(result)
    , QDialog(parent)
{
    ui.setupUi(this);
    QObject::connect(ui.buttonBox, 				SIGNAL(accepted()), 		this, SLOT(tryAccept()));
}

CBitSequenceEditor::~CBitSequenceEditor()
{
}

void CBitSequenceEditor::tryAccept()
{
    *m_result = convertToEpcString(ui.bitSequenceTextEdit->toPlainText());
    emit editingFinished();
}

QPair<QString, int> CBitSequenceEditor::convertToEpcString(QString & input)
{
    int bitCount = 0;
    int bitVal = 0;
    int byteVal = 0;
    int i = 0;
    int totalBits = 0;
    QPair<QString, int> result;
    result.first = "";
    result.second = 0;

    if (input.size())
    {
        while (i < input.size())
        {
            if (input[i] != '0' && input[i] != '1')
            {
                i++;
                continue;
            }
            bitVal = (input[i] == '1') ? 1 : 0;
            byteVal |= bitVal << (7 - bitCount);
            if (bitCount == 7)
            {
                result.first.append(QString::QString("%1-").arg(byteVal, 2, 16, QLatin1Char('0')));
                bitCount = 0;
                byteVal = 0;
            }
            else
            {
                bitCount++;
            }
            totalBits++;
            i++;
        }
        if (bitCount > 0)
        {
            result.first.append(QString::QString("%1").arg(byteVal, 2, 16, QLatin1Char('0')));
        }
        else
        {
            result.first.remove(result.first.size() - 1, 1);
        }
        result.second = totalBits;
        if (totalBits == 0)
        {
            QMessageBox::information(this, "Bit Stream Editor ", "Your string " + ui.bitSequenceTextEdit->toPlainText() + " is not a valid bit stream");
        }
    }
    return result;
}
