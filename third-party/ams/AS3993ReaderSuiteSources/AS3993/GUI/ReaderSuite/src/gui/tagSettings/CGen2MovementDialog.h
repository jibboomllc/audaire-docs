/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef CGEN2MOVEDIALOG_H
#define CGEN2MOVEDIALOG_H

#include <QtGui/QDialog>
#include <QTimer.h>
#include "ui_CGen2MovementDetector.h"

#include "../../reader/Gen2ReaderInterface.h"
#include "../../reader/ams/AmsReader.h"

#include <QrfeTrace.h>

class CGen2MovementDialog : public QDialog, QrfeTraceModule
{
    Q_OBJECT

public:
    CGen2MovementDialog(QWidget *parent = 0);
    ~CGen2MovementDialog();

public slots:
    int exec(Gen2ReaderInterface* reader, Gen2Tag &tag);
    virtual void closeEvent ( QCloseEvent * event );
    void cycle(void);
    void close(QAbstractButton * button);
    void on_start_stop_btn_clicked();

private:
    void handleError(Gen2ReaderInterface::Gen2Result result, QString text);
    QTimer itsTimer;

    void clearInfo();
    void showOK();
    void showTagUnreachable();
    void showWrongPassword();
    void showMemoryOverrun();
    void showMemoryLocked();
    void showInsufficentPower();
    void showNOK();
    Ui::CGen2MovementDetectorClass ui;

    AmsReader *m_reader;
};

#endif // CGEN2MOVEDIALOG_H
