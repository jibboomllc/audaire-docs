/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "CReaderConfigDialog.h"

#include "../helper/QrfeProgressBar.h"
#include "../../reader/ReaderInterface.h"

#include "globals.h"
#include "qwt_plot.hxx"
#include "qwt_plot_curve.h"
#include "qwt_legend.hxx"
#include "qwt_scale_widget.hxx"
#include "CReflectedPower.h"

#include <QMessageBox>
#include <QDebug>
#include <QString>
#include <QFileDialog>
#include <QTime>
#include <QStandardItemModel>

class CReaderConfigDialog::TuneParameter 
{
public:
    /*implement constructor to ensure that tuning is always initialized to disabled */
    TuneParameter()
    {   //initialize everything to 0, to avoid awkward values in tuning file
        tuneEnable[0] = false;
        tuneEnable[1] = false;
        cin[0] = 0;
        cin[1] = 0;
        clen[0] = 0;
        clen[1] = 0;
        cout[0] = 0;
        cout[1] = 0;
        dBm[0] = 0;
        dBm[1] = 0;
        iq[0] = 0;
        iq[1] = 0;
    }
	ulong frequency;		//< Frequency of the tuning parameter
    bool tuneEnable[2];	    //< defines if antenna 1/2 should be tuned
	quint8 cin[2];				//< cin value of antenna 1/2 as register value. no real Farad value!
	quint8 clen[2];			//< clen value of antenna 1/2 as register value. no real Farad value!
	quint8 cout[2];			//< cout value of antenna 1/2 as register value. no real Farad value!
	double dBm[2];			//< measured reflected power of antenna 1/2 after tuning
	quint16 iq[2];			//< i^2 + q^2 of reflected power of antenna 1/2 after tuning
};

CReaderConfigDialog::CReaderConfigDialog(QWidget *parent)
    : QDialog(parent),
    m_ph(0)
{
	ui.setupUi(this);

    ui.sweepModeBox->hide(); // RSSI not implemented at the moment
    ui.l_SweepMode->hide();
    // fill reference divider combobox
    ui.refDividerBox->setItemData(0, 25);
    ui.refDividerBox->setItemData(1, 50);
    ui.refDividerBox->setItemData(2, 100);
    ui.refDividerBox->setItemData(3, 125);

    superUser = false;
    ui.antennaPowerEvalCheckBox->setHidden(true);
    superUserAct = new QAction(this);
    superUserAct->setShortcut(tr("Ctrl+Alt+S"));
    connect(superUserAct, SIGNAL(triggered()), this, SLOT(superUserMode()));
    addAction(superUserAct);

	QObject::connect(ui.buttonBox, 						SIGNAL(rejected()), 		this, SLOT(tryAccept()));
	QObject::connect(ui.inventoryIntervalBox,        	SIGNAL(valueChanged(int)),	this, SLOT(setInventoryInterval()));
    QObject::connect(ui.antennaPowerCheckBox, 			SIGNAL(stateChanged(int)),	this, SLOT(setAntennaPower()));
	QObject::connect(ui.setFrequencyButton, 			SIGNAL(clicked()), 			this, SLOT(setFrequency()));
	QObject::connect(ui.tagModeBox,     	 			SIGNAL(currentIndexChanged(int)), this, SLOT(setTagMode()));
    QObject::connect(ui.autoAckBox,                     SIGNAL(clicked(bool)),      this, SLOT(autoAckChanged()));
    QObject::connect(ui.agcModeBox,                     SIGNAL(clicked(bool)),      this, SLOT(agcModeChanged()));
	QObject::connect(ui.setGen2Configuration,			SIGNAL(clicked()), 			this, SLOT(setGen2Configuration()));
	QObject::connect(ui.antBox, 						SIGNAL(currentIndexChanged(int)), this, SLOT(setAntenna()));
    QObject::connect(ui.rssiBox, 						SIGNAL(currentIndexChanged(int)), this, SLOT(setRssiMode()));
	QObject::connect(ui.levelSlider,					SIGNAL(valueChanged(int)), 	this, SLOT(levelSliderChanged(int)));
    QObject::connect(ui.levelSlider,					SIGNAL(sliderReleased()), 	this, SLOT(setLevel()));
	QObject::connect(ui.sensSlider,						SIGNAL(valueChanged(int)), 	this, SLOT(sensSliderChanged(int)));
    QObject::connect(ui.sensSlider,						SIGNAL(sliderReleased()), 	this, SLOT(setSensitivity()));
	QObject::connect(ui.sweepButton,		 			SIGNAL(clicked()), 			this, SLOT(sweepFrequencys()));
	QObject::connect(ui.commandButton,		 			SIGNAL(clicked()), 			this, SLOT(sendCommand()));
    QObject::connect(ui.setHoppingButton,      			SIGNAL(clicked()), 			this, SLOT(setHoppingParams()));

	QObject::connect(ui.startFrequencyBox,	 			SIGNAL(valueChanged(double)),this, SLOT(oneFreqChanged(double)));
	QObject::connect(ui.endFrequencyBox,	 			SIGNAL(valueChanged(double)),this, SLOT(oneFreqChanged(double)));
	QObject::connect(ui.incrementBox,	 				SIGNAL(valueChanged(double)),this, SLOT(oneFreqChanged(double)));
	QObject::connect(ui.allocationTimeBox,				SIGNAL(valueChanged(double)),this, SLOT(oneFreqChanged(double)));
    QObject::connect(ui.aBox,           				SIGNAL(valueChanged(int)),   this, SLOT(abValueChanged()));
    QObject::connect(ui.bBox,           				SIGNAL(valueChanged(int)),   this, SLOT(abValueChanged()));

    QObject::connect(ui.millerBox,      				SIGNAL(currentIndexChanged(int)),  this, SLOT(codingChanged(int)));
    QObject::connect(ui.linkBox,        				SIGNAL(currentIndexChanged(int)),  this, SLOT(linkFrequencyChanged(int)));
    QObject::connect(ui.tariBox,        				SIGNAL(currentIndexChanged(int)),  this, SLOT(tariChanged(int)));
    QObject::connect(ui.pilotToneBox,                   SIGNAL(toggled(bool)),             this, SLOT(pilotChanged(bool)));
	
	QObject::connect(ui.hoppingBox,			 			SIGNAL(currentIndexChanged( const QString &)),  this, SLOT(setHopper( const QString &)));
    QObject::connect(ui.refDividerBox,				    SIGNAL(currentIndexChanged(int)),               this, SLOT(refDividerChanged()));
	QObject::connect(ui.openRFPButton,					SIGNAL(clicked()),			this, SLOT(openRFPDialog()));
	QObject::connect(ui.continuousSendButton,			SIGNAL(clicked()),			this, SLOT(continuousSend()));

	QObject::connect(ui.createTuningButton,				SIGNAL(clicked()),			this, SLOT(tuneFrequencies()));
	QObject::connect(ui.uploadTuningButton,				SIGNAL(clicked()),			this, SLOT(uploadTuningParameters()));
    QObject::connect(ui.tuneStartFreqBox,               SIGNAL(valueChanged(double)),this, SLOT(calcTuneTableSize()));
    QObject::connect(ui.tuneEndFreqBox,                 SIGNAL(valueChanged(double)),this, SLOT(calcTuneTableSize()));
    QObject::connect(ui.tuneIncFreqBox,                 SIGNAL(valueChanged(double)),this, SLOT(calcTuneTableSize()));
    QObject::connect(ui.biasBox,                        SIGNAL(currentIndexChanged(int)), this, SLOT(biasSettingChanged()));
    QObject::connect(ui.powerModeBox,                   SIGNAL(currentIndexChanged(int)), this, SLOT(powerDownModeChanged()));
    QObject::connect(ui.cb_G8,                          SIGNAL(clicked()), this, SLOT(changeexternalPAGain()));
    QObject::connect(ui.cb_G16,                          SIGNAL(clicked()), this, SLOT(changeexternalPAGain()));
    QObject::connect(ui.cB_SelectMode,                  SIGNAL(currentIndexChanged(int)), this, SLOT(cB_SelectMode(int)));
    

    ui.commandBox->setStyleSheet("#commandBox {font-family: courier}");

	readProfiles();
    ui.cB_SelectMode->setEnabled(false);
    ui.l_target->hide();
    ui.cB_Gen2target->hide();
}

CReaderConfigDialog::~CReaderConfigDialog()
{

}

int CReaderConfigDialog::exec(ReaderInterface* ph)
{
	m_ph = ph;
	if( m_ph == 0 )
		return 0;

	clearInfo();

    readConfigAndUpdateControls();

	return QDialog::exec();
}

void CReaderConfigDialog::readConfigAndUpdateControls()
{
    bool success = true;
    quint8 tunerConfig;
    bool antennaSwitch;
    ReaderInterface::Result res;
    QrfeProgressBar* pb = new QrfeProgressBar("Reading data from reader...", 8, qobject_cast<QWidget*>(parent()));

    tunerConfig = m_ph->getReaderTunerConfig();
    res = m_ph->getMaxTunerTableSize(m_maxTunerTableSize);
    if (res != ReaderInterface::OK)
    {
        success = false;
        ui.labelTunerTableSize->setText("Max tuner table size: n/a");
        ui.labelTunerTableSize->setToolTip(QString("Error getting max tuner table size from reader."));
    }
    else
    {
        ui.labelTunerTableSize->setText(QString("Max tuner table size: %1").arg(m_maxTunerTableSize));
        ui.labelTunerTableSize->setToolTip(QString("Reader reported max tuner table size of %1.").arg(m_maxTunerTableSize));
    }
    antennaSwitch = m_ph->getReaderAntennaSwitch();
    if (antennaSwitch)
    {
        this->ui.antBox->setVisible(true);
        this->ui.antLabel->setVisible(true);
    }
    else
    {
        this->ui.antBox->setVisible(false);
        this->ui.antLabel->setVisible(false);
    }
    if (tunerConfig > NO_TUNER)
    {
        this->ui.tunerTab->setEnabled(true);
    }
    else
    {
        this->ui.tunerTab->setEnabled(false);
    }
    
    if ( m_ph->getHardwareId() == ReaderInterface::Radon)
        ui.levelSlider->setMaximum(-6);
    else
        ui.levelSlider->setMaximum(0);
    ui.levelSlider->setMinimum(-19);

    pb->increaseProgressBar();

	quint32 interval = m_ph->intervalTime();

	pb->increaseProgressBar();

	ui.inventoryIntervalBox->setValue(interval);
    ui.antennaPowerCheckBox->setChecked(false);
    ui.antennaPowerEvalCheckBox->setChecked(false);

    pb->increaseProgressBar();

	success = success && getGen2Config();
    pb->increaseProgressBar();
	success = success && getOutputLevel();
    pb->increaseProgressBar();
	success = success && getTxRxParams();
    pb->increaseProgressBar();
	success = success && getFreqs();
    pb->increaseProgressBar();
    success = success && getBiasSetting();
    success = success && getPowerDownMode();
	if (success)
	{
		ui.infoEdit->setText("-- Successfully read information from chip --");
		showOK();
	}

	pb->increaseProgressBar();
	pb->hide();

    if (m_ph->getHardwareId() == ReaderInterface::Radon)
    {
        ui.antBox->setItemText(0, "Antenna 1");
        ui.antBox->setItemText(1, "Antenna 2");
    }
    if (m_ph->getHardwareId() != ReaderInterface::Newton)
    {
        ui.w_G8G16->hide();
        ui.l_extPASett->hide();
    }
    else
    {
        quint8 g8 = 0;
        quint8 g16 = 0;
        m_ph->changePASetting( g8, g16, false);
        ui.cb_G8->setChecked(g8);
        ui.cb_G16->setChecked(g16);
    }
    delete pb;

}

void CReaderConfigDialog::tryAccept()
{
	QDialog::accept();
}

void CReaderConfigDialog::setInventoryInterval()
{
	clearInfo();
	m_ph->setIntervalTime(ui.inventoryIntervalBox->value());
	ui.infoEdit->setText("-- Inventory Interval Time set to " + QString::number(m_ph->intervalTime()) + " --");
	showOK();
}

void CReaderConfigDialog::setAntennaPower()
{
	clearInfo();
    bool power = ui.antennaPowerCheckBox->isChecked();
    bool eval = ui.antennaPowerEvalCheckBox->isChecked();
    if (!power)     // if we turn off antenna power, we enable the automatic regulation again
    {
        eval = false;
        ui.antennaPowerEvalCheckBox->setChecked(false);
    }
    QString onOff = (power)?"on":"off";
    QString enabled = (eval)?"disabled":"enabled";
	if(m_ph->setAntennaPower(power, eval) == ReaderInterface::OK){
		ui.infoEdit->setText("-- Antenna Power set " + onOff + ", power regulation " + enabled + " --");
		showOK();
	}
	else{
		ui.infoEdit->setText("-- Could not set Antenna Power " + onOff + " --");
		showNOK();
	}
}

int CReaderConfigDialog::changeProfile(ReaderInterface *ph, QString profileName)
{
   int profileIndex = ui.hoppingBox->findText(profileName);

   if (profileIndex < 0)
       return profileIndex;

   ui.hoppingBox->setCurrentIndex(profileIndex);
   if (m_ph == NULL)
       m_ph = ph;
   setFrequency();
   return 0;
}

void CReaderConfigDialog::setFrequency()
{
	QList<ulong> freqs;

	ui.infoEdit->setText("");
	freqs = getFrequencyList();
	uploadFrequencyList(freqs);
}


QList<ulong> CReaderConfigDialog::getFrequencyList()
{
	QList<ulong> freqs;
	ulong start,end,increment,freq;

	if(ui.startFrequencyBox->value() > ui.endFrequencyBox->value())
	{
		ui.endFrequencyBox->setValue(ui.startFrequencyBox->value());
	}
	start = (ulong) 1000 * ui.startFrequencyBox->value();
	end = (ulong) 1000 * ui.endFrequencyBox->value();
	increment = (ulong) 1000 * ui.incrementBox->value();

    if (0 == increment)
    {
        freqs.append(start);
        return freqs;
    }
	srand(QTime::currentTime().msec());
	for ( freq = start; freq <= end; freq += increment)
	{ /* add to the frequency list */
		int sz = freqs.size()+1;
		freqs.insert(rand()%sz,freq);
	}
	return freqs;
}

QList<ulong> CReaderConfigDialog::getTuneFrequencyList()
{
    QList<ulong> freqs;
    ulong start,end,increment,freq;

    if(ui.tuneStartFreqBox->value() > ui.tuneEndFreqBox->value())
    {
        ui.tuneEndFreqBox->setValue(ui.tuneStartFreqBox->value());
    }
    start = (ulong) 1000 * ui.tuneStartFreqBox->value();
    end = (ulong) 1000 * ui.tuneEndFreqBox->value();
    increment = (ulong) 1000 * ui.tuneIncFreqBox->value();

	srand(QTime::currentTime().msec());
	for ( freq = start; freq <= end; freq += increment)
	{ /* add to the frequency list */
		int sz = freqs.size()+1;
		freqs.insert(rand()%sz,freq);
	}
    return freqs;
}


/*! \brief Uploads tuning parameters to the reader
*/
void CReaderConfigDialog::uploadTuningTable(QList<TuneParameter> tuneParameters)
{
	TuneParameter p;

    //ask user if he want to upload table which is to big
    if (tuneParameters.size() > m_maxTunerTableSize)
    {
        int ret = QMessageBox::question(this, "Tuner table size", QString("The table which should be uploaded has %1 entries, but the reader supports only %2 entries.\n"
        "Do you want to continue? \nEntries which do not fit into the reader table will be disregarded.").arg(tuneParameters.size()).arg(m_maxTunerTableSize),
        QMessageBox::Yes | QMessageBox::No);
        if (ret != QMessageBox::Yes)
        {
            ui.infoEdit->append("-- Aborted tuning table upload --");
            return;
        }
    }

	//delete current tune table
	ui.infoEdit->append("-- Delete current tune table");
    if (m_ph->clearTunerTable() != ReaderInterface::OK)
    {
        ui.infoEdit->append("-- Failed to clear tuning table on reader --");
        showNOK();
        return;
    }
    //start uploading parameters
	foreach( p, tuneParameters )
	{
		if(m_ph->addTunerTableEntry(p.frequency, p.tuneEnable[0], p.cin[0], p.clen[0], p.cout[0], p.iq[0],
							                     p.tuneEnable[1], p.cin[1], p.clen[1], p.cout[1], p.iq[1]) == ReaderInterface::OK)
		{
			ui.infoEdit->append("-- Upload tuning parameters for frequency " + QString("%1").arg(p.frequency) + " --");
			showOK();
		}
		else
        {
			ui.infoEdit->append("-- Failed uploading tuning parameters for frequency " + QString("%1").arg(p.frequency) + " --");
            ui.infoEdit->append("-- Previous frequencies were uploaded successfully --");
			showNOK();
            return;
		}
	}
}

/*! \brief uploads a randomized frequency list to the reader.
*/
void CReaderConfigDialog::uploadFrequencyList( QList<ulong> freqs )
{
    ulong freq;
    bool clear = true;// first clear frequency list on reader

    QList<ulong> randFreq;

    //randomize freq list
    srand(QTime::currentTime().msec());
	foreach( freq, freqs )
	{
        int sz = randFreq.size()+1;
        randFreq.insert(rand()%sz,freq);
    }
    //start uploading frequencies
    ui.infoEdit->append("-- Delete current frequency list");
    foreach( freq, randFreq )
    {
        if(m_ph->addFrequency(clear, freq, ui.hoppingBox->currentIndex()) == ReaderInterface::OK)
		{
            ui.infoEdit->append("-- Added frequency " + QString("%1").arg(freq) + " --");
			showOK();
		}
		else{
            ui.infoEdit->append("-- Could not add frequency " + QString("%1").arg(freq) + " --");
			showNOK();
		}
		clear = false; // we already cleared, now add the frequencies
	}

	setHoppingParams();
}

bool CReaderConfigDialog::getGen2Config()
{
	bool success = true;
	int lf_khz = 40;
	int coding = 2;
	int session = 0;
	bool longPilot = true;
    int tari;
	int qbegin = 4;
	int index;
    int target;
	ReaderInterface::Result res = m_ph->getGen2Config(lf_khz, coding, session, longPilot, tari, qbegin, target);

    if (m_ph->getChipId() == ReaderInterface::AS3980)
    {
        ui.gen2GroupBox->setEnabled(false);
    } 
    else
    {
        ui.gen2GroupBox->setEnabled(true);
    }

	if ( res != ReaderInterface::OK)
	{
		ui.infoEdit->append(QString("-- Error getting gen2Config --"));
		success = false;
		showNOK();
	}

	index = ui.linkBox->findText(QString("%1 kHz").arg(lf_khz));
	if (index == -1) success = false;
	ui.linkBox->setCurrentIndex(index);

	if (coding == 0) index = ui.millerBox->findText("FM0");
	else index = ui.millerBox->findText(QString("Miller %1").arg(coding));
	if (index == -1) success = false;
	ui.millerBox->setCurrentIndex(index);

    if (tari < 0 || tari > 2)
        tari = 0;
    ui.tariBox->setCurrentIndex(tari);

	ui.qbegin->setValue(qbegin);

    if (session>4 || session<0)
    {
        session = 0;
        success = false;
    }
	ui.sessionBox->setCurrentIndex(session);
	
	ui.pilotToneBox->setChecked(longPilot);
    ui.cB_Gen2target->setCurrentIndex(target);
	return success;
}

bool CReaderConfigDialog::getTxRxParams()
{
	unsigned char ant;
    signed char sens;
	bool success = false;
	ReaderInterface::Result res = m_ph->setGetTxRxParams(sens, false, ant, false);

	if (res != ReaderInterface::OK)
	{
		ui.infoEdit->setText("-- Could not read tx rx params --");
		showNOK();
		success = false;
		return success;
	}
	success = true;

    ui.sensSlider->setValue(sens);

    if (m_ph->getReaderAntennaSwitch())
        ui.antBox->setCurrentIndex(ant-1);
    //if (id.startsWith("ROLAND") || id.startsWith("ARNIE"))
    //{
    //    ui.levelSlider->setValue((des+5)/10);
    //}
    quint8 regVal;
    res = m_ph->readRegister(0x00, regVal);
    if(res != ReaderInterface::OK)
    {
        ui.infoEdit->setText("-- Failed reading register 0x00 --");
        showNOK();
        return false;
    }
    if (regVal & 0x04)
        ui.agcModeBox->setChecked(true);
    else
        ui.agcModeBox->setChecked(false);

	return success;
}

bool CReaderConfigDialog::getOutputLevel()
{
	bool success = false;
	signed int coarse, fine;
	quint8 in, coarseSetting;

	ReaderInterface::Result res = m_ph->readRegister(0x15, in);

	if (res != ReaderInterface::OK)
	{
		ui.infoEdit->setText("-- Could not read output level --");
		showNOK();
		success = false;
		return success;
	}
	success = true;
	coarseSetting = (in & 0x18) >> 3;
	fine = in & 0x07;
    switch (coarseSetting)
    {
    case 0:
        coarse = 0;
    	break;
    case 1:
        coarse = -8;
        break;
    case 2:
        coarse = -12;
        break;
    default:
        coarse = 0;
    }

	ui.levelSlider->setValue(coarse-fine);
	ui.levelLabel->setText(QString("%1-%2=%3").arg(coarse).arg(fine).arg(coarse-fine));

	return success;
}

bool CReaderConfigDialog::getFreqs()
{
	ulong start, stop;
	ushort idleTime, listenTime, allocationTime;
	signed char rssi;
	uchar numFreqs, profile, numFreqsGui;

	ReaderInterface::Result res = m_ph->getFrequencies( profile, start, stop, numFreqs, numFreqsGui);
	if ( res != ReaderInterface::OK )
	{
		ui.infoEdit->setText("-- Could not read frequencies --");
		showNOK();
		return false;
	}
    res = m_ph->getHoppingParams(listenTime, allocationTime, idleTime, rssi);
    if ( res != ReaderInterface::OK )
    {
        ui.infoEdit->setText("-- Could not read frequency hopping parameters --");
        showNOK();
        return false;
    }
	if(profile != 0)
	{
		ui.hoppingBox->setCurrentIndex(profile);
	}
	else
	{
		ui.startFrequencyBox->setValue(start / 1000.0);
		ui.endFrequencyBox->setValue(stop / 1000.0);
		ui.idleTimeBox->setValue(idleTime);
		ui.listenTimeBox->setValue(listenTime);
		ui.allocationTimeBox->setValue(allocationTime);
		ui.rssiThresholdBox->setValue(rssi);
		if (numFreqs > 1)
		{
			ui.incrementBox->setValue((ui.endFrequencyBox->value() - ui.startFrequencyBox->value())/(numFreqs-1));
		}
	}
    quint8 reg17, refDivIndex;
    if (m_ph->readRegister(0x17, reg17) != ReaderInterface::OK)
    {
        ui.infoEdit->append("Failed reading register 0x17");
        showNOK();
        return false;
    }
    refDivIndex = 0x07 - ((reg17 >> 4) & 0x07);
    ui.refDividerBox->setCurrentIndex(refDivIndex);

	return true;
}

void CReaderConfigDialog::setGen2Configuration()
{
	bool ok;
    int tari = 0;
	int session = 0;
	bool longPilot = true;
	int qbegin = 4;
    int target = 0;
	int lf_khz = ui.linkBox->currentText().split(" ").at(0).toInt();
	int coding = ui.millerBox->currentText().right(1).toInt(&ok);
	if (!ok) coding = 2;

    tari = ui.tariBox->currentIndex();
	qbegin = ui.qbegin->value();
    target = ui.cB_Gen2target->currentIndex();
	//session = ui.sessionBox->currentText().right(1).toInt(&ok);
    QString s(ui.sessionBox->currentText());
    s = s.split(QRegExp("[\\(S\\)]"),QString::SkipEmptyParts).last();
    session = s.toInt(&ok);
	if (!ok) session = 4;

	longPilot = ui.pilotToneBox->isChecked();

	ReaderInterface::Result res = m_ph->setGen2Config(lf_khz, coding, session, longPilot, tari, qbegin,target);

	if ( res == ReaderInterface::OK)
	{
		ui.infoEdit->setText(QString("-- Set specified configuration : lf=%1 cod=%2 session=%3 pilot=%4 tari=%5 qbegin=%6 --").arg(lf_khz).arg(coding).arg(session).arg(longPilot).arg(tari).arg(qbegin));
		showOK();
	}
	else
	{
		ui.infoEdit->setText(QString("-- Could not specified configuration : lf=%1 cod=%2 session=%3 pilot=%4 tari=%5 qbegin=%6 --").arg(lf_khz).arg(coding).arg(session).arg(longPilot).arg(tari).arg(qbegin));
		showNOK();
	}
	return;
}

void CReaderConfigDialog::setTagMode()
{
	ReaderInterface::Result res = ReaderInterface::Error;
	if(ui.tagModeBox->currentIndex() == 0){
        QString name;
        m_ph->getReaderID(name);
        if (ui.linkBox->currentIndex() >= 4 && name.contains("fermi", Qt::CaseInsensitive))    // >=320kHz
        {
            ui.infoEdit->setText("Fast inventory rounds are not supported for BLF > 250kHz on Fermi.");
            ui.tagModeBox->setCurrentIndex(1);
            return;
        }
		res = m_ph->setTagType(ReaderInterface::TAG_GEN2_FAST);
        m_ph->setTIDEnabled(false);
        ui.cB_SelectMode->setCurrentIndex(0);
        ui.cB_SelectMode->setEnabled(false);
	}
	else if(ui.tagModeBox->currentIndex() == 1){
		res = m_ph->setTagType(ReaderInterface::TAG_GEN2);
        m_ph->setTIDEnabled(false);
        ui.cB_SelectMode->setCurrentIndex(0);
        ui.cB_SelectMode->setEnabled(false);
	}
    else if(ui.tagModeBox->currentIndex() == 2){
        res = m_ph->setTagType(ReaderInterface::TAG_GEN2_TID);
        m_ph->setTIDEnabled(true);
        ui.cB_SelectMode->setCurrentIndex(1);
        ui.cB_SelectMode->setEnabled(true);
    }
	else if(ui.tagModeBox->currentIndex() == 5)// not implemented at the moment
	{
		res = m_ph->setTagType(ReaderInterface::TAG_ISO6B);
	}
	else
		return;

	if(res == ReaderInterface::OK)
	{
		ui.infoEdit->append("-- Set TagType to " + ui.tagModeBox->currentText() + " --");
		showOK();
		return;
	}
	else
	{
		ui.infoEdit->append("-- Could not set TagType to " + ui.tagModeBox->currentText() + " --");
		showNOK();
		return;
	}
}

void CReaderConfigDialog::autoAckChanged()
{
    ReaderInterface::Result res;
    res = m_ph->setAutoAckMode(ui.autoAckBox->isChecked());
    if(res == ReaderInterface::OK)
    {
        if (ui.autoAckBox->isChecked())
            ui.infoEdit->setText("-- Enabled autoACK mode --");
        else
            ui.infoEdit->setText("-- Disabled autoACK mode --");
        showOK();
        return;
    }
    else if(res == ReaderInterface::NA)
    {
        ui.infoEdit->setText("-- Could not enable autoACK mode, not supported --");
        showNOK();
        return;
    }
    else
    {
        ui.infoEdit->setText("-- Could not enable autoACK mode --");
        showNOK();
        return;
    }
}

void CReaderConfigDialog::agcModeChanged()
{
    ReaderInterface::Result res;
    quint8 regVal;
    res = m_ph->readRegister(0x00, regVal);
    if(res != ReaderInterface::OK)
    {
        ui.infoEdit->setText("-- Failed reading register 0x00 --");
        showNOK();
        return;
    }
    regVal &= (~0x04);
    if (ui.agcModeBox->isChecked())
        regVal |= 0x04;
    res = m_ph->writeRegister(0x00, regVal);
    if(res != ReaderInterface::OK)
    {
        ui.infoEdit->setText("-- Failed writing register 0x00 --");
        showNOK();
        return;
    }
    if (ui.agcModeBox->isChecked())
        ui.infoEdit->setText("-- AGC is on --");
    else
        ui.infoEdit->setText("-- AGC is off --");
    showOK();
}

void CReaderConfigDialog::sendCommand()
{
	ReaderInterface::Result res;
	QString s = ui.commandBox->currentText();
	quint8 cmd = s.mid(s.length()-2,2).toInt(0,16);

	res = m_ph->writeRegister(cmd, 0);

	if (res != ReaderInterface::OK)
	{
		ui.infoEdit->setText(QString("-- Could not send command 0x%1 --").arg(cmd,0,16));
		showNOK();
		return;
	}
	else
	{
		ui.infoEdit->setText(QString("-- Sent command 0x%1 --").arg(cmd,0,16));
		showOK();
		return;
	}
}

void CReaderConfigDialog::setLevel()
{
    showOK();
    ui.infoEdit->setText("");
    executeSetLevel();
}

bool CReaderConfigDialog::executeSetLevel()
{
	ReaderInterface::Result res;
	quint8 in;
    quint8 out;

	//m_ph->getReaderID(s);
	//if (!(s.startsWith("ROLAND") || s.startsWith("ARNIE")))
	//{
	res = m_ph->readRegister(0x15, in);

	if (res != ReaderInterface::OK)
	{
        ui.infoEdit->append("-- Could not read 0x15 register --");
        showNOK();
        return false;
	}
	out = in & 0xe0;
	out = out | (m_tx_lev_coarse<<3) | m_tx_lev_fine;

	res = m_ph->writeRegister(0x15, out);

	if (res != ReaderInterface::OK)
	{
        ui.infoEdit->append("-- Could not set 0x15 register --");
		showNOK();
		return false;
	}
    QByteArray bin, bout;
    bin.append(in);
    bout.append(out);
    ui.infoEdit->append("-- Set 0x15 register from 0x" + bin.toHex() + " to 0x" + bout.toHex() + " --");
	showOK();
	//}
	//else
	//{
	//	short des,des2,act;
	//	unsigned char ant;
 //       signed char sens;
	//	des = 10 * this->ui.levelSlider->value();
	//	des2 = des;
	//	res = m_ph->setGetTxRxParams(sens, false, ant, false, des2, true, act);
	//	if (res != ReaderInterface::OK)
	//	{
	//		ui.infoEdit->append("-- Could not set output level --");
	//		showNOK();
	//		return false;
	//	}
	//	ui.infoEdit->append(QString("-- Setting output level to %1 dBm resulted in %2 dBm.")
	//		.arg(des/10.0).arg(des2/10.0));
	//	ui.levelSlider->setValue((des2+5)/10);
	//	showOK();
	//}
    return true;
}

void CReaderConfigDialog::setAntenna()
{
	ReaderInterface::Result res;
	unsigned char ant;
    signed char sens;
    QStringList sl;
	
	//QString s(ui.antBox->currentText());
	//sl = s.split(QRegExp("[\\(\\)]"),QString::SkipEmptyParts);
 //   if(sl.size() == 0 )
 //   {
 //       ui.infoEdit->setText("-- Could not set antenna --");
 //       showNOK();
 //       return;
 //   }
 //   s = sl.last();

 //	ant = s.toInt();
    ant = ui.antBox->currentIndex() + 1;
	res = m_ph->setGetTxRxParams(sens, false, ant, true);
	if (res != ReaderInterface::OK)
	{
		ui.infoEdit->setText("-- Could not set antenna --");
		showNOK();
		return;
	}
	ui.infoEdit->setText(QString("-- Set antenna to %1")
		.arg(ant));

    if (ant == 1)
    {
        ui.tuneAntenna1Box->setChecked(true);
        ui.tuneAntenna2Box->setChecked(false);
    }
    else if (ant == 2)
    {
        ui.tuneAntenna1Box->setChecked(false);
        ui.tuneAntenna2Box->setChecked(true);
    }
    else
    {
    }

	showOK();
}

void CReaderConfigDialog::setRssiMode()
{
    ReaderInterface::RssiModes rssiMode;
    QStringList sl;
    bool ok;
    
    QString s(ui.rssiBox->currentText());
    sl = s.split(QRegExp("[\\(\\)]"), QString::SkipEmptyParts);
    if(sl.size() == 0 )
    {
        ui.infoEdit->setText("-- Could not set rssi mode --");
        showNOK();
        return;
    }
    s = sl.last();

    rssiMode = static_cast<ReaderInterface::RssiModes>(s.toInt(&ok, 16));
    if (!ok)
    {
        ui.infoEdit->setText("-- Could not set rssi mode --");
        showNOK();
        return;
    }

    m_ph->setRssiMode(rssiMode);
    ui.infoEdit->setText(QString("-- Set rssi mode to 0x%1 --").arg(rssiMode));
    showOK();
}

void CReaderConfigDialog::levelSliderChanged(int val)
{
	QString text;
	int v=val;
	QString s;

	m_ph->getReaderID(s);

	//if (!(s.startsWith("ROLAND") || s.startsWith("ARNIE")))
	//{
	m_tx_lev_coarse = 0;

	if (val <= -12)
	{
		text.append("-12");
		v += 12;
		m_tx_lev_coarse = 2;
	}
	else if (val <= -8)
	{
		text.append("-8");
		v += 8;
		m_tx_lev_coarse = 1;
	}
	m_tx_lev_fine = -v;
	if(v==0) text.append("+");
	text.append(""+QString::number(v));
	text.append("="+QString::number(val));
	//}
	//else
	//{
	//	text = QString("%1 dBm").arg(val);
	//}

	ui.levelLabel->setText(text);

    if (!ui.levelSlider->isSliderDown())        //if value changed and slider is not clicked (pressed down) anymore, set new level
        setLevel();
}

void CReaderConfigDialog::sensSliderChanged(int val)
{
	this->ui.sensLText->setText(QString("%1 dBm").arg(val));
    if (!ui.sensSlider->isSliderDown())        //if value changed and slider is not clicked (pressed down) anymore, set new level
        setSensitivity();

}

void CReaderConfigDialog::setHoppingParams()
{
	ushort idleTime = ui.idleTimeBox->value();
	ushort listenTime = ui.listenTimeBox->value();
	ushort allocationTime = ui.allocationTimeBox->value();
    signed char rssi = ui.rssiThresholdBox->value();

	ReaderInterface::Result res = m_ph->setHoppingParams(listenTime, allocationTime, idleTime, rssi);

	ui.infoEdit->append(QString("Set idle=%1 ms listen=%2 ms sending=%3 ms rssi=%4")
        .arg(idleTime).arg(listenTime).arg(allocationTime).arg(rssi));
	if (res == ReaderInterface::OK)
	{
		showOK();
	}
	else
	{
		showNOK();
	}
}
void CReaderConfigDialog::displayPlot(double* ivals, double *qvals, double *svals, double *freqs, int size)
{
	QwtPlot *plot  = new QwtPlot();
	QwtPlotCurve icurve("I"),qcurve("Q"),scurve("dBm");
	QDialog plotDiag(this);
    double min, max;
    bool autoScale = true;

    QwtText iq("I, Q");
    QwtText dbm("dBm");
    QColor dbmColor(231, 38, 20);
    dbm.setColor(dbmColor);
    plot->enableAxis(QwtPlot::yLeft, true);
    plot->setAxisTitle(QwtPlot::yLeft, dbm);
    plot->enableAxis(QwtPlot::yRight, true);
    plot->setAxisTitle(QwtPlot::yRight, iq);

    QPalette p = plot->axisWidget(QwtPlot::yLeft)->palette();
    p.setColor(QPalette::WindowText, dbmColor);
    p.setColor(QPalette::Text, dbmColor);
    plot->axisWidget(QwtPlot::yLeft)->setPalette(p);

	QBoxLayout layout(QBoxLayout::LeftToRight);
	icurve.setData(freqs,ivals,size); icurve.setPen(QPen(Qt::DotLine));
    icurve.setYAxis(QwtPlot::yRight);
	qcurve.setData(freqs,qvals,size); qcurve.setPen(QPen(Qt::DashLine));
    qcurve.setYAxis(QwtPlot::yRight);
	scurve.setData(freqs,svals,size); scurve.setPen(QPen(dbmColor));
    scurve.setYAxis(QwtPlot::yLeft);
	
    QPen pen = scurve.pen();
    pen.setWidthF(pen.widthF() + 2.0);
    pen.setCosmetic(true);
    scurve.setPen(pen);

    icurve.attach(plot); qcurve.attach(plot); scurve.attach(plot);
	layout.addWidget(plot);
	plotDiag.setLayout(&layout);

	plot->insertLegend(new QwtLegend(NULL), QwtPlot::RightLegend);
	plot->replot();
    // fix scaling to show not too small y-areas which than looks like bad tuning
    min = plot->axisScaleDiv(QwtPlot::yLeft)->lowerBound();
    max = plot->axisScaleDiv(QwtPlot::yLeft)->upperBound();
    if (max < 3.0)
    {
        max = 3.0;
        autoScale = false;
    }
    if (min > -30.0)
    {
        min = -30.0;
        autoScale = false;
    }
    if (!autoScale)
    {
        plot->setAxisScale(QwtPlot::yLeft, min, max);
        plot->replot();
    }

	plotDiag.exec();
	icurve.detach(); qcurve.detach(); scurve.detach();
	delete plot; 
}

void CReaderConfigDialog::sweepFrequencys()
{
	ulong startFreq = ui.sweepStartSpinBox->value() * 1000;
	ulong stopFreq = ui.sweepStopSpinBox->value() * 1000;
	ulong steps = ui.sweepStepSpinBox->value() * 1000;
	ulong size = (stopFreq-startFreq)/steps + 1;
	double *ivals = new double[size];
	double *qvals = new double[size];
	double *svals = new double[size];
	double *freqs = new double[size];
	double G;
	int i;
    ulong freq;
    int ichannel, qchannel, dBm;
	bool rssi = ui.sweepModeBox->currentIndex() == 0;
	QString mode = (rssi)?"rssi":"reflected power";
    // Give Register 22 a right value if required
    signed char sens;
    quint8 reg04,reg0A, reg22, reg15;
    quint8 reg04Orig,reg0AOrig;
    qint8 sensitivity;
    unsigned char curAntenna;

    if (rssi)
    {
        G = m_ph->getG_general();
    }
    else
    {

        if (m_ph->getHardwareId() == ReaderInterface::Radon)
        { /* For Radon we have to limit output power and sensitivity, otherwise we will see wraparounds of measured reflected power */
            sens = -68;

            ui.infoEdit->append(QString("Radon: setting reg15: 0x0a, sensitivity -68"));
            m_ph->readRegister(0x15, reg15);
            m_ph->writeRegister(0x15, 0x0a); /* -10 */
            m_ph->setGetTxRxParams(sensitivity, false, curAntenna, false);
            m_ph->setGetTxRxParams(sens, true, curAntenna, false);
        }

        m_ph->readRegister(0x04, reg04);
        m_ph->readRegister(0x0A, reg0A);
        m_ph->readRegister(0x22, reg22);
        reg04Orig = reg04;
        reg0AOrig = reg0A;
        reg0A &= 0x03;// take for comparison only the last bits
        reg04 &= 0xF0;// take for comparison only the first bits

        if (m_ph->getHardwareId() == ReaderInterface::Fermi || m_ph->getHardwareId() == ReaderInterface::Radon )
        { /* Only these two readers have balanced input mixer */
            if (reg0A == 0x00)
            {
                if (reg04 != 0x00) m_ph->writeRegister(0x04, 0x02);
            }
            else if (reg0A == 0x01)
            {
                if (reg04 != 0x00) m_ph->writeRegister(0x04, 0x02);
            }
            else if (reg0A == 0x02)
            {
                if ( (reg04 != 0x40) && (reg04 != 0x00) ) m_ph->writeRegister(0x04, 0x02);
            }
            else
            {
                ui.infoEdit->append("Error Register 0x0A has the wrong value");
                goto exit;
            }
        }
        else // Take Femto Vals
        {
            if (reg0A == 0x00)
            {
                if (reg22 != 0x17) m_ph->writeRegister(0x22, 0x17);
            }
            else if (reg0A == 0x01)
            {
                if (reg22 != 0x14) m_ph->writeRegister(0x22, 0x14);
            }
            else if (reg0A == 0x03)
            {
                if ( (reg22 != 0x12) && (reg22 != 0x00) ) m_ph->writeRegister(0x22, 0x00);
            }
            else
            {
                ui.infoEdit->append("Error Register 0x0A has the wrong value");
                goto exit;
            }
        }

        G = m_ph->getG_rfp();
    }

	if(stopFreq < startFreq)
	{
		QMessageBox::critical(this, "Error", "The start value for the sweep must be lower than the stop value!");
		goto exit;
	}

	ui.infoEdit->setText("-- Starting sweep with " + mode + " --");
	for(i=0, freq = startFreq
		; freq <= stopFreq
		; freq += steps,i++)
	{
		ReaderInterface::Result res = ReaderInterface::Error;
        int useTT = ui.cbUseTuningTable->isChecked();
        if (rssi)
            res = m_ph->getRssi(freq, ichannel, qchannel, dBm);
        else
            res = m_ph->getReflectedPower(freq, useTT, ichannel, qchannel); // we want the tuning table values (if available) applied to the frequencies
		if(res != ReaderInterface::OK){
			ui.infoEdit->append(QString::number(freq)+ "; ");
			continue;
		}
		ivals[i] = ichannel;
		qvals[i] = qchannel;
        if (rssi)
            svals[i] = dBm;
        else
		{
			svals[i] = sqrt(ivals[i]*ivals[i]+qvals[i]*qvals[i]);
			if (svals[i] == 0) svals[i] = 0.5;
			svals[i] = 20*log10(svals[i]/G);
		}

		freqs[i] = freq;
		if (rssi)
			ui.infoEdit->append(QString::number(freq)+ "; " + QString("i=%1 q=%2, total %3 dBm")
								.arg(ichannel)
								.arg(qchannel)
			                    .arg(dBm)
								);
		else
			ui.infoEdit->append(QString::number(freq)+ "; " + QString("%1; %2").arg(ichannel).arg(qchannel));

		qApp->processEvents();
	}
	showOK();
	displayPlot(ivals,qvals,svals,freqs,size);
exit:

    if (!rssi)
    {
        m_ph->writeRegister(0x04, reg04Orig); // Set Register 04 back
        m_ph->writeRegister(0x22, reg22); // Set Register 22 back

        if (m_ph->getHardwareId() == ReaderInterface::Radon)
        { /* For Radon we have to limit output power and sensitivity, restore old values */
            m_ph->writeRegister(0x15, reg15);
            m_ph->setGetTxRxParams(sensitivity, true, curAntenna, false);
            ui.infoEdit->append(QString("Radon: restored reg15: 0x%1, sensitivity %2").arg(reg15, 2, 16, QChar('0')).arg((int)sensitivity,0,10));
        }
    }

	delete[] ivals; delete[] qvals; delete[] svals; delete[] freqs;
}

void CReaderConfigDialog::openRFPDialog(void)
{
    double freq = (ui.endFrequencyBox->value() + ui.startFrequencyBox->value()) / 2.0;

	CReflectedPower diag(this);
    diag.setWindowModality(Qt::WindowModal);
	diag.exec(m_ph, freq);

    //antenna might have been changed in the reflected power dialog, update local combobox
    ui.antBox->blockSignals(true);
    ui.antBox->setCurrentIndex(diag.currentAntenna());
    ui.antBox->blockSignals(false);
}

void CReaderConfigDialog::setSensitivity(void)
{
    unsigned char ant;
	signed char sens = this->ui.sensSlider->value();
	signed char sens_old = sens;

	ReaderInterface::Result res = m_ph->setGetTxRxParams(sens, true, ant, false);

	if ( res == ReaderInterface::OK)
	{
		ui.infoEdit->setText(QString("-- Desired sensitivity %1 dBm resulted in %2  --").arg(sens_old).arg(sens));
		showOK();
		this->ui.sensSlider->setValue(sens);
	}
	else
	{
		ui.infoEdit->setText(QString("-- Could not set sensitivity to %1 --").arg(sens_old));
		showNOK();
	}
}

void CReaderConfigDialog::continuousSend(void)
{
	ushort timeout_ms = ui.timeoutBox->value();
	double freq = ui.frequencyBox->value();
    uchar random = ui.pseudoRandomCheckBox->isChecked() ? 1 : 0;
    
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	ReaderInterface::Result res = m_ph->continuousSend(freq*1000,timeout_ms, random);
    SleepThread::msleep(timeout_ms);
    QApplication::restoreOverrideCursor();

	if (timeout_ms == 0)
	{
		ui.infoEdit->append(QString("Modulating continuously until next USB command on freq %2")
			.arg(freq));
	}
	else
	{
		ui.infoEdit->append(QString("Modulating continuously for %1 ms on freq %2")
			.arg(timeout_ms).arg(freq));
	}
	if (res == ReaderInterface::OK)
	{
		showOK();
	}
	else
	{
		showNOK();
	}

}

void CReaderConfigDialog::setHopper( const QString & text)
{
	QStringList sl = profiles.value(text);
	if (sl.empty()) return;
	QList<double> freqs;
    double freqStart, freqEnd, freqIncr;
    int hopIndex;
	foreach(QString s, sl)
	{
		s.toDouble();
		freqs.append(s.toDouble());
	}
	for ( int i = freqs.length(); i<7; i++)
	{
		freqs.append(55.5);
	}
    
    hopIndex = ui.hoppingBox->currentIndex();   //store current index of hopbox, because it will be changed in oneFreqChanged()

    if (freqs.size() < 8)
    {
        QMessageBox::warning(this, "profile.csv", "Found incompatible version of profile.csv!");
        return;
    }
    freqStart = freqs.takeFirst();
    freqEnd = freqs.takeFirst();
    freqIncr = freqs.takeFirst();
	ui.startFrequencyBox->setValue(freqStart);
	ui.endFrequencyBox->setValue(freqEnd);
	ui.incrementBox->setValue(freqIncr);
	ui.rssiThresholdBox->setValue(freqs.takeFirst());
	ui.listenTimeBox->setValue(freqs.takeFirst());
	ui.idleTimeBox->setValue(freqs.takeFirst());
	ui.allocationTimeBox->setValue(freqs.takeFirst());
    int refdiv = freqs.takeFirst();  //values are: 25,50,100,125
    ui.refDividerBox->setCurrentIndex(ui.refDividerBox->findData(refdiv));

    ui.hoppingBox->setCurrentIndex(hopIndex);
}

void CReaderConfigDialog::refDividerChanged()
{
    int index = ui.refDividerBox->currentIndex();
    if (index < 0)
        return;
    int ref = ui.refDividerBox->itemData(index).toInt();
    int frequency = ui.startFrequencyBox->value() * 1000;
    unsigned int divisor;
    unsigned int i, j, x, reg_A, reg_B;

    if (m_ph != 0)
    {   //workaround startup
        quint8 reg17, regRefDiv;
        if (m_ph->readRegister(0x17, reg17) != ReaderInterface::OK)
        {
            ui.infoEdit->append("Failed reading register 0x17");
            showNOK();
            return;
        }
        regRefDiv = 0x07 - index;
        reg17 &= 0x8F;
        reg17 |= (regRefDiv << 4);

        if (m_ph->writeRegister(0x17, reg17) != ReaderInterface::OK)
        {
            ui.infoEdit->append("Failed writing to register 0x17");
            showNOK();
            return;
        }
    }

    /* the following code is extracted from FW to get same value for A and B as in FW */
    divisor=frequency/ref;

    i = 0x3FFF & (divisor >> 6); /* division by 64 */
    x = (i<<6) + i;
    if (divisor > x)
    {
        x += 65;
        i++;
    }
    x -= divisor;
    j = i;
    do
    {
        if (x >= 33)
        {
            i--;
            x -= 33;
        }
        if (x >= 32)
        {
            j--;
            x -= 32;
        }
    } while (x >= 32);
    if (x > 16) 
    {            /* subtract 32 from x if it is larger than 16 */
        x -= 32; /* this yields more closely matched A and B values */
        j--;
    }

    reg_A = (i - x);
    reg_B = (j + x);
    /* end of code extracted from FW */

    reg_A &= 0x3FF;
    reg_B &= 0x3FF;
    ui.aBox->blockSignals(true);
    ui.bBox->blockSignals(true);
    ui.aBox->setValue(reg_A);
    ui.bBox->setValue(reg_B);
    ui.aBox->blockSignals(false);
    ui.bBox->blockSignals(false);

    if ((reg_B*32 + reg_A*33) * ref != frequency)
    {
        ui.infoEdit->setText(QString("Current reference divider and A/B values fail to match configured frequency."));
        showNOK();
    }
    else
    {
        ui.infoEdit->setText("");
        showOK();
    }
}

void CReaderConfigDialog::abValueChanged()
{
    int index = ui.refDividerBox->currentIndex();
    if (index < 0)
        return;
    int ref = ui.refDividerBox->itemData(index).toInt();
    double aVal, bVal;
    aVal = ui.aBox->value();
    bVal = ui.bBox->value();
    
    double confFreq = (bVal*32.0 + aVal*33.0) * ref / 1000.0;

    ui.hoppingBox->blockSignals(true);
    ui.startFrequencyBox->blockSignals(true);
    ui.endFrequencyBox->blockSignals(true);
    ui.incrementBox->blockSignals(true);
    ui.hoppingBox->setCurrentIndex(0);
    ui.startFrequencyBox->setValue(confFreq);
    ui.endFrequencyBox->setValue(confFreq);
    ui.incrementBox->setValue(0);
    ui.hoppingBox->blockSignals(false);
    ui.startFrequencyBox->blockSignals(false);
    ui.endFrequencyBox->blockSignals(false);
    ui.incrementBox->blockSignals(false);

    ui.infoEdit->setText("");
    showOK();
}

void CReaderConfigDialog::oneFreqChanged(double)
{
	ui.hoppingBox->setCurrentIndex(0);
    refDividerChanged();
}

void CReaderConfigDialog::oneFreqChanged(int)
{
	ui.hoppingBox->setCurrentIndex(0);
    refDividerChanged();
}


void CReaderConfigDialog::clearInfo()
{
    ui.infoEdit->setStyleSheet("");
	ui.infoEdit->clear();
}

void CReaderConfigDialog::showOK()
{
    ui.infoEdit->setStyleSheet("background-color:lightgreen;");
}

void CReaderConfigDialog::showNOK()
{
    ui.infoEdit->setStyleSheet("background-color:lightcoral;");
}

void CReaderConfigDialog::readProfiles()
{
	QFile qf("profiles.csv");
	QString line;
	QStringList sl;
	if (!qf.open(QIODevice::ReadOnly)) return;
	if(!qf.atEnd()) qf.readLine(); // skip heading row
	while(!qf.atEnd())
	{
		line = qf.readLine();
		sl = line.split(",");
		ui.hoppingBox->addItem(sl.first());
		profiles.insert(sl.takeFirst(),sl);
	}
	ui.hoppingBox->setCurrentIndex(1);
}

void CReaderConfigDialog::tuneFrequencies( void )
{
	//qDebug() << "tuneFrequencies()";

	//start calculating frequency list
    int ichannel, qchannel;
	ulong freq;
	double i, q, absmixdc, pindBm;
	QList<ulong> freqs;
	ui.infoEdit->setText("-- Start tuning --");
    showOK();
    qApp->processEvents();
    if (!ui.tuneAntenna1Box->isChecked() && !ui.tuneAntenna2Box->isChecked())
    {   /* if no antenna is selected for tuning, report error and quit */
        ui.infoEdit->append("-- No Tuning done, because no antenna is selected.");
        showNOK();
        return;
    }

    // ensure that output level is set.
    if (!executeSetLevel())
        return;

	freqs = getTuneFrequencyList();
	uploadFrequencyList(freqs);			// upload the current frequencies in the gui to the reader

	QMap<ulong, TuneParameter> tuneParameters;
	quint8 cinReg, clenReg, coutReg;
	float cinVal, clenVal, coutVal;
	unsigned char curAntenna, initialAntenna;
    signed char sens;
    double G;
    TuneParameter tuneTemp;
    // Give Register 22 a right value if required
    quint8 reg04,reg0A, reg22, reg15;
    quint8 reg04Orig,reg0AOrig;

    qint8 sensitivity;

    if (m_ph->getHardwareId() == ReaderInterface::Radon)
    { /* For Radon we have to limit output power and sensitivity, otherwise we will see wraparounds of measured reflected power */
        sens = -68;

        ui.infoEdit->append(QString("Radon: setting reg15: 0x0a, sensitivity -68"));
        ui.infoEdit->append("Tuning Mode: System Tuner!");
        m_ph->readRegister(0x15, reg15);
        m_ph->writeRegister(0x15, 0x0a); /* -10 */
        m_ph->setGetTxRxParams(sensitivity, false, curAntenna, false);
        m_ph->setGetTxRxParams(sens, true, curAntenna, false);
    }


    if (m_ph->readRegister(0x04, reg04) != ReaderInterface::NoError)
    {
        ui.infoEdit->append("Error Read Register 0x04");
        goto exit;
    }
    if (m_ph->readRegister(0x0A, reg0A) != ReaderInterface::NoError)
    {
        ui.infoEdit->append("Error Read Register 0x0A");
        goto exit;
    }
    if (m_ph->readRegister(0x22, reg22) != ReaderInterface::NoError)
    {
        ui.infoEdit->append("Error Read register 0x22");
        goto exit;
    }
    reg04Orig = reg04;
    reg0AOrig = reg0A;
    reg0A &= 0x03;// take for comparison only the last bits
    reg04 &= 0xF0;// take for comparison only the first bits
    
    if (m_ph->getHardwareId() == ReaderInterface::Fermi || m_ph->getHardwareId() == ReaderInterface::Radon )
    { /* Only these two readers have balanced input mixer */
        if (reg0A == 0x00)
        {
            if (reg04 != 0x00) m_ph->writeRegister(0x04, 0x02);
        }
        else if (reg0A == 0x01)
        {
            if (reg04 != 0x00) m_ph->writeRegister(0x04, 0x02);
        }
        else if (reg0A == 0x02)
        {
            if ( (reg04 != 0x40) && (reg04 != 0x00) ) m_ph->writeRegister(0x04, 0x02);
        }
        else
        {
            ui.infoEdit->append("Error Register 0x0A has the wrong value");
            goto exit;
        }
    }
    else // Take Femto Vals
    {
        if (reg0A == 0x00)
        {
            if (reg22 != 0x17) m_ph->writeRegister(0x22, 0x17);
        }
        else if (reg0A == 0x01)
        {
            if (reg22 != 0x14) m_ph->writeRegister(0x22, 0x14);
        }
        else if (reg0A == 0x03)
        {
            if ( (reg22 != 0x12) && (reg22 != 0x00) ) m_ph->writeRegister(0x22, 0x00);
        }
        else
        {
            ui.infoEdit->append("Error Register 0x0A has the wrong value");
            goto exit;
        }
    }

    
    G = m_ph->getG_rfp();

    m_ph->setGetTxRxParams(sens, false, initialAntenna, false);		/* store initial antenna to set it back at the end
                                                                            and set post pa switch for tuning*/

	/* If we want to use the faster tuning algorithm (1) after the first tuning of a frequency, the frequencies have to be applied
	in a sorted order. Because I am lazy, I'm using a map to get a sorted list. */

	foreach(freq, freqs)
		tuneParameters.insert(freq, tuneTemp);
	freqs = tuneParameters.keys();

    char tuneAlgoChange = 2;
    if (ui.tuneAlgorithmBox->currentIndex() == 1)
        tuneAlgoChange = 1;

    QCheckBox *tuneEnable[2];
    tuneEnable[0] = ui.tuneAntenna1Box;
    tuneEnable[1] = ui.tuneAntenna2Box;
	for(curAntenna=1; curAntenna<=2; curAntenna++)
	{
        if (!tuneEnable[curAntenna-1]->isChecked())
            continue;       // if antenna is not checked do not tune

		//change antenna
		if (m_ph->setGetTxRxParams(sens, false, curAntenna, true) != ReaderInterface::OK)
		{
			ui.infoEdit->append("-- Could not set antenna --");
			showNOK();
            qApp->processEvents();
			continue;
		}
		ui.infoEdit->append(QString("-- Set antenna to %1").arg(curAntenna));
        //qDebug() << QString("-- Set antenna to %1").arg(curAntenna);
        qApp->processEvents();

        m_ph->getReflectedPower(freqs[0], 0, ichannel, qchannel);
        m_ph->startAutoTuner(2, cinReg, clenReg, coutReg);

		QString cValues;
        char tuneAlgo = 2;
        int count = 0;
		foreach(freq, freqs)
		{
			TuneParameter p;
			if (tuneParameters.contains(freq))		//we might have already tuned this frequency for the other antenna
				p = tuneParameters.value(freq);
			//set frequency (mode=2), rssi and profile not required
            if (m_ph->getReflectedPower(freq, 0, ichannel, qchannel) == ReaderInterface::OK)
			{
				p.tuneEnable[curAntenna-1] = true;
				p.frequency = freq;

				//ui.infoEdit->append(QString("-- Tuning frequency %1").arg(freq));
				m_ph->startAutoTuner(tuneAlgo, cinReg, clenReg, coutReg);
                if (++count % 10 == 0)
                    tuneAlgo = 2;       // every 10th frequency is tuned with algorithm 2
                else
                    tuneAlgo = tuneAlgoChange;

				cinVal  = 1.3+cinReg*0.131;
				clenVal = 1.3+clenReg*0.131;
				coutVal = 1.3+coutReg*0.131;
				cValues.sprintf("Cin=%1.2f Clen=%1.2f Cout=%1.2f", cinVal, clenVal, coutVal);
				ui.infoEdit->append(QString("-- Tuning frequency %1 --> ").arg(freq).append(cValues));
                qApp->processEvents();
				if(!(m_ph->getReflectedPower(freq, 0, ichannel, qchannel) == ReaderInterface::OK))
					continue;				//setting frequency failed, stop tuning

				i = ichannel;
				q = qchannel;
				absmixdc = sqrt(i*i+q*q);
				if (absmixdc == 0) absmixdc = 0.5;
				pindBm = 20*log10(absmixdc/G); 

                p.cin[curAntenna-1] = cinReg;
				p.clen[curAntenna-1] = clenReg;
				p.cout[curAntenna-1] = coutReg;
				p.dBm[curAntenna-1] = pindBm;
				p.iq[curAntenna-1] = i*i+q*q;
				tuneParameters.insert(freq, p);
                //qDebug() << "Tuning Vals";
                //qDebug() << "cinReg:" << cinReg << "clenReg:" << clenReg << "coutReg:" << coutReg;
                //qDebug() << "i:"      << i      << "q:"       << q;
                //qDebug() << "dBm:"    << pindBm    << "iq:" << (i*i+q*q) ;
				//qDebug() << "Cin=" << cinVal << "  Clen=" << clenVal << "  Cout=" << coutVal;
			}
			else
			{		//setting frequency failed
				ui.infoEdit->append("-- Could not set Frequency " + QString("%1").arg(freq) + " for tuning --");
                qApp->processEvents();
				showNOK();
			}
		}
	}
	ui.infoEdit->append("-- Tuning finished, writing values to file.");
    qApp->processEvents();

	writeTuningFile(tuneParameters.values(), m_ph->getHardwareId());

    m_ph->writeRegister(0x04, reg04Orig); // Set Register 04 back
    m_ph->writeRegister(0x22, reg22); // Set Register 22 back

	if (m_ph->setGetTxRxParams(sens, false, initialAntenna, true) != ReaderInterface::OK)
		ui.infoEdit->append("-- Could not set antenna back to initial antenna value --");

exit:
    if (m_ph->getHardwareId() == ReaderInterface::Radon)
    { /* For Radon we have to limit output power and sensitivity, restore old values */
        m_ph->writeRegister(0x15, reg15);
        m_ph->setGetTxRxParams(sensitivity, true, curAntenna, false);
        ui.infoEdit->append(QString("Radon: restored reg15: 0x%1, sensitivity %2").arg(reg15, 2, 16, QChar('0')).arg((int)sensitivity,0,10));
    }

}

void CReaderConfigDialog::uploadTuningParameters( void )
{  
	ui.infoEdit->setText("-- Uploading tuning parameters to reader --");

	QList<TuneParameter> tuneParameters;
	ui.infoEdit->append("-- Reading tuning file --");
	readTuningFile(tuneParameters);
	uploadTuningTable(tuneParameters);

    //update the GUI elements showing the frequencies
    ulong minFreq, maxFreq, stepFreq;
    TuneParameter p;
    minFreq = 1e12;
    maxFreq = 0;
    if (tuneParameters.size() > 0)
    {
        foreach( p, tuneParameters )
        {
            if( p.frequency > maxFreq )
                maxFreq = p.frequency;
            if( p.frequency < minFreq )
                minFreq = p.frequency;
        }
        if (tuneParameters.size() == 1)
            stepFreq = 500;
        else
            stepFreq = (maxFreq - minFreq) / (tuneParameters.size() - 1);
        ui.startFrequencyBox->setValue((double)minFreq / 1000);
        ui.endFrequencyBox->setValue((double)maxFreq / 1000);
        ui.incrementBox->setValue((double)stepFreq / 1000);
    }
}


void CReaderConfigDialog::writeTuningFile( const QList<TuneParameter> tuneParameters, ReaderInterface::HardwareId hwType )
{
    QString fileName;
    if (m_ph->getHardwareId() == ReaderInterface::Radon)
    {
        fileName = QFileDialog::getSaveFileName(NULL, tr("Save Tuning File"), "Radon_SystemTuner_", tr("CSV Files (*.csv)"));
    }
    else
    {
	    fileName = QFileDialog::getSaveFileName(NULL, tr("Save Tuning File"), "", tr("CSV Files (*.csv)"));
    }
	if (fileName.isEmpty())
		return;

	QFile f(fileName);
	if (f.open(QIODevice::WriteOnly))
	{
		QTextStream ts( &f );
		QStringList line;
		TuneParameter p;

		ts << "# Tuning file for AS399x\n";
        if (m_ph->getHardwareId() == ReaderInterface::Radon)
        {
           ts << QString("# %1").arg("Reader Type: Radon; Tuning Mode: System Tuner \n");
        }
        
		ts << "# Frequency;TuneEnableAntenna1;Cin1;Clen1;Cout1;i*i+q*q;ReflectedPower1;TuneEnableAntenna2;Cin2;Clen2;Cout2;i*i+q*q;ReflectedPower2 \n";
		foreach(p, tuneParameters)
		{
			line.clear();
			line << QString::number(p.frequency);
			line << QString::number(p.tuneEnable[0]);
			line << QString::number(p.cin[0]) << QString::number(p.clen[0]) << QString::number(p.cout[0]);
			line << QString::number(p.iq[0]);
			line << QString::number(p.dBm[0]);
			line << QString::number(p.tuneEnable[1]);
			line << QString::number(p.cin[1]) << QString::number(p.clen[1]) << QString::number(p.cout[1]);
			line << QString::number(p.iq[1]);
			line << QString::number(p.dBm[1]);
			ts << line.join(";") + "\n";
		}
		f.close();
	}
}

void CReaderConfigDialog::readTuningFile( QList<TuneParameter>& tuneParameters )
{
	QString fileName = QFileDialog::getOpenFileName(NULL, tr("Open Tuning File"), "", tr("CSV Files (*.csv)"));
	if (fileName.isEmpty())
		return;

	QFile f(fileName);
	if (f.open(QIODevice::ReadOnly))
	{
		QTextStream ts( &f );
		QString line;
		QStringList split;
		TuneParameter p;
		bool ok;
		while(!ts.atEnd())
		{
			line = ts.readLine().trimmed();
			if (line.startsWith("#"))
				continue;		//ignore lines which start with # --> comment
			split = line.split(";");
			if (split.size() < 12)
				goto readError;
			p.frequency = split.at(0).trimmed().toULong(&ok);
			if (!ok)
				goto readError;
			//start reading parameters for antenna 1
			p.tuneEnable[0] = split.at(1).trimmed().toUShort(&ok);
			if (!ok)
				goto readError;
			p.cin[0] = split.at(2).trimmed().toUShort(&ok);
			if (!ok)
				goto readError;
			p.clen[0] = split.at(3).trimmed().toUShort(&ok);
			if (!ok)
				goto readError;
			p.cout[0] = split.at(4).trimmed().toUShort(&ok);
			if (!ok)
				goto readError;
			p.iq[0] = split.at(5).trimmed().toULong(&ok);
			if (!ok)
				goto readError;
			p.dBm[0] = split.at(6).trimmed().toDouble(&ok);
			if (!ok)
				goto readError;
			//start reading parameters for antenna 2
			p.tuneEnable[1] = split.at(7).trimmed().toUShort(&ok);
			if (!ok)
				goto readError;
			p.cin[1] = split.at(8).trimmed().toUShort(&ok);
			if (!ok)
				goto readError;
			p.clen[1] = split.at(9).trimmed().toUShort(&ok);
			if (!ok)
				goto readError;
			p.cout[1] = split.at(10).trimmed().toUShort(&ok);
			if (!ok)
				goto readError;
			p.iq[1] = split.at(11).trimmed().toULong(&ok);
			if (!ok)
				goto readError;
			p.dBm[1] = split.at(12).trimmed().toDouble(&ok);
			if (!ok)
				goto readError;

			// reading was successful
			tuneParameters.append(p);
			continue;	// next line
readError:
			ui.infoEdit->append("-- Error reading file in line: " + line);
			continue;
		}
		f.close();
	}
}

void CReaderConfigDialog::calcTuneTableSize()
{
    if (getTuneFrequencyList().size() > m_maxTunerTableSize)
    {
        ui.labelTunerTableSize->setText(QString("<font color='red'>Configured table size > %1</font>").arg(m_maxTunerTableSize));
        ui.labelTunerTableSize->setToolTip(QString("Reader reported max tuner table size of %1, the current configuration requires a larger table. "
            "Tuner table entries > %1 will be discarded.").arg(m_maxTunerTableSize));
    }
    else
    {
        ui.labelTunerTableSize->setText(QString("Max tuner table size: %1").arg(m_maxTunerTableSize));
        ui.labelTunerTableSize->setToolTip(QString("Reader reported max tuner table size of %1.").arg(m_maxTunerTableSize));
    }

}

void CReaderConfigDialog::codingChanged( int index )
{
    if (index == 0 && ui.linkBox->currentIndex() > 1)
    {
        ui.infoEdit->append("Warning: FM0 coding supports only link frequencies up to 160kHz reliably.");
        showNOK();
    }
    if (index == 0 && !ui.pilotToneBox->isChecked())
    {
        ui.pilotToneBox->setChecked(true);
        ui.infoEdit->append("FM0 only supported with pilot tone enabled.");
    }
    if (index == 1 && !ui.pilotToneBox->isChecked())
    {
        ui.pilotToneBox->setChecked(true);
        ui.infoEdit->append("Miller2 only supported with pilot tone enabled.");
    }
}

void CReaderConfigDialog::linkFrequencyChanged( int index )
{
    QString name;
    m_ph->getReaderID(name);
    if (ui.linkBox->currentIndex() >= 4 && ui.tagModeBox->currentIndex() == 0 && name.contains("fermi", Qt::CaseInsensitive))    // >320kHz
    {
        ui.infoEdit->setText("Fast inventory rounds are not supported for BLF > 250kHz on Fermi.");
        ui.tagModeBox->setCurrentIndex(1);
    }

    if (ui.millerBox->currentIndex() == 0 && index > 1)
    {
        ui.infoEdit->append("Warning: FM0 coding supports only link frequencies up to 160kHz reliably.");
        showNOK();
    }
    tariChanged(ui.tariBox->currentIndex());
}

void CReaderConfigDialog::tariChanged( int index )
{
    switch (ui.linkBox->currentIndex())
    {
    case 0: /* 40kHz */
        if (index!=2)
        {
            ui.tariBox->setCurrentIndex(2);     //only 25us allowed
            ui.infoEdit->append("Link frequency 40kHz: only Tari=25us allowed");
        }
        break;
    case 1: /* 160kHz */
        if (index==0)
        {
            ui.tariBox->setCurrentIndex(1);     //only 12.5us and 25us allowed
            ui.infoEdit->append("Link frequency 160kHz: only Tari=25us and Tari=12.5us allowed");
        }
        break;
    case 2: /* 213kHz */
        break;
    case 3: /* 256kHz */
        break;
    case 4: /* 320kHz */
        if (index==2)
        {
            ui.tariBox->setCurrentIndex(1);     //only 6.25us and 12.5us allowed
            ui.infoEdit->append("Link frequency 320kHz: only Tari=12.5us and Tari=6.25us allowed");
        }
        break;
    case 5: /* 640kHz */
        if (index!=0)
        {
            ui.tariBox->setCurrentIndex(0);     //only 6.25us allowed
            ui.infoEdit->append("Link frequency 640kHz: only Tari=6.25us allowed");
        }
        break;
    }
}

void CReaderConfigDialog::pilotChanged( bool checked )
{
    if (!checked)
    {   //check if setting is valid for current coding
        codingChanged(ui.millerBox->currentIndex());
    }
}


void CReaderConfigDialog::superUserMode()
{
    superUser = !superUser;
    if (superUser)
    {
        ui.antennaPowerEvalCheckBox->setHidden(false);
    }
    else
    {
        ui.antennaPowerEvalCheckBox->setHidden(true);
        ui.antennaPowerEvalCheckBox->setChecked(false);
    }
}

void CReaderConfigDialog::biasSettingChanged()
{
    quint8 newSetting;
    quint8 regVal;

    ui.infoEdit->clear();
    showOK();
    ui.infoEdit->append("Setting bias configuration in register 0x22");
    if (m_ph->readRegister(0x22, regVal) != ReaderInterface::OK)
    {
        ui.infoEdit->append("Failed reading register 0x22");
        showNOK();
        return;
    }
    regVal = regVal & 0x3F;

    if (ui.biasBox->currentIndex() > 3)
    {   //something is horribly wrong;
        ui.infoEdit->append("Unsupported value selected");
        showNOK();
        return;
    }

    newSetting = ((static_cast<quint8>(ui.biasBox->currentIndex())) & 0x03) << 6;
    regVal |= newSetting;
    if (m_ph->writeRegister(0x22, regVal) != ReaderInterface::OK)
    {
        ui.infoEdit->append("Failed writing to register 0x22");
        showNOK();
        return;
    }
    ui.infoEdit->append(QString("Successfully wrote 0x%1 to register 0x22").arg(regVal, 0, 16));
}

bool CReaderConfigDialog::getBiasSetting()
{
    quint8 readReg;
    quint8 bias;

    if (m_ph->readRegister(0x22, readReg) != ReaderInterface::OK)
    {
        ui.infoEdit->append("Failed reading register 0x22");
        showNOK();
        return false;
    }

    bias = (readReg >> 6) & 0x03;
    ui.biasBox->blockSignals(true);
    ui.biasBox->setCurrentIndex(bias);
    ui.biasBox->blockSignals(false);

    return true;
}

void CReaderConfigDialog::powerDownModeChanged()
{
    quint8 mode;
    mode = static_cast<quint8>(ui.powerModeBox->currentIndex());

    ui.infoEdit->clear();
    showOK();
    ui.infoEdit->append(QString("Setting power down mode configuration to %1.").arg(mode));

    if (m_ph->setPowerDownMode(mode) != ReaderInterface::OK)
    {
        ui.infoEdit->append("Failed to set power down mode configuration of reader");
        showNOK();
        return;
    }
}

bool CReaderConfigDialog::getPowerDownMode()
{
    quint8 mode;
    
    if (m_ph->getPowerDownMode(mode) != ReaderInterface::OK)
    {
        ui.infoEdit->append("Failed to get power down mode configuration from reader");
        showNOK();
        return false;
    }
    if (mode > ui.powerModeBox->count())
    {
        ui.infoEdit->append(QString("Unknown power down mode (%1) on reader").arg(mode));
        showNOK();
        return false;
    }
    ui.powerModeBox->blockSignals(true);
    ui.powerModeBox->setCurrentIndex(mode);
    ui.powerModeBox->blockSignals(false);

    return true;
}

void CReaderConfigDialog::changeexternalPAGain()
{
    quint8 g8val = static_cast<quint8>(ui.cb_G8->isChecked());
    quint8 g16val = static_cast<quint8>(ui.cb_G16->isChecked());
    ReaderInterface::Result err = m_ph->changePASetting(g8val, g16val, true);
    if (err != ReaderInterface::NoError)
    {
        ui.infoEdit->setText("Error: Change PA Gain");
        showNOK();
    }
    else
    {
        ui.cb_G8->setChecked(g8val);
        ui.cb_G16->setChecked(g16val);
        ui.infoEdit->setText("PA Gain changed");
        showOK();
    }
}

void CReaderConfigDialog::cB_SelectMode(int option)
{
    AmsReader  *amsreader = dynamic_cast<AmsReader*>(m_ph);
    amsreader->setGen2Select(option);
}
