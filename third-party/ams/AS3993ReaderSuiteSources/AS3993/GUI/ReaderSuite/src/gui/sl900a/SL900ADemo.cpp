/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "SL900ADemo.hxx"
#include <QMessageBox>
#include "qdebug.h"
#include <math.h>
#include "ams_colors.h"
#include "globals.h"

SL900ADemo::SL900ADemo ( SL900AMainDialog *parent ) : itsTempcurve("Temperature"), itsBattcurve("Battery Voltage")
    , itsExtUppcurve("ExtremeUpperLimit"), itsUppcurve("UpperLimit"), itsExtLowcurve("ExtremeLowerLimit"), itsLowcurve("LowerLimit")
{
    setupUi(this);
    itsParent = parent;
    // Initialize Member Variables
    itsGen2Res = Gen2ReaderInterface::Gen2_OK;
    itsCoolLogError = CoolLogError::CoolLogNoError; 
    
    itsListDirectTempMeasure.clear();
    
    memset(&itsLimits,        0x0, sizeof(itsLimits));
    memset(&itsLimitsCounter, 0x0, sizeof(itsLimitsCounter));
    memset(&itsSystemStatus,  0x0, sizeof(itsSystemStatus));
    memset(&itsDelayTime,     0x0, sizeof(itsDelayTime));
    memset(&itsUserData,      0x0, sizeof(itsUserData));
    memset(&itsLogMode,       0x0, sizeof(itsLogMode));
    memset(&itsShelfLife,     0x0, sizeof(itsShelfLife));
    itsActiveFlagFailCounter = 0;
    itslogInterval = 0;
    memset(&itsStatusFlags,   0x0, sizeof(itsStatusFlags));
    itsBattype = SL900Battype::Bat1V5;
    itsCurrentShelfLife=0;


    itsListMeasurementValues.clear();
    itsLastReadPointer = 0;
    clearCurves();

    //Plot Setup
    QColor red(AMSCOLOR_RED);
    QColor blue(AMSCOLOR_BLUE_70);
    QColor viola(AMSCOLOR_VIOLA);  
    QColor green(AMSCOLOR_GREEN);  
    QFont axisfont;
    axisfont.setFamily(QString::fromUtf8("Arial"));

    plot->setAxisTitle(QwtPlot::xBottom, "Samples");
    plot->setAxisTitle(QwtPlot::yLeft, "Temperature [�C]");
    plot->setAxisTitle(QwtPlot::yRight, "Battery Voltage [V]");
    plot->setAxisScale(QwtPlot::yLeft, 0 , 51 , 10.0);
    plot->setAxisScale(QwtPlot::yRight, 0 , 5 , 1.0);
    plot->setAxisFont(QwtPlot::xBottom, axisfont);
    plot->setAxisFont(QwtPlot::yLeft, axisfont);
    plot->setAxisFont(QwtPlot::yRight, axisfont);

    itsTempcurve.attach(plot);
    itsExtUppcurve.attach(plot);
    itsUppcurve.attach(plot);
    itsLowcurve.attach(plot);
    itsExtLowcurve.attach(plot);
    itsBattcurve.attach(plot);

    QPen tempPen(blue);
    QPen batVPen(viola);
    QPen extremPen(red);
    QPen limPen(green);

    tempPen.setWidth(2);
    batVPen.setWidth(2);
    limPen.setStyle(Qt::DashLine);
    extremPen.setStyle(Qt::DashLine);

    itsTempcurve.setPen(tempPen);
    itsExtUppcurve.setPen(extremPen);
    itsUppcurve.setPen(extremPen);
    itsLowcurve.setPen(limPen);
    itsExtLowcurve.setPen(extremPen);
    itsBattcurve.setPen(batVPen);

    // Plot Legend
    QwtLegend *plotlegend = new QwtLegend();
    plot->insertLegend(plotlegend, QwtPlot::BottomLegend);

    // Timer Setup
    connect(&itsDemoTimer, SIGNAL(timeout()), this, SLOT(poll()));
    itsDemoTimer.setSingleShot(true);
    itsInterval = 10000;
    // hide GUI Elements for Debug!
    sp_readAddress->hide();
    pb_ReadMeas->hide();

}

void SL900ADemo::on_pb_RunTempDemo_clicked ( )
{
    if (pb_RunTempDemo->isChecked())
        runContinousTempMeasDemo();
    else
        stopTheDemo();

}

void SL900ADemo::on_pb_RunOutOfFieldDemo_clicked ( )
{
    if (pb_RunOutOfFieldDemo->isChecked())
        runOutOfFieldDemo();
    else
        stopTheDemo();
}

void SL900ADemo::on_pb_PlotTagData_clicked ( )
{
    clearCurves(); 
    
    //Stop the Demo
    itsParent->coolLogEndLog(itsCoolLogError);
    SleepThread::mwait(10);
    
    //Get Chip State
    itsGen2Res = itsParent->coolLogGetLogState(itsCoolLogError, itsLimitsCounter, itsSystemStatus, itsStatusFlags, itsShelfLife,itsCurrentShelfLife);
    if ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
    {
        itsParent->logMessage(QString("SL900A Demo Tab: Error Get Log State, Demo not Started; Gen2Error: %1; CoolLogError: %2")
                              .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
        stopTheDemo();
        return;
    }
    itsGen2Res = itsParent->coolLogGetMeasurementSetup(itsCoolLogError, itsMDate, itsMTime, itsLimits, itsLogMode, itslogInterval, itsDelayTime, itsUserData);
    if ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
    {
        itsParent->logMessage(QString("SL900A Demo Tab: Error Get Log Measurement, Demo not Started; Gen2Error: %1; CoolLogError: %2")
            .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
        stopTheDemo();
        return;
    }
    //Update Fields
    le_ExtLowLimCount->setText(QString("%1").arg(itsLimitsCounter.extremeLowerCounter));
    le_LowLimCount->setText(QString("%1").arg(itsLimitsCounter.lowerCounter));
    le_UppLimCount->setText(QString("%1").arg(itsLimitsCounter.upperCounter));
    le_ExtUppLimCount->setText(QString("%1").arg(itsLimitsCounter.extremeUpperCounter));
    
    sp_ExtUpperLim->setValue(itsParent->tempfromCode(itsLimits.extremeUpperLimit));
    sp_UpperLim->setValue(itsParent->tempfromCode(itsLimits.upperLimit));
    sp_LowLim->setValue(itsParent->tempfromCode(itsLimits.lowerLimit));
    sp_ExtLowLim->setValue(itsParent->tempfromCode(itsLimits.extremeLowerLimit));
    sp_LogInterval->setValue(itslogInterval);

    le_numSamples->setText(QString("%1").arg(itsSystemStatus.measurements));
    dateEdit->setDate(itsMDate);
    timeEdit->setTime(itsMTime);

    if (itsLogMode.loggingForm <= 1 || itsLogMode.loggingForm == 3)
    {
        int cB_index = itsLogMode.loggingForm;
        if (cB_index > 1)
            cB_index--;
        
        cB_LoggingForm->setCurrentIndex(cB_index);
    }
    else
    {
        itsParent->logMessage("SL900A Demo Tab: Logging Form is not supported at the moment!");
    }
    
    //Create Plot
    itsLastReadPointer = 0;
    itsListMeasurementValues.clear();
    if (!updateMeasBlocks())
    {
        return; 
    }

    // Create Curves
    createLoggingFormCurves();
    createLimitCurves();
    // plot and stop/continue Demo
    plot->replot();

}

void SL900ADemo::runContinousTempMeasDemo ( )
{
    pb_RunTempDemo->setText("Stop");
    pb_RunOutOfFieldDemo->setEnabled(false);
    gB_ActiveDemo->setEnabled(false);
    gB_PassiveDemo->setEnabled(false);
    itsParent->itsCoolLogTab->setEnabled(false);
    pb_PlotTagData->setEnabled(false);

    plot->enableAxis(QwtPlot::yRight,false);
    //Stop the Demo
    itsParent->coolLogEndLog(itsCoolLogError);
    SleepThread::mwait(10);
    // Set Time in Gui
    QTime time;
    QDate date;

    dateEdit->setDate(date.currentDate());
    timeEdit->setTime(time.currentTime());
    SleepThread::mwait(10);
    // Update sample Field, Limit Counter and Plot
    clearCurves();
    itsListDirectTempMeasure.clear();
    itsInterval = sp_IntervalTempDemo->value() * 1000;
    itsParent->logMessage("SL900A Demo Tab: Demo Started");
    itsDemoTimer.start(itsInterval);

}

void SL900ADemo::runOutOfFieldDemo ( )
{
    pb_RunOutOfFieldDemo->setText("Stop");
    pb_RunTempDemo->setEnabled(false);
    gB_ActiveDemo->setEnabled(false);
    gB_PassiveDemo->setEnabled(false);
    itsParent->itsCoolLogTab->setEnabled(false);
    pb_PlotTagData->setEnabled(false);

    plot->enableAxis(QwtPlot::yRight,false);
    //Stop the Demo
    itsParent->coolLogEndLog(itsCoolLogError);
    SleepThread::mwait(10);
    
    // Initialize
    itsDelayTime.delayMode = 0;
    itsDelayTime.delayTime = 0;
    itsDelayTime.irqEnalbe = 0;
    itsUserData.brokenWordPointer = 0;
    itsUserData.numWordsAppData   = 0;
    itsGen2Res = itsParent->coolLogInitilize(itsCoolLogError, itsDelayTime, itsUserData);
    if  ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
    {
        itsParent->logMessage(QString("SL900A Demo Tab: Error Initialize, Demo not Started; Gen2Error: %1; CoolLogError: %2")
            .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
        stopTheDemo();
        return;
    }

    // Set Log Mode
    int logFVal = cB_LoggingForm->currentIndex();
    if (logFVal >= 2)
        logFVal++;
    itsLogMode.loggingForm     = logFVal;
    itsLogMode.storageRule     = 0;
    itsLogMode.extSen1Enable   = 0;
    itsLogMode.extSen2Enable   = 0;
    itsLogMode.tempSenEnable   = 1;
    itsLogMode.battCheckEnable = 1;
    itslogInterval = sp_LogInterval->value() - 1 ; //sec
    itsGen2Res = itsParent->coolLogSetLogMode(itsCoolLogError, itsLogMode, itslogInterval);
    if  ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
    {
        itsParent->logMessage(QString("SL900A Demo Tab: Error Set Log Mode, Demo not Started; Gen2Error: %1; CoolLogError: %2")
            .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
        stopTheDemo();
        return;
    }
    if (itsLogMode.battCheckEnable)
    {
    plot->enableAxis(QwtPlot::yRight,true);
    }

    // Set Log Limits
    itsLimits.extremeUpperLimit = itsParent->codefromTemp(sp_ExtUpperLim->value());
    itsLimits.extremeLowerLimit = itsParent->codefromTemp(sp_ExtLowLim->value());
    itsLimits.upperLimit        = itsParent->codefromTemp(sp_UpperLim->value());
    itsLimits.lowerLimit        = itsParent->codefromTemp(sp_LowLim->value());
    itsGen2Res = itsParent->coolLogSetLogLimits(itsCoolLogError, itsLimits);
    if  ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
    {
        itsParent->logMessage(QString("SL900A Demo Tab: Error Set Log Limits, Demo not Started; Gen2Error: %1; CoolLogError: %2")
            .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
        stopTheDemo();
        return;
    }
    
    // Get Battery Type
    bool adError;
    unsigned int batLevel;
    itsBattype = SL900Battype::Bat1V5;
    itsGen2Res = itsParent->coolLogGetBatteryLevel(itsCoolLogError, 0,  adError, itsBattype, batLevel);
    if ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) ||(adError))
    {
        itsParent->logMessage(QString("SL900A Demo Tab: Get Battery Level, Demo not Started; Gen2Error: %1; CoolLogError: %2")
            .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
        stopTheDemo();
        return;
    }
    QString cBattype = (itsBattype == SL900Battype::Bat3V0) ? "3.0V": "1.5V";
    itsParent->logMessage( QString("SL900A Demo Tab: Battery Type %1, Voltage %2").arg(itsBattype).arg(itsParent->battVfromCode(batLevel, itsBattype)) );
    // Start Log
    QTime startTime;
    QDate startDate;

    dateEdit->setDate(startDate.currentDate());
    timeEdit->setTime(startTime.currentTime());

    itsGen2Res = itsParent->coolLogStartLog(itsCoolLogError, startDate.currentDate(), startTime.currentTime());
    if  ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
    {
        itsParent->logMessage(QString("SL900A Demo Tab: Error StartLog, Demo not Started; Gen2Error: %1; CoolLogError: %2")
            .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
        stopTheDemo();
        return;
    }

    // Start Polling Loop
    itsParent->logMessage("SL900A Demo Tab: Demo Started");
    clearCurves();

    itsListMeasurementValues.clear();
    itsLastReadPointer = 0; // Demo has no User Memory

    itsInterval = 800;
    itsActiveFlagFailCounter = 0;
    itsDemoTimer.start(itsInterval);
    
}

void SL900ADemo::stopTheDemo ( )
{
    // reset GUI Elements
    itsDemoTimer.stop();

    pb_RunTempDemo->setText("Start");
    pb_RunTempDemo->setEnabled(true);
    pb_RunTempDemo->setChecked(false);
    pb_RunOutOfFieldDemo->setText("Start");
    pb_RunOutOfFieldDemo->setEnabled(true);
    pb_RunOutOfFieldDemo->setChecked(false);
    gB_ActiveDemo->setEnabled(true);
    gB_PassiveDemo->setEnabled(true);
    itsParent->itsCoolLogTab->setEnabled(true);
    pb_PlotTagData->setEnabled(true);

    itsInterval = 10000;
    //Stop the Demo
    itsGen2Res = itsParent->coolLogEndLog(itsCoolLogError);
    // Result to Gui
    if ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
    {
        itsParent->logMessage(QString("SL900A Demo Tab: Error Stop Demo; Gen2Error: %1; CoolLogError: %2")
        .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
    }
    else
        itsParent->logMessage("SL900A Demo Tab: Demo stopped! Tag to passive mode");
}

bool SL900ADemo::updateMeasBlocks ( )
{
    QByteArray answer;
    bool updateOccured = false;

    for ( unsigned int indexMReg = itsLastReadPointer; indexMReg < itsSystemStatus.measAddressPointer; indexMReg = indexMReg + 4 )
    {
        itsGen2Res = itsParent->readUserMemory(itsCoolLogError, indexMReg , answer);
        if ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
        {
            itsParent->logMessage(QString("SL900A Demo Tab: Error: update Measurements, readUserMemory; Gen2Error: %1; CoolLogError: %2")
                .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
            return false;
        }
        //qDebug() << "Read 4 Blocks : "  << QString("%1").arg(indexMReg, 2, 16, QLatin1Char('0')) << ": " << answer.toHex();
        qint16 block1 = ((answer[0] & 0xFF) << 8 ) | ((answer[1] & 0xFF));
        qint16 block2 = ((answer[2] & 0xFF) << 8 ) | ((answer[3] & 0xFF));
        qint16 block3 = ((answer[4] & 0xFF) << 8 ) | ((answer[5] & 0xFF));
        qint16 block4 = ((answer[6] & 0xFF) << 8 ) | ((answer[7] & 0xFF));
        itsListMeasurementValues.append(block1 );
        itsListMeasurementValues.append(block2);
        itsListMeasurementValues.append(block3);
        itsListMeasurementValues.append(block4);
        itsLastReadPointer = itsLastReadPointer + 4;
        updateOccured = true;
    }

    return updateOccured;
}

void SL900ADemo::poll ( )
{
    if (pb_RunOutOfFieldDemo->isChecked())
        pollOutofFieldDemo();
    else if (pb_RunTempDemo->isChecked())
        pollTemperature();
    else
        itsParent->logMessage("Error: Demo not implemented");

}

void SL900ADemo::pollTemperature ( )
{
    //  Temperature Measurement
    bool adError;
    unsigned int sensorVal;
    unsigned char sensorRange;

    itsGen2Res = itsParent->coolLogGetSensorValue(itsCoolLogError, 0, adError, sensorRange, sensorVal);
    
    // Result to Gui
    if ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) || adError)
    {
        itsParent->logMessage(QString("SL900A Demo Tab: Error: Get Sensor Value; Gen2Error: %1; CoolLogError: %2")
            .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res)).arg(itsParent->decodeCoolLogError(itsCoolLogError)));
        itsDemoTimer.start(itsInterval); // do not stop
        return;
    }
    
    float tempResult = itsParent->tempfromCode(sensorVal);
    itsListDirectTempMeasure.append(tempResult);

    itsTempValsY.append(tempResult);
    itsTempValsX.append(itsListDirectTempMeasure.length());

    itsTempcurve.setData(itsTempValsX, itsTempValsY);

    plot->replot();
    le_numSamples->setText(QString("%1").arg(itsListDirectTempMeasure.length()));

    itsDemoTimer.start(itsInterval); // do not stop

}

void SL900ADemo::pollOutofFieldDemo ( )
{
    SL900AGroupShelfLife slb1;
    unsigned long csl;
    // Get Log State
    itsGen2Res = itsParent->coolLogGetLogState(itsCoolLogError, itsLimitsCounter, itsSystemStatus, itsStatusFlags, slb1, csl);
    if  ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
    {
        // no Error Logging -> out of Field Demo
        itsDemoTimer.start(itsInterval);
        return;
    }

    // Update Fields
    le_ExtLowLimCount->setText(QString("%1").arg(itsLimitsCounter.extremeLowerCounter));
    le_LowLimCount->setText(QString("%1").arg(itsLimitsCounter.lowerCounter));
    le_UppLimCount->setText(QString("%1").arg(itsLimitsCounter.upperCounter));
    le_ExtUppLimCount->setText(QString("%1").arg(itsLimitsCounter.extremeUpperCounter));


    // Read User Bank if new Data is available
    le_numSamples->setText(QString("%1").arg(itsSystemStatus.measurements));

    if (itsStatusFlags.activeLogging == false && (itsActiveFlagFailCounter < 3))
    {
        itsActiveFlagFailCounter++;
        itsDemoTimer.start(itsInterval); // do not stop
        return;
    }
    
    if (itsActiveFlagFailCounter >= 3)
    {
        itsParent->logMessage("SL900A Demo Tab: Demo Stopped!!! Status Flag Active is not true!");
        stopTheDemo();
        return;
    }
    itsActiveFlagFailCounter = 0;
    // Create Logging Form Curves
    if (!updateMeasBlocks())
    {
        itsDemoTimer.start(itsInterval); // do not stop
        return; 
    }
    // Create Curves
    createLoggingFormCurves();
    createLimitCurves();
    // plot and stop/continue Demo
    plot->replot();
    
    if (itsSystemStatus.measAddressPointer == SL900A_MAX_ADRESS_POINTER) // One Stop condition
        itsInterval = 0;

    if (itsInterval != 0)
        itsDemoTimer.start(itsInterval); // do not stop
    else
        stopTheDemo();// stop the Demo

}

void SL900ADemo::clearCurves ( )
{
    itsTempValsY.clear();
    itsTempValsX.clear();	
    itsBattValsY.clear();
    itsBattValsX.clear();
    itsExtUppLimValsY.clear();
    itsExtUppLimValsX.clear();
    itsExtLowLimValsY.clear();
    itsExtLowLimValsX.clear();
    itsUppLimValsY.clear();
    itsUppLimValsX.clear(); 
    itsLowLimValsY.clear();
    itsLowLimValsX.clear();

    itsTempcurve.setData(itsTempValsX, itsTempValsY);
    itsBattcurve.setData(itsBattValsX, itsBattValsY);
    itsExtUppcurve.setData(itsExtUppLimValsX, itsExtUppLimValsY);
    itsUppcurve.setData(itsUppLimValsX, itsUppLimValsY);
    itsLowcurve.setData(itsLowLimValsX, itsLowLimValsY);
    itsExtLowcurve.setData(itsExtLowLimValsX, itsExtLowLimValsY);
}

void SL900ADemo::createLimitCurves ()
{
    itsExtUppLimValsY.clear();
    itsExtUppLimValsX.clear();
    itsUppLimValsY.clear();
    itsUppLimValsX.clear(); 
    itsLowLimValsY.clear();
    itsLowLimValsX.clear();
    itsExtLowLimValsY.clear();
    itsExtLowLimValsX.clear();

    if (!itsTempValsX.empty())
    {
        itsExtUppLimValsX.append(itsTempValsX.first());
        itsExtUppLimValsX.append(itsTempValsX.last());
        itsExtLowLimValsX.append(itsTempValsX.first());
        itsExtLowLimValsX.append(itsTempValsX.last()); 
        itsUppLimValsX.append(itsTempValsX.first());
        itsUppLimValsX.append(itsTempValsX.last());  
        itsLowLimValsX.append(itsTempValsX.first());
        itsLowLimValsX.append(itsTempValsX.last());
    }

    itsExtUppLimValsY.append(itsParent->tempfromCode(itsLimits.extremeUpperLimit));
    itsExtUppLimValsY.append(itsParent->tempfromCode(itsLimits.extremeUpperLimit));
    itsExtLowLimValsY.append(itsParent->tempfromCode(itsLimits.extremeLowerLimit));
    itsExtLowLimValsY.append(itsParent->tempfromCode(itsLimits.extremeLowerLimit));
    itsUppLimValsY.append(itsParent->tempfromCode(itsLimits.upperLimit));
    itsUppLimValsY.append(itsParent->tempfromCode(itsLimits.upperLimit));
    itsLowLimValsY.append(itsParent->tempfromCode(itsLimits.lowerLimit));
    itsLowLimValsY.append(itsParent->tempfromCode(itsLimits.lowerLimit));

    itsExtUppcurve.setData(itsExtUppLimValsX,itsExtUppLimValsY);
    itsExtLowcurve.setData(itsExtLowLimValsX,itsExtLowLimValsY);
    itsUppcurve.setData(itsLowLimValsX,itsLowLimValsY);
    itsLowcurve.setData(itsUppLimValsX,itsUppLimValsY);

}

void SL900ADemo::createLoggingFormCurves ()
{
    if (itsLogMode.storageRule == 1)
    {
        itsParent->logMessage("SL900A Demo: No Graphical Representation for Rolling Mode ");
        return;
    }

    itsTempValsX.clear();
    itsTempValsY.clear();
    itsBattValsX.clear();
    itsBattValsY.clear();

    unsigned int countX = 0;

    if ( itsLogMode.loggingForm == 0 ) // Dense // Temperature + Battery
    {
        unsigned int tempVal, batVal = 0;
        float vbatVal = 0;
        for ( int index = 0; index < itsListMeasurementValues.length(); index++ )
        {
            tempVal = (itsListMeasurementValues[index] & 0x03FF);
            batVal  = (itsListMeasurementValues[index] & 0xFC00) >> 6; // 4 LSB are 0;
            vbatVal = 10.0 * itsParent->battVfromCode(batVal,itsBattype);
            //qDebug() << "tempVal: " << QString("%1").arg(tempVal, 2, 16, QLatin1Char('0'))
            //         << "batVal:  " << QString("%1").arg(batVal,  2, 16, QLatin1Char('0'));

            itsTempValsY.append(itsParent->tempfromCode(tempVal));
            itsBattValsY.append(vbatVal);
            itsTempValsX.append(countX);
            itsBattValsX.append(countX++);
        }

    }
    else if (itsLogMode.loggingForm == 1) // Out Of Limits // Temperature + Battery
    {
        unsigned int tempVal, batVal = 0;
        float vbatVal = 0;
        for ( int index = 0; index < (itsListMeasurementValues.length()); index=index+2 )
        {
            tempVal = (itsListMeasurementValues[index] & 0x03FF);
            batVal  = (itsListMeasurementValues[index] & 0xFC00) >> 6; // 4 LSB are 0;
            vbatVal = 10.0 * itsParent->battVfromCode(batVal,itsBattype);
            //qDebug() << "tempVal: " << QString("%1").arg(tempVal, 2, 16, QLatin1Char('0'))
            //    << "batVal:  " << QString("%1").arg(batVal,  2, 16, QLatin1Char('0'));

            itsTempValsY.append(itsParent->tempfromCode(tempVal));
            itsBattValsY.append(vbatVal);
            itsTempValsX.append(countX);
            itsBattValsX.append(countX++);
        }

    }
    else if (itsLogMode.loggingForm == 3) // Limits Crossing // Temperature
    {
        unsigned int tempVal = 0;
        for ( int index = 0; index < (itsListMeasurementValues.length()); index=index+2 )
        {
            tempVal = (itsListMeasurementValues[index] & 0x03FF);
            //qDebug() << "tempVal: " << QString("%1").arg(tempVal, 2, 16, QLatin1Char('0'));
            itsTempValsY.append(itsParent->tempfromCode(tempVal));
            itsTempValsX.append(countX++);
        }

    }
    else
    {
        itsParent->logMessage("SL900A Demo Tab: This Logging Form is not implemented!");
        return;
    }
    //plot the Data
    itsTempcurve.setData(itsTempValsX, itsTempValsY);   
    if (itsLogMode.battCheckEnable)
    {
        itsBattcurve.setData(itsBattValsX, itsBattValsY);
    }

}

void SL900ADemo::on_pb_ReadMeas_clicked ( )
{
    QByteArray answer;

    itsGen2Res = itsParent->readUserMemory(itsCoolLogError, sp_readAddress->value() , answer);
    itsParent->logMessage("Read 4 Blocks " + QString("%1").arg(sp_readAddress->value(), 2, 16, QLatin1Char('0')) +": " + answer.toHex());
}
