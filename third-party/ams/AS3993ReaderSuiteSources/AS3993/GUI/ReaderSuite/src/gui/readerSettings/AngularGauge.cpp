/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "stable.h"
#include "AngularGauge.hxx"
#include "qwt_dial_needle.h"
#include <QPainter>
#include <QPixmap>
#include <QColor>
#include <QSvgRenderer>

AngularGauge::AngularGauge(QWidget* parent) : QwtDial(parent)
{
	setReadOnly(true);
    setScale(36, 2);        // 36 major ticks + 2 minor ticks per major tick
}

AngularGauge::~AngularGauge()
{
}

void AngularGauge::drawScaleContents(QPainter *painter, const QPoint &center, int radius) const
{
//    QPixmap bgImage(QString(":button icons/RoundGaugeBG3.png"));
//    QPixmap bgImage(QString(":/window images/ams_gauge_background_export.svg"));
    static QSize backgroundSize = QSize(0, 0);
    static QPixmap bgImage;

    // re-use already rendered svg if size did not change
    if(this->contentsRect().size() != backgroundSize)
    {
        QString svg(":/window images/ams_gauge_background_export.svg");
        QSvgRenderer renderer;
        renderer.load(svg);
        QPixmap newImage(this->contentsRect().size());
        newImage.fill(Qt::transparent);
        QPainter svgPainter(&newImage);
        renderer.render(&svgPainter);
        bgImage = newImage;
        backgroundSize = this->contentsRect().size();
    }
    painter->drawPixmap(this->contentsRect(), bgImage);
}
