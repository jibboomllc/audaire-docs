/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef CREADERCONFIGDIALOG_H
#define CREADERCONFIGDIALOG_H

#include <QtGui/QDialog>
#include "ui_CReaderConfigDialog.h"
#include "reader/ReaderInterface.h"
#include "reader/ams/AmsReader.h"

class ReaderInterface;
class AmsReader;

class CReaderConfigDialog : public QDialog
{
    Q_OBJECT

public:
    CReaderConfigDialog(QWidget *parent = 0);
    ~CReaderConfigDialog();

    int changeProfile(ReaderInterface *ph, QString profileName);

public slots:
	int exec ( ReaderInterface* ph );

	void tryAccept();
	void setHopper( const QString &);
    void refDividerChanged();
    void abValueChanged();
	void oneFreqChanged(double);
	void oneFreqChanged(int);

	void setInventoryInterval();
	void setAntennaPower();
	void setFrequency();
    void setHoppingParams();
	void setTagMode();
    void autoAckChanged();
    void agcModeChanged();
	void setGen2Configuration();

	void sweepFrequencys();
	void setLevel();
    bool executeSetLevel();
	void setAntenna();
    void setRssiMode();
	void sendCommand();
	void levelSliderChanged(int);
	void sensSliderChanged(int);
	void openRFPDialog(void);
	void continuousSend(void);
	void setSensitivity(void);

    void codingChanged(int);
    void linkFrequencyChanged(int);
    void tariChanged(int);
    void pilotChanged(bool);

	void tuneFrequencies(void);
	void uploadTuningParameters(void);
    void calcTuneTableSize();

    void superUserMode();

    void biasSettingChanged();
    void powerDownModeChanged();
    void changeexternalPAGain();
    void cB_SelectMode(int option);

private:
    void readConfigAndUpdateControls();
	void displayPlot(double* ivals, double *qvals, double *svals, double *freqs, int size);
	void clearInfo();
	void showOK();
	void showNOK();
	void readProfiles();
	bool getGen2Config();
	bool getOutputLevel();
	bool getTxRxParams();
	bool getFreqs();
    bool getBiasSetting();
    bool getPowerDownMode();

	class TuneParameter;
	void writeTuningFile(const QList<TuneParameter> tuneParameters, ReaderInterface::HardwareId hwType );
	void readTuningFile(QList<TuneParameter>&);
	void uploadFrequencyList(QList<ulong> freqs);
	void uploadTuningTable(QList<TuneParameter> tuneParameters);
	QList<ulong> getFrequencyList();
    QList<ulong> getTuneFrequencyList();

	QMap<QString,QStringList> profiles;

	Ui::CDialogClass ui;

	ReaderInterface* m_ph;
	int m_tx_lev_coarse;
	int m_tx_lev_fine;
    quint8 m_maxTunerTableSize;

    QAction *superUserAct;
    bool superUser;

};

#endif // CREADERCONFIGDIALOG_H
