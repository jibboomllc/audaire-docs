/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "CGen2TagDialog.h"
#include <QMessageBox>

#include "../../reader/Gen2ReaderInterface.h"


CGen2TagDialog::CGen2TagDialog(QWidget *parent)
    : QDialog(parent), m_bitSequenceEditor(&m_bitSequenceEditResult, this)
{
    setupUi(this);
    l_XPC1->hide();
    l_XPC2->hide();
    le_XPC_W1->hide();
    le_XPC_W2->hide();
    
    m_moveDialog = new CGen2MovementDialog(this);
    m_moveDialog->setWindowModality(Qt::WindowModal);

    m_Sl900AMainDialog = new SL900AMainDialog(this);


    for(int i = 0; i < 8; i++)
        memoryTableWidget->setColumnWidth(i, 40);

    QObject::connect(lockActionBox,        SIGNAL(currentIndexChanged(int)),     this, SLOT(loadDescription()) );
    QObject::connect(lockMemoryBox,        SIGNAL(currentIndexChanged(int)),     this, SLOT(loadDescription()) );
    QObject::connect(&m_bitSequenceEditor,      SIGNAL(editingFinished()),     this, SLOT(bitStreamEditorTryAccept()));


    this->resize(500, this->height());

    m_readActive = false;
    m_readMemSize = 0;

    loadDescription();
}

CGen2TagDialog::~CGen2TagDialog()
{
    delete m_Sl900AMainDialog;
}

int CGen2TagDialog::exec(ReaderInterface* ph, Gen2Tag tag)
{
    m_ph = ph;

    if(!(m_ph->tagType() == ReaderInterface::TAG_GEN2 || m_ph->tagType() == ReaderInterface::TAG_GEN2_FAST || m_ph->tagType() == ReaderInterface::TAG_GEN2_TID))
        return 0;

    if((m_gen2Reader = m_ph->getGen2Reader()) == 0)
        return 0;

    m_tag = tag;


    cmdDataLineEdit->setCursorPosition(0);
    tagIdEdit->setText(tag.epc);
    readerIdEdit->setText(ph->readerId());
    le_accessPassword->setText("00-00-00-00");
    sW_Functions->setCurrentIndex(0);
    rb_TagMem->setChecked(1);
    memBankBox->setCurrentIndex(0);
    setMemButton->setVisible(false);

    clearInfo();

    QrfeProgressBar* pb = new QrfeProgressBar("Reading data from tag...", 60, qobject_cast<QWidget*>(parent()));
    pb->show();

    for(int i = 0; i < 4; i++)
    {
        if(readTIDRegisterFromTag(pb))
            break;
    }

    pb->increaseProgressBar();
    pb->hide();
    delete pb;

    memoryTableWidget->setRowCount(0);
    userMemSizeEdit->setText("0 Bytes");

    m_initialLengthEPC = (tag.epc.length() + 1)/6;
    lengthBox->setValue(m_initialLengthEPC);
    on_lengthBox_valueChanged(m_initialLengthEPC); /* for the case that the lengths was not different from the different/default value */
    tagIdEdit->setText(tag.epc);
    newEpcEdit->setText(tag.epc);

    return QDialog::exec();
}

void CGen2TagDialog::on_tagDialogButtonBox_accepted()
{
    QDialog::accept();
}

void CGen2TagDialog::on_refreshTIDButton_clicked()
{
    QrfeProgressBar* pb = new QrfeProgressBar("Reading data from tag...", 2, qobject_cast<QWidget*>(parent()));
    pb->show();
    pb->raise();

    readTIDRegisterFromTag(pb);

    pb->hide();
    delete pb;
}

void CGen2TagDialog::on_rssiButton_clicked()
{
    m_moveDialog->exec(m_gen2Reader, m_tag);
}

void CGen2TagDialog::on_rb_TagMem_clicked()
{
    sW_Functions->setCurrentIndex(0);
}

void CGen2TagDialog::on_rb_SetEPC_clicked()
{
    sW_Functions->setCurrentIndex(1);
}

void CGen2TagDialog::on_rb_SetPassword_clicked()
{
    sW_Functions->setCurrentIndex(2);
}

void CGen2TagDialog::on_rb_Lock_clicked()
{
    sW_Functions->setCurrentIndex(3);
}

void CGen2TagDialog::on_rb_Kill_clicked()
{
    sW_Functions->setCurrentIndex(4);
}

void CGen2TagDialog::on_rb_GenericCommand_clicked()
{
    sW_Functions->setCurrentIndex(5);
}
void CGen2TagDialog::on_rb_SL900A_clicked()
{
    if(m_ph->tagType() == ReaderInterface::TAG_GEN2 || m_ph->tagType() == ReaderInterface::TAG_GEN2_FAST || m_ph->tagType() == ReaderInterface::TAG_GEN2_TID)
    {
        m_Sl900AMainDialog->exec(m_ph, m_tag.epc);
    }
    rb_SL900A->setChecked(false);
}

void CGen2TagDialog::on_readMemButton_clicked()
{
    QrfeProgressBar* pb = new QrfeProgressBar("Reading data from tag...", 60, qobject_cast<QWidget*>(parent()));
    pb->show();
    pb->raise();

    m_readBank = (MEMORY_BANK)memBankBox->currentIndex();

    uint size = 0;
    if(m_readBank == MEM_RES) size = 8;

    readMemoryFromTag(m_readBank, pb, size);

    pb->hide();
    delete pb;

    if( m_tag.epc.contains("E2-00-68-06-00-00-00-00-00-00-00-00", Qt::CaseInsensitive))
        emit easterKeyUnlocked();
}

void CGen2TagDialog::on_setMemButton_clicked()
{
    int written = 0;
    QrfeProgressBar* pb = new QrfeProgressBar("Writing data to tag...", 60, qobject_cast<QWidget*>(parent()));
    pb->show();
    pb->raise();

    bool ok = false;
    ComByteArray passw = ReaderInterface::stringToEpc(le_accessPassword->text(), &ok);
    if(!ok)
    {
        QMessageBox::critical(this, "Error", "The access password is not a valid!");
        return;
    }

    pb->increaseProgressBar();

    QByteArray data;
    for(uint i = 0; i < m_readMemSize; i++)
    {
        bool ok = false;
        data.append(memoryTableWidget->item(i/memoryTableWidget->columnCount(), i%memoryTableWidget->columnCount())->text().toInt(&ok, 16));
        if(!ok){
            pb->hide();
            delete pb;
            QMessageBox::critical(this, "Error", "The data written in the memory table are not valid!");
            return;
        }
    }

    pb->increaseProgressBar();

    quint32 address = 0;

    if(m_readBank == MEM_EPC)
    {
        data = data.mid(4, data.size() - 4);
        address = 2;
    }else if(m_readBank == MEM_RES)
    {
        data = data.left(8);
    }else
    {
        this->m_lowestChanged = this->m_lowestChanged / 2 * 2; /* To be 16-bit word aligned. */
        this->m_highestChanged = this->m_highestChanged / 2 * 2 + 1;
        data = data.mid(this->m_lowestChanged,this->m_highestChanged+1-this->m_lowestChanged);
        address = this->m_lowestChanged / 2;
    }

    Gen2ReaderInterface::Gen2Result res = m_gen2Reader->writeToTag(m_tag, m_readBank, address, passw, data, &written);

    if(res == Gen2ReaderInterface::Gen2_OK)
    {
        infoEdit->setText("-- Wrote data to tag memory - OK --");
        QString text;
        for (int i = 0; i < data.size(); i++)
            text += QString("%1 ").arg((unsigned char) data[i], 2, 16, QChar('0'));
        infoEdit->append("The data: " +  text);

        //if(m_readBank == MEM_EPC)
        //{
        //    m_tag = ReaderInterface::epcToString(data);
        //    tagIdEdit->setText(m_tag);
        //}
        //else if(m_readBank == MEM_RES)
        //    le_accessPassword->setText("00-00-00-00");

        this->m_lowestChanged = 65000; // some high never reached number....
        this->m_highestChanged = -1;
        memoryTableWidget->blockSignals(true);
        for(uint i = 0; i < m_readMemSize; i++)
        {
            QFont font;
            font = memoryTableWidget->item(i/8,i%8)->font();
            font.setBold(false);
            memoryTableWidget->item(i/8,i%8)->setFont(font);
        }
        memoryTableWidget->blockSignals(false);

        showOK();
    }
    else
        handleError(res, QString("Wrote %1 bytes of %2 bytes at %3")
                         .arg(written).arg(data.size()).arg(address));

    pb->hide();
    delete pb;

    // T  h  i  s  I  s  T  h  e  K  e  y
    if( m_tag.epc.contains("E2-00-68-06-00-00-00-00-00-00-00-00", Qt::CaseInsensitive))
        emit easterKeyUnlocked();
}

void CGen2TagDialog::on_memBankBox_currentIndexChanged(int index)
{
    if(index == MEM_USER)
        setMemButton->setVisible(true);
    else
        setMemButton->setVisible(false);

    memoryTableWidget->setRowCount(0);
}
void CGen2TagDialog::on_memoryTableWidget_itemChanged(QTableWidgetItem* item)
{
    int pos = item->column() + item->row()*8;
    if(m_readActive)
        return;

    QFont f = item->font();
    f.setBold(true);
    item->setFont(f);
    if (this->m_lowestChanged > pos) this->m_lowestChanged = pos;
    if (this->m_highestChanged < pos) this->m_highestChanged = pos;
}

void CGen2TagDialog::on_setEPCButton_clicked()
{
    QString newEPC;
    int writtenBytes = 0;
    clearInfo();

    if(!newEpcEdit->hasAcceptableInput())
    {
        QMessageBox::critical(this, "Error", "The new epc is not a valid!");
        return;
    }

    bool ok = false;
    ComByteArray passw = ReaderInterface::stringToEpc(le_accessPassword->text(), &ok);
    if(!ok)
    {
        QMessageBox::critical(this, "Error", "The current password is not a valid!");
        return;
    }

    newEPC = newEpcEdit->text();

    if(newEPC == tagIdEdit->text()){
        QMessageBox::critical(this, "Error", "EPC is the same!");
        return;
    }

    Gen2ReaderInterface::Gen2Result res = Gen2ReaderInterface::Gen2_ERR_NOMSG;

    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    if (m_currentLengthEPC!=m_initialLengthEPC)
    {
        ComByteArray pc;
        res = m_gen2Reader->readFromTag(m_tag,1,1,passw,1,true,pc);
        if (!res) pc[0] = pc[0] & 0x07 | (m_currentLengthEPC<<3);
        pc.append(ReaderInterface::stringToEpc(newEPC));
        if (!res) res = m_gen2Reader->writeToTag(m_tag,1,1,passw,pc,&writtenBytes);
    }
    else
    {
        res = m_gen2Reader->writeTagId(m_tag, newEPC, passw, &writtenBytes);
    }

    if(res == Gen2ReaderInterface::Gen2_OK){
        infoEdit->setText("-- TagId set to " + newEPC + " - OK --");
        showOK();
        tagIdEdit->setText(newEPC);
        m_tag.epc = newEPC;
    }
    else
    {
        if ( writtenBytes )
        {
            newEPC = newEPC.left(writtenBytes*3).append(tagIdEdit->text().mid(writtenBytes*3));
            handleError(res, QString("Could not set epc, but %1 bytes were written").arg(writtenBytes));
        }
        else
            handleError(res, "Could not set epc");
    }
    QApplication::restoreOverrideCursor();
}

void CGen2TagDialog::on_lengthBox_valueChanged(int len)
{
    QString inputMask;
    m_currentLengthEPC = len;
    for ( int i = 0; i< len; i++)
    {
        inputMask += QString("HH-HH-");
    }
    inputMask.chop(1);
    newEpcEdit->setInputMask(inputMask);
}

void CGen2TagDialog::on_setPasswordButton_clicked()
{
    clearInfo();

    bool ok = false;
    ComByteArray currentPassw = ReaderInterface::stringToEpc(le_accessPassword->text(), &ok);
    if(!ok)
    {
        QMessageBox::critical(this, "Error", "The current password is not a valid!");
        return;
    }
    ComByteArray newPassw = ReaderInterface::stringToEpc(newPasswordLineEdit->text(), &ok);
    if(!ok)
    {
        QMessageBox::critical(this, "Error", "The new password is not a valid!");
        return;
    }

    quint32 wordaddr = 0;

    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    if(typePW->currentIndex() == 1)
        wordaddr += 2;

    Gen2ReaderInterface::Gen2Result res = m_gen2Reader->writeToTag(m_tag, MEM_RES, wordaddr, currentPassw, newPassw);

    if(res == Gen2ReaderInterface::Gen2_OK){
        showOK();
        QString action;
        infoEdit->setText("-- Set " + typePW->currentText() + " Password of tag " + tagIdEdit->text() + " - OK -- ");
    }
    else
        handleError(res, "Could not set password");
    QApplication::restoreOverrideCursor();
}

void CGen2TagDialog::on_lockButton_clicked()
{
    clearInfo();

    bool ok = false;
    ComByteArray passw = ReaderInterface::stringToEpc(le_accessPassword->text(), &ok);
    if(!ok)
    {
        QMessageBox::critical(this, "Error", "The password is not a valid!");
        return;
    }

    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    LOCK_MODE mode = (LOCK_MODE)lockActionBox->currentIndex();
    LOCK_MEMORY_SPACE mem = (LOCK_MEMORY_SPACE)lockMemoryBox->currentIndex();

    Gen2ReaderInterface::Gen2Result res = m_gen2Reader->lockTag(m_tag, mode, mem, passw);

    if(res == Gen2ReaderInterface::Gen2_OK){
        showOK();
        QString action;
        if(lockActionBox->currentIndex() == 0)
            action = "Unlocked";
        else
            action = "Locked";
        infoEdit->setText("-- " + action + " tag " + tagIdEdit->text() + " - OK -- ");
    }
    else
        handleError(res, "Could not lock tag");
    QApplication::restoreOverrideCursor();
}

void CGen2TagDialog::loadDescription()
{
    QStringList lockDescriptions = QStringList()
        << "Associated memory bank is writeable from either the open or secured states."
        << "Associated memory bank is permanently writeable from either the open or secured states and may never be locked."
        << "Associated memory bank is writeable from the secured state but not from the open state."
        << "Associated memory bank is not writeable from any state."
        << "Associated password location is readable and writeable from either the open or secured states."
        << "Associated password location is permanently readable and writeable from either the open or secured states and may never be locked."
        << "Associated password location is readable and writeable from the secured state but not from the open state."
        << "Associated password location is not readable or writeable from any state."
        ;

    uchar action = lockActionBox->currentIndex();
    uchar memspace = lockMemoryBox->currentIndex();

    uchar index = action;

    if(memspace > 2)    //show text for password 
        index += 4;

    l_descriptionLock->setText(lockDescriptions.at(index));

    this->adjustSize();
}

void CGen2TagDialog::on_killTagButton_clicked()
{
    clearInfo();

    if(!le_killPW->hasAcceptableInput())
    {
        QMessageBox::critical(this, "Error", "The current password is not a valid!");
        return;
    }
    
    bool ok = false;
    ComByteArray passw = ReaderInterface::stringToEpc(le_killPW->text(), &ok);

    if(passw.size() != 4)
        return;

    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    
    Gen2ReaderInterface::Gen2Result res = m_gen2Reader->killTag(m_tag, passw);

    if(res == Gen2ReaderInterface::Gen2_OK)
        infoEdit->setText("-- Killed tag " + m_tag.epc + " - OK --");


    handleError(res, "Could not kill tag");
    
    QApplication::restoreOverrideCursor();
}

void CGen2TagDialog::on_pbGenericCMD_clicked()
{
    clearInfo();

    bool ok = false;
    ComByteArray passw = ReaderInterface::stringToEpc(le_accessPassword->text(), &ok);
    if(!ok)
    {
        QMessageBox::critical(this, "Error", "The password is not a valid!");
        return;
    }

    // store current values of controls, we want them to be displayed again
    m_commandGeneric               = cmdDataLineEdit->text();
    m_rxBitLengthGeneric           = rxBitLengthDoubleSpinBox->value();
    m_txBitLengthGeneric           = txBitLengthDoubleSpinBox->value();
    int m_rxNoResponseTimeout   = static_cast<int>(as399xRxNoRespTimeoutDoubleSpinBox->value());

    unsigned char tmp[3] = { 0x00 };
    tmp[0]  = ((static_cast<int>(m_txBitLengthGeneric + RN16SIZE) >> 8) & 0x0F) << 4;      // TX Bit Length of command upper nibble
    tmp[0] |= (static_cast<int>(m_txBitLengthGeneric + RN16SIZE) & 0xF0) >> 4;             // TX Bit Length of command middle nibble
    tmp[1]  = (static_cast<int>(m_txBitLengthGeneric + RN16SIZE) & 0x0F) << 4;             // TX Bit Length of command lower nibble
    tmp[1] |= (static_cast<int>(m_rxBitLengthGeneric + RN16SIZE + CRC16SIZE) >> 8) & 0x0F; // RX Bit Length of command upper nibble
    tmp[2]  = static_cast<int>(m_rxBitLengthGeneric + RN16SIZE + CRC16SIZE) & 0xFF;        // RX Bit Length of command middle and lower nibble
    // get TX and RX bit length: We use 12bit for length -> 3 bytes used for both lenghtes
    QString str = "";
    str.append(QString::QString("%1-%2-%3").arg(tmp[0], 2, 16, QLatin1Char('0')).arg(tmp[1], 2, 16, QLatin1Char('0')).arg(tmp[2], 2, 16, QLatin1Char('0')));

    QStringList sl = as399xDirectCmdComboBox->currentText().split(" ",QString::SkipEmptyParts);
    // append direct command
    str.append("-" + sl.last().replace(QString("0x"), QString(""), Qt::CaseInsensitive));
    // append RX no response timeout
    str.append(QString::QString("-%1").arg(m_rxNoResponseTimeout, 2, 16, QLatin1Char('0')));
    // append "the" command data
    str.append("-" + m_commandGeneric);

    ComByteArray command = ReaderInterface::stringToEpc(str, &ok);
    if(!ok)
    {
        QMessageBox::critical(this, "Error", "The command is not a valid!");
        return;
    }

    QByteArray commandData = command.toQByteArray();
    QByteArray resultBytes;
    resultBytes.resize(50);
    unsigned int receivedLength = 0;

    /* Select tag */
    QString epc = tagIdEdit->text();
    Gen2ReaderInterface::Gen2Result res = m_gen2Reader->genericCommand(m_tag, commandData, passw.toQByteArray(), resultBytes, receivedLength); // pass an empty tagID (EPC) to bypass tag select command

    if(res == Gen2ReaderInterface::Gen2_OK)
    {
        showOK();
        QString action;
        int numTxBytes = static_cast<int>(m_txBitLengthGeneric) / 8;
        if (static_cast<int>(m_txBitLengthGeneric) % 8)
        {
            numTxBytes += 1;
        }
        infoEdit->setText("-- Set Command: " + str.mid(5 * 3, numTxBytes * 3 - 1) + "    to Tag: " + epc + "    -- OK -- ");
        if ( resultBytes.isEmpty())
        {
            QMessageBox::information(this, "Set Generic command", "Set Command " + str.mid(5 * 3, numTxBytes * 3 - 1) + "    to Tag: " + epc + "\nreturned 0 bytes");
            //QDialog::accept();
        }
        else
        {
            QString data = resultBytes.toHex();
            infoEdit->append(QString("\nCommand returned values: %1").arg(data) );
        }
    }
    else
        handleError(res, "Could not set Generic Command");

}

void CGen2TagDialog::on_pb_OpenBitStreamEditor_clicked(bool clicked)
{
    m_bitSequenceEditor.show();
}


void CGen2TagDialog::bitStreamEditorTryAccept()
{
    if (m_bitSequenceEditResult.second > 0)
    {
        m_commandGeneric.clear();
        m_commandGeneric.append("00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00"
            "-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00");
        m_commandGeneric.replace(0, m_bitSequenceEditResult.first.size(), m_bitSequenceEditResult.first);
        cmdDataLineEdit->clear();
        cmdDataLineEdit->setText(m_commandGeneric);
        cmdDataLineEdit->setCursorPosition(0);
        m_txBitLengthGeneric = m_bitSequenceEditResult.second;
        txBitLengthDoubleSpinBox->setValue(m_txBitLengthGeneric);
    }
}

bool CGen2TagDialog::readTIDRegisterFromTag(QrfeProgressBar* pb)
{
    clearInfo();

    ComByteArray tid;
    bool gotInfos = false;
    quint16 manufacturer = 0;
    quint64 modelNr = 0;
    QString serial;

    Gen2ReaderInterface::Gen2Result res = m_gen2Reader->readFromTag(m_tag, MEM_TID, 0, ComByteArray(4, (char)0), 10, false, tid);

    if(tid.size() > 0 && (res == Gen2ReaderInterface::Gen2_Tag_MEM_OVERRUN || res == Gen2ReaderInterface::Gen2_OK))
    {
        if(tid.size()>7 && tid.at(0) == EPC_TID_CLASS_ID_1)
        {
            manufacturer = (quint8)tid.at(1);
            serial += QString("%1 ").arg((unsigned char)tid.at(2),2,16,QChar('0'));
            serial += QString("%1 ").arg((unsigned char)tid.at(3),2,16,QChar('0'));
            serial += QString("%1 ").arg((unsigned char)tid.at(4),2,16,QChar('0'));
            serial += QString("%1 ").arg((unsigned char)tid.at(5),2,16,QChar('0'));
            serial += QString("%1 ").arg((unsigned char)tid.at(6),2,16,QChar('0'));
            serial += QString("%1 ").arg((unsigned char)tid.at(7),2,16,QChar('0'));
            gotInfos = true;
        }
        else if(tid.size()>3 && tid.at(0) == EPC_TID_CLASS_ID_2)
        {
            manufacturer += ((quint16)tid.at(1)) << 4;
            manufacturer += ((quint16) (tid.at(2) & 0xF0)) >> 4;
            modelNr += ((quint16) (tid.at(2) & 0x0F)) << 8;
            modelNr += (uchar)tid.at(3);
            for ( int i = 4; i< tid.size(); i++)
            {
                serial += QString("%1 ").arg((unsigned char)tid.at(i),2,16,QChar('0'));
            }
            gotInfos = true;

        }
        else
        {
            infoEdit->setText("-- The tag has no valid epc class id - ERROR --");
            showNOK();
            return false;
        }
    }
    else
        handleError(res, "Could not read TID or too short TID");

    if(pb)
        pb->increaseProgressBar();

    QString manu;
    QString model;
    QString userMemSize;
    if(gotInfos)
    {
        manu = QString("0x%1").arg(manufacturer, 4, 16, QChar('0'));
        if(manufacturer > 0 && manufacturer < g_epcManufacturer.size())
            manu = g_epcManufacturer.at(manufacturer);

        if(g_tagInfo.contains(manu) && g_tagInfo.value(manu).contains(modelNr)){
            model = g_tagInfo.value(manu).value(modelNr).modelName;
            userMemSize = QString::number(g_tagInfo.value(manu).value(modelNr).memorySize) + " Bytes";
        }
        else{
            model = QString("0x%1").arg(modelNr, 6, 16, QChar('0'));
            userMemSize = "-";
        }

        infoEdit->setText("-- Refreshed informations - OK --");
        showOK();
    }
    else
    {
        manu = "-";
        model = "-";
        userMemSize = "-";
        serial = "-";
    }

    manufacturerInfoEdit->setText(manu);
    modelInfoEdit->setText(model);
    userMemSizeInfoEdit->setText(userMemSize);
    serialNumberEdit->setText(serial);

    return gotInfos;
}

bool CGen2TagDialog::readMemoryFromTag(MEMORY_BANK bank, QrfeProgressBar* pb, uint /*size*/)
{
    bool ok;
    int timeout = 3;

    this->m_lowestChanged = 65000; // some high never reached number....
    this->m_highestChanged = -1;

    clearInfo();

    Gen2ReaderInterface::Gen2Result res = Gen2ReaderInterface::Gen2_ERR_NOMSG;

    m_readActive = true;

    quint32 byteCount = 0;

    memoryTableWidget->setRowCount(0);

    ComByteArray passw = ReaderInterface::stringToEpc(le_accessPassword->text(), &ok);

    while(timeout)
    {
        ComByteArray mem;

        res = m_gen2Reader->readFromTag(m_tag, bank, 0 + (byteCount/2), passw, 64, false, mem);

        if(pb)
            pb->increaseProgressBar();

        if (mem.size() == 0) timeout--;
        if (mem.size() > 0) timeout++;
        if (timeout > 3) timeout = 3;

        for(int i = 0; i < mem.size(); i++)
        {
            if(byteCount%memoryTableWidget->columnCount() == 0){
                memoryTableWidget->setRowCount((byteCount/memoryTableWidget->columnCount()) + 1);
                memoryTableWidget->setRowHeight(memoryTableWidget->rowCount()-1, 22);
            }

            memoryTableWidget->setItem(
                (byteCount/memoryTableWidget->columnCount()),
                (byteCount%memoryTableWidget->columnCount()),
                new QTableWidgetItem(QString("%1").arg((uchar)mem.at(i), 2, 16, QChar('0'))));

            byteCount ++;
        }
        if(res == Gen2ReaderInterface::Gen2_Tag_MEM_OVERRUN){
            break;
        }

        if(pb)
            pb->increaseProgressBar();
    }

    userMemSizeEdit->setText(QString::number(byteCount) + " Bytes");

    if(res == Gen2ReaderInterface::Gen2_Tag_MEM_OVERRUN){
        infoEdit->setText("-- Read everything (" + QString::number(byteCount) + " Bytes) - OK --");
        showOK();
    }
    else
        handleError(res, "Could not read more data from tag");

    m_readActive = false;
    m_readMemSize = byteCount;

    return true;
}

void CGen2TagDialog::handleError(Gen2ReaderInterface::Gen2Result result, QString text)
{
    if(result == Gen2ReaderInterface::Gen2_OK)
    {
        showOK();
    }
    else
    {
        infoEdit->setText("-- " + text + " - ERROR -- \n" + m_gen2Reader->errorToString(result));
        switch (result)
        { /* some color codings */
        case Gen2ReaderInterface::Gen2_TAG_UNREACHABLE: showTagUnreachable(); break;
        case Gen2ReaderInterface::Gen2_WRONG_PASSW: showWrongPassword(); break;
        case Gen2ReaderInterface::Gen2_Tag_MEM_OVERRUN: showMemoryOverrun(); break;
        case Gen2ReaderInterface::Gen2_Tag_MEM_LOCKED: showMemoryLocked(); break;
        case Gen2ReaderInterface::Gen2_Tag_INSUFFICIENT_POWER: showInsufficentPower(); break;
        default: showNOK();
        }
    }
}

void CGen2TagDialog::clearInfo()
{
    infoEdit->setStyleSheet("");
    infoEdit->clear();
}

void CGen2TagDialog::showOK()
{
    infoEdit->setStyleSheet("background-color:lightgreen;");
}

void CGen2TagDialog::showTagUnreachable()
{
    infoEdit->setStyleSheet("background-color:grey;");
}

void CGen2TagDialog::showWrongPassword()
{
    infoEdit->setStyleSheet("background-color:darkred;");
}

void CGen2TagDialog::showMemoryOverrun()
{
    infoEdit->setStyleSheet("background-color:lightpink;");
}

void CGen2TagDialog::showMemoryLocked()
{
    infoEdit->setStyleSheet("background-color:lightcyan;");
}

void CGen2TagDialog::showInsufficentPower()
{
    infoEdit->setStyleSheet("background-color:lightblue;");
}

void CGen2TagDialog::showNOK()
{
    infoEdit->setStyleSheet("background-color:lightcoral;");
}
