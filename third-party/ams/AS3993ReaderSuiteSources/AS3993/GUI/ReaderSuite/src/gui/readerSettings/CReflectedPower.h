/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef CREFLECTEDPOWER_H
#define CREFLECTEDPOWER_H

#include <QtGui/QDialog>
#include <QtCore/QTimer>
#include "ui_CReflectedPower.h"
#include "qwt_dial_needle.h"

class ReaderInterface;
class AmsReader;
class DynamicNeedle;

class CReflectedPower : public QDialog
{
    Q_OBJECT

public:
    CReflectedPower(QWidget *parent = 0);
    ~CReflectedPower();
	int exec ( ReaderInterface* ph, double freq );
    int currentAntenna();

public slots:
    void setAntenna(int index);
    void setDefaultValue();
    void tunerChanged( int index );

private:
	ReaderInterface* m_ph;
	Ui::CReflectedPowerClass ui;
	DynamicNeedle * needle;
	QTimer timer;
	double G;
	unsigned oldFreq;
	quint8 oldCin, oldClen, oldCout;

private slots:
	void updateDial();
};

#endif