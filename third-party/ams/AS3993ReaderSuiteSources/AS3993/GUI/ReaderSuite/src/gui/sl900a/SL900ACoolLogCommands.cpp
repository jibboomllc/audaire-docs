/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "SL900ACoolLogCommands.hxx"
#include <QMessageBox>
#include "qdebug.h"


SL900ACoolLogCommands::SL900ACoolLogCommands ( SL900AMainDialog *parent )
{
    setupUi(this);
    itsParent = parent;

    // Initialize Member Variables
    itsGen2Res       = Gen2ReaderInterface::Gen2_OK;
    itsCoolLogError  = CoolLogError::CoolLogNoError; 
    itsCalDataLoaded = false;

    sW_Commands->setCurrentIndex(9);
    cB_Rang->setCurrentIndex(1);
    cB_Rang->setCurrentIndex(0);
}

CoolLogCommand SL900ACoolLogCommands::getCoolLogCommandFromIndex ( unsigned int index )
{
    CoolLogCommand code;

    switch (index)
    {
    case 0:  code = CoolLogCommand::CoolLogSetPassword;    break;
    case 1:  code = CoolLogCommand::CoolLogSetLogMode;     break;
    case 2:  code = CoolLogCommand::CoolLogSetLogLimits;   break;
    case 3:  code = CoolLogCommand::CoolLogGetMeasSetup;   break;
    case 4:  code = CoolLogCommand::CoolLogSetSfeParams;   break;
    case 5:  code = CoolLogCommand::CoolLogSetCalibration; break;
    case 6:  code = CoolLogCommand::CoolLogEndLog;         break;
    case 7:  code = CoolLogCommand::CoolLogStartLog;       break;
    case 8:  code = CoolLogCommand::CoolLogGetLogState;    break;
    case 9:  code = CoolLogCommand::CoolLogGetCalibration; break;
    case 10: code = CoolLogCommand::CoolLogGetBatLevel;    break;
    case 11: code = CoolLogCommand::CoolLogSetShelfLife;   break;
    case 12: code = CoolLogCommand::CoolLogInitialize;     break;
    case 13: code = CoolLogCommand::CoolLogGetSensorValue; break;
    case 14: code = CoolLogCommand::CoolLogOpenArea;       break;
    case 15: code = CoolLogCommand::CoolLogAccessFIFO;     break;
    default: code = CoolLogCommand::CoolLogUnknown;
    }

    return code;
}

QString SL900ACoolLogCommands::decodeCoolLogCommand ( CoolLogCommand command )
{
    QString answer;
    switch(command)
    {
    case CoolLogCommand::CoolLogSetPassword     : answer = "Set Password"              ; break;
    case CoolLogCommand::CoolLogSetLogMode      : answer = "Set Log Mode"              ; break;
    case CoolLogCommand::CoolLogSetLogLimits    : answer = "Set Log Limits"            ; break;
    case CoolLogCommand::CoolLogGetMeasSetup    : answer = "Get Measurement Setup"     ; break;
    case CoolLogCommand::CoolLogSetSfeParams    : answer = "Set SFE Parameters"        ; break;
    case CoolLogCommand::CoolLogSetCalibration  : answer = "Set Calibration"           ; break;
    case CoolLogCommand::CoolLogEndLog          : answer = "End Log"                   ; break;
    case CoolLogCommand::CoolLogStartLog        : answer = "Start Log"                 ; break;
    case CoolLogCommand::CoolLogGetLogState     : answer = "Get Log State"             ; break;
    case CoolLogCommand::CoolLogGetCalibration  : answer = "Get Calibration Data"      ; break;
    case CoolLogCommand::CoolLogGetBatLevel     : answer = "Get Battery Level"         ; break;
    case CoolLogCommand::CoolLogSetShelfLife    : answer = "Set Shelf Life"            ; break;
    case CoolLogCommand::CoolLogInitialize      : answer = "Set Initialize"            ; break;
    case CoolLogCommand::CoolLogGetSensorValue  : answer = "Get Sensor Value"          ; break;
    case CoolLogCommand::CoolLogOpenArea        : answer = "Open Area"                 ; break;
    case CoolLogCommand::CoolLogAccessFIFO      : answer = "Access FIFO"               ; break;
    default                                     : answer = QString("Unknown Command %1").arg(command); break;
    }
    return answer;
}


void SL900ACoolLogCommands::on_rb_SetPassword_clicked()
{
    sW_Commands->setCurrentIndex(0);
}

void SL900ACoolLogCommands::on_rb_SetLogMode_clicked()
{
    sW_Commands->setCurrentIndex(1);
}

void SL900ACoolLogCommands::on_rb_SetLogLimits_clicked()
{
    sW_Commands->setCurrentIndex(2);
}

void SL900ACoolLogCommands::on_rb_GetMeasurementSetup_clicked()
{
    sW_Commands->setCurrentIndex(3);
}

void SL900ACoolLogCommands::on_rb_SetSFEParameters_clicked()
{
    sW_Commands->setCurrentIndex(4);
}

void SL900ACoolLogCommands::on_rb_SetCalibrationData_clicked()
{
    sW_Commands->setCurrentIndex(5);
}

void SL900ACoolLogCommands::on_rb_EndLog_clicked()
{
    sW_Commands->setCurrentIndex(6);
}

void SL900ACoolLogCommands::on_rb_StartLog_clicked()
{
    sW_Commands->setCurrentIndex(7);
}

void SL900ACoolLogCommands::on_rb_GetLogState_clicked()
{
    sW_Commands->setCurrentIndex(8);
}

void SL900ACoolLogCommands::on_rb_GetCalibrationData_clicked()
{
    sW_Commands->setCurrentIndex(9);
}

void SL900ACoolLogCommands::on_rb_GetBatteryLevel_clicked()
{
    sW_Commands->setCurrentIndex(10);
}

void SL900ACoolLogCommands::on_rb_SetShelfLife_clicked()
{
    sW_Commands->setCurrentIndex(11);
}

void SL900ACoolLogCommands::on_rb_Initialize_clicked()
{
    sW_Commands->setCurrentIndex(12);
}

void SL900ACoolLogCommands::on_rb_GetSensorValue_clicked()
{
    sW_Commands->setCurrentIndex(13);
}

void SL900ACoolLogCommands::on_rb_OpenArea_clicked()
{
    sW_Commands->setCurrentIndex(14);
}

void SL900ACoolLogCommands::on_rb_AccessFIFO_clicked()
{
    sW_Commands->setCurrentIndex(15);
}

void  SL900ACoolLogCommands::on_pb_Enable_All_Calibrations_clicked()
{
    if (pb_Enable_All_Calibrations->isChecked())
    {
        QMessageBox::warning(this, "Enable All Calibration Fields", "Calibration changes can influence the functionality of the SAL900A Tag!");
        pb_Enable_All_Calibrations->setText("Disable All Calibrations");
        
        sp_ad1->setEnabled(true);
        sp_ad2->setEnabled(true);
        sp_selp->setEnabled(true);
        sp_df->setEnabled(true);
        sp_adf->setEnabled(true);
        sp_selp22->setEnabled(true);
        sp_ring_call->setEnabled(true);
        sp_reftc->setEnabled(true);
    }
    else
    {
        pb_Enable_All_Calibrations->setText("Enable All Calibrations");
        sp_ad1->setEnabled(false);
        sp_ad2->setEnabled(false);
        sp_selp->setEnabled(false);
        sp_df->setEnabled(false);
        sp_adf->setEnabled(false);
        sp_selp22->setEnabled(false);
        sp_ring_call->setEnabled(false);
        sp_reftc->setEnabled(false);
    }
}

void SL900ACoolLogCommands::on_cB_Rang_currentIndexChanged ( int index )
{
    switch (index)
    {
    case 0:  sp_Rang->setValue(1);  break;
    case 1:  sp_Rang->setValue(3);  break;
    case 2:  sp_Rang->setValue(4);  break;
    case 3:  sp_Rang->setValue(8);  break;
    case 4:  sp_Rang->setValue(16);  break;
    default: itsParent->logMessage("Wrong Index;");
    }
}

void SL900ACoolLogCommands::on_dsp_Seti_valueChanged( double value )
{
    sp_Seti->setValue(value/0.25);
}

void SL900ACoolLogCommands::on_cB_Ext1_currentIndexChanged ( int index )
{
    switch (index)
    {
    case 0:  sp_Ext1->setValue(0);  break;
    case 1:  sp_Ext1->setValue(1);  break;
    case 2:  sp_Ext1->setValue(3);  break;
    default: itsParent->logMessage("Wrong Index;");
    }
}

void SL900ACoolLogCommands::on_cB_Ext2_currentIndexChanged ( int index )
{
    sp_Ext2->setValue(index);
}

void SL900ACoolLogCommands::on_cB_verifySensorID_currentIndexChanged ( int index )
{
    sp_verifySensorID->setValue(index);
}

void SL900ACoolLogCommands::on_sp_extLowLim_valueChanged( int value )
{
    sp_extLowLimDeg->blockSignals(true);
    sp_extLowLimDeg->setValue(itsParent->tempfromCode(value));
    sp_extLowLimDeg->blockSignals(false);
}

void SL900ACoolLogCommands::on_sp_LowLim_valueChanged( int value )
{
    sp_LowLimDeg->blockSignals(true);
    sp_LowLimDeg->setValue(itsParent->tempfromCode(value));
    sp_LowLimDeg->blockSignals(false);
}

void SL900ACoolLogCommands::on_sp_UpperLim_valueChanged( int value )
{
    sp_UpperLimDeg->blockSignals(true);
    sp_UpperLimDeg->setValue(itsParent->tempfromCode(value));
    sp_UpperLimDeg->blockSignals(false);
}

void SL900ACoolLogCommands::on_sp_extUpperLim_valueChanged( int value )
{
    sp_extUpperLimDeg->blockSignals(true);
    sp_extUpperLimDeg->setValue(itsParent->tempfromCode(value));
    sp_extUpperLimDeg->blockSignals(false);
}

void SL900ACoolLogCommands::on_sp_extLowLimDeg_valueChanged( double value )
{
    sp_extLowLim->blockSignals(true);
    sp_extLowLim->setValue(itsParent->codefromTemp(value));
    sp_extLowLim->blockSignals(false);
}

void SL900ACoolLogCommands::on_sp_LowLimDeg_valueChanged( double value )
{
    sp_LowLim->blockSignals(true);
    sp_LowLim->setValue(itsParent->codefromTemp(value));
    sp_LowLim->blockSignals(false);
}

void SL900ACoolLogCommands::on_sp_UpperLimDeg_valueChanged( double value )
{
    sp_UpperLim->blockSignals(true);
    sp_UpperLim->setValue(itsParent->codefromTemp(value));
    sp_UpperLim->blockSignals(false);
}

void SL900ACoolLogCommands::on_sp_extUpperLimDeg_valueChanged( double value )
{
    sp_extUpperLim->blockSignals(true);
    sp_extUpperLim->setValue(itsParent->codefromTemp(value));
    sp_extUpperLim->blockSignals(false);
}

void SL900ACoolLogCommands::on_pb_Execute_clicked()
{
    int indexCommand = sW_Commands->currentIndex();
    switch (indexCommand)
    {
    case 0:   executeSetPassword();          break;
    case 1:   executeSetLogMode();           break;
    case 2:   executeSetLogLimits();         break;
    case 3:   executeGetMeasurementSetup();  break;
    case 4:   executeSetSFEParameters();     break;
    case 5:   executeSetCalibrationData();   break;
    case 6:   executeEndLog();               break;
    case 7:   executeStartLog();             break;
    case 8:   executeGetLogState();          break;
    case 9:   executeGetCalibrationData();   break;
    case 10:  executeGetBatteryLevel();      break;
    case 11:  executeSetShelfLife();         break;
    case 12:  executeInitialize();           break;
    case 13:  executeGetSensorValue();       break;
    case 14:  executeOpenArea();             break;
    case 15:  executeAccessFIFO();           break;
    default:  itsParent->logMessage("Command is not implemented;");
    }

    // Error Handling
    CoolLogCommand clCom = getCoolLogCommandFromIndex(indexCommand);

    if ( (itsCoolLogError != CoolLogError::CoolLogNoError) || (itsGen2Res != Gen2ReaderInterface::Gen2_OK) )
    {
        itsParent->logMessage(QString("Command: %1 executed").arg(decodeCoolLogCommand(clCom)));
        itsParent->logRequestAndAnswer();
        itsParent->logMessage(QString("Error occurred: Gen2Error: %1; CoolLogError: %2")
                              .arg(itsParent->itsGen2Reader->errorToString(itsGen2Res))
                              .arg(itsParent->decodeCoolLogError(itsCoolLogError)));
    }
    else
    {
        itsParent->logMessage(QString("Command: %1 executed, No Error").arg(decodeCoolLogCommand(clCom)));
        itsParent->logRequestAndAnswer();
    }

}

void SL900ACoolLogCommands::executeSetPassword ( )
{
    char pwLevel = cB_PWLevel->currentIndex() + 1;
    unsigned long password = le_setPW->text().toULong(0, 16);

    itsGen2Res = itsParent->coolLogSetPassword(itsCoolLogError, pwLevel , password);
}

void SL900ACoolLogCommands::executeSetLogMode ( )
{
    SL900AGroupLogMode logMode;
    logMode.storageRule     = cB_StorageRule->currentIndex();
    logMode.extSen1Enable   = cb_ExtSen1Enable->isChecked();
    logMode.extSen2Enable   = cb_ExtSen2Enable->isChecked();
    logMode.tempSenEnable   = cb_TempSenEnable->isChecked();
    logMode.battCheckEnable = cb_BatteryCheckEnable->isChecked();

    unsigned int loggingFormInd = cB_LoggingForm->currentIndex();
    switch (loggingFormInd)
    {
    case 0: logMode.loggingForm = 0; break;
    case 1: logMode.loggingForm = 1; break;
    case 2: logMode.loggingForm = 3; break;
    case 3: logMode.loggingForm = 5; break;
    case 4: logMode.loggingForm = 6; break;
    case 5: logMode.loggingForm = 7; break;
    default: logMode.loggingForm = 0; break;
    }
    unsigned int logInterval      = sp_LogInterval->value() - 1; //sec

    itsGen2Res = itsParent->coolLogSetLogMode(itsCoolLogError, logMode, logInterval);
}

void SL900ACoolLogCommands::executeSetLogLimits ( )
{
    SL900AGroupLimits limits;
    limits.extremeLowerLimit = sp_extLowLim->value();
    limits.extremeUpperLimit = sp_extUpperLim->value();
    limits.lowerLimit = sp_LowLim->value();
    limits.upperLimit = sp_UpperLim->value();
    
    itsGen2Res = itsParent->coolLogSetLogLimits(itsCoolLogError, limits);
}

void SL900ACoolLogCommands::executeGetMeasurementSetup ( )
{
    SL900AGroupLimits limits;
    SL900AGroupLogMode logMode;
    unsigned int logInterval;
    SL900AGroupDelayTime delayTime;
    SL900AGroupUserData userDate;

    QDate gDate;
    QTime gTime;

    itsGen2Res = itsParent->coolLogGetMeasurementSetup(itsCoolLogError, gDate, gTime, limits , logMode, logInterval, delayTime, userDate);
    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        return;
    dateEdit_Start_Display->setDate(gDate);
    timeEdit_Start_Display->setTime(gTime);
    //Limits
    le_extLowLim_Display->setText(QString("%1").arg(limits.extremeLowerLimit));
    le_LowLim_Display->setText(QString("%1").arg(limits.lowerLimit));
    le_UpperLim_Display->setText(QString("%1").arg(limits.upperLimit));
    le_extUpperLim_Display->setText(QString("%1").arg(limits.extremeUpperLimit));
    le_extLowLimDeg_Display->setText(QString("%1 �C").arg(itsParent->tempfromCode(limits.extremeLowerLimit)));
    le_LowLimDeg_Display->setText(QString("%1 �C").arg(itsParent->tempfromCode(limits.lowerLimit)));
    le_UpperLimDeg_Display->setText(QString("%1 �C").arg(itsParent->tempfromCode(limits.upperLimit)));
    le_extUpperLimDeg_Display->setText(QString("%1 �C").arg(itsParent->tempfromCode(limits.extremeUpperLimit)));

    switch (logMode.loggingForm)
    {
    case 0:  le_LogFormDisplay->setText("Dense");            break;
    case 1:  le_LogFormDisplay->setText("Out Of Limits");    break;
    case 3:  le_LogFormDisplay->setText("Limits Crossing");  break;
    case 5:  le_LogFormDisplay->setText("IRQ EXT1");         break;
    case 6:  le_LogFormDisplay->setText("IRQ EXT2");         break;
    case 7:  le_LogFormDisplay->setText("IRQ EXT1 & EXT2");  break;
    default: le_LogFormDisplay->setText(" ");                break;
    }
   
    switch (logMode.storageRule)
    {
    case 0:  le_StorRuleDisplay->setText("Normal");   break;
    case 1:  le_StorRuleDisplay->setText("Rolling");  break;
    default: le_StorRuleDisplay->setText(" ");        break;
    }

    cb_ExtSen1Enable_Display->setChecked(logMode.extSen1Enable);
    cb_ExtSen2Enable_Display->setChecked(logMode.extSen2Enable);
    cb_TempSenEnable_Display->setChecked(logMode.tempSenEnable);
    cb_BatteryCheckEnable_Display->setChecked(logMode.battCheckEnable);

    //Log Interval + Delay Time
    le_LogIntervalDisplay->setText(QString("%1").arg(logInterval + 1)); //sec.
    le_DelayTimeDisplay->setText(QString("%1").arg(delayTime.delayTime));
    cb_IRQEnableDisplay->setChecked(delayTime.irqEnalbe);

    switch (delayTime.delayMode)
    {
    case 0:  le_DelayModeDisplay->setText("Timer");        break;
    case 1:  le_DelayModeDisplay->setText("Ext. Switch");  break;
    default: le_DelayModeDisplay->setText(" ");            break;
    }
    //Application Data
    le_NumOfWordsAppDisplay->setText(QString("%1").arg(userDate.numWordsAppData));
    le_BrokenWordPointerDisplay->setText(QString("%1").arg(userDate.brokenWordPointer));

}

void SL900ACoolLogCommands::executeSetSFEParameters ( )
{
    SL900AGroupSFEParameters sfeParams;
    sfeParams.rang     = sp_Rang->value();
    sfeParams.seti     = sp_Seti->value();
    sfeParams.ext1     = sp_Ext1->value();
    sfeParams.ext2     = sp_Ext2->value();
    sfeParams.autodis  = cb_autoRangeDisable->isChecked();
    sfeParams.verifyId = sp_verifySensorID->value();

    itsGen2Res = itsParent->coolLogSetSFEParameters(itsCoolLogError, sfeParams);
}

void SL900ACoolLogCommands::executeSetCalibrationData ( )
{
    if (!itsCalDataLoaded)
    {
        itsParent->logMessage("Warning: Calibration Data not loaded!");
        itsParent->logMessage("Press Load Calibration Data to load actual Data!");
        return;
    }

    SL900AGroupCalibration calibration;

    calibration.ad1       = sp_ad1->value();
    calibration.coars1    = sp_coars1->value();
    calibration.ad2       = sp_ad2->value();
    calibration.coars2    = sp_coars2->value();
    calibration.gnd_sw    = cb_gndSW->isChecked();
    calibration.selp      = sp_selp->value();
    calibration.adf       = sp_adf->value();
    calibration.df        = sp_df->value();
    calibration.sw_ext_en = cb_swExtEn->isChecked();
    calibration.selp22    = sp_selp22->value();
    calibration.irlev     = sp_irlev->value();
    calibration.ring_call = sp_ring_call->value();
    calibration.off_int   = sp_off_int->value();
    calibration.reftc     = sp_reftc->value();
    calibration.exc_res   = cb_exc_res->isChecked();

    itsGen2Res = itsParent->coolLogSetCalibrationData(itsCoolLogError,calibration);

}

void SL900ACoolLogCommands::executeEndLog ( )
{
    itsGen2Res = itsParent->coolLogEndLog(itsCoolLogError);
}

void SL900ACoolLogCommands::executeStartLog ( )
{
    itsGen2Res = itsParent->coolLogStartLog(itsCoolLogError, dateEdit_Start->date(),timeEdit_Start->time());
}

void SL900ACoolLogCommands::executeGetLogState ( )
{
    SL900AGroupLimitsCounter limitCounter;
    SL900AGroupSystemStatus systemStatus;
    SL900AStatusFlags flags;
    SL900AGroupShelfLife shelfLife;
    unsigned long int currentShelfLife;
    itsGen2Res = itsParent->coolLogGetLogState(itsCoolLogError, limitCounter,systemStatus, flags, shelfLife, currentShelfLife);
    
    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        return;

    // Limit Counter
    l_CountExtLow_Display->setText(QString("%1").arg(limitCounter.extremeLowerCounter));
    l_CountLow_Display->setText(QString("%1").arg(limitCounter.lowerCounter));
    l_CountUpp_Display->setText(QString("%1").arg(limitCounter.upperCounter));
    l_CountExtUpp_Display->setText(QString("%1").arg(limitCounter.extremeUpperCounter));

    // System Status
    le_MeasPointer_Display->setText(QString("%1").arg(systemStatus.measAddressPointer));
    le_NumMemRep_Display->setText(QString("%1").arg(systemStatus.memoryReplacements));
    le_NumMeas_Display->setText(QString("%1").arg(systemStatus.measurements));
    le_Active_Display->setText(QString("%1").arg(systemStatus.active));
    
    //Status Flag
    cb_Active_Display->setChecked(flags.activeLogging);
    cb_MeasAreaFull_Display->setChecked(flags.areaFull);
    cb_MeasOverwritten_Display->setChecked(flags.overwritten);
    cb_ADError_Display->setChecked(flags.adError);
    cb_LowBattery_Display->setChecked(flags.lowBattery);
    cb_ShelfLifeLowErr_Display->setChecked(flags.shelfLifeLowError);
    cb_ShelfLifeHighErr_Display->setChecked(flags.shelfLifeHighError);
    cb_ShelfLifeExpired_Display->setChecked(flags.shelfLifeExpired);

    // ShelfLife Block and Current Shelf Life
    le_TmaxDisplay->setText(QString("%1").arg(shelfLife.tmax));
    le_TminDisplay->setText(QString("%1").arg(shelfLife.tmin));
    le_TsdDisplay->setText(QString("%1").arg(shelfLife.tstd));
    le_EaDisplay->setText(QString("%1").arg(shelfLife.ea));

    le_SLinitDisplay->setText(QString("%1").arg(shelfLife.slinit));
    le_TinitDisplay->setText(QString("%1").arg(shelfLife.tinit));
    cb_NegShelfLifeDisplay->setChecked(shelfLife.negShelfLife);
    cb_ShelfLifeAlgoDisplay->setChecked(shelfLife.algoShelfLife);

    switch (shelfLife.shelfLifeSensor)
    {
    case 0:  le_shelfLifeSensorDisplay->setText("Temperature"); break;
    case 1:  le_shelfLifeSensorDisplay->setText("External 1 "); break;
    case 2:  le_shelfLifeSensorDisplay->setText("External 2 "); break;
    case 3:  le_shelfLifeSensorDisplay->setText("Battery    "); break;
    default: le_shelfLifeSensorDisplay->setText("           "); break;
    }

    le_CurrShelfLifeDisplay->setText(QString("%1").arg(currentShelfLife));
}

void SL900ACoolLogCommands::executeGetCalibrationData ( )
{
    SL900AGroupCalibration calibration;
    SL900AGroupSFEParameters sfeParameters;
     
    itsGen2Res = itsParent->coolLogGetCalibrationData(itsCoolLogError,calibration, sfeParameters);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        return;

    le_ad1_Display->setText(QString("%1").arg(calibration.ad1));
    le_coars1_Display->setText(QString("%1").arg(calibration.coars1));
    le_ad2_Display->setText(QString("%1").arg(calibration.ad2));
    le_coars2_Display->setText(QString("%1").arg(calibration.coars2));

    cb_gndSW_Display->setChecked(calibration.gnd_sw);
    le_selp_Display->setText(QString("%1").arg(calibration.selp));
    le_adf_Display->setText(QString("%1").arg(calibration.adf));
    le_df_Display->setText(QString("%1").arg(calibration.df));

    cb_swExtEn_Display->setChecked(calibration.sw_ext_en);
    le_selp22_Display->setText(QString("%1").arg(calibration.selp22));
    le_irlev_Display->setText(QString("%1").arg(calibration.irlev));
    le_ring_call_Display->setText(QString("%1").arg(calibration.ring_call));
    le_off_int_Display->setText(QString("%1").arg(calibration.off_int));
    le_reftc_Display->setText(QString("%1").arg(calibration.reftc));
    cb_exc_res_Display->setChecked(calibration.exc_res);

    le_Rang_Display->setText(QString("%1").arg(sfeParameters.rang));
    le_Seti_Display->setText(QString("%1").arg(sfeParameters.seti));
    le_Ext1_Display->setText(QString("%1").arg(sfeParameters.ext1));
    le_Ext2_Display->setText(QString("%1").arg(sfeParameters.ext2));
    le_autoRangeDisable_Display->setText(QString("%1").arg(sfeParameters.autodis));
    le_verifySensorID_Display->setText(QString("%1").arg(sfeParameters.verifyId));

}

void SL900ACoolLogCommands::executeGetBatteryLevel ( )
{
    char battRetrigger = cb_BattRetrigger->currentIndex();
    bool adError;
    SL900Battype battType;
    unsigned int batteryLevel;

    itsGen2Res = itsParent->coolLogGetBatteryLevel(itsCoolLogError, battRetrigger, adError, battType, batteryLevel);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        return;

    le_ADErrorGetBatLevDisplay->setText(QString("%1").arg(adError));
    le_BatLevel->setText(QString("%1").arg(batteryLevel)); 
    
    if (battType == SL900Battype::Bat1V5) 
    {
        le_BatTypeDisplay->setText(QString("%1").arg("1.5V"));
        le_BatteryVoltageDisplay->setText(QString("%1 V").arg(itsParent->battVfromCode(batteryLevel, SL900Battype::Bat1V5)));
    }
    else if  (battType == SL900Battype::Bat3V0) 
    {
        le_BatTypeDisplay->setText(QString("%1").arg("3.0V"));
        le_BatteryVoltageDisplay->setText(QString("%1 V").arg(itsParent->battVfromCode(batteryLevel, SL900Battype::Bat3V0)));
    }

}

void SL900ACoolLogCommands::executeSetShelfLife ( )
{
    SL900AGroupShelfLife shelfLife;

    shelfLife.tmax   = sp_Tmax->value();
    shelfLife.tmin   = sp_Tmin->value();
    shelfLife.tstd   = sp_Tsd->value();
    shelfLife.ea     = sp_Ea->value();
    shelfLife.slinit = sp_SLinit->value();
    shelfLife.tinit  = sp_Tinit->value();
    shelfLife.shelfLifeSensor = cB_shelfLifeSensor->currentIndex();
    shelfLife.negShelfLife    = cb_negShelfLife->isChecked();
    shelfLife.algoShelfLife   = cb_AlgoShelfLife->isChecked();
    shelfLife.logInterval     = cB_LogInterval->currentIndex();
    
    itsGen2Res = itsParent->coolLogSetShelfLife(itsCoolLogError, shelfLife);
}

void SL900ACoolLogCommands::executeInitialize ( )
{
    SL900AGroupUserData userData;
    SL900AGroupDelayTime delayTime;
    delayTime.delayTime = sp_DelayTime->value();
    delayTime.delayMode = cB_DelayMode->currentIndex();
    delayTime.irqEnalbe = cb_IRQenable->isChecked();
    userData.numWordsAppData = sp_numOfWords->value();
    userData.brokenWordPointer = sp_BrokenWordPointer->value();

    itsParent->coolLogInitilize(itsCoolLogError, delayTime, userData);
}

void SL900ACoolLogCommands::executeGetSensorValue ( )
{
    unsigned char sensorType = cb_sensorType->currentIndex();
    bool adError;
    unsigned char rangeLim;
    unsigned int sensorVal;

    itsGen2Res = itsParent->coolLogGetSensorValue(itsCoolLogError,sensorType, adError, rangeLim, sensorVal);

    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        return;

    le_ADErrorDisplay->setText(QString("%1").arg(adError));
    le_RangeLimitDisplay->setText(QString("%1").arg(rangeLim));
    le_SensorValue->setText(QString("%1").arg(sensorVal));
    if (sensorType == 0 )
        l_CalcResult->setText(QString("%1").arg(itsParent->tempfromCode(sensorVal)));
    else
        l_CalcResult->setText(QString(""));

}

void SL900ACoolLogCommands::executeOpenArea ( )
{
    char pwLevel = cB_openPWLevel->currentIndex() + 1;
    unsigned long password = le_openPW->text().toULong(0, 16);

    itsGen2Res = itsParent->coolLogOpenArea(itsCoolLogError, pwLevel , password);
}

void SL900ACoolLogCommands::executeAccessFIFO ( )
{
    QByteArray writePayload, readPayload;
    int subcommand = 0x0;

    if (cB_subcommandFifo->currentIndex() == 0)
        subcommand = 0x80;
    else if (cB_subcommandFifo->currentIndex() == 1)
        subcommand = 0xA0;
    else if (cB_subcommandFifo->currentIndex() == 2)
        subcommand = 0xC0;
    else
        return; // not implemented!

    if (subcommand == 0xA0) // write
    {
        QString writeData = le_writeDataFifo->text(); //max 64bits Long
        QStringList bytes = writeData.split(QChar('-'));
        if (bytes[0].isEmpty())
        {
            itsParent->logMessage("Write Data Field: insert Data, direction from right to left");
            return;
        }

        int appendedBytes = 0;
        unsigned char bytetoAdd;

        for (int count1 = 0; count1 < bytes.length(); count1++)
        {
            if (bytes[count1].isEmpty())
            {
                break; // No more Bytes
            }
            else
            {
                bytetoAdd = bytes[count1].toInt(0,16) & 0xFF;
                writePayload.append(bytetoAdd);
                appendedBytes++;
            }

        }
        itsGen2Res = itsParent->coolLogAccessFIFO(itsCoolLogError, subcommand, writePayload, readPayload);
    }
    else if (subcommand == 0x80) // read
    {
        itsParent->logMessage("Info: Read of FIFO Status, before Read FIFO is done");

        itsGen2Res = itsParent->coolLogAccessFIFO(itsCoolLogError, subcommand, writePayload, readPayload);
        if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
            return;
        QString byteAw(readPayload.toHex());
        le_readDataFifo->setText(byteAw);
    }
    else if (subcommand == 0xC0) // status
    {
        itsGen2Res = itsParent->coolLogAccessFIFO(itsCoolLogError, subcommand, writePayload, readPayload);
        if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
            return;

        bool fifoB   = (readPayload[0] & 0x80) >> 7;
        bool dataR   = (readPayload[0] & 0x40) >> 6;
        bool noData  = (readPayload[0] & 0x20) >> 5;
        bool spiRfid = (readPayload[0] & 0x10) >> 4;
        unsigned int numBytesAvail = (readPayload[0] & 0x0F);
        cb_fifoBusy->setChecked(fifoB);
        cb_dataReady->setChecked(dataR);
        cb_NoData->setChecked(noData);
        if (spiRfid)
        {
            le_SPIRFIDDisplay->setText("RFID");
        }
        else
        {
            le_SPIRFIDDisplay->setText("SPI");
        }
        le_BytesAvailable->setText(QString("%1").arg(numBytesAvail));
    }

}

void SL900ACoolLogCommands::on_pb_NowDateTime_clicked ( )
{
    QTime time;
    QDate date;
    timeEdit_Start->setTime(time.currentTime());
    dateEdit_Start->setDate(date.currentDate());
}

void SL900ACoolLogCommands::on_pb_LoadCalData_clicked ( )
{
    itsCalDataLoaded = calibrationDataToSetCalibration();
}

bool SL900ACoolLogCommands::calibrationDataToSetCalibration ( )
{
    SL900AGroupCalibration calibration;
    SL900AGroupSFEParameters sfeParameters;
    
    itsGen2Res = itsParent->coolLogGetCalibrationData(itsCoolLogError, calibration, sfeParameters);
        
    if (itsGen2Res != Gen2ReaderInterface::Gen2_OK)
        return false;
    
    sp_ad1->setValue(calibration.ad1);
    sp_coars1->setValue(calibration.coars1);
    sp_ad2->setValue(calibration.ad2);
    sp_coars2->setValue(calibration.coars2);

    cb_gndSW->setChecked(calibration.gnd_sw);
    sp_selp->setValue(calibration.selp);
    sp_adf->setValue(calibration.adf);
    sp_df->setValue(calibration.df);

    cb_swExtEn->setChecked(calibration.sw_ext_en);
    sp_selp22->setValue(calibration.selp22);
    sp_irlev->setValue(calibration.irlev);
    sp_ring_call->setValue(calibration.ring_call);
    sp_off_int->setValue(calibration.off_int);
    sp_reftc->setValue(calibration.reftc);
    cb_exc_res->setChecked(calibration.exc_res);

    return true;
}
