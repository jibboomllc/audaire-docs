/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "CGen2MovementDialog.h"
#include "../../reader/ReaderInterface.h"
#include <QCloseEvent>
#include <QMessageBox>

#define _USE_MATH_DEFINES
#include <math.h>


CGen2MovementDialog::CGen2MovementDialog(QWidget *parent)
    : QDialog(parent), QrfeTraceModule("Movement Detector"), itsTimer(), m_reader(NULL)
{
	ui.setupUi(this);

	QObject::connect(ui.buttonBox, 			SIGNAL(clicked(QAbstractButton*)), 				this, SLOT(close(QAbstractButton*)));
    QObject::connect(ui.buttonBox, 			SIGNAL(rejected()), 				this, SLOT(reject()));
    QObject::connect(&itsTimer, SIGNAL(timeout()), this, SLOT(cycle()));

    itsTimer.setSingleShot(true);

}

CGen2MovementDialog::~CGen2MovementDialog()
{
}

int CGen2MovementDialog::exec(Gen2ReaderInterface* reader, Gen2Tag &tag)
{
	
    m_reader = dynamic_cast<AmsReader*>(reader);

    if( m_reader->getHardwareId() == ReaderInterface::Newton)
    {
        ui.frequency_sb->setValue(915.0);
    }

	clearInfo();

	ui.tagIdEdit->setText(tag.epc);
    ui.start_stop_btn->setText("Stop");
    ui.start_stop_btn->setChecked(true);

    Gen2ReaderInterface::Gen2Result gen2res = Gen2ReaderInterface::Gen2_OK;
    gen2res = m_reader->selectTag(tag);
    if(gen2res != Gen2ReaderInterface::Gen2_OK) 
    {
        ui.infoEdit->append("Select Tag failed!");
        ui.infoEdit->append("Movement Detector could not be started!");
        ui.infoEdit->append("Try to close and open Movement Detector again!");
        ui.start_stop_btn->setEnabled(false);
        itsTimer.stop();
        return QDialog::exec();
    }

    itsTimer.setInterval(1); //~start immediately
    itsTimer.start();

    trc(0x01, "Movement Detector started");
	return QDialog::exec();
}

void CGen2MovementDialog::cycle(void)
{
    static double old_phase;
    long sum_i = 0, sum_q = 0, sum_pbit = 0;
    double mean_i, mean_q, mean_pbit;
    double phase;
    int cnt = 0;
    
    QList<struct rssi_val> list(m_reader->rssiCommand(ui.burst_length_sb->value(), static_cast<ulong>(ui.frequency_sb->value()*1000)));
    ui.infoEdit->clear();
    ui.infoEdit->append("agc_reg;log_irssi;log_qrssi;lin_irssi;lin_qrssi");
    
    foreach (const struct rssi_val &r, list)
    {
        if (r.agc == 0 && r.lin_irssi == -128 && r.lin_qrssi == -128)
            continue;
        cnt++;
        sum_i += r.lin_irssi + 128;
        sum_q += r.lin_qrssi + 128;
        sum_pbit += r.agc >> 7; // extract subc_phase bit
        ui.infoEdit->append(QString("0x%1;%2;%3;%4;%5").arg(r.agc,2,16,QChar('0')).arg(r.log_rssis&0xf).arg(r.log_rssis>>4).arg(r.lin_irssi).arg(r.lin_qrssi));
    }
    if (0==cnt)
    {
        mean_i = 0;
        mean_q = 0;
        mean_pbit = 0.5;
    }
    else
    {
        mean_i = sum_i / cnt;
        mean_q = sum_q / cnt;
        mean_pbit = sum_pbit / cnt;
    }

    ui.q_result_le->setText(QString("%1").arg(mean_q, 0, 'f', 1));
    ui.i_result_le->setText(QString("%1").arg(mean_i, 0, 'f', 1));
    ui.phase_bit_le->setText(QString("%1").arg(mean_pbit, 0, 'f', 1));

    phase = 360.0 * (atan(mean_q/mean_i)/(2*M_PI));
    
    ui.phase_le ->setText(QString("%1�").arg(phase, 0, 'f', 1));
    ui.delta_phase_le->setText(QString("%1�").arg(phase - old_phase, 0, 'f', 2));
    
    trc(0x09, QString("RSSI I: %1, RSSI Q: %2, Phase: %3, Delta Phase: %4, Phase Bit: %5")
        .arg(mean_i, 0, 'f', 1).arg(mean_q, 0, 'f', 1)
        .arg(phase, 0, 'f', 1).arg(phase - old_phase, 0, 'f', 2).arg(mean_pbit, 0, 'f', 1));

    if (abs(phase-old_phase) > ui.phase_thresh_sb->value())
    {
        ui.indicator_lb->setPixmap(QPixmap(":/button icons/moving"));
    }
    else
    {
        ui.indicator_lb->setPixmap(QPixmap(":/button icons/standing"));
    }

    old_phase = phase;

    itsTimer.setInterval(ui.delta_t_sb->value());
    itsTimer.start();
}
void CGen2MovementDialog::closeEvent ( QCloseEvent * event )
{
    itsTimer.stop();
    event->accept();
}

void CGen2MovementDialog::close( QAbstractButton * button )
{
    itsTimer.stop();
}

void CGen2MovementDialog::handleError(Gen2ReaderInterface::Gen2Result result, QString text)
{
    ui.infoEdit->setText("-- " + m_reader->errorToString(result) + " - ERROR --");
	if(result == Gen2ReaderInterface::Gen2_OK)
	{
		showOK();
	}
	else if(result == Gen2ReaderInterface::Gen2_TAG_UNREACHABLE)
	{
		showTagUnreachable();
	}
	else if(result == Gen2ReaderInterface::Gen2_WRONG_PASSW)
	{
		showWrongPassword();
	}
	else if(result == Gen2ReaderInterface::Gen2_Tag_MEM_OVERRUN)
	{
		showMemoryOverrun();
	}
	else if(result == Gen2ReaderInterface::Gen2_Tag_MEM_LOCKED)
	{
		showMemoryLocked();
	}
	else if(result == Gen2ReaderInterface::Gen2_Tag_INSUFFICIENT_POWER)
	{
		showInsufficentPower();
	}
	else{
		showNOK();
	}
}

void CGen2MovementDialog::on_start_stop_btn_clicked()
{
    if (ui.start_stop_btn->isChecked())
    {
        ui.start_stop_btn->setText("Stop");
        itsTimer.setInterval(1);
        itsTimer.start();
    }
    else
    {
        ui.start_stop_btn->setText("Start");
        itsTimer.stop();
    }
}

void CGen2MovementDialog::clearInfo()
{
    ui.infoEdit->setStyleSheet("");
	ui.infoEdit->clear();
}

void CGen2MovementDialog::showOK()
{
    ui.infoEdit->setStyleSheet("background-color:lightgreen;");
}

void CGen2MovementDialog::showTagUnreachable()
{
    ui.infoEdit->setStyleSheet("background-color:gray;");
}

void CGen2MovementDialog::showWrongPassword()
{
    ui.infoEdit->setStyleSheet("background-color:darkred;");
}

void CGen2MovementDialog::showMemoryOverrun()
{
    ui.infoEdit->setStyleSheet("background-color:lightpink;");
}

void CGen2MovementDialog::showMemoryLocked()
{
    ui.infoEdit->setStyleSheet("background-color:lightcyan;");
}

void CGen2MovementDialog::showInsufficentPower()
{
    ui.infoEdit->setStyleSheet("background-color:lightblue;");
}

void CGen2MovementDialog::showNOK()
{
    ui.infoEdit->setStyleSheet("background-color:lightcoral;");
}
