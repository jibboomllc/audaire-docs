/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "CSettingsDialog.h"

#include <QMessageBox>

CSettingsDialog::CSettingsDialog(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	QObject::connect(ui.buttonBox, 			SIGNAL(accepted()), 					this, SLOT(tryAccept()));

    m_showAlias 			= false;

    m_useTtl 				= true;
    m_msecsToShowInactive 	= 1000;
    m_msecsToShowOutOfRange = 5000;
    m_msecsToDelete			= 15000;

    m_useTrace				= false;
    m_traceLevel 			= 0;

    m_useMultiplex 			= false;
    m_multiplexTime 		= 500;

}

CSettingsDialog::~CSettingsDialog()
{
}

int CSettingsDialog::exec()
{
	ui.showAliasCheckBox->setChecked(m_showAlias);

	ui.useTtlCheckBox->setChecked(m_useTtl);
	ui.inactiveSpinBox->setValue(m_msecsToShowInactive);
	ui.outOfRangeSpinBox->setValue(m_msecsToShowOutOfRange);
	ui.deleteSpinBox->setValue(m_msecsToDelete);

	ui.useTraceBox->setChecked(m_useTrace);

    ui.cbTraceApp->setChecked(m_traceLevel & 0x01);
    ui.cbTraceReader->setChecked(m_traceLevel & 0x02);
    ui.cbTraceUSB->setChecked(m_traceLevel & 0x04);
    ui.cbTraceTags->setChecked(m_traceLevel & 0x08);
    ui.cbTraceGen2->setChecked(m_traceLevel & 0x10);


	ui.useMultiplexBox->setChecked(m_useMultiplex);
	ui.multiplexReadTimeBox->setValue(m_multiplexTime);

	return QDialog::exec();
}

void CSettingsDialog::tryAccept()
{
    m_useTrace = ui.useTraceBox->isChecked();
    m_traceLevel  = ui.cbTraceApp->isChecked();
    m_traceLevel += ui.cbTraceReader->isChecked() << 1;
    m_traceLevel += ui.cbTraceUSB->isChecked() << 2;
    m_traceLevel += ui.cbTraceTags->isChecked() << 3;
    m_traceLevel += ui.cbTraceGen2->isChecked() << 4;
    
    if(ui.inactiveSpinBox->value() <= ui.outOfRangeSpinBox->value() && ui.outOfRangeSpinBox->value() <= ui.deleteSpinBox->value())
	{
		m_showAlias = ui.showAliasCheckBox->isChecked();

		m_useTtl = ui.useTtlCheckBox->isChecked();
		m_msecsToShowInactive = ui.inactiveSpinBox->value();
		m_msecsToShowOutOfRange = ui.outOfRangeSpinBox->value();
		m_msecsToDelete = ui.deleteSpinBox->value();

		m_useMultiplex = ui.useMultiplexBox->isChecked();
		m_multiplexTime = ui.multiplexReadTimeBox->value();

		accept();
	}
	else
	{
		QMessageBox::critical(this, "Error", "The time settings must be in order.");
	}

}

bool CSettingsDialog::showAlias()
{
	return m_showAlias;
}

void CSettingsDialog::setShowAlias(bool on)
{
	m_showAlias = on;
}


bool CSettingsDialog::useTtl()
{
	return m_useTtl;
}

void CSettingsDialog::setUseTtl(bool on)
{
	m_useTtl = on;
}


uint CSettingsDialog::msecsToShowInactive()
{
	return m_msecsToShowInactive;
}


uint CSettingsDialog::msecsToShowOutOfRange()
{
	return m_msecsToShowOutOfRange;
}

uint CSettingsDialog::msecsToDelete()
{
	return m_msecsToDelete;
}

bool CSettingsDialog::useTrace()
{
	return m_useTrace;
}

uchar CSettingsDialog::traceLevel()
{
	return m_traceLevel;
}


bool CSettingsDialog::useMultiplex()
{
	return m_useMultiplex;
}

uint CSettingsDialog::multiplexTime()
{
	return m_multiplexTime;
}
