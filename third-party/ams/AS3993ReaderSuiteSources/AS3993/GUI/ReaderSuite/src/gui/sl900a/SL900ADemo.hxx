/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef SL900A_DEMO_HXX
#define SL900A_DEMO_HXX

#include <QtGui/QDialog>
#include "qwt_plot.hxx"
#include "qwt_plot_curve.h"
#include "qwt_legend.hxx"
#include "qwt_scale_widget.hxx"
#include "qwt_plot_marker.h"
#include "qwt_symbol.h"
#include "qwt_legend_itemmanager.h"

#include "ui_SL900ADemo.h"
#include "SL900AMainDialog.hxx"
#include "SL900A_globals.h"

#include "../../reader/ReaderInterface.h"
#include "../../reader/Gen2ReaderInterface.h"
#include "../../reader/epc/EPC_Defines.h"
#include "../helper/QrfeProgressBar.h"
#include "QTimer"

class SL900AMainDialog;


class SL900ADemo : public QDialog, public Ui_SL900ADemoClass
{
    Q_OBJECT

public:
    SL900ADemo  ( SL900AMainDialog *parent = NULL );
    ~SL900ADemo ( ) {};

private slots:
    void poll ( );
    
    void on_pb_RunTempDemo_clicked ( );
    void on_pb_RunOutOfFieldDemo_clicked ( );
    void on_pb_ReadMeas_clicked ( );
    void on_pb_PlotTagData_clicked ( );
private:

    SL900AMainDialog    *itsParent;
    
    Gen2ReaderInterface::Gen2Result itsGen2Res;
    CoolLogError itsCoolLogError; 

    QTimer itsDemoTimer;
    int itsInterval;
    int itsActiveFlagFailCounter;
    QList< float> itsListDirectTempMeasure;

    // SL900A Register Groups
    SL900AGroupLimits        itsLimits;
    SL900AGroupLimitsCounter itsLimitsCounter;
    SL900AGroupSystemStatus  itsSystemStatus;
    SL900AGroupDelayTime     itsDelayTime;
    SL900AGroupUserData      itsUserData;
    SL900AGroupLogMode       itsLogMode;
    unsigned int             itslogInterval;
    SL900AStatusFlags        itsStatusFlags;
    SL900Battype             itsBattype; 
    SL900AGroupShelfLife     itsShelfLife;
    unsigned long int        itsCurrentShelfLife;
    QDate                    itsMDate;
    QTime                    itsMTime;

    // Variables for Out of Field Demo
    QList<quint16> itsListMeasurementValues;
    int itsLastReadPointer;

    //Log Curves
    QwtPlotCurve itsTempcurve;
    QVector<qreal> itsTempValsY;
    QVector<qreal> itsTempValsX;	
    QwtPlotCurve itsBattcurve;
    QVector<qreal> itsBattValsY;
    QVector<qreal> itsBattValsX;
    QwtPlotCurve itsExtUppcurve;
    QVector<qreal> itsExtUppLimValsY;
    QVector<qreal> itsExtUppLimValsX;
    QwtPlotCurve itsUppcurve;
    QVector<qreal> itsUppLimValsY;
    QVector<qreal> itsUppLimValsX; 
    QwtPlotCurve itsLowcurve;
    QVector<qreal> itsLowLimValsY;
    QVector<qreal> itsLowLimValsX;
    QwtPlotCurve itsExtLowcurve;
    QVector<qreal> itsExtLowLimValsY;
    QVector<qreal> itsExtLowLimValsX;


    void pollTemperature ( );
    void pollOutofFieldDemo ( );

    void runContinousTempMeasDemo ( );
    void runOutOfFieldDemo ( );

    void stopTheDemo ( );
    bool updateMeasBlocks ( );
    void clearCurves ( );
    void createLimitCurves ( );
    void createLoggingFormCurves ( );
    
};

#endif // SL900A_DEMO_HXX
