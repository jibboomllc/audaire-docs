/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef CGEN2TAGDIALOG_H
#define CGEN2TAGDIALOG_H

#include <QtGui/QDialog>
#include "ui_CGen2TagDialog.h"

#include "CGen2MovementDialog.h"
#include "SL900AMainDialog.hxx"
#include "CBitSequenceEditor.hxx"

#include "../../reader/ReaderInterface.h"
#include "../../reader/Gen2ReaderInterface.h"
#include "../../reader/epc/EPC_Defines.h"

#include "../helper/QrfeProgressBar.h"

class CGen2TagDialog : public QDialog, private Ui_CGen2TagDialogClass
{
    Q_OBJECT

public:
    CGen2TagDialog(QWidget *parent = 0);
    ~CGen2TagDialog();

public slots:
    int exec ( ReaderInterface* ph, Gen2Tag tagId );
    void on_tagDialogButtonBox_accepted();
    void bitStreamEditorTryAccept();

private slots:
    void on_refreshTIDButton_clicked();
    void on_rssiButton_clicked();

    void on_rb_TagMem_clicked();
    void on_rb_SetEPC_clicked();
    void on_rb_SetPassword_clicked();
    void on_rb_Lock_clicked();
    void on_rb_Kill_clicked();
    void on_rb_GenericCommand_clicked();
    void on_rb_SL900A_clicked();

    void on_readMemButton_clicked();
    void on_setMemButton_clicked();
    void on_memBankBox_currentIndexChanged(int index);
    void on_memoryTableWidget_itemChanged(QTableWidgetItem*);
    void on_setEPCButton_clicked();
    void on_lengthBox_valueChanged(int len);
    void on_setPasswordButton_clicked();
    void on_lockButton_clicked();
    void loadDescription();
    void on_killTagButton_clicked();
    void on_pbGenericCMD_clicked();
    void on_pb_OpenBitStreamEditor_clicked(bool clicked);    

signals:
    void easterKeyUnlocked();

private:
    bool readTIDRegisterFromTag(QrfeProgressBar* pb = 0);
    bool readMemoryFromTag(MEMORY_BANK bank, QrfeProgressBar* pb = 0, uint size = 0);
    void handleError(Gen2ReaderInterface::Gen2Result result, QString text);
    void clearInfo();
    void showOK();
    void showTagUnreachable();
    void showWrongPassword();
    void showMemoryOverrun();
    void showMemoryLocked();
    void showInsufficentPower();
    void showNOK();

    ReaderInterface* m_ph;
    Gen2ReaderInterface* m_gen2Reader;

    Gen2Tag m_tag;
    
    MEMORY_BANK m_readBank;
    bool     m_readActive;
    int     m_lowestChanged;
    int     m_highestChanged;
    uint    m_readMemSize;

    CGen2MovementDialog* m_moveDialog;
    SL900AMainDialog*    m_Sl900AMainDialog;

    int m_initialLengthEPC;
    int m_currentLengthEPC;

    QPair<QString, int> m_bitSequenceEditResult;
    CBitSequenceEditor m_bitSequenceEditor;
    QString m_commandGeneric;
    double m_rxBitLengthGeneric;
    double m_txBitLengthGeneric;

};

#endif // CGEN2TAGDIALOG_H
