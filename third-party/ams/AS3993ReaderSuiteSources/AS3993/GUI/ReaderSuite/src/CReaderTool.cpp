/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 * @file	CReaderTool.cpp
 * @brief	Main class of the app
 */

#include "CReaderTool.h"
#include "CTagViewManager.h"
#include "CReadRateCalc.h"
#include "CTagManager.h"
#include "action/CActionHandler.h"
#include "gui/CSettingsDialog.h"
#include "gui/CTagListView.h"
#include "gui/CTagSettingsDialog.h"
#include "gui/readerSettings/CReaderConfigDialog.h"
#include "gui/tagSettings/CGen2TagDialog.h"
#include "reader/ReaderInterface.h"
#include "gui/key/QrfeKeyWindow.h"

#include "register_map.hxx"
#include "register_xml.h"
#include "usb_hid_wrapper.hxx"
#include "AMSBootloader.h"
#include "AMSFirmwareUpgrader.h"
#include "PicUSBBootloader.h"
#include "AmsFirmwareCheck.h"
#include "AMSTrace.hxx"
#include "UpgradeProgressDialog.hxx"
#include "GetVersion.hxx"

#include <QTimer>
#include <QMessageBox>
#include <QInputDialog>
#include <QSplashScreen>
#include <QBitmap>

#include "enumser.h"

/*
 * @brief 	Constructor of the main class
 * The Constructor initializes all needed variables and one instance of every used dialog.
 */
CReaderTool::CReaderTool()
    : AMSMainWindow()
	, QrfeTraceModule("Reader_Tool")
{
	int alignment = Qt::AlignHCenter | Qt::AlignBottom;

	/* Init variables */
	m_scanActive = false;
    m_scanEndMode = Seconds;

	/* Setup the ui */
	ui.setupUi(mainWidget);
    //AMSTrace * tracer = AMSTrace::getInstance();
    //tracer->init(0, AMSTrace::Trace2Signal);
    //QObject::connect(tracer, SIGNAL(traceSignal(QString)), ui.traceBrowser, SLOT(append(QString)));

    this->setupMenus();
    this->setStatusBar(0);      //do not display statusbar
    this->resize(900, 600);

	splash->showMessage(tr("Create Handlers..."), alignment, Qt::white);

	/* Create Action Handler */
	m_actionHandler = new CActionHandler(&m_dataHandler, this);

	/* Create Read Rate Calculator */
	m_readRateCalc = new CReadRateCalc(this);

	/* Create Tag Manager */
	m_tagManager = new CTagManager(this);

	m_amsComWrapper = new USBHIDWrapper(0x0);

	splash->showMessage(tr("Create Dialogs..."), alignment, Qt::white);

	/* Create Dialogs */
	m_keyDialog = new QrfeKeyWindow(this);
	m_tagListDialog = new CTagListView(m_tagManager, this);
	m_settingsDialog = new CSettingsDialog(this);
	m_tagSettingsDialog = new CTagSettingsDialog(&m_dataHandler, this);
	m_gen2SettingsDialog = new CGen2TagDialog(this);
    m_gen2SettingsDialog->setWindowModality(Qt::WindowModal);
       
	/* Create the view manager */
	m_tagViewManager = new CTagViewManager(ui.readerTreeWidget, &m_dataHandler, m_readRateCalc, this);
	m_tagViewManager->setUp(	m_settingsDialog->showAlias(),
								m_settingsDialog->useTtl(),
								m_settingsDialog->msecsToShowInactive(),
								m_settingsDialog->msecsToShowOutOfRange(),
								m_settingsDialog->msecsToDelete());
	QObject::connect(m_tagViewManager, SIGNAL(requestTagSettings(QString)), this, SLOT(requestTagSettingsDialog(QString)));
	QObject::connect(m_tagViewManager, SIGNAL(requestTagAdvancedSettings(QString, Gen2Tag)), this, SLOT(requestTagAdvancedSettingsDialog(QString, Gen2Tag)));
	QObject::connect(m_tagViewManager, SIGNAL(requestReaderAdvancedSettings(QString)), this, SLOT(requestReaderAdvancedSettingsDialog(QString)));
	QObject::connect(m_tagViewManager, SIGNAL(requestReaderRegisterMap(QString)), this, SLOT(requestReaderRegisterMap(QString)));
	QObject::connect(m_tagViewManager, SIGNAL(newTagCount(int)), ui.tagCountNumber, SLOT(display(int)));
	QObject::connect(m_tagViewManager, SIGNAL(newDifferentTagCount(int)), ui.differentTagCountNumber, SLOT(display(int)));
	QObject::connect(m_tagViewManager, SIGNAL(newOverallDifferentTagCount(int)), ui.overallDifferentTagCountNumber, SLOT(display(int)));
	QObject::connect(m_tagViewManager, SIGNAL(oldTagEntryRemoved(QString,Gen2Tag)), m_tagManager, SLOT(oldTagEntryRemoved(QString,Gen2Tag)));
	QObject::connect(m_tagViewManager, SIGNAL(currentReaderChanged(QString)), this, SLOT(currentReaderChanged(QString)));
    QObject::connect(m_tagViewManager, SIGNAL(requestSL900A(QString, QString)), this, SLOT(requestSL900ADialog(QString, QString)));
	splash->showMessage(tr("Connect..."), alignment, Qt::white);

	/* Connect the signals of the gui to the right slots */
	QObject::connect(QrfeTrace::getInstance(), SIGNAL(traceSignal(QString)), 	ui.traceBrowser, SLOT(append(QString)));
    QObject::connect(firmwareUpgradeAct, SIGNAL(triggered (bool)), 		        this, SLOT(upgradeFirmware()));
	QObject::connect(ui.actionShow_TagList, SIGNAL(triggered (bool)), 			m_tagListDialog, SLOT(exec()));
    QObject::connect(ui.actionGlobal_Actions, SIGNAL(triggered(bool)),          this, SLOT(showGlobalActions()));

	QObject::connect(ui.readerTabWidget, SIGNAL(currentChanged(int)), 			this, SLOT(selectReader(int)));
	QObject::connect(ui.startScanButton, SIGNAL(toggled (bool)), 				this, SLOT(startScan(bool)));
    QObject::connect(ui.scanEndTypeBox, SIGNAL(currentIndexChanged(int)), 		this, SLOT(scanModeChanged(int)));
	QObject::connect(ui.handleActionPushButton, SIGNAL(toggled(bool)), 			this, SLOT(handleActionsToggled(bool)));
	QObject::connect(ui.actionAdd_Serial_Reader, SIGNAL(triggered(bool)), 		this, SLOT(addSerialReader()));
	QObject::connect(ui.actionHandle_Actions, SIGNAL(triggered(bool)), 			this, SLOT(handleActionsToggled(bool)));
	QObject::connect(ui.actionShow_Alias_Names, SIGNAL(triggered (bool)), 		this, SLOT(showAliasNames(bool)));
    QObject::connect(ui.actionShow_Rssi, SIGNAL(triggered (bool)),              this, SLOT(showRssi(bool)));
	QObject::connect(ui.actionUse_Time_To_Live, SIGNAL(triggered ( bool)), 		this, SLOT(useTimeToLive(bool)));
	QObject::connect(ui.actionPreferences, SIGNAL(triggered ( bool)), 			this, SLOT(showSettings()));
	QObject::connect(ui.actionOpen_Register_Map, SIGNAL(triggered ( bool)), 	this, SLOT(showRegisterMap()));
	QObject::connect(ui.clearButton, SIGNAL(clicked()), 						m_readRateCalc, SLOT(clearResults()));
	QObject::connect(ui.actionClear_Tags, SIGNAL(triggered (bool)), 			m_readRateCalc, SLOT(clearResults()));
	QObject::connect(ui.clearButton, SIGNAL(clicked()), 						m_tagViewManager, SLOT(clearTags()));
	QObject::connect(ui.actionClear_Tags, SIGNAL(triggered (bool)), 			m_tagViewManager, SLOT(clearTags()));
	QObject::connect(ui.clearOfflineReaderButton, SIGNAL(clicked()), 			m_tagViewManager, SLOT(clearOfflineReader()));
	QObject::connect(ui.clearOfflineReaderButton, SIGNAL(clicked()), 			this, SLOT(clearOfflineReader()));
	QObject::connect(ui.actionClear_Offline_Reader, SIGNAL(triggered(bool)), 	m_tagViewManager, SLOT(clearOfflineReader()));

	QObject::connect(m_gen2SettingsDialog,    SIGNAL(easterKeyUnlocked()),      this, SLOT(easterKeyUnlocked()));

	/* Create the scan timer to get the end of the scan */
	m_scanTimer = new QTimer(this);
	m_scanTimer->setSingleShot(true);
	QObject::connect(m_scanTimer, SIGNAL(timeout()), this, SLOT(stopScan()));

	/* Create timer for the scan progress bar */
	m_scanProgressTimer = new QTimer(this);
	m_scanProgressTimer->setSingleShot(false);
	m_scanProgressTimer->setInterval(1000);

	m_regMapWindow = NULL;

	/* Connect to the Reader Manager */
	QObject::connect(this, SIGNAL(currentReaderChanged(ReaderInterface*)),   m_amsComWrapper, SLOT(gotReader(ReaderInterface*)));
	QObject::connect(&m_readerManager, SIGNAL(lostReader(ReaderInterface*)), m_amsComWrapper, SLOT(lostReader(ReaderInterface*)));
	QObject::connect(&m_readerManager, SIGNAL(gotReader(ReaderInterface*)),  this, SLOT(gotReader(ReaderInterface*)));
    QObject::connect(&m_readerManager, SIGNAL(lostReader(ReaderInterface*)), this, SLOT(lostReader(ReaderInterface*)));
    QObject::connect(&m_readerManager, SIGNAL(foundStreamV1Reader()),        this, SLOT(handleStreamV1Reader()));

	/* Create the timer for the multiplexer control */
	m_multiplexTimer = new QTimer(this);
	m_multiplexTimer->setSingleShot(true);
	m_multiplexTimer->setInterval(m_settingsDialog->multiplexTime());
	QObject::connect(m_multiplexTimer, SIGNAL(timeout()), this, SLOT(multiplexISR()));

	/* Finally set up the gui */
	ui.traceDockWidget->setVisible(false);
	ui.informationBox->setVisible(false);
	ui.actionShow_Alias_Names->setChecked(m_settingsDialog->showAlias());
    ui.actionShow_Rssi->setChecked(true);
	ui.actionUse_Time_To_Live->setChecked(m_settingsDialog->useTtl());

	closeSplashScreen();
	ActivateSettings();
    if(itsInternalRelease)
    {
        qApp->setStyleSheet(qApp->styleSheet() + ".QTreeWidget {background-image:url(:/internal_bg.png); background-position: center center; background-repeat: none;}");
    }
    readSettings();
}

/*!
 * @brief 	Destructor
 * Destroys the object and frees all used memory.
 */
CReaderTool::~CReaderTool()
{
	/* Stop action handler */
	m_actionHandler->stopActionHandling();

	/* Stop and delete timer */
	m_scanTimer->stop();
	m_scanTimer->deleteLater();
	m_scanProgressTimer->stop();
	m_scanProgressTimer->deleteLater();
	m_multiplexTimer->stop();
	m_multiplexTimer->deleteLater();

	/* Delete view manager */
	m_tagViewManager->deleteLater();

	/* Delete dialogs */
	m_settingsDialog->deleteLater();
	m_tagSettingsDialog->deleteLater();
}

void CReaderTool::closeEvent(QCloseEvent *event)
{
	if(m_regMapWindow != NULL)	
		m_regMapWindow->close();
    if(m_scanActive)
        ui.startScanButton->toggle();

    AMSMainWindow::closeEvent(event);
	event->accept();
}
void CReaderTool::addSerialReader()
{
	int i;
	bool ok;
	QList<UINT> ports;
	QStringList sFriendlyNames;
	QStringList sl;
	QString s;
	if (CEnumerateSerial::UsingSetupAPI1(ports, sFriendlyNames))
	{
		for (i=0; i<ports.size(); i++)
		{
			sl.append(QString("COM%1 (%2)").arg(ports[i]).arg(sFriendlyNames[i]));
		}
	}

    s = QInputDialog::getItem(this, "Choose COM port", "Port", sl, 0, false, &ok);

    if ( ok )
	{
		QString msg;
        int index;
        unsigned int port;
        index = sl.indexOf(s);
        if (index < 0 || index >= ports.size() )
        {
            QMessageBox::critical(this, "Serial reader failed", "Could not get COM port from list");
            return;
        }
        port = ports[index];
		m_readerManager.serialReaderAttached(port, msg);
		if (msg.size() != 0) 
            QMessageBox::critical(this,"Serial reader failed", msg);
	}

}
/*!
 * @brief	Slot that is connected to the Reader Manager
 * This slot is connected to the Reader Manager. It is called when the reader manager detects a new reader.
 * @param	reader 		Pointer to the new reader implementation
 */
void CReaderTool::gotReader(ReaderInterface* reader)
{
	if (m_reader.size() == 0)
		emit currentReaderChanged(reader);

	/* Get the hardware and software revision of the reader */
	QString hardware, software;
    unsigned int fwVersion;
    QString compatResult;

    /* Save the ID and the pointer to the protocol handler in a map */
    m_reader.insert(reader->readerId(), reader);

    /* Check for compatibility */
    if(	reader->getHardwareDescription(hardware) != ReaderInterface::OK ||
        reader->getSoftwareDescription(software) != ReaderInterface::OK ||
        reader->getSoftwareVersion(fwVersion) != ReaderInterface::OK){
            hardware = "Unknown";
            software = "Unknown";
            fwVersion = 0;
    }
    m_tagViewManager->changeHardFirm(reader->readerId(), hardware, software);

    QString requiredFWVersion = AMSVersionInfo::Instance().GetProductVersionFWString();
    if(!AmsFirmwareCheck::isCompatible(requiredFWVersion, fwVersion, compatResult))
    {
        QMessageBox::critical(this, tr("AS3993 Reader Suite Critical"), compatResult);
    }

	/* Add the tab of the reader to the tab view */
	ui.readerTabWidget->addTab(new QWidget(ui.readerTabWidget), reader->readerId());

    ui.readerTabWidget->setTabIcon(ui.readerTabWidget->count() -1, QIcon(":/tree widget icons/passiveReader"));

	/* Reselect the current reader */
	selectReader(ui.readerTabWidget->currentIndex());

	/* Update the count of reader */
	ui.readerCountNumber->display(m_reader.size());

    /* set rssi to currently configured mode */
    reader->setRssiEnabled(ui.actionShow_Rssi->isChecked());

    /* Add and connect the reader to the tag view */
    m_tagViewManager->clearOfflineReader();
    m_tagViewManager->addReader(reader->readerId(), hardware, software, reader->currentActionString(), true, reader->getRssiChildren().size(), reader->getRssiChildren(), false);
    m_tagViewManager->readerChangedState(reader->readerId(), reader->currentState(), reader->currentStateString(), reader->currentStateDescription());
    m_tagManager->addReader(reader->readerId());

    QObject::connect(reader, SIGNAL(inventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)), m_tagManager,       SLOT(cyclicInventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)));
    QObject::connect(reader, SIGNAL(inventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)), m_readRateCalc,     SLOT(cyclicInventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)));
    QObject::connect(reader, SIGNAL(inventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)), m_tagViewManager,   SLOT(cyclicInventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)));
    QObject::connect(reader, SIGNAL(inventoryRoundDone(QString)),                       this,          	    SLOT(handleSingleInventoryRound(QString)));
    QObject::connect(reader, SIGNAL(changedRssiEnable(QString,bool,uchar,QStringList)), m_tagViewManager,   SLOT(readerSetRSSI(QString,bool,uchar,QStringList)));
    QObject::connect(reader, SIGNAL(changedTIDEnable(QString,bool)), m_tagViewManager,   SLOT(readerSetTagID(QString,bool)));
    /* Connect the reader to this object */
    QObject::connect(reader, SIGNAL(changedState(ReaderInterface::HandlerState, QString, QString)), this, SLOT(readerChangedState(ReaderInterface::HandlerState, QString, QString)));
    QObject::connect(reader, SIGNAL(changedAction(QString)), this, SLOT(readerChangedAction(QString)));

	/* If the scan is active in the moment, start the reader to scan */
	if(m_scanActive){
		if(m_settingsDialog->useMultiplex())
			m_multiplexTimer->start(m_settingsDialog->multiplexTime());
		else
			reader->startCyclicInventory();
	}

    if (reader->getHardwareId() == ReaderInterface::Newton)
    {
        //Set Frequencies to USA // Filter on Newton
        if (! m_readerConfigDialogs.contains(reader))
        {
            m_readerConfigDialogs.insert(reader, new CReaderConfigDialog(this));
        }
        m_readerConfigDialogs.value(reader)->changeProfile(reader,"USA");
    }


}

/*!
 * @brief 	Slot that is connected to the Reader Manager
 * The slot is called if a reader was either plugged off or is no more responding.
 * @param	reader	Pointer to the reader object that should be destroyed.
 */
void CReaderTool::lostReader(ReaderInterface* reader)
{
	m_readerConfigDialogs.remove(reader);
	if(!m_reader.contains(reader->readerId()))
		return;

	/* Delete the reader from the map */
	m_reader.remove(reader->readerId());
	m_activeReader.removeAll(reader->readerId());

    m_tagViewManager->clearOfflineReader();

	/* Remove tab from the tab view */
	for(int i = 1; i < ui.readerTabWidget->count(); i++){
		if(ui.readerTabWidget->tabText(i) == reader->readerId())
			ui.readerTabWidget->removeTab(i);
	}

	/* Display current reader count*/
	ui.readerCountNumber->display(m_reader.size());
}

/*!
 * @brief	Slot that is connected to every used reader object
 * Slot that is called, if a reader changed his state.
 * @param	state	String that contains the new state.
 */
void CReaderTool::readerChangedState( ReaderInterface::HandlerState state, QString stateName, QString stateDescription )
{
	/* get the reader */
	ReaderInterface* ph = qobject_cast<ReaderInterface*>(sender());

	if (ph == NULL)
	{
		trc(0x02, "The reader changed state and was null");
		return;
	}

	trc(0x02, "The reader " + ph->readerId() + " changed state to " + ph->currentStateString());
	/* notify the view manager to change the viewn state */
	m_tagViewManager->readerChangedState(ph->readerId(), state, stateName, stateDescription);
}

/*!
 * @brief	Slot that is connected to every used reader object
 * Slot that is called, if a reader changed his action.
 * @param	action	String that contains the new action.
 */
void CReaderTool::readerChangedAction(QString action)
{
	/* get the reader */
	ReaderInterface* ph = qobject_cast<ReaderInterface*>(sender());

	if (ph == NULL)
	{
		trc(2, "The reader changed action and was null");
		return;
	}

	trc(0x02, "The reader " + ph->readerId() + " changed action to " + ph->currentActionString());
	/* notify the view manager to change the viewn action */
	m_tagViewManager->readerChangedAction(ph->readerId(), action);
}


/*!
 * @brief	Slot that is connected to the tab bar
 * This slot is called if a tab of the tab bar is activated.
 * @param 	readerIndex		Index of the activated tab
 */
void CReaderTool::selectReader(int readerIndex)
{
	/* If tab is "All Reader" */
	if(readerIndex == 0){
		/* Reset read rate calculation */
		m_readRateCalc->reset();

		/* If scan active and multiplex is not activated, start every reader with scan */
		if(m_scanActive && !m_settingsDialog->useMultiplex()){
			foreach(QString readerId, m_reader.keys()){
				m_readRateCalc->readerStartedInventory(readerId);
				m_reader.value(readerId)->startCyclicInventory();
				m_tagViewManager->setActive(readerId);
			}
		}

		/* Show all reader in the view */
		m_tagViewManager->showAllReader();
		/* Now all reader are active, but only add those without init error. */
        m_activeReader.clear();
        ReaderInterface::InitError state;
        bool wrongChip;
        foreach(QString r, m_reader.keys())
        {
            m_reader.value(r)->getReaderInitStatus(wrongChip, state);
            if(!wrongChip && state == ReaderInterface::NoError)
                m_activeReader.append(r);
        }
	}
	else{
		/* Get reader ID */
		QString readerId = ui.readerTabWidget->tabText(readerIndex);

		/* Reset read rate calculation */
		m_readRateCalc->reset();

		/* If scan active and multiplex is not activated, stop every reader from scan */
		if(m_scanActive && !m_settingsDialog->useMultiplex()){
			foreach(QString rID, m_activeReader){
				m_reader.value(rID)->stopCyclicInventory();
				m_tagViewManager->setUnactive(rID);
			}
		}

		/* Only show the selected reader in the view */
		m_tagViewManager->selectSingleReader(readerId);
		/* The only active reader is the selected one */
		m_activeReader = QStringList() << readerId;

		/* If scan is running, and not multiplexed, start the scan */
		if(m_scanActive && !m_settingsDialog->useMultiplex()){
			m_readRateCalc->readerStartedInventory(readerId);
			m_reader.value(readerId)->startCyclicInventory();
			m_tagViewManager->setActive(readerId);
		}
	}
}


/*!
 * @brief	Slot that is connected to the Reader-Tag-View
 * It is called if the user requests the tag settings.
 * @param 	tagId		ID of the tag
 */
void CReaderTool::requestTagSettingsDialog(QString tagId)
{
	/* open the tag settings dialog */
	m_tagSettingsDialog->exec(tagId);
}

/*!
 * @brief	Slot that is connected to the Reader-Tag-View
 * It is called if the user requests the advanced tag settings.
 * @param 	readerId	ID of the reader
 * @param 	tagId		ID of the tag
 */
void CReaderTool::requestTagAdvancedSettingsDialog(QString readerId, Gen2Tag tagId)
{
	/* If scan is running it is not possible */
	if(m_scanActive)
	{
		QMessageBox::critical(this, "Advanced Tag Settings", "You must stop the scan before you can change the settings of a tag.");
		return;
	}

	/* Get the reader object */
	ReaderInterface* reader = m_reader.value(readerId);
	if(reader == 0)
		return;

	if(reader->tagType() == ReaderInterface::TAG_GEN2 || reader->tagType() == ReaderInterface::TAG_GEN2_FAST || reader->tagType() == ReaderInterface::TAG_GEN2_TID)
	{
		m_gen2SettingsDialog->exec(reader, tagId);
		return;
	}
	else if(reader->tagType() == ReaderInterface::TAG_ISO6B)
	{
		QMessageBox::information(this, "Advanced Tag Settings", "The advanced tag settings for ISO6B tags are not available at present.");
		return;
	}

	QMessageBox::information(this, "Advanced Tag Settings", "The advanced tag settings are not available at present.");

}

/*!
 * @brief	Slot that is connected to the Reader-Tag-View
 * It is called if the user requests the advanced tag settings.
 * @param 	readerId	ID of the reader
 * @param 	tagId		ID of the tag
 */
void CReaderTool::requestSL900ADialog(QString readerId, QString tagId)
{
	/* If scan is running it is not possible */
	if(m_scanActive)
	{
		QMessageBox::critical(this, "SL900A", "You must stop the scan before you can change the settings of a tag.");
		return;
	}

	/* Get the reader object */
	ReaderInterface* reader = m_reader.value(readerId);
	if(reader == 0)
		return;

	if(reader->tagType() == ReaderInterface::TAG_GEN2 || reader->tagType() == ReaderInterface::TAG_GEN2_FAST || reader->tagType() == ReaderInterface::TAG_GEN2_TID)
	{
		// To Implement if desired xxxDialog->exec(reader, tagId);
		return;
	}
	else if(reader->tagType() == ReaderInterface::TAG_ISO6B)
	{
		QMessageBox::information(this, "Advanced Tag Settings", "The advanced tag settings for ISO6B tags are not available at present.");
		return;
	}

	QMessageBox::information(this, "Advanced Tag Settings", "The advanced tag settings are not available at present.");

}

/*!
 * @brief	Slot that is connected to the Reader-Tag-View
 * It is called if the user requests the advanced reader settings.
 * @param 	readerId	ID of the reader
 */
void CReaderTool::requestReaderRegisterMap(QString readerId)
{
	/* If scan is running it is not possible */
	if(m_scanActive)
	{
		QMessageBox::critical(this, "Register Map", "You must stop the scan before you open the register map for a reader.");
		return;
	}

	/* Get the reader object */
	ReaderInterface* reader = m_reader.value(readerId);
	if(reader == 0)
		return;

	if (m_regMapWindow!=NULL)
    {
        disconnect(m_regMapWindow, SIGNAL(regMapStoreToFile(const QString)), this, SLOT(regMapStoreToFile(const QString)));
        disconnect(m_regMapWindow, SIGNAL(regMapLoadFromFile(const QString)), this, SLOT(regMapLoadFromFile(const QString)));
        delete m_regMapWindow;
    }
	m_regMapWindow = NULL;
	try
	{
        m_regFileAvailable = true;
        if (reader->getChipId() == ReaderInterface::AS3993
            || reader->getChipId() == ReaderInterface::AS3980)
		{
			m_regMapWindow = new RegisterMap(NULL,m_amsComWrapper,"register_map_AS3993.xml");
            m_regMapWindow->resize(730, 800);            
            // do write register values to file or load them from file
            connect(m_regMapWindow, SIGNAL(regMapStoreToFile(const QString)), this, SLOT(regMapStoreToFile(const QString)));
            connect(m_regMapWindow, SIGNAL(regMapLoadFromFile(const QString)), this, SLOT(regMapLoadFromFile(const QString)));
            // add 2 more actions to the file menu
            m_regMapWindow->createLoadStoreActions();
		}
		else
		{
			QMessageBox::warning(this, tr("Register Map"), tr("Unknown Chip ID. Can not load corresponding register map."));
            return;
		}
		m_regMapWindow->setWindowTitle("Register Map "+readerId);
		showRegisterMap();

		m_regMapWindow->readOnce();
		return;
	}
	catch(QString msg)
	{
		if (m_regMapWindow!=NULL) delete m_regMapWindow;
		m_regMapWindow = NULL;
        m_regFileAvailable = false;
		//handle here the exception in case no xml file available 
		QMessageBox::warning(this, tr("Register Map"), msg);
		return;
	}
}

/*!
 * @brief	Slot that is connected to the Reader-Tag-View
 * It is called if the user requests the advanced reader settings.
 * @param 	readerId	ID of the reader
 */
void CReaderTool::requestReaderAdvancedSettingsDialog(QString readerId)
{
	/* If scan is running it is not possible */
	if(m_scanActive)
	{
		QMessageBox::critical(this, "Advanced Reader Settings", "You must stop the scan before you can change the settings of a reader.");
		return;
	}

	/* Get the reader object */
	ReaderInterface* reader = m_reader.value(readerId);
	if(reader == 0)
		return;

	if (! m_readerConfigDialogs.contains(reader))
	{
		m_readerConfigDialogs.insert(reader,new CReaderConfigDialog(this));
	}

	m_readerConfigDialogs.value(reader)->setWindowTitle("Advanced Reader Settings "+readerId);
    m_readerConfigDialogs.value(reader)->setWindowModality(Qt::WindowModal);
	m_readerConfigDialogs.value(reader)->exec(reader);
}
/*!
 * @brief 	Slot that is connected to an action
 * This slot is called by an action. It changes the display of the tags to the stored alias names.
 * @param 	show	Says if to use alias names or not
 */
void CReaderTool::showAliasNames(bool show)
{
	/* Save the setting */
	m_settingsDialog->setShowAlias(show);
	/* Notify the view manager to change the settings */
	m_tagViewManager->setUp(	m_settingsDialog->showAlias(),
								m_settingsDialog->useTtl(),
								m_settingsDialog->msecsToShowInactive(),
								m_settingsDialog->msecsToShowOutOfRange(),
								m_settingsDialog->msecsToDelete());
}

/*!
 * @brief 	Slot that is connected to an action
 * This slot is called by an action. It changes if rssi information is shown in tag list.
 * @param 	show	true: show rssi; false: do not show rssi
 */
void CReaderTool::showRssi( bool show )
{
    foreach(QString readerId, m_reader.keys()){
        ReaderInterface* reader = m_reader.value(readerId);
        reader->setRssiEnabled(show);
    }
}

/*!
 * @brief 	Slot that is connected to an action
 * This slot is called by an action. It changes the behavior of the display of the tags.
 * @param 	use		Says if to use time to live or not
 */
void CReaderTool::useTimeToLive(bool use)
{
	/* Save the setting */
	m_settingsDialog->setUseTtl(use);
	/* Notify the view manager to change the settings */
	m_tagViewManager->setUp(	m_settingsDialog->showAlias(),
								m_settingsDialog->useTtl(),
								m_settingsDialog->msecsToShowInactive(),
								m_settingsDialog->msecsToShowOutOfRange(),
								m_settingsDialog->msecsToDelete());
}

/*!
 * @brief 	Slot that is connected to an action
 * This slot is called by an action. It activates the handling of the configured actions (Show picture/Start app).
 * @param 	checked		Says if to handle actions or not
 */
void CReaderTool::handleActionsToggled(bool checked)
{
	/* Set the buttons state */
	ui.actionHandle_Actions->setChecked(checked);
	ui.handleActionPushButton->setChecked(checked);

	if(checked){
		/* Connect every reader to the action handler */
		foreach(QString readerId, m_reader.keys()){
			ReaderInterface* reader = m_reader.value(readerId);
			QObject::connect(reader, SIGNAL(inventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)), m_actionHandler, SLOT(cyclicInventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)));
		}
	}
	else{
		/* Disconnect every reader to the action handler */
		foreach(QString readerId, m_reader.keys()){
			ReaderInterface* reader = m_reader.value(readerId);
			QObject::disconnect(reader, SIGNAL(inventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)), m_actionHandler, SLOT(cyclicInventoryResult(QString, QList<InventoryTagInfo>, quint32, quint8)));
		}
	}


	/* If a scan is already active, start the action handler immediately */
	if(m_scanActive){
		if(checked)
			m_actionHandler->startActionHandling();
		if(!checked)
			m_actionHandler->stopActionHandling();
	}
}

/*!
 * @brief	Slot is connected to the gui
 * This slot is called if a scan should be started OR stopped.
 * @param	start		Varaible that identifies whether to start or stop scanning
 */
void CReaderTool::startScan(bool start)
{
	/* Start the scan */
	if(start)
	{
		/* Set the variable that scan is running */
		m_scanActive = true;

		/* Change button to stop */
		ui.startScanButton->setText("Stop Scan");
		ui.actionStart_Scan->setText("Stop Scan");

		/* Clear trace browser, because of lack of memory */
		ui.traceBrowser->clear();

		/* disable the time out box */
		ui.scanTimeoutBox->setDisabled(true);
        ui.scanEndTypeBox->setDisabled(true);

        if(m_scanEndMode == Seconds)  /* we stop scan after some time out */
        {
		    /* Calculate the scan time out */
		    m_scanTimeout = ui.scanTimeoutBox->value() * 1000;
		    /* Save the start time of the scan */
		    m_scanStart = QDateTime::currentDateTime();

		    /* If a scan timeout is set*/
		    if(m_scanTimeout > 0)
		    {
			    /* Prepare progress bar */
			    ui.scanProgressBar->setValue(0);
			    ui.scanProgressBar->setMaximum(m_scanTimeout);
			    ui.scanProgressLabel->setText(QString::number(0) + " s");

			    /* Start the timer for the given time out */
			    m_scanTimer->start(m_scanTimeout);

			    /* Start the helper timer, to increment the progress bar */
			    QObject::connect(m_scanProgressTimer, SIGNAL(timeout()), this, SLOT(incrementScanProgress()));
			    m_scanProgressTimer->start();

		    }
		    /* If time out is set to infinite */
		    else
		    {
			    /* Prepare progress bar */
			    ui.scanProgressBar->setDisabled(true);
			    ui.scanProgressLabel->setText("Infinite");

			    /* Start the helper timer, to only show the time the scan is running */
			    QObject::connect(m_scanProgressTimer, SIGNAL(timeout()), this, SLOT(showScanTime()));
			    m_scanProgressTimer->start();
		    }
        }
        else    /* we stop scan after a number of rounds */
        {
            m_maxInventory = ui.scanTimeoutBox->value();
            if(m_maxInventory == 0) //skip if number of seconds/rounds is zero
            {
                ui.startScanButton->toggle();
                return;
            }
            /* Prepare progress bar */
            ui.scanProgressBar->setValue(0);
            ui.scanProgressBar->setMaximum(m_maxInventory);
            ui.scanProgressLabel->setText(QString::number(0) + " rounds");
            /* Start the helper timer, to increment the progress bar */
            QObject::connect(m_scanProgressTimer, SIGNAL(timeout()), this, SLOT(incrementScanProgress()));
            m_scanProgressTimer->start();
        }

		this->repaint();

		/* Reset read rate calculation */
		m_readRateCalc->reset();
		/* Notify tag manager that a scan is started */
		m_tagManager->startScan();
		/* Clear all reader and tag infos in the view */
		m_tagViewManager->clearAllReaderTagInfos();

		/* If it is configured to use multiplex */
		if(m_settingsDialog->useMultiplex())
		{
			m_multiplexCurrentReader = 0;
			m_multiplexReaderOn = false;

			/* Start the first reader of the active reader list */
			for(m_multiplexCurrentReader = 0; m_multiplexCurrentReader < m_activeReader.size(); m_multiplexCurrentReader++)
			{
				if(m_reader.contains(m_activeReader.at(m_multiplexCurrentReader))){
					if(m_reader.value(m_activeReader.at(m_multiplexCurrentReader))->startCyclicInventory() == ReaderInterface::OK){
						/* Notify the read rate calculator that a new round started */
						m_readRateCalc->readerStartedInventory(m_activeReader.at(m_multiplexCurrentReader));
						/* Set the reader active in the view */
						m_tagViewManager->setActive(m_activeReader.at(m_multiplexCurrentReader));
						/* Store that there is a reader running */
						m_multiplexReaderOn = true;
						break;
					}
				}
			}

			/* Start the multiplex timer */
			m_multiplexTimer->start(m_settingsDialog->multiplexTime());
		}
		else
		{
			/* Start every reader */
			foreach(QString id, m_activeReader)
			{
				if(m_reader.contains(id)){
					m_readRateCalc->readerStartedInventory(id);
                    if(m_scanEndMode == Seconds)  /* we stop scan after some time out */
					    m_reader.value(id)->startCyclicInventory();
                    else                                        /* we stop after 1 inventory round, the next one will be started in handleSingleInventoryRound() */
                    {
                        m_singleInventoryCounter.insert(id, 0);
                        m_reader.value(id)->startSingleInventory();
				}
			}
			}

			/* Set all reader active in the view */
			m_tagViewManager->setActive();
		}


		m_tagViewManager->startGuiUpdate();

		/* start the action handler, if needed */
		if(ui.handleActionPushButton->isChecked())
			m_actionHandler->startActionHandling();

	}

	/* Stop the scan */
	else
	{
		/* Change the button back to start */
		ui.startScanButton->setText("Start Scan");
		ui.actionStart_Scan->setText("Start Scan");

		/* Re-enable the time out box */
		ui.scanTimeoutBox->setEnabled(true);
        ui.scanEndTypeBox->setEnabled(true);

        if(m_scanEndMode == Seconds)  /* we stop scan after some time out */
        {
		/* If a scan timeout was configured */
		if(m_scanTimeout > 0)
		{
			/* Stop the scan timer */
			m_scanTimer->stop();

			/* Stop the helper timer */
			QObject::disconnect(m_scanProgressTimer, SIGNAL(timeout()), this, SLOT(incrementScanProgress()));
			m_scanProgressTimer->stop();

			/* Set progress bar to maximum */
			ui.scanProgressBar->setValue(m_scanTimeout);
			ui.scanProgressLabel->setText(QString::number(m_scanTimeout/1000) + " s");

			trc(0x01, "Scan stopped after " + getScanTime());
		}
		/* If the scan was infinite */
		else
		{
			/* Stop the scan timer */
			m_scanTimer->stop();

			/* Stop the helper timer */
			QObject::disconnect(m_scanProgressTimer, SIGNAL(timeout()), this, SLOT(showScanTime()));
			m_scanProgressTimer->stop();

			/* Prepare progress bar */
			ui.scanProgressBar->setEnabled(true);

			trc(0x01, "Scan stopped after " + getScanTime());
		}
        }
        else    /* we stop after a number of inventory rounds */
        {
            /* Stop the helper timer */
            QObject::disconnect(m_scanProgressTimer, SIGNAL(timeout()), this, SLOT(incrementScanProgress()));
            m_scanProgressTimer->stop();
            m_singleInventoryCounter.clear();

            /* Set progress bar to maximum */
            ui.scanProgressBar->setValue(m_maxInventory);
            ui.scanProgressLabel->setText(QString::number(m_maxInventory) + " rounds");

            trc(0x01, "Scan stopped after " + QString::number(m_maxInventory) + " rounds");
        }

		this->repaint();

		/* Stop each reader */
		foreach(QString id, m_activeReader)
		{
			if(m_reader.contains(id)){
				m_reader.value(id)->stopCyclicInventory();
				m_readRateCalc->readerStoppedInventory(id);
			}
		}

		/* Stop the view from updating */
		m_tagViewManager->stopGuiUpdate();

		/* Stop handling actions */
		m_actionHandler->stopActionHandling();

		/* Store that scanning is done */
		m_scanActive = false;

		/* Notify the reader manager to now clean up the plugged off reader */
		m_readerManager.cleanUp();
	}
}

/*!
 * @brief 	Slot that is connected to the scan timer.
 * If the scan timer runs into a timeout this slot is called. It sets the scan button to unchecked and thus calls indirectly the startScan
 * method with param false.
 */
void CReaderTool::stopScan()
{
	ui.startScanButton->setChecked(false);
}


/*!
 * @brief Slot that is called from the progressTimer to increment the progress bar.
 */
void CReaderTool::incrementScanProgress()
{
    if(m_scanEndMode == Seconds)  /* we stop scan after some time out */
    {
	ui.scanProgressBar->setValue(ui.scanProgressBar->value() + 1000);
	ui.scanProgressLabel->setText(QString::number(ui.scanProgressBar->value()/1000) + " s");
    }
    else
    {
        int count = 0;
        foreach(QString id, m_singleInventoryCounter.keys())
        {
            count += m_singleInventoryCounter.value(id);
        }

        if (m_singleInventoryCounter.size() > 0)
        {
            count = (int) count / m_singleInventoryCounter.size();
        }
        else
            count = 0;

        ui.scanProgressBar->setValue(count);
        ui.scanProgressLabel->setText(QString::number(count) + " rounds");
    }
}


/*!
 * @brief Counts the number of inventory rounds performed.
 * Counts the number of inventory rounds performed, if it is bigger than the desired
 * inventory rounds, the scanning will be stopped
 */
void CReaderTool::handleSingleInventoryRound( QString reader )
{
    quint32 counter, finishedReaderCount = 0;

    if(m_scanEndMode != Rounds)  /* we do not count the number of rounds */
        return;
    if(!m_singleInventoryCounter.contains(reader))
        return;

    counter = m_singleInventoryCounter.value(reader) + 1;
    m_singleInventoryCounter.insert(reader, counter);
    if(counter < m_maxInventory && m_reader.contains(reader))   /* if we haven't performed enough rounds, continue */
        m_reader.value(reader)->startSingleInventory();


    foreach(QString id, m_singleInventoryCounter.keys())
    {
        if(m_singleInventoryCounter.value(id) >= m_maxInventory && m_reader.contains(id)){
            m_reader.value(id)->stopCyclicInventory();
            m_readRateCalc->readerStoppedInventory(id);
            finishedReaderCount++;
        }
    }

    if (finishedReaderCount == m_singleInventoryCounter.size())
        stopScan();
}

/*!
 * @brief Slot which is called when the scan mode combobox is changed
 */
void CReaderTool::scanModeChanged( int newIndex )
{
    m_scanEndMode = (ScanEnd) newIndex;
}

/*!
 * @brief Slot that prints out the current scan time.
 */
void CReaderTool::showScanTime()
{
	ui.scanProgressLabel->setText(getScanTime());
}

/*!
 * @brief 	Slot that is called from the multiplex timer.
 * This slot is called from the multiplex timer. It handles the whole multiplex logic.
 */
void CReaderTool::multiplexISR()
{
	/* If reader is scanning, stop it */
	if(m_multiplexReaderOn == true){
		/* Check index */
		if(m_multiplexCurrentReader < m_activeReader.size())
		{
			/* Try to stop the active reader */
			if(m_reader.value(m_activeReader.at(m_multiplexCurrentReader))->stopCyclicInventory() == ReaderInterface::OK)
			{
				/* Notify the read rate calculator that the reader is paused*/
				m_readRateCalc->readerPausedInventory(m_activeReader.at(m_multiplexCurrentReader));
				/* Increment reader index */
				m_multiplexCurrentReader++;
				m_multiplexReaderOn = false;

			}
			/* If it was not successful to stop the reader, try again immediately. */
			else{
				m_multiplexTimer->start(1);
				return;
			}
		}
	}

	/* If no reader is scanning, but scan is still running, start the next */
	if(m_multiplexReaderOn == false && m_scanActive)
	{
		/* Check index */
		if(m_multiplexCurrentReader >= m_activeReader.size())
			m_multiplexCurrentReader = 0;

		/* Check index */
		if(m_multiplexCurrentReader < m_activeReader.size())
		{
			/* Try to start the next reader */
			if(m_reader.value(m_activeReader.at(m_multiplexCurrentReader))->startCyclicInventory() == ReaderInterface::OK)
			{
				m_multiplexReaderOn = true;

				/* Notify the read rate calculator that a new round started */
				m_readRateCalc->readerResumedInventory(m_activeReader.at(m_multiplexCurrentReader));
				/* Set the reader active in the view */
				m_tagViewManager->setActive(m_activeReader.at(m_multiplexCurrentReader));

				/* Restart the multiplex timer */
				m_multiplexTimer->start(m_settingsDialog->multiplexTime());
			}
			/* If it was not successful to start the reader, try again immediately. */
			else
			{
				m_multiplexTimer->start(1);
				return;
			}
		}
	}
}
/*!
 * @brief 	Function to activate the current settings.
 */

void CReaderTool::ActivateSettings()
{

	/* Notify the view manager with the new settings */
	m_tagViewManager->setUp(	m_settingsDialog->showAlias(),
								m_settingsDialog->useTtl(),
								m_settingsDialog->msecsToShowInactive(),
								m_settingsDialog->msecsToShowOutOfRange(),
								m_settingsDialog->msecsToDelete());

	/* Change the ui */
	ui.actionShow_Alias_Names->setChecked(m_settingsDialog->showAlias());
	ui.actionUse_Time_To_Live->setChecked(m_settingsDialog->useTtl());

	/* Activate trace if needed */
	if(m_settingsDialog->useTrace())
		QrfeTrace::setTrcLevel(m_settingsDialog->traceLevel());
	else
		QrfeTrace::setTrcLevel(0);
	/* Set the multiplex time out */
	if(m_settingsDialog->useMultiplex())
		m_multiplexTimer->setInterval(m_settingsDialog->multiplexTime());
	
}
/*!
 * @brief 	Slot that is called from the gui
 * This slot is called if the settings window is requested.
 */
void CReaderTool::showSettings()
{
	/* Show the settings dialog with the current settings */
	if(m_settingsDialog->exec() != QDialog::Accepted)
		return;
	ActivateSettings();
}


/*!
 * @brief 	Slot that is called from the special gui
 * This slot is called if the easter key is unlocked.
 */
void CReaderTool::easterKeyUnlocked()
{
    if(QMessageBox::Yes == QMessageBox::question(this, "The secret key", "The key was detected. \nYou wanna play?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No))
        m_keyDialog->exec();
}


/*!
 * @brief	Member function to get a reader interface
 * This member function returns the reader interface of the given readerId
 * @param 	readerId	String with the reader ID
 * @return	Returns a pointer to the reader interface. If there is no reader interface to the given reader id a zero pointer is returned.
 */
ReaderInterface* CReaderTool::getInterface(QString readerId)
{
	return m_reader.value(readerId);
}

void CReaderTool::clearOfflineReader()
{
	m_readerManager.cleanUp();
}

void CReaderTool::showRegisterMap()
{
	if(m_regMapWindow != NULL && m_regFileAvailable)
	{
		m_regMapWindow->show();
		m_regMapWindow->setWindowTitle("Register Map " + m_tagViewManager->getCurrentReader());
	}
}


void CReaderTool::currentReaderChanged(QString readerID)
{
	bool isVisible = false;
	ReaderInterface* reader = m_reader.value(readerID);
	if(reader == NULL)
		return;
	emit currentReaderChanged(reader);
	if (m_regMapWindow) 
	{
		isVisible = m_regMapWindow->isVisible();
		m_regMapWindow->close();
		delete m_regMapWindow;
	}

	try
    {
        m_regFileAvailable = true;
        if (reader->getChipId() == ReaderInterface::AS3993
            || reader->getChipId() == ReaderInterface::AS3980)
        {
            m_regMapWindow = new RegisterMap(NULL,m_amsComWrapper,"register_map_AS3993.xml");
        }
        else
        {
            QMessageBox::warning(this, tr("Register Map"), tr("Unknown Chip ID. Can not load corresponding register map."));
            return;
        }
    }
    catch(QString msg)
    {
        //handle here the exception in case no xml file available 
        m_regFileAvailable = false;
        QMessageBox::warning(this, tr("Register Map"), msg);
		return;
    }

	m_regMapWindow->setWindowTitle("Register Map "+readerID);
	if(isVisible)
	{
		showRegisterMap();
		m_regMapWindow->readOnce();
	}
}

void CReaderTool::show(void)
{
	QMainWindow::show();
}

void CReaderTool::showGlobalActions()
{
    //when the tag settings dialog is called with a specific tag ID it acts as global action dialog
    requestTagSettingsDialog(CTagSettingsDialog::m_globalActionId);
}

void CReaderTool::upgradeFirmware()
{
    QString reader;
    ReaderInterface *readerIf = 0;
    bool lookForBootloader = false;

    reader = m_tagViewManager->getCurrentReader();
    readerIf = m_reader.value(reader);
    if(reader.isEmpty() || readerIf == 0)
    {
        int ret = QMessageBox::question(this, "Firmware Upgrade", tr("No reader selected. Should I try to look for a device with bootloader enabled?"),
            QMessageBox::No | QMessageBox::Yes);
        if(ret == QMessageBox::No)
            return;
        lookForBootloader = true;
    }
    else
        readerIf = m_reader.value(reader);

    performFirmwareUpgrade(readerIf, lookForBootloader);

    return;
}

void CReaderTool::performFirmwareUpgrade(ReaderInterface *readerIf, bool lookForBootloader)
{
    QString message;
    static QString binFile = QCoreApplication::applicationDirPath ()+"\\firmware\\";
    QString fileName;

    fileName = QFileDialog::getOpenFileName(this, tr("Open Firmware File"), binFile, tr("Firmware Files (*.bin)"));
    if(fileName.isEmpty())
        return;

    binFile = fileName;     //store file location for next update

    /* enter bootloader mode */
    if(!lookForBootloader)          //we try to find a device which is already in bootloader mode.
    {
        if(readerIf->enterBootloader() != ReaderInterface::OK)
        {
            QMessageBox::information(NULL, "Firmware Upgrade", tr("Entering Bootloader failed. No Firmware update was performed!"));
            return;
        }
    }

    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

    /* start AMS Firmware Upgrade */
    int err;
    AMSBootloader *bootloader;
    AMSFirmwareUpgrader *upgrader;

    bootloader = new PicUSBBootloader();
    upgrader = new AMSFirmwareUpgrader(bootloader);

    UpgradeProgressDialog* progress = new UpgradeProgressDialog(upgrader, this);

    upgrader->prepareUpgradeSlot(fileName, 0x1E00);

    QThread *thread = new QThread;
    upgrader->moveToThread(thread);
    thread->start();
    QMetaObject::invokeMethod(upgrader, "upgradeSlot", Qt::QueuedConnection);

    progress->exec();

    err = upgrader->errorCode();
    QApplication::restoreOverrideCursor();
    switch (err)
    { 
    case 0:
        message.append("Successfully downloaded firmware!");
        break;
    case 1:
        message.append("Can't establish connection to bootloader");
        break;
    case 2:
        message.append("Can't erase flash memory");
        break;
    case 3:
        message.append("Can't program flash memory");
        break;
    case 4:
        message.append("Can't start programmed application");
        break;
    case 5:
        message.append("Can't read firmware");
        break;
    case 6:
        message.append("Not enough memory!");
        break;
    default:
        {
            message.append("Failed with error code ");
            message.append(QString::number(err));
        }
    }
    QMessageBox::information(this, "Firmware Upgrade", message);

    delete(upgrader);
    delete(bootloader);
    delete(thread);
    delete(progress);
}

void CReaderTool::setupMenus()
{
    fileMenu->setTitle(tr("&Control"));
    delete resetAct;
    delete readOutRegsAct;
    fileMenu->insertAction(exitAct, ui.actionStart_Scan);
    fileMenu->insertAction(exitAct, ui.actionAdd_Serial_Reader);
    fileMenu->insertAction(exitAct, ui.actionClear_Tags);
    fileMenu->insertAction(exitAct, ui.actionClear_Offline_Reader);
    fileMenu->insertAction(exitAct, ui.actionHandle_Actions);

    delete onOffTraceAct;
    delete autoUpdateAct;
    delete chipDetectionAct;
    delete settingsMenu;

    delete viewRegMapAct;
    delete viewTraceWindowAct;
    viewMenu->addAction(ui.actionAdvanced_Read_Information);
    viewMenu->addAction(ui.actionShow_Alias_Names);
    viewMenu->addAction(ui.actionShow_Rssi);
    viewMenu->addAction(ui.actionUse_Time_To_Live);
    viewMenu->addAction(ui.actionShow_Trace_Browser);
    viewMenu->addAction(ui.actionShow_TagList);
    viewMenu->addAction(ui.actionGlobal_Actions);
    viewMenu->addAction(ui.actionPreferences);
}

void CReaderTool::handleStreamV1Reader()
{
    QMessageBox::warning(this, "Incompatible Reader detected",
        "A reader which needs an Firmware upgrade has been detected.\n"
        "It has been put into bootloader mode and waits for the Firmware upgrade. "
        "Please disconnect other readers and select the correct Firmware image file in the next dialog.");

    performFirmwareUpgrade(0, true);
}

QString CReaderTool::getScanTime()// Because there is now appropriate Qt Function 
{
    int allSeconds = m_scanStart.secsTo(QDateTime::currentDateTime());

    int scDays = allSeconds / ( 60*60*24);
    QTime scTime(0,0,0);
    scTime = QTime(0,0,0).addSecs(allSeconds - scDays*( 60*60*24));

    return QString("%1d  %2").arg(scDays).arg(scTime.toString("hh:mm:ss"));
}

void CReaderTool::regMapStoreToFile ( const QString fileName )
{
    if ( m_regMapWindow )
    { /* signal should anyhow never come if reg-map is 0 */
        storeRegisterMapValues( fileName, *m_regMapWindow, m_amsComWrapper );
    }
}

void CReaderTool::regMapLoadFromFile ( const QString fileName )
{
    if ( m_regMapWindow )
    { /* signal should anyhow never come if reg-map is 0 */
        loadRegisterMapValues( fileName, *m_regMapWindow, m_amsComWrapper );
    }
}
