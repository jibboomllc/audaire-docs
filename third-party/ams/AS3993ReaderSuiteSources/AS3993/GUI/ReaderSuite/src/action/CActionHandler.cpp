/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "CActionHandler.h"
#include "../gui/CTagSettingsDialog.h"

CActionHandler::CActionHandler(CDataHandler* dataHandler, QObject* parent)
	: QObject(parent)
	, QrfeTraceModule("CActionHandler")
{
	m_doActionHandling = false;
    m_doGlobalActions = false;
	m_dataHandler = dataHandler;
}

CActionHandler::~CActionHandler()
{
}

void CActionHandler::startActionHandling()
{
	m_doActionHandling = true;

    TagActionInfo t;
    if(m_dataHandler->getConfig(CTagSettingsDialog::m_globalActionId, t)){
        //get config for global actions
        m_doGlobalActions = t.performAction;
    }
    else
        m_doGlobalActions = false;

}


void CActionHandler::stopActionHandling()
{
	m_doActionHandling = false;
    m_doGlobalActions = false;
	stopAll();
	m_handledTags.clear();
    m_firstReadHandledTags.clear();
}


void CActionHandler::cyclicInventoryResult(QString readerId, QList<InventoryTagInfo> tags, quint32 freq, quint8 status )
{
	if(!m_doActionHandling)
		return;

    foreach(InventoryTagInfo tag, tags)
    {
        if(m_doGlobalActions && !m_firstReadHandledTags.contains(tag.epc))
        {
            m_firstReadHandledTags.append(tag.epc);
            performAction(readerId, CTagSettingsDialog::m_globalActionId);
        }

	    trc(0x08, "-> handleNewTags");
	    // If the tag was already handled, skip
	    if(m_handledTags.contains(tag.epc)){
		    trc(0x08, "Tag already handled...");
		    return;
	    }
	    // Mark tag as handled
	    m_handledTags.append(tag.epc);
        performAction(readerId, tag.epc);
    }
}

void CActionHandler::deleteShownPicture()
{
    TagActionInfo t;
    bool pOnce = false;

	trc(0x01, "-> deleteShownPicture");
	// Get the timer that ran to timeout
	QTimer* timer = (QTimer*) sender();
	timer->stop();

	// Get the widget that was connected with the timer
	CShowPictureWidget* widget = dynamic_cast<CShowPictureWidget*>(m_openPicture.value(timer));
	if(widget == 0)return;

	// Remove picture from the display widget and mark tag as unhandled
	widget->setVisible(false);

	m_openPicture.remove(timer);
    if(m_dataHandler->getConfig(widget->epc(), t))
        pOnce = t.performOnce;
    if(!pOnce)
	    m_handledTags.removeAll(widget->epc());

	// Delete timer and widget
	widget->deleteLater();
	timer->deleteLater();
}

void CActionHandler::stoppApplication()
{
    TagActionInfo t;
    bool pOnce = false;

	trc(0x01, "-> stoppApplication");
	// Get the timer that ran to timeout
	QTimer* timer = (QTimer*) sender();
	timer->stop();

	// Get the app starter that was connected with the timer
	CApplicationStarter* app = m_openApps.value(timer);
	if(app == 0)return;

	// Remove app and mark tag as unhandled
	m_openApps.remove(timer);
    if(m_dataHandler->getConfig(app->epc(), t))
        pOnce = t.performOnce;
    if(!pOnce)
	m_handledTags.removeAll(app->epc());

	// Delete timer and app starter, the app will then be killed
	app->deleteLater();
	timer->deleteLater();
}

void CActionHandler::stopAll()
{
	foreach(QTimer* timer, m_openPicture.keys()){
		timer->stop();

		// Get the widget that was connected with the timer
		CShowPictureWidget* widget = dynamic_cast<CShowPictureWidget*>(m_openPicture.value(timer));
		if(widget == 0)return;

		// Remove picture from the display widget and mark tag as unhandled
		widget->setVisible(false);

		m_openPicture.remove(timer);
		m_handledTags.removeAll(widget->epc());

		// Delete timer and widget
		widget->deleteLater();
		timer->deleteLater();
	}

	foreach(QTimer* timer, m_openApps.keys())
	{
		timer->stop();

		CApplicationStarter* starter = m_openApps.value(timer);

		m_openApps.remove(timer);
		m_handledTags.removeAll(starter->epc());

		starter->deleteLater();
		timer->deleteLater();
	}

}

void CActionHandler::performAction(QString readerId, QString epc)
{
    // If no configuration was saved for the tag, skip
    TagActionInfo t;
    if(!m_dataHandler->getConfig(epc, t)){
        trc(0x08, "No configuration for tag...");
        m_handledTags.removeAll(epc);
        return;
    }

    // If we should not do anything, skip
    if(!t.performAction){
        trc(0x08, "No action set for tag...");
        m_handledTags.removeAll(epc);
        return;
    }

    if(t.type == ShowPicture)
    {
        // Create new timer and connect it
        QTimer* timer = new QTimer();
        timer->setInterval(m_dataHandler->timeValToMsecs(t.time));
        timer->setSingleShot(true);
        QObject::connect(timer, SIGNAL(timeout()), this, SLOT(deleteShownPicture()));

        // Create the widget that is shown in the display tab
        CShowPictureWidget* widget = new CShowPictureWidget((QWidget*)QApplication::desktop(), epc, t.picPath, t.aliasName, readerId);
        widget->show();
        m_openPicture.insert(timer, widget);

        // Start timer
        timer->start();

    }
    else if(t.type == StartApp)
    {
        // Create new timer and connect it
        QTimer* timer = new QTimer();
        timer->setInterval(m_dataHandler->timeValToMsecs(t.time));
        timer->setSingleShot(true);
        QObject::connect(timer, SIGNAL(timeout()), this, SLOT(stoppApplication()));

        trc(0x01, "Starting application " + t.appPath);
        // Create the widget that is shown in the display tab
        CApplicationStarter* app = new CApplicationStarter(epc, t.appPath, t.appParams, timer);
        m_openApps.insert(timer, app);

        // Start timer
        timer->start();
    }
    else{
        m_handledTags.removeAll(epc);
    }

}
