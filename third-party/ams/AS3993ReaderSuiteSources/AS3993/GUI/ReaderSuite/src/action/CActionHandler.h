/*
 * CActionHandler.h
 *
 *  Created on: 02.02.2009
 *      Author: stefan.detter
 */

#ifndef CACTIONHANDLER_H_
#define CACTIONHANDLER_H_

#include <QObject>
#include <QStringList>
#include <QMap>
#include <QTimer>

#ifdef QrfeDATABASEINTERFACE_DEBUG
#include <QrfeTrace.h>
#endif

#include "reader/ReaderInterface.h"
#include "reader/Gen2ReaderInterface.h"
#include "../CDataHandler.h"
#include "CApplicationStarter.h"
#include "CShowPictureWidget.h"

struct InventoryTagInfo;
struct InventoryInfo;

class CActionHandler : public QObject
	, QrfeTraceModule
{
	Q_OBJECT
public:
	CActionHandler(CDataHandler* dataHandler, QObject* parent = 0);
	virtual ~CActionHandler();

public slots:
	void startActionHandling();
	void stopActionHandling();

	void deleteShownPicture();
	void stoppApplication();

	void stopAll();

	void cyclicInventoryResult(QString readerId, QList<InventoryTagInfo> tags, quint32 freq, quint8 status );

private:
    void performAction(QString readerId, QString epc);

	CDataHandler* m_dataHandler;

    QStringList m_handledTags;
    QStringList m_firstReadHandledTags;             //!< contains a list of tags which were read first time and the according global actions was performed
    bool m_doGlobalActions;
    QMap<QTimer*, QWidget*>					m_openPicture;
    QMap<QTimer*, CApplicationStarter*>		m_openApps;

    bool 	m_doActionHandling;

};

#endif /* CACTIONHANDLER_H_ */
