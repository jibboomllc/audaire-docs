/*
 * CTagViewManager.cpp
 *
 *  Created on: 28.01.2009
 *      Author: stefan.detter
 */

#include "CTagViewManager.h"
#include "reader/Gen2ReaderInterface.h"

#include <QLabel>
#include <QProgressBar>
#include <QMessageBox>

const int CTagViewManager::m_ReaderColumn = 0;
const int CTagViewManager::m_HardwareColumn = 1;
const int CTagViewManager::m_SoftwareColumn = 2;
const int CTagViewManager::m_ActionColumn = 3;
const int CTagViewManager::m_StateColumn = 4;

const int CTagViewManager::m_TIDRow        = 0;
const int CTagViewManager::m_ReadCountRow  = 1;
const int CTagViewManager::m_TagInfoRow    = 2;
const int CTagViewManager::m_RSSIRow       = 3;

/*!
 * @brief 	Constructor of the view manager
 * @param	treeWidget		Pointer to tree widget, where the data should be written in
 * @param	dataHandler		Pointer to the data handler that enables access to the database
 * @param	readRateCalc	Pointer to the object that calculates the read rate
 * @param	parent			Pointer top the parent object
 */
CTagViewManager::CTagViewManager(CTreeWidget* treeWidget, CDataHandler* dataHandler, CReadRateCalc* readRateCalc, QObject* parent)
	: QObject(parent)
	, QrfeTraceModule("CTagViewManager")
    , m_reflDiag(NULL)
{
	/* Store data handler */
	m_dataHandler = dataHandler;

	/* Store read rate calc */
	m_readRateCalc = readRateCalc;

	/* Store tree widget and connect to the needed signals */
	m_treeWidget = treeWidget;
	m_treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
	QObject::connect(m_treeWidget, SIGNAL(customContextMenuRequested ( const QPoint& )), this, SLOT(showPopup(const QPoint &)));
	QObject::connect(m_treeWidget, SIGNAL(itemDoubleClicked (QTreeWidgetItem *, int)), this, SLOT(itemDoubleClicked ( QTreeWidgetItem *)));
	QObject::connect(m_treeWidget, SIGNAL(currentItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)), this, SLOT(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)));

	/* Set up tree widget */
	m_treeWidget->setColumnCount(5);
	m_treeWidget->setHeaderLabels(QStringList() << "Reader" << "Hardware" << "Software" << "Action" << "State" );
	m_treeWidget->setColumnPercentWidth(m_ReaderColumn, 45);
	m_treeWidget->setColumnPercentWidth(m_HardwareColumn, 30);
	m_treeWidget->setColumnPercentWidth(m_SoftwareColumn, 30);
	m_treeWidget->setColumnPercentWidth(m_ActionColumn, 8);
	m_treeWidget->setColumnPercentWidth(m_StateColumn, 10);
	m_treeWidget->setIconSize(QSize(30,30));

	/* Set up the used fonts */
	m_readerIdFont.setFamily("Tahoma");
	m_readerIdFont.setPointSize(11);
	m_readerIdFont.setBold(true);

	m_readerVersionFont.setFamily("Tahoma");
	m_readerVersionFont.setBold(true);
	m_readerVersionFont.setPointSize(8);

	m_readerStateFont.setFamily("Tahoma");
	m_readerStateFont.setBold(true);
	m_readerStateFont.setPointSize(10);

	m_readerInfoFont.setFamily("Tahoma");
	m_readerInfoFont.setBold(true);
	m_readerInfoFont.setPointSize(10);

	m_readerInfoDataFont.setFamily("Tahoma");
	m_readerInfoDataFont.setBold(false);
	m_readerInfoDataFont.setPointSize(8);

	m_tagIdFont.setFamily("Tahoma");
	m_tagIdFont.setBold(true);
	m_tagIdFont.setPointSize(9);

	m_tagInfoDataFont.setFamily("Tahoma");
	m_tagInfoDataFont.setBold(false);
	m_tagInfoDataFont.setPointSize(8);



	/* Create context menus and actions */
	m_tagContext = new QMenu("Tag Context Menu", m_treeWidget);
	m_tagContext->addAction(m_tagContext_Settings =	new QAction(QIcon(QString::fromUtf8(":/button icons/settingsIcon")), "Tag Associations", m_tagContext));
	QObject::connect(m_tagContext_Settings, SIGNAL(triggered (bool)), this, SLOT(contextMenuClicked()));
	m_tagContext->addAction(m_tagContext_AdvancedSettings = new QAction(QIcon(QString::fromUtf8(":/button icons/advancedSettingsIcon")), "Advanced Tag Settings", m_tagContext));
	QObject::connect(m_tagContext_AdvancedSettings, SIGNAL(triggered (bool)), this, SLOT(contextMenuClicked()));

    // only for Debug m_tagContext->addAction(m_tagContext_SAL900Functions = new QAction(QIcon(QString::fromUtf8(":/button icons/advancedSettingsIcon")), "SAL 900 Functions", m_tagContext));
    //QObject::connect(m_tagContext_SAL900Functions, SIGNAL(triggered (bool)), this, SLOT(contextMenuClicked()));

	m_readerContext = new QMenu("Reader Context Menu", m_treeWidget);
	m_readerContext->addAction(m_readerContext_AdvancedSettings = new QAction(QIcon(QString::fromUtf8(":/button icons/advancedSettingsIcon")), "Advanced Reader Settings", m_readerContext));
	QObject::connect(m_readerContext_AdvancedSettings, SIGNAL(triggered (bool)), this, SLOT(contextMenuClicked()));
	m_readerContext->addAction(m_readerContext_RegMap = new QAction(QIcon(QString::fromUtf8(":/button icons/advancedSettingsIcon")), "Register Map", m_readerContext));
	QObject::connect(m_readerContext_RegMap, SIGNAL(triggered (bool)), this, SLOT(contextMenuClicked()));

	/* Create timer for cyclic checking for old tag entries */
	m_guiUpdateTimer = new QTimer();
	m_guiUpdateTimer->setInterval(250);
	m_guiUpdateTimer->setSingleShot(false);
	QObject::connect(m_guiUpdateTimer, SIGNAL(timeout()), this, SLOT(cyclicGuiUpdate()));

	m_contextItem = 0;
}

/*!
 * @brief	Destructor
 */
CTagViewManager::~CTagViewManager()
{
	/* Stop and delete the timer */
	m_guiUpdateTimer->stop();
	m_guiUpdateTimer->deleteLater();
    if (m_reflDiag) delete m_reflDiag;
}

/*!
 * @brief 	Function to set up the behavior of the visualization
 */
void CTagViewManager::setUp(bool showAlias, bool useTimeToLive,  uint msecsToShowInactive, uint msecsToShowOutOfRange, uint msecsToDelete)
{
	/* If time to live is switched off, show all tags active */
	if(m_useTimeToLive && !useTimeToLive){
		foreach(QString readerId, m_readerEntries.keys()){
			showAllTagsActive(readerId);
		}
	}
	m_useTimeToLive = useTimeToLive;


	/* Save the given settings */
	m_showAlias = showAlias;
	m_msecsToShowInactive = msecsToShowInactive;
	m_msecsToShowOutOfRange = msecsToShowOutOfRange;
	m_msecsToDelete = msecsToDelete;

	changeTagNames();
}

/*!
 * @brief 	Function to add a new reader to the list
 */
void CTagViewManager::addReader(QString readerId, QString hardwareRev, QString softwareRev, QString action, bool showRSSI, uchar rssiChildCount, QStringList rssiChildNames, bool showTID)
{
	trc(0x02, "The reader >>" + readerId + "<< was added");
	/* If the reader was delete but we have still the entry in the treewidget, take this */
	if(m_readerEntriesToDelete.contains(readerId)){

		/* Move the reader to the active map */
		m_readerEntries.insert(readerId, m_readerEntriesToDelete.value(readerId));

		/* Save the information of this reader */
		SReaderInfo info;
		info.showRSSI = showRSSI;
		info.rssiChildCount = rssiChildCount;
		info.rssiChildNames = rssiChildNames;
		info.readCounter = 0;
		m_readerInfo.insert(readerId, info);
		/* Remove the reader from the inactive map */
		m_readerEntriesToDelete.remove(readerId);

		return;
	}

	/* Create new item for the reader */
	QTreeWidgetItem* readerItem = new QTreeWidgetItem(QStringList() << readerId << hardwareRev << softwareRev << action << "Online" );
	m_treeWidget->addTopLevelItem(readerItem);

	/* Set up font of the columns */
	readerItem->setFont(m_ReaderColumn, m_readerIdFont);
	readerItem->setFont(m_HardwareColumn, m_readerVersionFont);
	readerItem->setFont(m_SoftwareColumn, m_readerVersionFont);
	readerItem->setFont(m_ActionColumn, m_readerVersionFont);
	readerItem->setFont(m_StateColumn, m_readerStateFont);

	/* Set up foreground of the columns */
	readerItem->setForeground(m_ReaderColumn, QBrush( QColor( Qt::white ) ));
	readerItem->setForeground(m_HardwareColumn, QBrush( QColor( Qt::white ) ));
	readerItem->setForeground(m_SoftwareColumn, QBrush( QColor( Qt::white ) ));
	readerItem->setForeground(m_ActionColumn, QBrush( QColor( Qt::white ) ));

	/* Set up foreground of the columns */
	readerItem->setBackground(m_ReaderColumn, QBrush( QColor( Qt::darkGray ) ));
	readerItem->setBackground(m_HardwareColumn, QBrush( QColor( Qt::darkGray ) ));
	readerItem->setBackground(m_SoftwareColumn, QBrush( QColor( Qt::darkGray ) ));
	readerItem->setBackground(m_ActionColumn, QBrush( QColor( Qt::darkGray ) ));
	readerItem->setBackground(m_StateColumn, QBrush( QColor( Qt::darkGray ) ));

	/* Set the flags for the new item */
	readerItem->setFlags(Qt::ItemIsEnabled);

    readerItem->setIcon(m_ReaderColumn, QIcon(":/tree widget icons/passiveReader"));

	/* Insert the new reader object into the map */
	m_readerEntries.insert(readerId, readerItem);

	if(m_readerEntries.count() == 1) // first reader in the list
		m_treeWidget->setCurrentItem(readerItem);
	/* Save the information of this reader */
	SReaderInfo info;
	info.showRSSI = showRSSI;
	info.rssiChildCount = rssiChildCount;
	info.rssiChildNames = rssiChildNames;
	info.readCounter = 0;
    info.showTID = showTID;
	m_readerInfo.insert(readerId, info);

	/* Create reader information */
	QTreeWidgetItem* readerInfoItem = new QTreeWidgetItem(QStringList() << "Reader Information");
	readerInfoItem->setIcon(0, QIcon(":/tree widget icons/information"));
	readerInfoItem->setFirstColumnSpanned(true);
	readerInfoItem->setFont(0, m_readerInfoFont);
	readerInfoItem->setFlags(Qt::ItemIsEnabled);
	readerItem->addChild(readerInfoItem);

	QTreeWidgetItem* differentTagItem = new QTreeWidgetItem(QStringList() << "Different Tags" << "0");
	differentTagItem->setFont(0, m_readerInfoDataFont);
	differentTagItem->setFont(1, m_readerInfoDataFont);
	readerInfoItem->addChild(differentTagItem);

	QTreeWidgetItem* readerCountItem = new QTreeWidgetItem(QStringList() << "Read Count" << QString::number(info.readCounter));
	readerCountItem->setFont(0, m_readerInfoDataFont);
	readerCountItem->setFont(1, m_readerInfoDataFont);
	readerInfoItem->addChild(readerCountItem);

	QTreeWidgetItem* readerSpeedItem = new QTreeWidgetItem(QStringList() << "Reads per Second" << "0");
	readerSpeedItem->setFont(0, m_readerInfoDataFont);
	readerSpeedItem->setFont(1, m_readerInfoDataFont);
	readerInfoItem->addChild(readerSpeedItem);

	readerItem->setExpanded(true);
}

/*!
 * @brief	Function sets the reader to removed. The reader stays in the list, but is no more used. The reader entry is deleted on clearOfflineReader.
 */
void CTagViewManager::removeReader(QString readerId)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		return;
	}

	if(m_activeReader.contains(readerId))
		m_activeReader.removeOne(readerId);

	/* Remove all tags of this reader */
	clearTagsOfReader(readerId);
	m_readerTagsInfo.remove(readerId);

	/* Remove the entry out of the map with the active reader */
	QTreeWidgetItem* item = m_readerEntries.value(readerId);
	item->setExpanded(false);
	m_readerEntries.remove(readerId);
	m_readerInfo.remove(readerId);

	/* Put the reader into the map with the inactive reader */
	m_readerEntriesToDelete.insert(readerId, item);
}

/*!
 * @brief	Function is called if reader changes state
 */
void CTagViewManager::readerChangedState( QString readerId, ReaderInterface::HandlerState state, QString stateName, QString stateDescription )
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		//fatal("Got readerId that does not exist: " + readerId);
		return;
	}

	/* Get the treewidget entry */
	QTreeWidgetItem* item = m_readerEntries.value(readerId);
	if(item == 0)
	{
		trc(0x02, "Do not know this reader ID");
		return;
	}

	/* Set the new text in the right color */
	item->setText(m_StateColumn, stateName);
    item->setToolTip(m_StateColumn, "");
	if(state == ReaderInterface::STATE_ONLINE)
		item->setForeground(m_StateColumn, QBrush( QColor( Qt::green ) ));
    else if(state == ReaderInterface::STATE_ERROR){
        item->setToolTip(m_StateColumn, stateDescription);
        item->setForeground(m_StateColumn, QBrush( QColor( Qt::yellow ) ));
        QMessageBox::critical(m_treeWidget, QString("Reader Error"), QString("Reader reported error(s):\n").append(stateDescription));
    }
	else if(state == ReaderInterface::STATE_OFFLINE){
		item->setForeground(m_StateColumn, QBrush( QColor( Qt::red ) ));
		clearTagsOfReader(readerId);
		removeReader(readerId);
	}
	else{
		item->setForeground(m_StateColumn, QBrush( QColor( Qt::red ) ));
		clearTagsOfReader(readerId);
	}

}
void CTagViewManager::changeHardFirm(QString readerId, QString hardwareRev, QString softwareRev)
{
	if( !m_readerEntries.contains(readerId) ){
		return;
	}

	/* Get the treewidget entry */
	QTreeWidgetItem* item = m_readerEntries.value(readerId);

	item->setText(1,hardwareRev);
	item->setText(2,softwareRev);
}

/*!
 * @brief	Function is called if reader changes action
 */
void CTagViewManager::readerChangedAction(QString readerId, QString action)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		return;
	}

	/* Get the treewidget entry */
	QTreeWidgetItem* item = m_readerEntries.value(readerId);
	if(item == 0)
	{
		trc(0x02, "Do not know this reader ID");
		return;
	}

	/* Set the new text */
	item->setText(m_ActionColumn, action);
}

/*!
 * @brief	Function is called if reader changes action
 */
void CTagViewManager::readerSetRSSI(QString readerId, bool on, uchar rssiChildCount, QStringList rssiChildNames)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		fatal("Got readerId that does not exist: " + readerId);
		return;
	}

	/* Get the reader info of this reader */
	SReaderInfo& readerInfo = m_readerInfo[readerId];

	/* If rssi is already turned on, skip */
	if(readerInfo.showRSSI == on)
		return;

	/* Store the rssi description */
	readerInfo.showRSSI = on;
	readerInfo.rssiChildCount = rssiChildCount;
	readerInfo.rssiChildNames = rssiChildNames;

	/* Add or remove the rssi entry from each tag that is already in the list */
	QMap<Gen2Tag, STagInfo> &tagsOfReader = m_readerTagsInfo[readerId];
	foreach(Gen2Tag tagId, tagsOfReader.keys())
	{
		STagInfo &s = tagsOfReader[tagId];
		if(s.widgetEntry == 0)
		{
			trc(0x09, "Have no pointer to the widgetEntry");
			continue;
		}
		if(on)
			addRSSIInfo(s.widgetEntry, rssiChildCount, rssiChildNames);
		else
			removeRSSIInfo(s.widgetEntry);
	}

}

void CTagViewManager::readerSetTagID(QString readerId, bool on)
{
    if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
        fatal("Got readerId that does not exist: " + readerId);
        return;
    }
    /* Get the reader info of this reader */
    SReaderInfo& readerInfo = m_readerInfo[readerId];
    readerInfo.showTID = on;
    /* Add or remove Tag Identifier entry from each tag that is already in the list */
    QMap<Gen2Tag, STagInfo> &tagsOfReader = m_readerTagsInfo[readerId];
    foreach(Gen2Tag tagId, tagsOfReader.keys())
    {
        STagInfo &s = tagsOfReader[tagId];
        if(s.widgetEntry == 0)
        {
            trc(0x09, "Have no pointer to the widgetEntry");
            continue;
        }
        if(on)
            s.widgetEntry->child(m_TIDRow)->setHidden(false);
        else
            s.widgetEntry->child(m_TIDRow)->setHidden(true);
    }
   
}

/*!
 * @brief	Function removes all entries of offline reader
 */
void CTagViewManager::clearOfflineReader()
{
	/* Remove and delete every inactive reader from the treewidget */
	foreach(QString readerId, m_readerEntriesToDelete.keys())
	{
		QTreeWidgetItem* item = m_readerEntriesToDelete.value(readerId);
		m_readerEntriesToDelete.remove(readerId);
		if(item == 0)
			continue;
		int idxTopLevel = m_treeWidget->indexOfTopLevelItem(item);
		if (idxTopLevel != -1)
			m_treeWidget->takeTopLevelItem(idxTopLevel);
		delete item;
	}

	/* Clear the map with the inactive reader */
	m_readerEntriesToDelete.clear();
}

/*!
 * @brief	Function to reset the reader info of each reader
 */
void CTagViewManager::resetReaderInfo()
{
	foreach(QString readerId, m_readerEntries.keys()){
		resetReaderInfo(readerId);
	}
}

/*!
 * @brief	Function to reset the reader info of the given reader
 */
void CTagViewManager::resetReaderInfo(QString readerId)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		return;
	}

	/* Reset the reader info */
	SReaderInfo& readerInfo = m_readerInfo[readerId];
	readerInfo.readCounter = 0;

	/* Update the visualization of the reader info */
	updateReaderInfo(readerId);
}

/*!
 * @brief	Function to reset the tag info of the tags of the given reader
 */
void CTagViewManager::resetTagInfo(QString readerId)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		return;
	}

	/* Get the tags of the reader */
	QMap<Gen2Tag, STagInfo> &tagsOfReader = m_readerTagsInfo[readerId];
	foreach(Gen2Tag tagId, tagsOfReader.keys())
	{
		STagInfo &s = tagsOfReader[tagId];

		/* Reset read counter */
		s.readCounter = 0;
        s.agc = 0;
        s.qChannel = 0;

		/* Update the visualization of the tag info */
		updateTagInfo(readerId, tagId);
	}
}

/*!
 * @brief	Function to clear all tags of each reader
 */
void CTagViewManager::clearTags()
{
	/* Remove and Delete every tag of every reader from the treewidget */
	foreach(QString readerId, m_readerTagsInfo.keys())
	{
		clearTagsOfReader(readerId);
		resetReaderInfo(readerId);
	}

	/* Clear the map with the tag entries */
	m_tagEntries.clear();

	m_overallDifferentTags.clear();

	/* Emit new tag counts */
	emit newTagCount(m_tagEntries.size());
	emit newDifferentTagCount(QSet<Gen2Tag>::fromList(m_tagEntries.values()).size());
	emit newOverallDifferentTagCount(m_overallDifferentTags.size());
}


/*!
 * @brief	Function to clear the tags of the specified reader
 */
void CTagViewManager::clearTagsOfReader(QString readerId)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		return;
	}

	/* Remove the tags */
	m_readerTagsInfo.remove(readerId);
	QTreeWidgetItem* readerItem = m_readerEntries.value(readerId);
	if(readerItem == 0)
	{
		trc(0x02, "Do not know this reader ID");
		return;
	}

	int count = readerItem->childCount();
	for(int i = 1; i < count; i++)
	{
		removeTagEntry(readerItem->child(1));
	}

	/* Emit new tag count */
	emit newTagCount(m_tagEntries.size());
	emit newDifferentTagCount(QSet<Gen2Tag>::fromList(m_tagEntries.values()).size());

}

/*!
 * @brief	Function to clear all information of every reader
 */
void CTagViewManager::clearAllReaderTagInfos()
{
	foreach(QString readerId, m_readerEntries.keys())
	{
		resetReaderInfo(readerId);
		resetTagInfo(readerId);
	}
    if(m_reflDiag)
    {
        delete m_reflDiag;
        m_reflDiag = NULL;
    }
}

/*!
 * @brief 	Function to select only one single reader and only show this reader in the view
 */
void CTagViewManager::selectSingleReader(QString readerId)
{
	foreach(QString rId, m_readerEntries.keys())
	{
		QTreeWidgetItem* item = m_readerEntries.value(rId);
		if(rId != readerId)
			item->setHidden(true);
		else
			item->setHidden(false);
	}
	foreach(QString rId, m_readerEntriesToDelete.keys())
	{
		QTreeWidgetItem* item = m_readerEntriesToDelete.value(rId);
		item->setHidden(true);
	}

}

/*!
 * @brief 	Function to show all reader in the view
 */
void CTagViewManager::showAllReader()
{
	foreach(QString rId, m_readerEntries.keys())
	{
		QTreeWidgetItem* item = m_readerEntries.value(rId);
		item->setHidden(false);
	}
	foreach(QString rId, m_readerEntriesToDelete.keys())
	{
		QTreeWidgetItem* item = m_readerEntriesToDelete.value(rId);
		item->setHidden(false);
	}

}

/*!
 * @brief 	Function to set reader as active, only active reader are checked for old tags
 */
void CTagViewManager::setActive(QString readerId)
{
	m_activeReader.clear();

	if(readerId.isNull()){
		foreach(readerId, m_readerEntries.keys()){
			m_activeReader << readerId;
		}
	}
	else
	{
		if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
			return;
		}
		m_activeReader << readerId;
	}
}

/*!
 * @brief 	Function to remove reader from the active list
 */
void CTagViewManager::setUnactive(QString readerId)
{
	m_activeReader.removeAll(readerId);
}




/*******************************************************************************************************************************/
// Interrupts from Reader


/*!
 * @brief 	Cyclic inventory result interrupt from the reader
 */
void CTagViewManager::cyclicInventoryResult( QString readerId, QList<InventoryTagInfo> tags, quint32 freq, quint8 status )
{
	trc(0x08, QString("Got from reader: >> %1<<  %2 tags").arg(readerId).arg(tags.size()));

	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		fatal("Got readerId that does not exist: " + readerId);
		return;
	}

	/* Get the item of the reader */
	QTreeWidgetItem* readerItem = m_readerEntries.value(readerId);
	if(readerItem == 0)
	{
		trc(0x02, "Do not know this reader ID");
		return;
	}

	/* Get the current tags of the reader */
	QMap<Gen2Tag, STagInfo> &tagsOfReader = m_readerTagsInfo[readerId];

	STagInfo tagInfo;
	QTreeWidgetItem* tagItem;
    m_treeWidget->setUpdatesEnabled(false);
    foreach( InventoryTagInfo tag, tags)
    {
	    /* If the tag was already read, get the information and the treewidget entry */
        Gen2Tag invtag;
        invtag.epc = tag.epc;
        invtag.tid = tag.tid;

	    if(tagsOfReader.contains(invtag)){
		    tagInfo = tagsOfReader.value(invtag);
		    tagItem = tagsOfReader.value(invtag).widgetEntry;
	    }
	    /* If the tag was not read before */
	    else
	    {
            tagInfo.readCounter = 0;
            tagInfo.qChannel = 0;
            tagInfo.subCarrierInPhase = 0;
            tagInfo.lastRssiUpdate = QTime::currentTime().addSecs(-60);
		    tagItem = createTagEntry(readerId, invtag);
		    if(tagItem == 0)
            {
                m_treeWidget->setUpdatesEnabled(true);
			    return;
            }
		    m_overallDifferentTags.insert(invtag);
		    emit newTagCount(m_tagEntries.size());
		    emit newDifferentTagCount(QSet<Gen2Tag>::fromList(m_tagEntries.values()).size());
		    emit newOverallDifferentTagCount(m_overallDifferentTags.size());

	    }
	    /* Change and show the state of the tag to ACTIVE */
	    changeTagState(readerId, invtag, STATE_ACTIVE);

	    /* Save the tag information */
	    tagInfo.readCounter += 1;
	    tagInfo.lastTimeSeen = QTime::currentTime();
	    tagInfo.widgetEntry = tagItem;
        tagInfo.rssi = tag.rssi;
        tagInfo.agc = tag.agc;
        if (tag.qChannel)
            tagInfo.qChannel += 1;
        if (tag.subCarrierInPhase)
            tagInfo.subCarrierInPhase += 1;
        tagInfo.tid = tag.tid;

	    /* Overwrite the entry of the tag */
	    tagsOfReader.insert(invtag, tagInfo);
	    updateTagInfo(readerId, invtag);
    }

	SReaderInfo &readerInfo = m_readerInfo[readerId];
    if (0 == tags.size())
    {
        readerInfo.err2Count[status]++;
        readerInfo.freq2NoTagsFound[freq]++;
        if (static_cast<signed char>(status) == ERR_REFLECTED_POWER)
        {
            if (m_reflDiag == NULL)
            {
                /* Just calling QMessageBox::critical() causes recursion loop */
                m_reflDiag = new QMessageBox(QMessageBox::Critical,
                                             "Reflected Power",
                                             QString(tr("Reflected power is too high @ %1 kHz. Please consider tuning or reducing tx power!")).arg(freq),
                                             QMessageBox::Ok,
                                             NULL);
                m_reflDiag->show();
            }
        }
    }
	readerInfo.readCounter += tags.size();
	updateReaderInfo(readerId);

	readerItem->setExpanded(true);
    m_treeWidget->setUpdatesEnabled(true);
}

/*!
 * @brief	Function that creates a new tag entry
 */
QTreeWidgetItem* CTagViewManager::createTagEntry (QString readerId, Gen2Tag tag)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		fatal("Got readerId that does not exist: " + readerId);
		return 0;
	}
	if (tag.epc.size() == 0) tag.epc = QString("<zero>");

	// get the item of the reader
	QTreeWidgetItem* readerItem = m_readerEntries.value(readerId);
	if(readerItem == 0)
	{
		trc(0x02, "Do not know this reader ID");
		return 0;
	}

	// create new tree widget item
	QTreeWidgetItem* tagItem = new QTreeWidgetItem();
	readerItem->addChild(tagItem);
    tagItem->setIcon(0, QIcon(":/tree widget icons/passiveTag"));
	tagItem->setFlags(Qt::ItemIsEnabled);

    // Tag Identifier
    QTreeWidgetItem* tidItem = new QTreeWidgetItem();
    tagItem->addChild(tidItem);
    tidItem->setText(0, "Tag Identifier:");
    tidItem->setText(1, " N/A");
    tidItem->setFont(0, m_tagInfoDataFont);
    tidItem->setFont(1, m_tagInfoDataFont);
    if(m_readerInfo.value(readerId).showTID)
    {
        tagItem->child(m_TIDRow)->setHidden(false);
    }
    else
    {
        tagItem->child(m_TIDRow)->setHidden(true);
    }
    //Read Count
	QTreeWidgetItem* readCountItem = new QTreeWidgetItem();
	tagItem->addChild(readCountItem);
	readCountItem->setText(0, "Read Count:");
	readCountItem->setText(2, ".");
  	tagItem->setExpanded(true);
    // additional tag information
    QTreeWidgetItem* tagInfoItem = new QTreeWidgetItem(QStringList() << "Tag Information");
    //tagInfoItem->setIcon(0, QIcon(":/tree widget icons/information"));
    tagInfoItem->setFirstColumnSpanned(true);
    tagInfoItem->setFont(0, m_readerInfoDataFont);
    tagInfoItem->setFlags(Qt::ItemIsEnabled);
    tagItem->addChild(tagInfoItem);

    QTreeWidgetItem* agcItem = new QTreeWidgetItem(QStringList() << "Agc:" << "0");
    agcItem->setFont(0, m_readerInfoDataFont);
    agcItem->setFont(1, m_readerInfoDataFont);
    tagInfoItem->addChild(agcItem);

    QTreeWidgetItem* iqCountItem = new QTreeWidgetItem(QStringList() << "I/Q Channel Count" << "0%\t0%");
    iqCountItem->setFont(0, m_readerInfoDataFont);
    iqCountItem->setFont(1, m_readerInfoDataFont);
    tagInfoItem->addChild(iqCountItem);

    QTreeWidgetItem* iqDistItem = new QTreeWidgetItem(QStringList() << "I/Q Channel Distribution" << "0%\t0%");
    iqDistItem->setFont(0, m_readerInfoDataFont);
    iqDistItem->setFont(1, m_readerInfoDataFont);
    tagInfoItem->addChild(iqDistItem);

	// set up font of the entry
	tagItem->setFont(0, m_tagIdFont);

	readCountItem->setFont(0, m_tagInfoDataFont);
	readCountItem->setFont(1, m_tagInfoDataFont);
	readCountItem->setFont(2, m_tagInfoDataFont);


	if(m_readerInfo.value(readerId).showRSSI)
		addRSSIInfo(tagItem, m_readerInfo.value(readerId).rssiChildCount, m_readerInfo.value(readerId).rssiChildNames);


	// save the new treewidget entry
	m_tagEntries.insert(tagItem, tag);


	// set tag name
	TagActionInfo t;
	// if the sowAlias setting is activated get the config
	if(m_showAlias && m_dataHandler->getConfig(tag.epc, t)){
		// insert the alias name and some more information
		tagItem->setFirstColumnSpanned(false);
		tagItem->setText(0, t.aliasName);
		if(t.performAction){
			if(t.type == StartApp)
				tagItem->setText(1, "Starts the Application: " + t.appPath);
			else
				tagItem->setText(1, "Shows the Picture: " + t.picPath);
		}
	}
	else{
		// insert the epc of the tag
		tagItem->setFirstColumnSpanned(true);
		tagItem->setText(0, tag.epc);
	}


	return tagItem;
}

/*!
 * @brief	Function that removes a tag entry
 */
void CTagViewManager::removeTagEntry(QTreeWidgetItem* tagItem)
{

	while(tagItem->childCount() != 0)
		delete tagItem->takeChild(0);

	QTreeWidgetItem* readerItem = tagItem->parent();
	if(readerItem != 0)
	{
		trc(0x08, "tag removed");
		readerItem->removeChild(tagItem);
	}

	m_tagEntries.remove(tagItem);

	delete tagItem;

}

/*!
 * @brief	Function that adds a rssi information to a tag entry
 */
void CTagViewManager::addRSSIInfo(QTreeWidgetItem* tagItem, uchar rssiChildCount, QStringList rssiChildNames)
{
	QTreeWidgetItem* rssiItem = new QTreeWidgetItem();
	tagItem->addChild(rssiItem);
	rssiItem->setText(0, "Input Power:");
	QProgressBar* pb = new QProgressBar(m_treeWidget);
	m_treeWidget->setItemWidget(rssiItem, 1, pb);
	pb->setValue(-60);
	pb->setMaximum(-60+31.5);
    pb->setMinimum(-60);
	pb->setMaximumHeight(15);
    pb->setFormat("%v dBm");
	rssiItem->setFont(0, m_tagInfoDataFont);

	rssiItem->setExpanded(false);

	for(int i = 0; i < rssiChildCount; i++){
		QTreeWidgetItem* rssi_Child_Item = new QTreeWidgetItem();
		rssiItem->addChild(rssi_Child_Item);
		rssi_Child_Item->setText(0, rssiChildNames.at(i));
		QProgressBar* pbI = new QProgressBar(m_treeWidget);
		m_treeWidget->setItemWidget(rssi_Child_Item, 1, pbI);
		pbI->setValue(0);
		pbI->setMaximum(15);
		pbI->setMaximumHeight(15);
		pbI->setFormat("%v");
		rssi_Child_Item->setFont(0, m_tagInfoDataFont);
	}
}

/*!
 * @brief	Function that removes the rssi informartion from a tag entry
 */
void CTagViewManager::removeRSSIInfo(QTreeWidgetItem* tagItem)
{
	if(tagItem->childCount() < 2)
		return;

	while(tagItem->child(m_RSSIRow)->childCount() != 0)
		delete tagItem->child(m_RSSIRow)->takeChild(0);
	delete tagItem->takeChild(m_RSSIRow);

	return;
}


/*!
 * @brief	Function that updates the tag info in the view according to the stored data
 */
void CTagViewManager::updateTagInfo(QString readerId, Gen2Tag tagId)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		fatal("Got readerId that does not exist: " + readerId);
		return;
	}

	// get the current tags of the reader
	QMap<Gen2Tag, STagInfo> &tagsOfReader = m_readerTagsInfo[readerId];

	if(!tagsOfReader.contains(tagId)){
		fatal("Got tagId that does not exist: " + tagId.epc);
		return;
	}

	STagInfo &tagInfo = tagsOfReader[tagId];

	QTreeWidgetItem* tagItem = tagInfo.widgetEntry;

	// Update read count
	QTreeWidgetItem* readCountItem = tagItem->child(m_ReadCountRow);
	if(readCountItem == 0){
		fatal("Invalid pointer to readCountItem of tag: " + tagId.epc);
		return;
	}

	readCountItem->setText(1, QString::number(tagInfo.readCounter));

	QString runner;
	for(int i = 0; i < tagInfo.readCounter%10; i++)
		runner += " ";
	runner += ".";
	if(runner.size() >= 10)
		runner = ".";
	readCountItem->setText(2, runner);

    //update tag info
    QTreeWidgetItem* tagInfoItem = tagItem->child(m_TagInfoRow);
    QTreeWidgetItem* agcItem = tagInfoItem->child(0);
    QTreeWidgetItem* iqCountItem = tagInfoItem->child(1);
    QTreeWidgetItem* iqDistItem = tagInfoItem->child(2);
    if (agcItem == 0 || iqCountItem == 0 || iqDistItem == 0) {
        fatal(QString("Invalid pointer for agcItem=%1 or iqCountItem=%2 or iqDistItem=%3")
            .arg((int)agcItem).arg((int)iqCountItem).arg((int)iqDistItem));
        return;
    }
    agcItem->setText(1, QString("%1dB").arg(tagInfo.agc));
    iqCountItem->setText(1, QString("I: %1\tQ: %2").arg(tagInfo.readCounter-tagInfo.qChannel).arg(tagInfo.qChannel));
    int qc = qRound((100.0*(float)tagInfo.qChannel)/(float)tagInfo.readCounter);
    int ic = 100 - qc;
    iqDistItem->setText(1, QString("I: %1%\tQ: %2%").arg(ic).arg(qc));

    //update rssi bar only every 100ms
    bool doRssiUpdate;
    if (tagInfo.lastRssiUpdate.msecsTo(QTime::currentTime()) > 100)
        doRssiUpdate = true;
    else
        doRssiUpdate = false;

	if(m_readerInfo.value(readerId).showRSSI && doRssiUpdate)
	{
		//save new rssi update time
        tagInfo.lastRssiUpdate = QTime::currentTime();
        tagsOfReader.insert(tagId, tagInfo);    
        // Update rssi value
		QTreeWidgetItem* rssiItem = tagItem->child(m_RSSIRow);
		if(rssiItem == 0 && m_readerInfo.value(readerId).showRSSI){
			fatal("Invalid pointer to readCountItem of tag: " + tagId.epc);
			return;
		}

		int val = tagInfo.rssi.pin;
		QProgressBar* pb = qobject_cast<QProgressBar*>(m_treeWidget->itemWidget(rssiItem, 1));
		if(pb == 0)
			return;
        pb->setMinimum(tagInfo.rssi.pinMin);
        pb->setMaximum(tagInfo.rssi.pinMax);
		pb->setValue( (val > pb->maximum()) ? pb->maximum() : val);

        QList<int> qi;
        qi.append(tagInfo.rssi.q);
        qi.append(tagInfo.rssi.i);
		for(int i = 0; i < rssiItem->childCount(); i++){
			if(tagItem->child(m_RSSIRow)->child(i) == 0)
				continue;
			QProgressBar* pbI = qobject_cast<QProgressBar*>(m_treeWidget->itemWidget(tagItem->child(m_RSSIRow)->child(i), 1));

			int val = (qi.size() < i+1) ? 0 : qi.at(i);
			pbI->setValue( (val > pbI->maximum()) ? pbI->maximum() : val);
		}
	}
    if(m_readerInfo.value(readerId).showTID )
    {
        QTreeWidgetItem* tidItem = tagItem->child(m_TIDRow); 
        if (tagInfo.tid.length() <=2)
        {
            tidItem->setTextColor(1, Qt::lightGray);
            //tidItem->setText(1, "");
        }
        else
        {
            tidItem->setTextColor(1, Qt::black);
            tidItem->setText(1, QString("%1").arg(tagInfo.tid));
        }
    }

}

/*!
 * @brief	Function that updates the reader info in the view according to the stored data
 */
void CTagViewManager::updateReaderInfo(QString readerId)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		fatal("Got readerId that does not exist: " + readerId);
		return;
	}

	SReaderInfo& readerInfo = m_readerInfo[readerId];

	QTreeWidgetItem* readerItem = m_readerEntries.value(readerId);
	QTreeWidgetItem* readerInfoItem = readerItem->child(0);

	QTreeWidgetItem* differentTagItem = readerInfoItem->child(0);
	differentTagItem->setText(1, QString::number(m_readerTagsInfo.value(readerId).size()));

	QTreeWidgetItem* readerCountItem = readerInfoItem->child(1);
	readerCountItem->setText(1, QString::number(readerInfo.readCounter));

	QTreeWidgetItem* readerSpeedItem = readerInfoItem->child(2);
	readerSpeedItem->setText(1, QString::number(m_readRateCalc->getReadRate(readerId)));
}


/**********************************************************************************************************************/
// Cyclic gui value refresh

/*!
 * @brief	Function that changes the visualization of the tag names. Either the epc or the alias name is shown.
 */
void CTagViewManager::changeTagNames (  )
{
	// For every reader
	foreach(QString readerId, m_readerTagsInfo.keys())
	{
		QMap<Gen2Tag, STagInfo> tagsOfReader = m_readerTagsInfo.value(readerId);
		// and every tag of the reader
		foreach(Gen2Tag tagId, tagsOfReader.keys())
		{
			// get the information of this tag
			STagInfo s = tagsOfReader.value(tagId);
			if(s.widgetEntry == 0)
			{
				trc(0x09, "Have no pointer to the widgetEntry");
				continue;
			}

			// insert the text for this tag
			TagActionInfo t;
			if(m_showAlias && m_dataHandler->getConfig(tagId.epc, t)){
				s.widgetEntry->setFirstColumnSpanned(false);
				s.widgetEntry->setText(0, t.aliasName);
				if(t.performAction){
					if(t.type == StartApp)
						s.widgetEntry->setText(1, "Starts the Application: " + t.appPath);
					else
						s.widgetEntry->setText(1, "Shows the Picture: " + t.picPath);
				}
			}
			else{
				s.widgetEntry->setFirstColumnSpanned(true);
				s.widgetEntry->setText(0, tagId.epc);
			}
		}
	}
}

/*!
 * @brief	Function that starts the cyclic gui update
 */
void CTagViewManager::startGuiUpdate()
{
	if(!m_guiUpdateTimer->isActive())
		m_guiUpdateTimer->start();
}

/*!
 * @brief	Function to stop the gui update
 */
void CTagViewManager::stopGuiUpdate()
{
	m_guiUpdateTimer->stop();
}

/*!
 * @brief	Slot is cyclic called to update the gui
 */
void CTagViewManager::cyclicGuiUpdate ( )
{
    m_treeWidget->setUpdatesEnabled(false);

	foreach(QString readerId, m_activeReader)
	{
		if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
			fatal("Got readerId that does not exist: " + readerId);
			continue;
		}

		updateReaderInfo(readerId);
		if(m_useTimeToLive)
			checkForOld(readerId);
	}
    m_treeWidget->setUpdatesEnabled(true);
}


/*!
 * @brief	Function to check for old tag id entries
 */
void CTagViewManager::checkForOld(QString readerId)
{
	if( !m_readerInfo.contains(readerId) || !m_readerEntries.contains(readerId) ){
		fatal("Got readerId that does not exist: " + readerId);
		return;
	}

	QMap<Gen2Tag, STagInfo> &tagsOfReader = m_readerTagsInfo[readerId];
	// and every tag of the reader
	foreach(Gen2Tag tagId, tagsOfReader.keys())
	{
		// get the information of this tag
		STagInfo &s = tagsOfReader[tagId];

		// check how long the tag was not read with the settings
		if((uint)s.lastTimeSeen.msecsTo(QTime::currentTime()) > m_msecsToDelete){
			changeTagState(readerId, tagId, STATE_DELETE);
			emit oldTagEntryRemoved(readerId, tagId);
			continue;
		}
		if((uint)s.lastTimeSeen.msecsTo(QTime::currentTime()) > m_msecsToShowOutOfRange){
			changeTagState(readerId, tagId, STATE_OUTOFRANGE);
			continue;
		}
		else if((uint)s.lastTimeSeen.msecsTo(QTime::currentTime()) > m_msecsToShowInactive){
			changeTagState(readerId, tagId, STATE_INACTIVE);
			continue;
		}
		else{
			changeTagState(readerId, tagId, STATE_ACTIVE);
			continue;
		}
	}
}


/*!
 * @brief	Function to set the state of a tag and to visualize this at the view
 */
void CTagViewManager::changeTagState(QString readerId, Gen2Tag tagId, TagState tagState)
{
	QMap<Gen2Tag, STagInfo> &tagsOfReader = m_readerTagsInfo[readerId];

	if(!tagsOfReader.contains(tagId))
		return;

	STagInfo &s = tagsOfReader[tagId];
	if(s.widgetEntry == 0)
	{
		trc(0x09, "Have no pointer to the widgetEntry");
		return;
	}

	if(tagState != s.tagState)
	{
		switch(tagState){
		case STATE_ACTIVE:
			for(int i = 0; i < s.widgetEntry->columnCount(); i++){
				s.widgetEntry->setForeground(i, QBrush( QColor( Qt::black ) ));
				s.widgetEntry->setBackground(i, QBrush( QColor( Qt::transparent ) ));
			}

			s.tagState = tagState;

			return;

		case STATE_INACTIVE:
			for(int i = 0; i < s.widgetEntry->columnCount(); i++){
				s.widgetEntry->setBackground(i, QBrush( QColor( Qt::lightGray ) ));
			}

			s.tagState = tagState;
            s.agc = 0;
			updateTagInfo(readerId, tagId);

			return;

		case STATE_OUTOFRANGE:
			for(int i = 0; i < s.widgetEntry->columnCount(); i++){
				s.widgetEntry->setForeground(i, QBrush( QColor( Qt::darkGray ) ));
			}

			s.tagState = tagState;

			return;

		case STATE_DELETE:
			removeTagEntry(s.widgetEntry);
			tagsOfReader.remove(tagId);

			newTagCount(m_tagEntries.size());
			newDifferentTagCount(QSet<Gen2Tag>::fromList(m_tagEntries.values()).size());

			return;
		}

	}
}

/*!
 * @brief	Function that sets the state of each tag of the specified reader to active
 */
void CTagViewManager::showAllTagsActive(QString readerId)
{
	if(!m_readerTagsInfo.contains(readerId))
		return;

	QMap<Gen2Tag, STagInfo> tagsOfReader = m_readerTagsInfo.value(readerId);
	foreach(Gen2Tag tagId, tagsOfReader.keys())
	{
		changeTagState(readerId, tagId, STATE_ACTIVE);
	}
}

/*******************************************************************************************************************************
 * Context menus
 */

void CTagViewManager::showPopup(const QPoint & iPoint)
{
	// save the item, on which was clicked in a member
	m_contextItem = m_treeWidget->itemAt(iPoint);
	if ( m_contextItem == 0 )
		return;

	if(!m_tagEntries.keys().contains(m_contextItem) && !m_readerEntries.values().contains(m_contextItem))
	{
		m_contextItem = 0;
		return;
	}

	// if the item is a reader item, show the context menu for reader
	if(m_contextItem->parent() == 0)
		m_readerContext->exec(QPoint(m_treeWidget->mapToGlobal(iPoint).x(), m_treeWidget->mapToGlobal(iPoint).y()+20));
	// else show the context menu of the tags
	else
		m_tagContext->exec(QPoint(m_treeWidget->mapToGlobal(iPoint).x(), m_treeWidget->mapToGlobal(iPoint).y()+20));
}

void CTagViewManager::contextMenuClicked()
{
	// if there was no item saved, return
	if(m_contextItem == 0)
		return;

	// if the saved item is a reader item
	if(m_contextItem->parent() == 0)
	{
		// get the reader id
		QString readerId = m_readerEntries.key(m_contextItem);
		if(readerId.isNull())
			return;

		// emit the signal, that the advanced settings dialog is requested
		if((QAction*)sender() == m_readerContext_AdvancedSettings)
		{
			emit requestReaderAdvancedSettings(readerId);
		}
		// emit the signal, that the advanced settings dialog is requested
		if((QAction*)sender() == m_readerContext_RegMap)
		{
			emit requestReaderRegisterMap(readerId);
		}

	}

	// if the saved item is a tag item
	else if(m_tagEntries.contains(m_contextItem))
	{
		// get the epc of the tag
		Gen2Tag gen2tag = m_tagEntries.value(m_contextItem);
    
		if(gen2tag.epc.isNull())
			return;

		QString readerId = m_readerEntries.key(m_contextItem->parent());
		if(readerId.isNull())
			return;

		// emit the right signal, that a dialog is requested
		if((QAction*)sender() == m_tagContext_Settings)
		{
			emit requestTagSettings(gen2tag.epc);
		}
		else if((QAction*)sender() == m_tagContext_AdvancedSettings)
		{
			emit requestTagAdvancedSettings(readerId, gen2tag);
		}
        //else if((QAction*)sender() == m_tagContext_SAL900Functions) //  for Debug only
        //{
        //    emit requestSL900A(readerId, epc);
        //}
	}
}

void CTagViewManager::itemDoubleClicked ( QTreeWidgetItem * item )
{
	// if a tag item was double clicked, emit the signal that the normal settings dialog is requested
	if(item->parent() == 0)
		trc(0x01, "TopLevel Item pressed.");
	else 
	if(m_tagEntries.contains(m_contextItem))
	{
		Gen2Tag gen2tag = m_tagEntries.value(item);
		if(gen2tag.epc.isNull())
			return;
		emit requestTagSettings(gen2tag.epc);
	}
}

QString CTagViewManager::getCurrentReader()
{
	QTreeWidgetItem* currentItem = m_treeWidget->currentItem();

	if(currentItem == 0)
		return "";
	
	// if the saved item is a reader item
	if(currentItem->parent() == 0)
	{
		// get the reader id
		return m_readerEntries.key(currentItem);
	}

	// if the saved item is a tag item
	else if(m_tagEntries.contains(currentItem))
	{
		return m_readerEntries.key(currentItem->parent());
	}
	return "";
}

void CTagViewManager::currentItemChanged ( QTreeWidgetItem * currentItem, QTreeWidgetItem * previous )
{
	if(currentItem && currentItem->parent() != 0)
	{
		return;
	}
	emit currentReaderChanged(getCurrentReader());
}
