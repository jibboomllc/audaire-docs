/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
#ifndef CREADERTOOL_H
#define CREADERTOOL_H

#include <QtGui/QMainWindow>
#include <QMap>

#include "AMSMainWindow.hxx"
#include "ui_CReaderTool.h"

#include "CDataHandler.h"
#include "CReaderManager.h"
#include <QrfeTrace.h>
#include "SL900AMainDialog.hxx"

class ReaderInterface;
class CActionHandler;
class QrfeAboutDialog;
class CTagListView;
class QrfeKeyWindow;
class CSettingsDialog;
class CTagSettingsDialog;
class CReaderConfigDialog;
class CGen2TagDialog;
class CReadRateCalc;
class CTagViewManager;
class CTagManager;
class RegisterMap;
class AMSCommunication;
class AMSAutoUpdateThread;


class CReaderTool : public AMSMainWindow
	, QrfeTraceModule
{
    Q_OBJECT

public:
    CReaderTool();
    ~CReaderTool();

public slots:
	void gotReader(ReaderInterface* ph);
	void lostReader(ReaderInterface* ph);
    void handleStreamV1Reader();
    void readerChangedState(ReaderInterface::HandlerState state, QString stateName, QString stateDescription);
	void readerChangedAction(QString action);

	void selectReader(int readerIndex);

	void startScan(bool start);
	void stopScan();
	void incrementScanProgress();
    void handleSingleInventoryRound(QString);
    void scanModeChanged(int);
	void showScanTime();
	void showRegisterMap();
	void clearOfflineReader();

	void multiplexISR();

	void showSettings();
	void addSerialReader();

	void requestTagSettingsDialog(QString tagId);
	void requestTagAdvancedSettingsDialog(QString readerId, Gen2Tag tagId);
    void requestSL900ADialog(QString readerId, QString tagId);
	void requestReaderAdvancedSettingsDialog(QString readerId);
	void requestReaderRegisterMap(QString readerId);

	void showAliasNames(bool show);
    void showRssi(bool show);
	void useTimeToLive(bool use);
	void handleActionsToggled(bool checked);

	void easterKeyUnlocked();

	void currentReaderChanged(QString);

    void showGlobalActions();
	
    void upgradeFirmware();
    void performFirmwareUpgrade(ReaderInterface *readerIf, bool lookForBootloader = false);

    void regMapStoreToFile ( const QString fileName );
    void regMapLoadFromFile ( const QString fileName );
    
signals:
	void currentReaderChanged(ReaderInterface* ph);

public:
	ReaderInterface*	getInterface(QString readerId);
	virtual void show(void);

private:
    enum ScanEnd {
        Seconds = 0,
        Rounds
    };

    Ui::Reader_ToolClass ui;

    CActionHandler*				m_actionHandler;
    CDataHandler				m_dataHandler;

    CTagListView*				m_tagListDialog;
    QrfeKeyWindow*              m_keyDialog;
    CSettingsDialog* 			m_settingsDialog;
    CTagSettingsDialog* 		m_tagSettingsDialog;
    QMap<ReaderInterface*, CReaderConfigDialog*>     m_readerConfigDialogs;
	CGen2TagDialog*				m_gen2SettingsDialog;

    CReaderManager 				m_readerManager;
    CReadRateCalc*				m_readRateCalc;
    CTagViewManager* 			m_tagViewManager;
    CTagManager*				m_tagManager;

	RegisterMap*				m_regMapWindow;
	AMSCommunication*			m_amsComWrapper;

    QMap<QString, ReaderInterface*>			m_reader;
	QMap<ReaderInterface*, CGen2TagDialog*>	m_gen2SettingsDialogs;
    QMap<QString, quint32>      m_singleInventoryCounter;
    ScanEnd                     m_scanEndMode;

    QStringList					m_activeReader;

    bool	m_scanActive;
    quint32 m_scanTimeout;
    quint32 m_maxInventory;
    QTimer* m_scanTimer;
    QTimer* m_scanProgressTimer;
    QDateTime m_scanStart;

    QTimer*	m_multiplexTimer;
    int		m_multiplexCurrentReader;
    bool 	m_multiplexReaderOn;
	bool	m_regFileAvailable;

	void closeEvent(QCloseEvent *);
	void ActivateSettings(void);
    void setupMenus();
    QString getScanTime();
};

#endif // CREADERTOOL_H
