/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*!
 * @file	CReaderManager.cpp
 * @brief	Class that controls the reader and detects new plugged in reader.
 */

#include "CReaderManager.h"
#include "globals.h"
#include "reader/ams/AmsReader.h"
#include "UartComDriver.h"
#include "HidComDriver.h"

#include <QHostAddress>

/*!
 * @brief	Constructor of the reader manager
 * The constructor of the reader manager registers for the specified VIDs and PIDs and connects its slots to the device detector.
 */
CReaderManager::CReaderManager(QObject* parent)
	: QObject(parent)
	, QrfeTraceModule("CReaderManager")
{
    QTimer::singleShot(0, this, SLOT(setupDeviceDetector()));
}

/*!
 * @brief 	Destructor of the reader manager
 * Deletes all reader objects that the reader manager manages.
 */
CReaderManager::~CReaderManager()
{
	foreach(ReaderInterface* dev, m_reader.values())
	{
		dev->deleteLater();
	}
}


void CReaderManager::setupDeviceDetector()
{
    m_deviceDetector.registerForHIDDevice(AMS_VID, AMS_PID);
    QObject::connect(&m_deviceDetector, SIGNAL(hidDeviceAttached ( QString, quint16, quint16 )), this, SLOT(readerAttached ( QString, quint16, quint16 )));
    QObject::connect(&m_deviceDetector, SIGNAL(hidDeviceRemoved ( QString, quint16, quint16 )), this, SLOT(readerRemoved ( QString, quint16, quint16 )));

    searchForReader();
}


/*!
 * @brief 	Member function that searches for plugged in reader
 * This function searches for plugged in devices. If a device is found the function readerAttatched is called.
 * This function should only be called on start up. All devices that are plugged in while the application runs are detected by the
 * device detector.
 */
void CReaderManager::searchForReader()
{
	trc(0x06, "-> searchForReader");
	QStringList list;
	list << m_deviceDetector.getConnectedHIDDevicePaths(AMS_VID, AMS_PID);
	foreach(QString device, list)
	{
		readerAttached(device, AMS_VID, AMS_PID);
	}
}
/*!
 * @brief 	function that is called if a matching device is attached.
 * This slot is called if a matching device is attached. It tries to create a reader object and broadcast this to the main application.
 * @param devicePath : e.g. COM1
 */
void CReaderManager::serialReaderAttached ( unsigned int port, QString &msg )
{
	trc(0x06, "-> serialReaderAttatched");
    QString devicePath;
	PortSettings settings;

	/* If the device is already in use, skip */
	if(m_reader.contains(devicePath))
		return;

	trc(0x06, "A reader with the devicePath " + devicePath + " was attached.");
    /* give reader some time to power up */
    SleepThread::msleep(500);
    /* Create new uart device instance with the given path */
    devicePath = QString("COM%1").arg(port);
    UartComDriver * driver = new UartComDriver(port);
    settings = driver->portSettings();
    settings.BaudRate = BAUD115200;
    settings.DataBits = DATA_8;
    settings.FlowControl = FLOW_OFF;
    settings.Parity = PAR_NONE;
    settings.StopBits = STOP_1;
    driver->setPortSettings(settings);

    ReaderInterface* reader = 0;

    driver->open();
    if(!driver->isOpened()){
		msg = QString("Failed to open COM port %1").arg(devicePath);
		delete driver;
		return;
	}
    reader = new AmsReader(driver, ReaderInterface::UART, this);
    /* If no reader was created or the reader can not be initialized, delete it and skip */
    if(reader == 0 || reader->initDevice() != ReaderInterface::OK)
    {
        delete reader;
        msg = QString("Could not initialize reader on COM port %1").arg(devicePath);
        return;
    }

	/* Connect to the signal of the reader, if it lost connection */
	QObject::connect(reader, SIGNAL(lostConnection(ReaderInterface*)), this, SLOT(protocolHandlerLostConnection()));

	/* Store reader */
    m_reader.insert(devicePath, reader);

	/* Broadcast new reader */
	emit gotReader(reader);
}

/*!
 * @brief 	Slot that is called if a matching device is attached.
 * This slot is called if a matching device is attached. It tries to create a reader object and broadcast this to the main application.
 * @param	devicePath		The windows path to the hid device
 * @param	vendorID		The vendor id of the device
 * @param	productID		The product id of the device
 */
void CReaderManager::readerAttached ( QString devicePath, quint16 vendorID, quint16 productID )
{
	trc(0x06, "-> readerAttatched");

    ReaderInterface::Result result;
	/* Check the vendor and product id */
	if( (vendorID != AMS_VID || productID != AMS_PID) )
		return;

	/* If the device is already in use, skip */
	if(m_reader.contains(devicePath))
		return;

	trc(0x06, "A reader with the devicePath " + devicePath + " was attached.");
    /* give reader some time to power up */
    SleepThread::msleep(500);
	/* Create new hid device instance with the given path */
    HidComDriver * driver = new HidComDriver(productID, vendorID);
    driver->setTimeouts(4000, 4000);
    ReaderInterface* reader = 0;
    if(!(vendorID == AMS_VID && productID == AMS_PID))
        return;
    
    reader = new AmsReader(driver, ReaderInterface::HID, this);
    result = reader->initDevice();
    if(result != ReaderInterface::OK)
    {
        delete reader;
        if (result == ReaderInterface::NA)
            emit foundStreamV1Reader();
        return;
    }

	m_reader.insert(devicePath, reader);

	/* Connect to the signal of the reader, if it lost connection */
	QObject::connect(reader, SIGNAL(lostConnection(ReaderInterface*)), this, SLOT(protocolHandlerLostConnection(ReaderInterface*)));

	/* Broadcast new reader */
	emit gotReader(reader);
}

/*!
 * @brief 	Slot that is called if a reader was plugged off
 * This slot is called if a device with a matching vendor id and product id is plugged off.
 * @param	devicePath		The windows path to the hid device
 * @param	vendorID		The vendor id of the device
 * @param	productID		The product id of the device
 */
void CReaderManager::readerRemoved ( QString devicePath, quint16 vendorID, quint16 productID )
{
	trc(0x06, "-> readerRemoved");

	/* Check the vendor and product id */
	if( (vendorID != AMS_VID || productID != AMS_PID) )
		return;

	/* Check if the device was handled by this reader manager */
	if(!m_reader.keys().contains(devicePath))
		return;

	trc(0x06, "The reader with the devicePath " + devicePath + " was removed.");

	/* Remove the reader from the used objects and notify the rest of the application */
	ReaderInterface* reader = m_reader.value(devicePath);

	if (reader != NULL)
	{
		reader->deviceWasRemoved();
		emit lostReader(reader);
	}

	m_reader.remove(devicePath);

	/* Store the device in the map that is deleted if cleanup is called */
	m_toDelete.append(reader);
}

/*!
 * @brief	Slot is called from a reader if it losts connection to the reader.
 * This slot is called from a reader if it losts connection to the reader.
 */
void CReaderManager::protocolHandlerLostConnection(ReaderInterface * reader)
{
	trc(0x06, "-> protocolHandlerLostConnection");

    if(!m_reader.values().contains(reader))
        return;

    /* Notify the reader that the device is now plugged off */
	reader->deviceWasRemoved();

	QString devicePath = m_reader.key(reader);

	/* Notify the rest of the application that the reader is removed */
	emit lostReader(reader);

	m_reader.remove(devicePath);

	/* Store the device in the map that is deleted if cleanup is called */
	m_toDelete.append(reader);
}

/*!
 * @brief	Functions that cleans up the plugged off device objects
 */
void CReaderManager::cleanUp()
{
    foreach(ReaderInterface* dev, m_reader.values()) {
        if (dev->currentState() == ReaderInterface::STATE_ERROR || dev->currentState() == ReaderInterface::STATE_OFFLINE)
            protocolHandlerLostConnection(dev);
    }
	/* Delete the devices that are to remove */
	foreach(ReaderInterface* dev, m_toDelete){
		dev->deleteLater();
	}
	m_toDelete.clear();
}
