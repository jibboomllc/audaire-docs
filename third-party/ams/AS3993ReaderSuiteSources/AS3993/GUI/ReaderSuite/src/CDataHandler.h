/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#ifndef CDATAHANDLER_H_
#define CDATAHANDLER_H_

#include <QrfeTrace.h>
#include <QMap>
#include <QString>

enum ActionType{ShowPicture = 0, StartApp = 1};

struct TagActionInfo{
	QString aliasName;
	bool performAction;
    bool performOnce;
	int time;
	ActionType type;
	QString picPath;
	QString appPath;
	QString appParams;
};

class CDataHandler : public QObject
	, QrfeTraceModule
{
	Q_OBJECT

public:
	CDataHandler();
	virtual ~CDataHandler();

	bool isConfigSet(QString epc);

	bool getConfig(QString epc, TagActionInfo &t);
	bool saveConfig(QString epc, TagActionInfo t);

	quint64 timeValToMsecs(quint32 timeVal);

private:
    QString itsDataFile;
	QMap<QString, TagActionInfo> itsDataStore;

};

#endif /* CDATAHANDLER_H_ */
