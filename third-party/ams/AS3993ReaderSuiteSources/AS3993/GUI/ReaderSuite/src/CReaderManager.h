/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */
/*
 * CAmsReaderManager.h
 *
 *  Created on: 27.01.2009
 *      Author: stefan.detter
 */

#ifndef CAMSREADERMANAGER_H_
#define CAMSREADERMANAGER_H_

#include <QObject>
#include <QMap>

#include "reader/ReaderInterface.h"
#include <AMSDeviceDetector.hxx>
#include <QrfeTrace.h>

class AS3993Communication;

class CReaderManager : public QObject
	, QrfeTraceModule
{
	Q_OBJECT
public:
	CReaderManager(QObject* parent = 0);
	virtual ~CReaderManager();

	typedef enum{
		OK,
		ERROR_PORT,
		ERROR_PROTOCOLL,
		ERROR_DOUBLE_ID
	} Result;

public:
	void searchForReader();
	void serialReaderAttached ( unsigned int port, QString &msg );

	void cleanUp();

private slots:
    void setupDeviceDetector();

	void readerAttached ( QString devicePath, quint16 vendorID, quint16 productID );
	void readerRemoved ( QString devicePath, quint16 vendorID, quint16 productID );

	void protocolHandlerLostConnection(ReaderInterface *reader);

signals:
	void gotReader(ReaderInterface* ph);
	void lostReader(ReaderInterface* ph);
    void foundStreamV1Reader();

private:
	AMSDeviceDetector m_deviceDetector;

	QMap<QString, ReaderInterface*>     m_reader;
	QList<ReaderInterface*>	m_toDelete;
};

#endif /* CAMSREADERMANAGER_H_ */
