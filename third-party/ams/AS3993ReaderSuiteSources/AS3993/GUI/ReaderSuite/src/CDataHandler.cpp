/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include "CDataHandler.h"

#include <QStringList>
#include <QDesktopServices>
#include <QFile>
#include <QDataStream>

CDataHandler::CDataHandler()
	: QrfeTraceModule("CDataHandler")
{
	itsDataFile = QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/AS3993_Reader_Suite_Actions.txt";

    //read out old settings
    QFile file(itsDataFile);
    if (file.open(QIODevice::ReadWrite))
    {
        QDataStream in(&file);
        in >> itsDataStore;
    }
    else
        trc(0x01, "Could not read action file.");
    file.close();
}

CDataHandler::~CDataHandler() 
{
    //save settings to file
    QFile file(itsDataFile);
    if (file.open(QIODevice::ReadWrite | QIODevice::Truncate))
    {
        QDataStream out(&file);
        out << itsDataStore;
    }
    else
        trc(0x01, "Could not save action file.");
    file.close();
}

bool CDataHandler::isConfigSet(QString epc)
{
	if(itsDataStore.keys().contains(epc))
		return true;

    return false;
}

bool CDataHandler::getConfig(QString epc, TagActionInfo &t)
{
	if(itsDataStore.keys().contains(epc)){
		t = itsDataStore.value(epc);
		return true;
	}

	return false;
}

bool CDataHandler::saveConfig(QString epc, TagActionInfo t)
{
	itsDataStore.insert(epc, t);
	return true;
}

quint64 CDataHandler::timeValToMsecs(quint32 timeVal)
{
	switch(timeVal){
	case  0: return 1000;
	case  1: return 2000;
	case  2: return 3000;
	case  3: return 5000;
	case  4: return 10000;
	case  5: return 20000;
	case  6: return 30000;
	case  7: return 60000;
	case  8: return 120000;
	case  9: return 180000;
	case 10: return 300000;
	case 11: return 600000;
	case 12: return 1200000;
	case 13: return 1800000;
	default: return 1000;
	}
}

QDataStream & operator<< (QDataStream & out, const TagActionInfo & action)
{
    out << action.aliasName;
    out << action.performAction;
    out << action.performOnce;
    out << action.time;
    out << (static_cast<int>(action.type));
    out << action.picPath;
    out << action.appPath;
    out << action.appParams;
    return out;
}

QDataStream & operator>> (QDataStream & in, TagActionInfo & action)
{
    int type;
    in >> action.aliasName;
    in >> action.performAction;
    in >> action.performOnce;
    in >> action.time;
    in >> type;
    in >> action.picPath;
    in >> action.appPath;
    in >> action.appParams;

    action.type = static_cast<ActionType>(type);
    return in;
}