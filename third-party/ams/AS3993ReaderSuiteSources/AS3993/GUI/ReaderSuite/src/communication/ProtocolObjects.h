/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file ProtocolObjects.h
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Classes for manipulating protocol related parameters
 */

#ifndef PROTOCOL_OBJECTS_H
#define PROTOCOL_OBJECTS_H

#include "AmsComObject.h"
#include "ComGlobals.h"

#define AS3993_CMD_GEN2PARAMS       3
#define AS3993_CMD_GEN2SINGLEINV    5
#define AS3993_CMD_GEN2CYCLICINV    12

/** class to set gen2 parameters of reader: link frequency, coding, session, pilot, Tari, Qbegin.
 *  only the parameters whose corresponding set function has been called will be changed on the reader,
 *  but all current parameters are retrieved from the reader.
 */
class Gen2Parameters : public AmsComObject
{
public:
    /* default c'tor */
    Gen2Parameters ( ) : AmsComObject( AS3993_CMD_GEN2PARAMS, 0, 16, 16 )
        , itsLinkFreq( 0 )
        , itsSetLinkFreq( false )
        , itsCoding( 0 )
        , itsSetCoding( false )
        , itsSession( 0 )
        , itsSetSession( false )
        , itsLongPilot( 0 )
        , itsSetLongPilot( false )
        , itsTari( 0 )
        , itsSetTari( false )
        , itsQbegin( 0 )
        , itsSetQbegin( false )
        , itsSel( 0 )
        , itsSetSel( false )
        , itsSetTarget( false )
        , itsTarget( 0 )
    {        
    };

    /* d'tor */
    ~Gen2Parameters ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ){ return false; };

    void setLinkFreq(quint32 linkfreq);
    quint32 linkFreq();
    void setCoding(unsigned char coding);
    unsigned char coding();
    void setSession(unsigned char session);
    unsigned char session() { return itsSession; };
    void setLongPilot(bool pilot);
    bool longPilot() { return itsLongPilot; };
    void setTari( unsigned char tari );
    unsigned char tari() { return itsTari; };
    void setQbegin( unsigned char q );
    unsigned char qbegin() { return itsQbegin; };
    void setTarget( unsigned char target );
    unsigned char target() { return itsTarget; };

protected:
    ComByteArray itsData;
    unsigned char itsLinkFreq;
    bool itsSetLinkFreq;
    unsigned char itsCoding;
    bool itsSetCoding;
    unsigned char itsSession;
    bool itsSetSession;
    bool itsLongPilot;
    bool itsSetLongPilot;
    unsigned char itsTari;
    bool itsSetTari;
    unsigned char itsQbegin;
    bool itsSetQbegin;
    unsigned char itsSel;
    bool itsSetSel;
    unsigned char itsTarget;
    bool itsSetTarget;
    
};


/** class to start a gen2 single inventory on the reader.
 */
class Gen2SingleInventoryStart : public AmsComObject
{
public:

    /* default c'tor */
    Gen2SingleInventoryStart ( ) : AmsComObject( AS3993_CMD_GEN2SINGLEINV, 3, 0, 0 )
        , itsFastMode( false )
        , itsRssiMode( 0x06 )   // default: rssi at 2nd byte
        , itsAutoAckMode( false )
        , itsTIDMode(false)
    {        
    };

    /* d'tor */
    ~Gen2SingleInventoryStart ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * xml ); 

    virtual bool rxSerialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool deserialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool fill( QXmlStreamReader * ) { return false; };

    void setFastMode(bool fast);
    void setRssiMode(quint8 rssi);
    void setAutoAckMode(bool autoAck);
    void setTidMode(bool tidmode);

private:
    quint8 itsRssiMode;
    bool itsFastMode;
    bool itsAutoAckMode;
    bool itsTIDMode;
};

/** class to retrieve the result of a gen2 single inventory. Tags have to be retrieved as long as
 * receivedInventoryRoundData() reports false.
 */
class Gen2GetInventoryData : public AmsComObject
{
public:

    /* default c'tor */
    Gen2GetInventoryData ( ) : AmsComObject( 5, 0, 0, 0xFF )
        , itsCyclicInventory( false)
        , itsTagsToFetch( 0 )
        , itsNumTags( 0 )
        , itsDataReceiveFinished( false )
        , itsFrequency( 0 )
        , itsTIDMode( false )
    {
    };

    /* d'tor */
    ~Gen2GetInventoryData ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ) { return false; };

    unsigned int getFrequency() { return itsFrequency; };
    quint8 getStatus() { return itsStatus; };

    QList<RawTagInfo> tags();

    bool receivedInventoryRoundData() { return itsDataReceiveFinished; };
    bool cyclicInventoryRunning() { return itsCyclicInventory; };
    void setTidMode(bool withTID) {itsTIDMode = withTID; };

protected:
    ComByteArray itsData;
    bool itsCyclicInventory;/**< An cyclic(continuous) inventory round is currently running */
    int itsTagsToFetch;     /**< Number of tags which have been found in last inventory round but not yet fetched from device. */
    int itsNumTags;         /**< Number of tags which have been fetched from the device in the last transmit. */
    bool itsDataReceiveFinished;
    QList<RawTagInfo> itsTags;    /**< Data of tags which has been fetched from the device in the last transmit. */
    unsigned int itsFrequency; /**< Frequency as parsed from inventory response header */
    quint8 itsStatus;

private:
    bool itsTIDMode; 
};


/** class to start a gen2 cyclic/continous inventory on the reader.
 */
class Gen2CyclicInventoryStart : public AmsComObject
{
public:

    /* default c'tor */
    Gen2CyclicInventoryStart ( ) : AmsComObject( AS3993_CMD_GEN2CYCLICINV, 5, 0, 0 )
        , itsFastMode( false )
        , itsRssiMode( 0x06 )   // default: rssi at 2nd byte
        , itsAutoAckMode( false )
        , itsTIDMode( false)
    {        
    };

    /* d'tor */
    ~Gen2CyclicInventoryStart ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * xml ); 

    virtual bool rxSerialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool deserialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool fill( QXmlStreamReader * ) { return false; };

    void setFastMode(bool fast);
    void setRssiMode(quint8 rssi);
    void setAutoAckMode(quint8 autoAck);
    void setTidMode(bool tidmode);

private:
    quint8 itsRssiMode;
    bool itsFastMode;
    bool itsAutoAckMode;
    bool itsTIDMode;
};

/** class to stop a gen2 cyclic/continuous inventory on the reader.
 */
class Gen2CyclicInventoryStop : public AmsComObject
{
public:

    /* default c'tor */
    Gen2CyclicInventoryStop ( ) : AmsComObject( AS3993_CMD_GEN2CYCLICINV, 5, 0, 0 )
    {        
    };

    /* d'tor */
    ~Gen2CyclicInventoryStop ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * xml ); 

    virtual bool rxSerialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool deserialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool fill( QXmlStreamReader * ) { return false; };

};


#endif /* PROTOCOL_OBJECTS_H */
