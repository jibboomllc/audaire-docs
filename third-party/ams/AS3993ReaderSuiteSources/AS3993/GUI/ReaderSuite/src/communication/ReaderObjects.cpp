/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file ReaderObjects.cpp
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Implementation of classes get reader information
 */

#include "ReaderObjects.h"
#include "qDebug"
bool ReaderConfig::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = itsSetPowerDownMode ? 1 : 0;
    buffer[ 1 ] = itsPowerDownMode;
    return true;
}

bool ReaderConfig::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    itsData.clear();
    if ( bufferSize < itsDeserialSize )
        return false;

    itsData = ComByteArray( &buffer[0], bufferSize );

    if (itsData.at(0) == 0xFF)
    {
        itsWrongChipId = true;
        itsChipId = -1;
    }
    else
    {
        itsWrongChipId = false;
        itsChipId = 3900 + itsData.at(0);
    }

    itsInitError = itsData.at(1);
    itsVco = itsData.at(2);
    itsPa = itsData.at(3);
    itsInput = itsData.at(4);
    itsAntennaSwitch = itsData.at(5);
    itsTunerConfig = itsData.at(6);
    itsPowerDownMode = itsData.at(7);
    itsHwId = itsData.at(8);
    if (itsData.size() >= 10)
        itsTunerConfig |= (itsData.at(9)<<8);

    return true;
}

void ReaderConfig::setPowerDownMode( quint8 mode )
{
    itsSetPowerDownMode = true;
    itsPowerDownMode = mode;
}

void ReaderConfig::getData( bool &wrongChipId, int &chipId, quint8 &initError, quint8 &vco , quint8 &pa, quint8 &input, bool &antennaSwitch, quint16 &tunerConfig, quint8 &hwId )
{
    wrongChipId = itsWrongChipId;
    chipId = itsChipId;
    initError = itsInitError;
    vco = itsVco;
    pa = itsPa;
    input = itsInput;
    antennaSwitch = itsAntennaSwitch;
    tunerConfig = itsTunerConfig;
    hwId = itsHwId;
}

bool PAConfig::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
        return false;
    buffer[ 0 ] = itsreadWriteConfig &0x01;
    buffer[ 1 ] = (itsG16 & 0x01) << 1;
    buffer[ 1 ] |= (itsG8 & 0x01);
    return true;
}

bool PAConfig::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xmlW)
{
    return(serialise(buffer,bufferSize,xmlW));
}

bool PAConfig::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xmlW)
{
    if ( bufferSize < itsDeserialSize )
        return false;
    itsG16 = (buffer[0] & 0x02) >> 1;
    itsG8 = (buffer[0]& 0x01);
    //qDebug()<< "G8:"<< itsG8 <<"G16:"<< itsG16 ;
    return true;
}

void PAConfig::setData(quint8 g8, quint8 g16)
{
    itsG8 = g8;
    itsG16 = g16;
}
