/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file ComGlobals.h
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief global definitions for communication
 */

#ifndef COM_GLOBALS_H
#define COM_GLOBALS_H

#include <QList>
#include <QByteArray>
#include <QString>

#define READER_NO_ERROR 0
#define READER_NO_MEM  -1

struct RawTagInfo 
{
    QList<quint8> epc;
    QList<quint8> pc;
    QList<quint8> tid;
    quint8 agc;
    quint8 rssi;
    int freq;
};

struct RawInvInfo 
{
    QList<RawTagInfo> tags;
    quint8 result;
    quint32 freq;
};


/** \brief Array wrapper used in various communication related classes.
 * This class is a drop-in replacement for QByteArray. QByteArray uses (signed) char as internal storage,
 * this might lead to signed/unsigned problems, eg: when doing comparisons.
 */
class ComByteArray : public QList<quint8>
{
public:
    ComByteArray() : QList<quint8>() {};
    ComByteArray( QList<quint8> &copy ) : QList<quint8>(copy){};
    ComByteArray( unsigned char * buffer, int bufferSize ) : QList<quint8>()
    {
        reserve(bufferSize);
        if (bufferSize > 0)
        {
            for (int i=0; i<bufferSize; i++)
                append(buffer[i]);
        }
    };
    ComByteArray( int size, unsigned char val ) : QList<quint8>()
    {
        reserve(size);
        for (int i=0; i<size; i++)
        {
            append(val);
        }
    };
    ComByteArray( const QByteArray &ba )
    {
        reserve(ba.size());
        for (int i=0; i<ba.size(); i++)
        {
            append(ba.at(i));
        }
    };
public:
    ComByteArray mid( int pos, int length = -1 )
    {
        return QList::mid(pos, length);
    };
    QString toHex()
    {
        return toQByteArray().toHex();
    };
    QByteArray toQByteArray()
    {
        QByteArray ba;
        ba.reserve(this->size());
        for( int i=0; i<this->size(); i++ )
        {
            ba.append(this->at(i));
        }
        return ba;
    };
};

inline void writeU32IntoLittleEndianBuffer(quint32 value, quint8 * buffer)
{
    buffer[0] = (quint8)(value & 0xFF);
    buffer[1] = (quint8)((value >> 8) & 0xFF);
    buffer[2] = (quint8)((value >> 16) & 0xFF);
    buffer[3] = (quint8)((value >> 24) & 0xFF);
}

#endif /* COM_GLOBALS_H */
