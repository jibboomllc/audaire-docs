/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file FrequencyObjects.cpp
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Implementation of classes to manipulate frequency hopping related parameters
 */

#include "FrequencyObjects.h"

#define SUB_CMD_RSSI                0x01
#define SUB_CMD_REFLECTED_POWER     0x02
#define SUB_CMD_ADD_FREQ            0x04
#define SUB_CMD_GET_FREQ            0x05
#define SUB_CMD_SET_FREQ_HOP        0x08
#define SUB_CMD_GET_FREQ_HOP        0x09
#define SUB_CMD_MODULATION          0x10

bool FrequencyBase::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
    {
        return false;
    }

    return freqRxSerialise( &buffer[0], bufferSize );
}

bool FrequencyBase::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    itsData.clear();
    if ( bufferSize < itsDeserialSize )
        return false;

    itsData = ComByteArray( &buffer[0], bufferSize );
    return freqDeserialise( itsData );
}




/************************************************************************/

bool RssiCom::freqRxSerialise( unsigned char * buffer, int )
{
    buffer[0] = SUB_CMD_RSSI;
    buffer[1] =  itsFreq & 0x000000FF;
    buffer[2] = (itsFreq & 0x0000FF00) >> 8;
    buffer[3] = (itsFreq & 0x00FF0000) >> 16;
    return true;
}

bool RssiCom::freqDeserialise( ComByteArray data )
{
    if ( data.size() < 3 )
        return false;
    itsI = data.at(0);
    itsQ = data.at(1);
    itsDBM = data.at(2);
    return true;
}

void RssiCom::rssiData( int &ichannel, int &qchannel, int &dBm )
{
    ichannel = itsI;
    qchannel = itsQ;
    dBm = itsDBM;
}



/************************************************************************/

void ReflectedPowerCom::setFreq( ulong freq, uchar tune )
{
    itsFreq = freq;
    itsTune = tune;
}

bool ReflectedPowerCom::freqRxSerialise( unsigned char * buffer, int )
{
    buffer[0] = SUB_CMD_REFLECTED_POWER;
    buffer[1] =  itsFreq & 0x000000FF;
    buffer[2] = (itsFreq & 0x0000FF00) >> 8;
    buffer[3] = (itsFreq & 0x00FF0000) >> 16;
    buffer[4] = itsTune;
    return true;
}

bool ReflectedPowerCom::freqDeserialise( ComByteArray data )
{
    if ( data.size() < 2 )
        return false;
    itsI = (qint8) data.at(0);  //signed values!
    itsQ = (qint8) data.at(1);
    return true;
}

void ReflectedPowerCom::reflectedPower( int &ichannel, int &qchannel )
{
    ichannel = itsI;
    qchannel = itsQ;
}


/************************************************************************/

void AddFrequencyCom::setFreqParameters(bool clearFreqList, ulong freq, uchar profile)
{
    itsClearFreqList = clearFreqList;
    itsFreq = freq;
    itsProfile = profile;
}

bool AddFrequencyCom::freqRxSerialise( unsigned char * buffer, int )
{
    if ( itsFreq == 0 )
        return false;

    buffer[0] = SUB_CMD_ADD_FREQ;
    buffer[1] =  itsFreq & 0x000000FF;
    buffer[2] = (itsFreq & 0x0000FF00) >> 8;
    buffer[3] = (itsFreq & 0x00FF0000) >> 16;
    buffer[4] = itsClearFreqList ? 0x01 : 0x00;
    buffer[5] = itsProfile;
    return true;
}

bool AddFrequencyCom::freqDeserialise( ComByteArray data )
{
    if ( itsStatus == READER_NO_ERROR )
        itsSuccess = true;
    else
        itsSuccess = false;
    return true;
}


/************************************************************************/

bool GetFrequencyCom::freqRxSerialise( unsigned char * buffer, int )
{
    buffer[0] = SUB_CMD_GET_FREQ;
    return true;
}

bool GetFrequencyCom::freqDeserialise( ComByteArray data )
{
    if ( data.size() < 9 )
        return false;

    itsProfile = data.at(0);
    itsMinFreq = (data.at(1)) | (data.at(2) << 8) | (data.at(3) << 16);
    itsMaxFreq = (data.at(4)) | (data.at(5) << 8) | (data.at(6) << 16);
    itsNumFreq = data.at(7);
    itsNumFreqGui = data.at(8);

    return true;
}

void GetFrequencyCom::freqData( ulong &minFreq, ulong &maxFreq, uchar &numFreq, uchar &numFreqGui )
{
    minFreq = itsMinFreq;
    maxFreq = itsMaxFreq;
    numFreq = itsNumFreq;
    numFreqGui = itsNumFreqGui;
}



/************************************************************************/

void SetFreqHoppingCom::setFreqParameters( ushort listeningTime, ushort maxSendTime, ushort idleTime, signed char rssi )
{
    itsListeningTime = listeningTime;
    itsMaxSendTime = maxSendTime;
    itsIdleTime = idleTime;
    itsRssi = rssi;
}

bool SetFreqHoppingCom::freqRxSerialise( unsigned char * buffer, int )
{
    buffer[0] = SUB_CMD_SET_FREQ_HOP;
    buffer[1] =  itsListeningTime & 0x00FF;
    buffer[2] = (itsListeningTime & 0xFF00) >> 8;
    buffer[3] =  itsMaxSendTime & 0x00FF;
    buffer[4] = (itsMaxSendTime & 0xFF00) >> 8;
    buffer[5] =  itsIdleTime & 0x00FF;
    buffer[6] = (itsIdleTime & 0xFF00) >> 8;
    buffer[7] = itsRssi;
    return true;
}

bool SetFreqHoppingCom::freqDeserialise( ComByteArray data )
{
    if ( itsStatus == READER_NO_ERROR )
        itsSuccess = true;
    else
        itsSuccess = false;
    return true;
}


/************************************************************************/

bool GetFreqHoppingCom::freqRxSerialise( unsigned char * buffer, int )
{
    buffer[0] = SUB_CMD_GET_FREQ_HOP;
    return true;
}

bool GetFreqHoppingCom::freqDeserialise( ComByteArray data )
{
    if ( data.size() < 7 )
        return false;

    itsListeningTime = data.at(0) | data.at(1) << 8;
    itsMaxSendTime = data.at(2) | data.at(3) << 8;
    itsIdleTime = data.at(4) | data.at(5) << 8;
    itsRssi = data.at(6);

    return true;
}

void GetFreqHoppingCom::freqParameters( ushort &listeningTime, ushort &maxSendTime, ushort &idleTime, signed char &rssi )
{
     listeningTime = itsListeningTime;
     maxSendTime = itsMaxSendTime;
     idleTime = itsIdleTime;
     rssi = itsRssi;
}



/************************************************************************/

bool ContinuousModulationCom::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if (bufferSize < itsSerialSize)
        return false;

    buffer[0] = SUB_CMD_MODULATION;
    buffer[1] =  itsFreq & 0x000000FF;
    buffer[2] = (itsFreq & 0x0000FF00) >> 8;
    buffer[3] = (itsFreq & 0x00FF0000) >> 16;
    buffer[4] =  itsDuration & 0x00FF;
    buffer[5] = (itsDuration & 0xFF00) >> 8;
    buffer[6] = itsRandom ? 0x01 : 0x00;
    for(int i=7; i<17; i++)
    {
        buffer[i] = (quint8) (qrand() & 0xFF);
    }
    return true;
}

