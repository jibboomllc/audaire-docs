/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file ReaderObjects.h
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Classes getting status information from the reader.
 */

#ifndef READER_OBJECTS_H
#define READER_OBJECTS_H

#include "AmsComObject.h"
#include "ComGlobals.h"

#define AS3993_CMD_READERCONFIG 0
#define AS3993_CMD_CONFIG_PA    22

/** class to get/set reader configuration
 */
class ReaderConfig : public AmsComObject
{
public:
    /* default c'tor */
    ReaderConfig ( ) : AmsComObject( AS3993_CMD_READERCONFIG, 0, 2, 10 )
        , itsSetPowerDownMode( false )
        , itsPowerDownMode( 0 )
        , itsWrongChipId( false )
        , itsChipId( 0 )
        , itsInitError( 0 )
        , itsVco( 0 )
        , itsPa( 0 )
        , itsInput( 0 )
        , itsAntennaSwitch( false )
        , itsTunerConfig( 0 )
        , itsHwId( 0 )
    {        
    };

    /* d'tor */
    ~ReaderConfig ( ) { };

    virtual bool serialise( unsigned char *, int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ){ return false; };

    void setPowerDownMode(quint8 mode);
    quint8 powerDownMode(){ return itsPowerDownMode; };
    void getData(bool &wrongChipId, int &chipId, quint8 &initError, quint8 &vco , quint8 &pa, quint8 &input, bool &antennaSwitch, quint16 &tunerConfig, quint8 &hwId);

protected:
    ComByteArray itsData;
    //config data which is read/write able
    bool itsSetPowerDownMode;
    quint8 itsPowerDownMode;

    // config which can only be read from reader (no change possible)
    bool itsWrongChipId;
    int itsChipId;
    quint8 itsInitError;
    quint8 itsVco;
    quint8 itsPa;
    quint8 itsInput;
    bool itsAntennaSwitch;
    quint16 itsTunerConfig;
    quint8 itsHwId;
};

class PAConfig : public AmsComObject
{
public:
    /* default c'tor */
    PAConfig ( quint8 readWriteConfig) : AmsComObject( AS3993_CMD_CONFIG_PA, 2, 2, 1 )
    , itsG8( 0 )
    , itsG16( 0 )
    { itsreadWriteConfig = readWriteConfig;};

    /* d'tor */
    ~PAConfig ( ) { };

    bool serialise( unsigned char *, int, QXmlStreamWriter * );

    bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    bool fill( QXmlStreamReader * ){ return false; };

    void setData(quint8 g8, quint8 g16);
    quint8 getG8() { return itsG8;};
    quint8 getG16() { return itsG16;};
private:
       
    quint8 itsG8;
    quint8 itsG16;
    quint8 itsreadWriteConfig;

};

#endif /* READER_OBJECTS_H */
