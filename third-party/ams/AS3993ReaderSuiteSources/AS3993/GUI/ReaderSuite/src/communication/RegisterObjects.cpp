/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AntennaObjects.cpp
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Implementation of classes to manipulate antenna related parameters
 */

#include "RegisterObjects.h"

#define AS3993_SUB_CMD_READALLREGS  0
#define AS3993_SUB_CMD_READREG      1


bool ReadAllRegistersObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = AS3993_SUB_CMD_READALLREGS;
    buffer[ 1 ] = 0;
    return true;
}

bool ReadAllRegistersObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    int idx = 0;
    itsData.clear();
    if ( bufferSize < itsDeserialSize )
        return false;

    /* register data is transmitted without gaps like in register layout of AS3993. But in our SW we want to
       access register values by indexing the address therefore we copy the data into an array which is
       layouted like the registers on the AS3993, therefore index of the array = address of register */
    for ( int i = 0; i < 0x3f; i++)
    {
        if (   (i == 0x0F)
            || (i > 0x1D && i < 0x22)
            || (i > 0x22 && i < 0x29)
            || (i > 0x2E && i < 0x33)
            || (i == 0x34))
        {
        }
        else
        {
            itsData[i] = (buffer[idx]);  //1+idx: skip command byte
            idx++;
        }
    }

    return true;
}



/************************************************************************/
bool ReadRegisterObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = AS3993_SUB_CMD_READREG;
    buffer[ 1 ] = itsAddress;
    return true;
}

bool ReadRegisterObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < 1 )
        return false;

    itsData = buffer[0];
    return true;
}

void ReadRegisterObject::setAddress( unsigned char address )
{
    itsAddress = address;
}



/************************************************************************/
bool WriteRegisterObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = itsAddress;
    buffer[ 1 ] = itsData;

    return true;
}

bool WriteRegisterObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < 1 || buffer[0] != 0 )
        return false;

    return true;
}

bool WriteRegisterObject::setData( unsigned char address, unsigned char data )
{
    itsAddress = address;
    itsData = data;

    return true;
}


