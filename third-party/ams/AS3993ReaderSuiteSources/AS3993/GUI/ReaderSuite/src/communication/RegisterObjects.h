/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file RegisterObjects.h
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Classes for manipulating registers on AS3993
 */

#ifndef REGISTER_OBJECTS_H
#define REGISTER_OBJECTS_H

#include "AmsComObject.h"
#include "ComGlobals.h"
#include <QMap>

class ReadAllRegistersObject : public AmsComObject
{
public:
    /* default c'tor */
    ReadAllRegistersObject ( )
        : AmsComObject( AMS_COM_READ_REG, 0, 2, 0x30 )
    {        
    };

    /* d'tor */
    ~ReadAllRegistersObject ( ) { };

    virtual bool serialise( unsigned char *, int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ){ return false; };
    
    QMap<int, quint8> get( ) { return itsData; };

protected:
    QMap<int, quint8> itsData;
};

class ReadRegisterObject : public AmsComObject
{
public:
    /* default c'tor */
    ReadRegisterObject ( )
        : AmsComObject( AMS_COM_READ_REG, 0, 2, 1 )
        , itsAddress( 0 )
        , itsData( 0 )
    {        
    };

    /* d'tor */
    ~ReadRegisterObject ( ) { };

    virtual bool serialise( unsigned char *, int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ){ return false; };

    quint8 get( ) { return itsData; };

    void setAddress( unsigned char address );

protected:
    quint8 itsData;
    unsigned char itsAddress;
};

class WriteRegisterObject : public AmsComObject
{
public:
    /* default c'tor */
    WriteRegisterObject ( )
        : AmsComObject( AMS_COM_WRITE_REG, 0, 2, 1 )
        , itsAddress( 0 )
        , itsData( 0 )
    {        
    };

    /* d'tor */
    ~WriteRegisterObject ( ) { };

    virtual bool serialise( unsigned char *, int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ){ return false; };

    bool setData( unsigned char address, unsigned char data );

protected:
    unsigned char itsAddress;
    unsigned char itsData;
};

#endif /* REGISTER_OBJECTS_H */
