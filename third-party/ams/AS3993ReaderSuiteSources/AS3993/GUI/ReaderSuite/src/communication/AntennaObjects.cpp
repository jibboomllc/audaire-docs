/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AntennaObjects.cpp
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Implementation of classes to manipulate antenna related parameters
 */

#include "AntennaObjects.h"
#include "string.h"

#define ANTENNA_ON  0xFF
#define ANTENNA_OFF 0x00
#define EVAL_ON     0x01
#define EVAL_OFF    0x00 

#define AS3993_TUNER_TABLE_SIZE  0
#define AS3993_TUNER_CLEAR_TABLE 1
#define AS3993_TUNER_ADD_TABLE   2

bool AntennaPowerObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsRxSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = itsOn;
    buffer[ 1 ] = itsEval;
    return true;
}

bool AntennaPowerObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    itsData.clear();
    if ( bufferSize < itsDeserialSize )
        return false;

    itsData = ComByteArray( &buffer[0], bufferSize );

    return true;
}

void AntennaPowerObject::setData( bool on, bool eval )
{
    itsOn = on ? ANTENNA_ON : ANTENNA_OFF;
    itsEval = eval ? EVAL_ON : EVAL_OFF;
}



/************************************************************************/

bool TxParameters::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsRxSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = itsSetSensLevel ? 0x01 : 0x00;
    buffer[ 1 ] = (unsigned char) itsSensLevel;
    buffer[ 2 ] = itsSetAntenna ? 0x01 : 0x00;
    buffer[ 3 ] = itsAntenna;
    return true;
}

bool TxParameters::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    itsData.clear();
    if ( bufferSize < itsDeserialSize )
        return false;

    itsData = ComByteArray( &buffer[0], bufferSize );
    itsSensLevel = (signed char) itsData.at(1);
    itsAntenna = itsData.at(3);
    return true;
}

void TxParameters::setAntenna( unsigned char antenna )
{
    itsSetAntenna = true;
    itsAntenna = antenna;
}

void TxParameters::setSensitivity( signed char sensLevel )
{
    itsSetSensLevel = true;
    itsSensLevel = sensLevel;
}

void TxParameters::getData( signed char &sensitivity, unsigned char &antenna )
{
    sensitivity = itsSensLevel;
    antenna = itsAntenna;
}


/************************************************************************/
bool AntennaTuner::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    int idx = 0;
    if ( bufferSize < itsRxSerialSize )
    {
        return false;
    }
    memset(buffer,0, bufferSize);

    if (itsID == 1)
        idx = 6;
    
    buffer[ 0 + idx ] = itsSetCin ? 0x01 : 0x00;
    buffer[ 1 + idx ] = itsCin;
    buffer[ 2 + idx ] = itsSetClen ? 0x01 : 0x00;
    buffer[ 3 + idx ] = itsClen;
    buffer[ 4 + idx ] = itsSetCout ? 0x01 : 0x00;
    buffer[ 5 + idx ] = itsCout;
    return true;
}

bool AntennaTuner::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    itsData.clear();


    itsData = ComByteArray( &buffer[0], bufferSize );

    if( 0 == itsID )
    {
        if ( bufferSize < 6 )
            return false;
        itsCin = itsData.at(1);
        itsClen = itsData.at(3);
        itsCout = itsData.at(5);
    }
    else
    {
        if ( bufferSize < 12 )
            return false;
        itsCin = itsData.at(7);
        itsClen = itsData.at(9);
        itsCout = itsData.at(11);
    }

    if (itsStatus != READER_NO_ERROR)
        return false;
    return true;
}

void AntennaTuner::setID( quint8 id )
{
    itsID = id;
}

void AntennaTuner::setCin( quint8 cin )
{
    itsSetCin = true;
    itsCin = cin;
}

void AntennaTuner::setClen( quint8 clen )
{
    itsSetClen = true;
    itsClen = clen;
}

void AntennaTuner::setCout( quint8 cout )
{
    itsSetCout = true;
    itsCout = cout;
}

void AntennaTuner::setData( quint8 cin, quint8 clen, quint8 cout )
{
    setCin(cin);
    setClen(clen);
    setCout(cout);
}

void AntennaTuner::getData( quint8 &cin, quint8 &clen, quint8 &cout )
{
    cin = itsCin;
    clen = itsClen;
    cout = itsCout;
}

/************************************************************************/
bool AutoTuner::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
        return false;

    buffer[ 0 ] = itsAutoTune;
    return true;
}

void AutoTuner::setAutoTune( quint8 autotune )
{
    itsAutoTune = autotune;
}


/************************************************************************/
bool MaxTunerTableSize::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsRxSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = AS3993_TUNER_TABLE_SIZE;
    return true;
}

bool MaxTunerTableSize::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    itsData.clear();
    if ( bufferSize < itsDeserialSize )
        return false;

    itsData = ComByteArray( &buffer[0], bufferSize );
    itsTunerTableSize = itsData.at(1);

    if (itsStatus != READER_NO_ERROR || itsData.at(0) != AS3993_TUNER_TABLE_SIZE)
        return false;

    return true;
}


/************************************************************************/
bool ClearTunerTable::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsRxSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = AS3993_TUNER_CLEAR_TABLE;
    return true;
}

bool ClearTunerTable::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsDeserialSize || buffer[0] != AS3993_TUNER_CLEAR_TABLE || itsStatus != READER_NO_ERROR )
        return false;

    return true;
}


/************************************************************************/
bool AddTunerTableEntry::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsRxSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = AS3993_TUNER_ADD_TABLE;
    buffer[ 1 ] =  itsFreq & 0x000000FF;
    buffer[ 2 ] = (itsFreq & 0x0000FF00) >> 8;
    buffer[ 3 ] = (itsFreq & 0x00FF0000) >> 16;
    buffer[ 4 ] = itsTuneEnable1 ? 1 : 0;
    buffer[ 5 ] = itsCin1;
    buffer[ 6 ] = itsClen1;
    buffer[ 7 ] = itsCout1;
    buffer[ 8 ] = itsIq1 & 0x00FF;
    buffer[ 9 ] = (itsIq1 & 0xFF00) >> 8;
    buffer[ 10 ] = itsTuneEnable2 ? 1 : 0;
    buffer[ 11 ] = itsCin2;
    buffer[ 12 ] = itsClen2;
    buffer[ 13 ] = itsCout2;
    buffer[ 14 ] = itsIq2 & 0x00FF;
    buffer[ 15 ] = (itsIq2 & 0xFF00) >> 8;

    return true;
}

bool AddTunerTableEntry::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsDeserialSize || buffer[0] != AS3993_TUNER_ADD_TABLE || itsStatus != READER_NO_ERROR )
        return false;

    return true;
}

void AddTunerTableEntry::setFreq( quint32 freq )
{
    itsFreq = freq;
}

void AddTunerTableEntry::setTuningAntenna1( quint8 cin1, quint8 clen1, quint8 cout1, quint16 iq1 )
{
    itsTuneEnable1 = true;
    itsCin1 = cin1;
    itsClen1 = clen1;
    itsCout1 = cout1;
    itsIq1 = iq1;
}

void AddTunerTableEntry::setTuningAntenna2( quint8 cin2, quint8 clen2, quint8 cout2, quint16 iq2 )
{
    itsTuneEnable2 = true;
    itsCin2 = cin2;
    itsClen2 = clen2;
    itsCout2 = cout2;
    itsIq2 = iq2;
}
