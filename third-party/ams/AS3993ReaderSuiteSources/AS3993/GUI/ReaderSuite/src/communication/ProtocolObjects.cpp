/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file ProtocolObjects.cpp
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Implementation of classes to manipulate (rf) protocol related parameters
 */

#include "ProtocolObjects.h"
#include "QDebug"
#define AS3993_GEN2_CONST_DATA_SIZE 0x08

bool Gen2Parameters::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = itsSetLinkFreq ? 0x01 : 0x00;
    buffer[ 1 ] = itsLinkFreq;
    buffer[ 2 ] = itsSetCoding ? 0x01 : 0x00;
    buffer[ 3 ] = itsCoding;
    buffer[ 4 ] = itsSetSession ? 0x01 : 0x00;
    buffer[ 5 ] = itsSession;
    buffer[ 6 ] = itsSetLongPilot ? 0x01 : 0x00;
    buffer[ 7 ] = itsLongPilot;
    buffer[ 8 ] = itsSetTari ? 0x01 : 0x00;
    buffer[ 9 ] = itsTari;
    buffer[ 10 ] = itsSetQbegin ? 0x01 : 0x00;
    buffer[ 11 ] = itsQbegin;
    buffer[ 12 ] = itsSetSel ? 0x01 : 0x00;
    buffer[ 13 ] = itsSel;
    buffer[ 14 ] = itsSetTarget ? 0x01 : 0x00;
    buffer[ 15 ] = itsTarget;
    return true;
}

bool Gen2Parameters::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    itsData.clear();
    if ( bufferSize < itsDeserialSize )
        return false;

    itsData = ComByteArray( &buffer[0], bufferSize );
    itsLinkFreq = itsData.at(1);
    itsCoding = itsData.at(3);
    itsSession = itsData.at(5);
    if (itsData.at(7) == 0)
        itsLongPilot = false;
    else
        itsLongPilot = true;
    itsTari = itsData.at(9);
    itsQbegin = itsData.at(11);
    itsSel = itsData.at(13);
    itsTarget = itsData.at(15);
    //qDebug()<<"Target:"<<itsTarget;
    return true;
}

void Gen2Parameters::setLinkFreq( quint32 linkfreq )
{

    itsSetLinkFreq = true;
    switch(linkfreq){
    case 40:  itsLinkFreq = 0;  break;
    case 80:  itsLinkFreq = 3;  break;
    case 160: itsLinkFreq = 6;  break;
    case 213: itsLinkFreq = 8;  break;
    case 256: itsLinkFreq = 9;  break;
    case 320: itsLinkFreq = 12; break;
    case 640: itsLinkFreq = 15; break;
    default:  itsLinkFreq = 6;  break;
    }
}

quint32 Gen2Parameters::linkFreq()
{
    quint32 lf_khz;
    switch(itsLinkFreq){
    case 0 : lf_khz = 40 ;  break;
    case 3 : lf_khz = 80 ;  break;
    case 6 : lf_khz = 160;  break;
    case 8 : lf_khz = 213;  break;
    case 9 : lf_khz = 256;  break;
    case 12: lf_khz = 320; break;
    case 15: lf_khz = 640; break;
    default: lf_khz = 777;  break;
    }
    return lf_khz;
}

void Gen2Parameters::setCoding( unsigned char coding )
{
    itsSetCoding = true;
    switch(coding){
    case 0: itsCoding = 0; break;
    case 2: itsCoding = 1; break;
    case 4: itsCoding = 2; break;
    case 8: itsCoding = 3; break;
    default: itsCoding = 1;
    }
}

void Gen2Parameters::setSession( unsigned char session )
{
    itsSetSession = true;
    itsSession = session;
}

void Gen2Parameters::setLongPilot( bool pilot )
{
    itsSetLongPilot = true;
    itsLongPilot = pilot;
}

void Gen2Parameters::setTari( unsigned char tari )
{
    itsSetTari = true;
    itsTari = tari;
}

void Gen2Parameters::setQbegin( unsigned char q )
{
    itsSetQbegin = true;
    itsQbegin = q;
}

unsigned char Gen2Parameters::coding()
{
    unsigned char coding;
    switch(itsCoding){
    case 0:  coding = 0; break;
    case 1:  coding = 2; break;
    case 2:  coding = 4; break;
    case 3:  coding = 8; break;
    default: coding = 42;
    }
    return coding;
}
void Gen2Parameters::setTarget( unsigned char target )
{
    itsTarget = target;
    itsSetTarget = true;
}

/************************************************************************/

bool Gen2SingleInventoryStart::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = itsAutoAckMode ? 1 : 0;
    buffer[ 1 ] = itsFastMode ? 1 : 0;
    buffer[ 1 ] |= (itsTIDMode ? 1 : 0) << 1;
    buffer[ 2 ] = itsRssiMode;
    return true;
}

void Gen2SingleInventoryStart::setFastMode( bool fast )
{
    itsFastMode = fast;
}

void Gen2SingleInventoryStart::setRssiMode( quint8 rssi )
{
    itsRssiMode = rssi;
}

void Gen2SingleInventoryStart::setAutoAckMode( bool autoAck )
{
    itsAutoAckMode = autoAck;
}

void Gen2SingleInventoryStart::setTidMode(bool tidmode)
{
    itsTIDMode = tidmode;
}


/************************************************************************/

bool Gen2GetInventoryData::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    bool ret = false;

    itsData.clear();
    if (bufferSize < 3)
        return ret;

    itsData = ComByteArray( buffer, bufferSize);

    QByteArray dataAll =itsData.toQByteArray();
    //qDebug() << dataAll.toHex();

    itsCyclicInventory = true;
    if (itsData.at(0) == 0)
        itsCyclicInventory = false;
    itsTagsToFetch = itsData.at(1);
    itsNumTags = itsData.at(2);

    if (itsTagsToFetch == 0 && bufferSize >= 7)
    {
        itsStatus = itsData.at(3);
        itsFrequency = itsData.at(4) | (itsData.at(5) << 8) | (itsData.at(6) << 16);
    }
    else
    {
         itsStatus = 0;
    }

    int offset = 3;     //offset in itsData, where current tag info is located
    ret = true;
    for(int i=0; i<itsNumTags; i++)
    {
        if (itsData.size() < offset + 7)
        {
            ret = false;
            break;
        }
        RawTagInfo t;
        unsigned int epclen;
        t.agc = itsData.at(offset);
        t.rssi = itsData.at(offset+1);
        t.freq = itsData.at(offset+2) | (itsData.at(offset+3) << 8) | (itsData.at(offset+4) << 16);
        itsFrequency = t.freq;
        epclen = itsData.at(offset+5);
        if (epclen == 0)
        {
            ret = false;
            break;
        }
            
        epclen -= 2;      //epclen = length - pclen(2)

        t.pc = itsData.mid(offset+6, 2);
        if (itsData.size() < offset + 8 + static_cast<long>(epclen))
        {
            ret = false;
            break;
        }
        t.epc = itsData.mid(offset+8, epclen);

        if (itsTIDMode)
        {
           QByteArray aa = itsData.mid(offset+8+epclen,1).toQByteArray().toHex();

           bool ok;
            int tidLength = aa.toInt(&ok, 16);
            t.tid = itsData.mid(offset+8+epclen+1,tidLength);
            offset += tidLength+1;
        }
        
        offset += AS3993_GEN2_CONST_DATA_SIZE + epclen;
        itsTags.append(t);
    }


    if (itsTagsToFetch == 0)
        itsDataReceiveFinished = true;
    return ret;
}

QList<RawTagInfo> Gen2GetInventoryData::tags()
{
    QList<RawTagInfo> tags(itsTags);
    itsTags.clear();
    return tags;
}



/************************************************************************/

bool Gen2CyclicInventoryStart::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = 1;    //update start/stop value
    buffer[ 1 ] = 1;    //start inventory rounds
    buffer[ 2 ] = itsAutoAckMode ? 1 : 0;
    buffer[ 3 ] = itsFastMode ? 1 : 0;
    buffer[ 3 ] |= (itsTIDMode ? 1 : 0) << 1;
    buffer[ 4 ] = itsRssiMode;
    return true;
}

void Gen2CyclicInventoryStart::setFastMode( bool fast )
{
    itsFastMode = fast;
}

void Gen2CyclicInventoryStart::setRssiMode( quint8 rssi )
{
    itsRssiMode = rssi;
}

void Gen2CyclicInventoryStart::setAutoAckMode( quint8 autoAck )
{
    itsAutoAckMode = autoAck;
}

void Gen2CyclicInventoryStart::setTidMode(bool tidmode)
{
    itsTIDMode = tidmode;
}

/************************************************************************/

bool Gen2CyclicInventoryStop::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsSerialSize )
    {
        return false;
    }

    buffer[ 0 ] = 1;    //update start/stop value
    buffer[ 1 ] = 0;    //stop inventory rounds
    buffer[ 2 ] = 0;
    buffer[ 3 ] = 0;
    buffer[ 4 ] = 0;
    return true;
}

