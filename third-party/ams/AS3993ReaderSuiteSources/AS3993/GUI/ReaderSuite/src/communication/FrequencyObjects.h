/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file FrequencyObjects.h
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Classes for manipulating frequency hopping related parameters
 */

#ifndef FREQUENCY_OBJECTS_H
#define FREQUENCY_OBJECTS_H

#include "AmsComObject.h"
#include "ComGlobals.h"

#define AS3993_CMD_FREQPARAMS       2

/** pure virtual base class for implementing frequency related transfers to FW.
    Child classes only have to implement freqRxSerialise (fill payload for transmit) and 
    freqDeserialise (evaluate result) which will be called from FrequencyBase::rxSerialise and
    FrequencyBase::deserialise.
 */
class FrequencyBase : public AmsComObject
{
public:
    FrequencyBase ( int rxSerialSize, int deserialSize ) : AmsComObject( AS3993_CMD_FREQPARAMS, 0, rxSerialSize, deserialSize )
    {        
    };

    ~FrequencyBase ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * ) { return false; }; /*final, do not implement in child class*/

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); /*final, do not implement in child class*/

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); /*final, do not implement in child class*/

    virtual bool fill( QXmlStreamReader * ) { return false; };

    virtual bool freqRxSerialise( unsigned char * buffer, int bufferSize ) = 0;

    virtual bool freqDeserialise( ComByteArray data ) = 0;

protected:
    ComByteArray itsData;
    
};


/************************************************************************/
/** class to retrieve rssi value for a specific frequency
 */
class RssiCom : public FrequencyBase
{
public:
    RssiCom ( ulong freq ) : FrequencyBase( 4, 3 )
        , itsI( 0 )
        , itsQ( 0 )
        , itsDBM( 0 )
        , itsFreq( freq )
    {        
    };

    ~RssiCom( ) { };

    virtual bool freqRxSerialise( unsigned char * buffer, int bufferSize );

    virtual bool freqDeserialise( ComByteArray data );

    void setFreq(ulong freq) { itsFreq = freq; };
    void rssiData( int &ichannel, int &qchannel, int &dBm );
    int iChannel() { return itsI; };
    int qChannel() { return itsQ; };
    int dBm() { return itsDBM; };

protected:
    int itsI, itsQ, itsDBM;
    ulong itsFreq;
};


/************************************************************************/
/** class to retrieve reflected power value for a specific frequency. Optionally
 *  the tuner network can be set to the matching value. (Only if reader has been tuned beforehand)
 */
class ReflectedPowerCom : public FrequencyBase
{
public:
    ReflectedPowerCom ( ulong freq, uchar tune ) : FrequencyBase( 5, 2 )
        , itsI( 0 )
        , itsQ( 0 )
        , itsFreq( freq )
        , itsTune( tune )
    {        
    };

    ~ReflectedPowerCom( ) { };

    virtual bool freqRxSerialise( unsigned char * buffer, int bufferSize );

    virtual bool freqDeserialise( ComByteArray data );

    void setFreq(ulong freq, uchar tune);
    void reflectedPower(int &ichannel, int &qchannel);
    int iChannel() { return itsI; };
    int qChannel() { return itsQ; };

protected:
    int itsI, itsQ;
    ulong itsFreq;
    uchar itsTune;
};


/************************************************************************/
/** class to add a frequency entry to the frequency list on the reader.
 * It is possible to delete the old list before adding the new frequency.
 * The rssi threshold and profile values are global values in the FW and not
 * stored for every frequency.
 */
class AddFrequencyCom : public FrequencyBase
{
public:
    AddFrequencyCom ( ) : FrequencyBase( 6, 1 )
        , itsClearFreqList( false )
        , itsFreq( 0 )
        , itsProfile( 0 )
        , itsSuccess( false )
    {        
    };

    ~AddFrequencyCom( ) { };

    virtual bool freqRxSerialise( unsigned char * buffer, int bufferSize );

    virtual bool freqDeserialise( ComByteArray data );

    void setFreqParameters( bool clearFreqList, ulong freq, uchar profile );
    bool success() { return itsSuccess; };

protected:
    bool itsClearFreqList;
    ulong itsFreq;
    uchar itsProfile;
    bool itsSuccess;
};


/************************************************************************/
/** class to get information about the frequency list on the reader
 */
class GetFrequencyCom : public FrequencyBase
{
public:
    GetFrequencyCom ( ) : FrequencyBase( 1, 9 )
        , itsMinFreq( 0 )
        , itsMaxFreq( 0 )
        , itsNumFreq( 0 )
        , itsNumFreqGui( 0 )
        , itsProfile( 0 )
    {        
    };

    ~GetFrequencyCom( ) { };

    virtual bool freqRxSerialise( unsigned char * buffer, int bufferSize );

    virtual bool freqDeserialise( ComByteArray data );

    /** get data which has been received. numFreq indicates the number of frequencies in the list of the reader.
     * numFreqGui is the number of frequencies sent by the GUI (could be more) */
    void freqData( ulong &minFreq, ulong &maxFreq, uchar &numFreq, uchar &numFreqGui );
    uchar profile() { return itsProfile; };

protected:
    ulong itsMinFreq;
    ulong itsMaxFreq;
    uchar itsNumFreq;
    uchar itsNumFreqGui;
    uchar itsProfile;
};


/************************************************************************/
/** class to set frequency hopping related parameters
 */
class SetFreqHoppingCom : public FrequencyBase
{
public:
    SetFreqHoppingCom ( ) : FrequencyBase( 8, 1 )
        , itsListeningTime( 0 )
        , itsMaxSendTime( 0 )
        , itsIdleTime( 0 )
        , itsRssi( 0 )
        , itsSuccess( false )
    {        
    };

    ~SetFreqHoppingCom( ) { };

    virtual bool freqRxSerialise( unsigned char * buffer, int bufferSize );

    virtual bool freqDeserialise( ComByteArray data );

    void setFreqParameters( ushort listeningTime, ushort maxSendTime, ushort idleTime, signed char rssi );
    bool success() { return itsSuccess; };

protected:
    ushort itsListeningTime;
    ushort itsMaxSendTime;
    ushort itsIdleTime;
    signed char itsRssi;
    bool itsSuccess;
};

/************************************************************************/
/** class to get frequency hopping related parameters
 */
class GetFreqHoppingCom : public FrequencyBase
{
public:
    GetFreqHoppingCom ( ) : FrequencyBase( 1, 7 )
        , itsListeningTime( 0 )
        , itsMaxSendTime( 0 )
        , itsIdleTime( 0 )
        , itsRssi( 0 )
    {        
    };

    ~GetFreqHoppingCom( ) { };

    virtual bool freqRxSerialise( unsigned char * buffer, int bufferSize );

    virtual bool freqDeserialise( ComByteArray data );

    void freqParameters( ushort &listeningTime, ushort &maxSendTime, ushort &idleTime, signed char &rssi );

protected:
    ushort itsListeningTime;
    ushort itsMaxSendTime;
    ushort itsIdleTime;
    signed char itsRssi;
};


/************************************************************************/
/** class to start a modulation sequence on the reader. This object only transmits
 *  data and does not expect a response as the reader enters modulation mode directly.
 */
class ContinuousModulationCom : public AmsComObject
{
public:
    ContinuousModulationCom ( ulong freq, ushort duration, bool random ) : AmsComObject( AS3993_CMD_FREQPARAMS, 18, 0, 0 )
        , itsFreq( freq )
        , itsDuration( duration )
        , itsRandom( random )
    {        
    };

    ~ContinuousModulationCom( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * xml );

    virtual bool rxSerialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool deserialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool fill( QXmlStreamReader * ) { return false; };

protected:
    ulong itsFreq;
    ushort itsDuration;
    bool itsRandom;
};

#endif /* FREQUENCY_OBJECTS_H */
