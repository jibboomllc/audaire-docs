/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file AntennaObjects.h
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Classes for manipulating antenna related parameters
 */

#ifndef ANTENNA_OBJECTS_H
#define ANTENNA_OBJECTS_H

#include "AmsComObject.h"
#include "ComGlobals.h"

#define AS3993_CMD_ANTENNAPOWER 1
#define AS3993_CMD_TXPARAMS     4
#define AS3993_CMD_ANTENNATUNER 15
#define AS3993_CMD_AUTOTUNER    14
#define AS3993_CMD_TUNERTABLE   13

class AntennaPowerObject : public AmsComObject
{
public:
    /* default c'tor */
    AntennaPowerObject ( )
        : AmsComObject( AS3993_CMD_ANTENNAPOWER, 0, 2, 1 )
        , itsOn( 0 )
        , itsEval( 0 )
    {
    };

    /* d'tor */
    ~AntennaPowerObject ( ) { };

    virtual bool serialise( unsigned char *, int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ) { return false; };
    
    ComByteArray get( ) { return itsData; };

    void setData(bool on, bool eval);

protected:
    ComByteArray itsData; /* deep copy */
    unsigned char itsOn;
    unsigned char itsEval;
};


/** class to set tx parameters of reader: antenna port, PA switch, output power level.
 *  only the parameters whose corresponding set function has been called will be changed on the reader,
 *  but all current parameters are retrieved from the reader.
 */
class TxParameters : public AmsComObject
{
public:
    /* default c'tor */
    TxParameters ( ) : AmsComObject( AS3993_CMD_TXPARAMS, 0, 4, 4 )
        , itsAntenna( 0 )
        , itsSetAntenna( false )
        , itsSensLevel( 0 )
        , itsSetSensLevel( false )
    {        
    };

    /* d'tor */
    ~TxParameters ( ) { };

    virtual bool serialise( unsigned char *, int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ) { return false; };

    void setSensitivity(signed char sensLevel);
    signed char getSensitivity() { return itsSensLevel; };
    void setAntenna(unsigned char antenna);
    unsigned char getAntenna() { return itsAntenna; };

    void getData( signed char &sensitivity, unsigned char &antenna );

protected:
    ComByteArray itsData;
    unsigned char itsAntenna;
    bool itsSetAntenna;
    signed char itsSensLevel;
    bool itsSetSensLevel;
};

/** class to set antenna tune parameters
 */
class AntennaTuner : public AmsComObject
{
public:
    /* default c'tor */
    AntennaTuner ( ) : AmsComObject( AS3993_CMD_ANTENNATUNER, 0, 12, 12 )
        , itsCin( 0 )
        , itsClen( 0 )
        , itsCout( 0 )
        , itsSetCin( false )
        , itsSetClen( false )
        , itsSetCout( false )
        , itsID( 0 )
    {        
    };

    /* d'tor */
    ~AntennaTuner ( ) { };

    virtual bool serialise( unsigned char *, int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ) { return false; };

    void setCin(quint8 cin);
    quint8 getCin() { return itsCin; };
    void setClen(quint8 clen);
    quint8 getClen() { return itsClen; };
    void setCout(quint8 cout);
    quint8 getCout() { return itsCout; };
    void setID( quint8 id );

    void setData( quint8 cin, quint8 clen, quint8 cout );
    void getData( quint8 &cin, quint8 &clen, quint8 &cout );

protected:
    ComByteArray itsData;
    quint8 itsCin;
    bool itsSetCin;
    quint8 itsClen;
    bool itsSetClen;
    quint8 itsCout;
    bool itsSetCout;
    quint8 itsID;
};


/** class to set trigger auto tune cycle
 */
class AutoTuner : public AmsComObject
{
public:
    /* default c'tor */
    AutoTuner ( ) : AmsComObject( AS3993_CMD_AUTOTUNER, 1, 0, 0 )
        , itsAutoTune( 0 )
    {        
    };

    /* d'tor */
    ~AutoTuner ( ) { };

    virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

    virtual bool rxSerialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool deserialise( unsigned char *, int, QXmlStreamWriter * ) { return false; };

    virtual bool fill( QXmlStreamReader * ) { return false; };

    void setAutoTune(quint8 autotune);

protected:
    quint8 itsAutoTune;
};


/** class to get the maximum tuning table size
 */
class MaxTunerTableSize : public AmsComObject
{
public:
    /* default c'tor */
    MaxTunerTableSize ( ) : AmsComObject( AS3993_CMD_TUNERTABLE, 0, 1, 3 )
        , itsTunerTableSize( 0 )
    {        
    };

    /* d'tor */
    ~MaxTunerTableSize ( ) { };

    virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) { return false; };

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * );

    virtual bool fill( QXmlStreamReader * ) { return false; };

    quint8 tunerTableSize() { return itsTunerTableSize; };

protected:
    ComByteArray itsData;
    quint8 itsTunerTableSize;
};


/** class to clear tuner table on reader
 */
class ClearTunerTable : public AmsComObject
{
public:
    /* default c'tor */
    ClearTunerTable ( ) : AmsComObject( AS3993_CMD_TUNERTABLE, 0, 1, 3 )
    {        
    };

    /* d'tor */
    ~ClearTunerTable ( ) { };

    virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) { return false; };

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * );

    virtual bool fill( QXmlStreamReader * ) { return false; };

protected:
    ComByteArray itsData;
};


/** class to add entry to tuner table on reader
 */
class AddTunerTableEntry : public AmsComObject
{
public:
    /* default c'tor */
    AddTunerTableEntry ( ) : AmsComObject( AS3993_CMD_TUNERTABLE, 0, 16, 3 )
        , itsTuneEnable1( false )
        , itsTuneEnable2( false )
        , itsFreq( 0 )
        , itsCin1( 0 ), itsClen1( 0 ), itsCout1( 0 )
        , itsCin2( 0 ) , itsClen2( 0 ), itsCout2( 0 )
        , itsIq1( 0 ), itsIq2( 0 )
    {        
    };

    /* d'tor */
    ~AddTunerTableEntry ( ) { };

    virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ) { return false; };

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * );

    virtual bool fill( QXmlStreamReader * ) { return false; };

    void setFreq( quint32 freq);
    void setTuningAntenna1(quint8 cin1, quint8 clen1, quint8 cout1, quint16 iq1);
    void setTuningAntenna2(quint8 cin2, quint8 clen2, quint8 cout2, quint16 iq2);

protected:
    bool itsTuneEnable1, itsTuneEnable2;
    quint32 itsFreq;
    quint8 itsCin1, itsClen1, itsCout1;
    quint16 itsIq1;
    quint8 itsCin2, itsClen2, itsCout2;
    quint16 itsIq2;
    ComByteArray itsData;
};

#endif /* ANTENNA_OBJECTS_H */
