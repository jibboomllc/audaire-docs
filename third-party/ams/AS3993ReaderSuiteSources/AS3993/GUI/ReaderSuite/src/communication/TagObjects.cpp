/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file TagObjects.cpp
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Implementation of classes to access tags
 */

#include "TagObjects.h"

bool TagRead::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < this->serialiseSize() )
    {
        return false;
    }

    buffer[ 0 ] = itsMemType;
    writeU32IntoLittleEndianBuffer(itsAddress, &buffer[1]);
    buffer[ 5 ] = itsPassword.value(0,0);
    buffer[ 6 ] = itsPassword.value(1,0);
    buffer[ 7 ] = itsPassword.value(2,0);
    buffer[ 8 ] = itsPassword.value(3,0);

    return true;
}

bool TagRead::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsDeserialSize )
    {
        return false;
    }
    itsTagReadData.clear();
    if ( bufferSize < 1 )
        return false;

    itsNumBytesRead = bufferSize;
    itsTagReadData = ComByteArray( &buffer[0], itsNumBytesRead );
    decodeGen2Result(buffer[bufferSize - 1]);

    return true;
}

void TagRead::setPassword( const ComByteArray pw )
{
    itsPassword = pw;
}

void TagRead::setMemoryAddress( const quint8 memType, const quint32 address, const quint8 numWords, bool bulkMode )
{
    itsMemType = memType;
    itsAddress = address;
    itsDataLen = numWords;
    itsBulkMode = bulkMode; /* if false memory will be read word by word */
}

bool TagRead::getData( ComByteArray & data )
{
    data = itsTagReadData.mid(0,itsTagReadData.size()/2*2);
    return itsSuccess;
}

int TagRead::deserialiseSize( ) const
{ 
    return AmsComObject::deserialiseSize() + itsDataLen * 2 + (itsBulkMode?0:1); 
}

Gen2ReaderInterface::Gen2Result TagObject::decodeGen2Result(quint8 tag_byte)
{
    Gen2ReaderInterface::Gen2Result gen2Result = static_cast<Gen2ReaderInterface::Gen2Result>(status());

    if (gen2Result == Gen2ReaderInterface::Gen2_HEADER)
    {
        gen2Result = static_cast<Gen2ReaderInterface::Gen2Result>(Gen2ReaderInterface::Gen2_Tag_OTHER + tag_byte);
    }

    itsGen2Result = gen2Result;
    return gen2Result;
}


/************************************************************************/
bool TagWrite::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < this->serialiseSize())
    {
        return false;
    }

    buffer[ 0 ] = itsMemType;
    writeU32IntoLittleEndianBuffer(itsAddress, &buffer[1]);
    buffer[ 5 ] = itsTagPassword[0];
    buffer[ 6 ] = itsTagPassword[1];
    buffer[ 7 ] = itsTagPassword[2];
    buffer[ 8 ] = itsTagPassword[3];
    for ( int i = 0; i<itsTagWriteData.size(); i++)
    {
        buffer[9+i] = itsTagWriteData[i];

    }
    return true;
}

bool TagWrite::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsDeserialSize )
        return false;

    itsWordsWritten = buffer[0];
    itsTagError = buffer[1];
    decodeGen2Result(itsTagError);

    return true;
}

int TagWrite::rxSerialiseSize() const
{
    return itsRxSerialSize + itsTagWriteData.size();
}

void TagWrite::setMemoryAddress( const quint8 memType, const quint32 address)
{
    itsMemType = memType;
    itsAddress = address;
}

bool TagWrite::setData( const ComByteArray & data )
{
    itsTagWriteData = data;
    return true;
}

void TagWrite::setPassword( const ComByteArray & data )
{
    itsTagPassword = data;
}
/************************************************************************/
bool TagLock::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < this->serialiseSize())
    {
        return false;
    }

    buffer[ 0 ] = itsMaskAndAction.value(0,0);
    buffer[ 1 ] = itsMaskAndAction.value(1,0);
    buffer[ 2 ] = itsMaskAndAction.value(2,0);
    buffer[ 3 ] = itsTagPassword[0];
    buffer[ 4 ] = itsTagPassword[1];
    buffer[ 5 ] = itsTagPassword[2];
    buffer[ 6 ] = itsTagPassword[3];
    return true;
}

bool TagLock::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsDeserialSize )
        return false;

    itsTagError = buffer[0];
    decodeGen2Result(itsTagError);

    return true;
}

void TagLock::setPassword( const ComByteArray & data )
{
    itsTagPassword = data;
}

void TagLock::setMemBankLock( enum MemBank membank, enum Protection p)
{
    unsigned i_maa = 0;
    i_maa = 3 << (2*membank + 10);
    i_maa |= p << (2*membank);
    itsMaskAndAction = ComByteArray(3,'\0');
    itsMaskAndAction [ 0 ] = (i_maa >> 12) & 0xff;
    itsMaskAndAction [ 1 ] = (i_maa >>  4) & 0xff;
    itsMaskAndAction [ 2 ] = (i_maa << 4) & 0xf0;
};


/************************************************************************/
bool TagKill::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < this->serialiseSize())
    {
        return false;
    }

    buffer[ 0 ] = itsTagPassword.value(0,0);
    buffer[ 1 ] = itsTagPassword.value(1,0);
    buffer[ 2 ] = itsTagPassword.value(2,0);
    buffer[ 3 ] = itsTagPassword.value(3,0);
    buffer[ 4 ] = itsRfu;
    buffer[ 5 ] = itsRecom;
    return true;
}

bool TagKill::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsDeserialSize )
        return false;

    itsTagError = buffer[0];
    decodeGen2Result(itsTagError);

    return true;
}

void TagKill::setPassword( const ComByteArray & data )
{
    itsTagPassword = data;
}

void TagKill::setRecomRfu( quint8 recom, quint8 rfu )
{
    itsRecom = recom;
    itsRfu = rfu;
}

/************************************************************************/
bool TagSelect::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < rxSerialiseSize() )
    {
        return false;
    }

    buffer[ 0 ] = itsSelectMode;
    if (itsSelectMode == Clear) return true;
    buffer[ 1 ] = itsTarget;
    buffer[ 2 ] = itsAction;
    buffer[ 3 ] = itsMemBank;
    buffer[ 4 ] = itsMaskAddress & 0xff;
    buffer[ 5 ] = itsMaskAddress >> 8;
    buffer[ 6 ] = itsMaskLen;
    buffer[ 7 ] = itsTruncation;
    for (int i=0; i<itsMask.length(); i++)
    {
        buffer[8+i] = itsMask.at(i);
    }
    return true;
}

bool TagSelect::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * )
{
    if ( bufferSize < itsDeserialSize )
        return false;

    decodeGen2Result(0);

    return true;
}

int TagSelect::serialiseSize() const
{
    return rxSerialiseSize();
}

int TagSelect::rxSerialiseSize() const
{
    return itsRxSerialSize + ((itsSelectMode)?(itsMask.size()+7):0);
}

void TagSelect::setEpc( const ComByteArray &epc )
{
    itsMask = epc;
    if (itsMask.size()<32)
        itsMaskLen = itsMask.size() * 8;
    else
        itsMaskLen = 255;
    itsTarget = 0;
    itsAction = 0;
    itsMemBank = 1;
    itsMaskAddress = 0x20;
    itsTruncation = 0;
}

void TagSelect::setTID( const ComByteArray &tid )
{
    itsMask = tid;
    if (itsMask.size()<32)
        itsMaskLen = itsMask.size() * 8;
    else
        itsMaskLen = 255;

    itsTarget = 0;
    itsAction = 0;
    itsMemBank = 2;
    itsMaskAddress = 0x00;
    itsTruncation = 0;
}


const QByteArray& GenericCMDObject::get()
{
    return itsIn;
}

void GenericCMDObject::set(const QByteArray &out)
{
    itsOut = out;
    itsSerialSize = out.size();
    itsRxSerialSize = out.size();
}


bool GenericCMDObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    if ( bufferSize < itsOut.size() + 1)
    {
        return false;
    }
    memcpy(buffer, itsOut.data(), itsOut.size());
    return true;
}

bool GenericCMDObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    return serialise( buffer, bufferSize, xml);
}

bool GenericCMDObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    itsIn.clear();
    itsIn.append((char*)buffer, bufferSize);
    decodeGen2Result(buffer[0]);
    return true;
}

const QByteArray& RssiObject::get()
{
    return itsIn;
}

const QList<struct rssi_val>& RssiObject::getRssis()
{   
    struct rssi_val val;

    for (int i = 0; i < itsIn.size(); i+=4)
    {
        val.agc = itsIn[i];
        val.log_rssis = itsIn[i+1];
        val.lin_irssi = itsIn[i+2];
        val.lin_qrssi = itsIn[i+3];
        itsList.append(val);
    }
    return itsList;
}

bool RssiObject::serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    if (bufferSize < itsSerialSize)
        return false;

    buffer[0] =  itsFreq & 0x000000FF;
    buffer[1] = (itsFreq & 0x0000FF00) >> 8;
    buffer[2] = (itsFreq & 0x00FF0000) >> 16;

    return true;
}

bool RssiObject::rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    if (bufferSize < itsRxSerialSize)
        return false;

    buffer[0] =  itsFreq & 0x000000FF;
    buffer[1] = (itsFreq & 0x0000FF00) >> 8;
    buffer[2] = (itsFreq & 0x00FF0000) >> 16;
    return serialise( buffer, bufferSize, xml);
}

bool RssiObject::deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml )
{
    itsIn.clear();
    itsIn.append((char*)buffer, bufferSize);
    return true;
}