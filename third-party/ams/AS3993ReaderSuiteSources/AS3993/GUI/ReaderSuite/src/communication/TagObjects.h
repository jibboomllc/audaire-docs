/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AS3993 
 *      $Revision: $
 *      LANGUAGE: C++
 */

/*! \file TagObjects.h
 *
 *  \author Bernhard Breinbauer
 *
 *  \brief Classes for accessing tags (read, write, kill, ...)
 */

#ifndef TAG_OBJECTS_H
#define TAG_OBJECTS_H

#include "AmsComObject.h"
#include "ComGlobals.h"
#include "../Reader/Gen2ReaderInterface.h"

#define AS3993_CMD_SELECTTAG        6
#define AS3993_CMD_WRITETAG         7
#define AS3993_CMD_READTAG          8
#define AS3993_CMD_LOCKTAG          9
#define AS3993_CMD_KILLTAG          10
#define AS3993_CMD_GENERIC          20
#define AS3993_CMD_MEAS_RSSI        21

class TagObject : public AmsComObject
{
public:
    TagObject
    (     unsigned char protocol
        , int serialSize = 0
        , int rxSerialSize = 0
        , int deserialSize = 0
        ) : AmsComObject( protocol, serialSize, rxSerialSize, deserialSize )
    { itsGen2Result = Gen2ReaderInterface::Gen2_Tag_OTHER; };

    Gen2ReaderInterface::Gen2Result decodeGen2Result(quint8 tagByte);
    Gen2ReaderInterface::Gen2Result getGen2Result() const { return itsGen2Result; };
private:
    Gen2ReaderInterface::Gen2Result itsGen2Result;
};

/** class to read memory of tag
 */
class TagRead : public TagObject
{
public:
    /* default c'tor */
    TagRead ( ) : TagObject( AS3993_CMD_READTAG, 0, 9, 0 )
    , itsSuccess( false )
    , itsNumBytesRead( 0 )
    , itsMemType( 0 )
    , itsAddress( 0 )
    , itsDataLen( 0 )
    , itsBulkMode( false )
    {
    };

    /* d'tor */
    ~TagRead ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ) { return false; };

    virtual int deserialiseSize ( ) const;

    void setMemoryAddress( const quint8 memType, const quint32 address, const quint8 numWords, bool bulkMode );
    void setPassword( const ComByteArray data );
    bool getData( ComByteArray & data );

protected:
    ComByteArray itsTagReadData;
    ComByteArray itsPassword;
    bool itsSuccess;
    quint8 itsNumBytesRead;
    quint8 itsMemType;
    quint32 itsAddress;
    quint8 itsDataLen;
    bool itsBulkMode;
};

/** class to read memory of tag
 */
class TagWrite : public TagObject
{
public:
    /* default c'tor */
    TagWrite ( ) : TagObject( AS3993_CMD_WRITETAG, 0, 9, 2 )
        , itsWordsWritten( 0 )
        , itsTagError( 0 )
        , itsMemType( 0 )
        , itsAddress( 0 )
    {        
    };

    /* d'tor */
    ~TagWrite ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ) { return false; };
    
    virtual int rxSerialiseSize ( ) const;

    void setMemoryAddress( const quint8 memType, const quint32 address );
    bool setData( const ComByteArray & data );
    void setPassword( const ComByteArray & data );
    quint8 getTagError() const {return itsTagError;};
    quint8 getWordsWritten() const {return itsWordsWritten;};

protected:
    ComByteArray itsTagWriteData;
    ComByteArray itsTagPassword;
    quint8 itsWordsWritten;
    quint8 itsTagError;
    quint8 itsMemType;
    quint32 itsAddress;
};
/** class to lock/unlock a tag
 */
class TagLock : public TagObject
{
public:
    /* default c'tor */
    TagLock ( ) : TagObject( AS3993_CMD_LOCKTAG, 0, 7, 1 )
        , itsTagError( 0 )
    {        
    };

    enum MemBank{
        TO_User = 0,
        TO_TID = 1,
        TO_EPC = 2,
        TO_AccessPwd = 3,
        TO_KillPwd = 4,
    };

    enum Protection{
        None = 0x00,
        Lock = 0x02,
        PermaLock = 0x01,
        Both = 0x03
    };

    /* d'tor */
    ~TagLock ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ) { return false; };
    
    void setPassword( const ComByteArray & data );
    void setMemBankLock( enum MemBank membank, enum Protection p);
    quint8 getTagError() const {return itsTagError;};
    
protected:
    ComByteArray itsMaskAndAction;
    ComByteArray itsTagPassword;
    quint8 itsTagError;
};

/** class to kill a tag
 */
class TagKill : public TagObject
{
public:
    /* default c'tor */
    TagKill ( ) : TagObject( AS3993_CMD_KILLTAG, 0, 6, 1 ), itsRfu(0), itsRecom(0), itsTagError( 0 )
    {
    };

    /* d'tor */
    ~TagKill ( ) { };

    virtual bool serialise( unsigned char * , int, QXmlStreamWriter * ) { return false; }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ) { return false; };
    
    void setPassword( const ComByteArray & data );
    void setRecomRfu( quint8 recom, quint8 rfu = 0);
    quint8 getTagError() const {return itsTagError;};

protected:
    ComByteArray itsTagPassword;
    quint8 itsRecom;
    quint8 itsRfu;
    quint8 itsTagError;
};


/************************************************************************/
/** class to select tag on reader and mark it as target for next tag access call.
 */
class TagSelect : public TagObject
{
public:
    enum SelListMode
    {
        Clear = 0,
        Add = 1,
        Clear_and_add = 2
    };

    /* default c'tor */
    TagSelect ( enum SelListMode m = Clear_and_add ) : TagObject( AS3993_CMD_SELECTTAG, 2, 2, 0 ),
                    itsTarget(0),
                    itsAction(0),
                    itsMemBank(1),
                    itsMaskAddress(0x20),
                    itsSelectMode(m),
                    itsTruncation(0), 
                    itsTagError( 0 )

    {        
    };

    /* d'tor */
    ~TagSelect ( ) { };

    virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml) { return rxSerialise(buffer, bufferSize, xml); }; 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual bool fill( QXmlStreamReader * ) { return false; };

    virtual int rxSerialiseSize ( ) const;

    virtual int serialiseSize ( ) const;

    void setEpc( const ComByteArray &epc );

    void setTID( const ComByteArray &tid );

    quint8 getTagError() const {return itsTagError;};

protected:
    ComByteArray itsMask;
    quint8 itsTagError;
    quint8 itsMaskLen;
    quint8 itsTarget;
    quint8 itsAction;
    quint8 itsMemBank;
    quint16 itsMaskAddress;
    quint8 itsTruncation;
    enum SelListMode itsSelectMode;
};

/* class to reset MCU, peripherals or communication */
class GenericCMDObject : public TagObject
{
public:

    /* default c'tor */
    GenericCMDObject ( unsigned char cmd, unsigned short tx_len = 0,  unsigned short rx_len = 0 ) : TagObject( cmd, tx_len, tx_len, rx_len), itsAS3911Command( cmd ) { };

    /* copy c'tor */
    GenericCMDObject ( const GenericCMDObject & other ) : TagObject( other ), itsAS3911Command( 0 ) { };

    /* d'tor */
    ~GenericCMDObject ( ) { };

    virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual const QByteArray& get();
    virtual void set(const QByteArray&);

    virtual bool fill( QXmlStreamReader * xml ) { return false; };

protected:
    unsigned char itsAS3911Command; /* bit mask of objects to reset */
private:
    QByteArray itsOut;
    QByteArray itsIn;
};

struct rssi_val
{
    unsigned char agc;
    unsigned char log_rssis;
    signed char lin_irssi;
    signed char lin_qrssi;
};

/* class to reset MCU, peripherals or communication */
class RssiObject : public AmsComObject
{
public:

    /* default c'tor */
    RssiObject ( unsigned char measurements ) : AmsComObject( AS3993_CMD_MEAS_RSSI, 4, 4, measurements*4), itsFreq(0xd3000){ };

    /* copy c'tor */
    RssiObject ( const RssiObject & other ) : AmsComObject( other ) { };

    /* d'tor */
    ~RssiObject ( ) { };

    virtual bool serialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

    virtual bool rxSerialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml ); 

    virtual bool deserialise( unsigned char * buffer, int bufferSize, QXmlStreamWriter * xml );

    virtual const QByteArray& get();
    virtual const QList<struct rssi_val>& getRssis();

    virtual bool fill( QXmlStreamReader * xml ) { return false; };

    void setFreq(ulong freq) { itsFreq = freq; };

protected:
    ulong itsFreq;

private:
    QByteArray itsIn;
    QList<struct rssi_val> itsList;

};


#endif /* TAG_OBJECTS_H */
