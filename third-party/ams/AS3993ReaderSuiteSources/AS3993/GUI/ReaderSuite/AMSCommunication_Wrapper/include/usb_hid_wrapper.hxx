/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

/*
 *      PROJECT:   AMSCommunication
 *      $Revision: $
 *      LANGUAGE: QT C++
 */

/*! \file
 *
 *  \author S. Puri
 *
 *  \brief  Communication class for communication via USB HID
 *
 *  This controls the communication to the SiLabs C8051F34x.
 *  Actual USB access is handled via SLABHIDDevice.dll.
 *  Human Interface Device (HID) Tutorials (AN249).
 */

#ifndef USB_HID_WRAPPER_H
#define USB_HID_WRAPPER_H

#include <AMSCommunication.hxx>
#include "reader\ReaderInterface.h"


class USBHIDWrapper : public AMSCommunication
{
	Q_OBJECT
public:
	USBHIDWrapper(unsigned char devAddr);
	~USBHIDWrapper();

	AMSCommunication::Error hwConnect();
	void hwDisconnect();
	AMSCommunication::Error hwReadRegister(unsigned char reg, unsigned char *val);
	AMSCommunication::Error hwWriteRegister(unsigned char reg, unsigned char val);
	AMSCommunication::Error hwSendCommand(QString command, QString * answer);
	AMSCommunication::Error writeSubRegister(unsigned char reg, unsigned char val, unsigned char subAddress, bool verify=false, bool doemit=false, unsigned char trcLevel=0);
	AMSCommunication::Error readSubRegister(unsigned char reg, unsigned char *val, unsigned char subAddress, bool doemit=false, unsigned char trcLevel=0);

public slots:
	void gotReader(ReaderInterface* ph);
	void lostReader(ReaderInterface* ph);

protected:
	void setConnectionProperties(void *);

private:
	void update(void);

	unsigned char devAddr;

	QTime lastUpdate;
	QMap<int, quint8> valArray;
	int inputReportBufferLength;
	int outputReportBufferLength;
	int featureReportBufferLength;
	int maxReportRequest;

	ReaderInterface* ph;
};

#endif // USB_HID_WRAPPER_H
