/*
 *****************************************************************************
 * Copyright by ams AG                                                       *
 * All rights are reserved.                                                  *
 *                                                                           *
 * IMPORTANT - PLEASE READ CAREFULLY BEFORE COPYING, INSTALLING OR USING     *
 * THE SOFTWARE.                                                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT         *
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS         *
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT          *
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY     *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE     *
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *****************************************************************************
 */

#include <usb_hid_wrapper.hxx>


USBHIDWrapper::USBHIDWrapper(unsigned char devAddr)
{
	this->devAddr = devAddr;
	connected = false;
}

USBHIDWrapper::~USBHIDWrapper()
{
}


AMSCommunication::Error USBHIDWrapper::hwConnect()
{
	if(connected)
		return NoError;
	return ConnectionError;
}

void USBHIDWrapper::hwDisconnect()
{
}

void USBHIDWrapper::update()
{
	if ( lastUpdate.isValid() && lastUpdate.elapsed() < 1000)
	{
		return;
	}
	lastUpdate.start();
	if (!ph->readRegisters(valArray) == ReaderInterface::OK)
	{
		valArray.clear();
	}

}

AMSCommunication::Error USBHIDWrapper::hwReadRegister(unsigned char registerAddress, unsigned char* registerValue)
{
	int r = registerAddress;
	update();
    if (valArray.contains(registerAddress))
    {
	    *registerValue = valArray[r];
    }
    else
    {
        ph->readRegister(registerAddress, *registerValue);
    }
	return NoError;
}

AMSCommunication::Error USBHIDWrapper::readSubRegister(unsigned char registerAddress, unsigned char *registerValue, unsigned char subAddress, bool doemit, unsigned char trcLevel)
{
    //no subregisters
	return ReadError;
}

  /** USBCommunication::writeByte
   * This function sends one Byte to the specified address
   * In case of success ERR_NO_ERR is returned
   */
AMSCommunication::Error USBHIDWrapper::hwWriteRegister(unsigned char registerAddress, unsigned char registerValue)
{
	AMSCommunication::Error err = NoError;
	ReaderInterface::Result res;
	
	res = ph->writeRegister(registerAddress, registerValue);
	if(res == ReaderInterface::OK)
		err = NoError;
	return err;
}

AMSCommunication::Error USBHIDWrapper::writeSubRegister(unsigned char registerAddress, unsigned char registerValue, unsigned char subAddress, bool verify, bool doemit, unsigned char trcLevel)
{
    // no subregisters
	return WriteError;
}



AMSCommunication::Error USBHIDWrapper::hwSendCommand(QString command, QString * answer)
{
	return AMSCommunication::NoError;
}

void USBHIDWrapper::setConnectionProperties(void *)
{

}

void USBHIDWrapper::gotReader(ReaderInterface* ph)
{
	this->ph = ph;
	this->connected = true;
}

void USBHIDWrapper::lostReader(ReaderInterface* ph)
{
	if (this->ph != ph) return;
	this->ph = NULL;
	this->connected = false;
	// do some more clean up here
}
